import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
//import Header from './componentsAPI/Header/Header';
import UniversalHeader from "./componentsAPI/Header/UniversalHeaderFolder/UniversalHeader";
//import Footer from "./componentsAPI/Footer/Footer";
import UniversalFooter from "./componentsAPI/Footer/UniversalFooterFolder/UniversalFooter";
import Grid from '@material-ui/core/Grid';
import { IntlProvider } from "react-intl";
import translations from './translations';
import AppRoutes from './routes';
import './App.css';
import { Helmet } from "react-helmet";
import { StylesProvider } from '@material-ui/core/styles';
import Keycloak from 'keycloak-js';
import setupAxiosInterceptors from "./config/setupAxios";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import ScrollToTop from "react-scroll-to-top";
import { KEYCLOAK_URL, getLogoutUrl } from "./config/constants";
//import { addUnloadListener } from "./config/constants";
import ErrorBoundary from './componentsAPI/ErrorBoundary';
import CookieConsentComponent from "./componentsAPI/CookieConsentComponent";
import { Cookies } from "react-cookie-consent";
import ReactGA from 'react-ga';
import { PRODUCTION, BASE_URL } from "./config/constants";
const cookieconsent_status = Cookies.get("cookieconsent_status");

if (cookieconsent_status && cookieconsent_status === "allow" && PRODUCTION) {
  ReactGA.initialize('UA-136240201-2', {
    debug: !PRODUCTION,
    gaOptions: {
      //  siteSpeedSampleRate: 100
    }
  });
}
export let keycloak;
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { keycloak: null, authenticated: null }
    //addUnloadListener();//remove session storage key before leave catalogue
  }
  componentDidMount() {
    if (!this.state.keycloak) {
      keycloak = new Keycloak({
        "clientId": "react-client",
        "url": `${KEYCLOAK_URL}`,
        "realm": "ELG",
        "ssl-required": "all",//'all', 'external' and 'none'.
        "public-client": true,
      });
      //keycloak.init({ onLoad: 'check-sso', promiseType: 'native', "silentCheckSsoRedirectUri": window.location.origin + '/silent-check-sso.html' }).then(authenticated => {
      keycloak.init({ onLoad: 'check-sso', promiseType: 'native', "silentCheckSsoRedirectUri": `${window.location.origin}${(BASE_URL.indexOf('european-language-grid.eu/') >= 0 || window.location.origin.indexOf("192.168.188.194") >= 0 || PRODUCTION) ? '/catalogue/' : '/'}silent-check-sso.html` }).then(authenticated => {
        if (authenticated) {
          setInterval(() => {
            try {
              keycloak.updateToken(70).then((refreshed) => {
                if (refreshed) {
                  //console.debug('Token refreshed' + refreshed);
                  setupAxiosInterceptors(keycloak);
                } else {
                  //console.warn('Token not refreshed, valid for ' + Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
                }
              }).catch((err) => {
                console.error('Failed to refresh token', err);
                const logoutOptionsObject = { 'redirectUri': getLogoutUrl() }
                keycloak && keycloak.logout(logoutOptionsObject);
                //window.location.reload();
                //keycloak.clearToken();
              });
            } catch (catch_error) {
              console.log(catch_error);
            }
          }, 1000)//1 minute//60000
        }
        this.setState({ keycloak: keycloak, authenticated: authenticated });
      }).catch((response) => { console.log("keycloak error", response); });
      setupAxiosInterceptors(keycloak);
    }
  }
  //onKeycloakEvent = (event, error) => { console.log('onKeycloakEvent', event, error) }
  //onKeycloakTokens = tokens => { console.log('onKeycloakTokens', tokens) }

  render() {
    if (!this.state.keycloak) {
      return <div></div>
    }

    return (
      <StylesProvider injectFirst>
        <Grid container direction="row" className="app-main">
          <Router basename='/catalogue'>
            <ErrorBoundary>
              <Helmet>
                <title>European Language Grid - Catalogue</title>
                <meta name="description" content="The ELG develops and deploys a scalable cloud platform, providing, in an easy-to-integrate way, access to hundreds of commercial and non-commercial Language Technologies for all European languages, including running tools and services as well as data sets and resources.It will enable the commercial and non-commercial European LT community to deposit and upload their technologies and data sets into the platform, to deploy them through the grid, and to connect with other resources. The European Language Grid will boost the Multilingual Digital Single Market towards a thriving European Language Technology community, creating new jobs, new markets and new opportunities."></meta>
                <meta name="keywords" content="ELG, Part-of-Speech Tagging, Tokenization, Morphological annotation, Lemmatization, Dependency parsing, Machine Translation, Named Entity Recognition, Speech Recognition, Text-to-Speech Synthesis, Sentiment analysis, Named Entity Disambiguation, Polarity labelling, Language identification, Speech Synthesis, Summarization, Structural annotation, Text categorization, Sentence splitting, Event detection, Hate speech recognition, Morphological analysis, Annotation of measurements, Data labelling, Discourse annotation, Keyword extraction, Language Technology, Linguistic analysis, Noun phrase chunking, Polarity detection, Readability annotation, Semantic annotation, Speech annotation, Temporal Expression Analysis, Term extraction, Truth labelling, Word Sense Disambiguation "></meta>
                <link rel="canonical" href={`${BASE_URL}catalogue/`}></link>
                <meta property="og:url" content={`${BASE_URL}catalogue/`} />
                <meta property="og:site_name" content="European Language Grid" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="European Language Grid" />
                <meta property="og:description" content="The ELG develops and deploys a scalable cloud platform, providing, in an easy-to-integrate way, access to hundreds of commercial and non-commercial Language Technologies for all European languages, including running tools and services as well as data sets and resources.It will enable the commercial and non-commercial European LT community to deposit and upload their technologies and data sets into the platform, to deploy them through the grid, and to connect with other resources. The European Language Grid will boost the Multilingual Digital Single Market towards a thriving European Language Technology community, creating new jobs, new markets and new opportunities."></meta>
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:title" property="og:title" itemprop="name" content="European Language Grid" />
                <meta name="twitter:description" property="og:description" itemprop="description" content="The ELG develops and deploys a scalable cloud platform, providing, in an easy-to-integrate way, access to hundreds of commercial and non-commercial Language Technologies for all European languages, including running tools and services as well as data sets and resources.It will enable the commercial and non-commercial European LT community to deposit and upload their technologies and data sets into the platform, to deploy them through the grid, and to connect with other resources. The European Language Grid will boost the Multilingual Digital Single Market towards a thriving European Language Technology community, creating new jobs, new markets and new opportunities."></meta>
              </Helmet>
              <IntlProvider locale={"en"} messages={translations["en"]}>

                <ToastContainer position={toast.POSITION.BOTTOM_RIGHT} theme="dark" />
                <ScrollToTop smooth color="#7c5cd6" />
                <Grid item xs={12}><UniversalHeader keycloak={this.state.keycloak} /></Grid>
                <Grid container direction="column" className="page-wrapper">
                  <Grid item xs={12} className="main">
                    <div className="content-wrapper">
                      <AppRoutes keycloak={this.state.keycloak} />
                      {/*<Footer />*/}
                      <UniversalFooter />
                      {PRODUCTION && <CookieConsentComponent />}
                    </div>
                  </Grid>
                </Grid>
                
                
              </IntlProvider>
            </ErrorBoundary>
          </Router>
        </Grid>
      </StylesProvider>
    );
  }
}

export default App;

