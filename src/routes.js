import React, { lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import ProgressBar from "./componentsAPI/CommonComponents/ProgressBar";
import ConsumerRoute from './DashboardComponents/myValidations/ConsumerRoute';
//const DistributionDirectAccess = lazy(() => import("./componentsAPI/DistributionDirectAccess"));
//const UploadWithSteps = lazy(() => import("./componentsAPI/UploadWithSteps"));
const UploadXmlComponent = lazy(() => import("./componentsAPI/xmlUpload/UploadXmlComponent"));
const PublicValidateXml = lazy(() => import("./componentsAPI/xmlUpload/PublicValidateXml"));
const SearchCatalogue = lazy(() => import("./componentsAPI/SearchCatalogue"));
const ServiceTool = lazy(() => import("./componentsAPI/ServiceTool"));
const ProjectResourceDetail = lazy(() => import("./componentsAPI/ProjectResourceDetail"));
const LexicalConceptualResource = lazy(() => import('./componentsAPI/LexicalConceptualResource'));
const LanguageDescription = lazy(() => import('./componentsAPI/LanguageDescription'));
const Corpus = lazy(() => import("./componentsAPI/Corpus"));
const Organization = lazy(() => import("./componentsAPI/Organization"));
const SelectResource = lazy(() => import("./Editor/SelectResource"));
//const SearchMyResources = lazy(() => import("./DashboardComponents/SearchMyResources"));
//const SearchMyResources = lazy(() => import("./DashboardComponents/myResources/SearchMyResources"));
//const SearchMyValidations = lazy(() => import("./DashboardComponents/myValidations/SearchMyValidations"));
const OrganizationEditor = lazy(() => import("./Editor/OrganizationEditor"));
const ProjectEditor = lazy(() => import("./Editor/ProjectEditor"));
const CreateCorpus = lazy(() => import("./Editor/CreateCorpus"));
const CreateService = lazy(() => import("./Editor/CreateService"));
const CreateLd = lazy(() => import("./Editor/CreateLd"));
const CreateLcr = lazy(() => import("./Editor/CreateLcr"));
const EditorRoute = lazy(() => import("./Editor/EditorRoute"));
const EditorEditRoute = lazy(() => import("./Editor/EditorEditRoute"));
//const MyItemRoute = lazy(() => import("./DashboardComponents/MyItemsRoute"));
const ResourcesHome = lazy(() => import("./DashboardComponents/ResourcesHome"));
const MyValidationsRoute = lazy(() => import("./DashboardComponents/myValidations/MyValidationsRoute"));
const ResourcesRoute = lazy(() => import("./DashboardComponents/ResourcesRoute"));
const DashboardHome = lazy(() => import("./DashboardComponents/DashboardHome"));
const Feedback = lazy(() => import("./DashboardComponents/Feedback"));
const ErrorUnauthorized = lazy(() => import("./componentsAPI/ErrorUnauthorized"));
const BatchServiceRegistrationTable = lazy(() => import("./DashboardComponents/bulk_actions/BatchServiceRegistrationTable"));
const Matrix = lazy(() => import("./componentsAPI/Matrix"));

class Routes extends React.Component {
  render() {
    return <div className="view-routes">
      <Suspense fallback={<ProgressBar />}>
        <Switch>
          <Route exact path="/" render={(props) => <SearchCatalogue {...props} keycloak={this.props.keycloak} />} />
          {/*<Route exact path="/:id/distribution/:distribution_id/" render={(props) => <DistributionDirectAccess {...props} keycloak={this.props.keycloak} />} />*/}
          <Route exact path="/search/:term" render={(props) => <SearchCatalogue {...props} keycloak={this.props.keycloak} />} />
          {/*<Route exact path="/upload" render={(props) => <UploadWithSteps {...props} keycloak={this.props.keycloak} />} />*/}
          <Route exact path="/upload" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak}><UploadXmlComponent keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/upload/:tab/" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak}><UploadXmlComponent keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/validate-xml" render={(props) => <PublicValidateXml {...props} keycloak={this.props.keycloak} />} />

          <Route exact path="/tool-service/:id/:tab?/" render={(props) => <ServiceTool {...props} keycloak={this.props.keycloak} />} />
          <Route exact path="/project/:id/:tab?/" render={(props) => <ProjectResourceDetail {...props} keycloak={this.props.keycloak} />} />
          <Route exact path="/organization/:id/:tab?/" render={(props) => <Organization {...props} keycloak={this.props.keycloak} />} />
          <Route exact path="/corpus/:id/:tab?/" render={(props) => <Corpus {...props} keycloak={this.props.keycloak} />} />
          <Route exact path="/lcr/:id/:tab?/" render={(props) => <LexicalConceptualResource {...props} keycloak={this.props.keycloak} />} />
          <Route exact path="/ld/:id/:tab?/" render={(props) => <LanguageDescription {...props} keycloak={this.props.keycloak} />} />

          <Route exact path="/createResource" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak}><SelectResource keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/create/Organization" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak} ><OrganizationEditor keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/create/Project" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak} ><ProjectEditor keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/create/Corpus" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak} ><CreateCorpus keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/create/Service" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak} ><CreateService keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/create/language_description" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak} ><CreateLd keycloak={this.props.keycloak} /></EditorRoute>} />
          <Route exact path="/create/lexical_conceptual_resource" render={(props) => <EditorRoute {...props} keycloak={this.props.keycloak} ><CreateLcr keycloak={this.props.keycloak} /></EditorRoute>} />

          {/*<Route exact path="/myItems/:term" render={(props) => <MyItemRoute {...props} keycloak={this.props.keycloak}><SearchMyResources {...props} keycloak={this.props.keycloak} /></MyItemRoute>} />
          <Route exact path="/myItems" render={(props) => <MyItemRoute {...props} keycloak={this.props.keycloak}><SearchMyResources {...props} keycloak={this.props.keycloak} /></MyItemRoute>} />*/}

          <Route exact path="/myItems/:term" render={(props) => <ResourcesRoute {...props} keycloak={this.props.keycloak}><ResourcesHome {...props} keycloak={this.props.keycloak} /></ResourcesRoute>} />
          <Route exact path="/myItems" render={(props) => <ResourcesRoute {...props} keycloak={this.props.keycloak}><ResourcesHome keycloak={this.props.keycloak} /></ResourcesRoute>} />

          <Route exact path="/myUsage/:term" render={(props) => <ConsumerRoute {...props} keycloak={this.props.keycloak}><ResourcesHome {...props} keycloak={this.props.keycloak} /></ConsumerRoute>} />
          <Route exact path="/myUsage" render={(props) => <ConsumerRoute {...props} keycloak={this.props.keycloak}><ResourcesHome keycloak={this.props.keycloak} /></ConsumerRoute>} />
          <Route exact path="/myDownloads/:term" render={(props) => <ConsumerRoute {...props} keycloak={this.props.keycloak}><ResourcesHome {...props} keycloak={this.props.keycloak} /></ConsumerRoute>} />
          <Route exact path="/myDownloads" render={(props) => <ConsumerRoute {...props} keycloak={this.props.keycloak}><ResourcesHome keycloak={this.props.keycloak} /></ConsumerRoute>} />

          <Route exact path="/myValidations/:term" render={(props) => <MyValidationsRoute {...props} keycloak={this.props.keycloak}><ResourcesHome {...props} keycloak={this.props.keycloak} /></MyValidationsRoute>} />
          <Route exact path="/myValidations" render={(props) => <MyValidationsRoute {...props} keycloak={this.props.keycloak}><ResourcesHome {...props} keycloak={this.props.keycloak} /></MyValidationsRoute>} />
          <Route exact path="/batch-validation/batch-validate/" render={(props) => <MyValidationsRoute {...props} keycloak={this.props.keycloak}><BatchServiceRegistrationTable {...props} keycloak={this.props.keycloak} /></MyValidationsRoute>} />


          <Route exact path="/create/Organization/:id/" render={(props) => <EditorEditRoute {...props} keycloak={this.props.keycloak} ><OrganizationEditor {...props} /></EditorEditRoute>} />
          <Route exact path="/create/Project/:id/" render={(props) => <EditorEditRoute {...props} keycloak={this.props.keycloak} ><ProjectEditor {...props} /></EditorEditRoute>} />
          <Route exact path="/create/Corpus/:id/" render={(props) => <EditorEditRoute {...props} keycloak={this.props.keycloak} ><CreateCorpus {...props} /></EditorEditRoute>} />
          <Route exact path="/create/Service/:id/" render={(props) => <EditorEditRoute {...props} keycloak={this.props.keycloak} ><CreateService {...props} /></EditorEditRoute>} />
          <Route exact path="/create/language_description/:id/" render={(props) => <EditorEditRoute {...props} keycloak={this.props.keycloak} ><CreateLd {...props} /></EditorEditRoute>} />
          <Route exact path="/create/lexical_conceptual_resource/:id/" render={(props) => <EditorEditRoute {...props} keycloak={this.props.keycloak} ><CreateLcr {...props} /></EditorEditRoute>} />
          <Route exact path="/mygrid" render={(props) => <DashboardHome keycloak={this.props.keycloak} />} />
          {/*<Route exact path="/dashboard" render={(props) => <DashboardHome keycloak={this.props.keycloak} />} />*/}
          <Route exact path="/feedback" render={(props) => <Feedback keycloak={this.props.keycloak} />} />
          <Route exact path="/dashboard" render={(props) => <Matrix keycloak={this.props.keycloak} />} />
          <Route exact path="/error/unauthorized" render={(props) => <ErrorUnauthorized keycloak={this.props.keycloak} />} />
          <Redirect to="/" />
        </Switch>
      </Suspense>
    </div>
  }
}
export default Routes;
