import React from "react";
//import { redirectToCatalogue, PRODUCTION } from "../config/constants";
import { GoBackToCatalogue, PRODUCTION } from "../config/constants";
export default class ErrorBoundary extends React.Component {
    state = { hasError: false, error: null };
    static getDerivedStateFromError(error) {
        return { hasError: true, error };
    }
    componentDidCatch(error, info) {
        //this.setState({ hasError: true });
        //console.log(error);
    }
    render() {
        if (this.state.hasError) {
            if (this.state?.error?.name === 'ChunkLoadError') {
                return <div className="yup-error-link" style={{ position: "fixed", top: "25%", left: "25%", cursor: "auto" }}>
                    <p>You are browsing an old version of the ELG catalogue</p>
                    Please refresh the page in order to get the latest upgrades and features
                </div>
            } else {
                console.log("ERROR ERROR");
                PRODUCTION ? GoBackToCatalogue(100) : void 0;
            }
            return <div id="error-boundary-component">error</div>
        } else {
            return this.props.children;
        }
    }
}