import React from "react";
import Chip from '@material-ui/core/Chip';

export default class SelectedFacetsTags extends React.Component {
    handleDelete = (facet, index) => {
        this.props.handleUnselectedFacet(index);
        let keyword = this.getFacetFormatToUnselectNonEncoded(facet);
        if (!keyword) {
            keyword = this.getFacetFormatToUnselectDecoded(facet);
        }
        if (!keyword && (facet.indexOf("function__term") >= 0 || facet.indexOf("function_categories__term") >= 0)) {
            keyword = facet;
        } else {
            keyword = decodeURIComponent(keyword);
        }
        this.props.unselectFacet(keyword);
    }

    getFacetFormatToUnselectDecoded = (facet) => {
        const decodedFacetSearchKeyword = decodeURIComponent(this.props.facetSearchKeyword);
        if (!this.props.facetSearchKeyword || !facet || decodedFacetSearchKeyword.indexOf(facet) < 0) {
            return "";
        }
        if (decodedFacetSearchKeyword.indexOf("&") < 0) {
            return this.props.facetSearchKeyword;
        }
        const formatedFacetsArray = decodedFacetSearchKeyword.split("&");
        for (var index = 0; index < formatedFacetsArray.length; index++) {
            const formatedFacet = formatedFacetsArray[index];
            if (formatedFacet.indexOf(facet) > 0) {
                return formatedFacet;
            }
        }
    }

    getFacetFormatToUnselectNonEncoded = (facet) => {
        if (!this.props.facetSearchKeyword || !facet || this.props.facetSearchKeyword.indexOf(facet) < 0) {
            return "";
        }
        if (this.props.facetSearchKeyword.indexOf("&") < 0) {
            return this.props.facetSearchKeyword;
        }
        const formatedFacetsArray = this.props.facetSearchKeyword.split("&");
        for (var index = 0; index < formatedFacetsArray.length; index++) {
            const formatedFacet = formatedFacetsArray[index];
            if (formatedFacet.indexOf(facet) > 0) {
                return formatedFacet;
            }
        }
    }

    render() {
        if (this.props.selectedFacets && this.props.selectedFacets.length === 0) {
            return <div></div>
        }
        return <div className="chips_facet_area">
            {this.props.selectedFacets.map((facet, index) => {
                let facet_to_show = decodeURIComponent(facet);
                if (facet_to_show === "false") {
                    facet_to_show = "language independent"
                } else if (facet_to_show === "true") {
                    facet_to_show = "language dependent"
                } else if (facet_to_show === false) {
                    facet_to_show = "language independent"
                } else if (facet_to_show === true) {
                    facet_to_show = "language dependent"
                } else if (facet_to_show.indexOf("function__term::__::") >= 0) {
                    facet_to_show = facet_to_show.replace("function__term::__::", "").replace("function_categories__term::__::", "").replace("false", "language independent").replace("true", "language dependent").replace(1, "True").replace(0, "False");
                } else if (facet_to_show.indexOf("function_categories__term::__::") >= 0) {
                    facet_to_show = facet_to_show.replace("function_categories__term::__::", "").replace("function__term::__::", "").replace("false", "language independent").replace("true", "language dependent").replace(1, "True").replace(0, "False");
                }
                facet = (facet !== false && facet !== true) && facet.replace("function__term::__::", "function__term=");
                facet = (facet !== false && facet !== true) && facet.replace("function_categories__term::__::", "function_categories__term=");
                return <Chip
                    key={index}
                    color="primary"
                    //color="secondary"
                    //variant="outlined"
                    className="mr-05"
                    size="small"
                    label={facet_to_show.replace('Collection', '')}
                    onDelete={() => { this.handleDelete(facet, index) }}//material-ui bug, doesn't work so we are using the onClick event
                    onClick={() => this.handleDelete(facet, index)}
                />
            })}
        </div>
    }
}