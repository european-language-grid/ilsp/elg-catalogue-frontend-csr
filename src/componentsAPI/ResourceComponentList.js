import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withRouter, Link } from "react-router-dom";
import Chip from '@material-ui/core/Chip';
import Paper from '@material-ui/core/Paper';
import ResourceEntityTypeIconOnly from './CommonComponents/ResourceEntityTypeIconOnly';
import Analytics from "./CommonComponents/Analytics";
import ResourceStatus from "./CommonComponents/ResourceStatus";
import ResourceUnderConstruction from "./CommonComponents/ResourceUnderConstruction";
import ResourceForInfo from "./CommonComponents/ResourceForInfo";
import HostedAtElg from "./CommonComponents/HostedAtElg";
import { ELG_CATALOGUE_PREV_PAGE, getUrlToLandingPageFromCatalogue } from "../config/constants";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

class ResourceComponentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { expanded: false, expandedLang: false, expandedLicence: false, keycloak: props.keycloak };
    }

    togglexpanded = () => {
        this.setState({ expanded: !this.state.expanded })
    }

    togglexpandedLang = () => {
        this.setState({ expandedLang: !this.state.expandedLang })
    }

    togglexpandedLicence = () => {
        this.setState({ expandedLicence: !this.state.expandedLicence })
    }

    render() {
        const { resource } = this.props;
        const { expanded, expandedLang, expandedLicence } = this.state;
        const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
        //const cataloguePageUrl = window.location.href;
        const cataloguePageUrl = window.location.pathname + window.location.search
        return (
            <React.Fragment>
                <Paper className="ResourceListItem">
                    <Grid item xs={12} container spacing={3} className="ResourceListItem--inner">
                        <Grid item xs={12} sm={9}>
                            <Grid item container xs={12} direction="row" justifyContent="space-between" alignItems="baseline" className="pb-05">
                                <Grid item xs={11} style={{ display: "flex", justify: "flex-start", alignItems: "center" }}>
                                    <div>
                                        <span className="pr-05"><ResourceEntityTypeIconOnly resource_type={resource.resource_type} entity_type={resource.entity_type} /></span>
                                    </div>
                                    <div>
                                        {(resource.is_division_of && resource.is_division_of.length > 0) ? <span style={{ display: "flex" }}>
                                            <span>
                                                <Typography variant="h4">{resource.is_division_of},</Typography>
                                            </span>
                                            <span style={{ paddingLeft: "2px" }}>
                                                <Typography variant="h4" className="ResourceListTitle" onClick={() => { sessionStorage.setItem(ELG_CATALOGUE_PREV_PAGE, cataloguePageUrl); }}>
                                                    <Link to={
                                                        {
                                                            pathname: url2LandingPage,
                                                            "detail": resource.detail
                                                        }
                                                    }>
                                                        {resource.resource_name} 
                                                    </Link>
                                                </Typography>
                                            </span>
                                        </span>
                                            : <span>
                                                <Typography variant="h4" className="ResourceListTitle" onClick={() => { sessionStorage.setItem(ELG_CATALOGUE_PREV_PAGE, cataloguePageUrl); }}>
                                                    <Link to={
                                                        {
                                                            pathname: url2LandingPage,
                                                            "detail": resource.detail
                                                        }
                                                    }>
                                                        <span>{resource.resource_name} </span> <span>{resource.duplicate && resource.duplicate.length > 0 && <Chip size="small" label={"duplicate"} className="ChipTagPink" />}</span>
                                                        <span>{resource.potential_duplicate && resource.potential_duplicate.length > 0 && <Chip size="small" label={"Potential duplicate"} className="ChipTagPink" />}</span>
                                                    </Link>
                                                </Typography>
                                            </span>

                                        }
                                        {resource.resource_type === null && resource.resource_short_name && resource.resource_short_name.length > 0 && resource.resource_short_name.map((short_name, index) => <span key={index} className="bold-p--id">{short_name}</span>)}
                                        {resource.version !== "undefined" && resource.version !== null && <span><Typography variant="body2" className="grey--font">version: {resource.version}</Typography></span>}
                                    </div>
                                </Grid>
                                <Grid item xs={1}>
                                    <ResourceStatus status={resource.status} xSize={12} keycloak={this.state.keycloak} />
                                </Grid>
                            </Grid>

                            <div>
                                {expanded ? (
                                    <div>
                                        <Typography variant="body2" className="search-results__description">

                                            {this.props.resource.description} <span className="ExpandButton" onClick={this.togglexpanded} > <ExpandLessIcon className="grey--font" /> </span>
                                        </Typography>
                                    </div>
                                ) : (
                                    <div>
                                        <Typography variant="body2" className="search-results__description">
                                            {this.props.resource.description.substring(0, 220)}{this.props.resource.description.length > 220 ? <span className="ExpandButton" onClick={this.togglexpanded} > <ExpandMoreIcon className="grey--font" /> </span> : void 0} </Typography>
                                    </div>
                                )
                                }
                            </div>
                            <div>
                                {resource.keywords && resource.keywords.length > 0 ?
                                    (<div className="pb-03 flex-tags">
                                        {resource.keywords.length > 1 ? <span className="bold-p--id">Keywords: </span> : <span className="bold-p--id">Keyword: </span>}

                                        {resource.keywords.slice(0, 4).map((keyword, index) =>
                                            <Chip key={index} size="small" label={keyword} className="ChipTagLicence" />
                                        )}</div>)
                                    : void 0}
                            </div>
                            <div>
                                {resource.languages && resource.languages.length > 0 ?
                                    (<div className="pb-03 flex-tags">
                                        {resource.languages.length > 1 ? <span className="bold-p--id">Languages: </span> : <span className="bold-p--id">Language: </span>}
                                        {expandedLang ? (
                                            <div>
                                                {resource.languages.map((lang, index) => <Chip key={index} size="small" label={lang} className="ChipTagLicence" />)}
                                                <span className="ExpandButton grey--font" onClick={this.togglexpandedLang} > <ExpandLessIcon className="grey--font" /> </span>
                                            </div>
                                        ) : (
                                            <div>
                                                {resource.languages.slice(0, 5).map((lang, index) => <Chip key={index} size="small" label={lang} className="ChipTagLicence" />)}
                                                {resource.languages.length > 5 ?
                                                    <span className="ExpandButton grey--font" onClick={this.togglexpandedLang} > <ExpandMoreIcon className="grey--font" /> </span>
                                                    : void 0}
                                            </div>
                                        )
                                        }

                                    </div>)
                                    : void 0
                                }
                            </div>
                            <div>
                                {resource.licences && resource.licences.length > 0 ?
                                    (<div style={{ display: "flex", alignItems: "baseline" }}>
                                        {resource.licences.length > 1 ? <span className="bold-p--id">Licences:</span> : <span className="bold-p--id">Licence:</span>}
                                        {expandedLicence ? (
                                            <div>
                                                {resource.licences.map((license, index) => <Chip key={index} size="small" label={license} className="ChipTagLicence" />)}
                                                <span className="ExpandButton grey--font" onClick={this.togglexpandedLicence} > <ExpandLessIcon className="grey--font" /> </span>
                                            </div>
                                        ) : (
                                            <div>
                                                {resource.licences.slice(0, 2).map((license, index) => <Chip key={index} size="small" label={license} className="ChipTagLicence" />)}
                                                {resource.licences.length > 2 ?
                                                    <span className="ExpandButton grey--font" onClick={this.togglexpandedLicence} > <ExpandMoreIcon className="grey--font" /> </span>
                                                    : void 0}
                                            </div>
                                        )
                                        }

                                    </div>) : void 0
                                }
                                
                                {resource.licences && resource.licences.length === 0 ? (<div style={{ display: "flex", alignItems: "baseline" }}>
                                    {resource.access_rights && resource.access_rights.length > 0 && <span className="bold-p--id">Access rights :<span>&nbsp;</span></span>}
                                    {resource.access_rights && resource.access_rights.length > 0 && resource.access_rights.map((right, index) => <Chip key={index} size="small" label={right} className="ChipTagLicence" />)}
                                </div>) : void 0
                                }

                                {(resource.other_versions_count && resource.other_versions_count !== 0 && resource.harvested_versions_exceeding_limit === false) ? <div>
                                    {resource.other_versions_count > 1 && resource.harvested_versions_exceeding_limit === false ? <span className="date_value grey--font italic">{resource.other_versions_count} more versions exist for this record</span> : <span className="date_value grey--font italic">{resource.other_versions_count} more version exists for this record</span>}
                                </div> : void 0}

                                {(resource.other_versions_count && resource.other_versions_count !== 0 && resource.harvested_versions_exceeding_limit === true) ? <div>
                                    {resource.other_versions_count > 1 && resource.harvested_versions_exceeding_limit === true ? <span className="date_value grey--font italic">{resource.other_versions_count} more versions exist for this record (and more versions at source)</span> : <span className="date_value grey--font italic">{resource.other_versions_count} more version exists for this record (and more versions at source)</span>}
                                </div> : void 0}

                                {(resource.potential_duplicate && resource.potential_duplicate.length > 0 ) ? <div style={{ display: "flex", alignItems: "baseline" }}>
                                <span className="date_value grey--font italic"> {resource.potential_duplicate.length > 1 ? "Potential duplicate with records:" :  "Potential duplicate with record:"} <span>&nbsp;</span></span>
                                {resource.potential_duplicate.map((item, index) => <span> <a href={item.url} target="_blank" rel="noopener noreferrer" className="date_value"> {item.resource_name} </a> <span className="date_value">({item.source}) </span> <span>&nbsp;</span> </span>)}
                                </div> : void 0}

                                {(resource.duplicate && resource.duplicate.length > 0 ) ? <div style={{ display: "flex", alignItems: "baseline" }}>
                                <span className="date_value grey--font italic"> Alternative sources : <span>&nbsp;</span></span>
                                {resource.duplicate.map((item, index) => <span> <a href={item.url} target="_blank" rel="noopener noreferrer" className="date_value"> {item.resource_name} </a> <span className="date_value">({item.source}) </span> <span>&nbsp;</span> </span>)}
                                </div> : void 0}

                            </div>

                        </Grid>


                        <Grid item xs={12} sm={3} >
                            <Grid item>
                                {/*<Typography variant="caption" className="ResourceListCaption">{resource.resource_type}</Typography>*/}
                            </Grid>
                            <Grid item container spacing={1} direction="row" justifyContent="flex-start" alignItems="baseline" >
                                {/*<Grid item xs={12} >
                                    <ResourceDownloadable size={resource.size} xSize={12} position={"right ribbon label"}/>
                            </Grid>*/}

                                {/*<Grid item xs={12}>
                                    <ResourceEntityType resource_type={resource.resource_type} entity_type={resource.entity_type} align={"right"} />
                        </Grid>*/}
                                <Grid item xs={12}>
                                    <Analytics views={resource.views} under_construction={resource.under_construction} downloads={resource.downloads} resource_type={resource.resource_type} entity_type={resource.entity_type} service_execution_count={resource.service_execution_count} xSize={6} />
                                    <ResourceUnderConstruction under_construction={resource.under_construction} xSize={12} justifyContent={"center"} alignItems={"flex-end"} />
                                    <ResourceForInfo for_information_only={resource.for_information_only} xSize={12} justifyContent={"center"} alignItems={"flex-end"} />
                                    {resource.resource_type && <HostedAtElg proxied={resource.proxied} snippet={true} functional_service={resource.elg_compatible_service ? resource.elg_compatible_service : false} has_data={resource.has_data} resource_type={resource.resource_type} justifyContent={"center"} alignItems={"flex-end"} xSize={12} />}
                                </Grid>

                            </Grid>
                        </Grid>

                    </Grid>

                </Paper>
            </React.Fragment>
        );

    }
}
export default withRouter(ResourceComponentList);
