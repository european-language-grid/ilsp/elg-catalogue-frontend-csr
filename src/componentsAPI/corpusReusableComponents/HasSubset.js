//delete if not needed
/*import React from "react";
import Typography from '@material-ui/core/Typography';


export default class HasSubset extends React.Component {
    componentDidMount() {
        const { data } = this.props;
        if (!data) {
            return false;
        }
        const { has_subset = null } = data.described_entity.field_value.lr_subclass ? data.described_entity.field_value.lr_subclass.field_value : [];
        if (!has_subset) {
            return false;
        }
        const { displayTabGeneral } = this.props;
        if (has_subset && has_subset.length > 0) {
            displayTabGeneral ? void 0 : this.props.displayTabGeneralFunction(true);
        }
    }
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const { has_subset } = data.described_entity.field_value.lr_subclass.field_value || [];
        if (!has_subset) {
            return <div></div>
        }
        return <div>
            {has_subset && has_subset.field_value.length > 0 && <h3>{has_subset.field_label[metadataLanguage] || has_subset.field_label["en"]}</h3>}
            {has_subset.field_value.map((subsetItem, subsetIndex) => {
                let size_per_language_label = subsetItem.size_per_language.field_label[metadataLanguage] || subsetItem.size_per_language.field_label["en"];
                let size_per_text_format_label = subsetItem.size_per_text_format.field_label[metadataLanguage] || subsetItem.size_per_text_format.field_label["en"];
                let size_per_languageArray = subsetItem.size_per_language ? subsetItem.size_per_language.field_value.map((item, index) => {
                    let amount = item.amount.field_value || "";
                    let size_unit = item.size_unit.label[metadataLanguage] || item.size_unit.label["en"];
                    return <div key={subsetIndex}>
                        {amount && <div><Typography className="bold-p--id">{size_per_language_label}</Typography><span className="info_value">{amount} {size_unit}</span></div>}
                    </div>
                }) : [];
                let size_per_text_formatArray = subsetItem.size_per_text_format ? subsetItem.size_per_text_format.field_value.map((item, index) => {
                    let amount = item.amount.field_value || "";
                    let size_unit = item.size_unit.label[metadataLanguage] || item.size_unit.label["en"];
                    return <div key={subsetIndex}>
                        {amount && <div><Typography className="bold-p--id">{size_per_text_format_label}</Typography><span className="info_value">{amount} {size_unit}</span></div>}
                    </div>
                }) : [];
                return <div key={subsetIndex}>
                    {size_per_languageArray.length > 0 &&
                        <div>
                            <span className="info_value">{size_per_languageArray.map((item, index) =>
                                <span key={index}>{item} </span>)}</span></div>}

                    {size_per_text_formatArray.length > 0 &&
                        <div>
                            <span className="info_value">{size_per_text_formatArray.map((item, index) =>
                                <span key={index}>{item} </span>)}</span></div>}
                    <hr />
                </div>
            })}
        </div>
    }
}*/