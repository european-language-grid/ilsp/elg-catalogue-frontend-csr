import React from "react";
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
import ResourceActor from "../CommonComponents/ResourceActor";
import GeneralIdentifier from "../CommonComponents/GeneralIdentifier";
import MediaPartIconType from "../CommonComponents/MediaPartIconType";
import LcrDynamicElement from '../lcrReusableComponents/LcrDynamicElement';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import Chip from '@material-ui/core/Chip';
import DocumentIdentifier from "../CommonComponents/DocumentIdentifier";
import Languages from "../CommonComponents/Languages";


/*function a11yProps(index) {
    return {
        id: `wrapped-tab-${index}`,
        'aria-controls': `wrapped-tabpanel-${index}`,
    };
}*/

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export default class CorpusMediaPart extends React.Component {

    constructor(props) {
        super(props);
        this.state = { tab: 0 };
        this.toggleTab = this.toggleTab.bind(this);
    }

    toggleTab(tabIndex) {
        this.setState({ tab: tabIndex });
    }


    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const { corpus_media_part } = data.described_entity.field_value.lr_subclass.field_value || [];
        if (!corpus_media_part) {
            return <div></div>
        }
        //console.log(data)
        const { inferred_language = false } = data.management_object || {};
        return <>
            <Typography variant="h3" className="section-links"> {corpus_media_part.field_label[metadataLanguage] || corpus_media_part.field_label["en"]}</Typography>
            <div className="tabs-main-container">
                <div className="vertical-tabs-container">
                    <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs">
                        {corpus_media_part && corpus_media_part.field_value.map((mediaPart, mediaPartIndex) => {
                            //let corpus_media_type = mediaPart.corpus_media_type ? mediaPart.corpus_media_type.field_value : "";
                            let media_type = mediaPart.media_type ? (mediaPart.media_type.label[metadataLanguage] || mediaPart.media_type.label[Object.keys(mediaPart.media_type.label)[0]]) : "";
                            //let media_type_label = mediaPart.media_type ? (mediaPart.media_type.field_label[metadataLanguage] || mediaPart.media_type.field_label["en"]) : "";

                            return <Tab key={media_type + mediaPartIndex} label={<div><span className=""><MediaPartIconType media_type={media_type} /></span><span> {media_type}</span></div>} {...a11yProps(mediaPartIndex)} onClick={() => { this.toggleTab(mediaPartIndex); }} />
                        })}
                    </Tabs>
                    {corpus_media_part.field_value.map((mediaPart, mediaPartIndex) => {
                        //let corpus_media_type = mediaPart.corpus_media_type ? mediaPart.corpus_media_type.field_value : "";
                        //let media_type = mediaPart.media_type ? (mediaPart.media_type.label[metadataLanguage] || mediaPart.media_type.label[Object.keys(mediaPart.media_type.label)[0]]) : "";
                        //let media_type_label = mediaPart.media_type ? (mediaPart.media_type.field_label[metadataLanguage] || mediaPart.media_type.field_label["en"]) : "";
                        let linguality_type = mediaPart.linguality_type ? (mediaPart.linguality_type.label[metadataLanguage] || mediaPart.linguality_type.label[Object.keys(mediaPart.linguality_type.label)[0]]) : "";
                        let linguality_type_label = mediaPart.linguality_type ? (mediaPart.linguality_type.field_label[metadataLanguage] || mediaPart.linguality_type.field_label["en"]) : "";
                        let multilinguality_type = mediaPart.multilinguality_type ? (mediaPart.multilinguality_type.label[metadataLanguage] || mediaPart.multilinguality_type.label[Object.keys(mediaPart.multilinguality_type.label)[0]]) : "";
                        let multilinguality_type_label = mediaPart.multilinguality_type ? (mediaPart.multilinguality_type.field_label[metadataLanguage] || mediaPart.multilinguality_type.field_label["en"]) : "";

                        let multilinguality_type_details = (mediaPart.multilinguality_type_details && (mediaPart.multilinguality_type_details.field_value[metadataLanguage] || mediaPart.multilinguality_type_details.field_value[Object.keys(mediaPart.multilinguality_type_details.field_value)[0]])) || "";
                        let multilinguality_type_details_label = mediaPart.multilinguality_type_details ? (mediaPart.multilinguality_type_details.field_label[metadataLanguage] || mediaPart.multilinguality_type_details.field_label["en"]) : "";

                         let modality_type_label = mediaPart.modality_type ? (mediaPart.modality_type.field_label[metadataLanguage] || mediaPart.modality_type.field_label["en"]) : "";
                        let modality_typeArray = (mediaPart.modality_type && mediaPart.modality_type.field_value.length > 0 && mediaPart.modality_type.field_value.map(modality => modality.label[metadataLanguage] || modality.label[Object.keys(modality.label)[0]])) || [];
                        let audio_genre = [];
                        let audio_genre_label = mediaPart.audio_genre ? (mediaPart.audio_genre.field_label[metadataLanguage] || mediaPart.audio_genre.field_label["en"]) : "";
                        (mediaPart.audio_genre && mediaPart.audio_genre.field_value.length > 0 && mediaPart.audio_genre.field_value.map((genre, genreIndex) => {
                            let category_label = (genre.category_label && (genre.category_label.field_value[metadataLanguage] || genre.category_label.field_value[Object.keys(genre.category_label.field_value)[0]])) || "";
                            audio_genre.push(category_label);
                            return void 0;
                        }));
                        let speech_genre = [];
                        let speech_genre_label = mediaPart.speech_genre ? (mediaPart.speech_genre.field_label[metadataLanguage] || mediaPart.speech_genre.field_label["en"]) : "";
                        (mediaPart.speech_genre && mediaPart.speech_genre.field_value.length > 0 && mediaPart.speech_genre.field_value.map((speech, speechIndex) => {
                            let category_label = (speech.category_label && (speech.category_label.field_value[metadataLanguage] || speech.category_label.field_value[Object.keys(speech.category_label.field_value)[0]])) || "";
                            speech_genre.push(category_label);
                            return void 0;
                        }));
                        let video_genre = [];
                        let video_genre_label = mediaPart.video_genre ? (mediaPart.video_genre.field_label[metadataLanguage] || mediaPart.video_genre.field_label["en"]) : "";
                        (mediaPart.video_genre && mediaPart.video_genre.field_value.length > 0 && mediaPart.video_genre.field_value.map((video, videoIndex) => {
                            let category_label = (video.category_label && (video.category_label.field_value[metadataLanguage] || video.category_label.field_value[Object.keys(video.category_label.field_value)[0]])) || "";
                            video_genre.push(category_label);
                            return void 0;
                        }));
                        let type_of_image_content = [];
                        (mediaPart.type_of_image_content && mediaPart.type_of_image_content.field_value.length > 0 && mediaPart.type_of_image_content.field_value.map((keyword, DIndex) => {
                            type_of_image_content.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                        }));
                        let type_of_image_content_label = mediaPart.type_of_image_content ? (mediaPart.type_of_image_content.field_label[metadataLanguage] || mediaPart.type_of_image_content.field_label["en"]) : "";
                        //let type_of_video_content = (mediaPart.type_of_video_content && (mediaPart.type_of_video_content.field_value[metadataLanguage] || mediaPart.type_of_video_content.field_value[Object.keys(mediaPart.type_of_video_content.field_value)[0]])) || "";
                        let type_of_video_content = [];
                        (mediaPart.type_of_video_content && mediaPart.type_of_video_content.field_value.length > 0 && mediaPart.type_of_video_content.field_value.map((keyword, DIndex) => {
                            type_of_video_content.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                        }));
                        let type_of_video_content_label = mediaPart.type_of_video_content ? (mediaPart.type_of_video_content.field_label[metadataLanguage] || mediaPart.type_of_video_content.field_label["en"]) : "";
                        let type_of_text_numerical_content = [];
                        (mediaPart.type_of_text_numerical_content && mediaPart.type_of_text_numerical_content.field_value.length > 0 && mediaPart.type_of_text_numerical_content.field_value.map((keyword, DIndex) => {
                            type_of_text_numerical_content.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                        }));
                        let type_of_text_numerical_content_label = mediaPart.type_of_text_numerical_content ? (mediaPart.type_of_text_numerical_content.field_label[metadataLanguage] || mediaPart.type_of_text_numerical_content.field_label["en"]) : "";

                        let speech_item = (mediaPart.speech_item && mediaPart.speech_item.field_value.length > 0 && mediaPart.speech_item.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)][0])) || [];
                        let speech_item_label = mediaPart.speech_item ? (mediaPart.speech_item.field_label[metadataLanguage] || mediaPart.speech_item.field_label["en"]) : "";
                        let non_speech_item = (mediaPart.non_speech_item && mediaPart.non_speech_item.field_value.length > 0 && mediaPart.non_speech_item.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)][0])) || [];
                        let non_speech_item_label = mediaPart.non_speech_item ? (mediaPart.non_speech_item.field_label[metadataLanguage] || mediaPart.non_speech_item.field_label["en"]) : "";
                        let legend = mediaPart.legend ? (mediaPart.legend.field_value[metadataLanguage] || mediaPart.legend.field_value[Object.keys(mediaPart.legend.field_value)[0]]) : "";
                        let legend_label = mediaPart.legend ? (mediaPart.legend.field_label[metadataLanguage] || mediaPart.legend.field_label["en"]) : "";
                        let noise_level = mediaPart.noise_level ? (mediaPart.noise_level.label[metadataLanguage] || mediaPart.noise_level.label[Object.keys(mediaPart.noise_level.label)[0]]) : "";
                        let noise_level_label = mediaPart.noise_level ? (mediaPart.noise_level.field_label[metadataLanguage] || mediaPart.noise_level.field_label["en"]) : "";
                        let naturality = mediaPart.naturality ? (mediaPart.naturality.label[metadataLanguage] || mediaPart.naturality.label[Object.keys(mediaPart.naturality.label)[0]]) : "";
                        let naturality_label = mediaPart.naturality ? (mediaPart.naturality.field_label[metadataLanguage] || mediaPart.naturality.field_label["en"]) : "";
                        let conversational_type = (mediaPart.conversational_type && mediaPart.conversational_type.field_value.length > 0 && mediaPart.conversational_type.field_value.map(item => (item.label[metadataLanguage] || item.label[Object.keys(item.label)][0]))) || [];
                        let conversational_type_label = mediaPart.conversational_type ? (mediaPart.conversational_type.field_label[metadataLanguage] || mediaPart.conversational_type.field_label["en"]) : "";
                        let scenario_type = (mediaPart.scenario_type && mediaPart.scenario_type.field_value.length > 0 && mediaPart.scenario_type.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)][0])) || [];
                        let scenario_type_label = mediaPart.scenario_type ? (mediaPart.scenario_type.field_label[metadataLanguage] || mediaPart.scenario_type.field_label["en"]) : "";
                        let audience = mediaPart.audience ? (mediaPart.audience.label[metadataLanguage] || mediaPart.audience.label[Object.keys(mediaPart.audience.label)[0]]) : "";
                        let audience_label = mediaPart.audience ? (mediaPart.audience.field_label[metadataLanguage] || mediaPart.audience.field_label["en"]) : "";
                        let interactivity = mediaPart.interactivity ? (mediaPart.interactivity.label[metadataLanguage] || mediaPart.interactivity.label[Object.keys(mediaPart.interactivity.label)[0]]) : "";
                        let interactivity_label = mediaPart.interactivity ? (mediaPart.interactivity.field_label[metadataLanguage] || mediaPart.interactivity.field_label["en"]) : "";
                        let interaction = mediaPart.interaction ? (mediaPart.interaction.field_value[metadataLanguage] || mediaPart.interaction.field_value[Object.keys(mediaPart.interaction.field_value)[0]]) : "";
                        let interaction_label = mediaPart.interaction ? (mediaPart.interaction.field_label[metadataLanguage] || mediaPart.interaction.field_label["en"]) : "";
                        let recording_device_type = mediaPart.recording_device_type ? (mediaPart.recording_device_type.label[metadataLanguage] || mediaPart.recording_device_type.label[Object.keys(mediaPart.recording_device_type.label)[0]]) : "";
                        let recording_device_type_label = mediaPart.recording_device_type ? (mediaPart.recording_device_type.field_label[metadataLanguage] || mediaPart.recording_device_type.field_label["en"]) : "";
                        let recording_device_type_details = mediaPart.recording_device_type_details ? (mediaPart.recording_device_type_details.field_value[metadataLanguage] || mediaPart.recording_device_type_details.field_value[Object.keys(mediaPart.recording_device_type_details.field_value)[0]]) : "";
                        let recording_device_type_details_label = mediaPart.recording_device_type_details ? (mediaPart.recording_device_type_details.field_label[metadataLanguage] || mediaPart.recording_device_type_details.field_label["en"]) : "";
                        let recording_platform_software = mediaPart.recording_platform_software ? mediaPart.recording_platform_software.field_value : "";
                        let recording_platform_software_label = mediaPart.recording_platform_software ? (mediaPart.recording_platform_software.field_label[metadataLanguage] || mediaPart.recording_platform_software.field_label["en"]) : "";
                        let recording_environment = mediaPart.recording_environment ? (mediaPart.recording_environment.label[metadataLanguage] || mediaPart.recording_environment.label[Object.keys(mediaPart.recording_environment.label)[0]]) : "";
                        let recording_environment_label = mediaPart.recording_environment ? (mediaPart.recording_environment.field_label[metadataLanguage] || mediaPart.recording_environment.field_label["en"]) : "";
                        let source_channel = mediaPart.source_channel ? (mediaPart.source_channel.label[metadataLanguage] || mediaPart.source_channel.label[Object.keys(mediaPart.source_channel.label)[0]]) : "";
                        let source_channel_label = mediaPart.source_channel ? (mediaPart.source_channel.field_label[metadataLanguage] || mediaPart.source_channel.field_label["en"]) : "";
                        let source_channel_type = mediaPart.source_channel_type ? (mediaPart.source_channel_type.label[metadataLanguage] || mediaPart.source_channel_type.label[Object.keys(mediaPart.source_channel_type.label)[0]]) : "";
                        let source_channel_type_label = mediaPart.source_channel_type ? (mediaPart.source_channel_type.field_label[metadataLanguage] || mediaPart.source_channel_type.field_label["en"]) : "";
                        let source_channel_name = mediaPart.source_channel_name ? (mediaPart.source_channel_name.field_value[metadataLanguage] || mediaPart.source_channel_name.field_value[Object.keys(mediaPart.source_channel_name.field_value)[0]]) : "";
                        let source_channel_name_label = mediaPart.source_channel_name ? (mediaPart.source_channel_name.field_label[metadataLanguage] || mediaPart.source_channel_name.field_label["en"]) : "";
                        let source_channel_details = mediaPart.source_channel_details ? (mediaPart.source_channel_details.field_value[metadataLanguage] || mediaPart.source_channel_details.field_value[Object.keys(mediaPart.source_channel_details.field_value)[0]]) : "";
                        let source_channel_details_label = mediaPart.source_channel_details ? (mediaPart.source_channel_details.field_label[metadataLanguage] || mediaPart.source_channel_details.field_label["en"]) : "";
                        //let recorder_label = mediaPart.recorder ? (mediaPart.recorder.field_label[metadataLanguage] || mediaPart.recorder.field_label["en"]) : "";
                        let recorderArray = mediaPart.recorder ? mediaPart.recorder.field_value : [];
                        let capturing_device_type = mediaPart.capturing_device_type ? (mediaPart.capturing_device_type.label[metadataLanguage] || mediaPart.capturing_device_type.label[Object.keys(mediaPart.capturing_device_type.label)[0]]) : "";
                        let capturing_device_type_label = mediaPart.capturing_device_type ? (mediaPart.capturing_device_type.field_label[metadataLanguage] || mediaPart.capturing_device_type.field_label["en"]) : "";
                        let capturing_device_type_details = mediaPart.capturing_device_type_details ? (mediaPart.capturing_device_type_details.field_value[metadataLanguage] || mediaPart.capturing_device_type_details.field_value[Object.keys(mediaPart.capturing_device_type_details.field_value)[0]]) : "";
                        let capturing_device_type_details_label = mediaPart.capturing_device_type_details ? (mediaPart.capturing_device_type_details.field_label[metadataLanguage] || mediaPart.capturing_device_type_details.field_label["en"]) : "";
                        let capturing_details = mediaPart.capturing_details ? (mediaPart.capturing_details.field_value[metadataLanguage] || mediaPart.capturing_details.field_value[Object.keys(mediaPart.capturing_details.field_value)[0]]) : "";
                        let capturing_details_label = mediaPart.capturing_details ? (mediaPart.capturing_details.field_label[metadataLanguage] || mediaPart.capturing_details.field_label["en"]) : "";
                        let capturing_environment = mediaPart.capturing_environment ? (mediaPart.capturing_environment.label[metadataLanguage] || mediaPart.capturing_environment.label[Object.keys(mediaPart.capturing_environment.label)[0]]) : "";
                        let capturing_environment_label = mediaPart.capturing_environment ? (mediaPart.capturing_environment.field_label[metadataLanguage] || mediaPart.capturing_environment.field_label["en"]) : "";
                        let sensor_technology = mediaPart.sensor_technology ? mediaPart.sensor_technology.field_value : "";
                        let sensor_technology_label = mediaPart.sensor_technology ? (mediaPart.sensor_technology.field_label[metadataLanguage] || mediaPart.sensor_technology.field_label["en"]) : "";
                        let scene_illumination = mediaPart.scene_illumination ? (mediaPart.scene_illumination.label[metadataLanguage] || mediaPart.scene_illumination.label[Object.keys(mediaPart.scene_illumination.label)[0]]) : "";
                        let scene_illumination_label = mediaPart.scene_illumination ? (mediaPart.scene_illumination.field_label[metadataLanguage] || mediaPart.scene_illumination.field_label["en"]) : "";
                        let number_of_participants = mediaPart.number_of_participants ? mediaPart.number_of_participants.field_value : null;
                        let number_of_participants_label = mediaPart.number_of_participants ? (mediaPart.number_of_participants.field_label[metadataLanguage] || mediaPart.number_of_participants.field_label["en"]) : "";
                        let age_group_of_participants = mediaPart.age_group_of_participants ? (mediaPart.age_group_of_participants.label[metadataLanguage] || mediaPart.age_group_of_participants.label[Object.keys(mediaPart.age_group_of_participants.label)[0]]) : "";
                        let age_group_of_participants_label = mediaPart.age_group_of_participants ? (mediaPart.age_group_of_participants.field_label[metadataLanguage] || mediaPart.age_group_of_participants.field_label["en"]) : "";
                        let age_range_start_of_participants = mediaPart.age_range_start_of_participants ? mediaPart.age_range_start_of_participants.field_value : null;
                        let age_range_start_of_participants_label = mediaPart.age_range_start_of_participants ? (mediaPart.age_range_start_of_participants.field_label[metadataLanguage] || mediaPart.age_range_start_of_participants.field_label["en"]) : "";
                        let age_range_end_of_participants = mediaPart.age_range_end_of_participants ? mediaPart.age_range_end_of_participants.field_value : null;
                        let age_range_end_of_participants_label = mediaPart.age_range_end_of_participants ? (mediaPart.age_range_end_of_participants.field_label[metadataLanguage] || mediaPart.age_range_end_of_participants.field_label["en"]) : "";
                        let sex_of_participants = mediaPart.sex_of_participants ? (mediaPart.sex_of_participants.label[metadataLanguage] || mediaPart.sex_of_participants.label[Object.keys(mediaPart.sex_of_participants.label)[0]]) : "";
                        let sex_of_participants_label = mediaPart.sex_of_participants ? (mediaPart.sex_of_participants.field_label[metadataLanguage] || mediaPart.sex_of_participants.field_label["en"]) : "";
                        let origin_of_participants = mediaPart.origin_of_participants ? (mediaPart.origin_of_participants.label[metadataLanguage] || mediaPart.origin_of_participants.label[Object.keys(mediaPart.origin_of_participants.label)[0]]) : "";
                        let origin_of_participants_label = mediaPart.origin_of_participants ? (mediaPart.origin_of_participants.field_label[metadataLanguage] || mediaPart.origin_of_participants.field_label["en"]) : "";
                        //let dialect_accent_of_participants = mediaPart.dialect_accent_of_participants ? mediaPart.dialect_accent_of_participants.field_value[metadataLanguage] || mediaPart.dialect_accent_of_participants.field_value[Object.keys(mediaPart.dialect_accent_of_participants.field_value)[0]] : "";
                        let dialect_accent_of_participants = [];
                        (mediaPart.dialect_accent_of_participants && mediaPart.dialect_accent_of_participants.field_value.length > 0 && mediaPart.dialect_accent_of_participants.field_value.map((keyword, DIndex) => {
                            dialect_accent_of_participants.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                        }));
                        let dialect_accent_of_participants_label = mediaPart.dialect_accent_of_participants ? (mediaPart.dialect_accent_of_participants.field_label[metadataLanguage] || mediaPart.dialect_accent_of_participants.field_label["en"]) : "";
                        //let geographic_distribution_of_participants = mediaPart.geographic_distribution_of_participants ? mediaPart.geographic_distribution_of_participants.field_value[metadataLanguage] || mediaPart.geographic_distribution_of_participants.field_value[Object.keys(mediaPart.geographic_distribution_of_participants.field_value)[0]] : "";
                        let geographic_distribution_of_participants_label = mediaPart.geographic_distribution_of_participants ? (mediaPart.geographic_distribution_of_participants.field_label[metadataLanguage] || mediaPart.geographic_distribution_of_participants.field_label["en"]) : "";
                        let geographic_distribution_of_participants = [];
                        (mediaPart.geographic_distribution_of_participants && mediaPart.geographic_distribution_of_participants.field_value.length > 0 && mediaPart.geographic_distribution_of_participants.field_value.map((keyword, DIndex) => {
                            geographic_distribution_of_participants.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                        }));
                        let hearing_impairment_of_participants = mediaPart.hearing_impairment_of_participants ? (mediaPart.hearing_impairment_of_participants.label[metadataLanguage] || mediaPart.hearing_impairment_of_participants.label[Object.keys(mediaPart.hearing_impairment_of_participants.label)[0]]) : "";
                        let hearing_impairment_of_participants_label = mediaPart.hearing_impairment_of_participants ? (mediaPart.hearing_impairment_of_participants.field_label[metadataLanguage] || mediaPart.hearing_impairment_of_participants.field_label["en"]) : "";
                        let speaking_impairment_of_participants = mediaPart.speaking_impairment_of_participants ? (mediaPart.speaking_impairment_of_participants.label[metadataLanguage] || mediaPart.speaking_impairment_of_participants.label[Object.keys(mediaPart.speaking_impairment_of_participants.label)[0]]) : "";
                        let speaking_impairment_of_participants_label = mediaPart.speaking_impairment_of_participants ? (mediaPart.speaking_impairment_of_participants.field_label[metadataLanguage] || mediaPart.speaking_impairment_of_participants.field_label["en"]) : "";
                        let number_of_trained_speakers = mediaPart.number_of_trained_speakers ? mediaPart.number_of_trained_speakers.field_value : null;
                        let number_of_trained_speakers_label = mediaPart.number_of_trained_speakers ? (mediaPart.number_of_trained_speakers.field_label[metadataLanguage] || mediaPart.number_of_trained_speakers.field_label["en"]) : "";
                        let speech_influence = mediaPart.speech_influence ? (mediaPart.speech_influence.label[metadataLanguage] || mediaPart.speech_influence.label[Object.keys(mediaPart.speech_influence.label)[0]]) : "";
                        let speech_influence_label = mediaPart.speech_influence ? (mediaPart.speech_influence.field_label[metadataLanguage] || mediaPart.speech_influence.field_label["en"]) : "";
                        //let participant_label = mediaPart.participant ? (mediaPart.participant.field_label[metadataLanguage] || mediaPart.participant.field_label["en"]) : "";
                        /*let participantArray = (mediaPart.participant && mediaPart.participant.field_value.map((participant, participantIndex) => {
                            let alias = participant.alias ? (participant.alias.field_value[metadataLanguage] || participant.alias.field_value[Object.keys(participant.alias.field_value)[0]]) : "";
                            let alias_label = participant.alias ? (participant.alias.field_label[metadataLanguage] || participant.alias.field_label["en"]) : "";
                            let age = participant.age ? participant.age.value : "";
                            let age_label = participant.age ? (participant.age.field_label[metadataLanguage] || participant.age.field_label["en"]) : "";
                            let age_group = participant.age_group ? (participant.age_group.label[metadataLanguage] || participant.age_group.label[Object.keys(participant.age_group.label)[0]]) : "";
                            let age_group_label = participant.age_group ? (participant.age_group.field_label[metadataLanguage] || participant.age_group.field_label["en"]) : "";
                            let sex = participant.sex ? (participant.sex.label[metadataLanguage] || participant.sex.label[Object.keys(participant.sex.label)[0]]) : "";
                            let sex_label = participant.sex ? (participant.sex.field_label[metadataLanguage] || participant.sex.field_label["en"]) : "";
                            let height = participant.height ? participant.height.field_value : "";
                            let height_label = participant.height ? (participant.height.field_label[metadataLanguage] || participant.height.field_label["en"]) : "";
                            let weight = participant.weight ? participant.weight.field_value : "";
                            let weight_label = participant.weight ? (participant.weight.field_label[metadataLanguage] || participant.weight.field_label["en"]) : "";
                            let dialect_accent = participant.dialect_accent ? (participant.dialect_accent.field_value[metadataLanguage] || participant.dialect_accent.field_value[Object.keys(participant.dialect_accent.field_value)[0]]) : "";
                            let dialect_accent_label = participant.dialect_accent ? (participant.dialect_accent.field_label[metadataLanguage] || participant.dialect_accent.field_label["en"]) : "";
                            let origin = participant.origin ? (participant.origin.label[metadataLanguage] || participant.origin.label[Object.keys(participant.origin.label)[0]]) : "";
                            let origin_label = participant.origin ? (participant.origin.field_label[metadataLanguage] || participant.origin.field_label["en"]) : "";
                            let place_of_living = participant.place_of_living ? (participant.place_of_living.field_value[metadataLanguage] || participant.place_of_living.field_value[Object.keys(participant.place_of_living.field_value)[0]]) : "";
                            let place_of_living_label = participant.place_of_living ? (participant.place_of_living.field_label[metadataLanguage] || participant.place_of_living.field_label["en"]) : "";
                            let place_of_birth = participant.place_of_birth ? (participant.place_of_birth.field_value[metadataLanguage] || participant.place_of_birth.field_value[Object.keys(participant.place_of_birth.field_value)[0]]) : "";
                            let place_of_birth_label = participant.place_of_birth ? (participant.place_of_birth.field_label[metadataLanguage] || participant.place_of_birth.field_label["en"]) : "";
                            let place_of_childhood = participant.place_of_childhood ? (participant.place_of_childhood.field_value[metadataLanguage] || participant.place_of_childhood.field_value[Object.keys(participant.place_of_childhood.field_value)[0]]) : "";
                            let place_of_childhood_label = participant.place_of_childhood ? (participant.place_of_childhood.field_label[metadataLanguage] || participant.place_of_childhood.field_label["en"]) : "";
                            let place_of_second_education = participant.place_of_second_education ? (participant.place_of_second_education.field_value[metadataLanguage] || participant.place_of_second_education.field_value[Object.keys(participant.place_of_second_education.field_value)[0]]) : "";
                            let place_of_second_education_label = participant.place_of_second_education ? (participant.place_of_second_education.field_label[metadataLanguage] || participant.place_of_second_education.field_label["en"]) : "";
                            let education_level = participant.education_level ? (participant.education_level.field_value[metadataLanguage] || participant.education_level.field_value[Object.keys(participant.education_level.field_value)[0]]) : "";
                            let education_level_label = participant.education_level ? (participant.education_level.field_label[metadataLanguage] || participant.education_level.field_label["en"]) : "";
                            let profession = participant.profession ? (participant.profession.field_value[metadataLanguage] || participant.profession.field_value[Object.keys(participant.profession.field_value)[0]]) : "";
                            let profession_label = participant.profession ? (participant.profession.field_label[metadataLanguage] || participant.profession.field_label["en"]) : "";
                            let smoking_habit = participant.smoking_habit ? (participant.smoking_habit.field_value[metadataLanguage] || participant.smoking_habit.field_value[Object.keys(participant.smoking_habit.field_value)[0]]) : "";
                            let smoking_habit_label = participant.smoking_habit ? (participant.smoking_habit.field_label[metadataLanguage] || participant.smoking_habit.field_label["en"]) : "";
                            let hearing_impairment = participant.hearing_impairment ? (participant.hearing_impairment.field_value[metadataLanguage] || participant.hearing_impairment.field_value[Object.keys(participant.hearing_impairment.field_value)[0]]) : "";
                            let hearing_impairment_label = participant.hearing_impairment ? (participant.hearing_impairment.field_label[metadataLanguage] || participant.hearing_impairment.field_label["en"]) : "";
                            let speaking_impairment = participant.speaking_impairment ? (participant.speaking_impairment.field_value[metadataLanguage] || participant.speaking_impairment.field_value[Object.keys(participant.speaking_impairment.field_value)[0]]) : "";
                            let speaking_impairment_label = participant.speaking_impairment ? (participant.speaking_impairment.field_label[metadataLanguage] || participant.speaking_impairment.field_label["en"]) : "";
                            let trained_speaker = participant.trained_speaker ? Boolean(participant.trained_speaker) : "";
                            let trained_speaker_label = participant.trained_speaker ? (participant.trained_speaker.field_label[metadataLanguage] || participant.trained_speaker.field_label["en"]) : "";
                            let vocal_tract_condition = participant.vocal_tract_condition ? (participant.vocal_tract_condition.label[metadataLanguage] || participant.vocal_tract_condition.label[Object.keys(participant.vocal_tract_condition.label)[0]]) : "";
                            let vocal_tract_condition_label = participant.vocal_tract_condition ? (participant.vocal_tract_condition.field_label[metadataLanguage] || participant.vocal_tract_condition.field_label["en"]) : "";

                            return <div key={participantIndex}>
                                {alias && <div className="padding15"><Typography className="bold-p--id">{alias_label}</Typography><span className="info_value">{alias}</span></div>}
                                {age && <div className="padding15"><Typography className="bold-p--id">{age_label}</Typography><span className="info_value">{age}</span></div>}
                                {age_group && <div className="padding15"><Typography className="bold-p--id">{age_group_label}</Typography><span className="info_value">{age_group}</span></div>}
                                {sex && <div className="padding15"><Typography className="bold-p--id">{sex_label}</Typography><span className="info_value">{sex}</span></div>}
                                {height && <div className="padding15"><Typography className="bold-p--id">{height_label}</Typography><span className="info_value">{height}</span></div>}
                                {weight && <div className="padding15"><Typography className="bold-p--id">{weight_label}</Typography><span className="info_value">{weight}</span></div>}
                                {dialect_accent && <div className="padding15"><Typography className="bold-p--id">{dialect_accent_label}</Typography><span className="info_value">{dialect_accent}</span></div>}
                                {origin && <div className="padding15"><Typography className="bold-p--id">{origin_label}</Typography><span className="info_value">{origin}</span></div>}
                                {place_of_living && <div className="padding15"><Typography className="bold-p--id">{place_of_living_label}</Typography><span className="info_value">{place_of_living}</span></div>}
                                {place_of_birth && <div className="padding15"><Typography className="bold-p--id">{place_of_birth_label}</Typography><span className="info_value">{place_of_birth}</span></div>}
                                {place_of_childhood && <div className="padding15"><Typography className="bold-p--id">{place_of_childhood_label}</Typography><span className="info_value">{place_of_childhood}</span></div>}
                                {place_of_second_education && <div className="padding15"><Typography className="bold-p--id">{place_of_second_education_label}</Typography><span className="info_value">{place_of_second_education}</span></div>}
                                {education_level && <div className="padding15"><Typography className="bold-p--id">{education_level_label}</Typography><span className="info_value">{education_level}</span></div>}
                                {profession && <div className="padding15"><Typography className="bold-p--id">{profession_label}</Typography><span className="info_value">{profession}</span></div>}
                                {smoking_habit && <div className="padding15"><Typography className="bold-p--id">{smoking_habit_label}</Typography><span className="info_value">{smoking_habit}</span></div>}
                                {hearing_impairment && <div className="padding15"><Typography className="bold-p--id">{hearing_impairment_label}</Typography><span className="info_value">{hearing_impairment}</span></div>}
                                {speaking_impairment && <div className="padding15"><Typography className="bold-p--id">{speaking_impairment_label}</Typography><span className="info_value">{speaking_impairment}</span></div>}
                                {trained_speaker && <div className="padding15"><Typography className="bold-p--id">{trained_speaker_label}</Typography><span className="info_value">{`${trained_speaker}`}</span></div>}
                                {vocal_tract_condition && <div className="padding15"><Typography className="bold-p--id">{vocal_tract_condition_label}</Typography><span className="info_value">{vocal_tract_condition}</span></div>}
                                <hr />
                            </div>
                        })) || [];*/
                        let creation_mode_label = mediaPart.creation_mode ? (mediaPart.creation_mode.field_label[metadataLanguage] || mediaPart.creation_mode.field_label["en"]) : "";
                        let creation_mode = mediaPart.creation_mode ? (mediaPart.creation_mode.label[metadataLanguage] || mediaPart.creation_mode.label[Object.keys(mediaPart.creation_mode.label)[0]]) : "";
                        let is_created_by_label = mediaPart.is_created_by ? (mediaPart.is_created_by.field_label[metadataLanguage] || mediaPart.is_created_by.field_label["en"]) : "";
                        let is_created_byArray = (mediaPart.is_created_by && mediaPart.is_created_by.field_value.map((createdBy, createdByIndex) => {
                            let resource_name = createdBy.resource_name ? (createdBy.resource_name.field_value[metadataLanguage] || createdBy.resource_name.field_value[Object.keys(createdBy.resource_name.field_value)[0]]) : "";
                            let version = createdBy.version ? createdBy.version.field_value : "";
                            let full_metadata_record = commonParser.getFullMetadata(createdBy.full_metadata_record);
                            return <div key={createdByIndex}>
                                {full_metadata_record ?
                                    <div className="padding5 internal_url">
                                        <span><NavIcon className="xsmall-icon mr-05" /></span>
                                        <Link to={full_metadata_record.internalELGUrl}>
                                            <span className="info_value">{resource_name} ({version})</span>
                                        </Link>
                                    </div> :
                                    <span className="info_value">{resource_name} ({version})</span>
                                }
                            </div>
                        })) || [];
                        let has_original_source_label = mediaPart.has_original_source ? (mediaPart.has_original_source.field_label[metadataLanguage] || mediaPart.has_original_source.field_label["en"]) : "";
                        let has_original_sourceArray = (mediaPart.has_original_source && mediaPart.has_original_source.field_value.map((originalSource, originalSourceIndex) => {
                            let resource_name = originalSource.resource_name ? (originalSource.resource_name.field_value[metadataLanguage] || originalSource.resource_name.field_value[Object.keys(originalSource.resource_name.field_value)[0]]) : "";
                            let version = originalSource.version ? originalSource.version.field_value : "";
                            let full_metadata_record = commonParser.getFullMetadata(originalSource.full_metadata_record);
                            return <div key={originalSourceIndex}>
                                {full_metadata_record ?
                                    <div className="padding5 internal_url">
                                        <span><NavIcon className="xsmall-icon mr-05" /></span>
                                        <Link to={full_metadata_record.internalELGUrl}>
                                            <span className="info_value">{resource_name} ({version})</span>
                                        </Link>
                                    </div> :
                                    <span className="info_value">{resource_name} ({version})</span>
                                }
                            </div>
                        })) || [];

                        let annotation_label = mediaPart.annotation ? (mediaPart.annotation.field_label[metadataLanguage] || mediaPart.annotation.field_label["en"]) : "";
                        let annotation_Array = (mediaPart.annotation && mediaPart.annotation.field_value.map((annotationItem, annotationIndex) => {
                            let annotation_type_label = annotationItem.annotation_type ? annotationItem.annotation_type.field_label[metadataLanguage] || annotationItem.annotation_type.field_label["en"] : "";
                            //let annotation_typeArray = annotationItem.annotation_type? annotationItem.annotation_type.field_value.label[metadataLanguage] || annotationItem.annotation_type.field_value.label[Object.keys(annotationItem.annotation_type.field_value.label)[0]]: "";
                            let annotation_typeArray = annotationItem.annotation_type ? annotationItem.annotation_type.field_value.map(item => item.label ? (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]]) : item.value) : [];
                            let annotated_elementArray = annotationItem.annotated_element ? annotationItem.annotated_element.field_value.map(item => (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]])) : [];
                            let annotated_element_label = annotationItem.annotated_element ? annotationItem.annotated_element.field_label[metadataLanguage] || annotationItem.annotated_element.field_label["en"] : "";
                            let segmentation_levelArray = annotationItem.segmentation_level ? annotationItem.segmentation_level.field_value.map(item => (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]])) : [];
                            let segmentation_level_label = annotationItem.segmentation_level ? annotationItem.segmentation_level.field_label[metadataLanguage] || annotationItem.segmentation_level.field_label["en"] : "";
                            let annotation_standoff = annotationItem.annotation_standoff ? Boolean(annotationItem.annotation_standoff.field_value) : undefined;
                            let annotation_standoff_label = annotationItem.annotation_standoff ? annotationItem.annotation_standoff.field_label[metadataLanguage] || annotationItem.annotation_standoff.field_label["en"] : "";
                            let guidelines_label = annotationItem.guidelines ? annotationItem.guidelines.field_label[metadataLanguage] || annotationItem.guidelines.field_label["en"] : "";


                            let guidelinesArray = annotationItem.guidelines ? annotationItem.guidelines.field_value.map((item, index) => {
                                let bibliographic_record = item.bibliographic_record ? item.bibliographic_record.field_value : "";
                                return <div key={index}>
                                    <DocumentIdentifier data={item} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                                    {bibliographic_record && <>
                                        <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                                </div>
                            }) : [];

                            /*let tagset_label = annotationItem.tagset ? annotationItem.tagset.field_label[metadataLanguage] || annotationItem.tagset.field_label["en"] : "";
                            let tagsetArray = annotationItem.tagset ? annotationItem.tagset.field_value.map((item, index) => {
                                let resource_name = item.resource_name ? item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]] : "";//corpusParser.getLanguageDependentValue(item.resource_name, metadataLanguage);
                                let version = item.version ? item.version.field_value : "";
                                let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                                return <div key={index}>
                                    {full_metadata_record ?
                                        <div className="padding5 internal_url">
                                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                                            <Link to={full_metadata_record.internalELGUrl}>
                                                <span className="info_value">{resource_name} ({version})</span>
                                            </Link>
                                        </div> :
                                        <span className="info_value">{resource_name} ({version})</span>
                                    }
                                </div>
                            }) : [];*/
                            let theoretic_model = annotationItem.theoretic_model ? annotationItem.theoretic_model.field_value[metadataLanguage] || annotationItem.theoretic_model.field_value[Object.keys(annotationItem.theoretic_model.field_value)[0]] : "";
                            let theoretic_model_label = annotationItem.theoretic_model ? annotationItem.theoretic_model.field_label[metadataLanguage] || annotationItem.theoretic_model.field_label["en"] : "";
                            let annotation_mode = annotationItem.annotation_mode ? annotationItem.annotation_mode.label[metadataLanguage] || annotationItem.annotation_mode.label[Object.keys(annotationItem.annotation_mode.label)[0]] : "";
                            let annotation_mode_label = annotationItem.annotation_mode ? annotationItem.annotation_mode.field_label[metadataLanguage] || annotationItem.annotation_mode.field_label["en"] : "";
                            let annotation_mode_details = annotationItem.annotation_mode_details ? annotationItem.annotation_mode_details.field_value[metadataLanguage] || annotationItem.annotation_mode_details.field_value[Object.keys(annotationItem.annotation_mode_details.field_value)[0]] : "";
                            let annotation_mode_details_label = annotationItem.annotation_mode_details ? annotationItem.annotation_mode_details.field_label[metadataLanguage] || annotationItem.annotation_mode_details.field_label["en"] : "";
                            let is_annotated_by_label = annotationItem.is_annotated_by ? annotationItem.is_annotated_by.field_label[metadataLanguage] || annotationItem.is_annotated_by.field_label["en"] : "";
                            let is_annotated_byArray = annotationItem.is_annotated_by ? annotationItem.is_annotated_by.field_value.map((item, index) => {
                                let resource_name = item.resource_name ? item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]] : "";
                                let version = item.version ? item.version.field_value : "";
                                let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                                return <div key={index}>
                                    {full_metadata_record ?
                                        <div className="padding5 internal_url">
                                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                                            <Link to={full_metadata_record.internalELGUrl}>
                                                <span className="info_value">{resource_name} ({version})</span>
                                            </Link>
                                        </div> :
                                        <span className="info_value">{resource_name} ({version})</span>
                                    }
                                </div>
                            }) : [];
                            let annotator_label = annotationItem.annotator ? annotationItem.annotator.field_label[metadataLanguage] || annotationItem.annotator.field_label["en"] : "";
                            let annotatorArray = annotationItem.annotator ? annotationItem.annotator : [];
                            let annotation_start_date = annotationItem.annotation_start_date ? annotationItem.annotation_start_date.field_value : "";
                            //let annotation_start_date_label=annotationItem.annotation_start_date.field_label[metadataLanguage]||annotationItem.annotation_start_date.field_label["en"];
                            let annotation_end_date = annotationItem.annotation_end_date ? annotationItem.annotation_end_date.field_value : "";
                            //let annotation_end_date_label=annotationItem.annotation_end_date.field_label[metadataLanguage]||annotationItem.annotation_end_date.field_label["en"];        
                            let interannotator_agreement = annotationItem.interannotator_agreement ? annotationItem.interannotator_agreement.field_value[metadataLanguage] || annotationItem.interannotator_agreement.field_value[Object.keys(annotationItem.interannotator_agreement.field_label)[0]] : "";
                            let interannotator_agreement_label = annotationItem.interannotator_agreement ? annotationItem.interannotator_agreement.field_label[metadataLanguage] || annotationItem.interannotator_agreement.field_label["en"] : "";
                            let intraannotator_agreement = annotationItem.intraannotator_agreement ? annotationItem.intraannotator_agreement.field_value[metadataLanguage] || annotationItem.intraannotator_agreement.field_value[Object.keys(annotationItem.interannotator_agreement.field_label)[0]] : "";
                            let intraannotator_agreement_label = annotationItem.intraannotator_agreement ? annotationItem.intraannotator_agreement.field_label[metadataLanguage] || annotationItem.intraannotator_agreement.field_label["en"] : "";
                            let annotation_report_label = annotationItem.annotation_report ? annotationItem.annotation_report.field_label[metadataLanguage] || annotationItem.annotation_report.field_label["en"] : "";
                            let annotation_reportArray = annotationItem.annotation_report ? annotationItem.annotation_report.field_value.map((item, index) => {
                                let bibliographic_record = annotationItem.bibliographic_record ? annotationItem.bibliographic_record.field_value : "";
                                return <div key={index}>
                                    <DocumentIdentifier data={item} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                                    {bibliographic_record && <>
                                        <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                                </div>
                            }) : [];
                            let annotation_resource_label = annotationItem.annotation_resource ? annotationItem.annotation_resource.field_label[metadataLanguage] || annotationItem.annotation_resource.field_label["en"] : "";
                            let annotation_resourceArray = annotationItem.annotation_resource ? annotationItem.annotation_resource.field_value.map((item, index) => {
                                let resource_name = item.resource_name ? item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]] : "";
                                let version = item.version ? item.version.field_value : "";
                                let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                                return <div key={index}>
                                    {full_metadata_record ?
                                        <div className="padding5 internal_url">
                                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                                            <Link to={full_metadata_record.internalELGUrl}>
                                                <span className="info_value">{resource_name} ({version})</span>
                                            </Link>
                                        </div> :
                                        <span className="info_value">{resource_name} ({version})</span>
                                    }
                                </div>
                            }) : [];
                            return <div key={annotationIndex}>
                                {annotation_typeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotation_type_label}</Typography><div className="flex wrap">{annotation_typeArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                {annotated_elementArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotated_element_label}</Typography><div className="flex wrap">{annotated_elementArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                {annotation_resourceArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotation_resource_label}</Typography><span className="info_value">{annotation_resourceArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                {segmentation_levelArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{segmentation_level_label}</Typography><div className="flex wrap">{segmentation_levelArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                {annotation_standoff !== undefined && <div className="padding5"><Typography className="bold-p--id">{annotation_standoff_label}</Typography><span className="info_value">{`${annotation_standoff}`}</span></div>}
                                {guidelinesArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{guidelines_label}</Typography><span className="info_value">{guidelinesArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                {/*tagsetArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{tagset_label}</Typography><span className="info_value">{tagsetArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>*/}
                                {theoretic_model && <div className="padding5"><Typography className="bold-p--id">{theoretic_model_label}</Typography><span className="info_value">{theoretic_model}</span></div>}
                                {annotation_mode && <div className="padding5"><Typography className="bold-p--id">{annotation_mode_label}</Typography><span className="info_value">{annotation_mode}</span></div>}
                                {annotation_mode_details && <div className="padding5"><Typography className="bold-p--id">{annotation_mode_details_label}</Typography><span className="info_value">{annotation_mode_details}</span></div>}
                                {is_annotated_byArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_annotated_by_label}</Typography><span className="info_value">{is_annotated_byArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                {annotatorArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotator_label}</Typography><ResourceActor data={annotatorArray} metadataLanguage={metadataLanguage} /></div>}
                                {(annotation_start_date || annotation_end_date) && <div className="padding5"><Typography className="bold-p--id">Annotation date</Typography>
                                    <span className="info_value">{annotation_start_date && Intl.DateTimeFormat("en-GB", {
                                        year: "numeric",
                                        month: "long",
                                        day: "2-digit"
                                    }).format(new Date(annotation_start_date))}
                                        {annotation_end_date && <span>-</span>} {annotation_end_date && Intl.DateTimeFormat("en-GB", {
                                            year: "numeric",
                                            month: "long",
                                            day: "2-digit"
                                        }).format(new Date(annotation_end_date))}</span></div>}
                                {interannotator_agreement && <div className="padding5"><Typography className="bold-p--id">{interannotator_agreement_label}</Typography><span className="info_value">{interannotator_agreement}</span></div>}
                                {intraannotator_agreement && <div className="padding5"><Typography className="bold-p--id">{intraannotator_agreement_label}</Typography><span className="info_value">{intraannotator_agreement}</span></div>}
                                {annotation_reportArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotation_report_label}</Typography><span className="info_value">{annotation_reportArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                <hr />
                            </div>
                        })) || [];


                        let original_source_description_label = mediaPart.original_source_description ? (mediaPart.original_source_description.field_label[metadataLanguage] || mediaPart.original_source_description.field_label["en"]) : "";
                        let original_source_description = mediaPart.original_source_description ? (mediaPart.original_source_description.field_value[metadataLanguage] || mediaPart.original_source_description.field_value[Object.keys(mediaPart.original_source_description.field_value)[0]]) : "";
                        let synthetic_data_label = mediaPart.synthetic_data ? (mediaPart.synthetic_data.field_label[metadataLanguage] || mediaPart.synthetic_data.field_label["en"]) : "";
                        let synthetic_data = mediaPart.synthetic_data ? Boolean(mediaPart.synthetic_data.field_value) : "";
                        let creation_details_label = mediaPart.creation_details ? (mediaPart.creation_details.field_label[metadataLanguage] || mediaPart.creation_details.field_label["en"]) : "";
                        let creation_details = mediaPart.creation_details ? (mediaPart.creation_details.field_value[metadataLanguage] || mediaPart.creation_details.field_value[Object.keys(mediaPart.creation_details.field_value)[0]]) : "";
                        let link_to_other_media_label = mediaPart.link_to_other_media ? (mediaPart.link_to_other_media.field_label[metadataLanguage] || mediaPart.link_to_other_media.field_label["en"]) : "";
                        let link_to_other_mediaArary = (mediaPart.link_to_other_media && mediaPart.link_to_other_media.field_value.map((otherMedia, otherMediaIndex) => {
                            let other_mediaArray = (otherMedia.other_media && otherMedia.other_media.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]])) || [];
                            let otherMedia_label = otherMedia.other_media ? (otherMedia.other_media.field_label[metadataLanguage] || otherMedia.other_media.field_label["en"]) : "";
                            let media_type_details = otherMedia.media_type_details ? (otherMedia.media_type_details.field_value[metadataLanguage] || otherMedia.media_type_details.field_value[Object.keys(otherMedia.media_type_details.field_value)[0]]) : "";
                            let media_type_details_label = otherMedia.media_type_details ? (otherMedia.media_type_details.field_label[metadataLanguage] || otherMedia.media_type_details.field_label["en"]) : "";
                            let synchronized_with_text = otherMedia.synchronized_with_text ? Boolean(otherMedia.synchronized_with_text.field_value) : "";
                            let synchronized_with_text_label = otherMedia.synchronized_with_text ? (otherMedia.synchronized_with_text.field_label[metadataLanguage] || otherMedia.synchronized_with_text.field_label["en"]) : "";
                            let synchronized_with_audio = otherMedia.synchronized_with_audio ? Boolean(otherMedia.synchronized_with_audio.field_value) : "";
                            let synchronized_with_audio_label = otherMedia.synchronized_with_audio ? (otherMedia.synchronized_with_audio.field_label[metadataLanguage] || otherMedia.synchronized_with_audio.field_label["en"]) : "";
                            let synchronized_with_video = otherMedia.synchronized_with_video ? Boolean(otherMedia.synchronized_with_video.field_value) : "";
                            let synchronized_with_video_label = otherMedia.synchronized_with_video ? (otherMedia.synchronized_with_video.field_label[metadataLanguage] || otherMedia.synchronized_with_video.field_label["en"]) : "";
                            let synchronized_with_image = otherMedia.synchronized_with_image ? Boolean(otherMedia.synchronized_with_image.field_value) : "";
                            let synchronized_with_image_label = otherMedia.synchronized_with_image ? (otherMedia.synchronized_with_image.field_label[metadataLanguage] || otherMedia.synchronized_with_image.field_label["en"]) : "";
                            let synchronized_with_text_numerical = otherMedia.synchronized_with_text_numerical ? Boolean(otherMedia.synchronized_with_text_numerical.field_value) : "";
                            let synchronized_with_text_numerical_label = otherMedia.synchronized_with_text_numerical ? (otherMedia.synchronized_with_text_numerical.field_label[metadataLanguage] || otherMedia.synchronized_with_text_numerical.field_label["en"]) : "";
                            return <div key={otherMediaIndex}>
                                {other_mediaArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{otherMedia_label}</Typography> {other_mediaArray.map((media, mediaIndex) => <div className="info_value" key={mediaIndex}>{media} </div>)}</div>}
                                {media_type_details && <div className="padding15"><Typography className="bold-p--id">{media_type_details_label}</Typography><span className="info_value">{media_type_details}</span></div>}
                                {synchronized_with_text && <div className="padding15"><Typography className="bold-p--id">{synchronized_with_text_label}</Typography><span className="info_value">{`${synchronized_with_text}`}</span></div>}
                                {synchronized_with_audio && <div className="padding15"><Typography className="bold-p--id">{synchronized_with_audio_label}</Typography><span className="info_value">{`${synchronized_with_audio}`}</span></div>}
                                {synchronized_with_video && <div className="padding15"><Typography className="bold-p--id">{synchronized_with_video_label}</Typography><span className="info_value">{`${synchronized_with_video}`}</span></div>}
                                {synchronized_with_image && <div className="padding15"><Typography className="bold-p--id">{synchronized_with_image_label}</Typography><span className="info_value">{`${synchronized_with_image}`}</span></div>}
                                {synchronized_with_text_numerical && <div className="padding15"><Typography className="bold-p--id">{synchronized_with_text_numerical_label}</Typography><span className="info_value">{`${synchronized_with_text_numerical}`}</span></div>}
                            </div>
                        })) || [];
                        let text_type_label = mediaPart.text_type ? (mediaPart.text_type.field_label[metadataLanguage] || mediaPart.text_type.field_label["en"]) : "";
                        let text_typeArray = (mediaPart.text_type && mediaPart.text_type.field_value.map((textType, textTypeIndex) => {
                            let category_label = textType.category_label ? (textType.category_label.field_value[metadataLanguage] || textType.category_label.field_value[Object.keys(textType.category_label.field_value)[0]]) : "";
                            let text_type_identifier = textType.text_type_identifier ? textType.text_type_identifier : "";
                            //let category_label_label = textType.category_label ? (textType.category_label.field_label[metadataLanguage] || textType.category_label.field_label["en"]) : "";
                            return <div key={textTypeIndex}>
                                {category_label && <><span>{category_label}</span>
                                    <GeneralIdentifier data={text_type_identifier} identifier_name={"text_type_identifier"} identifier_scheme={"text_type_classification_scheme"} metadataLanguage={metadataLanguage} /> </>}
                            </div>
                        })) || [];
                        let text_genre = [];
                        let text_genre_label = mediaPart.text_genre ? (mediaPart.text_genre.field_label[metadataLanguage] || mediaPart.text_genre.field_label["en"]) : "";
                        (mediaPart.text_genre && mediaPart.text_genre.field_value.length > 0 && mediaPart.text_genre.field_value.map((genre, genreIndex) => {
                            let category_label = (genre.category_label && (genre.category_label.field_value[metadataLanguage] || genre.category_label.field_value[Object.keys(genre.category_label.field_value)[0]])) || "";
                            text_genre.push(category_label);
                            return void 0;
                        }));

                        let dynamic_elementArray = mediaPart.dynamic_element;
                        let dynamic_element_label = mediaPart.dynamic_element ? (mediaPart.dynamic_element.field_label[metadataLanguage] || mediaPart.dynamic_element.field_label["en"]) : "";

                        return <VerticalTabPanel key={mediaPartIndex} value={this.state.tab} index={mediaPartIndex} className="vertical-tab-pannel">
                            <Grid container direction="row" justifyContent="center" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs={12}>
                                    {<Languages language = {mediaPart.language} from_tool={false} inferred_language={inferred_language} />}
                                </Grid>
                                <Grid item xs={6}>
                                    {/*corpus_media_type && <div className="padding15"><Typography className="bold-p--id">corpus media type</Typography><span className="info_value">{corpus_media_type}</span></div>*/}
                                    {/*media_type && <div className="padding15"><Typography className="bold-p--id">{media_type_label}</Typography><span className="info_value">{media_type}</span></div>*/}
                                    {linguality_type && <div className="padding15"><Typography className="bold-p--id">{linguality_type_label}</Typography><span className="info_value">{linguality_type}</span></div>}
                                    {multilinguality_type && <div className="padding15"><Typography className="bold-p--id">{multilinguality_type_label}</Typography><span className="info_value">{multilinguality_type}</span></div>}
                                    {multilinguality_type_details && <div className="padding15"><Typography className="bold-p--id">{multilinguality_type_details_label}</Typography><span className="info_value">{multilinguality_type_details}</span></div>}
                                    
                                    {/*languageInfoArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{language_label}</Typography><span className="info_value">{languageInfoArray.map((item, index) => <span key={index}>{item}</span>)}</span></div>*/}
                                    {modality_typeArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{modality_type_label}</Typography><div className="flex wrap">{modality_typeArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {audio_genre.length > 0 && <div className="padding15"><Typography className="bold-p--id">{audio_genre_label}</Typography><div className="flex wrap">{audio_genre.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {naturality && <div className="padding15"><Typography className="bold-p--id">{naturality_label}</Typography><span className="info_value">{naturality}</span></div>}
                                    {conversational_type.length > 0 && <div className="padding15"><Typography className="bold-p--id">{conversational_type_label}</Typography><span className="info_value">{conversational_type.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</span></div>}
                                    {scenario_type.length > 0 && <div className="padding15"><Typography className="bold-p--id">{scenario_type_label}</Typography><div className="flex wrap">{scenario_type.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {text_typeArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{text_type_label}</Typography><div className="flex wrap">{text_typeArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {text_genre.length > 0 && <div className="padding15"><Typography className="bold-p--id">{text_genre_label}</Typography><div className="flex wrap">{text_genre.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {video_genre.length > 0 && <div className="padding15"><Typography className="bold-p--id">{video_genre_label}</Typography><div className="flex wrap">{video_genre.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {speech_genre.length > 0 && <div className="padding15"><Typography className="bold-p--id">{speech_genre_label}</Typography><div className="flex wrap">{speech_genre.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {speech_item.length > 0 && <div className="padding15"><Typography className="bold-p--id">{speech_item_label}</Typography><div className="flex wrap">{speech_item.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {non_speech_item.length > 0 && <div className="padding15"><Typography className="bold-p--id">{non_speech_item_label}</Typography><div className="flex wrap">{non_speech_item.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {legend && <div className="padding15"><Typography className="bold-p--id">{legend_label}</Typography><span className="info_value">{legend}</span></div>}
                                    {noise_level && <div className="padding15"><Typography className="bold-p--id">{noise_level_label}</Typography><span className="info_value">{noise_level}</span></div>}
                                    {audience && <div className="padding15"><Typography className="bold-p--id">{audience_label}</Typography><span className="info_value">{audience}</span></div>}
                                    {interactivity && <div className="padding15"><Typography className="bold-p--id">{interactivity_label}</Typography><span className="info_value">{interactivity}</span></div>}
                                    {interaction && <div className="padding15"><Typography className="bold-p--id">{interaction_label}</Typography><span className="info_value">{interaction}</span></div>}
                                    {type_of_image_content.length > 0 && <div className="padding15"><Typography className="bold-p--id">{type_of_image_content_label}</Typography><div className="flex wrap">{type_of_image_content.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {type_of_video_content.length > 0 && <div className="padding15"><Typography className="bold-p--id">{type_of_video_content_label}</Typography><div className="flex wrap">{type_of_video_content.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {dynamic_elementArray && <Typography className="section-headings">{dynamic_element_label}</Typography>}
                                    {<LcrDynamicElement dynamic_elementArray={dynamic_elementArray} metadataLanguage={metadataLanguage} />}
                                    {recording_device_type && <div className="padding15"><Typography className="bold-p--id">{recording_device_type_label}</Typography><span className="info_value">{recording_device_type}</span></div>}
                                    {recording_device_type_details && <div className="padding15"><Typography className="bold-p--id">{recording_device_type_details_label}</Typography><span className="info_value">{recording_device_type_details}</span></div>}
                                    {recording_platform_software && <div className="padding15"><Typography className="bold-p--id">{recording_platform_software_label}</Typography><span className="info_value">{recording_platform_software}</span></div>}
                                    {recording_environment && <div className="padding15"><Typography className="bold-p--id">{recording_environment_label}</Typography><span className="info_value">{recording_environment}</span></div>}
                                    {source_channel && <div className="padding15"><Typography className="bold-p--id">{source_channel_label}</Typography><span className="info_value">{source_channel}</span></div>}
                                    {source_channel_type && <div className="padding15"><Typography className="bold-p--id">{source_channel_type_label}</Typography><span className="info_value">{source_channel_type}</span></div>}
                                    {source_channel_name && <div className="padding15"><Typography className="bold-p--id">{source_channel_name_label}</Typography><span className="info_value">{source_channel_name}</span></div>}
                                    {source_channel_details && <div className="padding15"><Typography className="bold-p--id">{source_channel_details_label}</Typography><span className="info_value">{source_channel_details}</span></div>}
                                    {recorderArray && <ResourceActor data={recorderArray} metadataLanguage={metadataLanguage} />}
                                    {type_of_text_numerical_content.length > 0 && <div className="padding15"><Typography className="bold-p--id">{type_of_text_numerical_content_label}</Typography><div className="flex wrap">{type_of_text_numerical_content.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {capturing_device_type && <div className="padding15"><Typography className="bold-p--id">{capturing_device_type_label} </Typography><span className="info_value">{capturing_device_type}</span></div>}
                                    {capturing_device_type_details && <div className="padding15"><Typography className="bold-p--id">{capturing_device_type_details_label} </Typography><span className="info_value">{capturing_device_type_details}</span></div>}
                                    {capturing_details && <div className="padding15"><Typography className="bold-p--id">{capturing_details_label} </Typography><span className="info_value">{capturing_details}</span></div>}
                                    {capturing_environment && <div className="padding15"><Typography className="bold-p--id">{capturing_environment_label}</Typography><span className="info_value">{capturing_environment}</span></div>}
                                    {sensor_technology && <div className="padding15"><Typography className="bold-p--id">{sensor_technology_label}</Typography><span className="info_value">{sensor_technology}</span></div>}
                                    {scene_illumination && <div className="padding15"><Typography className="bold-p--id">{scene_illumination_label}</Typography><span className="info_value">{scene_illumination}</span></div>}
                                    {number_of_participants !== null && <div className="padding15"><Typography className="bold-p--id">{number_of_participants_label}</Typography><span className="info_value">{number_of_participants}</span></div>}
                                    {age_group_of_participants && <div className="padding15"><Typography className="bold-p--id">{age_group_of_participants_label}</Typography><span className="info_value">{age_group_of_participants}</span></div>}
                                    {(age_range_start_of_participants !== null || age_range_end_of_participants !== null) && <div className="padding15"><Typography className="bold-p--id">{age_range_start_of_participants_label}-{age_range_end_of_participants_label}</Typography><span className="info_value">{age_range_start_of_participants} - {age_range_end_of_participants}</span></div>}
                                    {sex_of_participants && <div className="padding15"><Typography className="bold-p--id">{sex_of_participants_label}</Typography><span className="info_value">{sex_of_participants}</span></div>}
                                    {origin_of_participants && <div className="padding15"><Typography className="bold-p--id">{origin_of_participants_label}</Typography><span className="info_value">{origin_of_participants}</span></div>}
                                    {dialect_accent_of_participants.length > 0 && <div className="padding15"><Typography className="bold-p--id">{dialect_accent_of_participants_label}</Typography><span className="info_value">{dialect_accent_of_participants.map((item, index) => <span className="info_value" key={index}>{item} </span>)}</span></div>}
                                    {geographic_distribution_of_participants.length > 0 && <div className="padding15"><Typography className="bold-p--id">{geographic_distribution_of_participants_label}</Typography><span className="info_value">{geographic_distribution_of_participants.map((item, index) => <span className="info_value" key={index}>{item} </span>)}</span></div>}
                                    {hearing_impairment_of_participants && <div className="padding15"><Typography className="bold-p--id">{hearing_impairment_of_participants_label}</Typography><span className="info_value">{hearing_impairment_of_participants}</span></div>}
                                    {speaking_impairment_of_participants && <div className="padding15"><Typography className="bold-p--id">{speaking_impairment_of_participants_label}</Typography><span className="info_value">{speaking_impairment_of_participants}</span></div>}
                                    {number_of_trained_speakers !== null && <div className="padding15"><Typography className="bold-p--id">{number_of_trained_speakers_label}</Typography><span className="info_value">{number_of_trained_speakers}</span></div>}
                                    {speech_influence && <div className="padding15"><Typography className="bold-p--id">{speech_influence_label}</Typography><span className="info_value">{speech_influence}</span></div>}
                                    {/*participantArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{participant_label}</Typography><span className="info_value">{participantArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>*/}
                                    {creation_mode && <div className="padding15"><Typography className="bold-p--id">{creation_mode_label}</Typography><span className="info_value">{creation_mode}</span></div>}
                                    {is_created_byArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{is_created_by_label}</Typography><span className="info_value">{is_created_byArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                    {has_original_sourceArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{has_original_source_label}</Typography><span className="info_value">{has_original_sourceArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                    {original_source_description && <div className="padding15"><Typography className="bold-p--id">{original_source_description_label}</Typography><span className="info_value">{original_source_description}</span></div>}
                                    {synthetic_data && <div className="padding15"><Typography className="bold-p--id">{synthetic_data_label}</Typography><span className="info_value">{`${synthetic_data}`}</span></div>}
                                    {creation_details && <div className="padding15"><Typography className="bold-p--id">{creation_details_label}</Typography><span className="info_value">{creation_details}</span></div>}
                                    {link_to_other_mediaArary.length > 0 && <div className="padding15"><Typography className="section-headings">{link_to_other_media_label}</Typography>{link_to_other_mediaArary.map((item, index) => <span className="info_value bottom-border" key={index} >{item}</span>)}</div>}

                                </Grid>
                                <Grid item xs={6}>
                                    {annotation_Array.length > 0 && <div className="padding15"><Typography className="section-headings">{annotation_label}</Typography>{annotation_Array.map((item, index) => <span className="info_value" key={index}>{item} </span>)}</div>}

                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                    })}
                </div>
            </div>
        </>
    }
}

