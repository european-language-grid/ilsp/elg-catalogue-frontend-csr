import React from "react";
import classnames from 'classnames';
import { Nav, NavItem, NavLink } from "reactstrap";
export default class NavigationTabs extends React.Component {
    render() {
        const { activeTab } = this.props;
        const { tabTitles = [] } = this.props || [];
        return <div>
            <Nav tabs>
                {tabTitles.map((tabTitle, index) => <NavItem key={index}>
                    <NavLink className={classnames({ active: activeTab === `${(index + 1)}` })} onClick={() => this.props.toggleTab(tabTitles, `${(index + 1)}`)}>{tabTitle}</NavLink>
                </NavItem>)}
            </Nav>
        </div>
    }
}