import React from "react";
import Typography from '@material-ui/core/Typography';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

export default class CorpusGeneral extends React.Component {
    componentDidMount() {
        const { data } = this.props;
        const { is_analysed_by, is_edited_by, is_elicited_by, is_converted_version_of, time_coverage, geographic_coverage, register } = data.described_entity.field_value.lr_subclass.field_value || [];
        const { is_annotated_version_of, is_aligned_version_of } = data.described_entity.field_value.lr_subclass.field_value || "";
        if (!data) {
            return false;
        }
        const { displayTabGeneral } = this.props;
        if (

            (is_analysed_by && is_analysed_by.field_value.length > 0) ||
            (is_edited_by && is_edited_by.field_value.length > 0) ||
            (is_elicited_by && is_elicited_by.field_value.length > 0) ||
            (is_annotated_version_of) ||
            (is_aligned_version_of) ||
            (is_converted_version_of && is_converted_version_of.field_value.length > 0) ||
            (time_coverage && time_coverage.field_value.length > 0) ||
            (geographic_coverage && geographic_coverage.field_value.length > 0) ||
            (register && register.field_value.length > 0)
        ) {
            displayTabGeneral ? void 0 : this.props.displayTabGeneralFunction(true);
        }
    }
    render() {
        const { personal_data_included, personal_data_details, sensitive_data_included, sensitive_data_details, anonymized, anonymization_details, user_query, data, metadataLanguage } = this.props;
        const { is_analysed_by, is_edited_by, is_elicited_by, is_converted_version_of, time_coverage, geographic_coverage, register } = data.described_entity.field_value.lr_subclass.field_value || [];
        const { is_annotated_version_of, is_aligned_version_of } = data.described_entity.field_value.lr_subclass.field_value || "";
        //const { personal_data_included_label, personal_data_details_label, sensitive_data_included_label, sensitive_data_details_label, anonymized_label, anonymization_details_label, user_query_label } = this.props;

        if (!data) {
            return <div></div>
        }
        return <>
            {/*corpus_subclass && <div className="padding5"><Typography className="bold-p--id">corpus subclass</Typography> <span className="info_value">{corpus_subclass}</span></div>*/}





            {is_analysed_by && is_analysed_by.field_value.length > 0 && <div className="padding5"> <Typography className="bold-p--id">{is_analysed_by.field_label[metadataLanguage] || is_analysed_by.field_label["en"]}</Typography>
                {is_analysed_by && is_analysed_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version?.field_value || "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {is_edited_by && is_edited_by.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_edited_by.field_label[metadataLanguage] || is_edited_by.field_label["en"]}</Typography>
                {is_edited_by && is_edited_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name.field_value ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version?.field_value || "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {is_elicited_by && is_elicited_by.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_elicited_by.field_label[metadataLanguage] || is_elicited_by.field_label["en"]}</Typography>
                {is_elicited_by && is_elicited_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version?.field_value || "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {is_annotated_version_of &&
                <div className="padding5">
                    <Typography className="bold-p--id">{is_annotated_version_of.field_label[metadataLanguage] || is_annotated_version_of.field_label["en"]}</Typography>
                    {
                        commonParser.getFullMetadata(is_annotated_version_of.field_value.full_metadata_record) ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={commonParser.getFullMetadata(is_annotated_version_of.field_value.full_metadata_record).internalELGUrl}>
                                    <span className="info_value">
                                        {is_annotated_version_of.field_value.resource_name.field_value[metadataLanguage] || is_annotated_version_of.field_value.resource_name.field_value[Object.keys(is_annotated_version_of.field_value.resource_name.field_value)[0]]}
                                        {is_annotated_version_of.field_value.version && is_annotated_version_of.field_value.version?.field_value}
                                    </span>
                                </Link>
                            </div>
                            :
                            <span className="info_value">
                                {is_annotated_version_of.field_value.resource_name.field_value[metadataLanguage] || is_annotated_version_of.field_value.resource_name.field_value[Object.keys(is_annotated_version_of.field_value.resource_name.field_value)[0]]}
                                {is_annotated_version_of.field_value.version && is_annotated_version_of.field_value.version?.field_value}
                            </span>
                    }
                </div>
            }
            {is_aligned_version_of &&
                <div className="padding5">
                    <Typography className="bold-p--id">{is_aligned_version_of.field_label[metadataLanguage] || is_aligned_version_of.field_label["en"]}</Typography>
                    {
                        commonParser.getFullMetadata(is_aligned_version_of.field_value.full_metadata_record) ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={commonParser.getFullMetadata(is_aligned_version_of.field_value.full_metadata_record).internalELGUrl}>
                                    <span className="info_value">
                                        {is_aligned_version_of.field_value.resource_name.field_value[metadataLanguage] || is_aligned_version_of.field_value.resource_name.field_value[Object.keys(is_aligned_version_of.field_value.resource_name.field_value)[0]]}
                                        {is_aligned_version_of.field_value.version && is_aligned_version_of.field_value.version?.field_value}
                                    </span>
                                </Link>
                            </div>
                            :
                            <span className="info_value">
                                {is_aligned_version_of.field_value.resource_name.field_value[metadataLanguage] || is_aligned_version_of.field_value.resource_name.field_value[Object.keys(is_aligned_version_of.field_value.resource_name.field_value)[0]]}
                                {is_aligned_version_of.field_value.version && is_aligned_version_of.field_value.version?.field_value}
                            </span>
                    }
                </div>
            }

            {is_converted_version_of && is_converted_version_of.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_converted_version_of.field_label[metadataLanguage] || is_converted_version_of.field_label["en"]}</Typography>
                {is_converted_version_of && is_converted_version_of.field_value.length > 0 && is_converted_version_of.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version?.field_value || "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {time_coverage && time_coverage.field_value.length > 0 && <div className="padding5"> <Typography className="bold-p--id">{time_coverage.field_label[metadataLanguage] || time_coverage.field_label["en"]}</Typography>
                {time_coverage && time_coverage.field_value.length > 0 && time_coverage.field_value.map((item, index) => {
                    let cov = item[metadataLanguage] || item[Object.keys(item)[0]];
                    return <div key={index}>
                        <span className="info_value">{cov}</span>
                    </div>
                })}</div>}
            {geographic_coverage && geographic_coverage.field_value.length > 0 && <div className="padding5"> <Typography className="bold-p--id">{geographic_coverage.field_label[metadataLanguage] || geographic_coverage.field_label["en"]}</Typography>
                {geographic_coverage && geographic_coverage.field_value.length > 0 && geographic_coverage.field_value.map((item, index) => {
                    let cov = item[metadataLanguage] || item[Object.keys(item)[0]];
                    return <div key={index}>
                        <span className="info_value">{cov}</span>
                    </div>
                })}</div>}
            {register && register.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{register.field_label[metadataLanguage] || register.field_label["en"]}</Typography>
                {register.field_value.map((item, index) => {
                    let reg = item[metadataLanguage] || item[Object.keys(item)[0]];
                    return <div key={index}>
                        <span className="info_value">{reg}</span>
                    </div>
                })}  </div>}
        </>
    }
}