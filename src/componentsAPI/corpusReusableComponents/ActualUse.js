import React from "react";
import Typography from '@material-ui/core/Typography';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';
import DocumentIdentifier from "../CommonComponents/DocumentIdentifier";
import { ReactComponent as DocIcon } from "./../../assets/elg-icons/document.svg";
export default class ActualUse extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        return <>
            {data.described_entity.field_value.actual_use && data.described_entity.field_value.actual_use.field_value.length > 0 &&
                <Typography variant="h3" className="title-links">{data.described_entity.field_value.actual_use.field_label[metadataLanguage] || data.described_entity.field_value.actual_use.field_label["en"]}</Typography>}
            {
                data.described_entity.field_value.actual_use && data.described_entity.field_value.actual_use.field_value.map((item, index) => {
                    let used_in_applicationArray = item.used_in_application ? item.used_in_application.field_value.map(used_in_applicationItem => (used_in_applicationItem.label ? (used_in_applicationItem.label[metadataLanguage] || used_in_applicationItem.label[Object.keys(used_in_applicationItem.label)[0]]) : used_in_applicationItem.value)) : [];
                    let used_in_application_label = item.used_in_application ? (item.used_in_application.field_label[metadataLanguage] || item.used_in_application.field_label["en"]) : "";
                    let has_outcomeArray = item.has_outcome ? item.has_outcome.field_value.map((has_outcomeItem) => {
                        let resource_name = has_outcomeItem.resource_name ? (has_outcomeItem.resource_name.field_value[metadataLanguage] || has_outcomeItem.resource_name.field_value[Object.keys(has_outcomeItem.resource_name.field_value)[0]]) : "";
                        let version = has_outcomeItem.version ? has_outcomeItem.version.field_value : "";
                        let full_metadata_record = commonParser.getFullMetadata(has_outcomeItem.full_metadata_record);
                        return { resource_name: resource_name, version: version, full_metadata_record: full_metadata_record }
                    }) : [];
                    const has_outcome_label = item.has_outcome ? (item.has_outcome.field_label[metadataLanguage] || item.has_outcome.field_label["en"]) : "";
                    let usage_projectArray = item.usage_project ? item.usage_project.field_value.map(usage_projectItem => {
                        let projectName = usage_projectItem.project_name ? (usage_projectItem.project_name.field_value[metadataLanguage] || usage_projectItem.project_name.field_value[Object.keys(usage_projectItem.project_name.field_value)[0]]) : "";
                        let website = usage_projectItem.website ? usage_projectItem.website.field_value.map(websiteItem => websiteItem) : [];
                        let full_metadata_record = commonParser.getFullMetadata(usage_projectItem.full_metadata_record);
                        return { projectName: projectName, website: website, full_metadata_record: full_metadata_record };
                    }) : [];
                    let usage_project_label = item.usage_project ? (item.usage_project.field_label[metadataLanguage] || item.usage_project.field_label["en"]) : "";
                    let usage_reportArray = item.usage_report ? item.usage_report.field_value.map((usage_reportItem, usage_reportIndex) => {
                        //let title = usage_reportItem.title && (usage_reportItem.title.field_value[metadataLanguage] || usage_reportItem.title.field_value[Object.keys(usage_reportItem.title.field_value)[0]]);
                        //let identifiers = usage_reportItem.document_identifier.field_value.filter(identifier => identifier.document_identifier_scheme.label["en"] !== "ELG") || [];
                        let bibliographic_record = usage_reportItem.bibliographic_record ? usage_reportItem.bibliographic_record.field_value : "";                         
                        return <div key={usage_reportIndex} className="padding5">
                        <div className="flex-centered">
                            <span className="mr-05"><DocIcon /></span>
                            <span>
                                <DocumentIdentifier data={usage_reportItem} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </span>
                        </div>
                        <div>
                            {bibliographic_record && <>
                                <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                        </div>
                    </div>
                    }) : [];
                    let usage_report_label = item.usage_report ? (item.usage_report.field_label[metadataLanguage] || item.usage_report.field_label["en"]) : "";
                    let actual_use_details = item.actual_use_details ? (item.actual_use_details.field_value[metadataLanguage] || item.actual_use_details.field_value[Object.keys(item.actual_use_details.field_value)[0]]) : "";
                    let actual_use_details_label = item.actual_use_details ? (item.actual_use_details.field_label[metadataLanguage] || item.actual_use_details.field_label["en"]) : "";
                    return <div key={index}  className="bottom-border mt-1">
                        {used_in_applicationArray.length > 0 && <div className="padding5">
                            <Typography className="bold-p--id">{used_in_application_label}: </Typography>
                            {used_in_applicationArray.map((application, applicationIndex) => <div className="info_value" key={applicationIndex}>{application} </div>)}
                        </div>
                        }
                        {has_outcomeArray.length > 0 &&
                            <div className="padding5">
                                <Typography className="bold-p--id">{has_outcome_label}</Typography>
                                <span className="info_value">{has_outcomeArray.map((outcome, outcomeIndex) =>
                                    <div className="info_value" key={outcomeIndex}>
                                        {outcome.full_metadata_record ?
                                            (<div className="padding5 internal_url">
                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                <Link to={outcome.full_metadata_record.internalELGUrl}>
                                                    {outcome.resource_name}
                                                </Link>
                                            </div>)
                                            :
                                            (<div>{outcome.resource_name} ({outcome.version})</div>)
                                        }

                                    </div>)}
                                </span>
                            </div>
                        }
                        {usage_projectArray.length > 0 &&
                            <div className="padding5">
                                <Typography className="bold-p--id">{usage_project_label} </Typography>
                                <div className="info_value">{usage_projectArray.map((project, projectIndex) =>
                                    <div className=" padding5 info_value" key={projectIndex}>
                                        {project.full_metadata_record ?
                                            <div className="padding5 internal_url"> <span><NavIcon className="xsmall-icon mr-05" /></span> <Link to={project.full_metadata_record.internalELGUrl}>{project.projectName} </Link></div>
                                            : <></>}
                                        { (project.website && project.website.length>0) ?  <>
                                            <span className="info_value">{project.projectName}</span> 
                                            <WebsitesWithIcon websiteArray={project.website} />
                                        </> 
                                        
                                        : <span className="info_value">{project.projectName}</span>}</div>)
                                        }
                                </div>
                            </div>
                        }
                        {usage_reportArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{usage_report_label}</Typography><span className="info_value">{usage_reportArray.map(item => item)}</span></div>}
                        {
                            actual_use_details && <div className="padding5">
                                <Typography className="bold-p--id">{actual_use_details_label} </Typography>
                                <span className="info_value">{actual_use_details}</span>
                            </div>
                        }
                         
                    </div>
                })
            }
        </>
    }
}