import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withRouter, Link } from "react-router-dom";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Chip from '@material-ui/core/Chip';
import ResourceEntityType from './CommonComponents/ResourceEntityType';
import Analytics from "./CommonComponents/Analytics";
import ResourceStatus from "./CommonComponents/ResourceStatus";
import HostedAtElg from "./CommonComponents/HostedAtElg";
import ResourceUnderConstruction from "./CommonComponents/ResourceUnderConstruction";
import { ELG_CATALOGUE_PREV_PAGE, getUrlToLandingPageFromCatalogue } from "../config/constants";

class ResourceComponentCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = { expanded: false, expandedLang: false, expandedLicence: false, keycloak: props.keycloak };
    }

    togglexpanded = () => {
        this.setState({ expanded: !this.state.expanded })
    }
    togglexpandedLang = () => {
        this.setState({ expandedLang: !this.state.expandedLang })
    }
    togglexpandedLicence = () => {
        this.setState({ expandedLicence: !this.state.expandedLicence })
    }

    render() {
        const { resource } = this.props;
        const { expanded, expandedLang, expandedLicence } = this.state;
        const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
        //const cataloguePageUrl = window.location.href;
        const cataloguePageUrl = window.location.pathname + window.location.search
        return (
            <React.Fragment>

                <Grid item xs={12} sm={6} className="CardItems" >
                    <div className="CardSection">
                        <Card className="ResourceCard">
                            {/*<ResourceDownloadable size={resource.size} xSize={12} position={"right ribbon label card-view"} />*/}
                            <CardHeader className="ResourceCardHeader"
                                title={<>
                                    <span className="ResourceListTitle" onClick={() => { sessionStorage.setItem(ELG_CATALOGUE_PREV_PAGE, cataloguePageUrl); }}>
                                        <Link to={
                                            {
                                                pathname: url2LandingPage,
                                                "detail": resource.detail
                                            }
                                        }>
                                            {resource.resource_name} 
                                        </Link>
                                    </span>
                                    {resource.resource_type ===null && resource.resource_short_name && resource.resource_short_name.length>0 && resource.resource_short_name.map((short_name, index) => <span key={index} className="bold-p--id">{short_name}</span>)}
                                    {resource.version !== "undefined" && resource.version !== null && <span><Typography variant="body2" className="grey--font">version: {resource.version}</Typography></span>}
                                    </>
                                }
                                subheader={<div className="mt1"><ResourceStatus status={resource.status} xSize={12} keycloak={this.state.keycloak} />
                                    <ResourceEntityType resource_type={resource.resource_type} entity_type={resource.entity_type} align={"left"} />
                                    <ResourceUnderConstruction under_construction={resource.under_construction} xSize={12} justifyContent={"center"} alignItems={"flex-end"} />
                                    {resource.resource_type && <HostedAtElg functional_service={resource.functional_service ? resource.functional_service : false} size={resource.size} resource_type={resource.resource_type} justifyContent={"center"} alignItems={"flex-end"} xSize={12} />}

                                </div>
                                }
                            />
                            <CardContent>
                                <div>
                                    {expanded ? (
                                        <div>
                                            <Typography variant="body2" className="search-results__description">
                                                {this.props.resource.description}<span className="ExpandButton" onClick={this.togglexpanded} > ... </span>
                                            </Typography>
                                        </div>
                                    ) : (
                                        <div>
                                            <Typography variant="body2" className="search-results__description">
                                                {this.props.resource.description.substring(0, 220)}{this.props.resource.description.length > 220 ? <span className="ExpandButton" onClick={this.togglexpanded} > ... </span> : void 0}  </Typography>
                                        </div>
                                    )
                                    }
                                    <div>
                                        {resource.keywords && resource.keywords.length > 0 ?
                                            (<div>{resource.keywords.length > 1 ? <span className="ResourceCardLabel">Keywords: </span> : <span className="ResourceCardLabel">Keyword: </span>}

                                                {resource.keywords.slice(0, 4).map((keyword, index) =>
                                                    <Chip key={index} size="medium" label={keyword} className="ChipTagGrey" />
                                                )}</div>)
                                            : void 0}
                                    </div>
                                    <div style={{ paddingTop: "0.5em" }}>
                                        {resource.languages && resource.languages.length > 0 ?
                                            (<div>
                                                {resource.languages.length > 1 ? <span className="ResourceCardLabel">Languages: </span> : <span className="ResourceCardLabel">Language: </span>}
                                                {expandedLang ? (
                                                    <div style={{ display: "inline-block" }}>
                                                        {resource.languages.map((lang, index) => <Chip key={index} size="small" label={lang} className="ChipTagLanguage" />)}
                                                        <span className="ExpandButton yellow--font" onClick={this.togglexpandedLang} > ... </span>
                                                    </div>
                                                ) : (
                                                    <div style={{ display: "inline-block" }}>
                                                        {resource.languages.slice(0, 5).map((lang, index) => <Chip key={index} size="small" label={lang} className="ChipTagLanguage" />)}
                                                        {resource.languages.length > 5 ?
                                                            <span className="ExpandButton yellow--font" onClick={this.togglexpandedLang} > ... </span>
                                                            : void 0}
                                                    </div>
                                                )
                                                }

                                            </div>)
                                            : void 0
                                        }
                                    </div>
                                    <div>
                                        {resource.licences && resource.licences.length > 0 ?
                                            (<div style={{ display: "flex", alignItems: "baseline" }}>
                                                {resource.licences.length > 1 ? <span className="ResourceCardLabel">Licences:</span> : <span className="ResourceCardLabel">Licence:</span>}
                                                {expandedLicence ? (
                                                    <div style={{ display: "inline-block" }}>
                                                        {resource.licences.map((license, index) => <Chip key={index} size="small" label={license} className="ChipTagLicence" />)}
                                                        <span className="ExpandButton grey--font" onClick={this.togglexpandedLicence} > ... </span>
                                                    </div>
                                                ) : (
                                                    <div style={{ display: "inline-block" }}>
                                                        {resource.licences.slice(0, 2).map((license, index) => <Chip key={index} size="small" label={license} className="ChipTagLicence" />)}
                                                        {resource.licences.length > 2 ?
                                                            <span className="ExpandButton grey--font" onClick={this.togglexpandedLicence} > ... </span>
                                                            : void 0}
                                                    </div>
                                                )
                                                }

                                            </div>) : void 0
                                        }
                                    </div>

                                    {resource.licences && resource.licences.length === 0 ? (<div style={{ display: "flex", alignItems: "baseline" }}>
                                        {resource.access_rights && resource.access_rights.length > 0 && <span className="bold-p--id">Access rights :<span>&nbsp;</span></span>}
                                        {resource.access_rights && resource.access_rights.length > 0 && resource.access_rights.map((right, index) => <Chip key={index} size="small" label={right} className="ChipTagLicence" />)}
                                    </div>) : void 0
                                    }

                                    {(resource.other_versions_count && resource.other_versions_count !== 0 && resource.harvested_versions_exceeding_limit === false) ? <div>
                                        {resource.other_versions_count > 1 && resource.harvested_versions_exceeding_limit === false ? <span className="date_value grey--font italic">{resource.other_versions_count} more versions exist for this record</span> : <span className="date_value grey--font italic">{resource.other_versions_count} more version exists for this record</span>}
                                    </div> : void 0}

                                    {(resource.other_versions_count && resource.other_versions_count !== 0 && resource.harvested_versions_exceeding_limit === true) ? <div>
                                        {resource.other_versions_count > 1 && resource.harvested_versions_exceeding_limit === true ? <span className="date_value grey--font italic">{resource.other_versions_count} more versions exist for this record (and more versions at source)</span> : <span className="date_value grey--font italic">{resource.other_versions_count} more version exists for this record (and more versions at source)</span>}
                                    </div> : void 0}

                                    {(resource.potential_duplicate && resource.potential_duplicate.length > 0) ? <div style={{ display: "flex", alignItems: "baseline" }}>
                                        <span className="date_value grey--font italic">  {resource.potential_duplicate.length > 1 ? "Potential duplicate with records:" :  "Potential duplicate with record:"} <span>&nbsp;</span></span>
                                        {resource.potential_duplicate.map((item, index) => <span> <a href={item.url} target="_blank" rel="noopener noreferrer" className="date_value"> {item.resource_name} </a> <span className="date_value">({item.source}) </span> <span>&nbsp;</span> </span>)}
                                    </div> : void 0}

                                    {(resource.duplicate && resource.duplicate.length > 0) ? <div style={{ display: "flex", alignItems: "baseline" }}>
                                        <span className="date_value grey--font italic"> Alternative sources : <span>&nbsp;</span></span>
                                        {resource.duplicate.map((item, index) => <span> <a href={item.url} target="_blank" rel="noopener noreferrer" className="date_value"> {item.resource_name} </a> <span className="date_value">({item.source}) </span> <span>&nbsp;</span> </span>)}
                                    </div> : void 0}

                                </div>

                                <CardActions >

                                    <Analytics views={resource.views} under_construction={resource.under_construction} size={resource.size} downloads={resource.downloads} resource_type={resource.resource_type} entity_type={resource.entity_type} service_execution_count={resource.service_execution_count} xSize={12} />
                                </CardActions>



                            </CardContent>


                        </Card>
                    </div>

                </Grid>




            </React.Fragment>
        );

    }
}
export default withRouter(ResourceComponentCard);