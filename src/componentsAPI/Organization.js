import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { TabContent, TabPane, } from "reactstrap";
//import { ReactComponent as BackIcon } from "./../assets/elg-icons/navigation-arrows-left-1.svg";
import GoToCatalogue from "./CommonComponents/GoToCatalogue";
import HelmetMetaData from "./CommonComponents/HelmetMetaData";
//custom components
import organizationParser from "../parsers/organizationParser";
import NavigationTabs from "./organizationReusableComponents/NavigationTabs";
import ResourceHeader from "./CommonComponents/ResourceHeader";
import ResourceActor from "./CommonComponents/ResourceActor";
import ResourceLifeCyrcleInfo from "./CommonComponents/ResourceLifeCyrcleInfo";
import DescriptionRichText from "./CommonComponents/DescriptionRichText";
import Keywords from './CommonComponents/Keywords';
import OrganizationInfo from './organizationReusableComponents/OrganizationInfo';
import OrganizationRoles from './organizationReusableComponents/OrganizationRoles';
import IsReplacedWith from './organizationReusableComponents/IsReplacedWith';
import PhoneFax from './organizationReusableComponents/PhoneFax';
import AddressSet from './organizationReusableComponents/AddressSet';
import MemberOfAssociation from './organizationReusableComponents/MemberOfAssociation';
import IsDivision from './organizationReusableComponents/IsDivision';
import ReverseRelations from "./CommonComponents/ReverseRelations";
import ProgressBar from "./CommonComponents/ProgressBar";
import OrganizationImage from "../assets/elg-icons/buildings-modern.svg";
import ExportMetadata from "./CommonComponents/ExportMetadata";
//import ShareMetadata from "./CommonComponents/ShareMetadata";
//constants 
import { SERVER_API_METADATARECORD, RETRIEVE_RECORD_INFO, getLogoutUrl } from "../config/constants";
import commonParser from "../parsers/CommonParser";
import RecordStats from "./CommonComponents/RecordStats";

export default class Organization extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', metadataLanguage: '', tab: '1', pk: "",
            loading: true, number_of_failed_loads: 0,
            isOwner: false, isLegalValidator: false, isMetadataValidator: false, isTechnicalValidator: false, source: null
        };
    }

    componentDidMount() {
        this.getMetadataResourceDetail();
        this.setState({ number_of_failed_loads: 0 });
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        clearTimeout(this.timeOutId);
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    updateRecord = () => {
        this.getMetadataResourceDetail();
    }

    retrieveRecordInfo = (id, source) => {
        if (this.props.keycloak && this.props.keycloak.authenticated) {
            axios.get(RETRIEVE_RECORD_INFO(id), { cancelToken: source.token })
                .then(res => {
                    const { curator = false, legal_validator = false, metadata_validator = false, technical_validator = false } = res.data;
                    this.setState({ isOwner: curator, isLegalValidator: legal_validator, isMetadataValidator: metadata_validator, isTechnicalValidator: technical_validator });
                })
                .catch(err => console.log(err));
        }
    }

    getMetadataResourceDetail = () => {
        const id = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const url = SERVER_API_METADATARECORD(id);
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        this.retrieveRecordInfo(id, source);
        axios.get(url, { cancelToken: source.token })
            .then(res => {
                this.setState(res);
                const lang = Object.keys(res.data.described_entity.field_value.organization_name.field_value).includes("en") ? "en" : Object.keys(res.data.described_entity.field_value.organization_name.field_value)[0];
                this.setState({ metadataLanguage: lang, pk: id, source: null, loading: false, number_of_failed_loads: 0 });
            }).catch(err => {
                console.log(err);
                this.setState({ source: null, loading: false });
            });
    }

    componentDidUpdate(prevProps, prevState) {
        const thisId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const prevId = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.id;
        if (thisId !== prevId) {
            this.setState({ data: '', metadataLanguage: '', tab: '1' });
            this.getMetadataResourceDetail();
        }
    }

    reloadPage = () => {
        const number_of_failed_loads = this.state.number_of_failed_loads;
        if (number_of_failed_loads < 3) {
            this.setState({ number_of_failed_loads: number_of_failed_loads + 1 });
            this.getMetadataResourceDetail();
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        } else {
            this.timeOutId = setTimeout(() => { window.location = getLogoutUrl() }, 5000)
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        }
    }

    render() {
        if (this.props.keycloak && !this.props.keycloak.authenticated) {
            if (commonParser.redirectUnregisteredUsersToKeycloak(this.props.location)) {
                this.props.keycloak.login();
            }
        }

        if (this.state.loading) {
            return <ProgressBar />
        } else {
            if (this.state.data) {
            } else {
                return this.reloadPage();
            }
        }

        const { data, metadataLanguage } = this.state;
        //const languages = [];// ["english", "spanish"];
        const resourceName = organizationParser.getOrganizationName(data, metadataLanguage);
        const Logo = organizationParser.getLogo(data);
        const shortnameArray = organizationParser.getOrganizationShortName(data, metadataLanguage);
        const OrganizationAltName = organizationParser.getOrganizationAltName(data, metadataLanguage);
        const resource_type = null;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        const description = organizationParser.getOrganizationBio(data, metadataLanguage);
        const keywords = organizationParser.getKeywords(data, metadataLanguage);
        const domainKeywords = organizationParser.getDomainKeywords(data, metadataLanguage);
        const LtAreaKeywords = organizationParser.getLtAreaKeywords(data, metadataLanguage);
        const disciplines = organizationParser.getDisciplineKeywords(data, metadataLanguage);
        const servicesOffered = organizationParser.getservicesOfferedKeywords(data, metadataLanguage);
        const SocialMedia = organizationParser.getSocialMedia(data);
        const OrganizationRolesArray = organizationParser.getOrganizationRole(data, metadataLanguage);
        const OrganizationLegalStatus = organizationParser.getOrganizationLegalStatus(data, metadataLanguage);
        const Startup = organizationParser.getStartup(data);
        const CountryOfRegistration = organizationParser.getCountryOfRegistration(data);
        const OrganizationWebsites = organizationParser.getOrganizationWebsites(data);
        const OrganizationEmails = organizationParser.getOrganizationEmails(data);
        const TelephoneNumbers = organizationParser.getTelephoneNumbers(data);
        const FaxNumbers = organizationParser.getFaxNumbers(data);
        const head_office_address = organizationParser.get_head_office_AddressSet(data);
        const other_office_address = organizationParser.get_other_office_AddressSet(data);
        //const divisionArray = organizationParser.getHasDivision(data);
        //labels
        const OrganizationRoleLabel = organizationParser.getOrganizationRoleLabel(data, metadataLanguage);
        const OrganizationWebsitesLabel = organizationParser.getOrganizationWebsitesLabel(data, metadataLanguage);
        const OrganizationEmailsLabel = organizationParser.getOrganizationEmailsLabel(data, metadataLanguage);
        const StartupLabel = organizationParser.getStartupLabel(data, metadataLanguage);
        const OrganizationLegalStatusLabel = organizationParser.getOrganizationLegalStatusLabel(data, metadataLanguage);
        const CountryOfRegistrationLabel = organizationParser.getCountryOfRegistrationLabel(data, metadataLanguage);

        const KeywordsLabel = organizationParser.getKeywordsLabel(data, metadataLanguage);
        const DomainKeywordsLabel = organizationParser.getDomainKeywordsLabel(data, metadataLanguage);
        const SocialMediaLabel = organizationParser.getSocialMediaLabel(data, metadataLanguage);
        const LtAreaKeywordsLabel = organizationParser.getLtAreaKeywordsLabel(data, metadataLanguage);
        const DisciplineKeywordsLabel = organizationParser.getDisciplineKeywordsLabel(data, metadataLanguage);
        const servicesOfferedKeywordsLabel = organizationParser.getservicesOfferedKeywordsLabel(data, metadataLanguage);
        const TelephoneNumbersLabel = organizationParser.getTelephoneNumbersLabel(data, metadataLanguage);
        const FaxNumbersLabel = organizationParser.getFaxNumbersLabel(data, metadataLanguage);
        const head_office_address_label = organizationParser.get_head_office_address_label(data, metadataLanguage);
        const other_office_address_label = organizationParser.get_other_office_address_label(data, metadataLanguage);
        //const HasDivisionLabel = organizationParser.getHasDivisionLabel(data, metadataLanguage);
        const { reverse_relations, has_division } = data.described_entity.field_value || [];
        const { for_information_only = false } = data.management_object || {};
        const { tombstone = false } = data.management_object || {};
        return <div>
            <HelmetMetaData data={data}
                resourceName={resourceName}
                description={description}
                keywords={keywords}
                domainKeywords={domainKeywords}
                subjectKeywords={null}
                intentedKeywords={null}
                corpus_subclass={null}
                languages={null}
                resourceShortName={null}
                for_information_only={for_information_only}
                ltAreaKeywordsArray={LtAreaKeywords}
                disciplines={disciplines}
                servicesOffered={servicesOffered}
                OrganizationRolesArray={OrganizationRolesArray}
                ld_subclass={null}
                lcr_subclass={null}
                type="organization"
                pk={this.state.pk}
            />

            <div className="search-top">
                <GoToCatalogue />
            </div>

            <Container maxWidth="xl">
                <div className="metadata-main-card-container">
                    <ResourceLifeCyrcleInfo key={data.management_object.status} status={data.management_object.status} keycloak={this.props.keycloak} />
                    <ResourceHeader
                        key={data.management_object ? 'header' + JSON.stringify(data.management_object) : 'header' + this.state.pk}
                        {...this.state}
                        logo={Logo}
                        title={resourceName}
                        short_name={null}
                        version={null}
                        version_Date={null}
                        proxied={null}
                        resource_type={resource_type}
                        tombstone={tombstone}
                        entity_type={entity_type} shortnameArray={shortnameArray} default_image={OrganizationImage} under_construction={false} for_information_only={for_information_only} keycloak={this.props.keycloak} data={data} pk={this.state.pk} updateRecord={this.updateRecord}
                        keywordsArray={null} KeywordsLabel={null}
                        disciplineArray={null} DisciplineKeywordsLabel={null}
                        domainKeywordsArray={null} DomainKeywordsLabel={null}
                        ltAreaKeywordsArray={null} LtAreaKeywordsLabel={null}
                        servicesOfferedArray={null} servicesOfferedKeywordsLabel={null}
                        languagesArray={null} languagesLabel={null}
                        subjectKeywordsArray={null} subjectKeywordsLabel={null}
                        intentedKeywordsArray={null} intentedKeywordsLabel={null}
                        lcr_subclass={null} lcr_subclassLabel={null}
                        corpus_subclass={null} corpus_subclassLabel={null}
                        ld_subclass={null} ld_subclassLabel={null} description={description} url_type="organization" status={data.management_object.status} />
                </div>
                {(!tombstone || tombstone === "F") &&
                    <div className="tab-pane-container">
                        <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} reverse_relations={reverse_relations} has_division={has_division} />
                        <TabContent activeTab={this.state.tab}>
                            <TabPane tabId="1">
                                <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
                                    <Grid item xs={12} sm={8} md={8} className="search-results-main" >
                                        <DescriptionRichText description={description} />
                                        <Keywords
                                            keywordsArray={keywords} KeywordsLabel={KeywordsLabel}
                                            disciplineArray={disciplines} DisciplineKeywordsLabel={DisciplineKeywordsLabel}
                                            domainKeywordsArray={domainKeywords} DomainKeywordsLabel={DomainKeywordsLabel}
                                            ltAreaKeywordsArray={LtAreaKeywords} LtAreaKeywordsLabel={LtAreaKeywordsLabel}
                                            servicesOfferedArray={servicesOffered} servicesOfferedKeywordsLabel={servicesOfferedKeywordsLabel}
                                            languagesArray={null} languagesLabel={null}
                                            subjectKeywordsArray={null} subjectKeywordsLabel={null}
                                            intentedKeywordsArray={null} intentedKeywordsLabel={null}
                                            lcr_subclass={null} lcr_subclassLabel={null}
                                            corpus_subclass={null} corpus_subclassLabel={null}
                                            ld_subclass={null} ld_subclassLabel={null} />


                                        <MemberOfAssociation data={data} metadataLanguage={metadataLanguage} />
                                        <IsDivision data={data} metadataLanguage={metadataLanguage} />

                                        {/*<Relations data={data} metadataLanguage={metadataLanguage} />*/}

                                    </Grid>

                                    <Grid item xs={12} sm={4} md={4} className="MetadataSidebar" >


                                        {/*data.management_object.status === "p" && <ShareMetadata shareUrl={`${BASE_URL}catalogue/organization/${this.state.pk}`} title={resourceName} description={description} logo={Logo} />*/}
                                        <RecordStats pk={this.state.pk} />
                                        {data.described_entity.field_value.is_replaced_with &&
                                            <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
                                                <IsReplacedWith data={this.state.data} metadataLanguage={metadataLanguage} />
                                            </div>
                                        }

                                        {this.state.data.described_entity.field_value.replaces_organization && <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
                                            <ResourceActor data={this.state.data.described_entity.field_value.replaces_organization} metadataLanguage={metadataLanguage} />
                                        </div>
                                        }


                                        <div className="ActionsButtonArea">
                                            <OrganizationInfo OrganizationWebsitesLabel={OrganizationWebsitesLabel}
                                                OrganizationEmailsLabel={OrganizationEmailsLabel}
                                                StartupLabel={StartupLabel}
                                                OrganizationLegalStatusLabel={OrganizationLegalStatusLabel}
                                                CountryOfRegistrationLabel={CountryOfRegistrationLabel}
                                                altnameArray={OrganizationAltName} websiteArray={OrganizationWebsites}
                                                emailArray={OrganizationEmails} OrganizationLegalStatus={OrganizationLegalStatus}
                                                Startup={Startup} CountryOfRegistration={CountryOfRegistration} SocialMedia={SocialMedia} SocialMediaLabel={SocialMediaLabel}
                                                metadataLanguage={metadataLanguage} described_entity={this.state.data.described_entity.field_value}
                                                identifier_name={"organization_identifier"} identifier_scheme={"organization_identifier_scheme"} />
                                            <OrganizationRoles rolesArray={OrganizationRolesArray} OrganizationRoleLabel={OrganizationRoleLabel} />
                                            <PhoneFax TelephoneNumbersLabel={TelephoneNumbersLabel} FaxNumbersLabel={FaxNumbersLabel} telephonenumbers={TelephoneNumbers} faxnumbers={FaxNumbers} />
                                            <AddressSet AddressSetLabel={head_office_address_label} addressArray={head_office_address} metadataLanguage={metadataLanguage} />
                                            <AddressSet AddressSetLabel={other_office_address_label} addressArray={other_office_address} metadataLanguage={metadataLanguage} />

                                        </div>

                                        <ExportMetadata pk={this.state.pk} under_construction={false} for_information_only={for_information_only} keycloak={this.props.keycloak} data={this.state.data} name={resourceName} {...this.state} />

                                    </Grid>
                                </Grid>

                            </TabPane>
                            {
                                (reverse_relations && has_division) && <>
                                    <TabPane tabId="2">
                                        <ReverseRelations data={data} metadataLanguage={metadataLanguage} />
                                    </TabPane>
                                    <TabPane tabId="3">
                                        <ResourceActor data={has_division} metadataLanguage={metadataLanguage} />
                                    </TabPane></>
                            }

                            {(reverse_relations && !has_division) && <TabPane tabId="2">
                                <ReverseRelations data={data} metadataLanguage={metadataLanguage} />
                            </TabPane>}

                            {(!reverse_relations && has_division) && <TabPane tabId="2">
                                <ResourceActor data={has_division} metadataLanguage={metadataLanguage} />
                            </TabPane>}




                        </TabContent>
                    </div>}
            </Container>

        </div >
    }

}