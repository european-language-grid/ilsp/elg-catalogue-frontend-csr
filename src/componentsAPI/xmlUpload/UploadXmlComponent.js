
import React from 'react';
import { Redirect, withRouter } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
import Container from '@material-ui/core/Container';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SimpleTabPanel from "../CustomVerticalTabs/SimpleTabPanel";
import DashboardAppBar from "../../DashboardComponents/DashboardAppBar";
import SingleXmlUpload from "./SingleXmlUpload";
import BatchXmlUpload from "./BatchXmlUpload";
import ValidateXml from "./ValidateXml";

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const TABS = [{ display_value: "VALIDATE XML FILES", url_value: "validate-xml-files" }, { display_value: "UPLOAD SINGLE ITEM", url_value: "upload-single-file" }, { display_value: "UPLOAD MULTIPLE ITEMS", url_value: "upload-multiple-files" }];

class UploadXmlComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: 0 }
  }

  componentDidMount() {
    let currentTab = this.props && this.props.match && this.props.match.params && this.props.match.params.tab;
    const tabIndex = TABS.findIndex(item => item.url_value.toLowerCase() === currentTab);
    this.setState({ activeTab: tabIndex >= 0 ? tabIndex : 0 });
  }

  componentDidUpdate(prevProps, prevState) {
    let currentTab = this.props && this.props.match && this.props.match.params && this.props.match.params.tab;
    let prevTab = prevProps.match && prevProps.match.params && prevProps.match.params.tab;
    if (currentTab !== prevTab) {
      const tabIndex = TABS.findIndex(item => item.url_value.toLowerCase() === currentTab);
      this.setState({ activeTab: tabIndex >= 0 ? tabIndex : 0 });
    }
  }

  PageHeader = () => {
    return <div>
      <Helmet>
        <title>ELG - Upload </title>
      </Helmet>
      <DashboardAppBar {...this.props} />
      <div className="editor-container-white">
        <Container maxWidth="lg">
          <div className="empty"></div>
          <Typography className="dashboard-title-box pb-3">
            Upload records
          </Typography>
        </Container>
      </div>
    </div>
  }


  render() {
    if (!this.props.keycloak || !this.props.keycloak.authenticated) {
      return <Redirect to="/" />
    }

    return (
      <>
        {this.PageHeader()}
        <Container maxWidth="lg" className="upload botomMargin boxed mt2">

          <Tabs value={this.state.activeTab} onChange={this.toggleTab} aria-label="simple tabs example" className="simple-tabs-forms">
            {TABS.map((value, index) => <Tab key={value.display_value} label={value.display_value} {...a11yProps(index)} onClick={() => { this.props.history.push(`/upload/${value.url_value}/`); }} />)}
          </Tabs>

          <SimpleTabPanel value={this.state.activeTab} index={0} className="upload--outer">
            <ValidateXml {...this.props} helmet_value={TABS[0].display_value} />
          </SimpleTabPanel>

          <SimpleTabPanel value={this.state.activeTab} index={1} className="upload--outer">
            <SingleXmlUpload {...this.props} helmet_value={TABS[1].display_value} />
          </SimpleTabPanel>

          <SimpleTabPanel value={this.state.activeTab} index={2} className="upload--outer">
            <BatchXmlUpload {...this.props} helmet_value={TABS[2].display_value} />
          </SimpleTabPanel>

        </Container>
      </>
    );
  }
}

export default withRouter(UploadXmlComponent);