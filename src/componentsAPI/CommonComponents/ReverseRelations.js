import React from "react";
import { withRouter, Link } from "react-router-dom";
import commonParser from "../../parsers/CommonParser";
import Typography from '@material-ui/core/Typography';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";

class ReverseRelations extends React.Component {

    constructor(props) {
        super(props);
        this.getURL = this.getURL.bind(this);
        this.switchStatement = this.switchStatement.bind(this);
    }

    switchStatement(type, id) {
        let redirectURL = "";
        switch (type) {
            case "LanguageDescription": redirectURL = `/ld/${id}`; break;
            case "LexicalConceptualResource": redirectURL = `/lcr/${id}`; break;
            case "ToolService": redirectURL = `/tool-service/${id}`; break;
            case "Corpus": redirectURL = `/corpus/${id}`; break;
            case "Project": redirectURL = `/project/${id}`; break;
            case "Organization": redirectURL = `/organization/${id}`; break;
            default: void 0;
        }
        return redirectURL;
    }

    getURL(relation) {
        const { link, entity_type, lr_type } = relation;
        let id = link.substring(0, link.lastIndexOf("/"))
        id = id.substring(id.lastIndexOf("/") + 1);
        let redirectURL = "";
        if (lr_type) {
            redirectURL = this.switchStatement(lr_type, id);
        } else if (entity_type) {
            redirectURL = this.switchStatement(entity_type, id);
        }
        //this.props.history.push({ pathname: redirectURL, state: { "detail": link } });
        return redirectURL;
    }

    render() {
        const { data, metadataLanguage, showTitle } = this.props;
        if (!data) {
            return <></>;
        }
        const reverseRelations = commonParser.getReverseRelations(data.described_entity.field_value.reverse_relations, metadataLanguage);
        return <>
            {reverseRelations && showTitle && reverseRelations.length > 0 ? <Typography variant="h3"> {data.described_entity.field_value.reverse_relations.field_label[metadataLanguage] || data.described_entity.field_value.reverse_relations.field_label["en"]} </Typography> : ""}
            {reverseRelations && reverseRelations.map((relation, relationIndex) => {
                return <div key={relationIndex} id={relationIndex} className="padding5">
                    <Typography className="bold-p--id"> {relation.relationLabel} </Typography>
                    {relation.relationsValuesArray.map((relationValue, relationValueIndex) => {
                        if (relationValue.link) {
                            return <div key={relationValueIndex} className="padding5">
                                <Link className="relations_button internal_url" to={this.getURL(relationValue)} target="_self" rel="noopener noreferrer">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>    <span>{relationValue.resourceName} {relationValue.version ? <span>({relationValue.version})</span> : ""}</span>
                                </Link>
                            </div>
                        } else {
                            return <div key={relationValueIndex} className="info_value">
                                <span>{relationValue.resourceName} {relationValue.version ? <span>({relationValue.version})</span> : ""}</span>
                            </div>
                        }
                    })}
                </div>

            })}
        </ >
    }
}

export default withRouter(ReverseRelations);