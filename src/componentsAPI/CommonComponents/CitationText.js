import React from "react";
import Typography from '@material-ui/core/Typography';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Linkify from 'react-linkify';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import Tooltip from '@material-ui/core/Tooltip';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default class CitationText extends React.Component {
    constructor(props) {
        super(props);

        this.state = { copySuccess: '', copySuccessAllVersions: '' }
    }

    copyToClipboard = (e) => {
        this.textArea.select();
        this.textArea.setSelectionRange(0, 99999); /* For mobile devices */
        document.execCommand('copy');
        this.setState({ copySuccess: 'Copied!' });
    };

    copyToClipboardAllVersions = (e) => {
        this.inputArea.select();
        this.inputArea.setSelectionRange(0, 99999); /* For mobile devices */
        document.execCommand('copy');
        this.setState({ copySuccessAllVersions: 'Copied!' });
    };

    render() {
        const { citationText, citation_all_versions, functional_service, has_data, tombstone } = this.props;
        //const {citationText_label,citation_all_versions_label }= this.props;
        if (!citationText) {
            return <div></div>
        }
        return <>            
            
            {(citationText && tombstone === "Y") && 
                <div className="padding15">
                    <Typography className="bold-p--id">Deleted resource</Typography> 
                    <Linkify><span className="citation_text">{citationText}</span> </Linkify>
                    <span style={{ opacity: "0", width: "1px" }}>
                        <span><textarea readOnly style={{ opacity: "0", width: "1px", height: "1px" }}
                            ref={(textarea) => this.textArea = textarea}
                            defaultValue={citationText}
                        /></span>
                    </span>
                    <span><Tooltip title={this.state.copySuccess || "Copy"}><button onClick={this.copyToClipboard} className="inner-actions-link-no--border--teal"> <FileCopyIcon  className="small-icon"  /> </button></Tooltip> </span>
                </div>
            } 

            {(citationText && tombstone === "T") &&
                <div className="padding15">
                    <Typography className="bold-p--id">Unpublished resource</Typography>
                    <Linkify><span className="citation_text">{citationText}</span> </Linkify>
                    <span style={{ opacity: "0", width: "1px" }}>
                        <span><textarea readOnly style={{ opacity: "0", width: "1px", height: "1px" }}
                            ref={(textarea) => this.textArea = textarea}
                            defaultValue={citationText}
                        /></span>
                    </span>
                    <span><Tooltip title={this.state.copySuccess || "Copy"}><button onClick={this.copyToClipboard} className="inner-actions-link-no--border--teal"> <FileCopyIcon className="small-icon" /> </button></Tooltip> </span>
                </div>
            }

            {(citation_all_versions && (!tombstone || tombstone === "F")) && <Accordion className="CitationBox">
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography variant="h3" className="title-links">Cite</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    {(citationText && (!tombstone || tombstone === "F")) &&
                        <div className="padding15">
                            {functional_service === true || has_data === true ? <Typography className="bold-p--id">Cite resource</Typography> : <Typography className="bold-p--id">Cite metadata record</Typography>}
                            <Linkify> <span className="citation_text">{citationText}</span> </Linkify>
                            <span style={{ opacity: "0", width: "1px" }}>
                                <span><textarea readOnly style={{ opacity: "0", width: "1px", height: "1px" }}
                                    ref={(textarea) => this.textArea = textarea}
                                    defaultValue={citationText}
                                /></span>
                            </span>
                            <span><Tooltip title={this.state.copySuccess || "Copy"}><button onClick={this.copyToClipboard} className="inner-actions-link-no--border--teal"> <FileCopyIcon className="small-icon" /> </button></Tooltip> </span>
                        </div>
                    }

                    {(citation_all_versions && (!tombstone || tombstone === "F")) &&
                        <div className="padding15">
                            <Typography className="bold-p--id">Cite all versions</Typography>
                            {/*<Typography className="bold-p--id">{citation_all_versions_label}</Typography>*/}
                            <Linkify><span className="citation_text">{citation_all_versions}</span></Linkify>
                            <span style={{ opacity: "0", width: "1px" }}>
                                <span><textarea readOnly style={{ opacity: "0", width: "1px", height: "1px" }}
                                    ref={(textarea) => this.inputArea = textarea}
                                    defaultValue={citation_all_versions}
                                /></span>
                            </span>
                            <span><Tooltip title={this.state.copySuccessAllVersions || "Use this to cite all versions; the PID resolves to the latest version of the resource."}><button onClick={this.copyToClipboardAllVersions} className="inner-actions-link-no--border--teal"> <FileCopyIcon fontSize="small" className="blue" /> </button></Tooltip> </span>
                        </div>
                    }
                </AccordionDetails>
            </Accordion>
            }


        </>
    }
}