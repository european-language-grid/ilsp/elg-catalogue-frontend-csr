import React from "react";
import Typography from '@material-ui/core/Typography';
//import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
//import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import { ReactComponent as PersonOutlineIcon } from "./../../assets/elg-icons/single-neutral-id-card-4.svg";
import { ReactComponent as AccountBalanceIcon } from "./../../assets/elg-icons/buildings-modern.svg";
import { ReactComponent as GroupIcon } from "./../../assets/elg-icons/multiple-users-2.svg";
//import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import WebsitesWithIcon from './WebsitesWithIcon';
import EmailsWithIcon from './EmailsWithIcon';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import ResourceIsDivision from "./ResourceIsDivision";
import GeneralIdentifier from "./GeneralIdentifier";
export default class ResourceActorSingleItem extends React.Component {

    renderActor(actor_type, data, metadataLanguage) {
        switch (actor_type) {
            case "Person":
                let given_name = data.given_name.field_value[metadataLanguage] || data.given_name.field_value[Object.keys(data.given_name.field_value)[0]];
                let surname = data.surname.field_value[metadataLanguage] || data.surname.field_value[Object.keys(data.surname.field_value)[0]];
                let email = data.email && data.email.field_value.length > 0 && (data.email.field_value.map(email => email) || []);
                given_name = (given_name === "unspecified" || given_name === undefined || given_name === "undefined") ? "" : given_name;
                surname = (surname === "unspecified" || surname === undefined || surname === "undefined") ? "" : surname;
                return <div className="padding5">
                    <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                        <Grid item xs={12}>
                            <Card className="actor_type_card">
                                <CardHeader avatar={<Avatar aria-label="actor-type" className="actor-avatar"> <PersonOutlineIcon className="small-icon" /> </Avatar>}
                                    title={<><Typography variant="h6">{given_name} {surname}
                                     <GeneralIdentifier data={data} identifier_name={"personal_identifier"} identifier_scheme={"personal_identifier_scheme"} metadataLanguage={metadataLanguage} /></Typography></>}
                                    subheader={ (email && email.length > 0) ? <EmailsWithIcon emailArray={email} /> : <span>{" "} </span>}
                                />
                            </Card>
                        </Grid>
                    </Grid>
                </div>

            case "Organization":
                let organization_name = data.organization_name.field_value[metadataLanguage] || data.organization_name.field_value[Object.keys(data.organization_name.field_value)[0]];
                let website = data.website ? data.website.field_value : [];
                let full_metadata_record = commonParser.getFullMetadata(data.full_metadata_record);
                return <div className="padding5">
                    <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                        <Grid item xs={12}>
                            <Card className="actor_type_card">
                                <CardHeader avatar={<Avatar aria-label="actor-type" className="actor-avatar"> <AccountBalanceIcon className="small-icon" /> </Avatar>}
                                    title={full_metadata_record ?
                                        <div className="internal_url">
                                            <Link to={full_metadata_record.internalELGUrl}>
                                                {organization_name}
                                            </Link>
                                        </div>
                                        :
                                        <Typography variant="h6" >{organization_name}</Typography>
                                    }
                                    subheader={<WebsitesWithIcon websiteArray={website} />}
                                />
                                {data.is_division_of && <CardContent>
                                    <ResourceIsDivision data={data.is_division_of} metadataLanguage={metadataLanguage} />
                                </CardContent>
                                }

                            </Card>
                        </Grid>
                    </Grid>
                </div>

            case "Group":
                let group_organization_name = data.organization_name.field_value[metadataLanguage] || data.organization_name.field_value[Object.keys(data.organization_name.field_value)[0]];
                let group_website = data.website ? data.website.field_value : [];
                return (
                    <div className="padding5">
                        <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                            <Grid item xs={12}>
                                <Card className="actor_type_card">
                                    <CardHeader avatar={<Avatar aria-label="actor-type" className="actor-avatar"> <GroupIcon className="small-icon" /> </Avatar>}
                                        title={<Typography variant="h6" >{group_organization_name}</Typography>}
                                        subheader={<WebsitesWithIcon websiteArray={group_website} />}
                                    />
                                </Card>
                            </Grid>
                        </Grid>
                    </div>
                )
            default:
                return <span></span>


        }
    }

    render() {
        const { data, metadataLanguage } = this.props;

        if (!data) {
            return <div></div>
        }

        let actor_type = data.actor_type.field_value;
        //console.log(actor_type)

        return <>
            {this.renderActor(actor_type, data, metadataLanguage)}
        </>
    }
}