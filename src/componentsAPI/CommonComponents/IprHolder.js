import React from "react";
import Typography from '@material-ui/core/Typography';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import Grid from '@material-ui/core/Grid';
import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';

export default class IprHolder extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;//data here should be data.described_entity
        return <div>
            {data.ipr_holder && data.ipr_holder.length > 0 &&
                <Typography variant="h3" className="title-links">Ipr Holder</Typography>}
            {
                data.ipr_holder && data.ipr_holder.map((holder, index) => {
                    if (holder.actor_type === "Person") {
                        let surname = holder.surname[metadataLanguage] || holder.surname[Object.keys(holder.surname)[0]];
                        let given_name = holder.given_name[metadataLanguage] || holder.given_name[Object.keys(holder.given_name)[0]];
                        //let email = holder.email || null;
                        given_name = (given_name === "unspecified" || given_name === undefined || given_name === "undefined") ? "" : given_name;
                        surname = (surname === "unspecified" || surname === undefined || surname === "undefined") ? "" : surname;
                        return <div className="padding15" key={index}>
                            <Grid container spacing={1}>
                                <Grid item xs={2} sm={2}>
                                    <PersonOutlineIcon className="general-icon general-icon--rounded" />
                                </Grid>
                                <Grid item xs={9} sm={9}>
                                    <div key={index}>
                                        <Typography variant="h4" className="bold-titles">{given_name} {surname}</Typography>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    } if (holder.actor_type === "Organization") {
                        let organization_name = holder.organization_name[metadataLanguage] || holder.organization_name[Object.keys(holder.organization_name)[0]];
                        let website = holder.website || [];
                        return <div className="padding15" key={index}>
                            <Grid container spacing={1}>
                                <Grid item xs={2} sm={2}>
                                    <AccountBalanceIcon className="general-icon general-icon--rounded" />
                                </Grid>
                                <Grid item xs={9} sm={9}>
                                    <div key={index}>
                                        <div><Typography variant="h4" className="bold-titles">{organization_name}</Typography></div>
                                        <WebsitesWithIcon websiteArray={website} />
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    } else if (holder.actor_type === "Group") {
                        const organization_name = holder.organization_name[metadataLanguage] || holder.organization_name[Object.keys(holder.organization_name)[0]];
                        const website = holder.website || [];
                        return (
                            <div className="padding15" key={index}>
                                <Grid container spacing={2}>
                                    <Grid item xs={2} sm={2}>
                                        <AccountBalanceIcon className="general-icon general-icon--rounded" />
                                    </Grid>
                                    <Grid item xs={9} sm={9}>
                                        <div key={index}>
                                            <div><Typography variant="h4" className="bold-titles">{organization_name}</Typography></div>
                                            <WebsitesWithIcon websiteArray={website} />
                                        </div>
                                    </Grid>
                                </Grid>

                            </div>
                        )
                    } else {
                        return <span key={index} ></span>
                    }
                })
            }
        </div>
    }
}