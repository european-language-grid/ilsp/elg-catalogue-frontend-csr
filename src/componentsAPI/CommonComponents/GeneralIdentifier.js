
import React from "react";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

export default class GeneralIdentifier extends React.Component {
    constructor(props) {
        super(props);
        this.state = { expanded: false};
    }

    togglexpanded = () => {
        this.setState({ expanded: !this.state.expanded })
    }

    render() {
        const { data, identifier_name, identifier_scheme } = this.props;
        const { expanded } = this.state;
        const filteredIdentifiers = data[identifier_name] && data[identifier_name].field_value.filter(item => item && item[identifier_scheme] && item[identifier_scheme].label && item[identifier_scheme].label["en"] !== "ELG");
        if (filteredIdentifiers && !filteredIdentifiers.length) {
            return <div></div>
        }
        return <div className="padding5">
            {/*
                filteredIdentifiers && filteredIdentifiers.length && <div><span className="bold-p--id">{data[identifier_name].field_label[metadataLanguage] || data[identifier_name].field_label["en"]}</span></div>
            */}

            {expanded ? (
                <div>
                    {data[identifier_name] && data[identifier_name].field_value.map((IdentifierItem, index) => {
                        let identifier = IdentifierItem[identifier_scheme].label["en"] !== "ELG" ? IdentifierItem[identifier_scheme].label["en"] : "";//filter out elg identifiers 
                        let value = IdentifierItem.value.field_value || "";
                        return <div key={index}>
                            {identifier && value.includes('http') && <div>
                                <a href={value} target="_blank" rel="noopener noreferrer" className="info_url"> {value} </a> ({identifier})</div>}
                            {identifier && !value.includes('http') && <div className="info_value">{value} ({identifier})  </div>}
                        </div>
                    })}
                    < span className="ExpandButton teal--font" onClick={this.togglexpanded} > <ExpandLessIcon className="teal--font" /> </span>
                </div>
            ) : (
                    <div>
                        {data[identifier_name] && data[identifier_name].field_value.slice(0, 5).map((IdentifierItem, index) => {
                        let identifier = IdentifierItem[identifier_scheme].label["en"] !== "ELG" ? IdentifierItem[identifier_scheme].label["en"] : "";//filter out elg identifiers 
                        let value = IdentifierItem.value.field_value || "";
                        return <div key={index}>
                            {identifier && value.includes('http') && <div>
                                <a href={value} target="_blank" rel="noopener noreferrer" className="info_url"> {value} </a> ({identifier})</div>}
                            {identifier && !value.includes('http') && <div className="info_value">{value} ({identifier})  </div>}
                        </div>
                    })}
                        {data[identifier_name] && data[identifier_name].field_value.length > 5 ?
                            <span className="ExpandButton teal--font" onClick={this.togglexpanded} > <ExpandMoreIcon className="teal--font" /> </span>
                            : void 0}
                    </div>
                )
            }



            {/*data[identifier_name] && data[identifier_name].field_value.map((IdentifierItem, index) => {
                let identifier = IdentifierItem[identifier_scheme].label["en"] !== "ELG" ? IdentifierItem[identifier_scheme].label["en"] : "";//filter out elg identifiers 
                let value = IdentifierItem.value.field_value || "";                
                return <div key={index}>
                    {identifier && value.includes('http') && <div>
                        <a href={value} target="_blank" rel="noopener noreferrer" className="info_url"> {value} </a> ({identifier})</div>}
                    {identifier && !value.includes('http') && <div className="info_value">{value} ({identifier})  </div>}                   
                </div>
            })
        */}
        </div>
    }
}




