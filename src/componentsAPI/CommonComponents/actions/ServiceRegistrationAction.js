import React from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import MenuItem from '@material-ui/core/MenuItem';
import messages from "../../../config/messages";
import { /*IS_METADATA_VALIDATOR, IS_SUPERVISOR, */IS_ADMIN } from "../../../config/constants";
import ServiceRegistration from "../../../DashboardComponents/single_actions/ServiceRegistration";

export default class ServiceRegistrationAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null, deleted = false } = this.props.data.management_object || {};
        //const technical_valid = this.props.data.management_object.technical_valid;
        const is_functional_service = this.props.data.management_object.functional_service;
        //isTechnicalValidator+ingested+functional_service=true

        // If its not null, service registreation is completed
        //const is_registered = this.props.data.service_info != null;

        if (deleted) {
            this.setState({ isAuthorized: false }, this.showServiceRegistrationButton);
        }
        //specs changed to show service registration action even if it is completed 
        else if (this.props.isTechnicalValidator && ((status === "g") && (is_functional_service))) {
            this.setState({ isAuthorized: true }, this.showServiceRegistrationButton);
            // Admins can change the service registration even when its completed, so we dont check (!is_registered)
        } else if (IS_ADMIN(keycloak) && ((status === "g") && (is_functional_service))) {
            this.setState({ isAuthorized: true }, this.showServiceRegistrationButton);
        } else {
            this.setState({ isAuthorized: false }, this.showServiceRegistrationButton);
        }

    }

    handleAction = (event, action, actionIndex) => {
        this.setState({ showServiceRegistrationDialog: true });
    }

    hideServiceRegistration = () => {
        this.setState({ showServiceRegistrationDialog: false });
        this.props.updateRecord();
    }



    showServiceRegistrationButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (this.props.landing_page === false || this.props.landing_page === true) {
            //show action at landing page. If landing_page is undefined then we are rendering at myItems, myValidation, mySupervisors views
        } else if (this.props.tab !== undefined && this.props.tab !== 'myval') {
            return;
        }
        if (!this.props.showServiceRegistrationActionButton) {
            this.props.setVisibilityOfServiceRegistration();
        }
    }

    getLanguageResource = () => {
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            return lr_type;
        } else {
            return entity_type;
        }
    }

    getResourceName = () => {

        return this.props.data.described_entity.field_value.resource_name.field_value["en"];

    }

    getIsRegisteredService = () => {

        return this.props.data.described_entity.field_value.resource_name.field_value["en"];

    }


    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showServiceRegistrationActionButton) {
            return <></>
        }

        const actions = [{ primary: `${messages.validator_action_service_registration}`, secondary: `${messages.validator_action_service_registration_secondary}` }];
        return <div>
            {
                this.state.showServiceRegistrationDialog && <ServiceRegistration
                    {...this.props}
                    resource={
                        {
                            "id": this.props.data.pk,
                            "resource_name": this.getResourceName(),//this.props.data.described_entity.field_value.resource_name.field_value["en"],
                            "resource_type": this.getLanguageResource(),
                            //"validator_notes": this.getIsRegisteredService(),
                        }
                    }
                    hideServiceRegistration={this.hideServiceRegistration}
                    type='service_registration' />
            }

            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <AssignmentTurnedInIcon className="ActionsIcon small-icon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}