import React from "react";
import {
    ADMIN_PERMITTED_ROLES, UPLOAD_XML_PERMITTED_ROLES, EDITOR_PERMITTED_ROLES,
    AUTHENTICATED_KEYCLOAK_USER_ROLES, VALIDATOR_PERMITTED_ROLES
    //CLAIM_PERMITTED_ROLES 
} from "../../../config/constants";
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import EditAction from "./EditAction";
import IngestAction from "./IngestAction";
import DeactivateVersionAction from "./DeactivateVersionAction";
import DeleteAction from "./DeleteAction";
import AskForUnpublishAction from "./AskForUnpublishButton";
import MetadataValidateAction from "./MetadataValidateAction"
import LegalValidateAction from "./LegalValidateAction"
import TechnicalValidateAction from "./TechnicalValidateAction"
import CreateVersionAction from "./CreateVersionAction";
import CreateCopyAction from "./CreateCopyAction";
import ServiceRegistrationAction from "./ServiceRegistrationAction";

import Tooltip from '@material-ui/core/Tooltip';

export default class ResourceActions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            anchorEl: null,
            isOwner: props.isOwner || null,
            isLegalValidator: (props.isLegalValidator === false || props.isLegalValidator === true) ? props.isLegalValidator : null,
            isMetadataValidator: (props.isMetadataValidator === false || props.isMetadataValidator === true) ? props.isMetadataValidator : null,
            isTechnicalValidator: (props.isTechnicalValidator === false || props.isTechnicalValidator === true) ? props.isTechnicalValidator : null,
            UserRoles: [],
            showEditActionButton: false,
            showIngestActionButton: false,
            showClaimActionButton: false,
            showDeleteActionButton: false,
            showAskForUnpublishActionButton: false,
            showMetadataValidateActionButton: false,
            showTechnicalValidateActionButton: false,
            showLegalValidateActionButton: false,
            showUploadDataActionButton: false,
            showCreateVersionActionButton: false,
            showServiceRegistrationActionButton: false,
            showCreateCopyActionButton: false,
            showDeactivateVersionButton: false
        }
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        ADMIN_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        UPLOAD_XML_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        EDITOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        VALIDATOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        return isAuthorized;
    }

    handleClick = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: false });
    }

    setVisibilityOfEdit = () => {
        this.setState({ showEditActionButton: true });
    }

    setVisibilityOfUploadData = () => {
        this.setState({ showUploadDataActionButton: true });
    }

    setVisibilityOfMetadataValidate = () => {
        this.setState({ showMetadataValidateActionButton: true });
    }

    setVisibilityOfLegalValidate = () => {
        this.setState({ showLegalValidateActionButton: true });
    }

    setVisibilityOfTechnicalValidate = () => {
        this.setState({ showTechnicalValidateActionButton: true });
    }

    setVisibilityOfServiceRegistration = () => {
        this.setState({ showServiceRegistrationActionButton: true });

    }

    setVisibilityOfCreateVersion = () => {
        this.setState({ showCreateVersionActionButton: true });
    }

    setVisibilityOfCreateCopy = () => {
        this.setState({ showCreateCopyActionButton: true });
    }

    setVisibilityOfIngest = () => {
        this.setState({ showIngestActionButton: true });
    }

    setVisibilityOfDelete = () => {
        this.setState({ showDeleteActionButton: true });
    }

    setVisibilityOfAskForUnpublish = () => {
        this.setState({ showAskForUnpublishActionButton: true });
    }

    setVisibilityOfDeactivateVersionButton = () => {
        this.setState({ showDeactivateVersionButton: true });
    }


    render() {
        const { keycloak, className } = this.props;
        if (!keycloak || (keycloak && !Boolean(keycloak.authenticated))) {
            return <></>
        }
        if (!this.isAuthorizedToView(this.state.UserRoles)) {
            return <></>
        }
        return (
            <>
                {(this.state.showEditActionButton ||
                    this.state.showIngestActionButton ||
                    this.state.showDeleteActionButton ||
                    this.state.showAskForUnpublishActionButton ||
                    this.state.showUploadDataActionButton ||
                    this.state.showCreateCopyActionButton ||
                    this.state.showCreateVersionActionButton ||
                    this.state.showMetadataValidateActionButton ||
                    this.state.showLegalValidateActionButton ||
                    this.state.showTechnicalValidateActionButton ||
                    this.state.showServiceRegistrationActionButton ||
                    this.state.showDeactivateVersionButton
                ) && <div className="actions-area">
                        <Tooltip title="Actions"><Button className={className ? className : "inner-link-outlined--teal"} endIcon={<ArrowDropDownIcon />} aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClick}>Actions</Button></Tooltip>
                    </div>
                }<Menu
                    id="main action buttons"
                    elevation={0}
                    anchorEl={this.state.anchorEl}
                    getContentAnchorEl={null}
                    keepMounted
                    open={Boolean(this.state.anchorEl)}
                    onClose={this.handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <EditAction isOwner={this.state.isOwner} keycloak={keycloak} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showEditActionButton={this.state.showEditActionButton} setVisibilityOfEdit={this.setVisibilityOfEdit} />
                    <MetadataValidateAction isMetadataValidator={this.state.isMetadataValidator} isTechnicalValidator={this.state.isTechnicalValidator} keycloak={keycloak} landing_page={this.props.landing_page} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showMetadataValidateActionButton={this.state.showMetadataValidateActionButton} setVisibilityOfMetadataValidate={this.setVisibilityOfMetadataValidate} tab={this.props.tab} />
                    <LegalValidateAction isLegalValidator={this.state.isLegalValidator} keycloak={keycloak} landing_page={this.props.landing_page} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showLegalValidateActionButton={this.state.showLegalValidateActionButton} setVisibilityOfLegalValidate={this.setVisibilityOfLegalValidate} tab={this.props.tab} />
                    <TechnicalValidateAction isTechnicalValidator={this.state.isTechnicalValidator} isMetadataValidator={this.state.isMetadataValidator} keycloak={keycloak} landing_page={this.props.landing_page} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showTechnicalValidateActionButton={this.state.showTechnicalValidateActionButton} setVisibilityOfTechnicalValidate={this.setVisibilityOfTechnicalValidate} tab={this.props.tab} />
                    <ServiceRegistrationAction isTechnicalValidator={this.state.isTechnicalValidator} keycloak={keycloak} landing_page={this.props.landing_page} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showServiceRegistrationActionButton={this.state.showServiceRegistrationActionButton} setVisibilityOfServiceRegistration={this.setVisibilityOfServiceRegistration} tab={this.props.tab} />
                    <CreateVersionAction isOwner={this.state.isOwner} keycloak={keycloak} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showCreateVersionActionButton={this.state.showCreateVersionActionButton} setVisibilityOfCreateVersion={this.setVisibilityOfCreateVersion} />
                    <CreateCopyAction isOwner={this.state.isOwner} keycloak={keycloak} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showCreateCopyActionButton={this.state.showCreateCopyActionButton} setVisibilityOfCreateCopy={this.setVisibilityOfCreateCopy} />
                    <IngestAction isOwner={this.state.isOwner} keycloak={keycloak} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showIngestActionButton={this.state.showIngestActionButton} setVisibilityOfIngest={this.setVisibilityOfIngest} />
                    <DeactivateVersionAction isOwner={this.state.isOwner} keycloak={keycloak} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showDeactivateVersionButton={this.state.showDeactivateVersionButton} setVisibilityOfDeactivateVersionButton={this.setVisibilityOfDeactivateVersionButton} />
                    <AskForUnpublishAction isOwner={this.state.isOwner} keycloak={keycloak} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showAskForUnpublishActionButton={this.state.showAskForUnpublishActionButton} setVisibilityOfAskForUnpublish={this.setVisibilityOfAskForUnpublish} />
                    <DeleteAction isOwner={this.state.isOwner} keycloak={keycloak} updateRecord={this.props.updateRecord} handleClose={this.handleClose} data={this.props.data} pk={this.props.pk} showDeleteActionButton={this.state.showDeleteActionButton} setVisibilityOfDelete={this.setVisibilityOfDelete} />
                </Menu>
            </>


        );
    }

}