import React from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
//import { TOOL_SERVICE, CORPUS, LD, LCR, PROJECT, ORGANIZATION } from "../../../config/constants";
import { PROJECT, ORGANIZATION } from "../../../config/constants";
import MenuItem from '@material-ui/core/MenuItem';
import messages from "../../../config/messages";
import CopyRecord from "../../../DashboardComponents/single_actions/CopyRecord";
import { IS_ADMIN } from "../../../config/constants";

export default class CreateCopyAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, showCreatCopyDialog: false, source: null };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    checkIfUserIsOwner = () => {
        const { status = null, deleted = false } = this.props.data.management_object || {};
        if (deleted) {
            this.setState({ isAuthorized: false, source: null }, this.showCreateCopyButton);
        } else if ((this.props.isOwner || IS_ADMIN(this.props.keycloak)) && (status !== "d")) {
            this.setState({ isAuthorized: true, source: null }, this.showCreateCopyButton);
        } else {
            this.setState({ isAuthorized: false, source: null }, this.showCreateCopyButton);
        }
    }

    handleAction = (event, action, actionIndex) => {
        this.setState({ showCreateCopyDialog: true });
    }

    hideCreateCopy = () => {
        this.setState({ showCreateCopyDialog: false });
        this.props.updateRecord();
    }

    showCreateCopyButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showCreateCopyActionButton) {
            this.props.setVisibilityOfCreateCopy();
        }
    }

    getLanguageResource = () => {
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            return lr_type;
        } else {
            return entity_type;
        }
    }

    getResourceName = () => {
        const resourceType = this.getLanguageResource();
        if (resourceType === PROJECT) {
            return this.props.data.described_entity.field_value.project_name.field_value["en"];
        } else if (resourceType === ORGANIZATION) {
            return this.props.data.described_entity.field_value.organization_name.field_value["en"];
        } else {
            return this.props.data.described_entity.field_value.resource_name.field_value["en"];
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showCreateCopyActionButton) {
            return <></>
        }

        const actions = [{ primary: messages.editor_action_create_copy_primary, secondary: messages.editor_action_create_copy_secondary }];

        return <div>
            {
                this.state.showCreateCopyDialog && <CopyRecord
                    resource={
                        {
                            "id": this.props.data.pk,
                            "resource_name": this.getResourceName(),//this.props.data.described_entity.field_value.resource_name.field_value["en"],
                            "resource_type": this.getLanguageResource()
                        }
                    }
                    hideCopyRecord={this.hideCreateCopy} />
            }
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <CreateNewFolderIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}