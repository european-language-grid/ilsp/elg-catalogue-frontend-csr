import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { IS_ADMIN, IS_CONTENT_MANAGER } from "../../../config/constants";
import { EDITOR_BULK_DELETE } from "../../../config/editorConstants";
import MenuItem from '@material-ui/core/MenuItem';
import { toast } from "react-toastify";
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import messages from "../../../config/messages";

class DeleteAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false, deletion_reason: "", showDialog: false, error: null };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null, deleted = false } = this.props.data.management_object || {};
        if (deleted) {
            this.setState({ isAuthorized: false }, this.showDeleteButton);
        } else if (this.props.isOwner && ((status === "i") || (status === "d"))) {
            this.setState({ isAuthorized: true }, this.showDeleteButton);
        } else if (IS_ADMIN(keycloak) && (status !== "p")) {
            this.setState({ isAuthorized: true }, this.showDeleteButton);
        } else if (IS_CONTENT_MANAGER(keycloak) && (status !== "p")) {
            this.setState({ isAuthorized: true }, this.showDeleteButton);
        } else {
            this.setState({ isAuthorized: false }, this.showDeleteButton);
        }
    }

    deleteRecord = () => {
        this.setState({ disable: true });
        axios.patch(EDITOR_BULK_DELETE(this.props.pk), { reason: this.state.deletion_reason }).then((res) => {
            toast.success("Record Deleted successfully.", { autoClose: 3500 });
            this.setState({ disable: true });
            if (this.props?.history?.location?.pathname?.includes("myitems")) {
                this.props.updateRecord();
            } else {
                this.props.history.push('/myitems');
            }
            this.props.handleClose();
        }).catch((err) => {
            toast.error("Delete action failed", { autoClose: 3500 });
            let errData = '';
            try {
                errData = err.response.data;
            } catch (err) {
                errData = 'The request has failed. Please contact the ELG team.'
            }
            this.setState({ disable: false, error: errData });
            this.props.handleClose();
        });
        return "";
    }

    handleAction = (event, action, actionIndex) => {
        const { keycloak } = this.props;
        if (IS_ADMIN(keycloak)) {
            this.setState({ showDialog: true });
        } else {
            this.setState({ showDialog: true });
        }
    }

    disableDisplay = (event, reason) => {
        if (reason === 'backdropClick') {// reason !== 'escapeKeyDown'
            return;
        }
        this.setState({ showDialog: false, deletion_reason: "", error: null });
        this.props.handleClose();
    }

    adminDeletionWithReasoning = (reasoning) => {
        return this.state.showDialog &&
            <Dialog open={this.state.showDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth='sm' fullWidth>
                <DialogTitle>You are about to delete this record are you sure?</DialogTitle>
                {reasoning && <DialogContent>
                    <DialogContentText component={"div"}>
                        <TextField className="pb-3 wd-100"
                            //type="text"
                            minRows={10}
                            multiline={true}
                            required={true}
                            label={"Reason for deletion"}
                            placeholder={"Reason for deletion"}
                            variant="outlined"
                            helperText={
                                <span>Please specify the reason for deletion.</span>
                            }
                            value={this.state.deletion_reason}
                            onChange={(e) => { this.setState({ deletion_reason: e.target.value }) }}
                        />
                    </DialogContentText>
                </DialogContent>
                }
                <DialogContentText component={"div"}>
                    {this.state.error && this.ShowErrorMessage(this.state.error)}
                </DialogContentText>
                <DialogActions>
                    <Button
                        color="primary"
                        onClick={() => this.disableDisplay()}
                        autoFocus
                    >
                        Cancel
                    </Button>
                    <Button
                        color="primary"
                        disabled={(reasoning && !this.state.deletion_reason) || this.state.disable}
                        onClick={() => this.deleteRecord()}
                    >
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
    }

    ShowErrorMessage(error) {
        return <div>
            {error && <div >
                <h3>Error</h3>
                <div className="boxed">
                    <Paper elevation={13} >
                        <code>
                            <pre id="special">
                                {JSON.stringify(error)}
                            </pre>
                        </code>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    showDeleteButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showDeleteActionButton) {
            this.props.setVisibilityOfDelete();
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showDeleteActionButton) {
            return <></>
        }
        const actions = [{ primary: `${messages.editor_action_landing_page_delete_primary}`, secondary: `${messages.editor_action_landing_page_delete_secondary}` }];
        if (this.state.showDialog) {
            return IS_ADMIN(this.props.keycloak) ? this.adminDeletionWithReasoning(true) : this.adminDeletionWithReasoning(false);
        }
        return <div>
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <DeleteForeverIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}
export default withRouter(DeleteAction);