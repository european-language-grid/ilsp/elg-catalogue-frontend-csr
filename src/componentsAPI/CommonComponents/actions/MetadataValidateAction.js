import React from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import MenuItem from '@material-ui/core/MenuItem';
import messages from "../../../config/messages";
import { /*IS_METADATA_VALIDATOR, IS_SUPERVISOR, */IS_ADMIN } from "./../../../config/constants";
import ValidateRecord from "../../../DashboardComponents/single_actions/ValidateRecord";
import { PROJECT, ORGANIZATION } from "../../../config/constants";

export default class MetadataValidateAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null, deleted = false } = this.props.data.management_object || {};
        const metadata_valid = this.props.data.management_object.metadata_valid;
        const technically_valid = this.props.data.management_object.technically_valid;
        if (deleted) {
            this.setState({ isAuthorized: false }, this.showMetadataValidateButton);
        } else if (this.props.isMetadataValidator && !this.props.isTechnicalValidator && ((status === "g") && (!metadata_valid && technically_valid))) {//if technically_valid=false do not show this action. Instead show technically/metadata action
            this.setState({ isAuthorized: true }, this.showMetadataValidateButton);
        } else if (IS_ADMIN(keycloak) && ((status === "g") && (!metadata_valid && technically_valid))) {
            this.setState({ isAuthorized: true }, this.showMetadataValidateButton);
        } else {
            this.setState({ isAuthorized: false }, this.showMetadataValidateButton);
        }

    }

    handleAction = (event, action, actionIndex) => {
        this.setState({ showCreateValidationDialog: true });
    }

    hideValidateRecord = () => {
        this.setState({ showCreateValidationDialog: false });
        this.props.updateRecord();
    }



    showMetadataValidateButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (this.props.landing_page === false || this.props.landing_page === true) {
            //show action at landing page. If landing_page is undefined then we are rendering at myItems, myValidation, mySupervisors views
        } else if (this.props.tab !== undefined && this.props.tab !== 'myval') {
            return;
        }
        if (!this.props.showMetadataValidateActionButton) {
            this.props.setVisibilityOfMetadataValidate();
        }
    }

    getLanguageResource = () => {
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            return lr_type;
        } else {
            return entity_type;
        }
    }

    getResourceName = () => {
        const resourceType = this.getLanguageResource();
        if (resourceType === PROJECT) {
            return this.props.data.described_entity.field_value.project_name.field_value["en"];
        } else if (resourceType === ORGANIZATION) {
            return this.props.data.described_entity.field_value.organization_name.field_value["en"];
        } else {
            return this.props.data.described_entity.field_value.resource_name.field_value["en"];
        }
    }

    getValidatorNotes = () => {
        return this.props.data.management_object.validator_notes;
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showMetadataValidateActionButton) {
            return <></>
        }

        const actions = [{ primary: `${messages.validator_action_metadata}`, secondary: `${messages.validator_action_metadata_secondary}` }];
        return <div>
            {
                this.state.showCreateValidationDialog && <ValidateRecord
                    resource={
                        {
                            "id": this.props.data.pk,
                            "resource_name": this.getResourceName(),//this.props.data.described_entity.field_value.resource_name.field_value["en"],
                            "resource_type": this.getLanguageResource(),
                            "validator_notes": this.getValidatorNotes(),
                        }
                    }
                    hideValidateRecord={this.hideValidateRecord}
                    type='metadata' />
            }

            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <AssignmentTurnedInIcon className="ActionsIcon small-icon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}