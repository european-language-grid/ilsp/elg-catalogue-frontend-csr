import React from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { ReactComponent as EditIcon } from "./../../../assets/elg-icons/editor/pencil.svg";
import { IS_ADMIN, IS_CONTENT_MANAGER } from "../../../config/constants";
import MenuItem from '@material-ui/core/MenuItem';
import { NavLink } from "react-router-dom";
import messages from "../../../config/messages";

const ForwardNavLink = React.forwardRef((props, ref) => (
    <NavLink {...props} innerRef={ref} />
));



export const getPath = (props) => {
    const { data } = props;
    const entity_type = data.described_entity.field_value.entity_type.field_value;
    let editor_url = "";
    if (entity_type === "LanguageResource") {
        const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
        switch (lr_type) {
            case "ToolService":
                editor_url = `/create/Service/${props.pk}/`;
                break;
            case "Corpus":
                editor_url = `/create/Corpus/${props.pk}/`;
                break;
            case "LexicalConceptualResource":
                editor_url = `/create/lexical_conceptual_resource/${props.pk}/`;
                break;
            case "LanguageDescription":
                editor_url = `/create/language_description/${props.pk}/`;
                break;
            default: break;
        }
    } else {
        switch (entity_type) {
            case "Project":
                editor_url = `/create/Project/${props.pk}/`;
                break;
            case "Organization":
                editor_url = `/create/Organization/${props.pk}/`;
                break;
            default: break;
        }
    }
    return editor_url;
}

export default class EditAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null, deleted = false } = this.props.data.management_object || {};
        if (deleted) {
            this.setState({ isAuthorized: false }, this.showEditButton);
        } else if (this.props.isOwner && ((status === "i") || (status === "d"))) {
            this.setState({ isAuthorized: true }, this.showEditButton);
        } else if (IS_ADMIN(keycloak) && ((status === "i") || (status === "g") || (status === "d") || (status === "u"))) {
            this.setState({ isAuthorized: true }, this.showEditButton);
        } else if (IS_CONTENT_MANAGER(keycloak) && ((status === "i") || (status === "g") || (status === "d") || (status === "u"))) {
            this.setState({ isAuthorized: true }, this.showEditButton);
        } else {
            this.setState({ isAuthorized: false }, this.showEditButton);
        }
    }


    getPath = () => {
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        let editor_url = "";
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            switch (lr_type) {
                case "ToolService":
                    editor_url = `/create/Service/${this.props.pk}/`;
                    break;
                case "Corpus":
                    editor_url = `/create/Corpus/${this.props.pk}/`;
                    break;
                case "LexicalConceptualResource":
                    editor_url = `/create/lexical_conceptual_resource/${this.props.pk}/`;
                    break;
                case "LanguageDescription":
                    editor_url = `/create/language_description/${this.props.pk}/`;
                    break;
                default: break;
            }
        } else {
            switch (entity_type) {
                case "Project":
                    editor_url = `/create/Project/${this.props.pk}/`;
                    break;
                case "Organization":
                    editor_url = `/create/Organization/${this.props.pk}/`;
                    break;
                default: break;
            }
        }
        return editor_url;
    }

    handleAction = (event, action, actionIndex) => {
        switch (action.secondary) {
            case "Edit":
                return this.getPath();
            default: break;
        }
        this.props.handleClose();
    }

    showEditButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        /////
        /*const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            switch (lr_type) {
                case "ToolService":
                    return;
                case "Corpus":
                    return;
                case "LanguageDescription":
                    return;
                case "LexicalConceptualResource":
                    return;
                default: break;
            }
        }*/
        /////
        if (!this.props.showEditActionButton) {
            this.props.setVisibilityOfEdit();
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showEditActionButton) {
            return <></>
        }
        const actions = [{ primary: `${messages.editor_action_edit_primary}`, secondary: `${messages.editor_action_edit_secondary}` }];
        return <div>
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem
                            component={ForwardNavLink}
                            to={() => this.handleAction(null, action)}
                        >
                            <ListItem button>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <EditIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}