import React from "react";
import Typography from '@material-ui/core/Typography';

export default class Ethics extends React.Component {

    componentDidMount() {
        const { personal_data_included, personal_data_details, sensitive_data_included, sensitive_data_details, anonymized, anonymization_details } = this.props;
        const { showEthics } = this.props;
        if (personal_data_included|| personal_data_details|| sensitive_data_included||sensitive_data_details||anonymized||anonymization_details) 
        {
            showEthics ? void 0 : this.props.showEthicsFunction(true);
        }

        
    }


    render() {
        const { personal_data_included, personal_data_details, sensitive_data_included, sensitive_data_details, anonymized, anonymization_details } = this.props;
        const { personal_data_included_label, personal_data_details_label, sensitive_data_included_label, sensitive_data_details_label, anonymized_label, anonymization_details_label } = this.props;
        //const showElements = (personal_data_included !== undefined && personal_data_included !== null) || (personal_data_details!== null) || (sensitive_data_included !== undefined && sensitive_data_included !== null) ||
        //    (sensitive_data_details!== null) || (anonymized !== undefined && anonymized != null) || (anonymization_details!== null);
        //console.log(showElements)
        //if (!showElements) {
        //    return <div></div>
        //}
        if (!personal_data_included && !personal_data_details && !sensitive_data_included && !sensitive_data_details && !anonymized && !anonymization_details){
            return <div></div>
        }
        return <>

            <Typography variant="h3" className="title-links">Ethics</Typography>
            {personal_data_included !== undefined && personal_data_included !== null && <div className="padding5"><Typography className="bold-p--id">{personal_data_included_label}</Typography><span className="info_value">{`${personal_data_included}`}</span></div>}
            {personal_data_details && <div className="padding5"><Typography className="bold-p--id">{personal_data_details_label}</Typography><span className="info_value">{personal_data_details}</span></div>}
            {sensitive_data_included !== undefined && sensitive_data_included !== null && <div className="padding5"><Typography className="bold-p--id">{sensitive_data_included_label}</Typography><span className="info_value">{`${sensitive_data_included}`}</span></div>}
            {sensitive_data_details && <div className="padding5"><Typography className="bold-p--id">{sensitive_data_details_label}</Typography> <span className="info_value">{sensitive_data_details}</span></div>}
            {anonymized !== undefined && anonymized !== null && <div className="padding5"><Typography className="bold-p--id">{anonymized_label}</Typography><span className="info_value">{`${anonymized}`}</span></div>}
            {anonymization_details && <div className="padding5"><Typography className="bold-p--id">{anonymization_details_label}</Typography><span className="info_value">{anonymization_details}</span> </div>}




        </>
    }
}