
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: "#E4E9F1",
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#289AA4',
  },
}))(LinearProgress);

export default function CustomizedProgressBars(props) {
    return (<Grid container direction="row" justifyContent="flex-start" alignItems="center"  spacing={1}>         
    <Grid item sm={9}><BorderLinearProgress variant="determinate" value={props.value} /></Grid>
    <Grid item sm={3}><Typography variant="caption">{`${Math.round(
        props.value,
      )}%`}</Typography> </Grid>
      
      </Grid>
    );
}