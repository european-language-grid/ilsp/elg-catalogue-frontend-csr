import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ResourceTopFlagsLandingPage from "./ResourceTopFlagsLandingPage";
import ResourceUnderConstruction from "./ResourceUnderConstruction";
import ResourceActions from "./actions/ResourceActions";
import Claim from "./Claim";
import HostedAtElg from "./HostedAtElg";
import ResourceForInfo from "./ResourceForInfo";
import ResourceActive from "./ResourceActive";
import CitationText from "./CitationText";
import Keywords from "./Keywords";
import Tooltip from '@material-ui/core/Tooltip';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import ShareMetadata from "./ShareMetadata";
import { BASE_URL} from "./../../config/constants";
import Chip from '@material-ui/core/Chip';
//entity_type patterns for displaying default images
//if "entity_type": "LanguageResource" then 
// "lr_type": "Corpus",
// "lr_type": "ToolService",
//  "lr_type": "LexicalConceptualResource",
//if  "entity_type": "Project" then 
// "lr_type":null
//if  "entity_type": "Organization" then
// "lr_type":null

 


class ResourceHeader extends React.Component {
    render() {
        const { logo, title, short_name, version, version_Date, resource_type, entity_type, shortnameArray, default_image, version_label, under_construction, for_information_only, proxied, is_active_version, keycloak, status,
            citationText, citationText_label, citation_all_versions, citation_all_versions_label, tombstone,
            keywordsArray, languagesArray, domainKeywordsArray, subjectKeywordsArray, intentedKeywordsArray, corpus_subclass, corpus_subclassLabel,lcr_subclass, lcr_subclassLabel, KeywordsLabel, DomainKeywordsLabel, subjectKeywordsLabel, intentedKeywordsLabel,
            model_typeArray, model_type_label, model_functionArray, model_function_label, servicesOfferedArray, servicesOfferedKeywordsLabel, disciplineArray, DisciplineKeywordsLabel, ltAreaKeywordsArray, LtAreaKeywordsLabel,
            inferred_language, description, url_type, duplicate, potential_duplicate} = this.props;
            console.log(duplicate, potential_duplicate)
        return (
            <React.Fragment>
                <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={3} className="pt1">
                    <Grid item sm={8}>
                        <Grid item container sm={12}>
                            <Grid item sm={1} >
                                <div className="project-logo" style={{display: "inline-flex"}}>
                                    {/*<img width="60%" src={logo} alt="corpus logo"></img>*/}                                    
                                    {inferred_language ? <span><Tooltip title="automatically inferred"><InfoOutlinedIcon fontSize="medium" className="teal--font" /></Tooltip> </span>: <></>}
                                    <span><img width="60px" src={logo ? logo : default_image} ref={img => this.img = img} onError={() => this.img.src = default_image} alt="resource logo" className="resource_logo"/></span>
                                </div>
                            </Grid>

                            <Grid item sm={9}>
                            
                                <div className="flex"><span><Typography variant="h2" id="resource-name"> {title} </Typography></span>
                                <span className="ml-05">{duplicate && <Grid item xs={12} ><Chip size="small" label={"duplicate"} className="ChipTagPink" />  </Grid>}
                                {potential_duplicate && <Grid item xs={12} ><Chip size="small" label={"potential duplicate"} className="ChipTagPink" />  </Grid>}</span></div>
                                
                                <Typography className="acronym"> {short_name} </Typography>

                                {version && <Typography>
                                    <span className="subheading">{version_label}: </span>
                                    <span className="subheading">{version}</span>
                                    {version_Date && <span className="subheading">{` (${Intl.DateTimeFormat("en-GB").format(new Date(version_Date))})`}</span>}
                                </Typography>}


                                {tombstone !== "N" && <ResourceTopFlagsLandingPage tombstone={tombstone} xSize={12} alignItems={"flex-start"} />}
                                
                                {resource_type && <HostedAtElg proxied={proxied} functional_service={this.props.data.management_object.functional_service} has_data={this.props.data.management_object.has_data} size={this.props.data.management_object.size} resource_type={resource_type} justifyContent={"center"} alignItems={"flex-start"} xSize={12} />}
                                {for_information_only && <ResourceForInfo for_information_only={for_information_only} xSize={12} justifyContent={"center"} alignItems={"flex-start"} />}
                                {<ResourceActive is_active_version={is_active_version} status={status} functional_service={this.props.data.management_object.functional_service} resource_type={resource_type} xSize={12} justifyContent={"center"} alignItems={"flex-start"} />}
                                
                               

                                {shortnameArray && shortnameArray.length > 0 &&
                                    shortnameArray.map((keyword, index) =>
                                        <Typography key={index} className="acronym"> {keyword} </Typography>
                                    )}
                            </Grid>
                            <Grid item sm={12} className="pt1">
                                <Keywords keywordsArray={keywordsArray}
                                 KeywordsLabel={KeywordsLabel}
                                    languagesArray={languagesArray}
                                    domainKeywordsArray={domainKeywordsArray}
                                    DomainKeywordsLabel={DomainKeywordsLabel}
                                    subjectKeywordsArray={subjectKeywordsArray}
                                    subjectKeywordsLabel={subjectKeywordsLabel}
                                    intentedKeywordsArray={intentedKeywordsArray}
                                    intentedKeywordsLabel={intentedKeywordsLabel}
                                    corpus_subclass={corpus_subclass}
                                    corpus_subclassLabel={corpus_subclassLabel}                                    
                                    model_typeArray={model_typeArray}
                                    model_type_label={model_type_label}
                                    model_functionArray={model_functionArray}
                                    model_function_label={model_function_label}
                                    lcr_subclass={lcr_subclass}
                                    lcr_subclassLabel={lcr_subclassLabel}
                                    servicesOfferedArray={servicesOfferedArray}
                                    servicesOfferedKeywordsLabel={servicesOfferedKeywordsLabel}
                                    disciplineArray={disciplineArray} 
                                    DisciplineKeywordsLabel={DisciplineKeywordsLabel}
                                    ltAreaKeywordsArray={ltAreaKeywordsArray} 
                                    LtAreaKeywordsLabel={LtAreaKeywordsLabel}
                                        />
                            </Grid>


                        </Grid>

                    </Grid>

                    <Grid item sm={4} >
                        <Claim keycloak={keycloak} data={this.props.data} pk={this.props.pk} />
                        <Grid item container xs={12} direction="row" justifyContent="space-between">
                            <Grid item xs={12} >
                                <CitationText citationText={citationText} citationText_label={citationText_label} citation_all_versions={citation_all_versions} citation_all_versions_label={citation_all_versions_label}
                                    functional_service={this.props.data.management_object.functional_service}
                                    has_data={this.props.data.management_object.has_data} tombstone={tombstone} />
                            </Grid>
                            
                            <Grid item xs={12} >
                                <ResourceUnderConstruction under_construction={under_construction} xSize={12} justifyContent={"center"} alignItems={"flex-end"} />
                            </Grid>
                        </Grid>

                        <Grid item container xs={12} className="pt-3">
                        <Grid item xs={12} >
                                {status === "p" && <ShareMetadata shareUrl={`${BASE_URL}catalogue/${url_type}/${this.props.pk}`} title={`ELG - ${title}`} description={description} logo={logo} short_name={short_name} />}
                                </Grid>
                            </Grid>

                            
                        <ResourceActions {...this.props} resource_type={resource_type} entity_type={entity_type} keycloak={keycloak} landing_page={true} data={this.props.data} pk={this.props.pk} updateRecord={this.props.updateRecord} />
                    </Grid>

                </Grid>

            </React.Fragment>




        );

    }
}

export default ResourceHeader;