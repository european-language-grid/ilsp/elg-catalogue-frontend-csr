import React from "react";
import Typography from '@material-ui/core/Typography';
//import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
//import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import { ReactComponent as PersonOutlineIcon } from "./../../assets/elg-icons/single-neutral-id-card-4.svg";
import { ReactComponent as AccountBalanceIcon } from "./../../assets/elg-icons/buildings-modern.svg";
import { ReactComponent as GroupIcon } from "./../../assets/elg-icons/multiple-users-2.svg";
//import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import WebsitesWithIcon from './WebsitesWithIcon';
import EmailsWithIcon from './EmailsWithIcon';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
//import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import ResourceIsDivision from "./ResourceIsDivision";
import GeneralIdentifier from "./GeneralIdentifier";
import Tooltip from '@material-ui/core/Tooltip';

export default class ResourceActor extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;

        if (!data) {
            return <div></div>
        }
        if (!data.field_value) {
            return <div></div>
        }
        return <div>
            <Typography variant="h3" className="title-links">{data.field_label[metadataLanguage] || data.field_label[Object.keys(data.field_label)[0]]}</Typography>
            {data.field_value.map((actor, actorIndex) => {
                let actor_type = actor.actor_type.field_value;
                if (actor_type === "Person") {
                    let given_name = actor.given_name.field_value[metadataLanguage] || actor.given_name.field_value[Object.keys(actor.given_name.field_value)[0]];
                    let surname = actor.surname.field_value[metadataLanguage] || actor.surname.field_value[Object.keys(actor.surname.field_value)[0]];
                    let name = actor.name ? (actor.name.field_value[metadataLanguage] || actor.name.field_value[Object.keys(actor.name.field_value)[0]]) : null;
                    let email = actor.email && actor.email.field_value.length > 0 && (actor.email.field_value.map(email => email) || []);
                    given_name = (given_name === "unspecified" || given_name === undefined || given_name === "undefined") ? "" : given_name;
                    surname = (surname === "unspecified" || surname === undefined || surname === "undefined") ? "" : surname;
                    let affiliations = "";
                    actor.affiliation && actor.affiliation.field_value.length > 0 && (actor.affiliation.field_value.map(item => {
                        let aff = item.affiliated_organization ? (item.affiliated_organization.field_value.organization_name.field_value[metadataLanguage] || item.affiliated_organization.field_value.organization_name.field_value[Object.keys(item.affiliated_organization.field_value.organization_name.field_value)[0]]) : null;
                        affiliations = affiliations + aff + ","
                        return item;
                    }))

                    return (
                        <div className="padding15" key={actorIndex}>
                            <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                                <Grid item xs={12}>
                                    <Card className="actor_type_card">
                                        <CardHeader avatar={<Avatar aria-label="actor-type" className="actor-avatar"> <PersonOutlineIcon className="small-icon" /> </Avatar>}
                                            title={<>
                                                <Tooltip title={affiliations.replace(/,\s*$/, "")} placement="top-start" arrow>
                                                    <Typography variant="h6">{given_name} {surname}</Typography>
                                                </Tooltip>
                                                {name && <Typography variant="h6">{name} </Typography>}
                                                <GeneralIdentifier data={actor} identifier_name={"personal_identifier"} identifier_scheme={"personal_identifier_scheme"} metadataLanguage={metadataLanguage} /> </>}
                                            subheader={(email && email.length > 0) ? <EmailsWithIcon emailArray={email} /> : <span>{""} </span>}
                                        />
                                    </Card>
                                </Grid>
                            </Grid>
                        </div>
                    )

                }
                else if (actor_type === "Organization") {
                    const organization_name = actor.organization_name.field_value[metadataLanguage] || actor.organization_name.field_value[Object.keys(actor.organization_name.field_value)[0]];
                    const organization_short_name = actor.organization_short_name ? actor.organization_short_name.field_value[metadataLanguage] || actor.organization_short_name.field_value[Object.keys(actor.organization_name.field_value)[0]] : null;
                    const website = actor.website ? actor.website.field_value : [];
                    //console.log(organization_name)                 
                    const full_metadata_record = commonParser.getFullMetadata(actor.full_metadata_record);
                    return (
                        <div className="padding15 pt-05" key={actorIndex}>
                            <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                                <Grid item xs={12}>
                                    <Card className="actor_type_card">
                                        <CardHeader avatar={<Avatar aria-label="actor-type" className="actor-avatar"> <AccountBalanceIcon className="small-icon" /> </Avatar>}
                                            title={full_metadata_record ?
                                                <div className="internal_url">
                                                    <Link to={full_metadata_record.internalELGUrl}>
                                                        {organization_name}
                                                    </Link>
                                                </div>
                                                : <>
                                                    <Typography variant="h6" >{organization_name}</Typography>
                                                    {organization_short_name && <Typography variant="h6" >{organization_short_name}</Typography>}
                                                </>
                                            }
                                            subheader={<WebsitesWithIcon websiteArray={website} />}
                                        />
                                        {actor.is_division_of && actor.is_division_of.field_value.length > 0 && <ResourceIsDivision data={actor.is_division_of} metadataLanguage={metadataLanguage} />}

                                    </Card>
                                </Grid>
                            </Grid>
                        </div>)
                } else if (actor_type === "Group") {
                    const organization_name = actor.organization_name.field_value[metadataLanguage] || actor.organization_name.field_value[Object.keys(actor.organization_name.field_value)[0]];
                    const website = actor.website ? actor.website.field_value : [];
                    return (
                        <div className="padding15" key={actorIndex}>
                            <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                                <Grid item xs={12}>
                                    <Card className="actor_type_card">
                                        <CardHeader avatar={<Avatar aria-label="actor-type" className="actor-avatar"> <GroupIcon className="small-icon" /> </Avatar>}
                                            title={<Typography variant="h6" >{organization_name}</Typography>}
                                            subheader={<WebsitesWithIcon websiteArray={website} />}
                                        />
                                    </Card>
                                </Grid>
                            </Grid>
                        </div>
                    )


                } else {
                    return (
                        <span key={actorIndex}>

                        </span>
                    )
                }
            })}
        </div>
    }
}