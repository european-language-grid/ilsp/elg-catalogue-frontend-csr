import React from "react";
//import Typography from '@material-ui/core/Typography';
import sanitizeHtml from 'sanitize-html';

export default class DescriptionRichText extends React.Component {
    constructor(props) {
        super(props);
        this.state = { expanded: false };
    }

    toggle_more_less = (message) => {
        return <span
            className="ExpandButton BtnDescription"
            onClick={() => { this.setState({ expanded: !this.state.expanded }) }}>
            {message}
        </span>
    };

    render() {
        const { description } = this.props;
        const { expanded } = this.state;

        if (!description) {
            return <div></div>
        }

        const data_full = sanitizeHtml(description, {
            allowedTags: ['a', 'li', 'ol', 'p', 'ul', 'b', 'i', 'u', 'strong', 'em', 'code',
                'hr', 'blockquote', 'pre', 'h3', 'h2', 'br',
                'sub', 'strike', 'caption', 'table', 'tbody', 'thead', 'th', 'td', 'tr', 'notes-element'],
            allowedAttributes: {
                'a': ['href', 'name', 'target']
            },
            allowedSchemes: ['http', 'https'],
        })

        let chars_to_keep = 320;
        let data_cropped = data_full.substring(0, chars_to_keep);
        let exit_loop_counter = 0;
        if (data_cropped) {
            while (data_cropped.endsWith("</") || data_cropped.endsWith("<") || data_cropped.endsWith("/")) {
                chars_to_keep += 10;
                data_cropped = data_full.substring(0, chars_to_keep);
                if (exit_loop_counter++ >= 100) {
                    break;
                }
            }
        }

        return <>

            <div className="padding15">
                <span>
                    {/*<Typography variant="body2" className="search-results__description">*/}
                    <div style={{ marginBottom: "10px" }}
                        dangerouslySetInnerHTML={{ __html: expanded ? data_full : data_cropped }}
                    className = "description_rich_text"/>
                    {data_full.length > chars_to_keep && this.toggle_more_less(expanded ? "Read less" : "Read more")}
                </span>
            </div>

        </>

    }
}