import React from "react";
import { withRouter, Link } from 'react-router-dom'
import { ReactComponent as BackIcon } from "./../../assets/elg-icons/navigation-arrows-left-1.svg";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { ELG_CATALOGUE_PREV_PAGE } from "../../config/constants";
import GoogleAnalytics from "../GoogleAnalytics";
import messages from "./../../config/messages";
class GoToCatalogue extends React.Component {

    catalogueMainPage = () => {
        const catalogueUrl = sessionStorage.getItem(ELG_CATALOGUE_PREV_PAGE);
        let url = '';
        if (catalogueUrl) {
            if (catalogueUrl.indexOf("/catalogue/") >= 0) {
                url = catalogueUrl.substring('/catalogue'.length);
            } else {
                url = catalogueUrl;
            }
        } else {
            url = "/";
        }
        return url;
    }

    render() {
        const url = this.catalogueMainPage();
        return <div>
            <Container maxWidth="xl">
                <Grid container direction="row" justifyContent="flex-end" alignItems="center"  >
                    <Grid item >
                        <Link className="linkButton" to={url} >
                            <span><BackIcon className="xsmall-icon mr-05" /></span>
                            <span>{messages.go_back_catalog_message}</span>
                        </Link>
                    </Grid>
                </Grid>
            </Container>
            <GoogleAnalytics />
        </div>
    }
}

export default withRouter(GoToCatalogue);