import React from "react";
import axios from "axios";
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import {
    EXPORT_XML_METADATARECORD_ENDPOINT, EXPORT_RDF_METADATARECORD_ENDPOINT,
    EXPORT_DataCite_XML_METADATARECORD_ENDPOINT, EXPORT_DataCite_JSON_METADATARECORD_ENDPOINT, DataCite_PROD_VALUE_STARTS, DataCite_DEV_VALUE_STARTS,
    IS_ADMIN, IS_CONTENT_MANAGER, PRODUCTION
} from "../../config/constants";
//import CircularProgress from '@material-ui/core/CircularProgress'; 

export default class ExportMetadata extends React.Component {
    constructor(props) {
        super(props);
        this.state = { pendingRequest: false, isCurator: false, source: null, value: "" };
    }

    componentDidMount() {
        this.allowDataCite_export(this.props.data);
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    handleAction = (pk, action) => {
        this.setState({ pendingRequest: true });
        axios.get(action === "xml" ? EXPORT_XML_METADATARECORD_ENDPOINT(pk) : EXPORT_RDF_METADATARECORD_ENDPOINT(pk), { responseType: 'blob', timeout: 30000, })
            .then(res => {
                if (res && res.data) {
                    this.setState({ pendingRequest: false });
                    const url = window.URL.createObjectURL(new Blob([res.data]));
                    var [, filename] = res.headers['content-disposition'].split('filename=');
                    if (!filename) {
                        try {
                            const content_disp = res.headers['content-disposition'];
                            if (content_disp.indexOf("b?") > 0 && content_disp.indexOf("?=") >= 0) {
                                var s = content_disp.indexOf("b?");
                                var e = content_disp.lastIndexOf("?=");
                                const payload = content_disp.substring(s + 2, e);
                                const decoded = Buffer.from(payload, "base64").toString("utf-8");
                                filename = decoded.substring(decoded.indexOf("filename=") + "filename=".length);
                            } else {
                                filename = this.props.data.pk || "undefined";
                                filename = filename + (action === "xml" ? ".xml" : ".owl");
                            }
                        } catch (err) {
                            filename = this.props.data.pk || "undefined";
                            filename = filename + (action === "xml" ? ".xml" : ".owl");
                        }
                    }
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', filename);
                    document.body.appendChild(link);
                    link.click();
                    link.remove();
                }
            })
            .catch(err => { this.setState({ pendingRequest: false }); });
    }

    handleExternalAction = (value, action) => {
        const extension = action === "DataCiteXML" ? ".xml" : ".json";
        const fileName = (this.props.name || this.props.data.pk) + extension;
        this.setState({ pendingRequest: true });
        axios.get(action === "DataCiteXML" ? EXPORT_DataCite_XML_METADATARECORD_ENDPOINT(value) : EXPORT_DataCite_JSON_METADATARECORD_ENDPOINT(value), { responseType: 'blob', timeout: 30000, })
            .then(res => {
                if (res && res.data) {
                    this.setState({ pendingRequest: false });
                    const bl = res.data;
                    const url = window.URL.createObjectURL(bl, bl.type);
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', fileName);
                    document.body.appendChild(link);
                    link.click();
                    link.remove();
                }
            })
            .catch(err => { this.setState({ pendingRequest: false }); });
    }


    allowDataCite_export(data) {
        const { lr_identifier } = data.described_entity.field_value;
        let datacite_value = "";
        lr_identifier && lr_identifier.field_value.length > 0 && lr_identifier.field_value.map((identifier) => {
            let identifier_scheme = identifier.lr_identifier_scheme.field_value ? identifier.lr_identifier_scheme.field_value : "";
            if (identifier_scheme === "http://purl.org/spar/datacite/doi") {
                datacite_value = identifier.value.field_value
                if (PRODUCTION) {
                    if (datacite_value.includes(`${DataCite_PROD_VALUE_STARTS}`)) {
                        this.setState({ value: datacite_value })
                        return datacite_value;
                    }
                } else {
                    if (datacite_value.includes(`${DataCite_DEV_VALUE_STARTS}`)) {
                        this.setState({ value: datacite_value })
                        return datacite_value;
                    }
                }
            }
            return identifier;
        })
    }


    allow_export = (pk, value, under_construction, keycloak, data, status) => {

        if (IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak)) {
            return true;
        }
        if (this.props.for_information_only === true) {
            return false;
        }
        if (!under_construction && status === "p") {
            return true;
        }
        if (this.props.isOwner) {
            return true;
        }
        return false;
    }

    render() {
        const { pk, under_construction, keycloak, data } = this.props;
        const { status } = data.management_object;
        const { value } = this.state;
        //console.log(value)
        if (!this.allow_export(pk, value, under_construction, keycloak, data, status)) {
            return <></>
        }
        return <div className="ActionsButtonArea" style={{ marginTop: '1em' }}>
            <Typography variant="h3" className="title-links">Export</Typography>
            <Grid container direction="row" justifyContent="flex-start" alignItems="center"  >
                <Grid item sm={2}>
                    <Link disabled={this.state.pendingRequest} component="button" variant="body2" className="link-style-button" onClick={() => { this.handleAction(pk, "xml"); }}> ELG (XML) </Link>
                </Grid>
                <Grid item sm={4}>
                    <Link disabled={this.state.pendingRequest} component="button" variant="body2" className="link-style-button" onClick={() => { this.handleAction(pk, "rdf"); }}> MS-OWL (RDF/XML) </Link>
                </Grid>
                {value && <Grid item sm={3}>
                    <Link disabled={this.state.pendingRequest} component="button" variant="body2" className="link-style-button" onClick={() => { this.handleExternalAction(value, "DataCiteXML"); }}> DataCite (XML) </Link>
                </Grid>}
                {value && <Grid item sm={3}>
                    <Link disabled={this.state.pendingRequest} component="button" variant="body2" className="link-style-button" onClick={() => { this.handleExternalAction(value, "DataCiteJSON"); }}> DataCite (JSON) </Link>
                </Grid>}
            </Grid>

        </div>
    }
}