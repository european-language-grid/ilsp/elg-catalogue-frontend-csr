import React from "react";
import Typography from '@material-ui/core/Typography';
//import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
//import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
//import { ReactComponent as PersonOutlineIcon } from "./../../assets/elg-icons/single-neutral-id-card-4.svg";
//import { ReactComponent as AccountBalanceIcon } from "./../../assets/elg-icons/buildings-modern.svg";
//import { ReactComponent as GroupIcon } from "./../../assets/elg-icons/multiple-users-2.svg";
//import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import WebsitesWithIcon from './WebsitesWithIcon';
//import EmailsWithIcon from './EmailsWithIcon';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
//import Avatar from '@material-ui/core/Avatar';

export default class ResourceIsDivision extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        //console.log(data)
        if (!data) {
            return <div></div>
        }
        if (!data.field_value) {
            return <div></div>
        }   
        let is_division_of_label = data.field_value.is_division_of ? data.field_value.is_division_of.field_label[metadataLanguage] || data.field_value.is_division_of.field_label["en"] : "";
        let divisionArray = (data.field_value && data.field_value.map((divisionItem, divisionIndex) => {
            const organization_name = divisionItem.organization_name.field_value[metadataLanguage] || divisionItem.organization_name.field_value[Object.keys(divisionItem.organization_name.field_value)[0]];
            const website = divisionItem.website ? divisionItem.website.field_value : [];
            const full_metadata_record = commonParser.getFullMetadata(divisionItem.full_metadata_record);
            return <div key={divisionIndex} className="padding5">
                <Typography className="bold-p--id">{data.field_label[metadataLanguage] || data.field_label[Object.keys(data.field_label)[0]]}</Typography>
                <div>
                    <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={1}>
                        <Grid item xs={12}>
                            <Card className="actor_type_card">
                                <CardHeader
                                    title={full_metadata_record ?
                                        <div className="internal_url padding5">
                                            <Link to={full_metadata_record.internalELGUrl}>
                                                {organization_name}
                                            </Link>
                                        </div>
                                        :
                                        <Typography variant="h6">{organization_name}</Typography>
                                    }
                                    subheader={<WebsitesWithIcon websiteArray={website} />}
                                />

                                {divisionItem.is_division_of && divisionItem.is_division_of.field_value.length>0 && <ResourceIsDivision data={divisionItem.is_division_of} metadataLanguage={metadataLanguage} /> }

                            </Card>
                        </Grid>
                    </Grid>
                </div>
            </div>
        })) || [];

        return <>
        {divisionArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_division_of_label}</Typography><ul className="flex wrap">{divisionArray.map((item, index) => <li key={index}>{item}</li>)}</ul></div>}
        </>

    }
}