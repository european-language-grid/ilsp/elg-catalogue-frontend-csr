import React from "react";
//import Typography from '@material-ui/core/Typography';
import AppsIcon from '@material-ui/icons/Apps';
import { ReactComponent as AudioIcon } from "./../../assets/elg-icons/audio-file-volume.svg";
import { ReactComponent as ImageIcon } from "./../../assets/elg-icons/image-file-camera.svg";
import { ReactComponent as TextIcon } from "./../../assets/elg-icons/office-file-text-graph-alternate.svg";
import { ReactComponent as VideoIcon } from "./../../assets/elg-icons/video-edit-magic-wand.svg";
import GradientIcon from '@material-ui/icons/Gradient';
class MediaPartIconType extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: '' };
  }
  typeofmedia = (type) => {
    switch (type) {

      case "audio": return (<AudioIcon className="small-icon general-icon--grey mr-05"/> );
      case "video": return (<VideoIcon className="small-icon general-icon--grey mr-05"/> );
      case "image": return ( <ImageIcon className="small-icon general-icon--grey mr-05"/>);
      case "text": return ( <TextIcon className="small-icon general-icon--grey mr-05"/> );
      case "unspecified": return ( <GradientIcon className="small-icon general-icon--grey mr-05"/> );
      default: return ( <AppsIcon  className="small-icon general-icon--grey mr-05"/> );
    }
  }

  render() {
    const { media_type} = this.props;



    return (
      <React.Fragment>
        { 
            this.typeofmedia(media_type)
            
        }


      </React.Fragment>
    );
  }

}

export default MediaPartIconType;