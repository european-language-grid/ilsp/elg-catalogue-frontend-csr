import React from "react";
import Grid from '@material-ui/core/Grid';
//import { ADMIN_PERMITTED_ROLES } from "../../config/constants";

export default class ResourceDownloadable extends React.Component {   
    
    render() {
        const { size,xSize,position } = this.props;
       if (!this.size === 0) {
           return <div></div>
       }
       
        return (         
            <Grid  container direction="column" justifyContent="center"   alignItems="flex-end">
             <Grid item xs={xSize}>           
              { size > 0 && <span className={"caption grey--font ui purple  "+position}>available for download</span>} 
            </Grid>               
          </Grid>
        );
         
    }
}