import React from "react";
//import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
//import VisibilityIcon from '@material-ui/icons/Visibility';
//import GetAppIcon from '@material-ui/icons/GetApp';
//import { TOOL_SERVICE, CORPUS, LCR, LD, PROJECT, ORGANIZATION } from "../config/constants";
import { TOOL_SERVICE } from "../../config/constants";
export default class Analytics extends React.Component {

    constructor(props) {
        super(props)
        this.showProjectOrganizationAnalytics = this.showProjectOrganizationAnalytics.bind(this);
        this.showToolServiceAnalytics = this.showToolServiceAnalytics.bind(this);
    }
    showProjectOrganizationAnalytics(views, xSize) {
        return <Grid container direction="column" justifyContent="center" alignItems="flex-end">
            <Grid item xs={xSize}>
                <span className="caption grey--font"> {views}  views </span>
            </Grid>
            <Grid item xs={xSize}>
                <span className="caption grey--font"></span>
            </Grid>
        </Grid>
    }

    showToolServiceAnalytics(views, service_execution_count, xSize) {
        return <Grid container direction="column" justifyContent="center" alignItems="flex-end">
            <Grid item xs={xSize}>
                <span className="caption grey--font"> {views}  views </span>
            </Grid>
            {service_execution_count !== null && service_execution_count !== undefined && service_execution_count >= 0 && <Grid item xs={xSize}>
                <span className="caption grey--font"> {service_execution_count} times used  </span>
            </Grid>
            }
        </Grid>
    }

    render() {
        const { views, xSize, resource_type, service_execution_count, downloads, under_construction } = this.props;
        //const { entity_type,downloads } = this.props;
        if (resource_type === null) {
            return this.showProjectOrganizationAnalytics(views, xSize);
        }
        if ((resource_type && resource_type === TOOL_SERVICE) && !under_construction) {
            return this.showToolServiceAnalytics(views, service_execution_count, xSize);
        }
        return (
            <Grid container direction="column" justifyContent="center" alignItems="flex-end">
                <Grid item xs={xSize}>
                    <span className="caption grey--font"> {views}  views </span>
                </Grid>
                {resource_type && resource_type !== TOOL_SERVICE && downloads !== null && downloads !== undefined && !under_construction && <Grid item xs={xSize}>
                    <span className="caption grey--font"> {downloads} downloads  </span>
                </Grid>
                }
            </Grid>
        );
    }
}