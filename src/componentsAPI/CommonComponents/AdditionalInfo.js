import React, { Fragment } from "react";
import Typography from '@material-ui/core/Typography';
//import LaunchIcon from '@material-ui/icons/Launch';
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";
import ResourceActor from "../CommonComponents/ResourceActor";
import ResourceActorSingleItem from "../CommonComponents/ResourceActorSingleItem"; 
//import MailingList from './MailingList';
import DiscussionUrls from './DiscussionUrls';
//import GeneralIdentifier from "./GeneralIdentifier";
import { ReactComponent as MailOutlineIcon} from './../../assets/elg-icons/email-action-send-1.svg';

function reverseString(str) {
    return str.split("").reverse().join("");
}
 
export default class AdditionalInfo extends React.Component {
    render() {
        const { additioanlInfo, data, metadataLanguage, discussionUrl, mailingList, AdditionalInfoLabel/*, identifier_name, identifier_scheme */} = this.props;
        const { SourceOfMetadata_label,  repository_name_value , repository_url_value,  repository_institution} = this.props;  //repository_name_label,
        
        if (!data) {
            return <div></div>
        }
        const showComponent = (additioanlInfo && additioanlInfo.length > 0) || (discussionUrl && discussionUrl.length > 0) || (mailingList && mailingList.length > 0);
        const hasContact = data.described_entity.field_value.contact && data.described_entity.field_value.contact.field_value.length > 0;
        //console.log(additioanlInfo && additioanlInfo.length > 0);
        if (!showComponent && !hasContact) {
            return <div></div>
        }
        return <div className="padding15">
            {(showComponent || hasContact) && <Typography variant="h3" className="title-links"> {AdditionalInfoLabel}  </Typography>}

            {additioanlInfo && additioanlInfo.map((additionalItem, index) =>
                <Fragment key={index}>
                    {additionalItem.landing_page &&
                        <div className="padding5">
                            {/*<Typography className="bold-p--id">{additionalItem.landing_page.field_label[metadataLanguage] || additionalItem.landing_page.field_label["en"]}</Typography>*/}
                            <a href={additionalItem.landing_page.field_value} target="_blank" rel="noopener noreferrer" className="info_url">
                                <span><LaunchIcon className="xsmall-icon" /></span> <span>{additionalItem.landing_page.field_label[metadataLanguage] || additionalItem.landing_page.field_label["en"]}</span> </a>
                        </div>
                    }

                    { additionalItem.email && <div className="padding5">
                        <a href={`mailto:${reverseString(additionalItem.email.field_value)}`} className="info_url" target="_blank" rel="noopener noreferrer" >
                        {<div><span><MailOutlineIcon className="xsmall-icon"/></span> <span>Email</span></div>}
                        
                    </a> </div>
                    }
                    {/*data.metadata_creator && additionalItem.email &&
                        <div className="padding15">
                            <a href={`mailto:${additionalItem.email.field_value}`} target="_blank" rel="noopener noreferrer" className="info_url">
                                <span><MailOutlineIcon className="xsmall-icon" /></span> <span>Email</span> </a>
                        </div>
                    */ }
                </Fragment>
            )}           

            {/*<GeneralIdentifier data={data.described_entity.field_value} identifier_name={identifier_name} identifier_scheme={identifier_scheme} metadataLanguage={metadataLanguage} />*/}
            <ResourceActor data={data.described_entity.field_value.contact} metadataLanguage={metadataLanguage} />
            {/*<MailingList mailingListArray={mailingList} />*/}
            <DiscussionUrls discussionUrl={discussionUrl} />

            {repository_name_value && <>
            <Typography variant="h3" className="title-links"> {SourceOfMetadata_label}  </Typography> 
            <div className="padding5">
            {/*<Typography className="bold-p--id">{repository_name_label}</Typography> */}
                {repository_url_value ? ( <a href={repository_url_value} target="_blank" rel="noopener noreferrer" className="info_url">
                <span><LaunchIcon className="xsmall-icon" /></span> <span>{repository_name_value}</span> </a>)
                :
                (<span  className="info_value">{repository_name_value}</span>)
                }
                </div>

                <ResourceActorSingleItem data={repository_institution} metadataLanguage={metadataLanguage} />

                </>
            
            }


        </div>
    }
}