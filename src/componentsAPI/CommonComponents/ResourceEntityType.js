import React from "react";
//import ToolServiceImage from "../../assets/images/toolservice.svg";
//import ProjectImage from "../../assets/images/project.svg";
//import ResourceImage from "../../assets/images/resource.svg";
//import OrganizationImage from "../../assets/images/network.svg";
import BlurLinearIcon from '@material-ui/icons/BlurLinear';
import BlurCircularIcon from '@material-ui/icons/BlurCircular';
import BlurOnIcon from '@material-ui/icons/BlurOn';
import Typography from '@material-ui/core/Typography';
import { ReactComponent as OrganizationIcon } from "./../../assets/elg-icons/buildings-modern.svg";
import { ReactComponent as ProjectIcon } from "./../../assets/elg-icons/human-resources-team-settings.svg";
import { ReactComponent as LCRIcon } from "./../../assets/elg-icons/archive-folder.svg";
import { ReactComponent as ToolServiceIcon } from "./../../assets/elg-icons/settings-user.svg";
import { ReactComponent as ResourceIcon } from "./../../assets/elg-icons/database.svg";

import { TOOL_SERVICE, CORPUS, LCR, LD, PROJECT, ORGANIZATION } from "../../config/constants";
class ResourceEntityType extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: '' };
  }
  typeofresource = (type, align) => {
    switch (type) {

      case TOOL_SERVICE: return (<div className={align}>
        <ToolServiceIcon className="xsmall-icon ToolIcon" /> {/*<img alt="img" src={ToolServiceImage} title="Tool Service" className="pr-05" />*/}
        <Typography variant="caption" className="ResourceListCaption">{TOOL_SERVICE}</Typography>
      </div>);

      case CORPUS: return (<div className={align}>
        <ResourceIcon className="xsmall-icon ResourceIcon" /> {/*<img alt="img" src={ResourceImage} title="Corpus" className="pr-05" />*/}
        <Typography variant="caption" className="ResourceListCaption">{CORPUS}</Typography>
      </div>);

      case LCR: return (
        <div className={align}>
          <LCRIcon className="xsmall-icon LCRIcon" />
          <Typography variant="caption" className="ResourceListCaption">{LCR}</Typography>
        </div>);

      case LD: return (
        <div className={align}>
          <BlurLinearIcon className=" xsmall-icon LDIcon" />
          <Typography variant="caption" className="ResourceListCaption">{LD}</Typography>
        </div>);

      case "ToolService": return (<div className={align}>
        <ToolServiceIcon className="xsmall-icon ToolIcon" /> {/*<img alt="img" src={ToolServiceImage} title="Tool Service" className="pr-05" />*/}
        <Typography variant="caption" className="ResourceListCaption">ToolService</Typography>
      </div>);

      case "LexicalConceptualResource": return (
        <div className={align}>
          <LCRIcon className="xsmall-icon LCRIcon" />
          <Typography variant="caption" className="ResourceListCaption">LexicalConceptualResource</Typography>
        </div>);

      case "LanguageDescription": return (
        <div className={align}>
          <BlurLinearIcon className=" xsmall-icon LDIcon" />
          <Typography variant="caption" className="ResourceListCaption grey-font">LanguageDescription</Typography>
        </div>);

      case "Uncategorized Language Description": return (
        <div className={align}>
          <BlurLinearIcon className=" xsmall-icon LDIcon" />
          <Typography variant="caption" className="ResourceListCaption grey-font">LanguageDescription</Typography>
        </div>);

      case "uncategorized language description": return (
        <div className={align}>
          <BlurLinearIcon className=" xsmall-icon LDIcon" />
          <Typography variant="caption" className="ResourceListCaption grey-font">LanguageDescription</Typography>
        </div>);
      case "model": return (
        <div className={align}>
          <BlurCircularIcon className=" xsmall-icon LDIcon" />
          <Typography variant="caption" className="ResourceListCaption">Model</Typography>
        </div>);
      case "grammar": return (
        <div className={align}>
          <BlurOnIcon className=" xsmall-icon LDIcon" />
          <Typography variant="caption" className="ResourceListCaption">Grammar</Typography>
        </div>);
      case "N-gram Model": return (
        <div className={align}>
          <BlurLinearIcon className=" xsmall-icon LDIcon" />
          <Typography variant="caption" className="ResourceListCaption">N-gram Model</Typography>
        </div>);
      default: return ""
    }
  }

  typeofentity = (type, align) => {
    switch (type) {
      case PROJECT: return (
        <div className={align}>
          <ProjectIcon className="xsmall-icon ProjectIcon" />
          <Typography variant="caption" className="ResourceListCaption">{PROJECT}</Typography>

        </div>);
      case ORGANIZATION: return (
        <div className={align}>
          <OrganizationIcon className="xsmall-icon OrganizationIcon" />
          <Typography variant="caption" className="ResourceListCaption">{ORGANIZATION}</Typography>

        </div>);


      default: return ""
    }
  }

  render() {
    const { resource_type, entity_type, align } = this.props;



    return (
      <React.Fragment>
        {

          !resource_type ?
            (this.typeofentity(entity_type, align)
            ) : (this.typeofresource(resource_type, align))
        }


      </React.Fragment>
    );
  }

}

export default ResourceEntityType;