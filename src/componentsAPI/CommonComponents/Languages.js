import React from "react";
import Typography from '@material-ui/core/Typography';
//import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';

export default class Languages extends React.Component {
    constructor(props) {
        super(props);
        this.state = { expanded: false, };
    }

    togglexpanded = () => {
        this.setState({ expanded: !this.state.expanded })
    }


    render() {
        const { language, metadataLanguage , from_tool, inferred_language} = this.props;
        const { expanded } = this.state;
        if (!language) {
            return <></>
        }

        let language_label = language ? (language.field_label[metadataLanguage] || language.field_label["en"]) : "";
        let languageInfoArray = (language && language.field_value.length > 0 && language.field_value.map((lang, langIndex, arr) => {
            let language_varietyNameArray = [];
            let language_variety_name = "";
            let language_variety_label = "";

            let language_tag = lang.language_tag ? (lang.language_tag.field_value) : "";
            let language_tag_label = lang.language_tag ? (lang.language_tag.field_label[metadataLanguage] || lang.language_tag.field_label["en"]) : " ";
            let language_id = (lang.language_id && (lang.language_id.label[metadataLanguage] || lang.language_id.label[Object.keys(lang.language_id.label)[0]])) || [];
            let language_id_label = lang.language_id ? (lang.language_id.field_label[metadataLanguage] || lang.language_id.field_label["en"]) : " ";

            language_variety_name = lang.language_variety_name ? (lang.language_variety_name.field_value[metadataLanguage] || lang.language_variety_name.field_value[Object.keys(lang.language_variety_name.field_value)[0]]) : "";
            language_variety_label = language_variety_name && lang.language_variety_name ? (lang.language_variety_name.field_label[metadataLanguage] || lang.language_variety_name.field_label["en"]) : language_variety_label;
            language_variety_name && language_varietyNameArray.push(language_variety_name);

            let language_region_id_value = lang.region_id ? (lang.region_id.label[metadataLanguage] || lang.region_id.label["en"]) : "";
            let language_region_id_label = lang.region_id ? (lang.region_id.field_label[metadataLanguage] || lang.region_id.field_label["en"]) : " ";

            let language_variant_idArray = (lang.variant_id && lang.variant_id.field_value.map(variant => variant.label[metadataLanguage] || variant.label[Object.keys(variant.label)[0]])) || [];
            let language_variant_id_label = lang.variant_id ? (lang.variant_id.field_label[metadataLanguage] || lang.variant_id.field_label["en"]) : " ";

            let script_id_value_label = lang.script_id ? (lang.script_id.label[metadataLanguage] || lang.script_id.label["en"]) : "";
            let script_id_label = lang.script_id ? (lang.script_id.field_label[metadataLanguage] || lang.script_id.field_label["en"]) : " ";

            let glottolog_code_field_label = lang.glottolog_code ? (lang.glottolog_code.field_label[metadataLanguage] || lang.glottolog_code.field_label["en"]) : " ";
            let glottolog_code_label = (lang.glottolog_code && (lang.glottolog_code.label[metadataLanguage] || lang.glottolog_code.label[Object.keys(lang.glottolog_code.label)[0]])) || null;
            let glottolog_code_value = lang.glottolog_code ? lang.glottolog_code.field_value : "" ;

            return <span key={langIndex}>
                <Accordion className="LanguageBox">
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon className="teal--font" />}
                        aria-controls="panel1a-content"
                        id="panel1a-header">
                        {<>
                            {language_id && <span className="teal--font">{language_id} </span>}                            
                            {/*language_tag && <span className="teal--font">- {language_tag} </span>*/}
                            {script_id_value_label && <span className="teal--font">- {script_id_value_label} </span>}
                            {language_region_id_value && <span className="teal--font">- {language_region_id_value} </span>}
                            {language_variant_idArray.length > 0 && <span className="teal--font">{language_variant_idArray.map((item, index) => <span key={index}>- {item} </span>)}</span>}
                            {glottolog_code_label && <span className="teal--font">- {glottolog_code_label} </span>}
                            {language_varietyNameArray.length > 0 && language_varietyNameArray.map((langVarietyName, langVarietyIndex) => <span className="teal--font" key={langVarietyIndex}> - {langVarietyName} </span>)}


                        </>}
                    </AccordionSummary>
                    <AccordionDetails>
                        <div>
                            {language_tag && <div className="padding5"><Typography className="bold-p--id">{language_tag_label}</Typography><span className="info_value">{language_tag}</span></div>}
                            {language_id && <div className="padding5"><Typography className="bold-p--id">{language_id_label}</Typography><span className="info_value">{language_id}</span></div>}                           
                            {script_id_value_label && <div className="padding5"><Typography className="bold-p--id">{script_id_label}</Typography><span className="info_value">{script_id_value_label}</span></div>}
                            {language_region_id_value && <div className="padding5"><Typography className="bold-p--id">{language_region_id_label}</Typography><span className="info_value">{language_region_id_value}</span></div>}
                            {language_variant_idArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{language_variant_id_label}</Typography><span className="info_value">{language_variant_idArray.map((item, index) => <span className="info_value" key={index}>{item} </span>)}</span></div>}
                            {/* glottologhere */}
                            {glottolog_code_label && <div className="padding5"><Typography className="bold-p--id">{glottolog_code_field_label}</Typography><span className="info_value">{glottolog_code_value}</span></div>}
                            {/*glottolog_code_label && <div className="padding5"><Typography className="bold-p--id">{glottolog_code_field_label}</Typography><span className="info_value">{glottolog_code_label}</span></div>*/}
                            {language_varietyNameArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{language_variety_label}</Typography> <span className="info_value">{language_varietyNameArray.map((langVarietyName, langVarietyIndex, varr) => <span className="info_value" key={langVarietyIndex}>{varr.length - 1 !== langVarietyIndex && <span className="comma_value"> - </span>}{langVarietyName}</span>)}</span></div>}

                        </div>
                    </AccordionDetails>
                </Accordion>

            </span>


        })) || [];


        return (
            <>
                {languageInfoArray && languageInfoArray.length > 0 ?
                    (<>
                        <Typography className="bold-p--id">{language_label} {inferred_language===true ? <Tooltip title="automatically inferred"><InfoOutlinedIcon fontSize="medium" className="teal--font" /></Tooltip> : ""} </Typography>
                        {expanded ? (
                            <Grid container spacing={2} direction="row" justifyContent="flex-start" alignItems="flex-start" className="padding5">
                                {languageInfoArray.map((item, index, arr) => <Grid item xs={12} sm={from_tool===true ? 6 : 4} key={index} >{item}</Grid>)}
                                <Grid item xs={12} className="grey--font ExpandButton caption" onClick={this.togglexpanded} >Show less</Grid>
                            </Grid>
                        ) : (
                                <Grid container spacing={2} direction="row" justifyContent="flex-start" alignItems="flex-start" className="padding5">

                                    {languageInfoArray.slice(0, 4).map((item, index, arr) => <Grid item xs={12} sm={from_tool===true ? 6 : Math.floor(12/arr.length)} key={index} >{item}</Grid>)}
                                    {languageInfoArray.length > 4 ?
                                        <Grid item xs={12} sm={from_tool===true ? 6 : 4} className="grey--font ExpandButton caption" onClick={this.togglexpanded} > Show more </Grid>
                                        : void 0}
                                </Grid>
                            )
                        }

                    </>)
                    : void 0
                }




            </>
        )
    }
}