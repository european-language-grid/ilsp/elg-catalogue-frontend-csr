import React from "react";
import Typography from '@material-ui/core/Typography';
import ReverseRelations from "./ReverseRelations";
import GeneralIdentifier from "./GeneralIdentifier";
import SingleIdentifier from "./SingleIdentifier";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

export default class Relations extends React.Component {
    componentDidMount() {
        const { data } = this.props;
        const { is_part_of, is_part_with, is_similar_to, is_exact_match_with, is_archived_by, is_continuation_of, relation, is_version_of, reverse_relations, has_metadata, is_related_to_lr, is_compatible_with, is_supplemented_by, is_supplement_to } = data.described_entity.field_value || [];
        const { is_annotated_version_of, is_aligned_version_of } = data.described_entity.field_value.lr_subclass.field_value || "";
        const { is_analysed_by, is_edited_by, is_elicited_by, is_converted_version_of } = data.described_entity.field_value.lr_subclass.field_value || [];
        if (!data) {
            return false;
        }
        const showComponent = is_part_of || is_part_with || is_similar_to || is_exact_match_with || is_archived_by || is_continuation_of || relation || is_version_of || reverse_relations || is_annotated_version_of || is_aligned_version_of ||
            is_analysed_by || is_edited_by || is_elicited_by || is_converted_version_of || has_metadata || is_related_to_lr || is_supplemented_by || is_compatible_with || is_supplement_to;
        const { displayTabGeneral } = this.props;

        if (showComponent) {
            displayTabGeneral ? void 0 : this.props.displayTabGeneralFunction(true);
        }
    }

    render() {

        const { data, metadataLanguage } = this.props;
        const { is_part_of, is_part_with, is_similar_to, is_exact_match_with, is_archived_by, is_continuation_of, relation, is_version_of, is_supplemented_by, is_compatible_with, is_supplement_to } = data.described_entity.field_value || [];
        const { is_annotated_version_of, is_aligned_version_of } = data.described_entity.field_value.lr_subclass.field_value || "";
        const { is_analysed_by, is_elicited_by } = data.described_entity.field_value.lr_subclass.field_value || [];

        //let title_label = (is_part_of || is_part_with || is_similar_to || is_exact_match_with || is_archived_by || is_continuation_of || replaces || relation || is_version_of || is_annotated_version_of || is_aligned_version_of ||
        //    is_analysed_by || is_edited_by || is_elicited_by || is_converted_version_of) ? "Relations to other resources" : "" ;
        //let showReverseTitle_label =  reverse_relations ||  false ;                          

        return <>


            {is_part_of && <Typography className="bold-p--id">{is_part_of.field_label[metadataLanguage] || is_part_of.field_label["en"] || "Is part of"}</Typography>}
            {is_part_of && <div className="padding5">  {is_part_of.field_value.map((partOfItem, index) => {
                let resource_name = (partOfItem.resource_name.field_value[metadataLanguage] || partOfItem.resource_name.field_value[Object.keys(partOfItem.resource_name.field_value)[0]]) || "";
                let version = partOfItem.version ? "(" + partOfItem.version.field_value +")" : '';                 
                // const is_part_of_label = is_part_of.field_label[metadataLanguage] || is_part_of.field_label["en"] || "Is part of";
                let full_metadata_record = commonParser.getFullMetadata(partOfItem.full_metadata_record);
                let classid = "info_value";
                if (resource_name.includes('untitled harvested')) {classid="show_hidden"}
                return <div key={index}>
                    {full_metadata_record ? <>
                        <div className="internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span>{resource_name} {version}</span>
                            </Link>
                        </div>
                        <GeneralIdentifier data={partOfItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                    </> :
                        resource_name && <><div className={classid}> {resource_name} {version} </div>
                            <GeneralIdentifier data={partOfItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </>}
                </div>
            })} </div>}

            {is_part_with && <Typography className="bold-p--id">{is_part_with.field_label[metadataLanguage] || is_part_with.field_label["en"] || "Is part with"}</Typography>}
            {is_part_with && <div className="padding5">  {is_part_with.field_value.map((partWithItem, index) => {
                let resource_name = (partWithItem.resource_name.field_value[metadataLanguage] || partWithItem.resource_name.field_value[Object.keys(partWithItem.resource_name.field_value)[0]]) || "";
                let version = partWithItem.version?.field_value ?  "(" + partWithItem.version.field_value + ")" : ''; 
                //const part_with_label = is_part_with.field_label[metadataLanguage] || is_part_with.field_label["en"] || "Is part with";
                let classid = "info_value";
                if (resource_name.includes('untitled harvested')) {classid="show_hidden"}
                let full_metadata_record = commonParser.getFullMetadata(partWithItem.full_metadata_record);
                return <div key={index}>
                    {full_metadata_record ?
                        <div className="internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span> {resource_name} {version} </span>
                            </Link>
                            <GeneralIdentifier data={partWithItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </div> :
                        resource_name && <div> <div className={classid}> {resource_name} {version} </div>
                            <GeneralIdentifier data={partWithItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>}
                </div>
            })} </div>}
            {is_similar_to && <Typography className="bold-p--id"> {is_similar_to.field_label[metadataLanguage] || is_similar_to.field_label["en"] || "Is similar to"}</Typography>}
            {is_similar_to && <div className="padding5">  {is_similar_to.field_value.map((similarToItem, index) => {
                let resource_name = (similarToItem.resource_name.field_value[metadataLanguage] || similarToItem.resource_name.field_value[Object.keys(similarToItem.resource_name.field_value)[0]]) || "";
                let version = similarToItem.version ? "(" + similarToItem.version.field_value + ")" : '';
                //const similar_to_label = is_similar_to.field_label[metadataLanguage] || is_similar_to.field_label["en"] || "Is similar to";
                let classid = "info_value";
                if (resource_name.includes('untitled harvested')) {classid="show_hidden"}
                let full_metadata_record = commonParser.getFullMetadata(similarToItem.full_metadata_record);
                return <div key={index}>
                    {full_metadata_record ?
                        <div className="internal_url">

                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span>{resource_name} {version} </span>
                            </Link>
                            <GeneralIdentifier data={similarToItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </div> :
                        resource_name && <div><div className={classid}> {resource_name} {version} </div>
                            <GeneralIdentifier data={similarToItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>}
                </div>
            })}</div>}
            {is_exact_match_with && <Typography className="bold-p--id"> {is_exact_match_with.field_label[metadataLanguage] || is_exact_match_with.field_label["en"] || "Is exact match with"}</Typography>}
            {is_exact_match_with && <div className="padding5">  {is_exact_match_with.field_value.map((exactMatchWithItem, index) => {
                let resource_name = (exactMatchWithItem.resource_name.field_value[metadataLanguage] || exactMatchWithItem.resource_name.field_value[Object.keys(exactMatchWithItem.resource_name.field_value)[0]]) || "";
                let version = exactMatchWithItem.version ? "(" +  exactMatchWithItem.version.field_value + ")" : '';
                //const exact_match_with_label = is_exact_match_with.field_label[metadataLanguage] || is_exact_match_with.field_label["en"] || "Is exact match with";
                let classid = "info_value";
                if (resource_name.includes('untitled harvested')) {classid="show_hidden"}
                let full_metadata_record = commonParser.getFullMetadata(exactMatchWithItem.full_metadata_record);
                return <div key={index}>
                    {full_metadata_record ?
                        <div className="internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span> {resource_name} {version} </span>
                            </Link>
                            <GeneralIdentifier data={exactMatchWithItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </div> :
                        resource_name && <div><div className={classid}> {resource_name} {version} </div>
                            <GeneralIdentifier data={exactMatchWithItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>}
                </div>
            })}</div>}

            {data.described_entity.field_value.has_metadata && <Typography className="bold-p--id">{data.described_entity.field_value.has_metadata.field_label[metadataLanguage] || data.described_entity.field_value.has_metadata.field_label["en"] || "Has metadata"}</Typography>}
            {data.described_entity.field_value.has_metadata && <div className="padding5">{data.described_entity.field_value.has_metadata.field_value.map((hasMetadataItem, index) => {
                return <SingleIdentifier key={index} data={hasMetadataItem} identifier_name={"metadata_record_identifier"} identifier_scheme={"metadata_record_identifier_scheme"} metadataLanguage={metadataLanguage} />
            })}</div>}

            {is_archived_by && <Typography className="bold-p--id"> {is_archived_by.field_label[metadataLanguage] || is_archived_by.field_label["en"] || "Is archived by"}</Typography>}
            {is_archived_by && <div className="padding5">  {is_archived_by.field_value.map((isArchivedByItem, index) => {
                let resource_name = (isArchivedByItem.resource_name.field_value[metadataLanguage] || isArchivedByItem.resource_name.field_value[Object.keys(isArchivedByItem.resource_name.field_value)[0]]) || "";
                let version = isArchivedByItem.version ? "(" + isArchivedByItem.version.field_value + ")" : '';
                //const is_archived_by_label = is_archived_by.field_label[metadataLanguage] || is_archived_by.field_label["en"] || "Is archived by";
                let classid = "info_value";
                if (resource_name.includes('untitled harvested')) {classid="show_hidden"}
                let full_metadata_record = commonParser.getFullMetadata(isArchivedByItem.full_metadata_record);
                return <div key={index}>
                    {full_metadata_record ?
                        <div className="internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span> {resource_name} {version} </span>
                            </Link>
                            <GeneralIdentifier data={isArchivedByItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </div> :
                        resource_name && <div><div className={classid}>{resource_name} {version} </div>
                            <GeneralIdentifier data={isArchivedByItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>}
                </div>
            })}</div>}
            {is_continuation_of && <Typography className="bold-p--id"> {is_continuation_of.field_label[metadataLanguage] || is_continuation_of.field_label["en"] || "Is continuation of"}</Typography>}
            {is_continuation_of && <div className="padding5">  {is_continuation_of.field_value.map((isContinuationOfItem, index) => {
                let resource_name = (isContinuationOfItem.resource_name.field_value[metadataLanguage] || isContinuationOfItem.resource_name.field_value[Object.keys(isContinuationOfItem.resource_name.field_value)[0]]) || "";
                let version = isContinuationOfItem.version ? "(" + isContinuationOfItem.version.field_value + ")": '';
                //const is_continuation_of_label = is_continuation_of.field_label[metadataLanguage] || is_continuation_of.field_label["en"] || "Is continuation of";
                let classid = "info_value";
                if (resource_name.includes('untitled harvested')) {classid="show_hidden"}
                let full_metadata_record = commonParser.getFullMetadata(isContinuationOfItem.full_metadata_record);
                return <div key={index}>
                    {full_metadata_record ?
                        <div className="internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span> {resource_name} {version} </span>
                            </Link>
                            <GeneralIdentifier data={isContinuationOfItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </div> :
                        resource_name && <div><div className={classid}> {resource_name} {version} </div>
                            <GeneralIdentifier data={isContinuationOfItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>}
                </div>
            })}</div>}

            {(is_version_of && is_version_of.field_value.resource_name) && <Typography className="bold-p--id">{is_version_of.field_label[metadataLanguage] || is_version_of.field_label["en"]}</Typography>}
            {is_version_of ? (is_version_of.field_value.full_metadata_record) ?
                <div className="padding5 internal_url">
                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                    <Link to={commonParser.getFullMetadata(is_version_of.field_value.full_metadata_record).internalELGUrl}>
                        {is_version_of.field_value.resource_name.field_value[metadataLanguage] || is_version_of.field_value.resource_name.field_value[Object.keys(is_version_of.field_value.resource_name.field_value)[0]]} ({is_version_of.field_value.version?.field_value ? is_version_of.field_value.version.field_value : ''})
                    </Link>
                    <GeneralIdentifier data={is_version_of.field_value} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                </div>
                : (<><div className="info_value">{is_version_of.field_value.resource_name.field_value[metadataLanguage] || is_version_of.field_value.resource_name.field_value[Object.keys(is_version_of.field_value.resource_name.field_value)[0]]} ({is_version_of.field_value.version?.field_value ? is_version_of.field_value.version.field_value : ''}) </div>
                    <GeneralIdentifier data={is_version_of.field_value} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></>)

                : <></>
            }
            {data.described_entity.field_value.is_related_to_lr && <Typography className="bold-p--id"> {data.described_entity.field_value.is_related_to_lr.field_label[metadataLanguage] || data.described_entity.field_value.is_related_to_lr.field_label["en"] || "Is similar to"}</Typography>}
                    {data.described_entity.field_value.is_related_to_lr && <div className="padding5">{data.described_entity.field_value.is_related_to_lr.field_value.map((similarToItem, index) => {
                        let resource_name = (similarToItem.resource_name.field_value[metadataLanguage] || similarToItem.resource_name.field_value[Object.keys(similarToItem.resource_name.field_value)[0]]) || "";
                        let classid = "info_value";
                        if (resource_name.includes('untitled harvested')) {classid="show_hidden"}
                        let version = similarToItem?.version?.field_value ? "(" + similarToItem.version.field_value + ")": '';
                        //const similar_to_label = data.described_entity.field_value.is_related_to_lr.field_label[metadataLanguage] || data.described_entity.field_value.is_related_to_lr.field_label["en"] || "Is similar to";
                        let full_metadata_record = commonParser.getFullMetadata(similarToItem.full_metadata_record);
                        return <div key={index}>
                            {full_metadata_record ? <>
                                <div className="internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span>{resource_name} {version} </span>
                                    </Link>
                                </div>
                                <GeneralIdentifier data={similarToItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </> :
                                resource_name && <>
                                    <div className={classid}> { resource_name } {version} </div>
                                    <GeneralIdentifier data={similarToItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></>}
                        </div>
                    })}</div>}
                    
            <div className="padding5"></div>
            {relation && <div className="padding5">{relation.field_value.map((relationItem, index) => {
                let relation_type = relationItem.relation_type ? (relationItem.relation_type.field_value[metadataLanguage] || relationItem.relation_type.field_value[Object.keys(relationItem.relation_type.field_value)[0]]) : "";
                let related_lr = relationItem.related_lr ? (relationItem.related_lr.field_value.resource_name.field_value[metadataLanguage] || relationItem.related_lr.field_value.resource_name.field_value[Object.keys(relationItem.related_lr.field_value.resource_name.field_value)[0]]) : "";
                let version = (relationItem.related_lr && relationItem.related_lr.field_value.version) ? (relationItem.related_lr.field_value.version.field_value || undefined) : "";
                let full_metadata_record = relationItem.related_lr ? commonParser.getFullMetadata(relationItem.related_lr.field_value.full_metadata_record) : null;
                return <div key={index}>
                    {(full_metadata_record || relation_type) && <Typography className="bold-p--id"> {relation_type} </Typography>}
                    {full_metadata_record ?
                        <div className=" padding5 internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span> {related_lr ? related_lr : ''} ({version ? version : ''})</span>
                            </Link>
                            <GeneralIdentifier data={relationItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </div> :
                        relation_type && <div><div className="info_value"> {related_lr ? related_lr : ''} ({version ? version : ''})</div>
                            <GeneralIdentifier data={relationItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>}
                </div>
            })}</div>}

            {is_annotated_version_of &&
                <div className="padding5">
                    <Typography className="bold-p--id">{is_annotated_version_of.field_label[metadataLanguage] || is_annotated_version_of.field_label["en"]}</Typography>
                    {
                        commonParser.getFullMetadata(is_annotated_version_of.field_value.full_metadata_record) ?
                            <div className="internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={commonParser.getFullMetadata(is_annotated_version_of.field_value.full_metadata_record).internalELGUrl}>
                                    <span className="info_value">
                                        {is_annotated_version_of.field_value.resource_name.field_value[metadataLanguage] || is_annotated_version_of.field_value.resource_name.field_value[Object.keys(is_annotated_version_of.field_value.resource_name.field_value)[0]]}
                                        <span> </span>({is_annotated_version_of.field_value.version && is_annotated_version_of.field_value.version?.field_value})
                                    </span>
                                </Link>
                                <GeneralIdentifier data={is_annotated_version_of.field_value} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div>
                            :
                            <div>
                                <div className="info_value">
                                    {is_annotated_version_of.field_value.resource_name.field_value[metadataLanguage] || is_annotated_version_of.field_value.resource_name.field_value[Object.keys(is_annotated_version_of.field_value.resource_name.field_value)[0]]}
                                    <span> </span> ({is_annotated_version_of.field_value.version && is_annotated_version_of.field_value.version?.field_value})
                                </div>
                                <GeneralIdentifier data={is_annotated_version_of.field_value} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div>
                    }
                </div>
            }

            {is_aligned_version_of &&
                <div className="padding5">
                    <Typography className="bold-p--id">{is_aligned_version_of.field_label[metadataLanguage] || is_aligned_version_of.field_label["en"]}</Typography>
                    {
                        commonParser.getFullMetadata(is_aligned_version_of.field_value.full_metadata_record) ?
                            <div className="internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={commonParser.getFullMetadata(is_aligned_version_of.field_value.full_metadata_record).internalELGUrl}>
                                    <span className="info_value">
                                        {is_aligned_version_of.field_value.resource_name.field_value[metadataLanguage] || is_aligned_version_of.field_value.resource_name.field_value[Object.keys(is_aligned_version_of.field_value.resource_name.field_value)[0]]}
                                    <span> </span>({is_aligned_version_of.field_value.version && is_aligned_version_of.field_value.version?.field_value})
                                    </span>
                                </Link>
                                <GeneralIdentifier data={is_aligned_version_of.field_value} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div>
                            :
                            <div>
                                <div className="info_value">
                                    {is_aligned_version_of.field_value.resource_name.field_value[metadataLanguage] || is_aligned_version_of.field_value.resource_name.field_value[Object.keys(is_aligned_version_of.field_value.resource_name.field_value)[0]]}
                                    <span> </span> ({is_aligned_version_of.field_value.version && is_aligned_version_of.field_value.version?.field_value})
                                </div>
                                <GeneralIdentifier data={is_aligned_version_of.field_value} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div>
                    }
                </div>
            }
            {is_analysed_by && is_analysed_by.field_value.length > 0 && <div className="padding5">  <Typography className="bold-p--id">{is_analysed_by.field_label[metadataLanguage] || is_analysed_by.field_label["en"]}</Typography>
                {is_analysed_by && is_analysed_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version?.field_value ?  "(" + item.version.field_value + ")": '';
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    if (resource_name.includes('untitled harvested')) {resource_name=""}
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name} {version ? version : ''}</span>
                                </Link>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div> :
                            <div><div className="info_value">{resource_name} {version ? version : ''}</div>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>
                        }
                    </div>
                })}</div>}
            {is_elicited_by && is_elicited_by.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_elicited_by.field_label[metadataLanguage] || is_elicited_by.field_label["en"]}</Typography>
                {is_elicited_by && is_elicited_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    if (resource_name.includes('untitled harvested')) {resource_name=""}
                    let version = item.version?.field_value ?  "(" + item.version.field_value + ")": '';
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name} {version ? version: ''}</span>
                                </Link>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div> :
                            <div><div className="info_value">{resource_name} {version ? version : ''}</div>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>
                        }
                    </div>
                })}</div>}

            {is_compatible_with && is_compatible_with.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_compatible_with.field_label[metadataLanguage] || is_compatible_with.field_label["en"]}</Typography>
                {is_compatible_with && is_compatible_with.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    if (resource_name.includes('untitled harvested')) {resource_name=""}
                    let version = item.version?.field_value ?  "(" + item.version.field_value + ")": '';
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name} {version ? version : ''}</span>
                                </Link>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div> :
                            <div><div className="info_value">{resource_name} {version ? version : ''}</div>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>
                        }
                    </div>
                })}</div>}

            {is_supplemented_by && is_supplemented_by.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_supplemented_by.field_label[metadataLanguage] || is_supplemented_by.field_label["en"]}</Typography>
                {is_supplemented_by && is_supplemented_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    if (resource_name.includes('untitled harvested')) {resource_name=""}
                    let version = item.version?.field_value ?  "(" + item.version.field_value + ")": '';
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name} {version ? version : ''}</span>
                                </Link>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div> :
                            <div><div className="info_value">{resource_name} {version ? version : ''}</div>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>
                        }
                    </div>
                })}</div>}

            {is_supplement_to && is_supplement_to.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_supplement_to.field_label[metadataLanguage] || is_supplement_to.field_label["en"]}</Typography>
                {is_supplement_to && is_supplement_to.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    if (resource_name.includes('untitled harvested')) {resource_name=""}
                    let version = item.version?.field_value ?  "(" + item.version.field_value + ")": '';
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name} {version ? version : ''}</span>
                                </Link>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div> :
                            <div><div className="info_value">{resource_name} {version ? version : ''}</div>
                                <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></div>
                        }
                    </div>
                })}</div>}

            <ReverseRelations data={data} metadataLanguage={metadataLanguage} showTitle={false} />

        </>


    }
}