import React from "react";
import Grid from '@material-ui/core/Grid';
import { TOOL_SERVICE } from "../../config/constants";
import messages from "../../config/messages";

//this component is used for the landing page but also for the snippet
//I addded a snippet prop, that is true only when HosteAtElg is called inside the snippet (ResourceComponentList)
//if service has proxied===true, but snippet also is true -> dont show proxied flag on snippet  
//if service has proxied===false or null, show the elg-compatible service on snippet and landing page
// if service  has proxied===true, but snippet == false, show the proxied flag, this time it is visible only on landing pages 

export default class HostedAtElg extends React.Component {
    constructor(props) {
        super(props);
        this.isUnmount = false;
    }

    render() {
        const { functional_service, resource_type, has_data, xSize, jystify, alignItems, proxied, snippet } = this.props;
        return (
            <Grid container direction="column" justifyContent={jystify} alignItems={alignItems}>
                <Grid item xs={xSize}>                 
                    {(resource_type === "ToolService" || resource_type === TOOL_SERVICE) && functional_service === true && (!proxied || proxied === false) && <div className="pt-1"><span className="caption grey--font ui grey right ribbon label">{messages.label_functional_service}</span></div>}
                    {(resource_type === "ToolService" || resource_type === TOOL_SERVICE) && functional_service === true && (proxied === true && !snippet) && <div className="pt-1"><span className="caption grey--font ui purple right ribbon label">{messages.label_functional_service_proxied}</span></div>}
                    {(resource_type === "ToolService" || resource_type === TOOL_SERVICE) && functional_service === true && (proxied === true && snippet) && <div className="pt-1"><span className="caption grey--font ui grey right ribbon label">{messages.label_functional_service}</span></div>}
                    {(resource_type !== "ToolService" || resource_type !== TOOL_SERVICE) && has_data && <div className="pt-1"><span className="caption grey--font ui grey right ribbon label">{messages.label_hosted_at_elg}</span></div>}
                </Grid>
            </Grid>
        );

    }
}