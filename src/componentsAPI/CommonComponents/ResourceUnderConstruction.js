import React from "react";
import Grid from '@material-ui/core/Grid';

export default class ResourceUnderConstruction extends React.Component {
    constructor(props) {
        super(props);
        this.isUnmount = false;
    }

    render() {
        const { under_construction, xSize, jystify, alignItems } = this.props;

        return (
            <Grid container direction="column" justifyContent={jystify} alignItems={alignItems}>
                <Grid item xs={xSize}>
                    { /*status ==="p" && <span className="caption grey--font ui purple right ribbon label">published</span>*/}
                    {under_construction === true && <div className="pt-1"><span className="caption grey--font ui grey right ribbon label">work in progress</span></div>}
                </Grid>
            </Grid>
        );

    }
}