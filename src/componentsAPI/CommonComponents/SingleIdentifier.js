
import React from "react";


export default class SingleIdentifier extends React.Component {
    render() {
        const { data, identifier_name, identifier_scheme } = this.props;
        //console.log(data[identifier_name].field_value[identifier_scheme])
        const filteredIdentifier = data[identifier_name] && data[identifier_name].field_value[identifier_scheme].label=== "ELG";    
        const identifier = data[identifier_name].field_value[identifier_scheme].label["en"] !== "ELG" ? data[identifier_name].field_value[identifier_scheme].label["en"] : "";//filter out elg identifiers 
        const value = data[identifier_name].field_value.value.field_value || "";

        if (filteredIdentifier) {
            return <div></div>
        }
        return <>
            {/*
                filteredIdentifier && <div><span className="bold-p--id">{data[identifier_name].field_label[metadataLanguage] || data[identifier_name].field_label["en"]}</span></div>
            */ }
            {identifier && <div>
                <span className="info_value">{value} ({identifier}) </span></div> 
              
            } 
          
        </>
    }
}




