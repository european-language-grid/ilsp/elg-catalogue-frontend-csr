import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import { TabContent, TabPane, } from "reactstrap";
import Container from '@material-ui/core/Container';
//import { ReactComponent as BackIcon } from "./../assets/elg-icons/navigation-arrows-left-1.svg";
import GoToCatalogue from "./CommonComponents/GoToCatalogue";
import corpusParser from "../parsers/corpusParser";
import commonParser from "../parsers/CommonParser";
import ResourceHeader from "./CommonComponents/ResourceHeader";
import ResourceLifeCyrcleInfo from "./CommonComponents/ResourceLifeCyrcleInfo";
import NavigationTabs from "./corpusReusableComponents/NavigationTabs";
//import Keywords from "./CommonComponents/Keywords";
//import Description from "./CommonComponents/Description"; 
import DescriptionRichText from "./CommonComponents/DescriptionRichText";
import Attribution from "./CommonComponents/Attribution";
import ResourceActor from "./CommonComponents/ResourceActor";
import ExportMetadata from "./CommonComponents/ExportMetadata";
//import ShareMetadata from "./CommonComponents/ShareMetadata";
import FundedBy from "./corpusReusableComponents/FundedBy";
import AdditionalInfo from "./CommonComponents/AdditionalInfo";
import ProgressBar from "./CommonComponents/ProgressBar";
//import Replaces from "./CommonComponents/Replaces";
//import IsReplacedWith from "./CommonComponents/IsReplacedWith";
import Relations from "./CommonComponents/Relations";
import CorpusImage from "../assets/images/corpus.png";
import Documentations from "./corpusReusableComponents/Documentations";
import Creation from "./corpusReusableComponents/Creation";
import Ethics from "./CommonComponents/Ethics";
import ActualUse from "./corpusReusableComponents/ActualUse";
import Validation from "./corpusReusableComponents/Validation";
import HelmetMetaData from "./CommonComponents/HelmetMetaData";
import CorpusGeneralCategories from "./corpusReusableComponents/CorpusGeneralCategories";
//import CorpusGeneral from "./corpusReusableComponents/CorpusGeneral";
import { SERVER_API_METADATARECORD, RETRIEVE_RECORD_INFO, getLogoutUrl, IS_ADMIN } from "../config/constants";
import CorpusMediaPart from "./corpusReusableComponents/CorpusMediaPart";
import CorpusUnspecifiedPart from "./corpusReusableComponents/CorpusUnspecifiedPart";
import Distributions from "./corpusReusableComponents/Distributions";
//import Annotation from "./corpusReusableComponents/Annotation";
//import HasSubset from "./corpusReusableComponents/HasSubset";
import ResourceImage from "./../assets/elg-icons/database.svg";
import Versions from "./CommonComponents/Versions";
import RecordStats from "./CommonComponents/RecordStats";

function select_size(first_column, second_column, third_column) {
    let grid_size = 12;

    if (first_column && second_column && third_column) {
        grid_size = 4;
    }
    if (first_column && !second_column && !third_column) {
        grid_size = 12;
    }
    if ((first_column && second_column && !third_column) || (!first_column && second_column && third_column) || (first_column && !second_column && third_column)) {
        grid_size = 6;
    }
    return grid_size;

}

export default class Corpus extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', metadataLanguage: '', tab: '1', showEthics: false, showDocs: false, showCreation: false, showArea: false, displayTabGeneral: false,
            pk: "",
            loading: true, number_of_failed_loads: 0,
            isOwner: false, isLegalValidator: false, isMetadataValidator: false, isTechnicalValidator: false, source: null,
            routeTab: null
        };
    }

    componentDidMount() {
        this.getMetadataResourceDetail();
        this.setState({ number_of_failed_loads: 0 });
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        clearTimeout(this.timeOutId);
    }

    displayTabGeneralFunction = (flag) => {
        this.setState({ displayTabGeneral: flag });
    }

    showEthicsFunction = (flag) => {
        this.setState({ showEthics: flag });
    }
    showDocsFunction = (flag) => {
        this.setState({ showDocs: flag });
    }

    showCreationFunction = (flag) => {
        this.setState({ showCreation: flag });
    }

    showAreaFunction = (flag) => {
        this.setState({ showArea: flag });
    }


    toggleTab = (tabTitles, tabIndex) => {
        //this.setState({ tab: tabIndex }); 
        try {
            const tab2Route = tabTitles[tabIndex - 1];
            const encodedTab2Route = encodeURIComponent(tab2Route.toLowerCase());
            this.props.history.push(`/corpus/${this.state.pk}/${encodedTab2Route}/`);
        } catch (err) {
            console.log(err);
        }
    }

    updateRecord = () => {
        this.getMetadataResourceDetail();
    }

    retrieveRecordInfo = (id, source) => {
        if (this.props.keycloak && this.props.keycloak.authenticated) {
            axios.get(RETRIEVE_RECORD_INFO(id), { cancelToken: source.token })
                .then(res => {
                    const { curator = false, legal_validator = false, metadata_validator = false, technical_validator = false } = res.data;
                    this.setState({ isOwner: curator, isLegalValidator: legal_validator, isMetadataValidator: metadata_validator, isTechnicalValidator: technical_validator });
                })
                .catch(err => console.log(err));
        }
    }

    getMetadataResourceDetail() {
        const id = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const tab = this.props && this.props.match && this.props.match.params && this.props.match.params.tab;
        const url = SERVER_API_METADATARECORD(id);
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true, routeTab: tab });
        this.retrieveRecordInfo(id, source);
        axios.get(url)
            .then(res => {
                this.setState(res);
                const lang = Object.keys(res.data.described_entity.field_value.resource_name.field_value).includes("en") ? "en" : Object.keys(res.data.described_entity.field_value.resource_name.field_value)[0];
                this.setState({ metadataLanguage: lang, pk: id, source: null, loading: false, number_of_failed_loads: 0 });
            }).catch(err => {
                console.log(err);
                this.setState({ source: null, loading: false });
            });
    }

    componentDidUpdate(prevProps, prevState) {
        const thisId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const prevId = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.id;
        const thisTab = this.props && this.props.match && this.props.match.params && this.props.match.params.tab;
        const prevTab = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.tab;
        if (thisId !== prevId) {
            this.setState({ data: '', metadataLanguage: '', tab: '1', displayTabGeneral: false });
            this.getMetadataResourceDetail();
        }
        else if (thisTab !== prevTab) {
            this.setState({ routeTab: thisTab });
        }
    }

    reloadPage = () => {
        const number_of_failed_loads = this.state.number_of_failed_loads;
        if (number_of_failed_loads < 3) {
            this.setState({ number_of_failed_loads: number_of_failed_loads + 1 });
            this.getMetadataResourceDetail();
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        } else {
            this.timeOutId = setTimeout(() => { window.location = getLogoutUrl() }, 5000)
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        }
    }

    render() {
        if (this.props.keycloak && !this.props.keycloak.authenticated) {
            if (commonParser.redirectUnregisteredUsersToKeycloak(this.props.location)) {
                this.props.keycloak.login();
            }
        }

        if (this.state.loading) {
            return <ProgressBar />
        } else {
            if (this.state.data) {
            } else {
                return this.reloadPage();
            }
        }

        const { data, metadataLanguage } = this.state;
        const languages = [];// ["english", "spanish"];
        const resourceName = corpusParser.getResourceName(data, metadataLanguage) ? corpusParser.getResourceName(data, metadataLanguage).value : "";
        const corpusLogo = corpusParser.getLogo(data);
        const short_name = corpusParser.getResourceShortName(data, metadataLanguage) ? corpusParser.getResourceShortName(data, metadataLanguage).value : "";
        const version = corpusParser.getVersion(data, metadataLanguage) ? corpusParser.getVersion(data, metadataLanguage).value : "";
        const version_label = corpusParser.getVersion(data, metadataLanguage) ? corpusParser.getVersion(data, metadataLanguage).label : "";
        const version_date = corpusParser.getVersionDate(data, metadataLanguage) ? corpusParser.getVersionDate(data).value : "";
        const resource_type = corpusParser.getLRType(data, metadataLanguage) ? corpusParser.getLRType(data, metadataLanguage).value : null;
        const entity_type = corpusParser.getEntityType(data, metadataLanguage) ? corpusParser.getEntityType(data, metadataLanguage).value : null;
        //const update_frequency = corpusParser.getUpdateFrequency(data, metadataLanguage);
        const description = corpusParser.getDescription(data, metadataLanguage) ? corpusParser.getDescription(data, metadataLanguage).value : null;
        const keywords = corpusParser.getKeywords(data, metadataLanguage) ? corpusParser.getKeywords(data, metadataLanguage).value : [];
        const keywords_label = corpusParser.getKeywords(data, metadataLanguage) ? corpusParser.getKeywords(data, metadataLanguage).label : "";
        const domainKeywords = corpusParser.getDomainKeywords(data, metadataLanguage) ? corpusParser.getDomainKeywords(data, metadataLanguage).value : [];
        const domainKeywords_label = corpusParser.getDomainKeywords(data, metadataLanguage) ? corpusParser.getDomainKeywords(data, metadataLanguage).label : "";
        const subjectKeywords = corpusParser.getSubjectKeywords(data, metadataLanguage) ? corpusParser.getSubjectKeywords(data, metadataLanguage).value : [];
        const subjectKeywords_label = corpusParser.getSubjectKeywords(data, metadataLanguage) ? corpusParser.getSubjectKeywords(data, metadataLanguage).label : "";
        const intentedKeywords = corpusParser.getIntendedKeywords(data, metadataLanguage) ? corpusParser.getIntendedKeywords(data, metadataLanguage).value : [];
        const intentedKeywords_label = corpusParser.getIntendedKeywords(data, metadataLanguage) ? corpusParser.getIntendedKeywords(data, metadataLanguage).label : "";
        const citationText = corpusParser.getCitationText(data, metadataLanguage) ? corpusParser.getCitationText(data, metadataLanguage).value : "";
        const citationText_label = corpusParser.getCitationText(data, metadataLanguage) ? corpusParser.getCitationText(data, metadataLanguage).label : "Cite as";
        const citation_all_versions = corpusParser.getCitationAllVersions(data, metadataLanguage) ? corpusParser.getCitationAllVersions(data, metadataLanguage).value : "";
        const citation_all_versions_label = corpusParser.getCitationAllVersions(data, metadataLanguage) ? corpusParser.getCitationAllVersions(data, metadataLanguage).label : "Cite as";
        const mailingList = corpusParser.getMailingList(data, metadataLanguage) ? corpusParser.getMailingList(data, metadataLanguage).value : [];
        const corpus_subclass = corpusParser.getCorpusSubclass(data, metadataLanguage) ? corpusParser.getCorpusSubclass(data, metadataLanguage).value : [];
        const corpus_subclass_label = corpusParser.getCorpusSubclass(data, metadataLanguage) ? corpusParser.getCorpusSubclass(data, metadataLanguage).label : "";
        const lr_subclass = data.described_entity.field_value.lr_subclass ? data.described_entity.field_value.lr_subclass.field_value : null;
        const discussionUrl = corpusParser.getDiscussionUrl(data, metadataLanguage) ? corpusParser.getDiscussionUrl(data, metadataLanguage).value : [];
        //const discussionUrl_label = corpusParser.getDiscussionUrl(data,metadataLanguage)?corpusParser.getDiscussionUrl(data,metadataLanguage).label:"";
        const additioanlInfo = corpusParser.getAdditionalInfo(data, metadataLanguage) ? corpusParser.getAdditionalInfo(data, metadataLanguage).value : [];
        const additioanlInfo_label = corpusParser.getAdditionalInfo(data, metadataLanguage) ? corpusParser.getAdditionalInfo(data, metadataLanguage).label : "";
        //const physical_resource = corpusParser.getPhysicalResource(data, metadataLanguage) ? corpusParser.getPhysicalResource(data, metadataLanguage).value : [];
        //const physical_resource_label = corpusParser.getPhysicalResource(data, metadataLanguage) ? corpusParser.getPhysicalResource(data, metadataLanguage).label : "";
        const personal_data_included = corpusParser.getPersonalDataIncluded(data, metadataLanguage) ? corpusParser.getPersonalDataIncluded(data, metadataLanguage).value : null;
        const personal_data_included_label = corpusParser.getPersonalDataIncluded(data, metadataLanguage) ? corpusParser.getPersonalDataIncluded(data, metadataLanguage).label : "";
        const personal_data_details = corpusParser.getPersonalDataDetails(data, metadataLanguage) ? corpusParser.getPersonalDataDetails(data, metadataLanguage).value : [];
        const personal_data_details_label = corpusParser.getPersonalDataDetails(data, metadataLanguage) ? corpusParser.getPersonalDataDetails(data, metadataLanguage).label : "";
        const sensitive_data_included = corpusParser.getSensitiveDataIncluded(data, metadataLanguage) ? corpusParser.getSensitiveDataIncluded(data, metadataLanguage).value : null;
        const sensitive_data_included_label = corpusParser.getSensitiveDataIncluded(data, metadataLanguage) ? corpusParser.getSensitiveDataIncluded(data, metadataLanguage).label : "";
        const sensitive_data_details = corpusParser.getSensitiveDataDetails(data, metadataLanguage) ? corpusParser.getSensitiveDataDetails(data, metadataLanguage).value : [];
        const sensitive_data_details_label = corpusParser.getSensitiveDataDetails(data, metadataLanguage) ? corpusParser.getSensitiveDataDetails(data, metadataLanguage).label : "";
        const anonymized = corpusParser.getAnonymized(data, metadataLanguage) ? corpusParser.getAnonymized(data, metadataLanguage).value : null;
        const anonymized_label = corpusParser.getAnonymized(data, metadataLanguage) ? corpusParser.getAnonymized(data, metadataLanguage).label : "";
        const anonymization_details = corpusParser.getAnonymizedDetails(data, metadataLanguage) ? corpusParser.getAnonymizedDetails(data, metadataLanguage).value : [];
        const anonymization_details_label = corpusParser.getAnonymizedDetails(data, metadataLanguage) ? corpusParser.getAnonymizedDetails(data, metadataLanguage).label : "";
        const all_versions = data.described_entity.field_value.all_versions ? data.described_entity.field_value.all_versions : null;
        const all_versions_label = data.described_entity.field_value.all_versions ? (data.described_entity.field_value.all_versions.field_label[metadataLanguage] || data.described_entity.field_value.all_versions.field_label["en"]) : "";
        const { under_construction = false } = data.management_object || {};
        const { for_information_only = false } = data.management_object || {};
        const { is_latest_version = false } = data.management_object || {};
        const { tombstone = false } = data.management_object || {};
        const { inferred_language = false } = data.management_object || {};
        const { versions_exceeding_limit = false } = data.management_object || {};
        const tabTitles = ["Overview"];
        if (!under_construction) {
            tabTitles.push("Download");
        } else if (IS_ADMIN(this.props.keycloak)) {
            tabTitles.push("Download");
        }
        if (this.state.displayTabGeneral) {
            tabTitles.push("Related LRTs");
        }
        const tab2SetAsActive = commonParser.translateRouteTabToID(tabTitles, this.state.routeTab);
        if (this.state.tab !== tab2SetAsActive) {
            this.setState({ tab: tab2SetAsActive });
            return <></>;
        }
        //control the number of columns to appear
        const first_column = this.state.showDocs || this.state.showCreation || data.described_entity.field_value.ipr_holder;
        const second_column = data.described_entity.field_value.actual_use || data.described_entity.field_value.validated;
        const third_column = this.state.showEthics || this.state.showArea || this.state.showLcrMedia;
        const grid_size = select_size(first_column, second_column, third_column);

        return <div>
            <HelmetMetaData data={data}
                resourceName={resourceName}
                description={description}
                keywords={keywords}
                domainKeywords={domainKeywords}
                subjectKeywords={subjectKeywords}
                intentedKeywords={intentedKeywords}
                corpus_subclass={corpus_subclass}
                languages={languages}
                resourceShortName={short_name}
                for_information_only={for_information_only}
                ltAreaKeywordsArray={null}
                disciplines={null}
                servicesOffered={null}
                OrganizationRolesArray={null}
                ld_subclass={null}
                lcr_subclass={null}
                type="corpus"
                pk={this.state.pk}
            />
            <div className="search-top">
                <GoToCatalogue />
            </div>
            <Container maxWidth="xl">
                <div className="metadata-main-card-container">
                    <ResourceLifeCyrcleInfo key={data.management_object.status} status={data.management_object.status} keycloak={this.props.keycloak} />
                    {is_latest_version === false && data.management_object.status === "p" && under_construction === false && <section>
                        <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{"There is a "}<span className="bold">{"newer version "}</span>{"of this record available"}</span></div>
                    </section>}
                    <ResourceHeader
                        key={data.management_object ? 'header' + JSON.stringify(data.management_object) : 'header' + this.state.pk}
                        {...this.state}
                        logo={corpusLogo}
                        title={resourceName}
                        short_name={short_name}
                        version={version}
                        version_label={version_label}
                        version_Date={version_date}
                        resource_type={resource_type}
                        proxied={null}
                        tombstone={tombstone}
                        entity_type={entity_type} shortnameArray={null} default_image={ResourceImage} under_construction={under_construction} for_information_only={for_information_only} keycloak={this.props.keycloak} data={data} pk={this.state.pk} updateRecord={this.updateRecord}
                        citationText={citationText} citationText_label={citationText_label}
                        citation_all_versions={citation_all_versions} citation_all_versions_label={citation_all_versions_label}
                        keywordsArray={keywords}
                        languagesArray={languages}
                        domainKeywordsArray={domainKeywords}
                        subjectKeywordsArray={subjectKeywords}
                        intentedKeywordsArray={intentedKeywords}
                        corpus_subclass={corpus_subclass}
                        corpus_subclassLabel={corpus_subclass_label}
                        KeywordsLabel={keywords_label}
                        DomainKeywordsLabel={domainKeywords_label}
                        subjectKeywordsLabel={subjectKeywords_label}
                        intentedKeywordsLabel={intentedKeywords_label} inferred_language={inferred_language} description={description} url_type="corpus" status={data.management_object.status}
                        duplicate={data.management_object.duplicate} potential_duplicate={data.management_object.potential_duplicate} />
                </div>
                {(!tombstone || tombstone === "F") &&
                    <div className="tab-pane-container">
                        <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} tabTitles={tabTitles} />
                        <TabContent activeTab={this.state.tab}>
                            <TabPane tabId={`${tabTitles.indexOf('Overview') + 1}`}>
                                <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
                                    <Grid item xs={12} sm={8} md={8} className="search-results-main" >
                                        <DescriptionRichText description={description} />
                                        {/*<Keywords keywordsArray={keywords}
                                        languagesArray={languages}
                                        domainKeywordsArray={domainKeywords}
                                        subjectKeywordsArray={subjectKeywords}
                                        intentedKeywordsArray={intentedKeywords}
                                        corpus_subclass={corpus_subclass}
                                        corpus_subclassLabel={corpus_subclass_label}
                                        KeywordsLabel={keywords_label}
                                        DomainKeywordsLabel={domainKeywords_label}
                                        subjectKeywordsLabel={subjectKeywords_label}
                                        intentedKeywordsLabel={intentedKeywords_label} />*/}

                                        <CorpusMediaPart data={data} metadataLanguage={metadataLanguage} />
                                        <CorpusUnspecifiedPart unspecified_part={lr_subclass.unspecified_part} metadataLanguage={metadataLanguage} inferred_language={inferred_language} />
                                    </Grid>
                                    <Grid item xs={12} sm={4} md={4} className="MetadataSidebar" >

                                        {/*data.management_object.status === "p" && <ShareMetadata shareUrl={`${BASE_URL}catalogue/corpus/${this.state.pk}`} title={resourceName} description={description} logo={corpusLogo} />*/}
                                        <RecordStats pk={this.state.pk} />
                                        <Versions all_versions={all_versions} all_versions_label={all_versions_label} metadataLanguage={metadataLanguage} versions_exceeding_limit={versions_exceeding_limit} additioanlInfo={additioanlInfo} />
                                        <div className="ActionsButtonArea">
                                            {/*<Replaces data={data} metadataLanguage={metadataLanguage} />*/}
                                            {/*<IsReplacedWith data={data} metadataLanguage={metadataLanguage} />*/}
                                            <ResourceActor data={this.state.data.described_entity.field_value.resource_provider} metadataLanguage={metadataLanguage} />
                                            <AdditionalInfo additioanlInfo={additioanlInfo}
                                                mailingList={mailingList}
                                                discussionUrl={discussionUrl}
                                                data={data}
                                                metadataLanguage={metadataLanguage} AdditionalInfoLabel={additioanlInfo_label}
                                                identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"}
                                                {...commonParser.getSourceOfMetadataRecord(data, metadataLanguage)} />
                                            <FundedBy data={data} metadataLanguage={metadataLanguage} CorpusImage={CorpusImage} navigateToProjectDetail={this.navigateToProjectDetail} />

                                        </div>
                                        <ExportMetadata pk={this.state.pk} under_construction={under_construction} for_information_only={for_information_only} keycloak={this.props.keycloak} data={this.state.data} name={resourceName} {...this.state} />
                                    </Grid>
                                </Grid>
                                {
                                    <Grid container spacing={2} className="MetaDataDetailsBottomContainer">
                                        <Grid item xs={12} sm={grid_size}>
                                            <Documentations data={data} metadataLanguage={metadataLanguage} showDocsFunction={this.showDocsFunction} showDocs={this.state.showDocs} />
                                            <Creation data={data} metadataLanguage={metadataLanguage} showCreationFunction={this.showCreationFunction} showCreation={this.state.showCreation} />
                                            <ResourceActor data={data.described_entity.field_value.ipr_holder} metadataLanguage={metadataLanguage} />
                                            <Attribution data={data} metadataLanguage={metadataLanguage} />
                                        </Grid>


                                        {second_column && <Grid item xs={12} sm={grid_size} >
                                            <ActualUse data={data} metadataLanguage={metadataLanguage} />
                                            <Validation data={data} metadataLanguage={metadataLanguage} />
                                        </Grid>}

                                        <Grid item xs={12} sm={grid_size} >
                                            <Ethics personal_data_included={personal_data_included}
                                                personal_data_details={personal_data_details}
                                                sensitive_data_included={sensitive_data_included}
                                                sensitive_data_details={sensitive_data_details}
                                                anonymized={anonymized}
                                                anonymization_details={anonymization_details}
                                                personal_data_included_label={personal_data_included_label}
                                                personal_data_details_label={personal_data_details_label}
                                                sensitive_data_included_label={sensitive_data_included_label}
                                                sensitive_data_details_label={sensitive_data_details_label}
                                                anonymized_label={anonymized_label}
                                                anonymization_details_label={anonymization_details_label}
                                                showEthicsFunction={this.showEthicsFunction}
                                                showEthics={this.state.showEthics}
                                            />
                                            <CorpusGeneralCategories data={data} metadataLanguage={metadataLanguage} title="Corpus categories" showAreaFunction={this.showAreaFunction} showArea={this.state.showArea} />

                                        </Grid>


                                    </Grid>
                                }
                            </TabPane>

                            {(!under_construction || (under_construction && IS_ADMIN(this.props.keycloak))) && <TabPane tabId={`${tabTitles.indexOf('Download') + 1}`}>
                                <Distributions data={data} metadataLanguage={metadataLanguage} {...this.state} />
                            </TabPane>}

                            <TabPane tabId={`${tabTitles.indexOf('Related LRTs') + 1}`}>
                                <Relations data={this.state.data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />
                            </TabPane>

                        </TabContent>
                    </div>
                }
            </Container>

        </div >
    }

}