import React from "react";
import { Helmet } from "react-helmet";
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import GoToCatalogue from "./CommonComponents/GoToCatalogue";

export default class ErrorUnauthorized extends React.Component {

    render() {
        return <>
            <Helmet>
                <title>ELG</title>
            </Helmet>
            <div className="search-top">
                <GoToCatalogue />
            </div>
            <div className="fullWhiteBackground mb-2 mt-2 centered-text">
                <Container maxWidth="lg" >
                    <Grid container spacing={2} direction="row" justifyContent="center" alignItems="center">
                        <Grid item xs={12} sm={12} className="content-inner mb-2 mt-5">
                            <Typography variant="h1" className="pt-1 mt-5">401</Typography>
                            <Typography variant="h3">Unauthorized</Typography>
                        </Grid>
                    </Grid>
                </Container >
            </div>
        </>
    }
}

