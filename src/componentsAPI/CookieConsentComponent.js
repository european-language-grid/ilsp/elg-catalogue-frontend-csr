import React from "react";
import CookieConsent from "react-cookie-consent";
import { COOKIE_CONSENT_POLICY_URL, PRODUCTION } from "../config/constants";
import ReactGA from 'react-ga';
export default class CookieConsentComponent extends React.Component {
    constructor(props) {
        super(props);
        this.onCookiesAccept = this.onCookiesAccept.bind(this);
    }
    onCookiesAccept() {
        if (PRODUCTION) {
            ReactGA.initialize('UA-136240201-2', {
                debug: !PRODUCTION,
                gaOptions: {
                    // siteSpeedSampleRate: 100
                }
            });
        }
    }
    render() {
        return <div>
            <CookieConsent
                location="bottom"
                cookieName="cookieconsent_status"
                buttonText="Allow Cookies"
                cookieValue="allow"
                enableDeclineButton={true}
                declineButtonText="Refuse Cookies"
                declineCookieValue="deny"
                //extraCookieOptions={{ domain: '.elg-front.ilsp.gr' }}
                extraCookieOptions={{ domain: '.european-language-grid.eu' }}
                onAccept={this.onCookiesAccept}
                style={{ background: "#eceff1" }}
                contentClasses="cookie__content pt-1 pb-1 p-1"
                buttonStyle={{ color: "#fff", borderRadius: "4px", fontWeight: "600" }}
                declineButtonStyle={{ color: "#299ba3" }}
                declineButtonClasses="mx-3 mat-flat-button color-transparent decline-button"
                buttonClasses="px-5 mx-0 mr-1 mr-md-5 mat-flat-button color-purple accept-button"
            >
                ELG uses cookies for functional and analytical purposes. Please agree to our Cookie Policy.
                <span><a href={COOKIE_CONSENT_POLICY_URL}> Learn more</a></span>
            </CookieConsent>
        </div>
    }
}