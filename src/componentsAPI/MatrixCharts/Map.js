import React from "react";
import MapChart from "./MapChart";
import ReactTooltip from "react-tooltip";
import { Alert, AlertTitle } from '@material-ui/lab';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CancelIcon from '@material-ui/icons/Cancel';
import Grid from '@material-ui/core/Grid';

export default class Map extends React.Component {
    constructor(props) {
      super(props);
      this.state = {content:"" , alertContent:"", open: false};
      this.setTooltipContent = this.setTooltipContent.bind(this);
      this.setAlertContent = this.setAlertContent.bind(this);
    }

    setTooltipContent(newContent){
      this.setState({content: newContent});
    }

    setAlertContent(newContent, openState){
      this.setState({alertContent: newContent, open: openState});
    }

  

    render() {
      return <Grid item container xs={12} sm={12} direction="row" justifyContent="flex-start">
        <Grid item xs={9}>
          <MapChart setTooltipContent={this.setTooltipContent} setAlertContent={this.setAlertContent}/>
          <ReactTooltip html={true}>{this.state.content}</ReactTooltip>
        </Grid>  
        <Grid item xs={3}>
          <Collapse in={this.state.open} >
                <Alert
                severity="info"
                action={
                  <IconButton
                      aria-label="close"
                      color="inherit"
                      size="small"
                      onClick={() => {
                          this.setState({ open: false })
                      }}
                  >
                      <CancelIcon />
                  </IconButton>
              }
          >
            <AlertTitle>{this.state.alertContent}</AlertTitle>
            </Alert>
            </Collapse>
        </Grid>   
      </Grid>
    }
}