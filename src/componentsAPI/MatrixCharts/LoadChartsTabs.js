import React from "react";
import Grid from '@material-ui/core/Grid';
import DLECharts from './DLECharts';
import ResourcesChartCrossLanguage from './ResourcesChartCrossLanguage';
import DLETimelineChart from './DLETimelineChart';
import ResourcesRadialBarChart from './ResourcesRadialBarChart';
import ResourcesChartSingleLanguage from './ResourcesChartSingleLanguage';
import ResourcesTableStats from "./ResourcesTableStats";
//import MapLeaflet from "./MapLeaflet";
import About from "./About";
import { TabContent, TabPane, } from "reactstrap";
import NavigationTabs from "./../corpusReusableComponents/NavigationTabs"; 
import { ELE_BASE_URL } from "./../../config/constants";
import eleLogo from "./../../assets/images/rgb_ele__logo--colour.svg";

const tabTitles = ["About", "DLE Scores",
  // "Technological factors stats",
  "Cross-language comparison",
  "Within-language comparison",
  //"Heatmaps and tables",  
  //"Radial bar",
  "Evolution over time",
  
];


const tabTitlesSecondLevel = [ 
  "Histograms",  
  "Heatmaps and tables",  
  "Radial bar",  
];


export default class LoadChartsTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: '1', tabSecodLevel: "1" , 
    };
  }


  toggleTab = (tabs = [], tabIndex) => {
    this.setState({ tab: tabIndex });
  }


  toggleTabSecondLevel = (tabs = [], tabIndex) => {
    this.setState({ tabSecodLevel: tabIndex });
  }

  render() {

    return <div className="tab-pane-container">
      {<Grid item xs={2} sm={2} style={{ float: "right" }}>
        <a href={ELE_BASE_URL}>
          <img className="ele__logo" src={eleLogo} alt="ELE logo" /></a>
      </Grid>}

      <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} tabTitles={tabTitles} />
      <TabContent activeTab={this.state.tab}>
        <TabPane tabId={`${tabTitles.indexOf('About') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <About />
          </Grid>
        </TabPane>
        <TabPane tabId={`${tabTitles.indexOf('DLE Scores') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <DLECharts />
          </Grid>
        </TabPane>

        <TabPane tabId={`${tabTitles.indexOf('Cross-language comparison') + 1}`}>          
        <div className="second-level">
            <NavigationTabs toggleTab={this.toggleTabSecondLevel} activeTab={this.state.tabSecodLevel} tabTitles={tabTitlesSecondLevel}/>
            
            <TabContent activeTab={this.state.tabSecodLevel} >
              <TabPane tabId={`${tabTitlesSecondLevel.indexOf('Histograms') + 1}`}>
                <Grid container spacing={3} direction="row" justifyContent="flex-start" alignItems="stretch" className="MetaDataDetailsMain">
                  <ResourcesChartCrossLanguage />
                </Grid>
              </TabPane>
              <TabPane tabId={`${tabTitlesSecondLevel.indexOf('Heatmaps and tables') + 1}`}>
                <Grid container spacing={3} direction="row" justifyContent="flex-start" alignItems="stretch" className="MetaDataDetailsMain">
                  <ResourcesTableStats />
                </Grid>
              </TabPane>
              <TabPane tabId={`${tabTitlesSecondLevel.indexOf('Radial bar') + 1}`}>
                <Grid container spacing={3} direction="row" justifyContent="flex-start" alignItems="stretch" className="MetaDataDetailsMain">
                  <ResourcesRadialBarChart />
                </Grid>
              </TabPane>
            </TabContent> 
            </div> 
          
        </TabPane>
        <TabPane tabId={`${tabTitles.indexOf('Within-language comparison') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <ResourcesChartSingleLanguage />
          </Grid>
        </TabPane>
             
        <TabPane tabId={`${tabTitles.indexOf('Evolution over time') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <DLETimelineChart />
          </Grid>
        </TabPane>
       
        
      </TabContent> 
    </div> 

  }
}