import React from "react";
import axios from "axios";
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import BarChart from "./BarChart";
import { namedColor, transparentize, getRandomArbitrary, CHART_COLORS } from "./Utils";
import Chip from '@material-ui/core/Chip';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Switch from "@material-ui/core/Switch";
import Fade from "@material-ui/core/Fade";
import RadarChart from "./RadarChart";
import { DLE_LANGUAGES, MATRIX_ELG_DATA } from "./../../config/constants";
const truncated_number_of_items_to_show = 5;

const Functions = [
  "Text Processing",
  "Speech Processing",
  "Support operation",
  "Image/Video Processing",
  "Translation Technologies",
  "Human Computer Interaction",
  "Natural Language Generation",
  "Information Extraction And Information Retrieval",
  "Other",
];

const ResourceTypes = [
  "Corpus",
  "Model",
  "Grammar",
  "Lexical/Conceptual resource",
  "Uncategorized Language Description",
];

const LingualityType = ["monolingual", "bilingual", "multilingual"];
const MediaType = ["text", "audio", "image", "video", "numerical text"];
const TypeOfAccess = ["research use allowed", "derivatives not allowed", "redistribution not allowed", "commercial uses not allowed", "no conditions", "other specific restrictions", "unspecified"];

const verticalBarOptions = {  
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        //text: 'Chart.js Bar Chart'
      }
    }
  };

export default class ResourcesChart extends React.Component {
  constructor(state) {
    super(state);
    this.state = {
      languages: "", checkedLanguagesList: [], checkedResourceList: [], checkedFunctionList: [], isDatasetsCheckAll: false, isSoftwareCheckAll: false, expandedLangEu: false, expandedLangOther: false, 
      chartData: [], LingualityList: [], DataMediaTypeList: [], DataTypeOfAccessList: [], SoftwareInputMediaTypeList: [], SoftwareOutputMediaTypeList: [], SoftwareTypeOfAccessList: [], checked: false,     
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getEULanguages();
  }

   
  clearAllFilters() {
    this.setState({ DataMediaTypeList: [], checkedLanguagesList: [], checkedResourceList: [], checkedFunctionList: [], SoftwareInputMediaTypeList: [] });
    this.setState({ SoftwareOutputMediaTypeList: [], LingualityList: [], SoftwareTypeOfAccessList: [], DataTypeOfAccessList: [] , chartData: [] });
    this.setState({  isDatasetsCheckAll: false }); this.setState({  isSoftwareCheckAll: false , checked: false }); 
  }

  getEULanguages() {
    try {
      axios.get(DLE_LANGUAGES)
        .then(res => {
          this.setState({ languages: res.data.results });
        });
    } catch (e) {
      console.log(e);
      return;
    }
  }

  getELEData(glottolog) {
    let { chartData } = this.state;
    try {
      axios.get(MATRIX_ELG_DATA(glottolog))
        .then(res => {
          chartData.push(res.data);
          this.setState({ chartData });
          //console.log(this.state.chartData);
        });
    } catch (e) {
      console.log(e);
      return;
    }
  }

  addCheckedResource2List = (resource) => {
    this.setState({ isDatasetsCheckAll: false });
    this.setState({ isSoftwareCheckAll: false });
    this.removeAllFunctionFromCheckedList();
    this.setState({ SoftwareOutputMediaTypeList: [], SoftwareInputMediaTypeList: [] , SoftwareTypeOfAccessList: []});
    const { checkedResourceList } = this.state;
    checkedResourceList.push(resource);
    this.setState({ checkedResourceList });
  }

  removeResourceFromCheckedList = (resource) => {
    //this.setState({ isDatasetsCheckAll: false }); //if we remove one Item we keep the all button off
    let { checkedResourceList } = this.state;
    checkedResourceList = checkedResourceList.filter(item => item !== resource);
    this.setState({ checkedResourceList });

  }

  addCheckedFunction2List = (functionItem) => {
    this.setState({ isSoftwareCheckAll: false });
    this.setState({ isDatasetsCheckAll: false });
    this.removeAllResourceFromCheckedList();
    this.setState({ DataMediaTypeList: [], LingualityList: [] , DataTypeOfAccessList: []});
    const { checkedFunctionList } = this.state;
    checkedFunctionList.push(functionItem);
    this.setState({ checkedFunctionList });
    this.setState({ SoftwareOutputMediaTypeList: [], SoftwareInputMediaTypeList: [] , SoftwareTypeOfAccessList: [],}); //pending until next backend change

  }

  removeFunctionFromCheckedList = (functionItem) => {
    //this.setState({ isSoftwareCheckAll: false }); 
    let { checkedFunctionList } = this.state;
    checkedFunctionList = checkedFunctionList.filter(item => item !== functionItem);
    this.setState({ checkedFunctionList });
  }


  removeAllResourceFromCheckedList = () => {
    this.setState({ checkedResourceList: [] });
  }

  removeAllFunctionFromCheckedList = () => {
    //this.setState({ isSoftwareCheckAll: false });
    this.setState({ checkedFunctionList: [] });
  }

  addLangChecked2List = (lang) => {
    const { checkedLanguagesList } = this.state;
    checkedLanguagesList.push(lang);
    this.setState({ checkedLanguagesList });
    this.getELEData(lang);

  }

  removeLangFromCheckedList = (lang) => {
    let { checkedLanguagesList, chartData } = this.state;
    checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
    this.setState({ checkedLanguagesList });
    chartData = chartData.filter(item => item.id !== lang);
    this.setState({ chartData });

  }

  handleDatasetCheckAll = (e) => {
    this.setState({ isDatasetsCheckAll: !this.state.isDatasetsCheckAll });
    if (e.target.checked) {
      this.removeAllResourceFromCheckedList();
      this.removeAllFunctionFromCheckedList();
    }
  }

  handleSoftwareCheckAll = (e) => {
    this.setState({ isSoftwareCheckAll: !this.state.isSoftwareCheckAll });
    if (e.target.checked) {
      this.removeAllFunctionFromCheckedList();
      this.removeAllResourceFromCheckedList();
    }
  }


  AddAnotherGroupToDatasetList(dynamicBarChart, chartData, ListName, type, color_num){
    //console.log(ListName,type)
    ListName.length > 0 && ListName.map((item, idx) => {
      const newData = [];
      let datasetBaseItem = {
        fill: true,
        label: "" , //"Dataset "+ item + " (" + type + ")",
        borderColor: namedColor(getRandomArbitrary(idx+color_num)), //select one color 
        backgroundColor: transparentize(namedColor(getRandomArbitrary(idx+color_num)), 0.5),
        pointBackgroundColor: "rgba(34, 202, 236, 1)",
        poingBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        data: []
      }
      chartData && chartData.length > 0 && chartData.map((language, index) => {           
          Object.entries(language.data.datasets[type]).map(([key, value]) => {
            if (item === key) {
              newData.push(value);
            }
          })        
        datasetBaseItem.data = newData;
        datasetBaseItem.label = "Dataset "+ " (" + this.mapTypetoKeyword(type) +":" + item + ")";
      })
      dynamicBarChart.datasets.push(datasetBaseItem);
    })
  }

  addDataset(dynamicBarChart, chartData, List, color_num) {    
    List.length > 0 && List.map((item, idx) => {
    const newData = [];
    let datasetBaseItem = {
      fill: true,
      label: item,
      borderColor: namedColor(getRandomArbitrary(idx+color_num)), //select one color 
      backgroundColor: transparentize(namedColor(getRandomArbitrary(idx+color_num)), 0.5),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }
    chartData && chartData.length > 0 && chartData.map((language, index) => {
      List.length > 0 && Object.entries(language.data.datasets.resource_type).map(([key, value]) => {
        if (item === key) {
          newData.push(value.count);
        }
      })
      datasetBaseItem.data = newData;
    })
    dynamicBarChart.datasets.push(datasetBaseItem);
  })
  return dynamicBarChart; 
}

addDatasetLevel2(dynamicBarChart, chartData, List, element, type , color_num) {    
  List.length > 0 && List.map((item, idx) => {
  const newData = [];
  let datasetBaseItem = {
    fill: true,
    label: "",
    borderColor: namedColor(getRandomArbitrary(idx+color_num)), //select one color 
    backgroundColor: transparentize(namedColor(getRandomArbitrary(idx+color_num)), 0.5),
    pointBackgroundColor: "rgba(34, 202, 236, 1)",
    poingBorderColor: "#fff",
    pointHoverBackgroundColor: "#fff",
    data: []
  }
  chartData && chartData.length > 0 && chartData.map((language, index) => {
    Object.entries(language.data.datasets.resource_type[item][type]).map(([key, value]) => {
      if (element === key) {
        newData.push(value);
        datasetBaseItem.label = item + " (" + this.mapTypetoKeyword(type) +":" + element + ")";
      }
    })
    datasetBaseItem.data = newData;
  })
  dynamicBarChart.datasets.push(datasetBaseItem);
})
return dynamicBarChart; 
}

addFunction(dynamicBarChart, chartData, List, color_num) {    
  List.length > 0 && List.map((item, idx) => {
    const newData = [];
    let datasetBaseItem = {
      fill: true,
      label: item,
      borderColor: namedColor(getRandomArbitrary(idx+color_num)), //select one color 
      backgroundColor: transparentize(namedColor(getRandomArbitrary(idx+color_num)), 0.3),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }
    chartData && chartData.length > 0 && chartData.map((language, index) => {
      List.length > 0 && Object.entries(language.data.software.functions).map(([key, value]) => {
        if (item === key) {
          newData.push(value.count);
        }
      })
      datasetBaseItem.data = newData;
    })
    dynamicBarChart.datasets.push(datasetBaseItem);
  })
  return dynamicBarChart; 
} 

addFunctionLevel2(dynamicBarChart, chartData, List, element, type , color_num) {    
  List.length > 0 && List.map((item, idx) => {
  const newData = [];
  let datasetBaseItem = {
    fill: true,
    label: "",
    borderColor: namedColor(getRandomArbitrary(idx + color_num)), //select one color 
    backgroundColor: transparentize(namedColor(getRandomArbitrary(idx + color_num)), 0.3),
    pointBackgroundColor: "rgba(34, 202, 236, 1)",
    poingBorderColor: "#fff",
    pointHoverBackgroundColor: "#fff",
    data: []
  }
  chartData && chartData.length > 0 && chartData.map((language, index) => {
    //console.log(language.id)
    Object.entries(language.data.software.functions[item][type]).map(([key, value]) => {
      if (element === key) {
        newData.push(value);
        datasetBaseItem.label = item + " (" + this.mapTypetoKeyword(type) +":" + element + ")";
      }
    })
    datasetBaseItem.data = newData;
  })
  dynamicBarChart.datasets.push(datasetBaseItem);
})
return dynamicBarChart; 
}


addRadarData(dynamicRadarChart,   checkedResourceList, chartData) { 
  chartData && chartData.length > 0 && chartData.map((language, index) => {     
    let filterLanguageName = this.state.languages.filter(item => item.glottocode === language.id) || "";
    //console.log(filterLanguageName)
    let datasetBaseItem = {
      fill: true,
      label: filterLanguageName[0].preferred_name,
      borderColor: namedColor(getRandomArbitrary(index)), //select one color 
      backgroundColor: transparentize(namedColor(getRandomArbitrary(index)), 0.5),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }    
    checkedResourceList && checkedResourceList.length > 0 && checkedResourceList.map((item, idx) => {
      Object.entries(language.data.datasets.resource_type).map(([key, value]) => {
        if (item === key) {
          datasetBaseItem.data.push(value.count);
        }
      })
    })
    dynamicRadarChart.datasets.push(datasetBaseItem);
  })    
   return dynamicRadarChart;   
}

addSoftwareData(dynamicRadarChart,   checkedFunctionList, chartData) {   
  chartData && chartData.length > 0 && chartData.map((language, index) => {
    let filterLanguageName = this.state.languages.filter(item => item.glottocode === language.id) || "";
    console.log(filterLanguageName)
    let datasetBaseItem = {
      fill: true,
      label: filterLanguageName[0].preferred_name,
      borderColor: namedColor(getRandomArbitrary(index)), //select one color 
      backgroundColor: transparentize(namedColor(getRandomArbitrary(index)), 0.5),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }    
    checkedFunctionList && checkedFunctionList.length > 0 && checkedFunctionList.map((item, idx) => {
      Object.entries(language.data.software.functions).map(([key, value]) => {
        if (item === key) {
          datasetBaseItem.data.push(value.count)
        }
      })
    })
    dynamicRadarChart.datasets.push(datasetBaseItem);
  })    
   return dynamicRadarChart;   
}

addRadarLabels(dynamicRadarChart, indicator) {
  dynamicRadarChart.labels.push(indicator);
  this.addRadarData();
  return dynamicRadarChart;
}

createRadar(chartData){
  let dynamicRadarChart = {
    labels: [], //resource types,
    datasets: [], //per language
  } 
  this.state.checkedResourceList.length > 0 && this.state.checkedResourceList.map((indicator, index) => {
    this.addRadarLabels(dynamicRadarChart, indicator)}
  )

  this.state.checkedFunctionList.length > 0 && this.state.checkedFunctionList.map((indicator, index) => {
    this.addRadarLabels(dynamicRadarChart, indicator)}
  )

  this.state.checkedResourceList && this.state.checkedResourceList.length>0  && this.addRadarData(dynamicRadarChart, this.state.checkedResourceList, chartData);
  this.state.checkedFunctionList && this.state.checkedFunctionList.length>0  && this.addSoftwareData(dynamicRadarChart, this.state.checkedFunctionList, chartData);

   return dynamicRadarChart;
}

AddAnotherGroupToSoftWareList( dynamicBarChart, chartData, ListName, type, color_num ){
    ListName.length > 0 && ListName.map((item, idx) => {
      const newData = [];
      let datasetBaseItem = {
        fill: true,
        label: "", //"Software " + item,
        borderColor: namedColor(getRandomArbitrary(idx+color_num)), //select one color 
        backgroundColor: transparentize(namedColor(getRandomArbitrary(idx+color_num)), 0.5),
        pointBackgroundColor: "rgba(34, 202, 236, 1)",
        poingBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        data: []
      }
      chartData && chartData.length > 0 && chartData.map((language, index) => {
        Object.entries(language.data.software[type]).map(([key, value]) => {
            if (item === key) {
              newData.push(value);
            }
          })
        })
        datasetBaseItem.data = newData;
        datasetBaseItem.label = "Software "+ " (" + this.mapTypetoKeyword(type) +":" + item + ")";
        dynamicBarChart.datasets.push(datasetBaseItem);  
      })        
      return dynamicBarChart;
  }

  MyResourcetoChartJSData(chartData) {
    let dynamicBarChart = {
      labels: [], //types (resource types),
      datasets: [], //per language
    };

    this.state.checkedLanguagesList && this.state.checkedLanguagesList.length > 0 && this.state.checkedLanguagesList.map((language, index) => {
      let filterLanguage = this.state.languages.filter(item => item.glottocode === language) || "";
      let langName = [filterLanguage[0].preferred_name];
      dynamicBarChart.labels.push(langName);
    })

    this.state.checkedResourceList.length > 0 && this.addDataset(dynamicBarChart, chartData, this.state.checkedResourceList, 4)
    this.state.checkedFunctionList.length > 0 && this.addFunction(dynamicBarChart, chartData, this.state.checkedFunctionList, 5)

    this.state.LingualityList.length > 0 && this.state.LingualityList.map((element, idx) => {
      this.addDatasetLevel2(dynamicBarChart, chartData, this.state.checkedResourceList, element,"linguality", 1);  
    })
    this.state.DataMediaTypeList.length > 0 && this.state.DataMediaTypeList.map((element, idx) => {
      this.addDatasetLevel2(dynamicBarChart, chartData, this.state.checkedResourceList, element,"media_type", 2);  
    })
    this.state.DataTypeOfAccessList.length > 0 && this.state.DataTypeOfAccessList.map((element, idx) => {
      this.addDatasetLevel2(dynamicBarChart, chartData, this.state.checkedResourceList, element,"type_of_access", 3);  
    })
    
    this.state.SoftwareInputMediaTypeList.length > 0 && this.state.SoftwareInputMediaTypeList.map((element, idx) => {
      this.addFunctionLevel2(dynamicBarChart, chartData, this.state.checkedFunctionList, element,"linguality", 1);  
    })
    this.state.SoftwareOutputMediaTypeList.length > 0 && this.state.SoftwareOutputMediaTypeList.map((element, idx) => {
      this.addFunctionLevel2(dynamicBarChart, chartData, this.state.checkedFunctionList, element,"media_type", 2);  
    })
    this.state.SoftwareTypeOfAccessList.length > 0 && this.state.SoftwareTypeOfAccessList.map((element, idx) => {
      this.addFunctionLevel2(dynamicBarChart, chartData, this.state.checkedFunctionList, element,"type_of_access", 3);  
    })

    if (this.state.isDatasetsCheckAll === true) {
      const newData = [];
      let datasetBaseItem = {
        fill: true,
        label: "Datasets",
        borderColor: CHART_COLORS.highlightTeal, //select one color 
        backgroundColor: transparentize(CHART_COLORS.highlightTealDark, 0.3),
        pointBackgroundColor: "rgba(34, 202, 236, 1)",
        poingBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        data: []
      }
      chartData && chartData.length > 0 && chartData.map((language, index) => {
        if (this.state.isDatasetsCheckAll === true) {
          newData.push(language.data.datasets.count);
        }
        datasetBaseItem.data = newData;
      })
      dynamicBarChart.datasets.push(datasetBaseItem);
      this.state.LingualityList.length > 0 && this.AddAnotherGroupToDatasetList(dynamicBarChart,chartData,this.state.LingualityList, "linguality", 1);
      this.state.DataMediaTypeList.length > 0 && this.AddAnotherGroupToDatasetList( dynamicBarChart, chartData, this.state.DataMediaTypeList, "media_type", 2 );
      this.state.DataTypeOfAccessList.length > 0 && this.AddAnotherGroupToDatasetList( dynamicBarChart, chartData, this.state.DataTypeOfAccessList, "type_of_access", 3 );     
    }

    if (this.state.isSoftwareCheckAll === true) {
      const newData = [];
      let datasetBaseItem = {
        fill: true,
        label: "Software",
        borderColor: CHART_COLORS.elgYellow, //select one color 
        backgroundColor: transparentize(CHART_COLORS.elgYellow, 0.2),
        pointBackgroundColor: "rgba(34, 202, 236, 1)",
        poingBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        data: []
      }
      chartData && chartData.length > 0 && chartData.map((language, index) => {
        if (this.state.isSoftwareCheckAll === true) {
          newData.push(language.data.software.count);
        }
        datasetBaseItem.data = newData;
      })
      dynamicBarChart.datasets.push(datasetBaseItem);
      this.state.SoftwareInputMediaTypeList.length > 0 && this.AddAnotherGroupToSoftWareList(dynamicBarChart, chartData, this.state.SoftwareInputMediaTypeList, "input_media_type", 1);
      this.state.SoftwareOutputMediaTypeList.length > 0 && this.AddAnotherGroupToSoftWareList(dynamicBarChart, chartData, this.state.SoftwareOutputMediaTypeList, "output_media_type", 2);
      this.state.SoftwareTypeOfAccessList.length > 0 && this.AddAnotherGroupToSoftWareList(dynamicBarChart, chartData, this.state.SoftwareTypeOfAccessList, "type_of_access", 3);
    }
    return dynamicBarChart;
  }

  
  mapTypetoKeyword(param) {
    switch(param) {
      case 'type_of_access':
        return 'type of access';
      case 'input_media_type':
        return 'input';  
      case 'output_media_type':
        return 'output'; 
      case 'media_type':
         return 'media type'; 
      case 'linguality':
        return 'linguality';           
      default:
        return '';
    }
  }

  handleLangCheck = (e) => {
    if (e.target.checked) {
      this.addLangChecked2List(e.target.name);
    } else {
      this.removeLangFromCheckedList(e.target.name);
    }
  }

  handleResourceCheck = (e) => {
    if (e.target.checked) {
      this.addCheckedResource2List(e.target.name);
    } else {
      this.removeResourceFromCheckedList(e.target.name);
    }
  }

  handleFunctionCheck = (e) => {
    if (e.target.checked) {
      this.addCheckedFunction2List(e.target.name);
    } else {
      this.removeFunctionFromCheckedList(e.target.name);
    }
  }

  addLinguality2List = (type, index) => {
    const { LingualityList } = this.state;
    LingualityList.push(type);
    this.setState({ LingualityList });
  }

  removeLingualityFromList = (type) => {
    let { LingualityList, chartData } = this.state;
    LingualityList = LingualityList.filter(item => item !== type);
    this.setState({ LingualityList });
    chartData = chartData.filter(item => item.id !== type);
    this.setState({ chartData });
  }


  addDataMediaType2List = (type) => {
    const { DataMediaTypeList } = this.state;
    DataMediaTypeList.push(type);
    this.setState({ DataMediaTypeList });
  }

  removeDataMediaTypeFromList = (type) => {
    let { DataMediaTypeList, chartData } = this.state;
    DataMediaTypeList = DataMediaTypeList.filter(item => item !== type);
    this.setState({ DataMediaTypeList });
    chartData = chartData.filter(item => item.id !== type);
    this.setState({ chartData });
  }

  handleLingualityTypeClick = (type) => {
    this.addLinguality2List(type);
  }

  handleLingualityTypeDelete = (type) => {
    this.removeLingualityFromList(type);
  }

  handleDataMediaTypeClick = (type) => {
    this.addDataMediaType2List(type);
  }

  handleDataMediaTypeDelete = (type) => {
    this.removeDataMediaTypeFromList(type);
  }


  addDataTypeOfAccess2List = (type) => {
    const { DataTypeOfAccessList } = this.state;
    DataTypeOfAccessList.push(type);
    this.setState({ DataTypeOfAccessList });
  }

  removeDataTypeOfAccessFromList = (type) => {
    let { DataTypeOfAccessList, chartData } = this.state;
    DataTypeOfAccessList = DataTypeOfAccessList.filter(item => item !== type);
    this.setState({ DataTypeOfAccessList });
    chartData = chartData.filter(item => item.id !== type);
    this.setState({ chartData });
  }

  handleDataTypeOfAccessClick = (type) => {
    this.addDataTypeOfAccess2List(type);
  }

  handleDataTypeOfAccessDelete = (type) => {
    this.removeDataTypeOfAccessFromList(type);
  }
  addSoftwareInputMediaType2List = (type) => {
    const { SoftwareInputMediaTypeList } = this.state;
    SoftwareInputMediaTypeList.push(type);
    this.setState({ SoftwareInputMediaTypeList });
  }

  removeSoftwareInputMediaTypeFromList = (type) => {
    let { SoftwareInputMediaTypeList, chartData } = this.state;
    SoftwareInputMediaTypeList = SoftwareInputMediaTypeList.filter(item => item !== type);
    this.setState({ SoftwareInputMediaTypeList });
    chartData = chartData.filter(item => item.id !== type);
    this.setState({ chartData });
  }

  handleSoftwareInputMediaTypeClick = (type) => {
    this.addSoftwareInputMediaType2List(type);
  }

  handleSoftwareInputMediaTypeDelete = (type) => {
    this.removeSoftwareInputMediaTypeFromList(type);
  }
  addSoftwareOutputMediaType2List = (type) => {
    const { SoftwareOutputMediaTypeList } = this.state;
    SoftwareOutputMediaTypeList.push(type);
    this.setState({ SoftwareOutputMediaTypeList });
  }

  removeSoftwareOutputMediaTypeFromList = (type) => {
    let { SoftwareOutputMediaTypeList, chartData } = this.state;
    SoftwareOutputMediaTypeList = SoftwareOutputMediaTypeList.filter(item => item !== type);
    this.setState({ SoftwareOutputMediaTypeList });
    chartData = chartData.filter(item => item.id !== type);
    this.setState({ chartData });
  }

  handleSoftwareOutputMediaTypeClick = (type) => {
    this.addSoftwareOutputMediaType2List(type);
  }

  handleSoftwareOutputMediaTypeDelete = (type) => {
    this.removeSoftwareOutputMediaTypeFromList(type);
  }
  addSoftwareTypeOfAccess2List = (type, index) => {
    const { SoftwareTypeOfAccessList } = this.state;
    SoftwareTypeOfAccessList.push(type);
    this.setState({ SoftwareTypeOfAccessList });
  }

  removeSoftwareTypeOfAccessFromList = (type) => {
    let { SoftwareTypeOfAccessList, chartData } = this.state;
    SoftwareTypeOfAccessList = SoftwareTypeOfAccessList.filter(item => item !== type);
    this.setState({ SoftwareTypeOfAccessList });
    chartData = chartData.filter(item => item.id !== type);
    this.setState({ chartData });
  }

  handleSoftwareTypeOfAccessClick = (type) => {
    this.addSoftwareTypeOfAccess2List(type);
  }

  handleSoftwareTypeOfAccessDelete = (type) => {
    this.removeSoftwareTypeOfAccessFromList(type);
  }

  handleChange() {
    this.setState({ checked: !this.state.checked });
  };

  togglexpandedLangEu = () => {
    this.setState({ expandedLangEu: !this.state.expandedLangEu })
  }

  togglexpandedLangOther = () => {
    this.setState({ expandedLangOther: !this.state.expandedLangOther })
  }

  render() {
    const { chartData , expandedLangEu, expandedLangOther} = this.state;
    //const displayData = this.MyResourcetoChartArray(chartData);
    const dynamicBarChart = this.MyResourcetoChartJSData(chartData);
    const dynamicRadarChart = this.createRadar(chartData); 
    //console.log(chartData)
    //console.log(this.state.languages)
    //console.log(dynamicBarChart)
    return <Grid container className="chart-container" spacing={1} direction="row">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters">
          <Grid container direction="column">
            <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <FormGroup>
                {expandedLangEu? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>
            <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <FormGroup>
                {expandedLangOther ? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>
          </Grid>
          
        </Paper>
      </Grid>
      <Grid item sm={10}>
        <Grid container direction="column">
          <Grid item>
          <Accordion defaultExpanded className="selection-panel">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon style={{ fontSize: 30 }} className="grey--font"/>}
          aria-controls="panel1c-content"
          id="panel1c-header" className="chart-header"> <Typography variant="h4" className="bold">Filter by:</Typography> </AccordionSummary>
        <AccordionDetails>             
              <Grid container  spacing={3} direction="row">                
                <Grid item container sm={12} direction="row" justifyContent = "space-between" className="selection-row"> 
                  <Grid item container sm={6}  direction="row">
                    <Grid item container sm={12} direction="row"   alignItems="flex-start" justifyContent="flex-start">
                      <Grid item sm={2}>
                        <FormControlLabel className="pl05"
                          control={<Checkbox
                            checked={this.state.isDatasetsCheckAll}
                            //indeterminate={this.allInDiffStateResourceType()}
                            color="primary"
                            onChange={e => { this.handleDatasetCheckAll(e) }}
                            name={"datasets"} />}
                          label={"Datasets"}
                        />
                      </Grid>
                      <Grid item container sm={10} direction="row">
                      <Grid item sm={12} className="bg-light-grey rounded pl-03"><Typography variant="caption">Resource subclass</Typography></Grid>
                        {ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map((type, index) => {
                          return <Grid item sm={6} key={index}>
                            <FormControlLabel className="pl05"
                              control={<Checkbox
                                checked={this.state.checkedResourceList.find(item => item === type) ? true : false}
                                color="primary"
                                onChange={e => { this.handleResourceCheck(e) }}
                                name={type} />}
                              label={type}
                            />
                          </Grid>
                        })}
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item container sm={6}   spacing={2} direction="row">
                    <Grid item container sm={12} direction="row">
                      <Grid item sm={3}><Typography variant="caption" className="bold">Linguality type:</Typography></Grid>
                      {LingualityType && LingualityType.length > 0 && LingualityType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small"
                            className="ChipTagMatrix"
                            variant={this.state.LingualityList.find(item => item === type) ? "default" : "outlined"}
                            label={type}
                            onClick={() => this.handleLingualityTypeClick(type)}
                            onDelete={this.state.LingualityList.find(item => item === type) && (() => this.handleLingualityTypeDelete(type))}
                            disabled= {(this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll) && this.state.DataMediaTypeList.length === 0 && this.state.DataTypeOfAccessList.length === 0 ? false : true }
                          />
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12}direction="row">
                    <Grid item sm={3}><Typography variant="caption" className="bold">Media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small"
                          className="ChipTagMatrix"
                            label={type}
                            variant={this.state.DataMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onClick={() => this.handleDataMediaTypeClick(type)}
                            onDelete={this.state.DataMediaTypeList.find(item => item === type) && (() => this.handleDataMediaTypeDelete(type))}
                            disabled= {(this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll)  && this.state.LingualityList.length === 0 && this.state.DataTypeOfAccessList.length === 0  ? false : true }
                          />
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Type Of Access:</Typography></Grid>
                    <Grid item container sm={9}>{TypeOfAccess && TypeOfAccess.length > 0 && TypeOfAccess.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.DataTypeOfAccessList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.DataTypeOfAccessList.find(item => item === type) && (() => this.handleDataTypeOfAccessDelete(type))}
                            onClick={() => this.handleDataTypeOfAccessClick(type, index)}
                            disabled= {(this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll) && this.state.DataMediaTypeList.length === 0 && this.state.LingualityList.length === 0 ? false : true }
                          />
                        </Grid>
                      })}
                      </Grid>  
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item container sm={12}  direction="row" justifyContent = "space-between">
                  <Grid item container sm={6} direction="row">
                  <Grid item container sm={12} direction="row"   alignItems="flex-start" justifyContent="flex-start">
                    <Grid item sm={2}>
                      <FormControlLabel className="pl05"
                        control={<Checkbox
                          checked={this.state.isSoftwareCheckAll}
                          //indeterminate={this.allInDiffStateFunction()}
                          color="primary"
                          onChange={e => { this.handleSoftwareCheckAll(e) }}
                          name={"software"} />}
                        label={"Software"}
                      />
                    </Grid>
                    <Grid item container sm={10} direction="row">
                    <Grid item sm={12} className="bg-light-grey rounded pl-03"><Typography variant="caption">Functions</Typography></Grid>
                      {Functions && Functions.length > 0 && Functions.map((type, index) => {
                        return <Grid item sm={6} key={index} >
                          <FormControlLabel className="pl05"
                            control={<Checkbox
                              checked={this.state.checkedFunctionList.find(item => item === type) ? true : false}
                              color="primary"
                              onChange={e => { this.handleFunctionCheck(e) }}
                              name={type} />}
                            label={type}
                          />
                        </Grid>
                      })}
                    </Grid>
                  </Grid>
                  </Grid>
                  
                  <Grid item container sm={6}  spacing={2} direction="row">
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Input media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareInputMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareInputMediaTypeList.find(item => item === type) && (() => this.handleSoftwareInputMediaTypeDelete(type))}
                            onClick={() => this.handleSoftwareInputMediaTypeClick(type, index)} 
                            disabled= {this.state.checkedFunctionList.length>0 || !this.state.isSoftwareCheckAll ? true : false }/>
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Output media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareOutputMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareOutputMediaTypeList.find(item => item === type) && (() => this.handleSoftwareOutputMediaTypeDelete(type))}
                            onClick={() => this.handleSoftwareOutputMediaTypeClick(type, index)} 
                            disabled= {this.state.checkedFunctionList.length>0 || !this.state.isSoftwareCheckAll ? true : false }/>
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12}  direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Type Of Access:</Typography></Grid>
                    <Grid item container sm={9}>  {TypeOfAccess && TypeOfAccess.length > 0 && TypeOfAccess.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareTypeOfAccessList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareTypeOfAccessList.find(item => item === type) && (() => this.handleSoftwareTypeOfAccessDelete(type))}
                            onClick={() => this.handleSoftwareTypeOfAccessClick(type, index)} 
                            disabled= {this.state.checkedFunctionList.length>0 || this.state.isSoftwareCheckAll ? false : true }/>
                        </Grid>
                      })}</Grid>
                    </Grid>
                  </Grid>
                </Grid>

                  {this.state.chartData && this.state.chartData.length > 0 && <Grid item sm={12}>
                    <Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip>
                  </Grid>}
                </Grid>
              </AccordionDetails>
            </Accordion>
          </Grid>

          {dynamicBarChart.datasets.length > 2 && this.state.checkedLanguagesList.length > 2 &&
                    <Grid item container sm={12} justifyContent = "flex-end" className="p-1"><FormControlLabel
                      control={<Switch checked={this.state.checked} onChange={this.handleChange} />}
                      label="View as Radar" /></Grid>}

          {dynamicBarChart.datasets && dynamicBarChart.datasets.length > 0 && <Grid item sm={12}><BarChart BarData={dynamicBarChart} options={verticalBarOptions} /></Grid>}  

            <Fade in={this.state.checked}>
            <Grid item sm={12}>{dynamicRadarChart.datasets && dynamicRadarChart.datasets.length > 2 &&  <RadarChart RadarData={dynamicRadarChart} /> } </Grid>         
            </Fade>    


          </Grid>        
      </Grid>
    </Grid>
  }
}