import React from "react";
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
//import Tooltip from '@material-ui/core/Tooltip';

export default class PeriodsAutocomplete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      DlePeriods: this.props.DlePeriods,
      disabled: this.props.disabled,
      DlePeriod: "",
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      DlePeriods: nextProps.DlePeriods || [],
    };
  }

  setValues = (selValue, newValue) => {
    let { DlePeriod } = this.state;
    DlePeriod = newValue;
    this.setState({ DlePeriod });
    this.props.updateDLEData(DlePeriod, this.props.label, selValue);
    //this.props.resetPeriod();
  }

  //onBlur = () => {
  //console.log(this.state.quarterDlePeriod)
  //  this.props.updateDLEData(this.state.DlePeriod);    
  //}

  renderAutocomplete = (selectedPeriod, DlePeriods) => {
    return <Autocomplete
      id="grouped-demo"
      fullWidth
      blurOnSelect={true}
      clearOnBlur={true}
      //disabled={Boolean(disabled)}
      value={selectedPeriod || ""}
      options={DlePeriods}
      groupBy={(option) => option.year}
      getOptionLabel={(option) => option.label}
      getOptionSelected={(option, value) => option.value === value.value}
      onChange={(event, newValue) => {
        const selValue = newValue ? newValue.value : "";
        this.setValues(newValue, selValue);
      }}
      renderInput={(params) => (
        <TextField
          {...params}
          variant="outlined"
          label={this.props.label}
        />
      )}
    />
  }

  render() {
    const DlePeriods = this.state.DlePeriods;
    const selectedPeriod = this.props.selectedPeriod;
    return <div onBlur={this.onBlur}>
      {this.renderAutocomplete(selectedPeriod, DlePeriods)}
    </div>
  }
}
