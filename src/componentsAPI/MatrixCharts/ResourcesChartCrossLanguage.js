import React from "react";
import axios from "axios";
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import BarChart from "./BarChart";
import { namedColor, transparentize, getRandomArbitrary, mapLabelsToColors, Functions, ResourceTypes, LingualityType, MediaType, TypeOfAccess } from "./Utils";
import Chip from '@material-ui/core/Chip';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AccordionSummary from '@material-ui/core/AccordionSummary';
//import Switch from "@material-ui/core/Switch";
//import Fade from "@material-ui/core/Fade";
//import RadarChart from "./RadarChart";
import { DLE_LANGUAGES, MATRIX_ELG_DATA_INDEXER } from "./../../config/constants";

const truncated_number_of_items_to_show = 6;



const verticalBarOptions = {  
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        //text: 'Chart.js Bar Chart'
      }
    }
  };
 
export default class ResourcesChartCrossLanguage extends React.Component {
  constructor(state) {
    super(state);
    this.state = {
      languages: "", checkedLanguagesList: [], checkedResourceList: [], checkedFunctionList: [], isDatasetsCheckAll: false, isSoftwareCheckAll: false,
      chartData: [], LingualityList: [], DataMediaTypeList: [], DataTypeOfAccessList: [], SoftwareInputMediaTypeList: [], SoftwareOutputMediaTypeList: [], SoftwareTypeOfAccessList: [], checked: false,  
      AndTerms: [],  expandedLangEu: false, expandedLangOther: false, 
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() { this.getEULanguages(); }
  togglexpandedLangEu = () => { this.setState({ expandedLangEu: !this.state.expandedLangEu }) }
  togglexpandedLangOther = () => { this.setState({ expandedLangOther: !this.state.expandedLangOther }) }
   
  clearAllFilters() {
    this.setState({ DataMediaTypeList: [], checkedLanguagesList: [], checkedResourceList: [], checkedFunctionList: [], SoftwareInputMediaTypeList: [] });
    this.setState({ SoftwareOutputMediaTypeList: [], LingualityList: [], SoftwareTypeOfAccessList: [], DataTypeOfAccessList: [] , chartData: [] });
    this.setState({  isDatasetsCheckAll: false }); this.setState({  isSoftwareCheckAll: false , checked: false }); 
    this.setState({  AndTerms:[]})
  }

  getEULanguages() {
    try {
      axios.get(DLE_LANGUAGES)
        .then(res => {
          this.setState({ languages: res.data.results });
          //console.log(res.data.results)
        });
    } catch (e) { console.log(e); return; }
  }

  addCheckedResource2List = (resource) => {
    this.setState({ isDatasetsCheckAll: false });
    this.setState({ isSoftwareCheckAll: false });       
    this.removeAllFunctionFromCheckedList();
    this.removeSoftwareInputMediaTypeFromList();
    this.removeSoftwareOutputMediaTypeFromList();
    this.removeSoftwareTypeOfAccessFromList();
    this.setState({ SoftwareOutputMediaTypeList: [], SoftwareInputMediaTypeList: [] , SoftwareTypeOfAccessList: []});
    const { checkedResourceList } = this.state;
    checkedResourceList.push(resource);
    this.setState({ checkedResourceList });        
    let selectedString2 =  "resource_type=" + encodeURIComponent(resource) ;
    this.markAndTermAsSelected(selectedString2);       
  }

  removeResourceFromCheckedList = (resource) => {     
    let { checkedResourceList } = this.state;
    checkedResourceList = checkedResourceList.filter(item => item !== resource);   
    this.setState({ checkedResourceList });
    let selectedString2 =  "resource_type=" + encodeURIComponent(resource) ;
    this.handleUnselectedAndTerm(selectedString2);     
  }

  addCheckedFunction2List = (functionItem) => {
    this.setState({ isSoftwareCheckAll: false });
    this.setState({ isDatasetsCheckAll: false });
    this.removeAllResourceFromCheckedList();
    this.removeAllFromLingualityList();
    this.removeAllFromDataMediaTypeList();
    this.removeAllFromDataTypeOfAccessList()
    this.setState({ DataMediaTypeList: [], LingualityList: [] , DataTypeOfAccessList: []});
    const { checkedFunctionList } = this.state;
    checkedFunctionList.push(functionItem);
    this.setState({ checkedFunctionList });        
    this.setState({ SoftwareOutputMediaTypeList: [], SoftwareInputMediaTypeList: [] , SoftwareTypeOfAccessList: [],}); //pending until next backend change
    let selectedString2 =  "function=" + encodeURIComponent(functionItem);
     
    this.markAndTermAsSelected(selectedString2);    
  }

  removeFunctionFromCheckedList = (functionItem) => {     
    let { checkedFunctionList } = this.state;
    checkedFunctionList = checkedFunctionList.filter(item => item !== functionItem);    
    this.setState({ checkedFunctionList });
    let selectedString2 = "function=" + encodeURIComponent(functionItem);   
    this.handleUnselectedAndTerm(selectedString2);    
  }


  removeAllResourceFromCheckedList = () => {      
    this.setState({ checkedResourceList: [] });    
  }

  removeAllFunctionFromCheckedList = () => {     
    this.setState({ checkedFunctionList: [] });    
  }

  removeAllFromLingualityList=()=>{ this.setState({ LingualityList: [] });  }
  removeAllFromDataMediaTypeList=()=>{this.setState({ DataMediaTypeList: [] });  }
  removeAllFromDataTypeOfAccessList=()=>{this.setState({ DataTypeOfAccessList: [] });  }
  removeSoftwareTypeofAccessList=()=> { this.setState({ SoftwareTypeOfAccessList: [] }); }
  //removeSoftwareInputMediaTypeFromList=()=> {this.setState({ SoftwareInputMediaTypeList: [] }); }
  //removeSoftwareOutputMediaTypeFromList=()=> {this.setState({ SoftwareOutputMediaTypeList: [] }); }

  addLangChecked2List = (lang) => {
    const { checkedLanguagesList } = this.state;
    checkedLanguagesList.push(lang);
    this.setState({ checkedLanguagesList });   
  }

  removeLangFromCheckedList = (lang) => {
    let { checkedLanguagesList, chartData } = this.state;
    checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
    this.setState({ checkedLanguagesList });
    chartData = chartData.filter(item => item.id !== lang);
    this.setState({ chartData });

  }

  markAndTermAsSelected = (facetName) => {
    const { AndTerms } = this.state;
    if (AndTerms.indexOf(facetName) === -1) {//checks if i have allready selected the facet, if not add it to array
      AndTerms.push(facetName);
      this.setState({ AndTerms });     
    }
  }

  handleUnselectedAndTerm = (facetName) => {
    const { AndTerms, chartData } = this.state;
    let tmpAndTerms = AndTerms.filter(item => item !== facetName);
    this.setState({ AndTerms: tmpAndTerms });
    let TmpchartData = chartData.filter(item => !item.query.includes(facetName));
    this.setState({chartData: TmpchartData})
  }

  handleDatasetCheckAll = (e) => {
    this.setState({ isDatasetsCheckAll: !this.state.isDatasetsCheckAll });
    if (e.target.checked) {
      this.removeAllResourceFromCheckedList();
      this.removeAllFunctionFromCheckedList(); 
      this.removeSoftwareTypeofAccessList();
      this.removeSoftwareInputMediaTypeFromList();
      this.removeSoftwareOutputMediaTypeFromList();          
      this.markAndTermAsSelected("lr_distinction=Dataset")
    }
    else {      
      this.handleUnselectedAndTerm("lr_distinction=Dataset");
      }
  }

  handleSoftwareCheckAll = (e) => {
    this.setState({ isSoftwareCheckAll: !this.state.isSoftwareCheckAll });
    if (e.target.checked) {
      this.removeAllFunctionFromCheckedList();
      this.removeAllResourceFromCheckedList();
      this.removeAllFromLingualityList();
      this.removeAllFromDataMediaTypeList();
      this.removeAllFromDataTypeOfAccessList();      
      this.markAndTermAsSelected("lr_distinction=Software")     
    }    
    else {      
      this.handleUnselectedAndTerm("lr_distinction=Software");     
      }
  }
 
  handleLangCheck = (e) => {
    if (e.target.checked) {
      this.addLangChecked2List(e.target.name);
    } else {
      this.removeLangFromCheckedList(e.target.name);
    }
  }

  handleResourceCheck = (e) => {
    if (e.target.checked) {
      this.addCheckedResource2List(e.target.name);
    } else {
      this.removeResourceFromCheckedList(e.target.name);
    }
  }

  handleFunctionCheck = (e) => {
    if (e.target.checked) {
      this.addCheckedFunction2List(e.target.name);
    } else {
      this.removeFunctionFromCheckedList(e.target.name);
    }
  }

  addLinguality2List = (type) => {
    const { LingualityList } = this.state;
    LingualityList.push(type);
    this.setState({ LingualityList });   
  }  

  removeLingualityFromList = (type) => {
    let { LingualityList } = this.state;
    LingualityList = LingualityList.filter(item => item !== type);
    this.setState({ LingualityList });
  }

  addDataMediaType2List = (type) => {
    const { DataMediaTypeList } = this.state;
    this.setState({ isSoftwareCheckAll: false});
    DataMediaTypeList.push(type);
    this.setState({ DataMediaTypeList });  
  }

  removeDataMediaTypeFromList = (type) => {
    let { DataMediaTypeList } = this.state;
    DataMediaTypeList = DataMediaTypeList.filter(item => item !== type);
    this.setState({ DataMediaTypeList });   
    }


  addDataTypeOfAccess2List = (type) => {
    const { DataTypeOfAccessList } = this.state;
    this.setState({ isSoftwareCheckAll: false});
    DataTypeOfAccessList.push(type);
    this.setState({ DataTypeOfAccessList });    
  }

  removeDataTypeOfAccessFromList = (type) => {
    let { DataTypeOfAccessList} = this.state;
    DataTypeOfAccessList = DataTypeOfAccessList.filter(item => item !== type);
    this.setState({ DataTypeOfAccessList }); 
  }


  addSoftwareInputMediaType2List = (type) => {
    const { SoftwareInputMediaTypeList } = this.state;
    SoftwareInputMediaTypeList.push(type);
    this.setState({ SoftwareInputMediaTypeList });    
  }

  removeSoftwareInputMediaTypeFromList = (type) => {
    let { SoftwareInputMediaTypeList } = this.state;
    SoftwareInputMediaTypeList = SoftwareInputMediaTypeList.filter(item => item !== type);
    this.setState({ SoftwareInputMediaTypeList });    
  }

  addSoftwareOutputMediaType2List = (type) => {
    const { SoftwareOutputMediaTypeList } = this.state;
    SoftwareOutputMediaTypeList.push(type);
    this.setState({ SoftwareOutputMediaTypeList });   
  }

  removeSoftwareOutputMediaTypeFromList = (type) => {
    let { SoftwareOutputMediaTypeList } = this.state;
    SoftwareOutputMediaTypeList = SoftwareOutputMediaTypeList.filter(item => item !== type);
    this.setState({ SoftwareOutputMediaTypeList });     
  }

  addSoftwareTypeOfAccess2List = (type, index) => {
    const { SoftwareTypeOfAccessList } = this.state;
    SoftwareTypeOfAccessList.push(type);
    this.setState({ SoftwareTypeOfAccessList });   
  }

  removeSoftwareTypeOfAccessFromList = (type) => {
    let { SoftwareTypeOfAccessList } = this.state;
    SoftwareTypeOfAccessList = SoftwareTypeOfAccessList.filter(item => item !== type);
    this.setState({ SoftwareTypeOfAccessList });    
  }

  handleSoftwareInputMediaTypeClick = (type) => {    this.addSoftwareInputMediaType2List(type);  }
  handleSoftwareInputMediaTypeDelete = (type) => {    this.removeSoftwareInputMediaTypeFromList(type);  }  
  handleDataTypeOfAccessClick = (type) => {    this.addDataTypeOfAccess2List(type);  }
  handleDataTypeOfAccessDelete = (type) => {    this.removeDataTypeOfAccessFromList(type);  }
  handleLingualityTypeClick = (type) => {    this.addLinguality2List(type);  }
  handleLingualityTypeDelete = (type) => {    this.removeLingualityFromList(type);  }
  handleDataMediaTypeClick = (type) => {    this.addDataMediaType2List(type);  }
  handleDataMediaTypeDelete = (type) => {    this.removeDataMediaTypeFromList(type);  }
  handleSoftwareOutputMediaTypeClick = (type) => {    this.addSoftwareOutputMediaType2List(type);  }
  handleSoftwareOutputMediaTypeDelete = (type) => {    this.removeSoftwareOutputMediaTypeFromList(type);  }
  handleSoftwareTypeOfAccessClick = (type) => {    this.addSoftwareTypeOfAccess2List(type);  }
  handleSoftwareTypeOfAccessDelete = (type) => {    this.removeSoftwareTypeOfAccessFromList(type);  }

  handleChange() {
    this.setState({ checked: !this.state.checked });
  };


  buildQuery(){
    let { checkedFunctionList, checkedResourceList, LingualityList, DataMediaTypeList, DataTypeOfAccessList, SoftwareInputMediaTypeList, SoftwareOutputMediaTypeList, SoftwareTypeOfAccessList } = this.state;
    let newquery = "";
    checkedResourceList.length > 0 && checkedResourceList.map((value) => {
      newquery = newquery + "&resource_type=" + encodeURIComponent(value);
      return value;
    })
    checkedFunctionList.length > 0 && checkedFunctionList.map((value) => {
       newquery = newquery  + "&function=" + encodeURIComponent(value) ;     
      return value;
    })
    LingualityList.length > 0 && LingualityList.map((value) => {
      newquery = newquery  + "&linguality_type=" + encodeURIComponent(value);
      return value;
    })
    DataMediaTypeList.length > 0 && DataMediaTypeList.map((value) => {
      newquery = newquery  + "&media_type=" + encodeURIComponent(value);
      return value;
    })
    DataTypeOfAccessList.length > 0 && DataTypeOfAccessList.map((value) => {
      newquery = newquery  + "&type_of_access=" + encodeURIComponent(value);
      return value;
    })
    SoftwareInputMediaTypeList.length > 0 && SoftwareInputMediaTypeList.map((value) => {
      newquery = newquery  + "&input_media_type=" + encodeURIComponent(value);
      return value;
    })
    SoftwareOutputMediaTypeList.length > 0 && SoftwareOutputMediaTypeList.map((value) => {
      newquery = newquery  + "&output_media_type=" + encodeURIComponent(value);
      return value;
    })
    SoftwareTypeOfAccessList.length > 0 && SoftwareTypeOfAccessList.map((value) => {
      newquery = newquery  + "&type_of_access=" + encodeURIComponent(value);
      return value;
    })

    return newquery;
  }

  getDataFromIndexer2() {
    let { chartData, checkedLanguagesList} = this.state;

    checkedLanguagesList.length > 0 && checkedLanguagesList.map((glottolog, idx) => {
      let newquery = glottolog; //"glottolog_code=" + glottolog;
      let new_data = {};
      let qq = this.buildQuery();
      newquery = newquery + qq ; 
      //console.log(newquery) //&language_id=cs&resource_type=Corpus
       
        try {
          axios.get(MATRIX_ELG_DATA_INDEXER(newquery))
            .then(res => {
              if (!chartData.find(element => element.query === newquery)) {
                new_data.id = glottolog;
                new_data.count = res.data.count;
                new_data.query = newquery;
                chartData.push(new_data);
                this.setState({ chartData });
              }
            });
        } catch (e) { console.log(e); return glottolog; }
      return glottolog;
    })    
  }

  addDataset2(dynamicBarChart, chartData, Label ) {     
    const color = mapLabelsToColors(Label); 
    let chart_label="Resources that meet the selected criteria per language"; 
    let datasetBaseItem = {
      fill: true,
      label: chart_label, //decodeURIComponent(Label),
      borderColor: color, //select one color 
      backgroundColor: transparentize(color, 0.3),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }
    dynamicBarChart.labels.length > 0 && dynamicBarChart.labels.map((label, index) => {
      let filterLanguageName = this.state.languages.filter(item => item.preferred_name === label[0]) || "";
      //let LabelGlottolog = filterLanguageName[0].glottocode;
      let LabelGlottolog = filterLanguageName[0].elg_query_params; 
      //let newquery = "glottolog_code=" + LabelGlottolog; //re-building the query 
      let newquery = LabelGlottolog; //re-building the query 
      let qq = this.buildQuery();
      newquery = newquery + qq ; 
      let TmpchartData = chartData.filter(item => item.query === newquery); 
      //console.log(newquery)
      if (TmpchartData.length > 0) {
        datasetBaseItem.data.push(TmpchartData[0].count);
      }     
      return label;
    })    
    dynamicBarChart.datasets.push(datasetBaseItem);
    return dynamicBarChart;
  }

  getDataFromIndexer() {
    let { chartData, AndTerms, checkedLanguagesList } = this.state;

  checkedLanguagesList.length > 0 && checkedLanguagesList.map((glottolog,idx)=>{
      AndTerms.length > 0 && AndTerms.map((value) => {
        let new_data = {};
        let query = glottolog + "&" + value;     //"glottolog_code=" + glottolog + "&" + value;        
          try {
            axios.get(MATRIX_ELG_DATA_INDEXER(query))
              .then(res => {                
                if (!chartData.find(element => element.query === query)){
                  new_data.id = glottolog;
                  new_data.count = res.data.count;
                  new_data.query = query;
                  chartData.push(new_data);
                  this.setState({ chartData });
                }
              });
          } catch (e) { console.log(e); return value; }
          return value;       
      }) 
      return glottolog;     
    })  
  }

  addDataset(dynamicBarChart, chartData, queryType, Label ) {     
    const color = mapLabelsToColors(Label);    
    let datasetBaseItem = {
      fill: true,
      label: decodeURIComponent(Label),
      borderColor: color, //select one color 
      backgroundColor: transparentize(color, 0.3),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }
    dynamicBarChart.labels.length > 0 && dynamicBarChart.labels.map((label, index) => {
      let filterLanguageName = this.state.languages.filter(item => item.preferred_name === label[0]) || "";
      //let LabelGlottolog = filterLanguageName[0].glottocode;
      let LabelGlottolog = filterLanguageName[0].elg_query_params;
      //let TmpchartData = chartData.filter(item => item.id === LabelGlottolog);      
      let query =  LabelGlottolog  + "&" + queryType + "=" + Label; //re-building the query  //"glottolog_code=" + LabelGlottolog  + "&" + queryType + "=" + Label; //re-building the query 
      //console.log(query)
      let TmpchartData = chartData.filter(item => item.query === query); 
      TmpchartData && TmpchartData.length > 0 && TmpchartData.map((chartItem, index) => {        
        if (chartItem.query.includes(Label)) {
          datasetBaseItem.data.push(chartItem.count);
        }
        return chartItem;
      })
      return label;
    })
    dynamicBarChart.datasets.push(datasetBaseItem);
    return dynamicBarChart;
  }

MyResourcetoChartJSData() {  
 let {chartData} = this.state;   
 let dynamicBarChart = {
    labels: [], //types (resource types),
    datasets: [], //per language
  };
  //this.state.checkedLanguagesList.length > 0 && this.getELEData() 
  this.state.checkedLanguagesList && this.state.checkedLanguagesList.length > 0 && this.state.checkedLanguagesList.map((language, index) => {
    //let filterLanguage = this.state.languages.filter(item => item.glottocode === language) || ""; 
    //console.log(this.state.languages)
    let filterLanguage = this.state.languages.filter(item => item.elg_query_params === language) || ""; 
    let langName = [filterLanguage[0].preferred_name];
    dynamicBarChart.labels.push(langName);          
    return language;
  })

  if (this.state.isDatasetsCheckAll === true && this.state.isSoftwareCheckAll === true) { 
    this.getDataFromIndexer();      
    this.addDataset(dynamicBarChart, chartData,"lr_distinction" , "Dataset" );
    this.addDataset(dynamicBarChart, chartData,"lr_distinction" , "Software" );           
  }

  if (this.state.isDatasetsCheckAll === true && this.state.isSoftwareCheckAll === false ) { 
    this.getDataFromIndexer(); //this.getDataFromIndexer2()  
    this.addDataset(dynamicBarChart, chartData,"lr_distinction" , "Dataset" ); //this.addDataset2(dynamicBarChart, chartData,"Dataset");          
  }

  if (this.state.isDatasetsCheckAll === false && this.state.isSoftwareCheckAll === true ) { 
    this.getDataFromIndexer(); //this.getDataFromIndexer2()  
    this.addDataset(dynamicBarChart, chartData,"lr_distinction" , "Software" );  //this.addDataset2(dynamicBarChart, chartData,"Software");          
  }


  if(this.state.DataTypeOfAccessList.length===0 && this.state.LingualityList.length===0 && this.state.DataMediaTypeList.length===0 && this.state.checkedResourceList.length > 0) {
    this.getDataFromIndexer2()  
    this.addDataset2(dynamicBarChart, chartData,"Resource subclass");
  } 
  if(this.state.SoftwareInputMediaTypeList.length===0 && this.state.SoftwareOutputMediaTypeList.length===0 && this.state.SoftwareTypeOfAccessList.length===0 && this.state.checkedFunctionList.length > 0){
    this.getDataFromIndexer2()  
    this.addDataset2(dynamicBarChart, chartData,"Function");
  } 

    /*the following lines create different bars per type */
  //this.state.DataTypeOfAccessList.length===0 && this.state.LingualityList.length===0 && this.state.DataMediaTypeList.length===0 && this.state.checkedResourceList.length > 0 && this.state.checkedResourceList.map((type,ind)=>{
  //  this.getDataFromIndexer(); 
  //  this.addDataset(dynamicBarChart, chartData, "resource_type", encodeURIComponent(type)  );
  //})
  //this.state.SoftwareInputMediaTypeList.length===0 && this.state.SoftwareOutputMediaTypeList.length===0 && this.state.SoftwareTypeOfAccessList.length===0 && this.state.checkedFunctionList.length > 0 && this.state.checkedFunctionList.map((type,ind)=>{
  //  this.getDataFromIndexer(); 
  //  this.addDataset(dynamicBarChart, chartData, "function", encodeURIComponent(type)  );
  //})

  if ((this.state.DataTypeOfAccessList.length >0 || this.state.LingualityList.length>0 || this.state.DataMediaTypeList.length>0) && (this.state.checkedResourceList.length > 0) ) {
    this.getDataFromIndexer2()  
    this.addDataset2(dynamicBarChart, chartData,"Resource subclass");
  }

  if ((this.state.SoftwareInputMediaTypeList.length >0 || this.state.SoftwareOutputMediaTypeList.length>0 || this.state.SoftwareTypeOfAccessList.length>0) && (this.state.checkedFunctionList.length > 0) ) {
    this.getDataFromIndexer2()  
    this.addDataset2(dynamicBarChart, chartData,"Function");
  } 
  return dynamicBarChart;
}

addSoftwareData(dynamicRadarChart) { 
  let { checkedLanguagesList,   checkedFunctionList, chartData} = this.state;
  checkedLanguagesList.length > 0 && checkedLanguagesList.map((glottocode, index) => {
    //let filterLanguageName = this.state.languages.filter(item => item.glottocode === glottocode) || "";
    let filterLanguageName = this.state.languages.filter(item => item.elg_query_params === glottocode) || "";
    let datasetBaseItem = {
      fill: true,
      label: filterLanguageName[0].preferred_name,
      borderColor: namedColor(getRandomArbitrary(index)), //select one color 
      backgroundColor: transparentize(namedColor(getRandomArbitrary(index)), 0.5),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }    
    checkedFunctionList && checkedFunctionList.length > 0 && checkedFunctionList.map((functionitem, idx) => {
      let query =  glottocode  + "&function=" + encodeURIComponent(functionitem); //re-building the query  //"glottolog_code=" + glottocode  + "&function=" + encodeURIComponent(functionitem); //re-building the query 
      //console.log(query)
      let TmpchartData = chartData.filter(item => item.query === query);     
      if (TmpchartData.length > 0) {
        datasetBaseItem.data.push(TmpchartData[0].count);
      }
      return functionitem;
    })
    dynamicRadarChart.datasets.push(datasetBaseItem);
    return glottocode;
  })    
   return dynamicRadarChart;   
}

addResourceData(dynamicRadarChart) { 
  let { checkedLanguagesList,   checkedResourceList, chartData} = this.state;
  checkedLanguagesList.length > 0 && checkedLanguagesList.map((glottocode, index) => {
    //let filterLanguageName = this.state.languages.filter(item => item.glottocode === glottocode) || "";
    let filterLanguageName = this.state.languages.filter(item => item.elg_query_params === glottocode) || "";
    let datasetBaseItem = {
      fill: true,
      label: filterLanguageName[0].preferred_name,
      borderColor: namedColor(getRandomArbitrary(index)), //select one color 
      backgroundColor: transparentize(namedColor(getRandomArbitrary(index)), 0.5),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }    
    checkedResourceList && checkedResourceList.length > 0 && checkedResourceList.map((type, idx) => {
      //query uses another parameter now
      let query = glottocode  + "&resource_type=" + encodeURIComponent(type); //re-building the query  //"glottolog_code=" + glottocode  + "&resource_type=" + encodeURIComponent(type); //re-building the query 
      //console.log(query)
      let TmpchartData = chartData.filter(item => item.query === query);     
      if (TmpchartData.length > 0) {
        datasetBaseItem.data.push(TmpchartData[0].count);
      }
      return type;
    })
    dynamicRadarChart.datasets.push(datasetBaseItem);
    return glottocode;
  })    
   return dynamicRadarChart;   
}

addRadarLabels(dynamicRadarChart, indicator) {
  dynamicRadarChart.labels.push(indicator);  
  return dynamicRadarChart;
}

createRadar(){
  let dynamicRadarChart = {
    labels: [], //resource types,
    datasets: [], //per language
  }
  this.state.checkedFunctionList.length > 0 && this.state.checkedFunctionList.map((indicator, index) => {
    this.addRadarLabels(dynamicRadarChart, indicator);
    return indicator;
  }) 
  this.state.checkedFunctionList.length>0  && this.addSoftwareData(dynamicRadarChart);

  this.state.checkedResourceList.length > 0 && this.state.checkedResourceList.map((indicator, index) => {
    this.addRadarLabels(dynamicRadarChart, indicator);
    return indicator;
  }) 
  this.state.checkedResourceList.length>0  && this.addResourceData(dynamicRadarChart);
  return dynamicRadarChart;
}

  render() {
    const {  expandedLangEu, expandedLangOther} = this.state;   
    const dynamicBarChart = this.MyResourcetoChartJSData();
    //const dynamicRadarChart = this.createRadar(); 
    //console.log(this.state.chartData)
    return <Grid container className="chart-container" spacing={1} direction="row">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters">
          <Grid container direction="column">
          <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <FormGroup>
                {expandedLangEu? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            //checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            checked={(this.state.checkedLanguagesList.find(item => item === language.elg_query_params)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            //name={language.glottocode} 
                            name={language.elg_query_params}
                            />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            //checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            checked={(this.state.checkedLanguagesList.find(item => item === language.elg_query_params)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                             //name={language.glottocode} 
                             name={language.elg_query_params} 
                            />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>
            <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other European languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <FormGroup>
                {expandedLangOther ? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            //checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            checked={(this.state.checkedLanguagesList.find(item => item === language.elg_query_params)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                             //name={language.glottocode} 
                             name={language.elg_query_params} 
                            />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            //checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            checked={(this.state.checkedLanguagesList.find(item => item === language.elg_query_params)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                             //name={language.glottocode} 
                             name={language.elg_query_params} 
                            />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>
          </Grid>
        </Paper>
      </Grid>
      <Grid item sm={10}>
        <Grid container direction="column">
          <Grid item>
          <Accordion defaultExpanded className="selection-panel">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon style={{ fontSize: 30 }} className="grey--font"/>}
          aria-controls="panel1c-content"
          id="panel1c-header" className="chart-header"> <Typography variant="h4" className="bold">Filter by:</Typography> </AccordionSummary>
        <AccordionDetails>
             
              <Grid container  spacing={3} direction="row">
                
                <Grid item container sm={12} direction="row" justifyContent = "space-between" className="selection-row">
                  <Grid item container sm={6}  direction="row">
                    <Grid item container sm={12} direction="row"   alignItems="flex-start" justifyContent="flex-start">
                      <Grid item sm={3} xs={6}>
                        <FormControlLabel className="pl05"
                          control={<Checkbox
                            checked={this.state.isDatasetsCheckAll}
                            //indeterminate={this.allInDiffStateResourceType()}
                            color="primary"
                            onChange={e => { this.handleDatasetCheckAll(e) }}
                            name={"datasets"} />}
                          label={"Datasets"}
                        />
                      </Grid>
                      <Grid item container sm={9} xs={6} direction="row">
                      <Grid item sm={12} className="bg-light-grey rounded pl-03"><Typography variant="caption">Resource subclass</Typography></Grid>
                        {ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map((type, index) => {
                          return <Grid item sm={6} key={index}>
                            <FormControlLabel className="pl05"
                              control={<Checkbox
                                checked={this.state.checkedResourceList.find(item => item === type) ? true : false}
                                color="primary"
                                onChange={e => { this.handleResourceCheck(e) }}
                                name={type} />}
                              label={type}
                            />
                          </Grid>
                        })}
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item container sm={6}   spacing={2} direction="row">
                    <Grid item container sm={12} direction="row">
                      <Grid item sm={3} xs={6}><Typography variant="caption" className="bold">Linguality type:</Typography></Grid>
                      {LingualityType && LingualityType.length > 0 && LingualityType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small"
                            className="ChipTagMatrix"
                            variant={this.state.LingualityList.find(item => item === type) ? "default" : "outlined"}
                            label={type}
                            onClick={() => this.handleLingualityTypeClick(type)}
                            onDelete={this.state.LingualityList.find(item => item === type) && (() => this.handleLingualityTypeDelete(type))}
                            disabled= {((this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll) && !this.state.isSoftwareCheckAll) ? false : true }
                          />
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12}direction="row">
                    <Grid item sm={3} xs={6}><Typography variant="caption" className="bold">Media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small"
                          className="ChipTagMatrix"
                            label={type}
                            variant={this.state.DataMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onClick={() => this.handleDataMediaTypeClick(type)}
                            onDelete={this.state.DataMediaTypeList.find(item => item === type) && (() => this.handleDataMediaTypeDelete(type))}
                            disabled= {((this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll)  && !this.state.isSoftwareCheckAll)  ? false : true }
                          />
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3} xs={6}><Typography variant="caption"  className="bold">Access Rights:</Typography></Grid>
                    <Grid item container sm={9}>{TypeOfAccess && TypeOfAccess.length > 0 && TypeOfAccess.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.DataTypeOfAccessList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.DataTypeOfAccessList.find(item => item === type) && (() => this.handleDataTypeOfAccessDelete(type))}
                            onClick={() => this.handleDataTypeOfAccessClick(type, index)}
                            disabled= {((this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll)  && !this.state.isSoftwareCheckAll) ? false : true }
                          />
                        </Grid>
                      })}
                      </Grid>  
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item container sm={12}  direction="row" justifyContent = "space-between">
                  <Grid item container sm={6} direction="row">
                  <Grid item container sm={12} direction="row"   alignItems="flex-start" justifyContent="flex-start">
                    <Grid item sm={3} xs={6}>
                      <FormControlLabel className="pl05"
                        control={<Checkbox
                          checked={this.state.isSoftwareCheckAll}
                          //indeterminate={this.allInDiffStateFunction()}
                          color="primary"
                          onChange={e => { this.handleSoftwareCheckAll(e) }}
                          name={"software"} />}
                        label={"Software"}
                      />
                    </Grid>
                    <Grid item container sm={9} direction="row">
                    <Grid item sm={12} className="bg-light-grey rounded pl-03"><Typography variant="caption">Functions</Typography></Grid>
                      {Functions && Functions.length > 0 && Functions.map((type, index) => {
                        return <Grid item sm={6} key={index} >
                          <FormControlLabel className="pl05"
                            control={<Checkbox
                              checked={this.state.checkedFunctionList.find(item => item === type) ? true : false}
                              color="primary"
                              onChange={e => { this.handleFunctionCheck(e) }}
                              name={type} />}
                              label={type.replace("Collection", '')}
                          />
                        </Grid>
                      })}
                    </Grid>
                  </Grid>
                  </Grid>
                  
                  <Grid item container sm={6}  spacing={2} direction="row">
                      {/*dynamicRadarChart && dynamicRadarChart.datasets.length > 1 && this.state.checkedFunctionList.length > 2 &&
                        <Grid item sm={12}><FormControlLabel
                          control={<Switch checked={this.state.checked} onChange={this.handleChange} />}
                    label="View as Radar" /></Grid>*/}

                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3} xs={6}><Typography variant="caption"  className="bold">Input media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareInputMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareInputMediaTypeList.find(item => item === type) && (() => this.handleSoftwareInputMediaTypeDelete(type))}
                            onClick={() => this.handleSoftwareInputMediaTypeClick(type, index)} 
                            disabled= {(this.state.checkedFunctionList.length>0 || this.state.isSoftwareCheckAll) && !this.state.isDatasetsCheckAll ? false : true }/>
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3} xs={6}><Typography variant="caption"  className="bold">Output media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareOutputMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareOutputMediaTypeList.find(item => item === type) && (() => this.handleSoftwareOutputMediaTypeDelete(type))}
                            onClick={() => this.handleSoftwareOutputMediaTypeClick(type, index)} 
                            disabled= {(this.state.checkedFunctionList.length>0 || this.state.isSoftwareCheckAll) && !this.state.isDatasetsCheckAll ? false : true }/>
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12}  direction="row">
                    <Grid item sm={3} xs={6}><Typography variant="caption"  className="bold">Access Rights:</Typography></Grid>
                    <Grid item container sm={9}>  {TypeOfAccess && TypeOfAccess.length > 0 && TypeOfAccess.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareTypeOfAccessList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareTypeOfAccessList.find(item => item === type) && (() => this.handleSoftwareTypeOfAccessDelete(type))}
                            onClick={() => this.handleSoftwareTypeOfAccessClick(type, index)} 
                            disabled= {(this.state.checkedFunctionList.length>0 || this.state.isSoftwareCheckAll )&& !this.state.isDatasetsCheckAll ? false : true }/>
                        </Grid>
                      })}</Grid>
                    </Grid>
                  </Grid>
                </Grid>
                  {this.state.chartData && this.state.chartData.length > 0 && <Grid item sm={12}>
                    <Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip>
                  </Grid>}
                </Grid>
              </AccordionDetails>
            </Accordion>
          </Grid>

          

          {dynamicBarChart.datasets && dynamicBarChart.datasets.length > 0 && <Grid item sm={12}><BarChart BarData={dynamicBarChart} options={verticalBarOptions} title={"Number of resources"} /></Grid>}  

          {/*<Fade in={this.state.checked}>
            <Grid item sm={12}>{dynamicRadarChart.datasets && dynamicRadarChart.datasets.length > 1 &&  <RadarChart RadarData={dynamicRadarChart}  title={"Number of resources"} /> } </Grid>         
                    </Fade> */}


          </Grid>        
      </Grid>
    </Grid>
  }
}