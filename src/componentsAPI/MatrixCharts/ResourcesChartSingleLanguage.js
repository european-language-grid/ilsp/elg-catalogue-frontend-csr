import React from "react";
import axios from "axios";
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography"
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import BarChart from "./BarChart";
import { transparentize, CHART_COLORS, mapLabelsToColors, Functions, ResourceTypes, LingualityType, MediaType, TypeOfAccess } from "./Utils";
import Chip from '@material-ui/core/Chip';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { DLE_LANGUAGES, MATRIX_ELG_DATA_INDEXER} from "./../../config/constants";
 
const truncated_number_of_items_to_show = 6;
 
const verticalBarOptions = {  
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      //text: 'Chart.js Bar Chart'
    }
  }
}; 

const StackedBarOptions = {
  //indexAxis: 'y',  
    responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true
      }       
    },
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        //text: 'Chart.js Bar Chart'
      }
    }
  };  
 
export default class ResourcesChartSingleLanguage extends React.Component {
  constructor(state) {
    super(state);
    this.state = {
      languages: "", checkedLanguagesList: [], checkedResourceList: [], checkedFunctionList: [], isDatasetsCheckAll: false, isSoftwareCheckAll: false, expandedLangEu: false, expandedLangOther: false, 
      chartData: [], LingualityList: [], DataMediaTypeList: [], DataTypeOfAccessList: [], SoftwareInputMediaTypeList: [], SoftwareOutputMediaTypeList: [], SoftwareTypeOfAccessList: [], checked: false, 
      languageValue: ""   , AndTerms: [], 
    }
  }

  markAndTermAsSelected = (facetName) => {
    const { AndTerms } = this.state;
    if (AndTerms.indexOf(facetName) === -1) { 
      AndTerms.push(facetName);
      this.setState({ AndTerms });       
    }
  }

  handleUnselectedAndTerm = (facetName) => {
    const { AndTerms, chartData } = this.state;
    let tmpAndTerms = AndTerms.filter(item => item !== facetName);
    this.setState({ AndTerms: tmpAndTerms });
    let TmpchartData = chartData.filter(item => !item.query.includes(facetName));
    this.setState({chartData: TmpchartData})
  }
  
  componentDidMount() {
    this.getEULanguages();
  }

   
  clearAllFilters() {
    this.setState({ DataMediaTypeList: [], checkedLanguagesList: [], checkedResourceList: [], checkedFunctionList: [], SoftwareInputMediaTypeList: [] });
    this.setState({ SoftwareOutputMediaTypeList: [], LingualityList: [], SoftwareTypeOfAccessList: [], DataTypeOfAccessList: [] , chartData: [] });
    this.setState({  isDatasetsCheckAll: false }); this.setState({  isSoftwareCheckAll: false , checked: false,  languageValue: ""  }); 
    this.setState({ AndTerms:[] })
  }

  getEULanguages() {
    try {
      axios.get(DLE_LANGUAGES)
        .then(res => {
          this.setState({ languages: res.data.results });
        });
    } catch (e) {
      console.log(e);
      return;
    }
  }
  
  addCheckedResource2List = (resource) => {
    this.setState({ isDatasetsCheckAll: false });
    this.setState({ isSoftwareCheckAll: false });
    this.removeAllFunctionFromCheckedList();
    this.setState({ SoftwareOutputMediaTypeList: [], SoftwareInputMediaTypeList: [] , SoftwareTypeOfAccessList: []});
    const { checkedResourceList } = this.state;
    checkedResourceList.push(resource);
    this.setState({ checkedResourceList });
    let selectedString2 =  "resource_type=" + encodeURIComponent(resource) ;
    this.markAndTermAsSelected(selectedString2); 
    if (this.state.LingualityList.length > 0) {
      this.state.LingualityList.map((type) => {
        this.markAndTermAsSelected("resource_type=" + encodeURIComponent(resource) + "&linguality_type=" + type);
        return type;
      })
    }  
  }

  removeResourceFromCheckedList = (resource) => {
    //this.setState({ isDatasetsCheckAll: false }); //if we remove one Item we keep the all button off
    let { checkedResourceList } = this.state;
    checkedResourceList = checkedResourceList.filter(item => item !== resource);
    this.setState({ checkedResourceList });
    let selectedString2 =  "resource_type=" + encodeURIComponent(resource) ;
    this.handleUnselectedAndTerm(selectedString2);
    if (this.state.LingualityList.length > 0) {
      this.state.LingualityList.map((type) => {
        this.handleUnselectedAndTerm("resource_type=" + encodeURIComponent(resource) + "&linguality_type=" + type);
        return type;
      })
    }  
  }

  addCheckedFunction2List = (functionItem) => {
    this.setState({ isSoftwareCheckAll: false });
    this.setState({ isDatasetsCheckAll: false });
    this.removeAllResourceFromCheckedList();
    this.setState({ DataMediaTypeList: [], LingualityList: [] , DataTypeOfAccessList: []});
    const { checkedFunctionList } = this.state;
    checkedFunctionList.push(functionItem);
    this.setState({ checkedFunctionList });
    this.setState({ SoftwareOutputMediaTypeList: [], SoftwareInputMediaTypeList: [] , SoftwareTypeOfAccessList: [],}); //pending until next backend change
    let selectedString2 =  "function=" + encodeURIComponent(functionItem);     
    this.markAndTermAsSelected(selectedString2);
  }

  removeFunctionFromCheckedList = (functionItem) => {
    //this.setState({ isSoftwareCheckAll: false }); 
    let { checkedFunctionList } = this.state;
    checkedFunctionList = checkedFunctionList.filter(item => item !== functionItem);
    this.setState({ checkedFunctionList });
    let selectedString2 =  "function=" + encodeURIComponent(functionItem);    
    this.handleUnselectedAndTerm(selectedString2);
  }

  removeAllResourceFromCheckedList = () => {
    this.setState({ checkedResourceList: [] });
  }

  removeAllFunctionFromCheckedList = () => {
    //this.setState({ isSoftwareCheckAll: false });
    this.setState({ checkedFunctionList: [] });
  }
   
  handleDatasetCheckAll = (e) => {
    this.setState({ isDatasetsCheckAll: !this.state.isDatasetsCheckAll });
    if (e.target.checked) {
      this.removeAllResourceFromCheckedList();
      this.removeAllFunctionFromCheckedList();
      this.removeSoftwareTypeofAccessList();
      this.removeSoftwareInputMediaTypeFromList();
      this.removeSoftwareOutputMediaTypeFromList();
      this.markAndTermAsSelected("lr_distinction=Dataset")
    }
    else {  this.handleUnselectedAndTerm("lr_distinction=Dataset");}
  }

  handleSoftwareCheckAll = (e) => {
    this.setState({ isSoftwareCheckAll: !this.state.isSoftwareCheckAll });
    if (e.target.checked) {
      this.removeAllFunctionFromCheckedList();
      this.removeAllResourceFromCheckedList();
      this.removeAllFromLingualityList();
      this.removeAllFromDataMediaTypeList();
      this.removeAllFromDataTypeOfAccessList();
      this.markAndTermAsSelected("lr_distinction=Software")
    }
    else { this.handleUnselectedAndTerm("lr_distinction=Software");}   
  }

  handleResourceCheck = (e) => {
    if (e.target.checked) {
      this.addCheckedResource2List(e.target.name);
    } else {
      this.removeResourceFromCheckedList(e.target.name);
    }
  }

  handleFunctionCheck = (e) => {
    if (e.target.checked) {
      this.addCheckedFunction2List(e.target.name);
    } else {
      this.removeFunctionFromCheckedList(e.target.name);
    }
  }

  getDataFromIndexer(glottolog) {
    let { chartData, AndTerms } = this.state;
    //console.log(AndTerms)
    AndTerms.length > 0 && AndTerms.map((value) => {
      let new_data = {};
      let query = glottolog + "&" + value; //"glottolog_code=" + glottolog + "&" + value; //queries are different, elg parameter is added now instead of glottolog
      //console.log("axios", query)
      const found = chartData.find(element => element.query === query);//checks if i have allready had run this query, if not add it to array               
      if (!found) {
        try {
          axios.get(MATRIX_ELG_DATA_INDEXER(query))
            .then(res => {
              new_data.id = glottolog;
              new_data.count = res.data.count;
              //console.log(query, res.data.count)
              new_data.query = query;
              chartData.push(new_data);
              this.setState({ chartData });
            });
        } catch (e) { console.log(e); return value; }
      }
      return value;
    })
  }

  addLinguality2List = (type, index) => {
    const { LingualityList } = this.state;
    LingualityList.push(type);
    this.setState({ LingualityList });
    this.setState({isSoftwareCheckAll: false})
    //this.markAndTermAsSelected("linguality_type=" + type)
    if (this.state.isDatasetsCheckAll) {      
      this.markAndTermAsSelected("lr_distinction=Dataset&linguality_type=" + type);
    }
    if (this.state.checkedResourceList.length > 0) {
      this.state.checkedResourceList.map((resource_type) => {
        this.markAndTermAsSelected("resource_type=" + encodeURIComponent(resource_type) + "&linguality_type=" + type);
        return resource_type;
      })
    }
  }

  removeLingualityFromList = (type) => {
    let { LingualityList  } = this.state;
    LingualityList = LingualityList.filter(item => item !== type);
    this.setState({ LingualityList });
    //this.handleUnselectedAndTerm("linguality_type=" + type);
    if (this.state.isDatasetsCheckAll) {      
      this.handleUnselectedAndTerm("lr_distinction=Dataset&linguality_type=" + type)
    }
    if (this.state.checkedResourceList.length > 0) {
      this.state.checkedResourceList.map((resource_type) => {
        this.handleUnselectedAndTerm("resource_type=" + encodeURIComponent(resource_type) + "&linguality_type=" + type)
        return resource_type;
      })
    }
  }

  addDataMediaType2List = (type) => {
    const { DataMediaTypeList } = this.state;
    DataMediaTypeList.push(type);
    this.setState({ DataMediaTypeList });
    this.setState({isSoftwareCheckAll: false})
    //this.markAndTermAsSelected("media_type=" + type)
    if (this.state.isDatasetsCheckAll) {      
      this.markAndTermAsSelected("lr_distinction=Dataset&media_type=" + encodeURIComponent(type))
    }
    if (this.state.checkedResourceList.length > 0) {
      this.state.checkedResourceList.map((resource_type) => {
        this.markAndTermAsSelected("resource_type=" + encodeURIComponent(resource_type) + "&media_type=" + encodeURIComponent(type));
        return resource_type;
      })
    }
  }

  removeDataMediaTypeFromList = (type) => {
    let { DataMediaTypeList  } = this.state;
    DataMediaTypeList = DataMediaTypeList.filter(item => item !== type);
    this.setState({ DataMediaTypeList });
    //this.handleUnselectedAndTerm("media_type=" + type);
    if (this.state.isDatasetsCheckAll) {      
      this.handleUnselectedAndTerm("lr_distinction=Dataset&media_type=" + encodeURIComponent(type));
    }
    if (this.state.checkedResourceList.length > 0) {
      this.state.checkedResourceList.map((resource_type) => {
        this.handleUnselectedAndTerm("resource_type=" + encodeURIComponent(resource_type) + "&media_type=" + encodeURIComponent(type));
        return resource_type;
      })
    }
  }

  addDataTypeOfAccess2List = (type) => {
    const { DataTypeOfAccessList } = this.state;
    DataTypeOfAccessList.push(type);
    this.setState({ DataTypeOfAccessList });
    this.setState({isSoftwareCheckAll: false})
    //this.markAndTermAsSelected("type_of_access=" + type)
    if (this.state.isDatasetsCheckAll) {      
      this.markAndTermAsSelected("lr_distinction=Dataset&type_of_access=" + encodeURIComponent(type));
    }   
    if (this.state.checkedResourceList.length>0){
      this.state.checkedResourceList.map((resource_type)=>{
        this.markAndTermAsSelected("resource_type="+ encodeURIComponent(resource_type) + "&type_of_access=" + encodeURIComponent(type));
        return resource_type;
      })
    }   
  }

  removeDataTypeOfAccessFromList = (type) => {
    let { DataTypeOfAccessList} = this.state;
    DataTypeOfAccessList = DataTypeOfAccessList.filter(item => item !== type);
    this.setState({ DataTypeOfAccessList });
    //this.handleUnselectedAndTerm("type_of_access=" + type);
    if (this.state.isDatasetsCheckAll) {      
      this.handleUnselectedAndTerm("lr_distinction=Dataset&type_of_access=" + encodeURIComponent(type));
    }
    if (this.state.checkedResourceList.length > 0) {
      this.state.checkedResourceList.map((resource_type) => {
        this.handleUnselectedAndTerm("resource_type=" + encodeURIComponent(resource_type) + "&type_of_access=" + encodeURIComponent(type));
        return resource_type;
      })
    }
  }

  addSoftwareInputMediaType2List = (type) => {
    const { SoftwareInputMediaTypeList } = this.state;
    SoftwareInputMediaTypeList.push(type);
    this.setState({ SoftwareInputMediaTypeList });
    this.setState({isDatasetsCheckAll: false});
    //this.markAndTermAsSelected("input_media_type=" + type)
    if (this.state.isSoftwareCheckAll) {      
      this.markAndTermAsSelected("lr_distinction=Software&input_media_type=" + encodeURIComponent(type));
    }    
    if (this.state.checkedFunctionList.length>0){
      this.state.checkedFunctionList.map((function_type)=>{
        this.markAndTermAsSelected("function="+ encodeURIComponent(function_type)+ "&input_media_type=" + encodeURIComponent(type));
        return function_type;
      })
    }
  }

  removeSoftwareInputMediaTypeFromList = (type) => {
    let { SoftwareInputMediaTypeList } = this.state;
    SoftwareInputMediaTypeList = SoftwareInputMediaTypeList.filter(item => item !== type);
    this.setState({ SoftwareInputMediaTypeList });
    //this.handleUnselectedAndTerm("input_media_type=" + type);
    if (this.state.isSoftwareCheckAll) {      
      this.handleUnselectedAndTerm("lr_distinction=Software&input_media_type=" + encodeURIComponent(type));
    }
    if (this.state.checkedFunctionList.length > 0) {
      this.state.checkedFunctionList.map((function_type) => {
        this.handleUnselectedAndTerm("function=" + encodeURIComponent(function_type) + "&input_media_type=" + encodeURIComponent(type));
        return function_type;
      })
    }
  }

  addSoftwareOutputMediaType2List = (type) => {
    const { SoftwareOutputMediaTypeList } = this.state;
    SoftwareOutputMediaTypeList.push(type);
    this.setState({ SoftwareOutputMediaTypeList });
    this.setState({isDatasetsCheckAll: false})
    //this.markAndTermAsSelected("output_media_type=" + type)
    if (this.state.isSoftwareCheckAll) {      
      this.markAndTermAsSelected("lr_distinction=Software&output_media_type=" + encodeURIComponent(type));
    }
    if (this.state.checkedFunctionList.length > 0) {
      this.state.checkedFunctionList.map((function_type) => {
        this.markAndTermAsSelected("function=" + encodeURIComponent(function_type) + "&output_media_type=" + encodeURIComponent(type));
        return function_type;
      })
    }
  }

  removeSoftwareOutputMediaTypeFromList = (type) => {
    let { SoftwareOutputMediaTypeList } = this.state;
    SoftwareOutputMediaTypeList = SoftwareOutputMediaTypeList.filter(item => item !== encodeURIComponent(type));
    this.setState({ SoftwareOutputMediaTypeList });
    //this.handleUnselectedAndTerm("output_media_type=" + type);
    if (this.state.isSoftwareCheckAll) {      
      this.handleUnselectedAndTerm("lr_distinction=Software&output_media_type=" + encodeURIComponent(type));
    }
    if (this.state.checkedFunctionList.length > 0) {
      this.state.checkedFunctionList.map((function_type) => {
        this.handleUnselectedAndTerm("function=" + encodeURIComponent(function_type) + "&output_media_type=" + encodeURIComponent(type));
        return function_type;
      })
    }
  }
 
  addSoftwareTypeOfAccess2List = (type, index) => {
    const { SoftwareTypeOfAccessList } = this.state;
    SoftwareTypeOfAccessList.push(type);
    this.setState({ SoftwareTypeOfAccessList });
    this.setState({isDatasetsCheckAll: false})
    //this.markAndTermAsSelected("type_of_access=" + type)
    if (this.state.isSoftwareCheckAll) {      
      this.markAndTermAsSelected("lr_distinction=Software&type_of_access=" + encodeURIComponent(type));
    }    
    if (this.state.checkedFunctionList.length>0){
      this.state.checkedFunctionList.map((function_type)=>{
        this.markAndTermAsSelected("function="+ encodeURIComponent(function_type) + "&type_of_access=" + encodeURIComponent(type));
        return function_type;
      })
    }
  }

  removeSoftwareTypeOfAccessFromList = (type) => {
    let { SoftwareTypeOfAccessList } = this.state;
    SoftwareTypeOfAccessList = SoftwareTypeOfAccessList.filter(item => item !== type);
    this.setState({ SoftwareTypeOfAccessList });
    //this.handleUnselectedAndTerm("type_of_access=" + type);
    if (this.state.isSoftwareCheckAll) {      
      this.handleUnselectedAndTerm("lr_distinction=Software&type_of_access=" + encodeURIComponent(type));
    }
    if (this.state.checkedFunctionList.length > 0) {
      this.state.checkedFunctionList.map((function_type) => {
        this.handleUnselectedAndTerm("function=" + encodeURIComponent(function_type) + "&type_of_access=" + encodeURIComponent(type));
        return function_type;
      })
    }
  }

  removeAllFromLingualityList=()=> { this.setState({ LingualityList: [] }); } 
  removeAllFromDataMediaTypeList=()=> { this.setState({ DataMediaTypeList: [] }); }
  removeAllFromDataTypeOfAccessList=()=> { this.setState({ DataTypeOfAccessList: [] }); }
  removeSoftwareTypeofAccessList=()=> { this.setState({ SoftwareTypeOfAccessList: [] }); }
  //removeSoftwareInputMediaTypeFromList=()=> { this.setState({ SoftwareInputMediaTypeList: [] }); }
  //removeSoftwareOutputMediaTypeFromList=()=> { this.setState({ SoftwareOutputMediaTypeList: [] }); }
  handleLingualityTypeClick = (type) => { this.addLinguality2List(type); }
  handleLingualityTypeDelete = (type) => { this.removeLingualityFromList(type); }
  handleDataMediaTypeClick = (type) => { this.addDataMediaType2List(type); }
  handleDataMediaTypeDelete = (type) => { this.removeDataMediaTypeFromList(type); }
  handleDataTypeOfAccessClick = (type) => { this.addDataTypeOfAccess2List(type); }
  handleDataTypeOfAccessDelete = (type) => { this.removeDataTypeOfAccessFromList(type); }
  handleSoftwareInputMediaTypeClick = (type) => { this.addSoftwareInputMediaType2List(type); }
  handleSoftwareInputMediaTypeDelete = (type) => { this.removeSoftwareInputMediaTypeFromList(type); }
  handleSoftwareOutputMediaTypeClick = (type) => { this.addSoftwareOutputMediaType2List(type); }
  handleSoftwareOutputMediaTypeDelete = (type) => { this.removeSoftwareOutputMediaTypeFromList(type); }
  handleSoftwareTypeOfAccessClick = (type) => { this.addSoftwareTypeOfAccess2List(type); }
  handleSoftwareTypeOfAccessDelete = (type) => { this.removeSoftwareTypeOfAccessFromList(type); }
  togglexpandedLangEu = () => { this.setState({ expandedLangEu: !this.state.expandedLangEu }) }
  togglexpandedLangOther = () => { this.setState({ expandedLangOther: !this.state.expandedLangOther }) }
  handleLangChange = (event) => {
    this.setState({ languageValue: event.target.value }); 
  }

  addDatasetUnstacked(dynamicBarChart, chartData, queryType, Label ) {     
    const color = mapLabelsToColors(Label);
    let datasetBaseItem = {
      fill: true,
      label: decodeURIComponent(Label),
      borderColor: color, //select one color 
      backgroundColor: transparentize(color, 0.3),
      pointBackgroundColor: "rgba(34, 202, 236, 1)",
      poingBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      data: []
    }
    dynamicBarChart.labels.length > 0 && dynamicBarChart.labels.map((label, index) => {
      //let filterLanguageName = this.state.languages.filter(item => item.preferred_name === label[0]) || "";
      //let LabelGlottolog = filterLanguageName[0].glottocode;
      let LabelGlottolog =   label;  
      let query = "" ;  
      query = LabelGlottolog  + "&" + queryType + "=" + Label; //re-building the query  //"glottolog_code=" + LabelGlottolog  + "&" + queryType + "=" + Label; //re-building the query
       
      //console.log(query)
      let TmpchartData = chartData.filter(item => item.query === query); 
      TmpchartData && TmpchartData.length > 0 && TmpchartData.map((chartItem, index) => {        
        if (chartItem.query.includes(Label)) {
          datasetBaseItem.data.push(chartItem.count);
        }
        return chartItem;
      })
      return label;
    })
    dynamicBarChart.datasets.push(datasetBaseItem);
    return dynamicBarChart;
  }

  addDataSet = (dynamicBarChart, List, queryType, List2, queryType2) => {
    let { languageValue, chartData } = this.state;

    let query = "";
    let bar_label = "";
    if (languageValue) {
      if (!List2) { 
        //console.log(queryType)
        if(queryType.includes("resource_type")) {bar_label = "Resource subclass";}
        if(queryType.includes("function")) {bar_label = "Function" ;}
        dynamicBarChart.labels.push(bar_label);       
      }
      else{
        List.map((Litem) => {
          //console.log(Litem) 
          dynamicBarChart.labels.push(Litem);
          return Litem;
        })
      }
        
      if (!List2) {        
        List.length > 0 && List.map((Litem, index) => { 
          const newDataset = {
            label: '',           
            backgroundColor: [],
            data: [],
          }; 
          query = languageValue + "&" + queryType + "=" + encodeURIComponent(Litem); //building the query  //"glottolog_code=" + languageValue + "&" + queryType + "=" + encodeURIComponent(Litem); //building the query  
          
          //console.log("mm" , query)
          let TmpchartData = chartData.filter(item => item.query === query);
          //console.log(TmpchartData, chartData)
          if (TmpchartData.length > 0) {
            //dynamicBarChart.labels.push(Litem); 
            //console.log(Litem)
            newDataset.label = Litem; //languageValue  + queryType + Litem        
            newDataset.data.push(TmpchartData[0].count);
            const colorIndex = index % Object.keys(CHART_COLORS).length;                    
            newDataset.backgroundColor.push(Object.values(CHART_COLORS)[colorIndex]);
          }
          dynamicBarChart.datasets.push(newDataset);
          return Litem;
        })
        
      }
      else{ 
        List2.length > 0 && List2.map((seconditem, index) => { 
          const newDataset = {
            label: '', // 'Dataset ' + languageValue + firstitem + seconditem,
            backgroundColor: [],
            data: [],
          };
          List.length > 0 && List.map((firstitem, ind) => {  
            query = languageValue  + "&" + queryType + "=" + encodeURIComponent(firstitem) + "&" + queryType2 + "=" + encodeURIComponent(seconditem); //building the query //"glottolog_code=" + languageValue  + "&" + queryType + "=" + encodeURIComponent(firstitem) + "&" + queryType2 + "=" + encodeURIComponent(seconditem) //building the query 
                  
          //console.log("ss", query)
          let TmpchartData = chartData.filter(item => item.query === query);           
          if (TmpchartData.length > 0) {
            //dynamicBarChart.labels.push(query)
            //console.log(seconditem)
            newDataset.label = seconditem; //languageValue + seconditem
            newDataset.data.push(TmpchartData[0].count);
            const colorIndex = index % Object.keys(CHART_COLORS).length;             
            newDataset.backgroundColor.push(Object.values(CHART_COLORS)[colorIndex]);
          }
          return firstitem;
        })
        dynamicBarChart.datasets.push(newDataset);
        return seconditem;
      })
      }
    }  
    return dynamicBarChart;
  }


   MyResourcetoChartJSData() {
    let { languageValue } = this.state;
    let dynamicBarChart = {
      labels: [], //types (resource types),
      datasets: [], //per language
    }; 

    if(this.state.checkedResourceList.length > 0 && this.state.LingualityList.length===0 && this.state.DataMediaTypeList.length===0 && this.state.DataTypeOfAccessList.length===0){
      languageValue && this.getDataFromIndexer(languageValue);
      this.addDataSet(dynamicBarChart, this.state.checkedResourceList, "resource_type")    
    }

    if(this.state.checkedFunctionList.length > 0  && this.state.SoftwareTypeOfAccessList.length===0 && this.state.SoftwareInputMediaTypeList.length===0 && this.state.SoftwareOutputMediaTypeList.length===0){
      languageValue && this.getDataFromIndexer(languageValue);
      this.addDataSet(dynamicBarChart, this.state.checkedFunctionList, "function")
    }
   
    if (this.state.LingualityList.length > 0){ 
      languageValue && this.getDataFromIndexer(languageValue);    
      this.state.isDatasetsCheckAll === true && this.addDataSet(dynamicBarChart, ["Dataset"], "lr_distinction", this.state.LingualityList , "linguality_type" );
      this.state.checkedResourceList.length > 0 && this.addDataSet(dynamicBarChart,this.state.checkedResourceList , "resource_type", this.state.LingualityList , "linguality_type"  ) 
    }

    if (this.state.DataMediaTypeList.length > 0){ 
      languageValue && this.getDataFromIndexer(languageValue);    
      this.state.isDatasetsCheckAll === true && this.addDataSet(dynamicBarChart, ["Dataset"], "lr_distinction", this.state.DataMediaTypeList , "media_type" );
      this.state.checkedResourceList.length > 0 && this.addDataSet(dynamicBarChart,this.state.checkedResourceList , "resource_type", this.state.DataMediaTypeList , "media_type"  ) 
    }

    if (this.state.DataTypeOfAccessList.length > 0){ 
      languageValue && this.getDataFromIndexer(languageValue);    
      this.state.isDatasetsCheckAll === true && this.addDataSet(dynamicBarChart, ["Dataset"], "lr_distinction", this.state.DataTypeOfAccessList , "type_of_access" );
      this.state.checkedResourceList.length > 0 && this.addDataSet(dynamicBarChart,this.state.checkedResourceList , "resource_type", this.state.DataTypeOfAccessList , "type_of_access"  ) 
    }

    if (this.state.SoftwareTypeOfAccessList.length > 0){ 
      languageValue && this.getDataFromIndexer(languageValue);    
      this.state.isSoftwareCheckAll === true && this.addDataSet(dynamicBarChart, ["Software"], "lr_distinction", this.state.SoftwareTypeOfAccessList , "type_of_access" );
      this.state.checkedFunctionList.length > 0 && this.addDataSet(dynamicBarChart,this.state.checkedFunctionList , "function", this.state.SoftwareTypeOfAccessList , "type_of_access"  ) 
    }
    if (this.state.SoftwareInputMediaTypeList.length > 0){ 
      languageValue && this.getDataFromIndexer(languageValue);    
      this.state.isSoftwareCheckAll === true && this.addDataSet(dynamicBarChart, ["Software"], "lr_distinction", this.state.SoftwareInputMediaTypeList , "input_media_type" );
      this.state.checkedFunctionList.length > 0 && this.addDataSet(dynamicBarChart,this.state.checkedFunctionList , "function", this.state.SoftwareInputMediaTypeList , "input_media_type"  ) 
    }

    if (this.state.SoftwareOutputMediaTypeList.length > 0){ 
      languageValue && this.getDataFromIndexer(languageValue);    
      this.state.isSoftwareCheckAll === true && this.addDataSet(dynamicBarChart, ["Software"], "lr_distinction", this.state.SoftwareOutputMediaTypeList , "output_media_type" );
      this.state.checkedFunctionList.length > 0 && this.addDataSet(dynamicBarChart,this.state.checkedFunctionList , "function", this.state.SoftwareOutputMediaTypeList , "output_media_type"  ) 
    }

    if (this.state.isDatasetsCheckAll === true && this.state.isSoftwareCheckAll === true && this.state.SoftwareInputMediaTypeList.length===0 && this.state.SoftwareOutputMediaTypeList.length===0 &&
      this.state.SoftwareTypeOfAccessList.length===0 && this.state.LingualityList.length===0 && this.state.DataMediaTypeList.length===0 && this.state.DataMediaTypeList.length===0) { 
      //console.log(languageValue)
        languageValue && this.getDataFromIndexer(languageValue);
      languageValue && dynamicBarChart.labels.push(languageValue);
      //this.addDataSet(dynamicBarChart, ["Dataset", "Software"],"lr_distinction");      
      this.addDatasetUnstacked(dynamicBarChart, this.state.chartData, "lr_distinction", "Dataset");
      this.addDatasetUnstacked(dynamicBarChart, this.state.chartData, "lr_distinction", "Software");
    }

    if (this.state.isDatasetsCheckAll === false && this.state.isSoftwareCheckAll === true && this.state.SoftwareInputMediaTypeList.length===0 && this.state.SoftwareOutputMediaTypeList.length===0 && 
      this.state.SoftwareTypeOfAccessList.length===0 && this.state.LingualityList.length===0 && this.state.DataMediaTypeList.length===0 && this.state.DataMediaTypeList.length===0) {
      languageValue && this.getDataFromIndexer(languageValue);
      this.addDataSet(dynamicBarChart, ["Software"],"lr_distinction");  
    }

    if (this.state.isDatasetsCheckAll === true && this.state.isSoftwareCheckAll === false && this.state.SoftwareInputMediaTypeList.length===0 && this.state.SoftwareOutputMediaTypeList.length===0 && 
      this.state.LingualityList.length===0 && this.state.LingualityList.length===0 && this.state.DataMediaTypeList.length===0 && this.state.DataMediaTypeList.length===0) {
      languageValue && this.getDataFromIndexer(languageValue);
      this.addDataSet(dynamicBarChart, ["Dataset"],"lr_distinction");  
    }

    return dynamicBarChart;
  }
 

  render() {
    const { /*chartData ,*/ expandedLangEu, expandedLangOther, isDatasetsCheckAll, isSoftwareCheckAll} = this.state; 
    const dynamicBarChart = this.MyResourcetoChartJSData();
    //console.log(dynamicBarChart)     
    return <Grid container className="chart-container" spacing={1} direction="row">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters">
          <Grid container direction="column">
          <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <FormGroup>
                {expandedLangEu? (
                  <div>
                    <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleLangChange}>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) =>
                        <Grid item key={index}><FormControlLabel key={index} 
                        //value={language.glottocode}
                        value={language.elg_query_params} 
                        control={<Radio />} label={language.preferred_name} /></Grid>)
                      }
                    </RadioGroup>                    
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                  </div>
                ) : (
                  <div>
                     <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleLangChange}>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) =>
                        <Grid item key={index}><FormControlLabel key={index} 
                        //value={language.glottocode}
                        value={language.elg_query_params} 
                        control={<Radio />} label={language.preferred_name} /></Grid>)
                      }
                    </RadioGroup>                     
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                      : void 0}
                  </div>
                )}
              </FormGroup>
            </FormControl>
            <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other European languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <FormGroup>
                {expandedLangOther ? (
                  <div>
                  <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleLangChange}>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) =>
                      <Grid item key={index}><FormControlLabel key={index} 
                      //value={language.glottocode}
                      value={language.elg_query_params} 
                      control={<Radio />} label={language.preferred_name} /></Grid>)
                    }
                  </RadioGroup>                    
                  <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                </div>
              ) : (
                <div>
                   <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleLangChange}>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) =>
                      <Grid item key={index}><FormControlLabel key={index} 
                      //value={language.glottocode}
                      value={language.elg_query_params} 
                      control={<Radio />} label={language.preferred_name} /></Grid>)
                    }
                  </RadioGroup>                     
                  {this.state.languages.length > truncated_number_of_items_to_show ?
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                    : void 0}
                </div>
                )}
              </FormGroup>
            </FormControl>
          </Grid>
        </Paper>
      </Grid>
      <Grid item sm={10}>
        <Grid container direction="column">
          <Grid item>
          <Accordion defaultExpanded className="selection-panel">
        <AccordionSummary
          expandIcon={<ExpandMoreIcon style={{ fontSize: 30 }} className="grey--font"/>}
          aria-controls="panel1c-content"
          id="panel1c-header" className="chart-header"> <Typography variant="h4" className="bold">Filter by:</Typography> </AccordionSummary>
        <AccordionDetails>             
              <Grid container  spacing={3} direction="row">                
                <Grid item container sm={12} direction="row" justifyContent = "space-between" className="selection-row"> 
                  <Grid item container sm={6}  direction="row">
                    <Grid item container sm={12} direction="row"   alignItems="flex-start" justifyContent="flex-start">
                      <Grid item sm={3}>
                        <FormControlLabel className="pl05"
                          control={<Checkbox
                            checked={this.state.isDatasetsCheckAll}
                            //indeterminate={this.allInDiffStateResourceType()}
                            color="primary"
                            onChange={e => { this.handleDatasetCheckAll(e) }}
                            name={"datasets"} />}
                          label={"Datasets"}
                        />
                      </Grid>
                      <Grid item container sm={9} direction="row">
                      <Grid item sm={12} className="bg-light-grey rounded pl-03"><Typography variant="caption">Resource subclass</Typography></Grid>
                        {ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map((type, index) => {
                          return <Grid item sm={6} key={index}>
                            <FormControlLabel className="pl05"
                              control={<Checkbox
                                checked={this.state.checkedResourceList.find(item => item === type) ? true : false}
                                color="primary"
                                onChange={e => { this.handleResourceCheck(e) }}
                                name={type} />}
                              label={type}
                            />
                          </Grid>
                        })}
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item container sm={6}   spacing={2} direction="row">
                    <Grid item container sm={12} direction="row">
                      <Grid item sm={3}><Typography variant="caption" className="bold">Linguality type:</Typography></Grid>
                      {LingualityType && LingualityType.length > 0 && LingualityType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small"
                            className="ChipTagMatrix"
                            variant={this.state.LingualityList.find(item => item === type) ? "default" : "outlined"}
                            label={type}
                            onClick={() => this.handleLingualityTypeClick(type)}
                            onDelete={this.state.LingualityList.find(item => item === type) && (() => this.handleLingualityTypeDelete(type))}
                            disabled= {(this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll) && this.state.DataMediaTypeList.length === 0 && this.state.DataTypeOfAccessList.length === 0  ? false : true }
                          />
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12}direction="row">
                    <Grid item sm={3}><Typography variant="caption" className="bold">Media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small"
                          className="ChipTagMatrix"
                            label={type}
                            variant={this.state.DataMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onClick={() => this.handleDataMediaTypeClick(type)}
                            onDelete={this.state.DataMediaTypeList.find(item => item === type) && (() => this.handleDataMediaTypeDelete(type))}
                            disabled= {(this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll)  && this.state.LingualityList.length === 0 && this.state.DataTypeOfAccessList.length === 0    ? false : true }
                          />
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Access Rights:</Typography></Grid>
                    <Grid item container sm={9}>{TypeOfAccess && TypeOfAccess.length > 0 && TypeOfAccess.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.DataTypeOfAccessList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.DataTypeOfAccessList.find(item => item === type) && (() => this.handleDataTypeOfAccessDelete(type))}
                            onClick={() => this.handleDataTypeOfAccessClick(type, index)}
                            disabled= {(this.state.checkedResourceList.length > 0 || this.state.isDatasetsCheckAll) && this.state.DataMediaTypeList.length === 0 && this.state.LingualityList.length === 0 ? false : true }
                          />
                        </Grid>
                      })}
                      </Grid>  
                    </Grid>
                  </Grid>
                </Grid>

                <Grid item container sm={12}  direction="row" justifyContent = "space-between">
                  <Grid item container sm={6} direction="row">
                  <Grid item container sm={12} direction="row"   alignItems="flex-start" justifyContent="flex-start">
                    <Grid item sm={3}>
                      <FormControlLabel className="pl05"
                        control={<Checkbox
                          checked={this.state.isSoftwareCheckAll}
                          //indeterminate={this.allInDiffStateFunction()}
                          color="primary"
                          onChange={e => { this.handleSoftwareCheckAll(e) }}
                          name={"software"} />}
                        label={"Software"}
                      />
                    </Grid>
                    <Grid item container sm={9} direction="row">
                    <Grid item sm={12} className="bg-light-grey rounded pl-03"><Typography variant="caption">Functions</Typography></Grid>
                      {Functions && Functions.length > 0 && Functions.map((type, index) => {
                        return <Grid item sm={6} key={index} >
                          <FormControlLabel className="pl05"
                            control={<Checkbox
                              checked={this.state.checkedFunctionList.find(item => item === type) ? true : false}
                              color="primary"
                              onChange={e => { this.handleFunctionCheck(e) }}
                              name={type} />}
                            label={type.replace("Collection", '')}
                          />
                        </Grid>
                      })}
                    </Grid>
                  </Grid>
                  </Grid>
                  
                  <Grid item container sm={6}  spacing={2} direction="row">
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Input media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareInputMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareInputMediaTypeList.find(item => item === type) && (() => this.handleSoftwareInputMediaTypeDelete(type))}
                            onClick={() => this.handleSoftwareInputMediaTypeClick(type, index)} 
                            //disabled= {this.state.checkedFunctionList.length>0 || this.state.isSoftwareCheckAll ? false : true }/>
                            disabled= {(this.state.checkedFunctionList.length > 0 || this.state.isSoftwareCheckAll) && this.state.SoftwareOutputMediaTypeList.length === 0 && this.state.SoftwareTypeOfAccessList.length === 0 ? false : true }/>
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12} direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Output media type:</Typography></Grid>
                      {MediaType && MediaType.length > 0 && MediaType.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareOutputMediaTypeList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareOutputMediaTypeList.find(item => item === type) && (() => this.handleSoftwareOutputMediaTypeDelete(type))}
                            onClick={() => this.handleSoftwareOutputMediaTypeClick(type, index)} 
                            //disabled= {this.state.checkedFunctionList.length>0 || this.state.isSoftwareCheckAll ? false : true }/>
                            disabled= {(this.state.checkedFunctionList.length > 0 || this.state.isSoftwareCheckAll) && this.state.SoftwareInputMediaTypeList.length === 0 && this.state.SoftwareTypeOfAccessList.length === 0 ? false : true }/>
                        </Grid>
                      })}
                    </Grid>
                    <Grid item container sm={12}  direction="row">
                    <Grid item sm={3}><Typography variant="caption"  className="bold">Access Rights:</Typography></Grid>
                    <Grid item container sm={9}>  {TypeOfAccess && TypeOfAccess.length > 0 && TypeOfAccess.map((type, index) => {
                        return <Grid item key={index} >
                          <Chip size="small" label={type} className="ChipTagMatrix"
                            variant={this.state.SoftwareTypeOfAccessList.find(item => item === type) ? "default" : "outlined"}
                            onDelete={this.state.SoftwareTypeOfAccessList.find(item => item === type) && (() => this.handleSoftwareTypeOfAccessDelete(type))}
                            onClick={() => this.handleSoftwareTypeOfAccessClick(type, index)} 
                            //disabled= {this.state.checkedFunctionList.length>0 || this.state.isSoftwareCheckAll ? false : true }/>
                            disabled= {(this.state.checkedFunctionList.length > 0 || this.state.isSoftwareCheckAll) && this.state.SoftwareInputMediaTypeList.length === 0 && this.state.SoftwareOutputMediaTypeList.length === 0 ? false : true }/>
                        </Grid>
                      })}</Grid>
                    </Grid>
                  </Grid>
                </Grid>
                  {this.state.chartData && this.state.chartData.length > 0 && <Grid item sm={12}>
                    <Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip>
                  </Grid>}
                </Grid>
              </AccordionDetails>
            </Accordion>
          </Grid>

          
         
          {dynamicBarChart.datasets && dynamicBarChart.datasets.length > 0 && <Grid item sm={12}><BarChart BarData={dynamicBarChart} options={(isDatasetsCheckAll && isSoftwareCheckAll) ? verticalBarOptions : StackedBarOptions} /></Grid>}  

          
          </Grid>        
      </Grid>
    </Grid>
  }
}