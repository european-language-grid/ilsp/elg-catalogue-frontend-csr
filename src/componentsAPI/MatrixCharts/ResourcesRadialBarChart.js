import React from "react";
import axios from "axios";
//import _ from 'lodash';
import Chip from '@material-ui/core/Chip';
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography";
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import ProgressBar from "./../CommonComponents/ProgressBar";
import { ResponsiveRadialBar } from '@nivo/radial-bar';
import { DLE_LANGUAGES, DLE_COUNTS_ALL } from "./../../config/constants";
import { downloadBlob } from "./Utils";

const truncated_number_of_items_to_show = 6;

export default class ResourcesRadialBarChart extends React.Component {
  constructor(state) {
    super(state);
    this.state = {
      languages: "", checkedLanguagesList: [],
      chartData: [], checked: false, allDLEData: [],
      expandedLangEu: false, expandedLangOther: false, loading: false,
    }
    this.svgRef = React.createRef();
  }

  componentDidMount() { this.getEULanguages(); this.fetchAllData() }
  togglexpandedLangEu = () => { this.setState({ expandedLangEu: !this.state.expandedLangEu }) }
  togglexpandedLangOther = () => { this.setState({ expandedLangOther: !this.state.expandedLangOther }) }

  getEULanguages() {
    this.setState({ loading: true });
    axios.get(DLE_LANGUAGES)
      .then(res => {
        this.setState({ languages: res.data.results });
        //console.log(res.data.results)
      }).catch(err => { this.setState({ loading: false }); });

  }

  adddatasetBaseItem(datasetBaseItem) {
    this.setState({ chartData: [...this.state.chartData, datasetBaseItem] })
  }


  fetchAllData() {
    this.setState({ loading: true });
    axios.get(DLE_COUNTS_ALL)
      .then(res => {
        this.setState({ allDLEData: res.data, loading: false });      
      }).catch(err => { this.setState({ loading: false }); });

  }

  fetchDataForRadial = (langName, glottocode) => {
    let { allDLEData } = this.state;
    let filter_res = allDLEData[glottocode];
    let datasetBaseItem = { id: langName, data: [] };
    Object.keys(filter_res).map(value => {
      let myobj = { "x": "", "y": "" }
      myobj.x = value.replace("Collection", "").replace("Other", "Other functions of software");
      myobj.y = Number(filter_res[value]);
      datasetBaseItem.data.push(JSON.parse(JSON.stringify(myobj)));
      return void 0;
    })
    this.adddatasetBaseItem(datasetBaseItem);
  }


  addLangChecked2List = (lang) => {
    const { checkedLanguagesList } = this.state;
    checkedLanguagesList.push(lang);
    this.setState({ checkedLanguagesList });
    let filterLanguage = this.state.languages.filter(item => item.glottocode === lang) || "";       
    let langName = filterLanguage[0].preferred_name;
    this.fetchDataForRadial(langName, filterLanguage[0].glottocode);
  }

  removeLangFromCheckedList = (lang) => {
    if (this.state.isLangSelectAll) {
      this.setState({ isLangSelectAll: false });
    }
    let { checkedLanguagesList, chartData } = this.state;
    checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
    this.setState({ checkedLanguagesList });
    let filterLanguage = this.state.languages.filter(item => item.glottocode === lang) || "";
    let langName = filterLanguage[0].preferred_name;
    chartData = chartData.filter(item => item.id !== langName);
    this.setState({ chartData });
  }

  handleLangCheck = (e) => {
    if (e.target.checked) {
      this.addLangChecked2List(e.target.name);
    } else {
      this.removeLangFromCheckedList(e.target.name);
    }
  }

  addAllCheckedLang2List = () => {
    this.setState({ isLangSelectAll: !this.state.isLangSelectAll });
    let items = this.state.languages;
    (items.map(item => this.addLangChecked2List(item.glottocode)));

  }

  removeAllCheckedLangFromList = () => {
    this.setState({ isLangSelectAll: false });
    this.clearAllFilters();
  }

  handleLangCheckAll = (e) => {
    if (e.target.checked) {
      this.removeAllCheckedLangFromList();
      this.setState({ chartData: [] }); //clear all
      this.addAllCheckedLang2List();
    } else {
      this.removeAllCheckedLangFromList();
    }
  }

  addAllEUCheckedLang2List = () => {
    this.setState({ isEULangSelectAll: !this.state.isEULangSelectAll });
    let items = this.state.languages;
    (items.map(item => item.eu === true && this.addLangChecked2List(item.glottocode, "eu")));
  }

  removeAllEUCheckedLangFromList = () => {
    this.setState({ isEULangSelectAll: false });
    this.clearAllData("eu");
  }

  handleEULangCheckAll = (e) => {
    if (e.target.checked) {
      this.removeAllEUCheckedLangFromList();
      this.addAllEUCheckedLang2List();
    } else {
      this.removeAllEUCheckedLangFromList();
    }
  }

  addAllOtherCheckedLang2List = () => {
    this.setState({ isOtherLangSelectAll: !this.state.isOtherLangSelectAll });
    let items = this.state.languages;
    (items.map(item => item.eu === false && this.addLangChecked2List(item.glottocode, "other")));
  }

  removeAllOtherCheckedLangFromList = () => {
    this.setState({ isOtherLangSelectAll: false });
    this.clearAllData("other");
  }

  handleOtherLangCheckAll = (e) => {
    if (e.target.checked) {
      this.removeAllOtherCheckedLangFromList();
      this.addAllOtherCheckedLang2List();
    } else {
      this.removeAllOtherCheckedLangFromList();
    }
  }

  clearAllFilters() {
    this.setState({ checkedLanguagesList: [], chartData: [] });
  }

  downloadSVG(){   
    const svg = this.svgRef.current.innerHTML;
    const blob = new Blob([svg], { type: "image/svg+xml" });
    downloadBlob(blob, `myimage.svg`);    
  }


  render() {
    const { expandedLangEu, expandedLangOther } = this.state;
    let dynamicBarChart = this.state.chartData; 
    return <Grid container className="chart-container" spacing={1} direction="row">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters">
          {this.state.loading ?
            <ProgressBar />
            :
            <Grid container direction="column">
              {/*<div className="bg-white pt-05 pb-05">
              <FormControlLabel className="pl05"
                control={<Checkbox
                  checked={this.state.isLangSelectAll}
                  color="primary"
                  onChange={e => { this.handleLangCheckAll(e) }}
                  name={"select all languages"} />}
                label={"Select all"}
                disabled={this.state.isEULangSelectAll && this.state.isOtherLangSelectAll}
              />
                </div>*/}
              <Grid item xs={12} sm={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>
              <FormControl component="fieldset" className="pl05">
                {/*<div className="italic pt-05 pb-05">
                <FormControlLabel
                  control={<Checkbox
                    checked={this.state.isEULangSelectAll}
                    color="primary"
                    onChange={e => { this.handleEULangCheckAll(e) }}
                    name={"select all eu languages"} />}
                  label={"Select all EU"}
                />
              </div>*/}
                <FormGroup>
                  {expandedLangEu ? (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                    </div>
                  ) : (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      {this.state.languages.length > truncated_number_of_items_to_show ?
                        <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                        : void 0}
                    </div>
                  )
                  }
                </FormGroup>
              </FormControl>
              <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other European languages</Typography></Grid>
              <FormControl component="fieldset" className="pl05">
                {/*<div className="italic pt-05 pb-05">
                <FormControlLabel
                  control={<Checkbox
                    checked={this.state.isOtherLangSelectAll}
                    color="primary"
                    onChange={e => { this.handleOtherLangCheckAll(e) }}
                    name={"select all other languages"} />}
                  label={"Select all other"}
                />
                  </div>*/}
                <FormGroup>
                  {expandedLangOther ? (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                    </div>
                  ) : (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      {this.state.languages.length > truncated_number_of_items_to_show ?
                        <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                        : void 0}
                    </div>
                  )
                  }
                </FormGroup>
              </FormControl>
            </Grid>
          }
        </Paper>
      </Grid>
      <Grid item sm={10}>
        <Grid container spacing={2} direction="column">
          <Grid item>
            <Paper elevation={3} variant="outlined" style={{ padding: "10px" }} className="selection-panel">

              <Grid container direction="row" style={{ flexWrap: "nowrap", gap: "1em", alignItems: "center" }}>
                <Grid item container sm={12} spacing={2} direction="row" alignItems="center" className="bg-light-grey rounded">
                  <Grid item sm={10}><Typography variant="h5" className="bold padding15">This graph compares the numbers of different resource types across the selected languages. </Typography>
                  </Grid>
                  {dynamicBarChart && dynamicBarChart.length > 0 && <Grid item container sm={2} spacing = {1} justify-content="flex-end" >
                  <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip></Grid>
                    <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Export as SVG"} onClick={() => this.downloadSVG()} style={{ float: "right" }}></Chip></Grid>
                  </Grid>}
                </Grid>


              </Grid>
              <Grid item container sm={12}>

                {dynamicBarChart && dynamicBarChart.length > 0 && <Grid item sm={12} style={{ height: "100%", width: "100%", overflow: "hidden" }}>
                  <div style={{ height: 650 }} ref={this.svgRef}>
                    <ResponsiveRadialBar
                      key={Math.random()}
                      data={dynamicBarChart}
                      //valueFormat=">-.2f"
                      theme={{
                        fontSize: 13,
                        textColor: "#212932",
                        axis: { legend: { text: { fontSize: 14, fill: "#212932" } } }
                      }}
                      padding={0.4}
                      cornerRadius={2}
                      margin={{ top: 40, right: 150, bottom: 40, left: 10 }}
                     // colors={{ scheme: 'paired' }}
                      colors={["#a6cee3",
                      "#1f78b4",
                      "#b2df8a",
                      "#33a02c",
                      "#fb9a99",
                      "#e31a1c",
                      "#fdbf6f",
                      "#ff7f00",
                      "#cab2d6",
                      "#6a3d9a",
                      "#ffff99",
                      "#b15928",
                      "#f92a82",
                      "#495867"]}
                      radialAxisStart={{ tickSize: 5, tickPadding: 5, tickRotation: 0 }}
                      circularAxisOuter={{ tickSize: 5, tickPadding: 12, tickRotation: 0 }}
                      legends={[
                        {
                          anchor: 'right',
                          direction: 'column',
                          justify: false,
                          translateX: -85,
                          translateY: 0,
                          itemsSpacing: 6,
                          itemDirection: 'left-to-right',
                          itemWidth: 100,
                          itemHeight: 18,
                          itemTextColor: '#999',
                          symbolSize: 18,
                          symbolShape: 'square',
                          effects: [
                            {
                              on: 'hover',
                              style: {
                                itemTextColor: '#000'
                              }
                            }
                          ]
                        }
                      ]}
                    />
                  </div>

                </Grid>}


              </Grid>



            </Paper>
          </Grid>

        </Grid>
      </Grid>
    </Grid>
  }
}