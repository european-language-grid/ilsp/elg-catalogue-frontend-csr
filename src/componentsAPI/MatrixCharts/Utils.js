import colorLib from '@kurkle/color';
import randomColor from "randomcolor";

export function transparentize(value, opacity) {
  var alpha = opacity === undefined ? 0.5 : 1 - opacity;
  return colorLib(value).alpha(alpha).rgbString();
}

// Returns a hex code for a "truly random" color
/*let bgcolor = randomColor({
  hue: "random",
  count: 1, //create multpile random colors, this number should be the size of the languages to show on the radar
  format: "rgba",
  luminosity: "light",
  alpha: 0.5,
});
*/

export var randomScalingFactor = function () {
  return Math.round(Math.random() * 100)
}


export function getRandomArbitrary(min, max = NAMED_COLORS.length) {
  return (Math.random() * (max - min) + min);
}


export function downloadBlob(blob, filename) {
  const objectUrl = URL.createObjectURL(blob);

  const link = document.createElement("a");
  link.href = objectUrl;
  link.download = filename;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);

  setTimeout(() => URL.revokeObjectURL(objectUrl), 5000);
}

//let bdcolor = randomColor({
//  hue: "#f5f7fa",
//  format: "rgba",
//  luminosity: "light",
//}),

export const CHART_COLORS = {
  highlightpurplelight: '#beadeb' ,  
  highlightTealLight: '#CEF0F3',
  yellow: 'rgb(255, 205, 86)',
  highlightPinkLight: '#ebadce',
  highlightMint: '#25c58f',   
  blue: 'rgb(54, 162, 235)',     
  elgYellow: '#ebd6ad',
  graysGlossy: '#ccd2d8',
  highlightvioletvivid: '#b447eb' ,  
  graysBright: '#9ca5b0',
  highlightgreendark: '#39a329' ,
  graysGloomy: '#636e7b',  
  highlightPink: '#d65c9d',
  highlightpurplevivid: '#7347eb',  
  highlightPinkDark: '#a3296a',
  highlightPinkDim: '#521435',
  highlightPurple: '#7c5cd6',  
  highlightTeal: '#299ba3',
  orange: '#ebb447', 
  highlightTealDim: '#144D52',
  highlightTealDark: '#289AA4',
  highlightDark: '#144e52',  
  violet:  '#ad5cd6' ,
  red: '#d90429',
  green: 'rgb(75, 192, 192)',
  highlightPinkVivid: '#eb479e',
  highlightvioletdark: '#7a29a3' ,
  highlightvioletdim: '#3d1452' ,  
  highlightpurpledark: '#4929a3' ,
  highlightpurpledim: '#251452' ,
  highlightgreenlight: '#b6ebad' ,
  highlightgreen:  '#6cd65c' ,
  highlightgreenvivid: '#5deb47' ,
  
  highlightgreendim: '#1d5214 ',
  highlightorangedim: '#523d14' ,

};

const NAMED_COLORS = [
  CHART_COLORS.red,
  CHART_COLORS.orange,
  CHART_COLORS.yellow,
  CHART_COLORS.green,
  CHART_COLORS.blue,
  CHART_COLORS.purple,
  CHART_COLORS.grey,
  CHART_COLORS.elgYellow,
  CHART_COLORS.graysGlossy,
  CHART_COLORS.graysBright,
  CHART_COLORS.highlightPinkLight,
  CHART_COLORS.highlightPink,
  CHART_COLORS.highlightPinkVivid,
  CHART_COLORS.highlightPinkDark,
  CHART_COLORS.highlightPinkDim,
  CHART_COLORS.highlightPinkDark,
  CHART_COLORS.highlightPurple,
  CHART_COLORS.highlightTeal,
  CHART_COLORS.highlightTealLight,
  CHART_COLORS.highlightTealDim,
  CHART_COLORS.highlightTealDark,
  CHART_COLORS.highlightDark,
  CHART_COLORS.violet,
  CHART_COLORS.highlightvioletvivid,
  CHART_COLORS.highlightvioletdark,
  CHART_COLORS.highlightvioletdim,
  CHART_COLORS.highlightpurplelight,
  CHART_COLORS.highlightpurplevivid,
  CHART_COLORS.highlightpurpledark,
  CHART_COLORS.highlightpurpledim,
  CHART_COLORS.highlightgreenlight,
  CHART_COLORS.highlightgreen,
  CHART_COLORS.highlightgreenvivid,
  CHART_COLORS.highlightgreendark,
  CHART_COLORS.highlightgreendim,
];

export function namedColor(index) {  
  //console.log(index)
  if(NAMED_COLORS[index % NAMED_COLORS.length]){
    return NAMED_COLORS[index % NAMED_COLORS.length];
  }
  else return randomColor();
  
}


export const Functions = [
  "Text Processing Collection",
    "Speech Processing Collection",
    "Information Extraction and Information Retrieval Collection",
    "Image / Video Processing Collection",
    "Translation Technologies Collection",
    "Natural Language Generation Collection",
    "Support operation Collection",
    "Human Computer Interaction Collection",
    "Other",
];

export const ResourceTypes = [
  "Corpus",
  "Model",
  "Grammar",
  "Lexical/Conceptual resource",
  "Uncategorized Language Description",
];

export const LingualityType = ["monolingual", "bilingual", "multilingual"];
export const MediaType = ["text", "audio", "image", "video", "numerical text"];
export const TypeOfAccess = ["unspecified", "research use allowed", "derivatives not allowed", "redistribution not allowed", "commercial uses not allowed", "no conditions", "other specific restrictions", "attribution required"];

export function mapLabelsToColors(Label){  
   
  let color = CHART_COLORS.yellow ; 
  let label = decodeURIComponent(Label);    
  if (Label === "Software") {color =  CHART_COLORS.yellow}
  if (Label === "Dataset") {color =  CHART_COLORS.highlightTeal}
   
  if (Functions.includes(label)) {
    let index = Functions.findIndex(item => item===label)
    const colorIndex = index % Object.keys(CHART_COLORS).length;
    color = Object.values(CHART_COLORS)[colorIndex]
  }
  if (ResourceTypes.includes(label)) {
    let index = ResourceTypes.findIndex(item => item===label)      
    const colorIndex = index % Object.keys(CHART_COLORS).length;
    color = Object.values(CHART_COLORS)[colorIndex]       
  }
  if (MediaType.includes(label)) {
    let index = MediaType.findIndex(item => item===label)
    const colorIndex = index % Object.keys(CHART_COLORS).length;
    color = Object.values(CHART_COLORS)[colorIndex]
  } 
  if (TypeOfAccess.includes(label)) {
    let index = TypeOfAccess.findIndex(item => item===label)
    const colorIndex = index % Object.keys(CHART_COLORS).length;
    color = Object.values(CHART_COLORS)[colorIndex]
  }    
  if (LingualityType.includes(label)) {
    let index = LingualityType.findIndex(item => item===label)
    const colorIndex = index % Object.keys(CHART_COLORS).length;
    color = Object.values(CHART_COLORS)[colorIndex]
  } 
  return color;
}