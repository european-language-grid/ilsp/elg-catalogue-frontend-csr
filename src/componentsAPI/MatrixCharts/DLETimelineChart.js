import React from "react";
import axios from "axios";
import _ from 'lodash';
import Chip from '@material-ui/core/Chip';
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography";
import Radio from '@material-ui/core/Radio';
import FormLabel from '@material-ui/core/FormLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import { ResponsiveAreaBump } from '@nivo/bump';
import { ResponsiveLine } from '@nivo/line';
import { DLE_LANGUAGES, ALL_MATRIX_ELG_DATA, ALL_MATRIX_ELG_DATA_DATE_RANGE, ALL_DLE_PERIODS } from "./../../config/constants";
import { downloadBlob } from "./Utils";

const startTime = "2020-01-01";
const truncated_number_of_items_to_show = 6;
//const dlePeriods={"2020":{"quarter":{"1":"2020-01-01__2020-03-31","2":"2020-04-01__2020-06-30","3":"2020-07-01__2020-09-30","4":"2020-10-01__2020-12-31"},"half_year":{"1":"2020-01-01__2020-06-30","2":"2020-07-01__2020-12-31"}},
//"2021":{"quarter":{"1":"2021-01-01__2021-03-31","2":"2021-04-01__2021-06-30","3":"2021-07-01__2021-09-30","4":"2021-10-01__2021-12-31"},"half_year":{"1":"2021-01-01__2021-06-30","2":"2021-07-01__2021-12-31"}},
//"2022":{"quarter":{"1":"2022-01-01__2022-03-31","2":"2022-04-01__2022-06-30","3":"2022-07-01__2022-09-30","4":"2022-10-01__2022-12-31"},"half_year":{"1":"2022-01-01__2022-06-30","2":"2022-07-01__2022-12-31"}}};


export default class DLETimelineChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      languages: "", typevalue: "sum", chartData: [], chartDataSUM: [], checkedLanguagesList: [], isLangSelectAll: false, isEULangSelectAll: false, isOtherLangSelectAll: false,
      expandedLangEu: false, expandedLangOther: false, open: false, Contextualopen: false, EUList: [], OtherList: [], loading: false, allDLEData: [], allDLEDataSUM: [],
      allDLEPeriodData: [], allQuarterDlePeriods: [], allSemesterDlePeriods: [], allQuarterDlePeriodsSUM: [], allSemesterDlePeriodsSUM: [],
      isSemesterCheckAll: false, isQuarterCheckAll: false,
    }
    this.svgAreaBumpRef = React.createRef();
    this.svgLineRef = React.createRef();
    this.downloadSVGArea = this.downloadSVGArea.bind(this);
    this.downloadSVGLine = this.downloadSVGLine.bind(this);
  }

  componentDidMount() {
    this.getEULanguages();
    this.getAllDlePeriods();
  }

  clearAllFilters() {
    this.setState({
      chartData: [], checkedLanguagesList: [], isLangSelectAll: false, isEULangSelectAll: false, isOtherLangSelectAll: false,
      EUList: [], OtherList: [], isSemesterCheckAll: false, isQuarterCheckAll: false,
    });
  }

  clearAllData(type) {

    let { checkedLanguagesList, EUList, OtherList, chartData } = this.state;
    if (type === "eu") { //I am clearing only the eu data
      EUList.length > 0 && EUList.map((lang, idx) => {
        checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
        this.setState({ checkedLanguagesList });
        chartData = chartData.filter(item => item.id !== lang);
        this.setState({ chartData });
        return lang;
      })
    }
    if (type === "other") { //I am clearing only the eu data
      OtherList.length > 0 && OtherList.map((lang, idx) => {
        checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
        this.setState({ checkedLanguagesList });
        chartData = chartData.filter(item => item.id !== lang);
        this.setState({ chartData });
        return lang;
      })
    }
  }

  getAllDLEData() {
    this.setState({ loading: true });
    axios.get(ALL_MATRIX_ELG_DATA)
      .then(res => {
        this.setState({ allDLEData: res.data.results, chartData: [] });
        //console.log(res.data.results);
      }).catch(err => { this.setState({ loading: false }); });
  }

  getDLEDataPeriods(all_periods) {
    let newArray = [];
    all_periods && all_periods.length > 0 && all_periods.map((period) => {
      axios.get(ALL_MATRIX_ELG_DATA_DATE_RANGE(period.value))
        .then(res => {
          newArray.push(res.data)
          //console.log(newArray)
          //this.setState({ allDLEPeriodData, chartData: [] });
        }).catch(err => { console.log(err) });
      return period;
    })
    this.setState({ allDLEData: newArray, chartData: [] });
  }

  getDLEDataPeriodsSUM(all_periods) {
    let newArray = [];
    all_periods && all_periods.length > 0 && all_periods.map((period) => {
      axios.get(ALL_MATRIX_ELG_DATA_DATE_RANGE(period.value))
        .then(res => {
          newArray.push(res.data)
        }).catch(err => { console.log(err) });
      return period;
    })
    this.setState({ allDLEDataSUM: newArray, chartDataSUM: [] });
  }

  updateDLEData = (period) => {
    if (period) { this.getDLEDataPeriods(period); }
    else { this.getAllDLEData() }
  }


  getAllDlePeriods() {
    this.setState({ loading: true });
    const newArray = [];
    const newArray2 = [];
    const newArray3 = [];
    const newArray4 = [];
    axios.get(ALL_DLE_PERIODS)
      .then(res => {
        let dlePeriods = res.data;
        //console.log(res.data);
        Object.entries(dlePeriods).map(([key, value]) => {
          //key = year
          //value["half_year"] = {"1":"2020-01-01__2020-06-30","2":"2020-07-01__2020-12-31"}
          //value["quarter"] = {"1":"2020-01-01__2020-03-31","2":"2020-04-01__2020-06-30","3":"2020-07-01__2020-09-30","4":"2020-10-01__2020-12-31"}     
          let obj = value["quarter"]
          let obj2 = value["half_year"]
          Object.entries(obj).map(([objkey, objvalue]) => {
            //let i = ordinal_suffix_of(objkey); 
            let i = objkey;
            let label = `Q${i} of ${key}`; //`${i} quarter of ${key}`;
            let obj = { "label": label, "value": objvalue, "year": key };
            newArray.push(obj); // Push the object            
            let parts = objvalue.split("__")
            let date_range = startTime + "__" + parts[1];
            let sumobj = { "label": label, "value": date_range, "year": key };
            newArray3.push(sumobj); // Push the object    
            return void 0;
          })
          Object.entries(obj2).map(([objkey, objvalue]) => {
            //let i = ordinal_suffix_of(objkey); 
            let i = objkey;
            let label = `S${i} of ${key}`; //`${i} semester of ${key}`;
            let obj = { "label": label, "value": objvalue, "year": key };
            newArray2.push(obj); // Push the object             
            let parts = objvalue.split("__")
            let date_range = startTime + "__" + parts[1];
            let sumobj = { "label": label, "value": date_range, "year": key };
            newArray4.push(sumobj); // Push the object     
            return void 0;
          })
          return void 0;
        })
        this.setState({
          allQuarterDlePeriods: newArray, allQuarterDlePeriodsSUM: newArray3
        });
        this.setState({
          allSemesterDlePeriods: newArray2, allSemesterDlePeriodsSUM: newArray4
        });
        //console.log(newArray, newArray3)
      }).catch(err => { this.setState({ loading: false }); });

  }

  filterELEData(glottolog) { //glottolog here is lang_id
    let { chartData, allDLEData } = this.state;
    let tmp_array = [];
    let myobj = {
      lang: glottolog,
      data: []
    }
    allDLEData && allDLEData.length > 0 && allDLEData.map((allItems, idx) => {
      //console.log(allItems)      
      let filter_res = allItems.filter(item => item.id === glottolog)
      tmp_array[idx] = (filter_res[0]);
      return void 0;
    })
    myobj.data = tmp_array
    //console.log(myobj) 
    chartData.push(myobj)
    this.setState({ chartData });
  }


  filterELEDataSUM(glottolog) { //glottolog here is lang_id
    let { chartDataSUM, allDLEDataSUM } = this.state;
    let tmp_array = [];
    let myobj = {
      lang: glottolog,
      data: []
    }
    allDLEDataSUM && allDLEDataSUM.length > 0 && allDLEDataSUM.map((allItems, idx) => {
      //console.log(allItems)      
      let filter_res = allItems.filter(item => item.id === glottolog)
      tmp_array[idx] = (filter_res[0]);
      return void 0;
    })
    myobj.data = tmp_array
    //console.log(myobj) 
    chartDataSUM.push(myobj)
    this.setState({ chartDataSUM });
  }

  addLangChecked2List = (lang, type) => {
    const { checkedLanguagesList, EUList, OtherList } = this.state;
    if (checkedLanguagesList.indexOf(lang) === -1) {//checks if i have allready selected the facet, if not add it to array
      checkedLanguagesList.push(lang);
      this.setState({ checkedLanguagesList });
      this.filterELEData(lang);
      this.filterELEDataSUM(lang);
      if (type && type === "eu") { EUList.push(lang); this.setState({ EUList }); }
      if (type && type === "other") { OtherList.push(lang); this.setState({ OtherList }); }
    }
  }

  removeLangFromCheckedList = (lang) => {
    if (this.state.isLangSelectAll) {
      this.setState({ isLangSelectAll: false });
    }
    let { checkedLanguagesList, chartData } = this.state;
    checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
    this.setState({ checkedLanguagesList });
    chartData = chartData.filter(item => item.lang !== lang);
    this.setState({ chartData });
  }

  getLabelFromPeriod = (item, type) => {
    switch (type) {
      case "intensity": {
        let tmp_data_semester = "";
        let tmp_data_quarter = "";
        let tmp = item.date_range.from + "__" + item.date_range.to;
        this.state.allQuarterDlePeriods && this.state.allQuarterDlePeriods.length > 0 ? tmp_data_quarter = this.state.allQuarterDlePeriods.filter(item => item.value === tmp) : tmp_data_quarter = "";
        this.state.allSemesterDlePeriods && this.state.allSemesterDlePeriods.length > 0 ? tmp_data_semester = this.state.allSemesterDlePeriods.filter(item => item.value === tmp) : tmp_data_semester = "";
        return { tmp_data_semester, tmp_data_quarter }
      }
      case "sum":
        {
          let tmp_data_semester = "";
          let tmp_data_quarter = "";
          let tmp = item.date_range.from + "__" + item.date_range.to;
          this.state.allQuarterDlePeriodsSUM && this.state.allQuarterDlePeriodsSUM.length > 0 ? tmp_data_quarter = this.state.allQuarterDlePeriodsSUM.filter(item => item.value === tmp) : tmp_data_quarter = "";
          this.state.allSemesterDlePeriodsSUM && this.state.allSemesterDlePeriodsSUM.length > 0 ? tmp_data_semester = this.state.allSemesterDlePeriodsSUM.filter(item => item.value === tmp) : tmp_data_semester = "";
          return { tmp_data_semester, tmp_data_quarter }
        }
      default:
        return 0;
    }
  }

  MyResourcetoChartJSData(type) {
    let { chartData, chartDataSUM } = this.state;
    let dynamicRadarChart = [];

    this.state.checkedLanguagesList && this.state.checkedLanguagesList.length > 0 && this.state.checkedLanguagesList.map((language, index) => {
      let TmpchartData = [];
      let tmp_data_semester = []; let tmp_data_quarter = [];
      let filterLanguage = this.state.languages.filter(item => item.glottocode === language) || "";
      let langName = filterLanguage[0].preferred_name;
      let datasetBaseItem = {
        id: langName,
        data: []
      };
      if (type === "intensity") {
        TmpchartData = chartData.filter(item => item.lang === language);
      }
      if (type === "sum") {
        TmpchartData = chartDataSUM.filter(item => item.lang === language);
      }
      TmpchartData[0] && TmpchartData[0].data && TmpchartData[0].data.length > 0 && TmpchartData[0].data.map((item) => { //it will always be one so use [0]
        let myobj = {
          "x": "",
          "y": "",
          "test": ""
        }
        myobj.y = item.total_count;
        myobj.test = item.date_range.from + "__" + item.date_range.to;
        let temp_periods = this.getLabelFromPeriod(item, type);
        tmp_data_semester = this.state.isSemesterCheckAll ? temp_periods.tmp_data_semester : [];
        tmp_data_quarter = this.state.isQuarterCheckAll ? temp_periods.tmp_data_quarter : [];
        if (tmp_data_semester.length > 0)
          myobj.x = tmp_data_semester[0].label;
        if (tmp_data_quarter.length > 0)
          myobj.x = tmp_data_quarter[0].label;
        datasetBaseItem.data.push(myobj);
        return item;
      })
      var sortedObjs = _.sortBy(datasetBaseItem.data, 'test');
      datasetBaseItem.data = sortedObjs;
      //console.log(sortedObjs)  
      dynamicRadarChart.push(datasetBaseItem);
      return language;
    })
    return dynamicRadarChart;
  }

  handleLangCheck = (e) => {
    if (e.target.checked) {
      this.addLangChecked2List(e.target.name);
    } else {
      this.removeLangFromCheckedList(e.target.name);
    }
  }

  handleSemesterCheck = (e) => {
    this.clearAllFilters();
    this.setState({ isSemesterCheckAll: !this.state.isSemesterCheckAll });
    this.getDLEDataPeriods(this.state.allSemesterDlePeriods);
    this.getDLEDataPeriodsSUM(this.state.allSemesterDlePeriodsSUM);
    if (e.target.checked) {
      this.setState({ isQuarterCheckAll: false });
    }
  }

  handleQuarterCheck = (e) => {
    this.clearAllFilters();
    this.setState({ isQuarterCheckAll: !this.state.isQuarterCheckAll });
    this.getDLEDataPeriods(this.state.allQuarterDlePeriods);
    this.getDLEDataPeriodsSUM(this.state.allQuarterDlePeriodsSUM);
    if (e.target.checked) {
      this.setState({ isSemesterCheckAll: false });
    }
  }

  addAllCheckedLang2List = () => {
    this.setState({ isLangSelectAll: !this.state.isLangSelectAll });
    let items = this.state.languages;
    (items.map(item => this.addLangChecked2List(item.glottocode)));

  }

  removeAllCheckedLangFromList = () => {
    this.setState({ isLangSelectAll: false });
    this.clearAllFilters();
  }

  handleLangCheckAll = (e) => {
    if (e.target.checked) {
      this.removeAllCheckedLangFromList();
      this.setState({ chartData: [] }); //clear all
      this.addAllCheckedLang2List();
    } else {
      this.removeAllCheckedLangFromList();
    }
  }

  addAllEUCheckedLang2List = () => {
    this.setState({ isEULangSelectAll: !this.state.isEULangSelectAll });
    let items = this.state.languages;
    (items.map(item => item.eu === true && this.addLangChecked2List(item.glottocode, "eu")));
  }

  removeAllEUCheckedLangFromList = () => {
    this.setState({ isEULangSelectAll: false });
    this.clearAllData("eu");
  }

  handleEULangCheckAll = (e) => {
    if (e.target.checked) {
      this.removeAllEUCheckedLangFromList();
      this.addAllEUCheckedLang2List();
    } else {
      this.removeAllEUCheckedLangFromList();
    }
  }

  addAllOtherCheckedLang2List = () => {
    this.setState({ isOtherLangSelectAll: !this.state.isOtherLangSelectAll });
    let items = this.state.languages;
    (items.map(item => item.eu === false && this.addLangChecked2List(item.glottocode, "other")));
  }

  removeAllOtherCheckedLangFromList = () => {
    this.setState({ isOtherLangSelectAll: false });
    this.clearAllData("other");
  }

  handleOtherLangCheckAll = (e) => {
    if (e.target.checked) {
      this.removeAllOtherCheckedLangFromList();
      this.addAllOtherCheckedLang2List();
    } else {
      this.removeAllOtherCheckedLangFromList();
    }
  }

  getEULanguages() {
    this.setState({ loading: true });
    axios.get(DLE_LANGUAGES)
      .then(res => {
        this.setState({ languages: res.data.results });
      }).catch(err => { this.setState({ loading: false }); });
  }

  togglexpandedLangEu = () => {
    this.setState({ expandedLangEu: !this.state.expandedLangEu })
  }

  togglexpandedLangOther = () => {
    this.setState({ expandedLangOther: !this.state.expandedLangOther })
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  };

  handleClose = () => {
    this.setState({ open: false })
  }

  handleContextualClickOpen = () => {
    this.setState({ Contextualopen: true })
  };

  handleContextualClose = () => {
    this.setState({ Contextualopen: false })
  }

  handleTypeChange = (event) => {
    this.setState({ typevalue: event.target.value })
  }

  getCharts(type) {
    const dynamicRadarChart = this.MyResourcetoChartJSData(type);
    //const dynamicRadarChartSUM = this.MyResourcetoChartJSData("sum");
    return dynamicRadarChart;
  }

  downloadSVGArea() {
    const svg = this.svgAreaBumpRef.current.innerHTML;
    const blob = new Blob([svg], { type: "image/svg+xml" });
    downloadBlob(blob, `myimage.svg`);
  }

  downloadSVGLine() {
    const svg = this.svgLineRef.current.innerHTML;
    const blob = new Blob([svg], { type: "image/svg+xml" });
    downloadBlob(blob, `myimage.svg`);
  }



  render() {
    const { expandedLangEu, expandedLangOther } = this.state;
    const dynamicRadarChart = this.getCharts(this.state.typevalue);
    let chart_height = (this.state.isEULangSelectAll || this.state.isOtherLangSelectAll) ? 900 : 550;
    //console.log(dynamicRadarChart,dynamicRadarChartSUM ) 
    return <Grid container className="chart-container" spacing={1} direction="row">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters bg-orange-light mb1">
          <Grid container direction="column">
            <div className="border-radius pt1 pb1">
              <Grid item container direction="row" alignItems="center" spacing={1} sm={12}>
                <Grid item>
                  <FormControlLabel className="pl05"
                    control={<Checkbox
                      checked={this.state.isSemesterCheckAll}
                      color="primary"
                      onChange={e => { this.handleSemesterCheck(e) }}
                      name={"semester"}
                      disabled={this.state.isQuarterCheckAll} />}
                    label={<span style={{ fontWeight: "600" }}>Per semester</span>}
                  /></Grid>

                <Grid item >
                  <FormControlLabel className="pl05"
                    control={<Checkbox
                      checked={this.state.isQuarterCheckAll}
                      color="primary"
                      onChange={e => { this.handleQuarterCheck(e) }}
                      name={"quarter"}
                      disabled={this.state.isSemesterCheckAll} />}

                    label={<span style={{ fontWeight: "600" }}>Per quarter</span>}
                  /></Grid>
              </Grid>
              {/*<FormControlLabel className="pl05"
                control={<Checkbox
                  checked={this.state.isLangSelectAll}
                  color="primary"
                  onChange={e => { this.handleLangCheckAll(e) }}
                  name={"select all languages"} />}
                label={"Select all"}
                disabled={this.state.isEULangSelectAll && this.state.isOtherLangSelectAll}
                />*/}
            </div>
          </Grid>
        </Paper>
        <Paper elevation={1} className="chart-filters">
          <Grid container direction="column" className={(this.state.isSemesterCheckAll || this.state.isQuarterCheckAll) ? "simple-container" : "container-disabled"}>
            <Grid item xs={12} sm={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <div className="italic pt-05 pb-05">
                <FormControlLabel
                  control={<Checkbox
                    checked={this.state.isEULangSelectAll}
                    color="primary"
                    onChange={e => { this.handleEULangCheckAll(e) }}
                    name={"select all eu languages"} />}
                  label={"Select all EU"}
                />
              </div>
              <FormGroup>
                {expandedLangEu ? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>
            <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other European languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
              <div className="italic pt-05 pb-05">
                <FormControlLabel
                  control={<Checkbox
                    checked={this.state.isOtherLangSelectAll}
                    color="primary"
                    onChange={e => { this.handleOtherLangCheckAll(e) }}
                    name={"select all other languages"} />}
                  label={"Select all other"}
                />
              </div>
              <FormGroup>
                {expandedLangOther ? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>
          </Grid>
        </Paper>
      </Grid>
      <Grid item sm={10}>
        <Grid container direction="column">
          <Paper elevation={3} variant="outlined" style={{ padding: "10px" }} className="selection-panel">
            <Grid container direction="row" style={{ flexWrap: "nowrap", gap: "1em", alignItems: "center" }}>
              <Grid item container sm={12} className="bg-light-grey rounded p-1">
                <Grid item sm={10}>
                  {/*<Typography variant="h5" className="bold padding15">This graph presents the evolution of the technological factors for each European language in selected time periods.</Typography> */}
                  <FormControl component="fieldset"> <FormLabel component="legend">Select type of measurement</FormLabel>
                    <RadioGroup defaultValue="sum" aria-label="chart_metric" name="chart_metric" typevalue={this.state.typevalue} onChange={this.handleTypeChange}>
                      <FormControlLabel value="sum" control={<Radio />} label="Overall evolution of the number of resources" />
                      <FormControlLabel value="intensity" control={<Radio />} label="Intensity evolution of the number of resources" />
                    </RadioGroup>
                  </FormControl>
                </Grid>
                {dynamicRadarChart && dynamicRadarChart.length > 0 && <Grid item container spacing={1} sm={2}>
                  <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip></Grid>
                  {this.state.typevalue === "intensity" && <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Export as SVG"} onClick={() => this.downloadSVGArea()} style={{ float: "right" }}></Chip></Grid>}
                  {this.state.typevalue === "sum" && <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Export as SVG"} onClick={() => this.downloadSVGLine()} style={{ float: "right" }}></Chip></Grid>}

                </Grid>}

              </Grid>

            </Grid>
            <Grid item container sm={12} className="mt2">
              {this.state.typevalue === "intensity" && (dynamicRadarChart && dynamicRadarChart.length > 0) && (this.state.isSemesterCheckAll || this.state.isQuarterCheckAll) && (this.state.checkedLanguagesList && this.state.checkedLanguagesList.length > 0)
                && <Grid item sm={12} style={{ height: "100%", width: "100%", overflow: "hidden" }}>

                  <div style={{ height: chart_height }} ref={this.svgAreaBumpRef}>
                    <ResponsiveAreaBump data={dynamicRadarChart} margin={{ top: 50, right: 100, bottom: 40, left: 100 }} spacing={20} colors={{ scheme: 'paired' }} blendMode="multiply"
                      theme={{
                        fontSize: 13,
                        textColor: "#212932",
                        axis: { legend: { text: { fontSize: 14, fontWeight: 600, fill: "#212932" } } }
                      }}

                      defs={[{
                        id: 'dots',
                        type: 'patternDots',
                        background: 'inherit',
                        color: '#CEF0F3',
                        size: 4,
                        padding: 1,
                        stagger: true
                      },
                      {
                        id: 'lines',
                        type: 'patternLines',
                        background: 'inherit',
                        color: '#adebd6',
                        rotation: -45,
                        lineWidth: 6,
                        spacing: 10
                      }
                      ]}
                      fill={[
                        {
                          match: {
                            id: 'English'
                          },
                          id: 'dots'
                        },
                        {
                          match: {
                            id: 'German'
                          },
                          id: 'lines'
                        }
                      ]}
                      startLabel="id"
                      endLabel="id"
                      axisTop={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: -20,
                        legend: '',
                        legendPosition: 'middle',
                        legendOffset: -36
                      }}
                      axisBottom={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: -20,
                        legend: '',
                        legendPosition: 'middle',
                        legendOffset: 32
                      }}
                    />
                  </div>
                </Grid>}
              {this.state.typevalue === "sum" && (dynamicRadarChart && dynamicRadarChart.length > 0) && (this.state.isSemesterCheckAll || this.state.isQuarterCheckAll) && (this.state.checkedLanguagesList && this.state.checkedLanguagesList.length > 0)
                && <Grid item sm={12} style={{ height: "100%", width: "100%", overflow: "hidden" }}>

                  <div style={{ height: chart_height }} ref={this.svgLineRef}>
                    <ResponsiveLine
                      data={dynamicRadarChart}
                      theme={{
                        fontSize: 13,
                        textColor: "#212932",
                        axis: { legend: { text: { fontSize: 14, fontWeight: 600, fill: "#212932" } } }
                      }}

                      margin={{ top: 50, right: 110, bottom: 50, left: 80 }}
                      xScale={{ type: 'point' }}
                      yScale={{
                        type: 'linear',
                        min: 'auto',
                        max: 'auto',
                        stacked: false,
                        reverse: false
                      }}
                      yFormat=" >-.2f"
                      axisTop={null}
                      axisRight={null}
                      axisBottom={{
                        orient: 'bottom',
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: 'time',
                        legendOffset: 36,
                        legendPosition: 'middle'
                      }}
                      axisLeft={{
                        orient: 'left',
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: 'number of resources',
                        legendOffset: -60,
                        legendPosition: 'middle'
                      }}
                      colors={{ scheme: 'paired' }}
                      pointSize={10}
                      pointColor={{ theme: 'background' }}
                      pointBorderWidth={2}
                      pointBorderColor={{ from: 'serieColor' }}
                      pointLabelYOffset={-12}
                      useMesh={true}
                      legends={[
                        {
                          anchor: 'bottom-right',
                          direction: 'column',
                          justify: false,
                          translateX: 100,
                          translateY: 0,
                          itemsSpacing: 0,
                          itemDirection: 'left-to-right',
                          itemWidth: 80,
                          itemHeight: 20,
                          itemOpacity: 0.75,
                          symbolSize: 12,
                          symbolShape: 'circle',
                          symbolBorderColor: 'rgba(0, 0, 0, .5)',
                          effects: [
                            {
                              on: 'hover',
                              style: {
                                itemBackground: 'rgba(0, 0, 0, .03)',
                                itemOpacity: 1
                              }
                            }
                          ]
                        }
                      ]}
                    /> </div>

                </Grid>}


            </Grid>



          </Paper>
        </Grid>

      </Grid>
    </Grid>

  }
}