import React from "react";
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography";
import { Radar } from "react-chartjs-2";
import { Chart, registerables } from 'chart.js';


Chart.register(...registerables); //https://www.chartjs.org/docs/next/getting-started/integration.html


const RadarOptions = {
  reponsive: true,
  scale: {
    ticks: {
      min: 0,
      max: 16,
      stepSize: 2,
      showLabelBackdrop: false,
      backdropColor: "rgba(203, 197, 11, 1)"
    },
    angleLines: {
      color: "rgba(255, 255, 255, .3)",
      lineWidth: 1
    },
    gridLines: {
      color: "rgba(255, 255, 255, .3)",
      circular: true
    }
  }
};


export default class RadarChart extends React.Component {
   
    render() {
      //https://github.com/reactchartjs/react-chartjs-2/issues/90 
      //chart does not render new data on parent state update
      const RadarData = this.props.RadarData;
      const title = this.props.title;     

      if (!RadarData || RadarData.length === 0 ) {
        return <div></div>
      }
      return  <Grid item sm={12} className="chart-container radar">
        <Typography variant="h3" className="section-links FunctionCentered"> {title} </Typography>
        <Radar data={RadarData} options={RadarOptions} redraw={true} key={Math.random()}/>
        </Grid>          
      
    }
}