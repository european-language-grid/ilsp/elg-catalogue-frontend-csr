import { useMap } from "react-leaflet";
import L from "leaflet";
import { useEffect } from "react";

function Legend() {
  const   map   = useMap();

  useEffect(() => {
    // get color depending on population density value
    const getColor = d => {
      return d > 1000 ? '#103E42' :
      d > 500 ? '#185E63' :
        d > 200 ? '#248C94' :
          d > 100 ? '#289CA4' :
            d > 50 ? '#3AC5CF' :
              d > 20 ? '#6BD4DB' :
                d > 10 ? '#9CE2E7' :
                  '#ebd6ad';
    };

    const legend = L.control({ position: "bottomright" });

    legend.onAdd = () => {
      const div = L.DomUtil.create("div", "mapinfo legend");
      const grades = [0, 10, 20, 50, 100, 200, 500, 1000];
      let labels = [];
      let from;
      let to;

      for (let i = 0; i < grades.length; i++) {
        from = grades[i];
        to = grades[i + 1];

        labels.push(
          '<i style="background:' +
            getColor(from + 1) +
            '"></i> ' +
            from +
            (to ? "&ndash;" + to : "+")
        );
      }

      div.innerHTML = labels.join("<br>");
      return div;
    };

    legend.addTo(map);
  }, [map]); //here add map https://stackoverflow.com/questions/66275764/react-leaflet-legend-with-mapcontainer
  return null;
};

export default Legend;
