import React from "react";
import axios from "axios";
import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import Typography from "@material-ui/core/Typography";
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { ReactComponent as InfoIcon } from "./../../assets/images/question-circle.svg";
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import BarChart from "./BarChart";
import Chip from '@material-ui/core/Chip';
import { transparentize, CHART_COLORS} from "./Utils";
import { DLE_LANGUAGES, MATRIX_ELG_DATA  } from "./../../config/constants";
import ProgressBar from "../CommonComponents/ProgressBar";
 
const truncated_number_of_items_to_show = 6;
const verticalBarOptions = {
   responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
      title: {
        display: true,
        //text: 'Chart.js Bar Chart'
      },
      zoom: {
        pan: {
          enabled: true,
          mode: 'x',
        },
        zoom: {
          wheel: {
            enabled: true,
          },
          pinch: {
            enabled: true
          },
          mode: 'x',
        }        
      }      
    },
    scales: {
      x: {
        ticks: {
          autoSkip: false,          
          font :{size:9},
        }
      }
    },
    tooltips: {
    mode: 'index',
    filter: function(item, data) {
      var data2 = data.datasets[item.datasetIndex].data[item.index];
      return !isNaN(data2) && data2 !== null;
    }
  },
    
  };
 

export default class DLECharts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      languages: "",   chartData: [] , checkedLanguagesList:[] , isTechnologicalCheckAll:false, isContextualCheckAll:false, isLangSelectAll:false, isEULangSelectAll:false, isOtherLangSelectAll:false ,
      expandedLangEu: false, expandedLangOther: false,   open:false, Contextualopen:false, EUList:[], OtherList:[], loading: false
    }     
  }  

  componentDidMount() {
    this.getEULanguages();    
  }
    

  clearAllFilters() {
    this.setState({ chartData: [] , checkedLanguagesList:[] , isTechnologicalCheckAll:false, isContextualCheckAll:false , isLangSelectAll:false, isEULangSelectAll:false, isOtherLangSelectAll:false, EUList:[], OtherList:[],});     
  }

  clearAllData(type) {
    this.setState({ isTechnologicalCheckAll:false, isContextualCheckAll:false  });  
    let { checkedLanguagesList, EUList, OtherList, chartData} = this.state;
    if (type ==="eu") { //I am clearing only the eu data
      EUList.length>0 && EUList.map((lang,idx)=>{
        checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
        this.setState({ checkedLanguagesList });
        chartData = chartData.filter(item => item.id !== lang);
        this.setState({chartData});      
        return lang;
      })         
    }
    if (type ==="other") { //I am clearing only the eu data
      OtherList.length>0 && OtherList.map((lang,idx)=>{
        checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
        this.setState({ checkedLanguagesList });
        chartData = chartData.filter(item => item.id !== lang);
        this.setState({chartData});      
        return lang;
      })         
    }
       
  }

  getELEData(glottolog) {
    let { chartData } = this.state; 
    this.setState({ loading: true }); 
    axios.get(MATRIX_ELG_DATA(glottolog))
        .then(res => {           
          chartData.push(res.data); 
          this.setState({chartData});
          //console.log(this.state.elgData);
        }).catch(err => { this.setState({  loading: false }); });
     
  }
  

  getELEData2() {
    let { chartData, checkedLanguagesList } = this.state; 
    this.setState({ loading: true });
    checkedLanguagesList.length > 0 && checkedLanguagesList.map((glottolog,idx)=>{

      axios.get(MATRIX_ELG_DATA(glottolog))
      .then(res => {
        chartData.push(res.data);
        this.setState({ chartData });
        //console.log(this.state.elgData);
      }).catch(err => { this.setState({ loading: false }); });
      return glottolog;
    })
    
  }    

  addLangChecked2List = (lang, type) => {
    const { checkedLanguagesList, EUList, OtherList} = this.state;
    if (checkedLanguagesList.indexOf(lang) === -1) {//checks if i have allready selected the facet, if not add it to array
      checkedLanguagesList.push(lang);
      this.setState({ checkedLanguagesList }); 
      this.getELEData(lang) ; 
      if (type && type === "eu") { EUList.push(lang); this.setState({ EUList });  }
      if (type && type === "other") { OtherList.push(lang); this.setState({ OtherList }); }
    }    
  }

  removeLangFromCheckedList = (lang) => {     
    if(this.state.isLangSelectAll){
      this.setState({isLangSelectAll:false});
    }
    let { checkedLanguagesList, chartData } = this.state;
    checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
    this.setState({ checkedLanguagesList });
    chartData = chartData.filter(item => item.id !== lang);
    this.setState({chartData});         
  }

  sortByScore (dynamicRadarChart) { 
    let tmpData = {...dynamicRadarChart};
    const labels = tmpData.labels;
    const data1= tmpData.datasets[0].data;
    // Construct and array of objects that have 'label' and 'data'
    // We expect that labels and data1 are the same length but if there is
    // a chance that they are not, you should check for that.
    const allData = [];
    for (let i = 0; i < labels.length; ++i) {
      allData.push({
        label: labels[i],
        data: data1[i]
      });
    }

    // Sort them by the data value
    allData.sort((a, b) => b.data - a.data );
    // And split them again
    const sortedLabels = allData.map(e => e.label);
    const sortedData = allData.map(e => e.data);
    tmpData.labels= sortedLabels;
    tmpData.datasets[0].data = sortedData;
    return tmpData;
  }

  MyResourcetoChartJSData() {
    let {chartData} = this.state;   
    let dynamicRadarChart = {
      labels: [], //types (resource types),
      datasets: [], //per language
    };  

    this.state.checkedLanguagesList && this.state.checkedLanguagesList.length > 0 && this.state.checkedLanguagesList.map((language, index) => {
      let filterLanguage = this.state.languages.filter(item => item.glottocode === language) || "";        
      let langName = [filterLanguage[0].preferred_name]; 
      dynamicRadarChart.labels.push(langName);     
      return language; 
    })
         
    if (this.state.isTechnologicalCheckAll === true) {   
      let datasetBaseItem = {
        fill: true,
        label: "Technological factors",
        borderColor: CHART_COLORS.highlightTeal, //select one color 
        backgroundColor: transparentize(CHART_COLORS.highlightTealDark, 0.5),
        pointBackgroundColor: "rgba(34, 202, 236, 1)",
        poingBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        data: []
      }
      dynamicRadarChart.labels.length > 0 && dynamicRadarChart.labels.map((label, index) => {
        let filterLanguageName = this.state.languages.filter(item => item.preferred_name === label[0]) || "";
        let LabelGlottolog = filterLanguageName[0].glottocode;
        let TmpchartData = chartData.filter(item => item.id === LabelGlottolog);         
        TmpchartData &&TmpchartData.length>0 && datasetBaseItem.data.push(TmpchartData[0].dle_score_total) //dle_score_normalized_sigmoid)
        return label;
      })   
      dynamicRadarChart.datasets.push(datasetBaseItem);
    }

    if (this.state.isContextualCheckAll === true) {     
      let datasetBaseItem = {
        fill: true,
        label: "Contextual factors",
        borderColor: CHART_COLORS.highlightorangedim, //select one color 
        backgroundColor: transparentize(CHART_COLORS.elgYellow, 0.2),
        pointBackgroundColor: "rgba(34, 202, 236, 1)",
        poingBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        data: []
      }
      dynamicRadarChart.labels.length > 0 && dynamicRadarChart.labels.map((label, index) => {
        let filterLanguageName = this.state.languages.filter(item => item.preferred_name === label[0]) || "";
        let LabelGlottolog = filterLanguageName[0].glottocode;
        let TmpchartData = chartData.filter(item => item.id === LabelGlottolog);         
        TmpchartData &&TmpchartData.length>0 && datasetBaseItem.data.push(TmpchartData[0].dle_contextual_score);
        return label;
      })       
      dynamicRadarChart.datasets.push(datasetBaseItem);
    }

    if (dynamicRadarChart.datasets.length>0){
      let tmpchart = this.sortByScore(dynamicRadarChart);
      dynamicRadarChart = {...tmpchart};
      //console.log(tmpchart)
    }

    return dynamicRadarChart;
  }
  
  handleLangCheck = (e) => {
    if (e.target.checked) {
      this.addLangChecked2List(e.target.name);
    } else {
      this.removeLangFromCheckedList(e.target.name);
    }
  }

  handleTechnologicalCheck = (e) => {   
    this.setState({ isTechnologicalCheckAll: !this.state.isTechnologicalCheckAll });
    if (e.target.checked) {     
      this.setState({ isContextualCheckAll: false });
    }
  } 

  handleContextualCheck = (e) => {
    this.setState({ isContextualCheckAll: !this.state.isContextualCheckAll });
    if (e.target.checked) {    
      this.setState({ isTechnologicalCheckAll: false });   
    }
  } 

  addAllCheckedLang2List = () => {
    this.setState({ isLangSelectAll: !this.state.isLangSelectAll });
    let items = this.state.languages;
    (items.map(item => this.addLangChecked2List(item.glottocode)));

  }

  removeAllCheckedLangFromList = () => {
    this.setState({ isLangSelectAll: false });
    this.clearAllFilters();    
  }

  handleLangCheckAll = (e) => {
    if (e.target.checked) {
        this.removeAllCheckedLangFromList();
        this.setState({chartData: [] }); //clear all
        this.addAllCheckedLang2List();
    } else {
        this.removeAllCheckedLangFromList();
    }
  }

  addAllEUCheckedLang2List = () => {
    this.setState({ isEULangSelectAll: !this.state.isEULangSelectAll });
    let items = this.state.languages;
    (items.map(item => item.eu === true && this.addLangChecked2List(item.glottocode, "eu")));   
  }

  removeAllEUCheckedLangFromList = () => {
    this.setState({ isEULangSelectAll: false });
    this.clearAllData("eu");
  }

  handleEULangCheckAll = (e) => {    
    if (e.target.checked) {        
        this.removeAllEUCheckedLangFromList();       
        this.addAllEUCheckedLang2List();
    } else {
        this.removeAllEUCheckedLangFromList();
    }
  }

  addAllOtherCheckedLang2List = () => {
    this.setState({ isOtherLangSelectAll: !this.state.isOtherLangSelectAll });
    let items = this.state.languages;
    (items.map(item => item.eu === false && this.addLangChecked2List(item.glottocode, "other")));
  }

  removeAllOtherCheckedLangFromList = () => {
    this.setState({ isOtherLangSelectAll: false });
    this.clearAllData("other");
  }

  handleOtherLangCheckAll = (e) => {      
    if (e.target.checked) {      
        this.removeAllOtherCheckedLangFromList();        
        this.addAllOtherCheckedLang2List();
    } else {
        this.removeAllOtherCheckedLangFromList();
    }
  }

  getEULanguages() {
    this.setState({ loading: true });
      axios.get(DLE_LANGUAGES)
        .then(res => {
          this.setState({ languages: res.data.results });             
        }).catch(err => { this.setState({ loading: false }); });    
  }

  togglexpandedLangEu = () => {
    this.setState({ expandedLangEu: !this.state.expandedLangEu })
  }

  togglexpandedLangOther = () => {
    this.setState({ expandedLangOther: !this.state.expandedLangOther })
  }

handleClickOpen = () => {
    this.setState({open:true})
  };

handleClose = () => {
    this.setState({open:false})
  }

  handleContextualClickOpen = () => {
    this.setState({Contextualopen:true})
  };

handleContextualClose = () => {
    this.setState({Contextualopen:false})
  }
  
     

  render() {
    const { expandedLangEu, expandedLangOther} = this.state; 
    const dynamicRadarChart = this.MyResourcetoChartJSData();
    //console.log(displayData)
    //console.log(this.state.languages)
    //console.log(chartData, dynamicRadarChart)
    const spinner_visibility = this.state.isLangSelectAll && this.state.languages.length !== this.state.chartData.length;
    return <Grid container className="chart-container" spacing={1} direction="row">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters">
          <Grid container direction="column">
          <div className="bg-white pt-05 pb-05">
            <FormControlLabel className="pl05"
              control={<Checkbox
                checked={this.state.isLangSelectAll}
                color="primary"
                onChange={e => { this.handleLangCheckAll(e) }}
                name={"select all languages"} />}
              label={"Select all"}
              disabled= {this.state.isEULangSelectAll && this.state.isOtherLangSelectAll}
            />
            </div>
            <Grid item xs={12} sm={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>            
            <FormControl component="fieldset" className="pl05">
            <div className="italic pt-05 pb-05">
                <FormControlLabel 
                  control={<Checkbox 
                    checked={this.state.isEULangSelectAll}
                    color="primary"
                    onChange={e => { this.handleEULangCheckAll(e) }}
                    name={"select all eu languages"} />}
                  label={"Select all EU"}
                />
              </div>
            <FormGroup>
                {expandedLangEu? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>  
            <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other European languages</Typography></Grid>
            <FormControl component="fieldset" className="pl05">
            <div className="italic pt-05 pb-05">
                <FormControlLabel 
                  control={<Checkbox 
                    checked={this.state.isOtherLangSelectAll}
                    color="primary"
                    onChange={e => { this.handleOtherLangCheckAll(e) }}
                    name={"select all other languages"} />}
                  label={"Select all other"}
                />
              </div>
              <FormGroup>
                {expandedLangOther ? (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                  </div>
                ) : (
                  <div>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                      return <Grid item key={index}>
                        <FormControlLabel
                          control={<Checkbox
                            checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                            onChange={e => { this.handleLangCheck(e) }}
                            name={language.glottocode} />}
                          label={language.preferred_name}
                        />
                      </Grid>
                    })}
                    {this.state.languages.length > truncated_number_of_items_to_show ?
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                      : void 0}
                  </div>
                )
                }
              </FormGroup>
            </FormControl>          
          </Grid>          
        </Paper>
      </Grid>
      <Grid item sm={10}>
        <Grid container spacing={2} direction="column">
          <Grid item>
            <Paper elevation={3} variant="outlined" style={{ padding: "10px" }} className="selection-panel">
              <Grid container direction="row" style={{flexWrap:"nowrap", gap:"1em"}}>
                <Grid item container  sm={6} direction="row" alignItems="center"  className="bg-light-grey rounded">
                  <Grid item sm={11}>
                    <FormControlLabel className="pl05"
                      control={<Checkbox
                        checked={this.state.isTechnologicalCheckAll}                       
                        color="primary"
                        onChange={e => { this.handleTechnologicalCheck(e) }}
                        name={"datasets"} />}
                      label={"Technological factors"}                      
                    />                    
                  </Grid>
                  <Grid item  sm={1}><InfoIcon style={{cursor: "pointer"}} className="small-icon purple--font" aria-hidden="true" onClick={this.handleClickOpen} /></Grid>
                  <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="lg" fullWidth={true}>
                  <DialogTitle id="simple-dialog-title">About technological factors</DialogTitle>
                  <DialogContent>
                    <DialogContentText component={"span"} id="alert-dialog-description">
                    <Typography variant="body1"> The score of the technological DLE factors per language is automatically calculated based on the Language Resources and Technologies (LRTs) catalogued in the European Language Grid. 
                    The LRTs include: (i) tools and services, (ii) corpora, (iii) language models and computational grammars (collectively referred to as language descriptions), (iv) lexical and conceptual resources. 
                    The mechanism for calculating the score is based on the weighted frequencies of LRTs types and features (e.g. whether they are annotated or not, domain-specific or general, free to use or proprietary and many more). 
                    The proposed weights are specified in <a href="https://european-language-equality.eu/wp-content/uploads/2022/04/ELE___Deliverable_D1_3.pdf" target="_blank" rel="noopener noreferrer">ELE Deliverable D1.3</a>, Annex A.
                    According to this document, the technological DLE score per language is calculated as follows:  </Typography>
                    <ol>
                      <li>
                      <p>Each LR (dataset or tool) in the ELG Catalogue obtains a score (Score<sub>LR</sub>), which is equal to the sum of the weights of its relevant features. Specifically for features "Annotation Type" and "Domain", 
                      instead of simply adding the respective weight, the weight is multiplied by the number of unique feature values possessed by the LR in question.  </p>
                      <p> <span className="bold">Example:</span>Suppose an LR in the ELG catalogue (LR1) has the following features: corpus, annotated, monolingual, with three different annotation types (morphology, syntax, semantics), with text as media type, 
                      covering one domain (e.g. finance), with conditions of use "research use allowed". 
                      Then, using the weights proposed in <a href="https://european-language-equality.eu/wp-content/uploads/2022/04/ELE___Deliverable_D1_3.pdf" target="_blank" rel="noopener noreferrer">ELE Deliverable D1.3</a>, LR1 is assigned the following score: </p> 

                      <p>Score<sub>LR1</sub>= 5 + 1 + 2.5 + (3*0.25) + 1 + (1* 0.3) + 3.5 = 14.05  </p>

                      </li>
                      <li>
                      <p>To compute the technological DLE score for language X (TechDLE<sub>LangX</sub>), for all LRs that support language X (LR1, LR2, ... LRN), one sums up the Score<sub>LR</sub> of all LRs that support language X (LR1, LR2, ... LRN), i.e.</p> 

                      <p><span className="bold">TechDLE<sub>LangX</sub> = Score<sub>LR1</sub> + Score<sub>LR2</sub> + ... + Score<sub>LRN</sub> </span></p>  
                      </li>
                    </ol>
                     
                    </DialogContentText>
                </DialogContent>
                  </Dialog> 

                </Grid>
                <Grid item container sm={6} direction="row" alignItems="center"  className="bg-light-grey rounded">
                  <Grid item sm={11} >
                    <FormControlLabel className="pl05"
                      control={<Checkbox
                        checked={this.state.isContextualCheckAll}                        
                        color="primary"
                        onChange={e => { this.handleContextualCheck(e) }}
                        name={"software"} />}
                      label={"Contextual factors"}                      
                    />
                  </Grid> 
                  <Grid item sm={1} ><InfoIcon style={{cursor: "pointer"}} className="small-icon purple--font" aria-hidden="true" onClick={this.handleContextualClickOpen}/> </Grid> 
                  <Dialog open={this.state.Contextualopen} onClose={this.handleContextualClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="lg" fullWidth={true}>
                  <DialogTitle id="simple-dialog-title">About contextual factors</DialogTitle>
                  <DialogContent>
                    <DialogContentText component={"span"} id="alert-dialog-description">
                    <Typography variant="body1"> The score of the contextual DLE factors per language has been calculated offline based on a set of situational indicators that are not strictly language-related, but rather have to do with socio-economic aspects, i.e., 
                    the general situation of the broader context of a language: economy, education, policy, society, industry, funding and investments etc. of the European countries in which the languages under investigation are spoken.   
                    The first attempt of the calculation and the factors taken into account is provided in <a href="https://european-language-equality.eu/wp-content/uploads/2022/04/ELE___Deliverable_D1_3.pdf" target="_blank" rel="noopener noreferrer">ELE Deliverable D1.3</a>. 
                    The scores presented in this dashboard are based on the metric described in the deliverable and have been developed further. Part of this improvement was the inclusion of the indicators "vitality state" and "competition with English as another official national language" 
                    and the replacement of the indicator "EU membership" by "European Economic Area membership". 
                    Additionally, the data preparation was adapted including the size of the language communities in different countries for the calculation of an overall number for the language. </Typography>
                    </DialogContentText>
                </DialogContent>
                  </Dialog> 

                </Grid>

                {this.state.chartData && this.state.chartData.length > 0 && <Grid item sm={1}>
                <Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip>
              </Grid>}
              </Grid>   
            </Paper>
          </Grid>         
          {spinner_visibility && <Grid item sm={12}><ProgressBar /></Grid>}
          {(dynamicRadarChart.datasets && dynamicRadarChart.datasets.length > 0) && !spinner_visibility && <Grid item sm={12}><BarChart BarData={dynamicRadarChart} options={verticalBarOptions}/></Grid>  }
        </Grid>
      </Grid>
    </Grid>
  }
}