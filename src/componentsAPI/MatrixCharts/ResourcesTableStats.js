import React from "react";
import axios from "axios";
//import _ from 'lodash';
import Chip from '@material-ui/core/Chip';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Checkbox from '@material-ui/core/Checkbox';
import ProgressBar from "./../CommonComponents/ProgressBar";
import { DLE_LANGUAGES, DLE_COUNTS_ALL } from "./../../config/constants";
import { Functions, ResourceTypes, downloadBlob } from "./Utils";
import { ResponsiveHeatMap } from '@nivo/heatmap'
import { DataGrid, GridToolbar } from '@material-ui/data-grid';

const truncated_number_of_items_to_show = 6;


export default class ResourcesTableStats extends React.Component {
  constructor(state) {
    super(state);
    this.state = {
      languages: "", checkedLanguagesList: [],
      chartData: [], percentagechartData: [], value: "heatmap", typevalue: "counts", loading: false,
      expandedLangEu: false, expandedLangOther: false, FunctionArrayColumns: [], rows: [], percentagerows: [], allDLEData: [], allDLETotal: [],
      isEULangSelectAll: false, isOtherLangSelectAll: false, isLangSelectAll: false,
    }
    this.svgRef = React.createRef();
  }

  componentDidMount() { this.getEULanguages(); this.createFunctionColumns(); this.fetchAllData(); }
  togglexpandedLangEu = () => { this.setState({ expandedLangEu: !this.state.expandedLangEu }) }
  togglexpandedLangOther = () => { this.setState({ expandedLangOther: !this.state.expandedLangOther }) }

  fetchAllData() {
    this.setState({ loading: true });
    axios.get(DLE_COUNTS_ALL)
      .then(res => {
        this.setState({ allDLEData: res.data, allDLETotal: res.data.total, loading: false });
        let keys = Object.keys(res.data);
        keys && keys.map((glottocode) => {
          if (glottocode !== "total") {
            let filterLanguage = this.state.languages.filter(item => item.glottocode === glottocode) || "";
            let langName = filterLanguage[0].preferred_name;
            this.fetchDataForDataGrid(langName, glottocode);
            this.fetchDataForHeatmap(langName, glottocode);
            this.fetchPercentageDataForDataGrid(langName, glottocode);
            this.fetchPercentageDataForHeatmap(langName, glottocode);
          }
          return glottocode;
        })
      }).catch(err => { this.setState({ loading: false }); });

  }

  getEULanguages() {
    this.setState({ loading: true });
    axios.get(DLE_LANGUAGES)
      .then(res => {
        this.setState({ languages: res.data.results });
      }).catch(err => { this.setState({ loading: false }); });

  }

  createFunctionColumns() {
    let { FunctionArrayColumns } = this.state;
    let columnItem0 = {
      field: 'id',
      headerName: 'ID',
      //width: 50,
      hide: true,
    };
    FunctionArrayColumns.push(columnItem0);
    let columnItem = {
      field: "language",
      headerName: "Language",
      flex: 1,
      resizable: false,
      headerClassName: 'dle-app-theme--header',
    };
    FunctionArrayColumns.push(columnItem);

    Functions && Functions.length > 0 && Functions.map((value) => {
      let columnItem = {
        field: "",
        headerName: "",
        flex: 1,
        resizable: false,
        headerClassName: 'dle-app-theme--header',
      };

      columnItem.field = value.replace(/\s/g, '').toLowerCase(); //To convert Lower Case;
      //columnItem.headerName = value.replace("Collection", "");
      let tempvalue = value.replace("Collection", "").replace("Other", "Other functions of software");
      columnItem.headerName = tempvalue.includes("Other") ? "Other functions of software" : tempvalue;
      FunctionArrayColumns.push(columnItem);
      return value;
    })

    ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map((value) => {
      let columnItem = {
        field: "",
        headerName: "",
        flex: 1,
        resizable: false,
        headerClassName: 'dle-app-theme--header',
      };
      columnItem.field = value.replace(/\s/g, '').toLowerCase(); //To convert Lower Case; 
      let tmp = value.replace("Collection", "");
      columnItem.headerName = tmp.replace("/", " / "); //columnItem.headerName = value.replace("Collection", ""); 
      FunctionArrayColumns.push(columnItem);
      return value;
    })
    this.setState({ FunctionArrayColumns });
  }

  adddatasetBaseItem(datasetBaseItem) {
    this.setState({ chartData: [...this.state.chartData, datasetBaseItem] });
  }

  addPercentagedatasetBaseItem(datasetBaseItem) {
    this.setState({ percentagechartData: [...this.state.percentagechartData, datasetBaseItem] });
  }

  addRowItem(row) {
    this.setState({ rows: [...this.state.rows, row] });
  }

  addPercentageRowItem(row) {
    this.setState({ percentagerows: [...this.state.percentagerows, row] });
  }

  fetchPercentageDataForDataGrid = (langName, glottocode) => {
    let { allDLEData, allDLETotal } = this.state;
    let filter_res = allDLEData[glottocode];
    let myobj = {};
    myobj["language"] = langName;
    myobj["id"] = this.state.rows.length + 1;
    Functions && Functions.length > 0 && Functions.map(value => {
      let property = value.replace(/\s/g, '').toLowerCase().replace("Collection", "").replace("Other", "Other functions of software");
      myobj[property] = ((Number(filter_res[value]) / Number(allDLETotal[value])) * 100).toFixed(2) + '%';
      return value;
    })
    ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map(value => {
      let property = value.replace(/\s/g, '').toLowerCase()
      myobj[property] = ((Number(filter_res[value]) / Number(allDLETotal[value])) * 100).toFixed(2) + '%';
      return value;
    })
    this.addPercentageRowItem(myobj)
  }

  fetchPercentageDataForHeatmap = (langName, glottocode) => {
    let { allDLEData, allDLETotal } = this.state;
    let filter_res = allDLEData[glottocode]
    let datasetBaseItem = { id: langName, data: [] };
    Functions && Functions.length > 0 && Functions.map(value => {
      let myobj = { "x": "", "y": "" }
      myobj.x = value.replace("Collection", "").replace("Other", "Other functions of software");
      myobj.y = ((Number(filter_res[value]) / Number(allDLETotal[value])) * 100).toFixed(2);//+'%';
      datasetBaseItem.data.push(JSON.parse(JSON.stringify(myobj)));
      return value;
    })
    ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map(value => {
      let myobj = { "x": "", "y": "" }
      myobj.x = value
      myobj.y = ((Number(filter_res[value]) / Number(allDLETotal[value])) * 100).toFixed(2);//+'%';
      datasetBaseItem.data.push(JSON.parse(JSON.stringify(myobj)));
      return value;
    })
    this.addPercentagedatasetBaseItem(datasetBaseItem);
  }

  fetchDataForDataGrid = (langName, glottocode) => {
    let { allDLEData } = this.state;
    let filter_res = allDLEData[glottocode];
    let myobj = {};
    myobj["language"] = langName;
    myobj["id"] = this.state.rows.length + 1;
    Functions && Functions.length > 0 && Functions.map(value => {
      let property = value.replace(/\s/g, '').toLowerCase().replace("Collection", '').replace("Other", "Other functions of software");
      myobj[property] = Number(filter_res[value]);
      return value;
    })
    ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map(value => {
      let property = value.replace(/\s/g, '').toLowerCase()
      myobj[property] = Number(filter_res[value]); //convert string to number     
      return value;
    })
    this.addRowItem(myobj)
  }

  fetchDataForHeatmap = (langName, glottocode) => {
    let { allDLEData } = this.state;
    let filter_res = allDLEData[glottocode]
    let datasetBaseItem = { id: langName, data: [] };
    Functions && Functions.length > 0 && Functions.map(value => {
      let myobj = { "x": "", "y": "" }
      myobj.x = value.replace("Collection", '').replace("Other", "Other functions of software");
      myobj.y = Number(filter_res[value]);
      datasetBaseItem.data.push(JSON.parse(JSON.stringify(myobj)));
      return value;
    })
    ResourceTypes && ResourceTypes.length > 0 && ResourceTypes.map(value => {
      let myobj = { "x": "", "y": "" }
      myobj.x = value
      myobj.y = Number(filter_res[value]);
      datasetBaseItem.data.push(JSON.parse(JSON.stringify(myobj)));
      return value;
    })
    this.adddatasetBaseItem(datasetBaseItem);
  }

  addLangChecked2List = (glottocode) => {
    const { checkedLanguagesList } = this.state;
    //let filterLanguage = this.state.languages.filter(item => item.glottocode === glottocode) || "";    
    //let langName = filterLanguage[0].preferred_name;
    if (checkedLanguagesList.indexOf(glottocode) === -1) {//checks if i have allready selected the facet, if not add it to array
      checkedLanguagesList.push(glottocode);
      this.setState({ checkedLanguagesList })//, this.fetchDataForHeatmap(langName, glottocode), this.fetchDataForDataGrid(langName, glottocode));
    }
  }

  removeLangFromCheckedList = (lang) => {
    let { checkedLanguagesList } = this.state;
    checkedLanguagesList = checkedLanguagesList.filter(item => item !== lang);
    this.setState({ checkedLanguagesList });
  }

  handleLangCheck = (e) => {
    if (e.target.checked) {
      this.addLangChecked2List(e.target.name);
    } else {
      this.removeLangFromCheckedList(e.target.name);
    }
  }

  addAllCheckedLang2List = () => {
    this.setState({ isLangSelectAll: !this.state.isLangSelectAll });
    this.addAllEUCheckedLang2List("eu");
    this.addAllEUCheckedLang2List("other");
  }

  removeAllCheckedLangFromList = () => {
    this.setState({ isLangSelectAll: false });
    this.clearAllFilters();
  }


  handleLangCheckAll = (e) => {
    if (e.target.checked) {
      //this.removeAllCheckedLangFromList();
      //this.setState({ chartData: [] }); //clear all
      this.addAllCheckedLang2List();
    } else {
      this.removeAllCheckedLangFromList();
    }
  }


  removeLanguages = (glottologs) => {
    let { checkedLanguagesList } = this.state;
    checkedLanguagesList = this.state.checkedLanguagesList.filter(item => !glottologs.includes(item));
    this.setState({ checkedLanguagesList });
  }

  addLanguages(glottologs) {
    let { checkedLanguagesList } = this.state;
    glottologs.map((glottocode) => {
      if (checkedLanguagesList.indexOf(glottocode) === -1) {//checks if i have allready selected the facet, if not add it to array
        checkedLanguagesList.push(glottocode);
        this.setState({ checkedLanguagesList });
      }
      return glottocode;
    })
  }

  addAllEUCheckedLang2List = (type) => {
    let euLanguages = [];
    let items = this.state.languages;
    let euglottologs = [];
    if (type === "eu") {
      this.setState({ isEULangSelectAll: !this.state.isEULangSelectAll });
      euLanguages = items.filter(item => item.eu === true)
    }
    if (type === "other") {
      this.setState({ isOtherLangSelectAll: !this.state.isOtherLangSelectAll });
      euLanguages = items.filter(item => item.eu === false)
    }
    euLanguages.map((item) => {
      euglottologs.push(item.glottocode);
      return item;
    });
    this.addLanguages(euglottologs);
  }

  removeAllEUCheckedLangFromList = (type) => {
    let euglottologs = []; let items = this.state.languages;
    if (type === "eu") {
      this.setState({ isEULangSelectAll: false });
      euglottologs = items.filter(item => item.eu === true);
    }
    if (type === "other") {
      this.setState({ isOtherLangSelectAll: false });
      euglottologs = items.filter(item => item.eu === false);
    }
    euglottologs.map((item) => {
      euglottologs.push(item.glottocode);
      return item;
    });
    this.removeLanguages(euglottologs);
  }

  handleEULangCheckAll = (e) => {
    if (e.target.checked) {
      this.addAllEUCheckedLang2List("eu");//this.removeAllEUCheckedLangFromList();
    } else {
      this.removeAllEUCheckedLangFromList("eu");
    }
  }

  handleOtherLangCheckAll = (e) => {
    if (e.target.checked) {
      this.addAllEUCheckedLang2List("other");//this.addAllOtherCheckedLang2List(); this.removeAllOtherCheckedLangFromList();      
    } else {
      this.removeAllEUCheckedLangFromList("other");//this.removeAllOtherCheckedLangFromList();
    }
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value })
  }

  handleTypeChange = (event) => {
    this.setState({ typevalue: event.target.value })
  }

  clearAllFilters() {
    this.setState({ checkedLanguagesList: [], isEULangSelectAll: false, isOtherLangSelectAll: false, isLangSelectAll: false });
  }

  minmax = (myArray) => {
    let minArray = []; let maxArray = [];
    myArray.map((itemArray) => {
      let min = Math.min(...itemArray.data.map(item => item.y));
      let max = Math.max(...itemArray.data.map(item => item.y));
      minArray.push(min);
      maxArray.push(max);
      return itemArray;
    })
    //console.log(minArray, maxArray)
    return {
      min: Math.min(...minArray),
      max: Math.max(...maxArray)
    };
  };

  doSortingHeat = (myArray) => {
    const strAscending = [...myArray].sort((a, b) => a.id > b.id ? 1 : -1,);
    return strAscending;
  }

  doSortingTable = (myArray) => {
    const strAscending = [...myArray].sort((a, b) => a.language > b.language ? 1 : -1,);
    return strAscending;
  }

  createFinalCharts() {
    let { checkedLanguagesList, chartData, rows, percentagerows, percentagechartData } = this.state;
    let heatmapData = []; let tableData = []; let tableDataPercentage = []; let heatmapDataPercentage = [];
    checkedLanguagesList && checkedLanguagesList.length > 0 && checkedLanguagesList.map((language, index) => {
      let filterLanguage = this.state.languages.filter(item => item.glottocode === language) || "";
      let langName = filterLanguage[0].preferred_name;
      heatmapDataPercentage.push(percentagechartData.filter(item => item.id === langName)[0]);
      heatmapData.push(chartData.filter(item => item.id === langName)[0]);
      tableData.push(rows.filter(item => item.language === langName)[0]);
      tableDataPercentage.push(percentagerows.filter(item => item.language === langName)[0]);
      return language;
    })
    heatmapDataPercentage = this.doSortingHeat(heatmapDataPercentage);
    heatmapData = this.doSortingHeat(heatmapData);
    tableData = this.doSortingTable(tableData);
    tableDataPercentage = this.doSortingTable(tableDataPercentage);
    return { heatmapData, tableData, tableDataPercentage, heatmapDataPercentage }
  }

  chooseChartHeight() {
    let chart_height = 800;
    if (this.state.isEULangSelectAll && this.state.isOtherLangSelectAll) {
      chart_height = 1900;
    }

    if (this.state.isEULangSelectAll && !this.state.isOtherLangSelectAll) {
      chart_height = 1200;
    }

    if (!this.state.isEULangSelectAll && this.state.isOtherLangSelectAll) {
      chart_height = 1600;
    }

    return chart_height;
  }

  downloadSVG() {
    //console.log(this.svgRef.current);
    const svg = this.svgRef.current.innerHTML;
    const blob = new Blob([svg], { type: "image/svg+xml" });
    downloadBlob(blob, `myimage.svg`);

  }

  render() {
    const { expandedLangEu, expandedLangOther } = this.state;
    let { heatmapData, tableData, tableDataPercentage, heatmapDataPercentage } = this.createFinalCharts();
    let { min, max } = this.minmax(heatmapData);
    let { minPercentage, maxPercentage } = this.minmax(heatmapDataPercentage);
    let chart_height = this.chooseChartHeight(); // (this.state.isEULangSelectAll || this.state.isOtherLangSelectAll) ? 1400 : 800;

    return <Grid container className="chart-container" spacing={1} direction="row">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters">
          {this.state.loading ?
            <ProgressBar />
            : <Grid container direction="column">

              <div className="bg-white pt-05 pb-05"><FormControlLabel className="pl05"
                control={<Checkbox
                  checked={this.state.isLangSelectAll}
                  color="primary"
                  onChange={e => { this.handleLangCheckAll(e) }}
                  name={"select all languages"} />}
                label={"Select all"}
              //disabled={this.state.isEULangSelectAll && this.state.isOtherLangSelectAll}
              /> </div>

              <Grid item xs={12} sm={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>
              <FormControl component="fieldset" className="pl05">
                <div className="italic pt-05 pb-05">
                  <FormControlLabel
                    control={<Checkbox
                      checked={this.state.isEULangSelectAll}
                      color="primary"
                      onChange={e => { this.handleEULangCheckAll(e) }}
                      name={"select all eu languages"} />}
                    label={"Select all EU"}
                  />
                </div>
                <FormGroup>
                  {expandedLangEu ? (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                    </div>
                  ) : (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      {this.state.languages.length > truncated_number_of_items_to_show ?
                        <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                        : void 0}
                    </div>
                  )
                  }
                </FormGroup>
              </FormControl>
              <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other European languages</Typography></Grid>
              <FormControl component="fieldset" className="pl05">
                <div className="italic pt-05 pb-05">
                  <FormControlLabel
                    control={<Checkbox
                      checked={this.state.isOtherLangSelectAll}
                      color="primary"
                      onChange={e => { this.handleOtherLangCheckAll(e) }}
                      name={"select all other languages"} />}
                    label={"Select all other"}
                  />
                </div>
                <FormGroup>
                  {expandedLangOther ? (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                    </div>
                  ) : (
                    <div>
                      {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) => {
                        return <Grid item key={index}>
                          <FormControlLabel
                            control={<Checkbox
                              checked={(this.state.checkedLanguagesList.find(item => item === language.glottocode)) ? true : false}
                              onChange={e => { this.handleLangCheck(e) }}
                              name={language.glottocode} />}
                            label={language.preferred_name}
                          />
                        </Grid>
                      })}
                      {this.state.languages.length > truncated_number_of_items_to_show ?
                        <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                        : void 0}
                    </div>
                  )
                  }
                </FormGroup>
              </FormControl>
            </Grid>}
        </Paper>
      </Grid>
      <Grid item sm={10}>
        <Grid container spacing={2} direction="column">
          <Grid item>
            <Paper elevation={3} variant="outlined" style={{ padding: "10px" }} className="selection-panel">
              <Grid container direction="row" style={{ flexWrap: "wrap", gap: "1em", alignItems: "center" }}>
                <Grid item container sm={12} className="bg-light-grey rounded p-1" justify-content="flex-start">
                  <Grid item container sm={12} justify-content="flex-start">
                    <Grid item sm={4}>
                      <FormControl component="fieldset"> <FormLabel component="legend">Select type of chart</FormLabel>
                        <RadioGroup defaultValue="heatmap" aria-label="chart_type" name="chart_type" value={this.state.value} onChange={this.handleChange}>
                          <FormControlLabel value="heatmap" control={<Radio />} label="Heatmap" />
                          <FormControlLabel value="table" control={<Radio />} label="Table" />
                        </RadioGroup>
                      </FormControl>
                    </Grid>

                    <Grid item sm={5}>
                      <FormControl component="fieldset"> <FormLabel component="legend">Select type of measurement</FormLabel>
                        <RadioGroup defaultValue="counts" aria-label="chart_metric" name="chart_metric" typevalue={this.state.typevalue} onChange={this.handleTypeChange}>
                          <FormControlLabel value="counts" control={<Radio />} label="Number of resources (Datasets and Software)" />
                          <FormControlLabel value="percentage" control={<Radio />} label="Percentage (of contribution per resource type)" />
                        </RadioGroup>
                      </FormControl>
                    </Grid>

                    {tableData && tableData.length > 0 &&
                      <Grid item container spacing={1} sm={3} justifyContent="flex-end" alignItems="center">
                        <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip></Grid>
                        {this.state.value === "heatmap" && <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Export as SVG"} onClick={() => this.downloadSVG()} style={{ float: "right" }}></Chip></Grid>}
                      </Grid>}
                  </Grid>

                </Grid>
                {this.state.value === "table" && tableData && tableData.length > 0 && <Grid item sm={12} className="mt2" > <div style={{ width: '100%' }}>
                  <DataGrid
                    className="datagridTable"
                    rows={this.state.typevalue === "counts" ? tableData : tableDataPercentage}
                    columns={this.state.FunctionArrayColumns}
                    checkboxSelection={false}
                    disableSelectionOnClick
                    autoHeight
                    //hideFooter
                    key={Math.random()}
                    components={{
                      Toolbar: GridToolbar,
                    }}
                    //disableColumnSelector={true}
                    disableColumnMenu={true}
                    disableColumnFilter
                    rowHeight={60}
                    headerHeight={90}
                  />
                </div> </Grid>}
                {this.state.value === "heatmap" && heatmapData && heatmapData.length > 0 && <Grid item sm={12} style={{ height: "100%", width: "100%", overflow: "hidden" }} className="heatmap">
                  <div style={{ height: chart_height }} ref={this.svgRef}>
                    <ResponsiveHeatMap
                      data={this.state.typevalue === "counts" ? heatmapData : heatmapDataPercentage}
                      margin={{ top: 160, right: 15, bottom: 60, left: 90 }}
                      theme={{
                        fontSize: 13,
                        axis: { legend: { text: { fontSize: 14, fontWeight: 600, fill: "#212932" } } }
                      }}
                      axisTop={{
                        format: (v) => {
                          return v.length > 30 ? (
                            <tspan>
                              {v.substring(0, 30) + "..."}
                              <title>{v}</title>
                            </tspan>
                          ) : (
                            v
                          );
                        },
                        tickSize: 0,
                        tickPadding: 5,
                        tickRotation: 45,
                        legend: '',
                        legendOffset: 46
                      }}
                      /*axisRight={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: 'Language',
                        legendPosition: 'middle',
                        legendOffset: 70
                      }}*/
                      axisLeft={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: 'Language',
                        legendPosition: 'middle',
                        legendOffset: -79,

                      }}
                      colors={{
                        type: 'diverging',
                        scheme: 'yellow_orange_brown',
                        divergeAt: 0.5,
                        minValue: this.state.typevalue === "counts" ? min : minPercentage,
                        maxValue: this.state.typevalue === "counts" ? max : maxPercentage
                      }}
                      emptyColor="#555555"
                      labelTextColor="#636e7b"
                      legends={[
                        {
                          anchor: 'bottom',
                          translateX: 0,
                          translateY: 40,
                          length: 400,
                          thickness: 12,
                          direction: 'row',
                          tickPosition: 'after',
                          tickSize: 3,
                          tickSpacing: 4,
                          tickOverlap: false,
                          tickFormat: '>-.2s',
                          title: 'Value →',
                          titleAlign: 'start',
                          titleOffset: 4
                        }
                      ]}
                    /> </div>
                </Grid>}

                {tableData && tableData.length > 0 && <Grid item container spacing={1} sm={12} justifyContent="flex-end" alignItems="center">
                  <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Clear graph"} onClick={() => this.clearAllFilters()} style={{ float: "right" }}></Chip></Grid>
                  {this.state.value === "heatmap" && <Grid item><Chip size="medium" className="ChipTagYellow" variant="default" label={"Export as SVG"} onClick={() => this.downloadSVG()} style={{ float: "right" }}></Chip></Grid>}
                </Grid>}



              </Grid>


            </Paper>
          </Grid>

        </Grid>
      </Grid>
    </Grid>
  }
}