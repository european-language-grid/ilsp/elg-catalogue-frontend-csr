import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

export default class About extends React.Component {
  render() {
    return <><Container maxWidth="xl" className="matrix-overview">
      <Grid container spacing={1}>
        <Grid item xs={12}><Typography variant="h3">About European Language Equality / European Language Grid Dashboard</Typography></Grid>
        <Grid item xs={12}> 
          <Typography variant="body1" className="p-05 mb1"> This dashboard facilitates interactive explorations and visualisations of the Digital Language Equality metric and its components. </Typography> 
         
          <Typography variant="body1" className="p-05 bg-light-grey rounded mb1"><span className="bold">Digital Language Equality (DLE)</span> is the state of affairs in which all languages have the technological support and situational context necessary for them to continue to exist and to prosper as living languages in the digital age.</Typography>
           
           
          <Typography variant="body1" className="p-05 bg-light-grey rounded mb1"> The <span className="bold">DLE Metric</span> is a measure that reflects the digital readiness of a language and its contribution to the state of technology-enabled multilingualism, tracking its progress towards the goal of DLE. 
            The DLE Metric is computed for each language on the basis of various factors, grouped into <span className="bold">technological factors</span> and <span className="bold">contextual factors</span>. (See <a href="https://european-language-equality.eu/wp-content/uploads/2021/05/ELE_Deliverable_D1_1.pdf" target="_blank" rel="noopener noreferrer">ELE Deliverable D1.1</a> and <a href="https://european-language-equality.eu/wp-content/uploads/2022/03/ELE___Deliverable_D1_3.pdf" target="_blank" rel="noopener noreferrer">D1.3</a>.)</Typography>

            <Typography variant="body1">The dashboard has been developed as part of the European Language Grid Catalogue in the framework of the <a href="https://european-language-equality.eu/" target="_blank" rel="noopener noreferrer">European Language Equality project</a>.</Typography>
        
        </Grid>

        </Grid>




         
    </Container>
    </>
  }
}