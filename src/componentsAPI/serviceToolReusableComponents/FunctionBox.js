import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Languages from "../CommonComponents/Languages";
import Paper from '@material-ui/core/Paper';
import { ReactComponent as ProcessingIcon } from "./../../assets/elg-icons/desktop-monitor-settings.svg";
import { ReactComponent as InputIcon } from "./../../assets/elg-icons/monitor-add.svg";
import { ReactComponent as OutputIcon } from "./../../assets/elg-icons/desktop-monitor-approve.svg";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

//import InputIcon from '@material-ui/icons/Input';
//import ExitToAppIcon from '@material-ui/icons/ExitToApp';
//import SettingsIcon from '@material-ui/icons/Settings';
 

export default class FunctionBox extends React.Component {

    grid_size(data) {
        let grid_size = 4 ;         
        if (data.described_entity.field_value.lr_subclass.field_value.input_content_resource 
        && data.described_entity.field_value.lr_subclass.field_value.output_resource 
        && data.described_entity.field_value.lr_subclass.field_value.function) {
            grid_size = 4            
        }
       else {
                grid_size = 6                 
        }
        return grid_size;
    }

    render() {
        const { data, metadataLanguage, inferred_language} = this.props.state;
        if (!data.described_entity.field_value.lr_subclass) {
            return <div></div>
        }
        //console.log(data)
        return (
            <>
                <Grid container spacing={2} className="MetaDataDetailsFunctionContainer" justifyContent="center" >
                   <Grid item xs={12} sm={ this.grid_size(data) }>
                        <Paper className="FunctionBox input" style={{ height:'100%'}}>
                            <div className="FunctionBox--card back">

                                <div className="personal-info" >
                                    <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                                        <Grid item xs={2}>
                                            <InputIcon style={{ width: 25, height: 25, marginRight: "0.2em" }} />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <Typography variant="h3" className="FunctionBoxHeading "> {data.described_entity.field_value.lr_subclass.field_value.input_content_resource ? data.described_entity.field_value.lr_subclass.field_value.input_content_resource.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.input_content_resource.field_label["en"] : ""} </Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>
                            {data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.input_content_resource && data.described_entity.field_value.lr_subclass.field_value.input_content_resource.field_value.length > 0 &&
                                data.described_entity.field_value.lr_subclass.field_value.input_content_resource.field_value.map(function (inputResource, inputResourceIndex) {
                                    let sample_label = inputResource.sample ? inputResource.sample.field_label[metadataLanguage] || inputResource.sample.field_label["en"] : "Sample";
                                    let sampleArray = inputResource.sample ? inputResource.sample.field_value.map((sampleItem, sampleIndex) => {
                                        let samples_location = sampleItem.samples_location ? sampleItem.samples_location.field_value : "";
                                        let samples_location_label = sampleItem.samples_location ? (sampleItem.samples_location.field_label[metadataLanguage] || sampleItem.samples_location.field_label["en"]) : "Samples location";
                                        let tag_label = sampleItem.tag ? sampleItem.tag.field_label[metadataLanguage] || sampleItem.tag.field_label["en"] : "Tag";
                                        let tag_value = sampleItem.tag ? sampleItem.tag.field_value : "";
                                        let sample_text_label = sampleItem.sample_text ? sampleItem.sample_text.field_label[metadataLanguage] || sampleItem.sample_text.field_label["en"] : "Sample text";
                                        let sample_text = sampleItem.sample_text ? sampleItem.sample_text.field_value : "";
                                        return <div key={sampleIndex} className="padding5">
                                            {samples_location && <div className="padding5"> <span className="info_url"><LaunchIcon className="xsmall-icon" /> </span> <span><a href={samples_location} target="_blank" rel="noopener noreferrer">{samples_location_label}</a></span></div>}
                                            {sample_text && <div className="padding5"> <Typography className="bold-p--id">{sample_text_label}</Typography> <span className="info_value">{sample_text}</span></div>}
                                            {tag_value && <div className="padding5"> <Typography className="bold-p--id">{tag_label}</Typography> <span className="info_value">{tag_value}</span></div>}
                                        </div>
                                    }) : [];

                                    let media_type = inputResource.media_type ? (inputResource.media_type.label[metadataLanguage] || inputResource.media_type.label[Object.keys(inputResource.media_type.label)[0]]) : "";
                                    let media_type_label = inputResource.media_type ? (inputResource.media_type.field_label[metadataLanguage] || inputResource.media_type.field_label["en"]) : "Media type";
                                    let character_encodingArray = inputResource.character_encoding ? inputResource.character_encoding.field_value.map(encoding => (encoding.label[metadataLanguage] || encoding.label[Object.keys(encoding.label)[0]])) : [];
                                    let character_encoding_label = inputResource.character_encoding ? (inputResource.character_encoding.field_label[metadataLanguage] || inputResource.character_encoding.field_label["en"]) : "Character encoding";
                                    let annotation_typeArray = inputResource.annotation_type ? inputResource.annotation_type.field_value.map(annotationType => annotationType.label ? (annotationType.label[metadataLanguage] || annotationType.label[Object.keys(annotationType.label)[0]]) : annotationType.value) : [];
                                    let annotation_type_label = inputResource.annotation_type ? (inputResource.annotation_type.field_label[metadataLanguage] || inputResource.annotation_type.field_label["en"]) : "Annotation type";
                                    let segmentation_levelArray = inputResource.segmentation_level ? inputResource.segmentation_level.field_value.map(segmentation => (segmentation.label[metadataLanguage] || segmentation.label[Object.keys(segmentation.label)[0]])) : [];
                                    let segmentation_level_label = inputResource.segmentation_level ? (inputResource.segmentation_level.field_label[metadataLanguage] || inputResource.segmentation_level.field_label["en"]) : "Segmentation level";
                                    let typesystem = (inputResource.typesystem && inputResource.typesystem.field_value.resource_name) ? (inputResource.typesystem.field_value.resource_name.field_value[metadataLanguage] || inputResource.typesystem.field_value.resource_name.field_value[Object.keys(inputResource.typesystem.field_value.resource_name.field_value)[0]]) : "";
                                    let typesystem_full_metadata_record = (inputResource.typesystem ? commonParser.getFullMetadata(inputResource.typesystem.field_value.full_metadata_record) : "");
                                    let typesystem_label = inputResource.typesystem ? (inputResource.typesystem.field_label[metadataLanguage] || inputResource.typesystem.field_label["en"]) : "Typesystem";
                                    let typesystemVersion = (inputResource.typesystem && inputResource.typesystem.field_value.version) ? inputResource.typesystem.field_value.version.field_value : "";

                                    let annotation_schema = (inputResource.annotation_schema && inputResource.annotation_schema.field_value.resource_name) ? (inputResource.annotation_schema.field_value.resource_name.field_value[metadataLanguage] || inputResource.annotation_schema.field_value.resource_name.field_value[Object.keys(inputResource.annotation_schema.field_value.resource_name.field_value)[0]]) : "";
                                    let annotation_schema_label = inputResource.annotation_schema ? (inputResource.annotation_schema.field_label[metadataLanguage] || inputResource.annotation_schema.field_label["en"]) : "Annotation schema"
                                    let annotation_schema_version = (inputResource.annotation_schema && inputResource.annotation_schema.field_value.version) ? inputResource.annotation_schema.field_value.version.field_value : "";
                                    let annotation_schema_full_metadata_record = (inputResource.annotation_schema ? commonParser.getFullMetadata(inputResource.annotation_schema.field_value.full_metadata_record) : "");

                                    let annotation_resource = (inputResource.annotation_resource && inputResource.annotation_resource.field_value.resource_name) ? (inputResource.annotation_resource.field_value.resource_name.field_value[metadataLanguage] || inputResource.annotation_resource.field_value.resource_name.field_value[Object.keys(inputResource.annotation_resource.field_value.resource_name.field_value)[0]]) : "";
                                    let annotation_resource_label = inputResource.annotation_resource ? (inputResource.annotation_resource.field_label[metadataLanguage] || inputResource.annotation_resource.field_label["en"]) : "Annotation resource"
                                    let annotation_resource_version = (inputResource.annotation_resource && inputResource.annotation_resource.field_value.version) ? inputResource.annotation_resource.field_value.version.field_value : "";
                                    let annotation_resource_full_metadata_record = (inputResource.annotation_resource ? commonParser.getFullMetadata(inputResource.annotation_resource.field_value.full_metadata_record) : "");

                                    let modality_typeArray = inputResource.modality_type ? inputResource.modality_type.field_value.map(modality => (modality.label[metadataLanguage] || modality.label[Object.keys(modality.label)[0]])) : [];
                                    let modality_type_label = inputResource.modality_type ? (inputResource.modality_type.field_label[metadataLanguage] || inputResource.modality_type.field_label["en"]) : "Modality type";

                                    let modality_type_details = inputResource.modality_type_details ? (inputResource.modality_type_details.field_value[metadataLanguage] || inputResource.modality_type_details.field_value[Object.keys(inputResource.modality_type_details.field_value)[0]]) : "";
                                    let modality_type_details_label = inputResource.modality_type_details ? (inputResource.modality_type_details.field_label[metadataLanguage] || inputResource.modality_type_details.field_label["en"]) : "Modality type details";

                                     
                                    let processingResourceType = inputResource.processing_resource_type ? (inputResource.processing_resource_type.label[metadataLanguage] || inputResource.processing_resource_type.label[Object.keys(inputResource.processing_resource_type.label)[0]]) : undefined;
                                    let processingResourceType_label = inputResource.processing_resource_type ? (inputResource.processing_resource_type.field_label[metadataLanguage] || inputResource.processing_resource_type.field_label["en"]) : "Processing resource type"
                                    let dataFormatArray = inputResource.data_format ? inputResource.data_format.field_value.map(format => (format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) : [];
                                    let dataFormat_label = inputResource.data_format ? (inputResource.data_format.field_label[metadataLanguage] || inputResource.data_format.field_label["en"]) : "Data format";
                                    let mimetype_label = inputResource.mimetype ? (inputResource.mimetype.field_label[metadataLanguage] || inputResource.mimetype.field_label["en"]) : "";
                                    let mimetypeArray = (inputResource.mimetype && inputResource.mimetype.field_value.map(mimetype => mimetype)) || [];

                                    return (
                                        <div id={inputResourceIndex} key={inputResourceIndex} className="FunctionBox--inner">

                                            {/*languageInfoArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{language_label}</Typography><span className="info_value">{languageInfoArray.map((item, index) => <span key={index} className="border-bottom-2">{item}</span>)}</span></div>*/}
                                            {<Languages language={inputResource.language} from_tool={true} inferred_language={inferred_language}/>}
            
                                            {processingResourceType && <div className="padding5"><Typography className="bold-p--id">{processingResourceType_label}</Typography><Chip size="small" label={processingResourceType} className="ChipTagTeal" /></div>}

                                            {dataFormatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{dataFormat_label}</Typography><div className="flex wrap">{dataFormatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div></div>}
                                            {mimetypeArray && mimetypeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{mimetype_label} </Typography>{mimetypeArray.map((mimetype, mimetypeIndex) => <span key={mimetypeIndex} className="info-url" >{mimetype}</span>)}</div>}
                                            {(sampleArray || media_type || typesystem || annotation_schema || annotation_resource || character_encodingArray
                                                || annotation_typeArray || segmentation_levelArray || modality_typeArray || modality_type_details) &&
                                                <div>

                                                    {character_encodingArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{character_encoding_label}</Typography><div className="flex wrap">{character_encodingArray.map((encoding, encodingIndex) => <Chip key={encodingIndex} size="small" label={encoding} className="ChipTagTeal" />)}</div></div>}
                                                    {media_type && <div className="padding5"><Typography className="bold-p--id">{media_type_label} </Typography><Chip size="small" label={media_type} className="ChipTagTeal" /></div>}
                                                    {modality_typeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{modality_type_label}</Typography><div className="flex wrap">{modality_typeArray.map((modality, modalityIndex) => <Chip key={modalityIndex} size="small" label={modality} className="ChipTagTeal" />)}</div></div>}
                                                    {modality_type_details && <div className="padding5"><Typography className="bold-p--id">{modality_type_details_label}</Typography><span className="info_value"> {modality_type_details} </span> </div>}
                                                    {annotation_typeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotation_type_label}</Typography><div className="flex wrap">{annotation_typeArray.map((annotation, annotationIndex) => <Chip key={annotationIndex} size="small" label={annotation} className="ChipTagTeal" />)}</div></div>}
                                                    {segmentation_levelArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{segmentation_level_label}</Typography><div className="flex wrap">{segmentation_levelArray.map((segmentation, segementationIndex) => <Chip key={segementationIndex} size="small" label={segmentation} className="ChipTagTeal" />)}</div></div>}


                                                    {typesystem && <div className="padding5"><Typography className="bold-p--id">{typesystem_label}</Typography><span className="info_value">
                                                        {typesystem_full_metadata_record ?
                                                            (<div className="padding5 internal_url">
                                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                                <Link to={typesystem_full_metadata_record.internalELGUrl}>
                                                                    {typesystem}  {typesystemVersion ? <span>({typesystemVersion})</span> : ''}
                                                                </Link>
                                                            </div>)
                                                            :
                                                            (<div>{typesystem} {typesystemVersion ? <span>({typesystemVersion})</span> : ''}</div>)
                                                        }</span> </div>}

                                                    {annotation_schema && <div className="padding5"><Typography className="bold-p--id">{annotation_schema_label} </Typography><span className="info_value">
                                                        {annotation_schema_full_metadata_record ?
                                                            (<div className="padding5 internal_url">
                                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                                <Link to={annotation_schema_full_metadata_record.internalELGUrl}>
                                                                    {annotation_schema} {annotation_schema_version ? <span>({annotation_schema_version})</span> : ''}
                                                                </Link>
                                                            </div>)
                                                            :
                                                            (<div>{annotation_schema} {annotation_schema_version ? <span>({annotation_schema_version})</span> : ''}</div>)
                                                        }</span> </div>}

                                                    {annotation_resource && <div className="padding5"><Typography className="bold-p--id">{annotation_resource_label}</Typography><span className="info_value">
                                                        {annotation_resource_full_metadata_record ?
                                                            (<div className="padding5 internal_url">
                                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                                <Link to={annotation_resource_full_metadata_record.internalELGUrl}>
                                                                    {annotation_resource} {annotation_resource_version ? <span>({annotation_resource_version})</span> : ''}
                                                                </Link>
                                                            </div>)
                                                            :
                                                            (<div>{annotation_resource} {annotation_resource_version ? <span>({annotation_resource_version})</span> : ''}</div>)
                                                        }</span> </div>}

                                                    {sampleArray.length > 0 && <div className="padding5" style={{backgroundColor: "#fff", marginBottom: "1em" , padding:"0.3em", borderRadius:"4px"}}><Typography className="bold-p--id">{sample_label}</Typography>{sampleArray.map((sample, sampleIndex) => <div key={sampleIndex}>{sample}</div>)}</div>}



                                                </div>
                                            }

                                        </div>
                                    )

                                }
                                )}
                        </Paper>
                    </Grid>

                    <Grid item xs={12} sm={this.grid_size(data)} >
                        <Paper className="FunctionBox function">
                            <div className="FunctionBox--card back">

                                <div className="personal-info" >
                                    <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                                        <Grid item xs={2}>
                                            <ProcessingIcon style={{ width: 25, height: 25 }} className="SpinIcon" />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <Typography variant="h3" className="FunctionBoxHeading ">{data.described_entity.field_value.lr_subclass.field_value.function ? data.described_entity.field_value.lr_subclass.field_value.function.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.function.field_label["en"] : ""} </Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                            </div>

                            <div className="FunctionBox--inner Function">
                                {data.described_entity.field_value.lr_subclass &&
                                    <div className="padding5">
                                        <Typography className="bold-p--id">{data.described_entity.field_value.lr_subclass.field_value.function ? data.described_entity.field_value.lr_subclass.field_value.function.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.function.field_label["en"] : ""}</Typography>
                                        <div className="flex wrap">{data.described_entity.field_value.lr_subclass.field_value.function && data.described_entity.field_value.lr_subclass.field_value.function.field_value.map(
                                            (functItem, functItemIndex) =>
                                                <Chip key={functItemIndex} size="small" label={(functItem.label ? (functItem.label[metadataLanguage] || functItem.label[Object.keys(functItem.label)[0]]) : functItem.value) || ""} className="ChipTagTeal" />
                                        )}
                                        </div></div>

                                }
                                <div className="padding5">
                                    {data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.language_dependent &&
                                        <div>
                                            <Typography className="bold-p--id">{data.described_entity.field_value.lr_subclass.field_value.language_dependent.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.language_dependent.field_label["en"]} </Typography>
                                            <Chip size="small" label={data.described_entity.field_value.lr_subclass.field_value.language_dependent.field_value === true ? "yes" : "no"} className="ChipTagTeal" />
                                        </div>
                                    }
                                </div>
                            </div>


                        </Paper>
                    </Grid>

                    {data.described_entity.field_value.lr_subclass.field_value.output_resource  && <Grid item xs={12} sm={this.grid_size(data)}>
                        <Paper className="FunctionBox output" style={{ height:'100%'}}>
                            <div className="FunctionBox--card back">
                                <div className="personal-info" >
                                    <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                                        <Grid item xs={2}>
                                            <OutputIcon style={{ width: 25, height: 25 }} />
                                        </Grid>
                                        <Grid item xs={10}>
                                            <Typography variant="h3" className="FunctionBoxHeading">{data.described_entity.field_value.lr_subclass.field_value.output_resource ? data.described_entity.field_value.lr_subclass.field_value.output_resource.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.output_resource.field_label["en"] : ""}</Typography>
                                        </Grid>
                                    </Grid>
                                </div>

                            </div>
                            {data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.output_resource && data.described_entity.field_value.lr_subclass.field_value.output_resource.field_value.length > 0 &&
                                data.described_entity.field_value.lr_subclass.field_value.output_resource.field_value.map(function (outputResource, outputResourceIndex) {
                                    let sample_label = outputResource.sample ? outputResource.sample.field_label[metadataLanguage] || outputResource.sample.field_label["en"] : "Sample";
                                    let sampleArray = outputResource.sample ? outputResource.sample.field_value.map((sampleItem, sampleIndex) => {
                                        let samples_location = sampleItem.samples_location ? sampleItem.samples_location.field_value : "";
                                        let samples_location_label = sampleItem.samples_location ? (sampleItem.samples_location.field_label[metadataLanguage] || sampleItem.samples_location.field_label["en"]) : "Samples location";
                                        let tag_label = sampleItem.tag ? sampleItem.tag.field_label[metadataLanguage] || sampleItem.tag.field_label["en"] : "Tag";
                                        let tag_value = sampleItem.tag ? sampleItem.tag.field_value : "";
                                        let sample_text_label = sampleItem.sample_text ? sampleItem.sample_text.field_label[metadataLanguage] || sampleItem.sample_text.field_label["en"] : "Sample text";
                                        let sample_text = sampleItem.sample_text ? sampleItem.sample_text.field_value : "";
                                        return <div key={sampleIndex} className="padding5">
                                            {samples_location && <div className="padding5"> <span className="info_url"><LaunchIcon className="xsmall-icon" /> </span> <span><a href={samples_location} target="_blank" rel="noopener noreferrer">{samples_location_label}</a></span></div>}
                                            {sample_text && <div className="padding5"> <Typography className="bold-p--id">{sample_text_label}</Typography> <span className="info_value">{sample_text}</span></div>}
                                            {tag_value && <div className="padding5"> <Typography className="bold-p--id">{tag_label}</Typography> <span className="info_value">{tag_value}</span></div>}
                                        </div>
                                    }) : [];
                                    let media_type = outputResource.media_type ? (outputResource.media_type.label[metadataLanguage] || outputResource.media_type.label[Object.keys(outputResource.media_type.label)[0]]) : "";
                                    let media_type_label = outputResource.media_type ? (outputResource.media_type.field_label[metadataLanguage] || outputResource.media_type.field_label["en"]) : "Media type";
                                    let character_encodingArray = outputResource.character_encoding ? outputResource.character_encoding.field_value.map(encoding => (encoding.label[metadataLanguage] || encoding.label[Object.keys(encoding.label)[0]])) : [];
                                    let character_encoding_label = outputResource.character_encoding ? (outputResource.character_encoding.field_label[metadataLanguage] || outputResource.character_encoding.field_label["en"]) : "Character encoding";
                                    let annotation_typeArray = outputResource.annotation_type ? outputResource.annotation_type.field_value.map(annotationType => annotationType.label ? (annotationType.label[metadataLanguage] || annotationType.label[Object.keys(annotationType.label)[0]]) : annotationType.value) : [];
                                    let annotation_type_label = outputResource.annotation_type ? (outputResource.annotation_type.field_label[metadataLanguage] || outputResource.annotation_type.field_label["en"]) : "Annotation type";
                                    let segmentation_levelArray = outputResource.segmentation_level ? outputResource.segmentation_level.field_value.map(segmentation => (segmentation.label[metadataLanguage] || segmentation.label[Object.keys(segmentation.label)[0]])) : [];
                                    let segmentation_level_label = outputResource.segmentation_level ? (outputResource.segmentation_level.field_label[metadataLanguage] || outputResource.segmentation_level.field_label["en"]) : "Segmentation level";
                                    let typesystem = (outputResource.typesystem && outputResource.typesystem.field_value.resource_name) ? (outputResource.typesystem.field_value.resource_name.field_value[metadataLanguage] || outputResource.typesystem.field_value.resource_name.field_value[Object.keys(outputResource.typesystem.field_value.resource_name.field_value)[0]]) : "";
                                    let typesystem_label = outputResource.typesystem ? (outputResource.typesystem.field_label[metadataLanguage] || outputResource.typesystem.field_label["en"]) : "Typesystem";
                                    let typesystemVersion = (outputResource.typesystem && outputResource.typesystem.field_value.version) ? outputResource.typesystem.field_value.version.field_value : "";
                                    let typesystem_full_metadata_record = (outputResource.typesystem ? commonParser.getFullMetadata(outputResource.typesystem.field_value.full_metadata_record) : "");

                                    let annotation_schema = (outputResource.annotation_schema && outputResource.annotation_schema.field_value.resource_name) ? (outputResource.annotation_schema.field_value.resource_name.field_value[metadataLanguage] || outputResource.annotation_schema.field_value.resource_name.field_value[Object.keys(outputResource.annotation_schema.field_value.resource_name.field_value)[0]]) : "";
                                    let annotation_schema_label = outputResource.annotation_schema ? (outputResource.annotation_schema.field_label[metadataLanguage] || outputResource.annotation_schema.field_label["en"]) : "Annotation schema"
                                    let annotation_schema_version = (outputResource.annotation_schema && outputResource.annotation_schema.field_value.version) ? outputResource.annotation_schema.field_value.version.field_value : "";
                                    let annotation_schema_full_metadata_record = (outputResource.annotation_schema ? commonParser.getFullMetadata(outputResource.annotation_schema.field_value.full_metadata_record) : "");

                                    let annotation_resource = (outputResource.annotation_resource && outputResource.annotation_resource.field_value.resource_name) ? (outputResource.annotation_resource.field_value.resource_name.field_value[metadataLanguage] || outputResource.annotation_resource.field_value.resource_name.field_value[Object.keys(outputResource.annotation_resource.field_value.resource_name.field_value)[0]]) : "";
                                    let annotation_resource_label = outputResource.annotation_resource ? (outputResource.annotation_resource.field_label[metadataLanguage] || outputResource.annotation_resource.field_label["en"]) : "Annotation resource"
                                    let annotation_resource_version = (outputResource.annotation_resource && outputResource.annotation_resource.field_value.version) ? outputResource.annotation_resource.field_value.version.field_value : "";
                                    let annotation_resource_full_metadata_record = (outputResource.annotation_resource ? commonParser.getFullMetadata(outputResource.annotation_resource.field_value.full_metadata_record) : "");;

                                    let modality_typeArray = outputResource.modality_type ? outputResource.modality_type.field_value.map(modality => (modality.label[metadataLanguage] || modality.label[Object.keys(modality.label)[0]])) : [];
                                    let modality_type_label = outputResource.modality_type ? (outputResource.modality_type.field_label[metadataLanguage] || outputResource.modality_type.field_label["en"]) : "Modality type";

                                    let modality_type_details = outputResource.modality_type_details ? (outputResource.modality_type_details.field_value[metadataLanguage] || outputResource.modality_type_details.field_value[Object.keys(outputResource.modality_type_details.field_value)[0]]) : "";
                                    let modality_type_details_label = outputResource.modality_type_details ? (outputResource.modality_type_details.field_label[metadataLanguage] || outputResource.modality_type_details.field_label["en"]) : "Modality type details";

                                    let processingResourceType = outputResource.processing_resource_type ? (outputResource.processing_resource_type.label[metadataLanguage] || outputResource.processing_resource_type.label[Object.keys(outputResource.processing_resource_type.label)[0]]) : undefined;
                                    let processingResourceType_label = outputResource.processing_resource_type ? (outputResource.processing_resource_type.field_label[metadataLanguage] || outputResource.processing_resource_type.field_label["en"]) : "Processing resource type"
                                    let dataFormatArray = outputResource.data_format ? outputResource.data_format.field_value.map(format => (format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) : [];
                                    let dataFormat_label = outputResource.data_format ? (outputResource.data_format.field_label[metadataLanguage] || outputResource.data_format.field_label["en"]) : "Data format";
                                    let mimetype_label = outputResource.mimetype ? (outputResource.mimetype.field_label[metadataLanguage] || outputResource.mimetype.field_label["en"]) : "";
                                    let mimetypeArray = (outputResource.mimetype && outputResource.mimetype.field_value.map(mimetype => mimetype)) || [];

                                    return (
                                        <div id={outputResourceIndex} key={outputResourceIndex} className="FunctionBox--inner">
                                            {/*languageInfoArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{language_label}</Typography><span className="info_value">{languageInfoArray.map((item, index) => <span key={index} className="border-bottom-2">{item}</span>)}</span></div>*/}
                                            {<Languages language = {outputResource.language} from_tool={true} inferred_language={inferred_language}/>}
                                            {processingResourceType && <div className="padding5"><Typography className="bold-p--id">{processingResourceType_label}</Typography><Chip size="small" label={processingResourceType} className="ChipTagTeal" /> </div>}
                                            {dataFormatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{dataFormat_label}</Typography><div className="flex wrap">{dataFormatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div></div>}
                                            {mimetypeArray && mimetypeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{mimetype_label} </Typography>{mimetypeArray.map((mimetype, mimetypeIndex) => <span key={mimetypeIndex} className="info-url" >{mimetype}</span>)}</div>}
                                            {(sampleArray || media_type || typesystem || annotation_schema || annotation_resource || character_encodingArray
                                                || annotation_typeArray || segmentation_levelArray || modality_typeArray || modality_type_details) &&
                                                <div>
                                                    {character_encodingArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{character_encoding_label}</Typography><div className="flex wrap">{character_encodingArray.map((encoding, encodingIndex) => <Chip key={encodingIndex} size="small" label={encoding} className="ChipTagTeal" />)}</div></div>}
                                                    {media_type && <div className="padding5"><Typography className="bold-p--id">{media_type_label} </Typography><Chip size="small" label={media_type} className="ChipTagTeal" /></div>}

                                                    {modality_typeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{modality_type_label}</Typography><div className="flex wrap">{modality_typeArray.map((modality, modalityIndex) => <Chip key={modalityIndex} size="small" label={modality} className="ChipTagTeal" />)}</div></div>}
                                                    {modality_type_details && <div className="padding5"><Typography className="bold-p--id">{modality_type_details_label}</Typography><span className="info_value"> {modality_type_details} </span> </div>}
                                                    {annotation_typeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotation_type_label}</Typography><div className="flex wrap">{annotation_typeArray.map((annotation, annotationIndex) => <Chip key={annotationIndex} size="small" label={annotation} className="ChipTagTeal" />)}</div></div>}
                                                    {segmentation_levelArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{segmentation_level_label}</Typography><div className="flex wrap">{segmentation_levelArray.map((segmentation, segementationIndex) => <Chip key={segementationIndex} size="small" label={segmentation} className="ChipTagTeal" />)}</div></div>}


                                                    {typesystem && <div className="padding5"><Typography className="bold-p--id">{typesystem_label}</Typography><span className="info_value">
                                                        {typesystem_full_metadata_record ?
                                                            (<div className="padding5 internal_url">
                                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                                <Link to={typesystem_full_metadata_record.internalELGUrl}>
                                                                    {typesystem} {typesystemVersion ? <span>({typesystemVersion})</span> : ''}
                                                                </Link>
                                                            </div>)
                                                            :
                                                            (<div>{typesystem} {typesystemVersion ? <span>({typesystemVersion})</span> : ''}</div>)
                                                        }</span> </div>}

                                                    {annotation_schema && <div className="padding5"><Typography className="bold-p--id">{annotation_schema_label} </Typography><span className="info_value">
                                                        {annotation_schema_full_metadata_record ?
                                                            (<div className="padding5 internal_url">
                                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                                <Link to={annotation_schema_full_metadata_record.internalELGUrl}>
                                                                    {annotation_schema} {annotation_schema_version ? <span>({annotation_schema_version})</span> : ''}
                                                                </Link>
                                                            </div>)
                                                            :
                                                            (<div>{annotation_schema} {annotation_schema_version ? <span>({annotation_schema_version})</span> : ''}</div>)
                                                        }</span> </div>}

                                                    {annotation_resource && <div className="padding5"><Typography className="bold-p--id">{annotation_resource_label}</Typography><span className="info_value">
                                                        {annotation_resource_full_metadata_record ?
                                                            (<div className="padding5 internal_url">
                                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                                <Link to={annotation_resource_full_metadata_record.internalELGUrl}>
                                                                    {annotation_resource} {annotation_resource_version ? <span>({annotation_resource_version})</span> : ''}
                                                                </Link>
                                                            </div>)
                                                            :
                                                            (<div>{annotation_resource} {annotation_resource_version ? <span>({annotation_resource_version})</span> : ''}</div>)
                                                        }</span> </div>}

                                                    {sampleArray.length > 0 && <div className="padding5" style={{backgroundColor: "#fff", marginBottom: "1em" , padding:"0.3em", borderRadius:"4px"}}><Typography className="bold-p--id">{sample_label}</Typography>{sampleArray.map((sample, sampleIndex) => <div key={sampleIndex}>{sample}</div>)}</div>}

                                                </div>
                                            }

                                        </div>
                                    )

                                }
                                )}

                        </Paper>
                    </Grid>}
                </Grid>
            </>
        )
    }
}