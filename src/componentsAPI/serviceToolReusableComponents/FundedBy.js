import React from "react";
import { Link } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import serviceToolParser from "../../parsers/ServiceToolParser";
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";

export default class FundedBy extends React.Component {

    constructor(props) {
        super(props);
        this.renderTitle = this.renderTitle.bind(this);
    }

    renderTitle(fundingProject, fundingProjectIndex, data) {
        const full_metadata_record = commonParser.getFullMetadata(fundingProject.full_metadata_record);
        if (full_metadata_record) {
            return <div className="padding5 internal_url">
                <span><NavIcon className="xsmall-icon mr-05" /></span><Link to={full_metadata_record.internalELGUrl}>
                    {serviceToolParser.getFundingProjects(data)[fundingProjectIndex].project_name}
                </Link>
            </div>
        } else {
            return <div className="padding5">
                <div className="info_value inline">{serviceToolParser.getFundingProjects(data)[fundingProjectIndex].project_name} </div>
            </div>
        }

    }

    render() {
        const { data, metadataLanguage } = this.props;
        //const {ProjectImage}=this.props;
        if (!data) {
            return <></>
        }
        const fundingProjects = serviceToolParser.getFundingProjects(data, metadataLanguage);
        return <>
            {fundingProjects && fundingProjects.length > 0 && <Typography variant="h3" className="title-links"> {data.described_entity.field_value.funding_project.field_label[metadataLanguage] || data.described_entity.field_value.funding_project.field_label["en"]}</Typography>}
            {fundingProjects && fundingProjects.length > 0 && data.described_entity.field_value.funding_project.field_value.map((fundingProject, fundingProjectIndex) =>
                <div key={fundingProjectIndex} id={fundingProjectIndex} className="padding15">
                    <div className="padding5">
                        {this.renderTitle(fundingProject, fundingProjectIndex, data)}
                        {serviceToolParser.getFundingProjects(data)[fundingProjectIndex].website &&
                        <>
                            <a className="info_url" href={serviceToolParser.getFundingProjects(data)[fundingProjectIndex].website} target="_blank" rel="noopener noreferrer">
                                <span><LaunchIcon className="xsmall-icon" /> </span>
                                <span>{serviceToolParser.getFundingProjects(data)[fundingProjectIndex].website_label}</span></a>
                        </>
                    }
                    {serviceToolParser.getFundingProjects(data)[fundingProjectIndex].funding_type && serviceToolParser.getFundingProjects(data)[fundingProjectIndex].funding_type.length > 0 && <Typography className="bold-p--id">{serviceToolParser.getFundingProjects(data)[fundingProjectIndex].funding_type_label}</Typography>}
                    {serviceToolParser.getFundingProjects(data)[fundingProjectIndex].funding_type && serviceToolParser.getFundingProjects(data)[fundingProjectIndex].funding_type.length > 0 && serviceToolParser.getFundingProjects(data)[fundingProjectIndex].funding_type.map((fund, fundIndex) =>
                        <span key={fundIndex} className="info_value">{fund.label[metadataLanguage] || fund.label[Object.keys(fund.label)[0]]} &nbsp;</span>)
                    }   
                             </div>
                </div>
            )}
        </>
    }
}