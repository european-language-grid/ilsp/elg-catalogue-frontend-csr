import React from "react";
import Typography from '@material-ui/core/Typography';
import DocumentIdentifier from "../CommonComponents/DocumentIdentifier";
import { ReactComponent as DocIcon } from "./../../assets/elg-icons/document.svg";

export default class Documentations extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;

        if (!data) {
            return <div></div>
        }
        return <div className="padding15">
            {((data.described_entity.field_value.is_documented_by && data.described_entity.field_value.is_documented_by.field_value.length > 0) ||
                (data.described_entity.field_value.is_described_by && data.described_entity.field_value.is_described_by.field_value.length > 0) ||
                (data.described_entity.field_value.is_cited_by && data.described_entity.field_value.is_cited_by.field_value.length > 0) ||
                (data.described_entity.field_value.is_reviewed_by && data.described_entity.field_value.is_reviewed_by.field_value.length > 0) ||
                (data.described_entity.field_value.is_to_be_cited_by && data.described_entity.field_value.is_to_be_cited_by.length > 0)) ? <Typography variant="h3" className="title-links">Documentation </Typography> : void 0
            }
            {data.described_entity.field_value.is_to_be_cited_by && data.described_entity.field_value.is_to_be_cited_by.field_value.length>0 && <Typography className="bold-p--id">{data.described_entity.field_value.is_to_be_cited_by.field_label[metadataLanguage] || data.described_entity.field_value.is_to_be_cited_by.field_label["en"]}</Typography>}
            {data.described_entity.field_value.is_to_be_cited_by && data.described_entity.field_value.is_to_be_cited_by.field_value.map((tobecitedBy, index) => {
                //let title = (tobecitedBy.title.field_value[metadataLanguage] || tobecitedBy.title.field_value[Object.keys(tobecitedBy.title.field_value)[0]]) || "";
                //let document_identifier = tobecitedBy.document_identifier.field_value.filter((identifier) => identifier.document_identifier_scheme.label["en"] !== "ELG") || [];//filter out elg identifiers
                ////let bibliographic_record_label = tobecitedBy.bibliographic_record ? tobecitedBy.bibliographic_record.field_label[metadataLanguage] || tobecitedBy.bibliographic_record.field_label["en"] : "";
                let bibliographic_record = tobecitedBy.bibliographic_record ? tobecitedBy.bibliographic_record.field_value : "";
                return <div key={index} className="padding5">
                    <div className="flex-centered">
                        <span className="mr-05"><DocIcon /></span>
                        <span>
                            <DocumentIdentifier data={tobecitedBy} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </span>
                    </div>
                    <div>
                        {bibliographic_record && <>
                            <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                    </div>
                </div>
            })
            }
            {data.described_entity.field_value.is_documented_by && data.described_entity.field_value.is_documented_by.field_value.length>0 && <Typography className="bold-p--id">{data.described_entity.field_value.is_documented_by.field_label[metadataLanguage] || data.described_entity.field_value.is_documented_by.field_label["en"]}</Typography>}
            {data.described_entity.field_value.is_documented_by && data.described_entity.field_value.is_documented_by.field_value.map((documentedBy, index) => {
                //let title = (documentedBy.title.field_value[metadataLanguage] || documentedBy.title.field_value[Object.keys(documentedBy.title.field_value)[0]]) || "";
                //let document_identifier = documentedBy.document_identifier.field_value.filter((identifier) => identifier.document_identifier_scheme.label["en"] !== "ELG") || [];//filter out elg identifiers 
                //let bibliographic_record_label = documentedBy.bibliographic_record ? documentedBy.bibliographic_record.field_label[metadataLanguage] || documentedBy.bibliographic_record.field_label["en"] : "";
                let bibliographic_record = documentedBy.bibliographic_record ? documentedBy.bibliographic_record.field_value : "";
                return <div key={index} className="padding5">
                <div className="flex-centered">
                    <span className="mr-05"><DocIcon /></span>
                    <span>
                        <DocumentIdentifier data={documentedBy} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                    </span>
                </div>
                <div>
                    {bibliographic_record && <>
                        <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                </div>
            </div>
            })
            }
            {data.described_entity.field_value.is_described_by && data.described_entity.field_value.is_described_by.field_value.length>0 && <Typography className="bold-p--id">{data.described_entity.field_value.is_described_by.field_label[metadataLanguage] || data.described_entity.field_value.is_described_by.field_label["en"]}</Typography>}
            {data.described_entity.field_value.is_described_by && data.described_entity.field_value.is_described_by.field_value.map((describedBy, index) => {
                //let title = (describedBy.title.field_value[metadataLanguage] || describedBy.title.field_value[Object.keys(describedBy.title.field_value)[0]]) || "";
                //let document_identifier = describedBy.document_identifier.field_value.filter((identifier) => identifier.document_identifier_scheme.label["en"] !== "ELG") || [];//filter out elg identifiers 
                //let bibliographic_record_label = describedBy.bibliographic_record ? describedBy.bibliographic_record.field_label[metadataLanguage] || describedBy.bibliographic_record.field_label["en"] : "";
                let bibliographic_record = describedBy.bibliographic_record ? describedBy.bibliographic_record.field_value : "";
                return <div key={index} className="padding5">
                <div className="flex-centered">
                    <span className="mr-05"><DocIcon /></span>
                    <span>
                        <DocumentIdentifier data={describedBy} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                    </span>
                </div>
                <div>
                    {bibliographic_record && <>
                        <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                </div>
            </div>
            })
            }
            {data.described_entity.field_value.is_cited_by && data.described_entity.field_value.is_cited_by.field_value.length>0 && <Typography className="bold-p--id">{data.described_entity.field_value.is_cited_by.field_label[metadataLanguage] || data.described_entity.field_value.is_cited_by.field_label["en"]}</Typography>}
            {data.described_entity.field_value.is_cited_by && data.described_entity.field_value.is_cited_by.field_value.map((citedBy, index) => {
                //let title = (citedBy.title.field_value[metadataLanguage] || citedBy.title.field_value[Object.keys(citedBy.title.field_value)[0]]) || "";
                //let document_identifier = citedBy.document_identifier.field_value.filter((identifier) => identifier.document_identifier_scheme.label["en"] !== "ELG") || [];//filter out elg identifiers 
                //let bibliographic_record_label = citedBy.bibliographic_record ? citedBy.bibliographic_record.field_label[metadataLanguage] || citedBy.bibliographic_record.field_label["en"] : "";
                let bibliographic_record = citedBy.bibliographic_record ? citedBy.bibliographic_record.field_value : "";
                return <div key={index} className="padding5">
                <div className="flex-centered">
                    <span className="mr-05"><DocIcon /></span>
                    <span>
                        <DocumentIdentifier data={citedBy} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                    </span>
                </div>
                <div>
                    {bibliographic_record && <>
                        <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                </div>
            </div>
            })
            }
            {data.described_entity.field_value.is_reviewed_by && data.described_entity.field_value.is_reviewed_by.field_value.length>0 &&  <Typography className="bold-p--id">{data.described_entity.field_value.is_reviewed_by.field_label[metadataLanguage] || data.described_entity.field_value.is_reviewed_by.field_label["en"]}</Typography>}
            {data.described_entity.field_value.is_reviewed_by && data.described_entity.field_value.is_reviewed_by.field_value.map((reviewedBy, index) => {
                //let title = (reviewedBy.title.field_value[metadataLanguage] || reviewedBy.title.field_value[Object.keys(reviewedBy.title.field_value)[0]]) || "";
                //let document_identifier = reviewedBy.document_identifier.field_value.filter((identifier) => identifier.document_identifier_scheme.label["en"] !== "ELG") || [];//filter out elg identifiers 
                //let bibliographic_record_label = reviewedBy.bibliographic_record ? reviewedBy.bibliographic_record.field_label[metadataLanguage] || reviewedBy.bibliographic_record.field_label["en"] : "";
                let bibliographic_record = reviewedBy.bibliographic_record ? reviewedBy.bibliographic_record.field_value : "";
                return <div key={index} className="padding5">
                <div className="flex-centered">
                    <span className="mr-05"><DocIcon /></span>
                    <span>
                        <DocumentIdentifier data={reviewedBy} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                    </span>
                </div>
                <div>
                    {bibliographic_record && <>
                        <pre className="cite"><code>{bibliographic_record}</code></pre></>}
                </div>
            </div>
            })
            }
        </div>
    }
}