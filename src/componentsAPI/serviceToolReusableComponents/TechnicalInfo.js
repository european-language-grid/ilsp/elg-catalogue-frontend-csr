import React from "react";
import Typography from '@material-ui/core/Typography';
import Dependencies from "./Dependencies";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

export default class TechnicalInfo extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const { development_framework, formalism,
            method, implementation_language,
            requires_software, required_hardware,
            running_environment_details, running_time,
            has_original_source, original_source_description,
            creation_details } = data.described_entity.field_value.lr_subclass.field_value;
        const showTechnicalInformation = development_framework || formalism || method || implementation_language || requires_software
            || required_hardware || running_environment_details || running_time || has_original_source || original_source_description
            || creation_details;

        if (!showTechnicalInformation) {
            return <div>
                <Dependencies data={data} metadataLanguage={metadataLanguage} />
            </div>
        }
        return <div className="padding5">
            {showTechnicalInformation && <Typography variant="h3" className="title-links">Technical Information</Typography>}
            {development_framework && development_framework.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{development_framework.field_label[metadataLanguage] || development_framework.field_label["en"]}</Typography>
                {development_framework && development_framework.field_value.length > 0 && development_framework.field_value.map((item, frIndex) => {
                    return <div key={frIndex}>
                        {item.label ? <span className="info_value">{item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]]}</span> : <span className="info_value">{item.value}</span>}
                    </div>
                })}</div>
            }


            {
                formalism && <div className="padding5">
                    <Typography className="bold-p--id">{formalism.field_label[metadataLanguage] || formalism.field_label["en"]}</Typography>  <span className="info_value">{formalism.field_value[metadataLanguage] || formalism.field_value[Object.keys(formalism.field_value)[0]]}</span>
                </div>
            }
            {
                method && <div className="padding5">
                    <Typography className="bold-p--id">{method.field_label[metadataLanguage] || method.field_label["en"]}</Typography><span className="info_value">{method.label[metadataLanguage] || method.label[Object.keys(method.label)[0]]} </span>
                </div>
            }            
             
                    <div>
                        {
                            implementation_language && <div className="padding15">
                                <Typography className="bold-p--id">{implementation_language.field_label[metadataLanguage] || implementation_language.field_label["en"]}</Typography>  <span className="info_value">{implementation_language.field_value}</span>
                            </div>
                        }
                        {
                            requires_software && requires_software.field_value.length > 0 && <div className="padding15">
                                <Typography className="bold-p--id"> {requires_software.field_label[metadataLanguage] || requires_software.field_label["en"]}</Typography>
                                {requires_software.field_value.map((item, index) => <div className="info_value" key={index}>{item}</div>)}
                            </div>
                        }
                        {
                            required_hardware && required_hardware.field_value.length > 0 && <div className="padding15">
                                <Typography className="bold-p--id">{required_hardware.field_label[metadataLanguage] || required_hardware.field_label["en"]}</Typography>
                                {required_hardware.field_value.map((item, index) => <div className="info_value" key={index}>{item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]]}</div>)}

                            </div>
                        }
                        {
                            running_environment_details && <div className="padding15">
                                <Typography className="bold-p--id">{running_environment_details.field_label[metadataLanguage] || running_environment_details.field_label["en"]}</Typography>  <span className="info_value ">{running_environment_details.field_value[metadataLanguage] || running_environment_details.field_value[Object.keys(running_environment_details.field_value)[0]]}</span>
                            </div>
                        }
                        {
                            running_time && <div className="padding15">
                                <Typography className="bold-p--id">{running_time.field_label[metadataLanguage] || running_time.field_label["en"]}</Typography>  <span className="info_value padding5">{running_time.field_value}</span>
                            </div>
                        }
                        {
                            has_original_source && has_original_source.field_value.length > 0 && <div className="padding15">
                                <Typography className="bold-p--id">{has_original_source.field_label[metadataLanguage] || has_original_source.field_label["en"]}</Typography>
                                {has_original_source.field_value.map((item, index) => {
                                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]] || "") : "";
                                    let version = item.version ? item.version.field_value : "";
                                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                                    return <>
                                        {full_metadata_record ?
                                            (<div className="padding5 internal_url">
                                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                                <Link to={full_metadata_record.internalELGUrl}>
                                                    {resource_name} , {version}
                                                </Link>
                                            </div>)
                                            :
                                            (<div className="info_value" key={index}>{resource_name} , {version} </div>)
                                        }

                                    </>




                                })
                                }

                            </div>
                        }
                        {original_source_description && <div className="padding15">
                            <Typography className="bold-p--id">{original_source_description.field_label[metadataLanguage] || original_source_description.field_label["en"]}</Typography>
                            <span className="info_value">{original_source_description.field_value[metadataLanguage] || original_source_description.field_value[Object.keys(original_source_description.field_value)[0]]}</span>
                        </div>
                        }
                        {creation_details && <div className="padding15">
                            <Typography className="bold-p--id">{creation_details.field_label[metadataLanguage] || creation_details.field_label["en"]}</Typography>
                            <span className="info_value"> {creation_details.field_value[metadataLanguage] || creation_details.field_value[Object.keys(creation_details.field_value)[0]]}</span>
                        </div>
                        }
                        <Dependencies data={data} metadataLanguage={metadataLanguage} />
                    </div>
                
        </div>
    }
}