import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import ResourceActor from "../CommonComponents/ResourceActor";
import { IS_ADMIN, IS_CONTENT_MANAGER, SHOW_BILLING_SERVICE_PK, IS_CUSTOMER } from "../../config/constants";
import { keycloak } from "../../App";
import DownloadDatasetCurator from "../CommonComponents/DownloadDatasetCurator";
import AcceptLicence from "../CommonComponents/AcceptLicence";
import { Helmet } from "react-helmet";
import DockerPullCommand from "./DockerPullCommand";
//import commonParser from "../../parsers/CommonParser";
//import { Link } from "react-router-dom";
//import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default class Distributions extends React.Component {

    show_private_urls = (data, keycloak) => {
        return (this.props.isOwner || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak) || (this.props.isLegalValidator && data.management_object.status === "g") || (this.props.isMetadataValidator && data.management_object.status === "g") || (this.props.isTechnicalValidator && data.management_object.status === "g"));
    }


    show_card_action = (data, distribution, downloadLocation, access_location) => {
        const show_download_location = downloadLocation && !["http://www.hiddenLocation.org", "Hidden value"].includes(downloadLocation);
        const show_access_location = access_location && !["http://www.hiddenLocation.org", "Hidden value"].includes(access_location);
        let show_download_button = false;
        const { dataset = "" } = distribution;
        const package_f = distribution.package;
        if (data && (dataset || package_f)) {
            show_download_button = true;
        }
        return show_download_location || show_access_location || show_download_button;
    }

    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const software_distribution = (data.described_entity && data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.software_distribution) ? data.described_entity.field_value.lr_subclass.field_value.software_distribution.field_value : [];
        /*const displayHyperLink = (url) => {
            if (!url) {
                return false;
            }
            if (url.indexOf("127.0") >= 0 || url.indexOf("0.0") >= 0 || url.indexOf("::1") >= 0 || url.toLowerCase().indexOf("localhost") >= 0) {
                return false;
            }
            return true;
        }*/
        const show_billing = data.pk === SHOW_BILLING_SERVICE_PK && IS_CUSTOMER(keycloak);
        return <div>
            {show_billing && <Helmet>
                <script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="european-language-grid-eu-test" > </script>
            </Helmet>}
            <Grid container className="upperSpace" spacing={2} direction="row">
                {software_distribution.map((distribution, distributionIndex) => {
                    let software_distribution_form = distribution.software_distribution_form ? (distribution.software_distribution_form.label[metadataLanguage] || distribution.software_distribution_form.label[Object.keys(distribution.software_distribution_form.label)[0]]) : "";
                    let software_distribution_form_label = distribution.software_distribution_form ? (distribution.software_distribution_form.field_label[metadataLanguage] || distribution.software_distribution_form.field_label["en"]) : "Software distribution form";
                    let downloadLocation = distribution.download_location ? distribution.download_location.field_value : "";
                    let downloadLocation_label = distribution.download_location ? (distribution.download_location.field_label[metadataLanguage] || distribution.download_location.field_label["en"]) : "Download location";
                    let docker_download_location = distribution.docker_download_location ? distribution.docker_download_location.field_value : "";
                    let docker_download_location_label = distribution.docker_download_location ? (distribution.docker_download_location.field_label[metadataLanguage] || distribution.docker_download_location.field_label["en"]) : "Docker download location";
                    let executionLocation = distribution.execution_location ? distribution.execution_location.field_value : "";
                    let executionLocation_label = distribution.execution_location ? (distribution.execution_location.field_label[metadataLanguage] || distribution.execution_location.field_label["en"]) : "Execution location";
                    let service_adapter_download_location = distribution.service_adapter_download_location ? distribution.service_adapter_download_location.field_value : "";
                    let service_adapter_download_location_label = distribution.service_adapter_download_location ? (distribution.service_adapter_download_location.field_label[metadataLanguage] || distribution.service_adapter_download_location.field_label["en"]) : "Service adapter download location";
                    let access_location = distribution.access_location ? distribution.access_location.field_value : "";
                    let access_location_label = distribution.access_location ? (distribution.access_location.field_label[metadataLanguage] || distribution.access_location.field_label["en"]) : "";
                    let demo_location = distribution.demo_location ? distribution.demo_location.field_value : "";
                    let demo_location_label = distribution.demo_location ? (distribution.demo_location.field_label[metadataLanguage] || distribution.demo_location.field_label["en"]) : "Demo location";
                    let package_format_label = distribution.package_format ? (distribution.package_format.field_label[metadataLanguage] || distribution.package_format.field_label["en"]) : "";
                    let package_format = distribution.package_format ? (distribution.package_format.label[metadataLanguage] || distribution.package_format.label["en"]) : "";

                    let is_described_byArray = distribution.is_described_by ? distribution.is_described_by.field_value.map((describedBy, describedByindex) => {
                        let title = (describedBy.title.field_value[metadataLanguage] || describedBy.title.field_value[Object.keys(describedBy.title.field_value)[0]]) || "";
                        let document_identifier = describedBy.document_identifier.field_value.filter((identifier) => identifier.document_identifier_scheme.label["en"] !== "ELG") || [];//filter out elg identifiers 
                        return <div key={describedByindex} >
                            <div>{document_identifier.length > 0 ? document_identifier.map((identifier, IdentifierIndex) => <div className="info_value" key={IdentifierIndex}><a href={identifier.value.field_value} target="blank">{title}</a></div>) : title}</div>

                        </div>
                    }) : [];
                    let is_described_by_label = distribution.is_described_by ? (distribution.is_described_by.field_label[metadataLanguage] || distribution.is_described_by.field_label["en"]) : "Is described by";

                    let additional_hw_requirements = distribution.additional_hw_requirements ? distribution.additional_hw_requirements.field_value : "";
                    let additional_hw_requirements_label = distribution.additional_hw_requirements ? (distribution.additional_hw_requirements.field_label[metadataLanguage] || distribution.additional_hw_requirements.field_label["en"]) : "Additional h/w requirements";
                    let command = distribution.command ? distribution.command.field_value : "";
                    let command_label = distribution.command ? (distribution.command.field_label[metadataLanguage] || distribution.command.field_label["en"]) : "Command";
                    let web_service_type = distribution.web_service_type ? (distribution.web_service_type.label[metadataLanguage] || distribution.web_service_type.label[Object.keys(distribution.web_service_type.label)[0]]) : "";
                    let web_service_type_label = distribution.web_service_type ? (distribution.web_service_type.field_label[metadataLanguage] || distribution.web_service_type.field_label["en"]) : "Web service type";
                    let operating_systemArray = distribution.operating_system ? distribution.operating_system.field_value.map(os => os.label[metadataLanguage] || os.label[Object.keys(os.label)[0]]) : [];
                    let operation_system_label = distribution.operating_system ? (distribution.operating_system.field_label[metadataLanguage] || distribution.operating_system.field_label["en"]) : "Operating system";



                    let licenceTerms_urlArray = distribution.licence_terms ? distribution.licence_terms.field_value.map((licence_terms_item, licence_terms_index) => {
                        let licence_terms_name = licence_terms_item.licence_terms_name ? (licence_terms_item.licence_terms_name.field_value[metadataLanguage] || licence_terms_item.licence_terms_name.field_value[Object.keys(licence_terms_item.licence_terms_name.field_value)[0]]) : "";
                        let licence_terms_url_local = licence_terms_item.licence_terms_url ? licence_terms_item.licence_terms_url.field_value.map(lurl => lurl) : [];
                        let condition_of_use_label = licence_terms_item.condition_of_use ? (licence_terms_item.condition_of_use.field_label[metadataLanguage] || licence_terms_item.condition_of_use.field_label["en"]) : "";
                        let condition_of_useArray = licence_terms_item.condition_of_use ? licence_terms_item.condition_of_use.field_value.map(cond => cond.label[metadataLanguage] || cond.label[Object.keys(cond.label)[0]]) : [];

                        return <div key={licence_terms_index} className="info_value">
                            {licence_terms_name && <span>{licence_terms_name}</span>}
                            {licence_terms_url_local.length > 0 && <div className="padding5"><Typography className="bold-p--id"></Typography><div className="info_value">{licence_terms_url_local.map((lurl, lurlIndex) => <div key={lurlIndex}><a href={lurl} target="_blank" rel="noopener noreferrer">{lurl}</a></div>)}</div></div>}
                            {condition_of_useArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{condition_of_use_label}</Typography><div className="info_value">{condition_of_useArray.map((cond, condIndex) => <Chip key={condIndex} size="small" label={cond} className="ChipTagTeal" />)}</div></div>}

                        </div>
                    }) : [];
                    let licenceTerms_label = distribution.licence_terms ? (distribution.licence_terms.field_label[metadataLanguage] || distribution.licence_terms.field_label["en"]) : "Licence terms";

                    let cost = distribution.cost ? distribution.cost.field_value.amount.field_value : null;
                    let cost_label = distribution.cost ? (distribution.cost.field_label[metadataLanguage] || distribution.cost.field_label["en"]) : "Cost";
                    let currency = (distribution.cost && distribution.cost.field_value.currency) ? (distribution.cost.field_value.currency.label[metadataLanguage] || distribution.cost.field_value.currency.label[Object.keys(distribution.cost.field_value.currency.label)[0]]) : "";
                    let membership_institutionArray = distribution.membership_institution ? distribution.membership_institution.field_value.map(membership => membership.label[metadataLanguage] || membership.label[Object.keys(membership.label)[0]]) : [];
                    let membership_institution_label = distribution.membership_institution ? (distribution.membership_institution.field_label[metadataLanguage] || distribution.membership_institution.field_label["en"]) : "Membership institution";
                    let attribution_text = distribution.attribution_text ? (distribution.attribution_text.field_value[metadataLanguage] || distribution.attribution_text.field_value[Object.keys(distribution.attribution_text.field_value)[0]]) : "";
                    let attribution_text_label = distribution.attribution_text ? (distribution.attribution_text.field_label[metadataLanguage] || distribution.attribution_text.field_label["en"]) : "Attribution text";
                    let copyright_statement = distribution.copyright_statement ? (distribution.copyright_statement.field_value[metadataLanguage] || distribution.copyright_statement.field_value[Object.keys(distribution.copyright_statement.field_value)[0]]) : "";
                    let copyright_statement_label = distribution.copyright_statement ? (distribution.copyright_statement.field_label[metadataLanguage] || distribution.copyright_statement.field_label["en"]) : "Copyright notice";
                    let availability_start_date = distribution.availability_start_date ? distribution.availability_start_date.field_value : "";
                    let availability_end_date = distribution.availability_end_date ? distribution.availability_end_date.field_value : "";
                    let distribution_rights_holderArray = distribution.distribution_rights_holder || [];
                    let access_rights_label = distribution.access_rights ? (distribution.access_rights.field_label[metadataLanguage] || distribution.access_rights.field_label["en"]) : "";
                    let access_rightsArray = (distribution.access_rights && distribution.access_rights.field_value.map((accessRightsItem, access_rightsIndex) => {
                        let category_label = accessRightsItem.category_label ? accessRightsItem.category_label.field_value[metadataLanguage] || accessRightsItem.category_label.field_value[Object.keys(accessRightsItem.category_label.field_value)[0]] : "";
                        //let access_rights_statement_identifier_label = accessRightsItem.access_rights_statement_identifier ? ( accessRightsItem.access_rights_statement_identifier.field_label[metadataLanguage] || accessRightsItem.access_rights_statement_identifier.field_label["en"]) : ""; 
                        let identifier = accessRightsItem.access_rights_statement_identifier && accessRightsItem.access_rights_statement_identifier.field_value.access_rights_statement_scheme.label["en"] !== "ELG" ? accessRightsItem.access_rights_statement_identifier.field_value.access_rights_statement_scheme.label["en"] : "";//filter out elg identifiers 
                        let value = accessRightsItem.access_rights_statement_identifier ? accessRightsItem.access_rights_statement_identifier.field_value.value.field_value : "";
                        return <div key={access_rightsIndex}>
                            {category_label && <div className="padding5"><span className="info_value">{category_label}</span></div>}
                            {false && identifier && value.includes('http') && <div>
                                <a href={value} target="_blank" rel="noopener noreferrer" className="info_url"> {value} </a> ({identifier})</div>}
                            {false && identifier && !value.includes('http') && <div className="info_value">{value} ({identifier})  </div>}
                        </div>
                    })) || [];
                    return <Grid item xs={12} sm={6} md={6} key={distributionIndex}>
                        <Card className="distribution-cards">
                            <CardHeader avatar={<Avatar aria-label="distribution" className="distribution-avatar"> D </Avatar>}
                                title={<Typography variant="h3" className="title-links">Distribution</Typography>}
                                action={
                                    this.show_card_action(data, distribution, downloadLocation, access_location) ? <Accordion>
                                        <AccordionSummary
                                            expandIcon={<ExpandMoreIcon className="teal--font" />}
                                            aria-controls="panel1a-content"
                                            id="panel1a-header"
                                        >
                                            <Typography variant="h4" className="teal--font">Download</Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <div style={{ display: "flex" }}>
                                                {(this.props.isOwner || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak) || (this.props.isLegalValidator && data.management_object.status === "g") || (this.props.isMetadataValidator && data.management_object.status === "g") || (this.props.isTechnicalValidator && data.management_object.status === "g")) && <DownloadDatasetCurator data={data} distribution={distribution} />}
                                                {!(this.props.isOwner || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak) || (this.props.isLegalValidator && data.management_object.status === "g") || (this.props.isMetadataValidator && data.management_object.status === "g") || (this.props.isTechnicalValidator && data.management_object.status === "g")) && <AcceptLicence data={data} distribution={distribution} {...this.props} keycloak={keycloak} />}
                                            </div>
                                            <div>
                                                {/*downloadLocation && !["http://www.hiddenLocation.org", "Hidden value"].includes(downloadLocation) && <div className="padding5"><Typography className="bold-p--id">{downloadLocation_label} </Typography><div className="info_url"><a href={downloadLocation} target="_blank" rel="noopener noreferrer"> {downloadLocation}</a></div> </div>*/}
                                                {downloadLocation && !["http://www.hiddenLocation.org", "Hidden value"].includes(downloadLocation) && <div className="padding5"><span className="info_url"><a href={decodeURIComponent(downloadLocation)} target="blank">{<div><span><LaunchIcon className="xsmall-icon" /> </span> <span>{downloadLocation_label}</span></div>}</a></span></div>}
                                                {access_location && !["http://www.hiddenLocation.org", "Hidden value"].includes(access_location) && <div className="padding5"><span className="info_url"><a href={decodeURIComponent(access_location)} target="blank">{<div><span><LaunchIcon className="xsmall-icon" /> </span> <span>{access_location_label}</span></div>}</a></span></div>}
                                            </div>
                                            <div>
                                                {/* eslint-disable-next-line*/}
                                                {show_billing && <a href="javascript:void(0)" data-cb-type="checkout" data-cb-item-0="ILSP-MT-for-English-to-modern-Greek-EUR-Monthly" > subscribe </a>}
                                            </div>
                                        </AccordionDetails>
                                    </Accordion> : <></>
                                }
                            />
                            <CardContent>
                                {software_distribution_form && <div className="padding5"><Typography className="bold-p--id">{software_distribution_form_label} </Typography><div className="info_value">{software_distribution_form}</div> </div>}
                                {false && docker_download_location && !["http://www.hiddenLocation.org", "Hidden value"].includes(docker_download_location) && <div className="padding5"><Typography className="bold-p--id">{docker_download_location_label} </Typography><div className="info_value mt-03"><span><span className="docker_location p-03">docker pull {docker_download_location} </span> <span>(requires authentication/you have to contact the provider)</span></span> </div> </div>}
                                {
                                    docker_download_location && distribution.private_resource && (this.show_private_urls(data, keycloak) || distribution.private_resource.field_value === false) && !["http://www.hiddenLocation.org", "Hidden value"].includes(docker_download_location) && <DockerPullCommand dockerDownloadLocation={docker_download_location} />
                                }
                                {
                                    service_adapter_download_location && distribution.private_resource && (this.show_private_urls(data, keycloak) || distribution.private_resource.field_value === false) && !["http://www.hiddenLocation.org", "Hidden value"].includes(service_adapter_download_location) && <><Typography className="bold-p--id">{service_adapter_download_location_label} </Typography><DockerPullCommand dockerDownloadLocation={service_adapter_download_location} /></>
                                }
                                {executionLocation && !["http://www.hiddenLocation.org", "Hidden value"].includes(executionLocation) && <div className="padding5"><div className="info_url"><span><LaunchIcon className="xsmall-icon" /> </span> <span><a href={executionLocation} target="_blank" rel="noopener noreferrer"> {executionLocation_label}</a></span></div></div>}
                                {demo_location && !["http://www.hiddenLocation.org", "Hidden value"].includes(demo_location) && <div className="padding5"><Typography className="bold-p--id">{demo_location_label} </Typography><div className="info_url"><a href={demo_location} target="_blank" rel="noopener noreferrer"> {demo_location}</a></div></div>}
                                {package_format && <div className="padding5"><Typography className="bold-p--id">{package_format_label}</Typography><span className="info_value">{package_format}</span></div>}

                                {additional_hw_requirements && <div className="padding5"><Typography className="bold-p--id">{additional_hw_requirements_label}</Typography><div className="info_value">{additional_hw_requirements}</div> </div>}
                                {web_service_type && <div className="padding5"><Typography className="bold-p--id">{web_service_type_label} </Typography><div className="info_value">{web_service_type}</div> </div>}
                                {command && <div className="padding5"><Typography className="bold-p--id">{command_label} </Typography><div className="info_value">{command}</div> </div>}
                                {operating_systemArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{operation_system_label}</Typography><div className="info_value">{operating_systemArray.map(os => `${os} `)}</div></div>}
                                {is_described_byArray && is_described_byArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_described_by_label}</Typography><span className="info_value">{is_described_byArray.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {licenceTerms_urlArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{licenceTerms_label} </Typography><div className="info_value">{licenceTerms_urlArray.map((licence, licenceIndex) => <div key={licenceIndex}>{licence}</div>)}</div></div>}

                                {cost !== null && <div className="padding5"><Typography className="bold-p--id">{cost_label} </Typography><div className="info_values">{Intl.NumberFormat('en').format(cost)} {currency}</div> </div>}
                                {membership_institutionArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{membership_institution_label}</Typography><div className="info_value">{membership_institutionArray.map(membership_institutionItem => membership_institutionItem)}</div></div>}

                                {attribution_text && <div className="padding5"><Typography className="bold-p--id">{attribution_text_label} </Typography><div className="info_value">{attribution_text}</div> </div>}
                                {copyright_statement && <div className="padding5"><Typography className="bold-p--id">{copyright_statement_label} </Typography><div className="info_value">{copyright_statement}</div> </div>}
                                {availability_start_date && <div className="padding5"><Typography className="bold-p--id">Availability </Typography><div className="info_value">{availability_start_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(availability_start_date))} {availability_end_date && <span>-</span>} {availability_end_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(availability_end_date))}</div></div>}
                                {distribution_rights_holderArray.length > 0 && <ResourceActor data={distribution_rights_holderArray} metadataLanguage={metadataLanguage} />}
                                {access_rightsArray && access_rightsArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{access_rights_label}</Typography><span className="info_value">{access_rightsArray.map((item, index) => <div key={index}>{item}</div>)}</span></div>}

                            </CardContent>
                        </Card>
                    </Grid>
                }
                )}

            </Grid>
        </div >
    }
}