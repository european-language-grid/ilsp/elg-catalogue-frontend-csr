import React from "react";
import { Input } from "reactstrap";

export default class SelectMetadataLanguage extends React.Component {
    render() {
        const { data } = this.props;
        if (!data) {
            return <div></div>
        }
        return <div className="selectLanguage">
            {
                <span>
                    <Input className="selectLanguageInput" type="select" name="select" id="exampleSelect" onChange={this.props.selectLanguage}>
                        {Object.keys(data).map((lang, index) => <option key={index}>{lang}</option>)}
                        <option>lang2</option>
                    </Input>
                </span>
            }
        </div>
    }
}