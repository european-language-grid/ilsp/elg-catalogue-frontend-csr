import React from "react"; 
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
export default class LcrStaticElement extends React.Component {
    render() {
        const { static_elementArray, metadataLanguage } = this.props;
        if (!static_elementArray) {
            return <div></div>
        }
        return (
            <div className="padding15">
                { static_elementArray.field_value.map(function (item, Index) {
                    //let type_of_element = item.type_of_element ? (item.type_of_element.field_value[metadataLanguage] || item.type_of_element.field_value[Object.keys(item.type_of_element.field_value)[0]]) : "";
                    let type_of_element = []; 
                        (item.type_of_element && item.type_of_element.field_value.length > 0 && item.type_of_element.field_value.map((keyword, DIndex) => {
                            type_of_element.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                    }));
                    let body_partArray = item.body_part ? item.body_part.field_value.map(body => body.label[metadataLanguage]||body.label[Object.keys(body.label)[0]]) : [];
                    //let face_view = item.face_view ? (item.face_view.field_value[metadataLanguage] || item.face_view.field_value[Object.keys(item.face_view.field_value)[0]]) : "";
                    let face_view = []; 
                        (item.face_view && item.face_view.field_value.length > 0 && item.face_view.field_value.map((keyword, DIndex) => {
                            face_view.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                    }));
                    //let face_expression = item.face_expression ? (item.face_expression.field_value[metadataLanguage] || item.face_expression.field_value[Object.keys(item.face_expression.field_value)[0]]) : "";
                    let face_expression = []; 
                    (item.face_expression && item.face_expression.field_value.length > 0 && item.face_expression.field_value.map((keyword, DIndex) => {
                        face_expression.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let landscape_part = item.landscape_part ? (item.landscape_part.field_value[metadataLanguage] || item.landscape_part.field_value[Object.keys(item.landscape_part.field_value)[0]]) : "";
                    let landscape_part = []; 
                    (item.landscape_part && item.landscape_part.field_value.length > 0 && item.landscape_part.field_value.map((keyword, DIndex) => {
                        landscape_part.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let artifact_part = item.artifact_part ? (item.artifact_part.field_value[metadataLanguage] || item.artifact_part.field_value[Object.keys(item.artifact_part.field_value)[0]]) : "";
                    let artifact_part = []; 
                    (item.artifact_part && item.artifact_part.field_value.length > 0 && item.artifact_part.field_value.map((keyword, DIndex) => {
                        artifact_part.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    let person_description = item.person_description ? (item.person_description.field_value[metadataLanguage] || item.person_description.field_value[Object.keys(item.person_description.field_value)[0]]) : "";
                    let thing_description = item.thing_description ? (item.thing_description.field_value[metadataLanguage] || item.thing_description.field_value[Object.keys(item.thing_description.field_value)[0]]) : "";
                    let organization_description = item.organization_description ? (item.organization_description.field_value[metadataLanguage] || item.organization_description.field_value[Object.keys(item.organization_description.field_value)[0]]) : "";
                    let event_description = item.event_description ? (item.event_description.field_value[metadataLanguage] || item.event_description.field_value[Object.keys(item.event_description.field_value)[0]]) : "";
                    
                    let type_of_element_label= item.type_of_element ? (item.type_of_element.field_label[metadataLanguage]||item.type_of_element.field_label["en"]): "";
                    let face_view_label= item.face_view ? (item.face_view.field_label[metadataLanguage]||item.face_view.field_label["en"]): "";
                    let face_expression_label= item.face_expression ? (item.face_expression.field_label[metadataLanguage]||item.face_expression.field_label["en"]): "";
                    let body_part_label= item.body_part ? (item.body_part.field_label[metadataLanguage]||item.body_part.field_label["en"]): "";

                    let landscape_part_label= item.landscape_part ? (item.landscape_part.field_label[metadataLanguage]||item.landscape_part.field_label["en"]): "";
                    let artifact_part_label= item.artifact_part ? (item.artifact_part.field_label[metadataLanguage]||item.artifact_part.field_label["en"]): "";
                    let person_description_label= item.person_description ? (item.person_description.field_label[metadataLanguage]||item.person_description.field_label["en"]): "";
                    let thing_description_label= item.thing_description ? (item.thing_description.field_label[metadataLanguage]||item.thing_description.field_label["en"]): "";
                    let organization_description_label= item.organization_description ? (item.organization_description.field_label[metadataLanguage]||item.organization_description.field_label["en"]): "";
                    let event_description_label= item.event_description ? (item.event_description.field_label[metadataLanguage]||item.event_description.field_label["en"]): "";


                    return (
                    <div id={Index} key={Index} className="padding5">
                    {type_of_element.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{type_of_element_label}</Typography><span className="info_value">{type_of_element.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagFunction" />)}</span></div>}
                    {body_partArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{body_part_label}</Typography><span className="info_value">{body_partArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {face_view.length > 0 && <div className="padding5"><Typography className="bold-p--id">{face_view_label}</Typography><span className="info_value">{face_view.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {face_expression.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{face_expression_label}</Typography><span className="info_value">{face_expression.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {landscape_part.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{landscape_part_label}</Typography><span className="info_value">{landscape_part.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {artifact_part.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{artifact_part_label}</Typography><span className="info_value">{artifact_part.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {person_description&&<div className="padding5"><Typography className="bold-p--id">{person_description_label}</Typography><span className="info_value">{person_description}</span></div>}
                    {thing_description&&<div className="padding5"><Typography className="bold-p--id">{thing_description_label}</Typography><span className="info_value">{thing_description}</span></div>}
                    {organization_description&&<div className="padding5"><Typography className="bold-p--id">{organization_description_label}</Typography><span className="info_value">{organization_description}</span></div>}
                    {event_description&&<div className="padding5"><Typography className="bold-p--id">{event_description_label}</Typography><span className="info_value">{event_description}</span></div>}
                    
                    </div> 
                                           
                
                )

                } )
                    
                }
            </div>
        )

    }
}