import React from "react";
import Typography from '@material-ui/core/Typography';
import lcrLrSubclassParser from '../../parsers/lcrLrSubclassParser';

import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

export default class LcrMediaPart extends React.Component {

    componentDidMount() {
        const { data,  metadataLanguage, showLcrMedia } = this.props;

        const lr_subclass = data.described_entity.field_value.lr_subclass || [];           
        const encodinglevelArray = lcrLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage).value : [];
        const contenttypeArray = lcrLrSubclassParser.getContentType(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getContentType(lr_subclass, metadataLanguage).value :  [];        
        const theoretic_model = lcrLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage).value : "";
        const extratextualinformationArray = lcrLrSubclassParser.getExtratextualInformation(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getExtratextualInformation(lr_subclass, metadataLanguage).value :  [];
        const extratextualinformationunitArray = lcrLrSubclassParser.getExtratextualInformationUnit(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getExtratextualInformationUnit(lr_subclass, metadataLanguage).value :  [];

        if((encodinglevelArray ) || (contenttypeArray )||
        (lr_subclass.field_value.external_reference && lr_subclass.field_value.external_reference.field_value.length > 0) || (theoretic_model && theoretic_model.length > 0) ||
        (extratextualinformationArray ) || (extratextualinformationunitArray ) )
        {
            showLcrMedia ? void 0 : this.props.showLcrMediaFunction(true);
        }


    }

    render() {
        const { data, metadataLanguage, showLcrMedia } = this.props;

        if (!data.described_entity.field_value.lr_subclass) {
            return <div></div>
        }
        const lr_subclass = data.described_entity.field_value.lr_subclass || [];
        //const lr_type= lcrLrSubclassParser.getLrtype(lr_subclass,metadataLanguage)? lcrLrSubclassParser.getLrtype(lr_subclass,metadataLanguage).value : "";      
        const encodinglevelArray = lcrLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage).value : [];
        const contenttypeArray = lcrLrSubclassParser.getContentType(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getContentType(lr_subclass, metadataLanguage).value :  [];        
        const theoretic_model = lcrLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage).value : "";
        const extratextualinformationArray = lcrLrSubclassParser.getExtratextualInformation(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getExtratextualInformation(lr_subclass, metadataLanguage).value :  [];
        const extratextualinformationunitArray = lcrLrSubclassParser.getExtratextualInformationUnit(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getExtratextualInformationUnit(lr_subclass, metadataLanguage).value :  [];
        //const lr_type_label= lcrLrSubclassParser.getLrtype(lr_subclass,metadataLanguage)? lcrLrSubclassParser.getLrtype(lr_subclass,metadataLanguage).label : "";      
        const encoding_level_label = lcrLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage).label : "";
        const content_type_label = lcrLrSubclassParser.getContentType(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getContentType(lr_subclass, metadataLanguage).label : "";        
        const theoretic_model_label = lcrLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage).label : "";
        const extratextual_information_label = lcrLrSubclassParser.getExtratextualInformation(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getExtratextualInformation(lr_subclass, metadataLanguage).label : "";
        const extratextual_information_unit_label = lcrLrSubclassParser.getExtratextualInformationUnit(lr_subclass, metadataLanguage) ? lcrLrSubclassParser.getExtratextualInformationUnit(lr_subclass, metadataLanguage).label : "";

        const showTitle = (encodinglevelArray && encodinglevelArray.length > 0) || (contenttypeArray && contenttypeArray.length > 0) ||
        (lr_subclass.field_value.external_reference && lr_subclass.field_value.external_reference.field_value.length > 0) || (theoretic_model && theoretic_model.length > 0) ||
        (extratextualinformationArray && extratextualinformationArray.length > 0 ) || (extratextualinformationunitArray && extratextualinformationunitArray.length > 0) ;

       

        return <> {showLcrMedia ? <div className="padding15">
            {/*<div className="padding5"><Typography className="bold-p--id">{lr_type_label}</Typography><span className="info_value">{lr_type}</span></div>*/}
            {/*<div className="padding5"><Typography className="bold-p--id">lcr_subclass</Typography><span className="info_value">{lcr_subclass}</span></div>*/}
            {showTitle && <Typography variant="h3" className="title-links">LCR details</Typography>}
            {encodinglevelArray && encodinglevelArray.length > 0 &&
                <div className="padding5"><Typography className="bold-p--id">{encoding_level_label}</Typography>
                    {encodinglevelArray.map((item, encodingIndex) => {
                        return <div key={encodingIndex}>
                            <span className="info_value">{item}</span>
                        </div>
                    })} </div>}

            {contenttypeArray && contenttypeArray.length > 0 &&
                <div className="padding5"><Typography className="bold-p--id">{content_type_label}</Typography>
                    {contenttypeArray.map((item, cIndex) => {
                        return <div key={cIndex}>
                            <span className="info_value">{item}</span>
                        </div>
                    })} </div>}           

            {lr_subclass.field_value.external_reference && lr_subclass.field_value.external_reference.field_value.length > 0 &&
                <div className="padding5"><Typography className="bold-p--id">{lr_subclass.field_value.external_reference.field_label[metadataLanguage] || lr_subclass.field_value.external_reference.field_label["en"]} </Typography>
                    {lr_subclass.field_value.external_reference && lr_subclass.field_value.external_reference.field_value.map((item, Index) => {
                        let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                        if (full_metadata_record) {
                            return <div className="padding5 internal_url" key={Index}>
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span>
                                        {item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]}
                                    </span>
                                </Link>
                            </div>
                        }
                        return <div key={Index}>
                            {item.resource_name &&
                                <>
                                    <Typography className="bold-p--id">{item.resource_name.field_label[metadataLanguage] || item.resource_name.field_label["en"]}</Typography>
                                    <span className="info_value">{item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]}</span>
                                </>
                            }

                            {item.version &&
                                <>
                                    <Typography className="bold-p--id">{item.version.field_label[metadataLanguage] || item.version.field_label["en"]}</Typography>
                                    <span className="info_value">{item.version.field_value}</span>
                                </>
                            }
                            {item.lr_identifier &&
                                <Typography className="bold-p--id">{item.lr_identifier.field_label[metadataLanguage] || item.lr_identifier.field_label["en"]}</Typography>
                            }
                            {item.lr_identifier && item.lr_identifier.field_value.length > 0 && item.lr_identifier.field_value.map((lridentifier, lrIndex) => {
                                if (lridentifier.lr_identifier_scheme.label["en"] !== "ELG") {
                                    return <div key={lrIndex}>
                                        <a href={lridentifier.lr_identifier_scheme.field_value} target="blank">{lridentifier.lr_identifier_scheme.label[metadataLanguage] || lridentifier.lr_identifier_scheme.label[Object.keys(lridentifier.lr_identifier_scheme.label)[0]]}</a>
                                    </div>
                                }
                                return void 0;
                            })}
                        </div>
                    })}</div>}


            {theoretic_model && theoretic_model.length > 0 &&
                <div className="padding5"><Typography className="bold-p--id">{theoretic_model_label}</Typography>
                    {theoretic_model.map((item, tIndex) => {
                        return <div key={tIndex}>
                            <span className="info_value">{item}</span>
                        </div>
                    })} </div>}

            {extratextualinformationArray && extratextualinformationArray.length > 0 &&
                <div className="padding5"><Typography className="bold-p--id">{extratextual_information_label}</Typography>
                    {extratextualinformationArray.map((item, wIndex) => {
                        return <div key={wIndex}>
                            <span className="info_value">{item}</span>
                        </div>
                    })} </div>}

            {extratextualinformationunitArray && extratextualinformationunitArray.length > 0 &&
                <div className="padding5"><Typography className="bold-p--id">{extratextual_information_unit_label}</Typography>
                    {extratextualinformationunitArray.map((item, wIndex) => {
                        return <div key={wIndex}>
                            <span className="info_value">{item}</span>
                        </div>
                    })} </div>}

 
        </div>:<span></span>
        }
        </>

    }
}

