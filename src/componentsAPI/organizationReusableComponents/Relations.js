import React from "react";
//import Typography from '@material-ui/core/Typography';
//import ExpansionPanel from '@material-ui/core/ExpansionPanel';
//import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
//import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
//import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ReverseRelations from "./../CommonComponents/ReverseRelations";

export default class Relations extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        const { reverse_relations } = data.described_entity.field_value || [];
        const showComponent = reverse_relations;
        if (!showComponent) {
            return <div></div>
        }
        return <div className="padding15">
            
            {/*<ExpansionPanel className="FunctionBox--accordions">
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon className="white--font" />}
                    aria-controls="panel1a-content"
    id="panel1a-header" className="FunctionBox--accordions__title-background">
                    <Typography className="bold-p--id--small white--font">more</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>*/}
            <div>
                <ReverseRelations data={data} metadataLanguage={metadataLanguage} />

            </div>
            {/*</ExpansionPanelDetails>
            </ExpansionPanel>*/}
        </div>
    }
}