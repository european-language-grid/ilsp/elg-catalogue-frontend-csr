import React from "react";

import Typography from '@material-ui/core/Typography';

class OrganizationRoles extends React.Component {

    render() {
        const { rolesArray,OrganizationRoleLabel } = this.props;
        return (
            <React.Fragment>
                 
                 {rolesArray && rolesArray.length>0 && <div className="padding15">
                        <Typography className="bold-p--id">{OrganizationRoleLabel}</Typography>                      
                    
                        {rolesArray.map((keyword, index) =>
                            <div key={index} className="info_value">{keyword} </div>
                        )}
                    </div>                  
                }             

            </React.Fragment>
        );
    }

}

export default OrganizationRoles;