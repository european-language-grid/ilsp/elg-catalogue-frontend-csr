import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import HasDivisionKeywords from './HasDivisionKeywords';
import organizationHasDivisionParser from "../../parsers/organizationHasDivisionParser";
import AddressSet from './AddressSet';
import PhoneFax from './PhoneFax';
import SocialMediaComponent from './SocialMediaComponent';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import HaDivisionOrganizationBio from './HaDivisionOrganizationBio';
import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';
//import EmailsWithIcon from '../CommonComponents/EmailsWithIcon';

class HasDivision extends React.Component {

    render() {
        const { divisionArray, metadataLanguage, HasDivisionLabel } = this.props;
        if (!divisionArray) {
            return <div></div>
        }
        return (
            <React.Fragment>
                {divisionArray && divisionArray.length > 0 &&
                    <div className="padding15">
                        <Grid container spacing={2}>
                            {divisionArray.map((divisionitem, index) => {
                                let division_name = divisionitem.division_name ? divisionitem.division_name.field_value[metadataLanguage] || divisionitem.division_name.field_value[Object.keys(divisionitem.division_name.field_value)[0]] : "";
                                let division_short_name = divisionitem.division_short_name ? divisionitem.division_short_name.field_value[metadataLanguage] || divisionitem.division_short_name.field_value[Object.keys(divisionitem.division_name.field_value)[0]] : "";
                                //let division_alternative_name = divisionitem.division_alternative_name ? divisionitem.division_alternative_name.field_value[metadataLanguage] || divisionitem.division_alternative_name.field_value[Object.keys(divisionitem.division_name.field_value)[0]] : "";
                                let division_alternative_name = organizationHasDivisionParser.getDivisionAltName(divisionitem, metadataLanguage);
                                let division_category = divisionitem.division_category ? divisionitem.division_category.label[metadataLanguage] : "";
                                let country_of_registrationArray = [];
                                (divisionitem.country_of_registration && divisionitem.country_of_registration.field_value.length > 0 && divisionitem.country_of_registration.field_value.map((country, countryIndex) => {
                                    country_of_registrationArray.push(country.label[metadataLanguage]);
                                    return false;
                                }));
                                let organization_bio = organizationHasDivisionParser.getOrganizationBio(divisionitem, metadataLanguage);
                                let OrganizationBioLabel = organizationHasDivisionParser.getOrganizationBioLabel(divisionitem, metadataLanguage);
                                let logo = divisionitem.logo ? divisionitem.logo.field_value : "";
                                //let service_offeredArray = organizationHasDivisionParser.getServiceOffered(divisionitem, metadataLanguage);
                                //let disciplineArray = organizationHasDivisionParser.getDiscipline(divisionitem, metadataLanguage);
                                //let keywordArray = organizationHasDivisionParser.getKeywords(divisionitem, metadataLanguage);
                                const head_office_address_array = organizationHasDivisionParser.get_head_office_AddressSet(divisionitem);
                                const other_office_address_array = organizationHasDivisionParser.get_other_office_AddressSet(divisionitem);
                                let TelephoneNumbers = organizationHasDivisionParser.getTelephoneNumbers(divisionitem);
                                let FaxNumbers = organizationHasDivisionParser.getFaxNumbers(divisionitem);
                                let socialmedia = organizationHasDivisionParser.getSocialMedia(divisionitem);
                                //let emailArray = organizationHasDivisionParser.getDivisionEmails(divisionitem);
                                let websites = organizationHasDivisionParser.getDivisionWebsites(divisionitem);
                                let memberOfAssociationNameArr = organizationHasDivisionParser.getmemberOfAssociationName(divisionitem, metadataLanguage);
                                //labels

                                //let disciplineLabel = organizationHasDivisionParser.getDisciplineLabel(divisionitem, metadataLanguage);
                                //let KeywordLabel = organizationHasDivisionParser.getKeywordLabel(divisionitem, metadataLanguage);
                                //let AddressSetDivisionLabel = organizationHasDivisionParser.getAddressSetDivisionLabel(divisionitem, metadataLanguage);
                                const head_office_address_label = organizationHasDivisionParser.get_head_office_address_label(divisionitem, metadataLanguage);
                                const other_office_address_label = organizationHasDivisionParser.get_other_office_address_label(divisionitem, metadataLanguage);
                                let SocialMediaLabel = organizationHasDivisionParser.getSocialMediaLabel(divisionitem, metadataLanguage);
                                let DivisionWebsitesLabel = organizationHasDivisionParser.getDivisionWebsitesLabel(divisionitem, metadataLanguage);
                                //let DivisionEmailsLabel = organizationHasDivisionParser.getDivisionEmailsLabel(divisionitem, metadataLanguage);
                                let TelephoneNumbersLabel = organizationHasDivisionParser.getTelephoneNumbersLabel(divisionitem, metadataLanguage);
                                let FaxNumbersLabel = organizationHasDivisionParser.getFaxNumbersLabel(divisionitem, metadataLanguage);
                                let memberOfAssociationNameLabel = organizationHasDivisionParser.getmemberOfAssociationNameLabel(divisionitem, metadataLanguage);
                                let DivisionCategoryLabel = organizationHasDivisionParser.getDivisionCategoryLabel(divisionitem, metadataLanguage);
                                let CountryOfRegistrationLabel = organizationHasDivisionParser.getCountryOfRegistrationLabel(divisionitem, metadataLanguage);
                                //let ServiceOfferedLabel = organizationHasDivisionParser.getServiceOfferedLabel(divisionitem, metadataLanguage);
                                return <Grid item xs key={index}>
                                    <Card >
                                        <CardHeader title={<Typography variant="h3" className="title-links">{HasDivisionLabel}</Typography>}>

                                        </CardHeader>
                                        <CardContent>
                                            <Grid item xs={12} container spacing={2}>
                                                {logo && <Grid item xs={3}>
                                                    <div><img src={logo} alt="organization logo" height="52" width="52"></img></div>
                                                </Grid>}
                                                <Grid item xs={9}>
                                                    {division_name && <Typography className="info_value">{division_name}</Typography>}
                                                    {division_short_name && <Typography className="info_value">{division_short_name}</Typography>}
                                                    {/*division_alternative_name && <Typography className="info_value">{division_alternative_name}</Typography>*/}
                                                    {division_alternative_name && division_alternative_name.length>0 &&
                                                        <div className="padding15">
                                                            {division_alternative_name.map((altname, altnameIndex) =>
                                                                <span key={altnameIndex} className="info_value">{altname} </span>
                                                            )}
                                                        </div>
                                                    }

                                                </Grid>
                                                <Grid item xs={12}>
                                                    {division_category && <div className="padding5"><Typography className="bold-p--id">{DivisionCategoryLabel}</Typography><span className="info_value">{division_category}</span></div>}
                                                    {country_of_registrationArray.length > 0 && <div><Typography className="bold-p--id">{CountryOfRegistrationLabel}</Typography><span className="info_value">{country_of_registrationArray.map((item, index) => <span key={index}>{item} </span>)}</span> </div>}
                                                    {organization_bio && <div><Typography className="bold-p--id">{OrganizationBioLabel}</Typography> <HaDivisionOrganizationBio data={organization_bio} /> </div>}
                                                </Grid>
                                                <Grid item xs={12}>
                                                    {/*service_offeredArray.length && <div className="padding5"><Typography className="bold-p--id">{ServiceOfferedLabel}</Typography><span className="info_value">{service_offeredArray.map((item, index) => <span key={index}>{`${item} `}</span>)}</span></div>*/}
                                                    {/*disciplineArray.length && <div className="padding5"><Typography className="bold-p--id">{disciplineLabel}</Typography><span className="info_value">{disciplineArray.map((item, index) => <span key={index}>{`${item} `}</span>)}</span> </div>*/}
                                                    {/*keywordArray.length && <div className="padding5"><Typography className="bold-p--id">{KeywordLabel}</Typography><span className="info_value">{keywordArray.map((item, index) => <span key={index}>{`${item} `}</span>)}</span></div>*/}
                                                    <HasDivisionKeywords data={divisionitem} metadataLanguage={metadataLanguage} />
                                                    {/*<AddressSet addressArray={addressArray} metadataLanguage={metadataLanguage} AddressSetLabel={AddressSetDivisionLabel} />*/}
                                                    <AddressSet addressArray={head_office_address_array} metadataLanguage={metadataLanguage} AddressSetLabel={head_office_address_label} />
                                                    <AddressSet addressArray={other_office_address_array} metadataLanguage={metadataLanguage} AddressSetLabel={other_office_address_label} />
                                                    {(TelephoneNumbers.length > 0 || FaxNumbers.length > 0) && <div>
                                                        {/*<Typography variant="h3" className="padding15">Contact Organization</Typography>*/}
                                                        <PhoneFax TelephoneNumbersLabel={TelephoneNumbersLabel} FaxNumbersLabel={FaxNumbersLabel} telephonenumbers={TelephoneNumbers} faxnumbers={FaxNumbers} />
                                                    </div>
                                                    }

                                                    {websites && websites.length > 0 &&
                                                        <div className="padding5">
                                                            {websites.length > 1 ? <Typography className="bold-p--id">{DivisionWebsitesLabel} </Typography> : <Typography className="bold-p--id">Website </Typography>}
                                                            <WebsitesWithIcon websiteArray={websites} />
                                                        </div>
                                                    }
                                                    {/*emailArray && emailArray.length > 0 &&
                                                        <div className="padding15">
                                                            {emailArray.length > 1 ? <Typography className="bold-p--id">Emails </Typography> : <Typography className="bold-p--id">Email </Typography>}  
                                                            <EmailsWithIcon emailArray={emailArray}/>
                                                        </div>
                                                    */}
                                                    <SocialMediaComponent socialmedia={socialmedia} label={SocialMediaLabel} />
                                                    {
                                                        <div className="padding15">
                                                            {memberOfAssociationNameArr.length > 0 && <Typography variant="h3" className="title-links"> {memberOfAssociationNameLabel} </Typography>}
                                                            {memberOfAssociationNameArr.length > 0 && divisionitem.member_of_association.field_value.map((MemberOrganization, MemberOrganizationIndex) =>
                                                                <div key={MemberOrganizationIndex} id={MemberOrganizationIndex} >
                                                                    <span className="info_value">
                                                                        {organizationHasDivisionParser.getmemberOfAssociationName(divisionitem)[MemberOrganizationIndex].organization_name} </span>
                                                                </div>
                                                            )}
                                                        </div>
                                                    }
                                                </Grid>
                                            </Grid>
                                        </CardContent>
                                    </Card>
                                </Grid>

                            })
                            }
                        </Grid>
                    </div>

                }

            </React.Fragment>
        );
    }

}

export default HasDivision;