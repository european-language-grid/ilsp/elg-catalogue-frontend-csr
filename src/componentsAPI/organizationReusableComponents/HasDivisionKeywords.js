import React from "react";
import Keywords from '../CommonComponents/Keywords';

import organizationHasDivisionParser from "../../parsers/organizationHasDivisionParser";

export default class HasDivisionKeywords extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        const ltAreaKeywordsArray = organizationHasDivisionParser.getLtAreaKeywords(data, metadataLanguage);
        const domainKeywordsArray = organizationHasDivisionParser.getDomainKeywords(data, metadataLanguage);
        let DomainKeywordsLabel = organizationHasDivisionParser.getDomainKeywordsLabel(data, metadataLanguage);
        let LtAreaKeywordsLabel = organizationHasDivisionParser.getLtAreaKeywordsLabel(data, metadataLanguage);
        const disciplineArray = organizationHasDivisionParser.getDiscipline(data, metadataLanguage);
        const keywordArray = organizationHasDivisionParser.getKeywords(data, metadataLanguage);
        const service_offeredArray = organizationHasDivisionParser.getServiceOffered(data, metadataLanguage);
        const disciplineLabel = organizationHasDivisionParser.getDisciplineLabel(data, metadataLanguage);
        const KeywordLabel = organizationHasDivisionParser.getKeywordLabel(data, metadataLanguage);
        const ServiceOfferedLabel = organizationHasDivisionParser.getServiceOfferedLabel(data, metadataLanguage);
        return (
            <div className="padding15">

                <Keywords
                    keywordsArray={keywordArray} KeywordsLabel={KeywordLabel}
                    disciplineArray={disciplineArray} DisciplineKeywordsLabel={disciplineLabel}
                    domainKeywordsArray={domainKeywordsArray} DomainKeywordsLabel={DomainKeywordsLabel}
                    ltAreaKeywordsArray={ltAreaKeywordsArray} LtAreaKeywordsLabel={LtAreaKeywordsLabel}
                    servicesOfferedArray={service_offeredArray} servicesOfferedKeywordsLabel={ServiceOfferedLabel}
                    languagesArray={null}
                    subjectKeywordsArray={null}
                    intentedKeywordsArray={null}
                    grid_size={6}
                />
            </div>
        );
    }
}