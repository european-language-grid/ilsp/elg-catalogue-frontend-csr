import React from "react";
import Typography from '@material-ui/core/Typography';
import commonParser from "./../../parsers/CommonParser";
import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';
import GeneralIdentifier from '../CommonComponents/GeneralIdentifier';
import ResourceIsDivision from '../CommonComponents/ResourceIsDivision';

import { Link } from "react-router-dom";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";

class MemberOfAssociation extends React.Component {

    render() {
        const {  data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }       
        let member_of_association_label = (data.described_entity.field_value.member_of_association && data.described_entity.field_value.member_of_association.field_value.length > 0) ? 
            data.described_entity.field_value.member_of_association.field_label[metadataLanguage] || data.described_entity.field_value.member_of_association.field_label["en"] : "" ;
                
        let member_of_associationArray = data.described_entity.field_value.member_of_association && data.described_entity.field_value.member_of_association.field_value.length > 0 ?  data.described_entity.field_value.member_of_association.field_value.map((item, index) => {
            let organization_name = item.organization_name ? item.organization_name.field_value[metadataLanguage] || item.organization_name.field_value[Object.keys(item.organization_name.field_value)[0]]:"";
            let websiteArray = (item.website && item.website.field_value.length > 0) ? item.website.field_value : [];
             
            let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
            return <div key={index}>
               {full_metadata_record ?
                        <div className="padding5 internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span className="info_value">{organization_name} </span>
                            </Link>
                            <GeneralIdentifier data={item} identifier_name={"organization_identifier"} identifier_scheme={"organization_identifier_scheme"} metadataLanguage={metadataLanguage} />                            
                        </div> : <div>
                        <span className="info_value">{organization_name}</span>
                        <GeneralIdentifier data={item} identifier_name={item.organization_identifier} identifier_scheme={item.organization_identifier_scheme} metadataLanguage={metadataLanguage} />                         
                        </div>
                }    
                {websiteArray && websiteArray.length > 0 &&  <WebsitesWithIcon websiteArray={websiteArray} />} 
                {item.is_division_of && item.is_division_of.field_value.length>0 && <ResourceIsDivision data={item.is_division_of} metadataLanguage={metadataLanguage} /> }
            </div>
        }) : [];


 
        
        
        return (
            <>
              
                {member_of_associationArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{member_of_association_label}</Typography><ul className="info_value">{member_of_associationArray.map((item, index) => <li key={index}>{item} </li>)}</ul></div>}
                                
            </>
        );
    }

}

export default MemberOfAssociation;