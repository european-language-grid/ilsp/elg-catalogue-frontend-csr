import React from "react";

import Typography from '@material-ui/core/Typography';

class PhoneFax extends React.Component {

    render() {
        const { telephonenumbers,faxnumbers,TelephoneNumbersLabel ,FaxNumbersLabel} = this.props;



        return (
            <React.Fragment>               
                
                {telephonenumbers && telephonenumbers.length>0 && 
                <div className="padding15">
                    <Typography className="bold-p--id">{TelephoneNumbersLabel}</Typography>
                 { telephonenumbers.map((phone, index) =>
                 <div key={index} className="info_value"> {phone} </div>
                 )}
                  </div>
                  }

                {faxnumbers && faxnumbers.length>0 && 
                <div className="padding15">
                    <Typography className="bold-p--id">{FaxNumbersLabel}</Typography>
                 { faxnumbers.map((fax, index) =>
                 <div key={index} className="info_value"> {fax} </div>
                 )}
                  </div>
                  }

                 
            </React.Fragment>
        );
    }

}

export default PhoneFax;