import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
//import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
//import InboxIcon from '@material-ui/icons/MoveToInbox';
//import DraftsIcon from '@material-ui/icons/Drafts';
//import SendIcon from '@material-ui/icons/Send';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
//import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import EditIcon from '@material-ui/icons/Edit';
//import GetAppIcon from '@material-ui/icons/GetApp';
import LibraryAddIcon from '@material-ui/icons/LibraryAdd';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';




export default class OrganizationActions extends React.Component {
    constructor(props) {
        super(props);
        this.state = { "anchorEl": null }
        this.handleClick = this.handleClick.bind(this);
        this.handleEditMetadata = this.handleEditMetadata.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleClaimMetadata = this.handleClaimMetadata.bind(this);
        this.handleValidateMetadata = this.handleValidateMetadata.bind(this);
    }

    handleClick(event) {
        this.setState({ "anchorEl": event.currentTarget });
    }

    handleClose() {
        this.setState({ "anchorEl": null });
    }

    handleEditMetadata() {
        console.log("send button pressed");
        this.handleClose();
    }

    handleClaimMetadata() {
        console.log("claim button pressed");
        this.handleClose();
    }

    handleValidateMetadata() {
        console.log("validate button pressed");
        this.handleClose();
    }

    render() {
        return <div>
            <Button classes={{ root: 'inner-link-outlined--teal' }} aria-controls="simple-menu" aria-haspopup="true" endIcon={<ArrowDropDownIcon />} onClick={this.handleClick}>
                Actions
      </Button>
            <Menu id="customized-menu" keepMounted anchorEl={this.state.anchorEl} open={Boolean(this.state.anchorEl)} onClose={this.handleClose}>
                <MenuItem onClick={this.handleEditMetadata}>
                    <ListItem button >
                        <ListItemAvatar>
                            <Avatar className="ActionsAvatar">
                                <EditIcon className="ActionsIcon" />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Edit Metadata" secondary="Sign in to Edit" className="ActionButtonText" />
                    </ListItem>
                </MenuItem>
                <MenuItem onClick={this.handleClaimMetadata}>
                    <ListItem button>
                        <ListItemAvatar>
                            <Avatar className="ActionsAvatar">
                                <LibraryAddIcon className="ActionsIcon" />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Claim Metadata" secondary="Sign in to claim" className="ActionButtonText" />
                    </ListItem>
                </MenuItem>
                <MenuItem onClick={this.handleValidateMetadata}>
                    <ListItem button>
                        <ListItemAvatar>
                            <Avatar className="ActionsAvatar">
                                <PlaylistAddCheckIcon className="ActionsIcon" />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary="Validate Metadata" secondary="Validate the metadata Record" className="ActionButtonText" />
                    </ListItem>
                </MenuItem>
            </Menu>
        </div >
    }
}
