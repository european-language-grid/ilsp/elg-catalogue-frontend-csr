import React from "react";

import Typography from '@material-ui/core/Typography';

class HaDivisionOrganizationBio extends React.Component {

    constructor() {       
        super();
        this.state = { expanded: false };
        this.togglexpanded = this.togglexpanded.bind(this);       
    }


    togglexpanded() {
        this.setState({ expanded: !this.state.expanded })
    }

    
    render() {
        const { data} = this.props;
        const { expanded } = this.state;


        return (
            <React.Fragment>                
                                                              
                     {expanded ? (
                                    <div>
                                        <Typography variant="body2" className="search-results__description">

                                            {data} <span className="ExpandButton" onClick={this.togglexpanded} > Read Less </span>
                                        </Typography>
                                    </div>
                                ) : (
                                        <div>
                                            <Typography variant="body2" className="search-results__description">
                                                {data.substring(0, 120)}{data.length > 120 ? <span className="ExpandButton" onClick={this.togglexpanded} > ... Read More </span> : void 0} </Typography>
                                        </div>
                                    )
                    }

            </React.Fragment>
        );
    }

}

export default HaDivisionOrganizationBio;