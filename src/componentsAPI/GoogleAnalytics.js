import React from "react";
import { Cookies } from "react-cookie-consent";
import ReactGA from 'react-ga';
import { GoogleAnalyticsCallback } from "../config/constants";
export default class GoogleAnalytics extends React.Component {
    componentDidMount() {
        GoogleAnalyticsCallback(Cookies, ReactGA);
    }
    render() {
        return <div>
        </div>
    }
}