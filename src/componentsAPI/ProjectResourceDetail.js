import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
//import { ReactComponent as BackIcon } from "./../assets/elg-icons/navigation-arrows-left-1.svg";
import GoToCatalogue from "./CommonComponents/GoToCatalogue";
import ProjectDocument from './ProjectReusableComponents/ProjectDocument';
import ResourceHeader from './CommonComponents/ResourceHeader';
import GenericProject from "./ProjectReusableComponents/GenericProject";
import ResourceLifeCyrcleInfo from "./CommonComponents/ResourceLifeCyrcleInfo";
import ProjectInfo from './ProjectReusableComponents/ProjectInfo';
import ProjectInfopartb from './ProjectReusableComponents/ProjectInfopartb';
import IsReplacedWith from './ProjectReusableComponents/IsReplacedWith';
import ProjectImage from "../assets/elg-icons/human-resources-team-settings.svg";
import Keywords from './CommonComponents/Keywords';
import projectParser from '../parsers/projectParser';
import ProjectCoordinator from './ProjectReusableComponents/ProjectCoordinator';
import ProjectParticipatingOrganization from './ProjectReusableComponents/ProjectParticipatingOrganization';
import ProjectFunding from './ProjectReusableComponents/ProjectFunding';
import ProjectChart from './ProjectReusableComponents/ProjectChart';
import DescriptionRichText from './CommonComponents/DescriptionRichText';
import NavigationTabs from "./organizationReusableComponents/NavigationTabs";
import Relations from "./organizationReusableComponents/Relations";
import ProgressBar from "./CommonComponents/ProgressBar";
import { TabContent, TabPane } from "reactstrap";
//import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import Container from '@material-ui/core/Container';
import commonParser from "../parsers/CommonParser";
import ExportMetadata from "./CommonComponents/ExportMetadata";
//import ShareMetadata from "./CommonComponents/ShareMetadata";
//import classnames from 'classnames';
import HelmetMetaData from "./CommonComponents/HelmetMetaData";
import { SERVER_API_METADATARECORD, RETRIEVE_RECORD_INFO, getLogoutUrl } from "../config/constants";
import RecordStats from "./CommonComponents/RecordStats";

export default class ProjectResourceDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', metadataLanguage: '', tab: '1', expanded: false, pk: "",
            loading: true, number_of_failed_loads: 0,
            isOwner: false, isLegalValidator: false, isMetadataValidator: false, isTechnicalValidator: false, source: null
        };
    }


    componentDidMount() {
        this.getMetadataResourceDetail();
        this.setState({ number_of_failed_loads: 0 });
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        clearTimeout(this.timeOutId);
    }

    updateRecord = () => {
        this.getMetadataResourceDetail();
    }

    retrieveRecordInfo = (id, source) => {
        if (this.props.keycloak && this.props.keycloak.authenticated) {
            axios.get(RETRIEVE_RECORD_INFO(id), { cancelToken: source.token })
                .then(res => {
                    const { curator = false, legal_validator = false, metadata_validator = false, technical_validator = false } = res.data;
                    this.setState({ isOwner: curator, isLegalValidator: legal_validator, isMetadataValidator: metadata_validator, isTechnicalValidator: technical_validator });
                })
                .catch(err => console.log(err));
        }
    }

    getMetadataResourceDetail = () => {
        const id = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const url = SERVER_API_METADATARECORD(id);
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        this.retrieveRecordInfo(id, source);
        axios.get(url).then(res => {
            this.setState(res);
            const lang = Object.keys(res.data.described_entity.field_value.project_name.field_value).includes("en") ? "en" : Object.keys(res.data.described_entity.field_value.project_name.field_value)[0];
            this.setState({ metadataLanguage: lang, pk: id, source: null, loading: false, number_of_failed_loads: 0 });
        }).catch(err => {
            console.log(err);
            this.setState({ source: null, loading: false });
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const thisId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const prevId = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.id;
        if (thisId !== prevId) {
            this.setState({ data: '', metadataLanguage: '', tab: '1', expanded: false });
            this.getMetadataResourceDetail();
        }
    }

    selectLanguage = (event) => {
        this.setState({ metadataLanguage: event.target.value });
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }
    back = () => {
        this.props.history.goBack();
    }

    reloadPage = () => {
        const number_of_failed_loads = this.state.number_of_failed_loads;
        if (number_of_failed_loads < 3) {
            this.setState({ number_of_failed_loads: number_of_failed_loads + 1 });
            this.getMetadataResourceDetail();
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        } else {
            this.timeOutId = setTimeout(() => { window.location = getLogoutUrl() }, 5000)
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        }
    }

    render() {
        if (this.props.keycloak && !this.props.keycloak.authenticated) {
            if (commonParser.redirectUnregisteredUsersToKeycloak(this.props.location)) {
                this.props.keycloak.login();
            }
        }

        if (this.state.loading) {
            return <ProgressBar />
        } else {
            if (this.state.data) {
            } else {
                return this.reloadPage();
            }
        }

        const { data, metadataLanguage } = this.state;

        //const languages = sessionStorage.getItem("languages") ? sessionStorage.getItem("languages").split(",") : [];
        //const licences = sessionStorage.getItem("licences") ? sessionStorage.getItem("licences").split(",") : [];
        const short_name = projectParser.getProjectShortName(data, metadataLanguage);
        const logo = projectParser.getLogo(data);
        const summary = projectParser.getSummary(data, metadataLanguage);
        const keywordsArray = projectParser.getKeywords(data, metadataLanguage);
        const domainKeywordsArray = projectParser.getDomainKeywords(data, metadataLanguage);
        const subjectKeywordsArray = projectParser.getSubjectKeywords(data, metadataLanguage);
        const ltAreaKeywordsArray = projectParser.getLtAreaKeywords(data);
        const resourceName = projectParser.getProjectName(data, metadataLanguage);
        const entity_type = data.described_entity.field_value.entity_type ? data.described_entity.field_value.entity_type.field_value : {};

        const KeywordsLabel = projectParser.getKeywordsLabel(data, metadataLanguage);
        const DomainKeywordsLabel = projectParser.getDomainKeywordsLabel(data, metadataLanguage);
        const subjectKeywordsLabel = projectParser.getSubjectKeywordsLabel(data, metadataLanguage);
        const LtAreaKeywordsLabel = projectParser.getLtAreaKeywordsLabel(data);
        const { reverse_relations } = data.described_entity.field_value || [];
        const { for_information_only = false } = data.management_object || {};
        const { tombstone = false } = data.management_object || {};

        return (
            <div>
                <HelmetMetaData data={data}
                    resourceName={resourceName}
                    description={summary}
                    keywords={keywordsArray}
                    domainKeywords={domainKeywordsArray}
                    subjectKeywords={subjectKeywordsArray}
                    intentedKeywords={null}
                    corpus_subclass={null}
                    languages={null}
                    resourceShortName={short_name}
                    for_information_only={for_information_only}
                    ltAreaKeywordsArray={ltAreaKeywordsArray}
                    disciplines={null}
                    servicesOffered={null}
                    OrganizationRolesArray={null}
                    ld_subclass={null}
                    lcr_subclass={null}
                    type="project"
                    pk={this.state.pk}
                />


                <div className="search-top">
                    <GoToCatalogue />
                </div>
                <Container maxWidth="xl">
                    <div className="metadata-main-card-container">
                        <ResourceLifeCyrcleInfo key={data.management_object.status} status={data.management_object.status} keycloak={this.props.keycloak} />
                        <ResourceHeader
                            key={data.management_object ? 'header' + JSON.stringify(data.management_object) : 'header' + this.state.pk}
                            {...this.state}
                            logo={logo}
                            title={resourceName}
                            short_name={short_name}
                            resource_type={null}
                            entity_type={entity_type}
                            version={null}
                            version_Date={null}
                            proxied={null}
                            shortnameArray={null} default_image={ProjectImage} under_construction={false} for_information_only={for_information_only} keycloak={this.props.keycloak}
                            data={data}
                            pk={this.state.pk}
                            updateRecord={this.updateRecord} tombstone={tombstone}
                            keywordsArray={null} KeywordsLabel={KeywordsLabel}
                            disciplineArray={null}
                            domainKeywordsArray={null} DomainKeywordsLabel={null}
                            ltAreaKeywordsArray={null} LtAreaKeywordsLabel={null}
                            servicesOfferedArray={null} languagesArray={null}
                            subjectKeywordsArray={null} subjectKeywordsLabel={null}
                            intentedKeywordsArray={null} lcr_subclass={null}
                            corpus_subclass={null} ld_subclass={null} description={summary} url_type="project" status={data.management_object.status} />
                    </div>
                    {(!tombstone || tombstone === "F") &&
                        <div className="tab-pane-container">
                            <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} reverse_relations={reverse_relations} />
                            <TabContent activeTab={this.state.tab}>
                                <TabPane tabId="1">
                                    <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
                                        <Grid item xs={12} sm={8} md={8} className="search-results-main" >
                                            {/*} <div className="selectLanguage">
                                                {
                                                    <>
                                                        <Input className="selectLanguageInput" type="select" name="select" id="exampleSelect" onChange={this.selectLanguage}>
                                                            {Object.keys(this.state.data.described_entity.project_summary).map((lang, index) => <option key={index}>{lang}</option>)}
                                                        </Input>
                                                    </>
                                                }
                                            </div>*/}


                                            <DescriptionRichText description={summary} />
                                            <Keywords keywordsArray={keywordsArray} KeywordsLabel={KeywordsLabel}
                                                disciplineArray={null}
                                                domainKeywordsArray={domainKeywordsArray} DomainKeywordsLabel={DomainKeywordsLabel}
                                                ltAreaKeywordsArray={ltAreaKeywordsArray} LtAreaKeywordsLabel={LtAreaKeywordsLabel}
                                                servicesOfferedArray={null} languagesArray={null}
                                                subjectKeywordsArray={subjectKeywordsArray} subjectKeywordsLabel={subjectKeywordsLabel}
                                                intentedKeywordsArray={null} lcr_subclass={null}
                                                corpus_subclass={null} ld_subclass={null} />


                                            {(this.state.data.described_entity.field_value.coordinator ||
                                                (this.state.data.described_entity.field_value.participating_organization && this.state.data.described_entity.field_value.participating_organization.field_value.length > 0) ||
                                                (this.state.data.described_entity.field_value.is_related_to_document && this.state.data.described_entity.field_value.is_related_to_document.field_value.length > 0)) &&
                                                <div className="MetaDataDetailsBottomContainer">
                                                    <div className="MetaDataDetailsBottomContainer--inner">


                                                        <ProjectCoordinator described_entity={this.state.data.described_entity.field_value} metadataLanguage={this.state.metadataLanguage} />

                                                        <ProjectParticipatingOrganization described_entity={this.state.data.described_entity.field_value} metadataLanguage={this.state.metadataLanguage} />

                                                        <ProjectDocument described_entity={this.state.data.described_entity.field_value} metadataLanguage={this.state.metadataLanguage} />



                                                        {/*<Relations data={data} metadataLanguage={metadataLanguage} />*/}
                                                    </div>
                                                </div>
                                            }
                                        </Grid>

                                        <Grid item xs={12} sm={4} md={4} className="MetadataSidebar" >


                                            {/*data.management_object.status === "p" && <ShareMetadata shareUrl={`${BASE_URL}catalogue/project/${this.state.pk}`} title={resourceName} description={summary} logo={logo} />*/}
                                            <RecordStats pk={this.state.pk} />
                                            {(data.described_entity.field_value.project_alternative_name || data.described_entity.field_value.is_replaced_with || data.described_entity.field_value.website || data.described_entity.field_value.email || data.described_entity.field_value.social_media_occupational_account ||
                                                data.described_entity.field_value.project_start_date || data.described_entity.field_value.project_end_date || data.described_entity.field_value.replaces_project) &&
                                                <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
                                                    <div className="padding15">

                                                        <IsReplacedWith data={this.state.data} metadataLanguage={metadataLanguage} />

                                                        <GenericProject data={this.state.data.described_entity.field_value.replaces_project} metadataLanguage={metadataLanguage} />

                                                        < ProjectInfo described_entity={this.state.data.described_entity.field_value} metadataLanguage={this.state.metadataLanguage} />
                                                    </div>

                                                </div>}
                                            {(data.described_entity.field_value.grant_number || data.described_entity.field_value.status || data.described_entity.field_value.related_call || data.described_entity.field_value.related_programme || data.described_entity.field_value.related_subprogramme
                                                || data.described_entity.field_value.funder || data.described_entity.field_value.funding_type || data.described_entity.field_value.funding_scheme_category || data.described_entity.field_value.funding_country
                                                || data.described_entity.field_value.cost || data.described_entity.field_value.ec_max_contribution || data.described_entity.field_value.national_max_contribution) &&

                                                <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
                                                    <div className="padding15">
                                                        <ProjectFunding described_entity={this.state.data.described_entity.field_value} metadataLanguage={this.state.metadataLanguage} />
                                                        <ProjectInfopartb described_entity={this.state.data.described_entity.field_value} metadataLanguage={this.state.metadataLanguage} identifier_name={"project_identifier"} identifier_scheme={"project_identifier_scheme"} />
                                                        <ProjectChart described_entity={this.state.data.described_entity.field_value} metadataLanguage={this.state.metadataLanguage} />
                                                    </div>
                                                </div>
                                            }
                                            <ExportMetadata pk={this.state.pk} under_construction={false} for_information_only={for_information_only} keycloak={this.props.keycloak} data={this.state.data} name={resourceName} {...this.state} />

                                            { /* <div>
                                                    <Typography variant="h6"> Funded Resources </Typography>
                                                    {this.state.data.described_entity.funded_lrs.map((fund, fundIndex) =>
                                                        <div key={fundIndex} id={fundIndex} className="actions-content">
                                                            <img alt="img" src={ResourceImage} title="Resource" />
                                                            <span className="info_url"
                                                                onClick={() => this.navigateToResourceDetail(fund.metadata_id)}>
                                                                {fund.name[Object.keys(fund.name)[0]]} </span>

                                                        </div>

                                                    )}
                                                    </div>*/}

                                        </Grid>
                                    </Grid>
                                </TabPane>

                                {reverse_relations && <TabPane tabId="2">
                                    <Relations data={data} metadataLanguage={metadataLanguage} />
                                </TabPane>
                                }
                            </TabContent>
                        </div>}

                </Container>
            </div >
        )
    }
}
