import React from "react";
import axios from "axios";
import { FormattedMessage } from 'react-intl';
import { Link } from "react-router-dom";
//import {FormattedHTMLMessage} from 'react-intl';
//import LanguageIcon from '@material-ui/icons/Language';
import ResourceComponentList from "./ResourceComponentList";
import ResourceComponentCard from "./ResourceComponentCard";
//import BtnGroup from './BtnGroup';
import FacetsComponentAccordion from "./FacetsComponentAccordion2";
import { Input } from "reactstrap";
//import SearchIcon from '@material-ui/icons/Search';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ReorderIcon from '@material-ui/icons/Reorder';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import { ReactComponent as InfoIcon } from "./../assets/images/question-circle.svg";
import Popover from '@material-ui/core/Popover';
import { Helmet } from "react-helmet";
import { /*SERVER_API_URL,*/ searchFacetUrl, searchKeywordCombineWithFacetUrl, BASE_URL } from "../config/constants";
//import {searchUrl} from "../config/constants";
//import { renderStatic } from "react-helmet";
//import { Helmet } from "react-helmet";
import { Cookies } from "react-cookie-consent";
import ReactGA from 'react-ga';
import { GoogleAnalyticsCallback } from "../config/constants";
import queryString from 'query-string';
import CircularProgress from '@material-ui/core/CircularProgress';
import SelectedFacetsTags from "./SelectedFacetsTags";
import SortComponent from "./SortComponent";
import Pagination from '@material-ui/lab/Pagination';
import PaginationItem from '@material-ui/lab/PaginationItem';
//import ProgressBar from "./CommonComponents/ProgressBar";

class SearchCatalogue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', width: window.innerWidth, searchKeyword: this.getInitialSearchKeyword(),
            facetSearchKeyword: '', listView: true, gridView: false, selectedFacets: [], keycloak: props.keycloak,
            source: null, loading: false, anchorEl: null, sortBy: ""
        };
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
        if (this.props.match.params && this.props.match.params.term) {
            let keywordSearch = this.props.match.params.term;
            this.getKeywordBasedSearchResults(keywordSearch);
        } else {
            this.getAllResults();
        }
        this.initializeGridListView();
        GoogleAnalyticsCallback(Cookies, ReactGA);
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    componentDidUpdate(prevProps, prevState) {
        let keyword1 = this.props && this.props.match && this.props.match.params && this.props.match.params.term;
        let keyword2 = prevProps.match && prevProps.match.params && prevProps.match.params.term;
        let search1 = this.props && this.props.location && this.props.location.search;
        let search2 = prevProps && prevProps.location && prevProps.location.search;
        if (keyword1 !== keyword2 || search1 !== search2) {
            GoogleAnalyticsCallback(Cookies, ReactGA);
            this.getKeywordBasedSearchResults(keyword1);
            return;
        }
        if (keyword1 !== keyword2) {
            if (keyword1) {
                this.getKeywordBasedSearchResults(keyword1);
            } else {
                this.getAllResults();
            }
        }
    }

    initializeGridListView = () => {
        const view = sessionStorage.getItem('listView');
        if (view === 'list') {
            this.handleList();
        } else if (view === 'grid') {
            this.handleGrid();
        }
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    getInitialSearchKeyword = () => {
        if (this.props.match.params && this.props.match.params.term) {
            return this.props.match.params.term;
        } else {
            return '';
        }
    }

    handleList = () => {
        this.setState({ listView: true, gridView: false });
        sessionStorage.setItem('listView', 'list');
    };

    handleGrid = () => {
        this.setState({ listView: false, gridView: true });
        sessionStorage.setItem('listView', 'grid');
    };

    getAllResults = () => {
        this.getKeywordBasedSearchResults(null);
    }

    getKeywordBasedSearchResults = (keyword) => {
        try {
            let facetSearchFromURL = this.props.location ? this.props.location.search : "";
            const parsed = queryString.parse(facetSearchFromURL);
            if (facetSearchFromURL) {
                facetSearchFromURL = facetSearchFromURL.substring(facetSearchFromURL.indexOf("?") + 1);
            }
            if (Object.keys(parsed).length > 0) {
                const arrayWithFacetValues = [];
                for (const [key, value] of Object.entries(parsed)) {
                    if (key === "page") {
                        if (!Number.isInteger(Number(value)) || Number(value) <= 0) {
                            this.props.history.push("/");
                            return;
                        }
                        continue;
                    } else if (key === "ordering") {
                        this.setState({ sortBy: key + "=" + value });
                        continue;
                    }
                    if (Array.isArray(value)) {
                        for (let valueIndex = 0; valueIndex < value.length; valueIndex++) {
                            if (key === "function__term") {
                                arrayWithFacetValues.push(decodeURIComponent("function__term::__::" + value[valueIndex]));
                            } else if (key === "function_categories__term") {
                                arrayWithFacetValues.push(decodeURIComponent("function_categories__term::__::" + value[valueIndex]));
                            } else {
                                arrayWithFacetValues.push(decodeURIComponent(value[valueIndex]));
                            }
                        }
                    } else if (key === "function__term") {
                        arrayWithFacetValues.push("function__term::__::" + value);
                    } else if (key === "function_categories__term") {
                        arrayWithFacetValues.push(decodeURIComponent("function_categories__term::__::" + value));
                    } else {
                        arrayWithFacetValues.push(decodeURIComponent(value));
                    }
                }
                this.setState({ facetSearchKeyword: facetSearchFromURL, selectedFacets: arrayWithFacetValues });
            } else {
                this.setState({ searchKeyword: keyword ? keyword : "", facetSearchKeyword: "", selectedFacets: [] });
            }
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            if (facetSearchFromURL) {//make complex query with facets included
                axios.get(searchKeywordCombineWithFacetUrl(keyword || "", facetSearchFromURL), { cancelToken: source.token })
                    .then(res => {
                        this.setState({ data: res.data, searchKeyword: keyword || "", facetSearchKeyword: facetSearchFromURL, source: null, loading: false });
                    }).catch(err => { this.setState({ source: null, loading: false }); });
            } else {
                axios.get(searchKeywordCombineWithFacetUrl(keyword || "", ""), { cancelToken: source.token })
                    .then(res => {
                        this.setState({ data: res.data, searchKeyword: keyword || "", facetSearchKeyword: "", selectedFacets: [], source: null, loading: false });
                    }).catch(err => { this.setState({ source: null, loading: false }); });
            }
        } catch (err) {
            console.log(err);
        }
    }

    getNextPage = () => {
        if (!this.state.data.next) {
            return;
        }
        try {
            const parsed = queryString.parse(this.props.location.search)
            let pageNumber = parsed.page || 1;
            if (pageNumber === "0") {
                pageNumber = Number(1);
            } else {
                pageNumber = Number(pageNumber);
            }
            pageNumber += 1;
            pageNumber = pageNumber <= 0 ? 1 : pageNumber;
            let value = "page=" + pageNumber;
            let previousfacetSearchKeyword = this.state.facetSearchKeyword;
            previousfacetSearchKeyword = this.clearPage(previousfacetSearchKeyword);
            if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) === -1) {
                value = `${previousfacetSearchKeyword}&${value}`;
            } else if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) !== -1) {
                value = `${previousfacetSearchKeyword}`;//value is already in url, do not append it and do not clear the previous facets
            }
            const url = `?${value}`;
            return url;
        } catch (error) {
            console.log(error);
        }
    }

    getPreviousPage = () => {
        if (!this.state.data.previous) {
            return;
        }
        try {
            const parsed = queryString.parse(this.props.location.search)
            let pageNumber = parsed.page || 1;
            if (pageNumber === "0") {
                pageNumber = Number(1);
            } else {
                pageNumber = Number(pageNumber);
            }
            pageNumber -= 1;
            pageNumber = pageNumber <= 0 ? 1 : pageNumber;
            let value = "page=" + pageNumber;
            let previousfacetSearchKeyword = this.state.facetSearchKeyword;
            previousfacetSearchKeyword = this.clearPage(previousfacetSearchKeyword);
            if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) === -1) {
                value = `${previousfacetSearchKeyword}&${value}`;
            } else if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) !== -1) {
                value = `${previousfacetSearchKeyword}`;//value is already in url, do not append it and do not clear the previous facets
            }
            const url = `?${value}`;
            return url;
        } catch (error) {
            console.log(error);
        }
    }

    getUrlFromPagination = (pageNumber) => {
        try {
            let value = "page=" + pageNumber;
            let previousfacetSearchKeyword = this.state.facetSearchKeyword;
            previousfacetSearchKeyword = this.clearPage(previousfacetSearchKeyword);
            if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) === -1) {
                value = `${previousfacetSearchKeyword}&${value}`;
            } else if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) !== -1) {
                value = `${previousfacetSearchKeyword}`;//value is already in url, do not append it and do not clear the previous facets
            }
            const url = `?${value}`;
            return url;
        } catch (error) {
            console.log(error);
        }
    }

    search = (event) => {
        event && event.preventDefault();
        if (!this.state.searchKeyword) {
            this.props.history.push(`/search/`);
            //this.getAllResults();
            this.clearAllSearchFilters();
            return false;
        }
        if (this.state.searchKeyword && this.state.searchKeyword.length === 0) {
            this.props.history.push(`/search/`);
            this.getAllResults();
            return false;
        }
        let previousfacetSearchKeyword = this.state.facetSearchKeyword;
        if (this.state.facetSearchKeyword) {
            previousfacetSearchKeyword = this.clearPage(previousfacetSearchKeyword);
            this.props.history.push(`/search/${encodeURIComponent(this.state.searchKeyword)}?${previousfacetSearchKeyword}`);
        } else {
            this.props.history.push(`/search/${encodeURIComponent(this.state.searchKeyword)}`);
        }
        this.getKeywordBasedSearchResults(this.state.searchKeyword)
        return false;
    }


    handleChange = (event) => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const searchTerm = event.target.value;
        this.setState({ searchKeyword: searchTerm });
        if (searchTerm.length === 0) {
            this.clearAllSearchFilters();
            return false;
        }
    }

    clearPage = (previousfacetSearchKeyword) => {
        if (previousfacetSearchKeyword.indexOf("page") >= 0) {
            if (previousfacetSearchKeyword.indexOf("&" >= 0)) {
                const parts = previousfacetSearchKeyword.split("&");
                let valueToReturn = "";
                for (let i = 0; i < parts.length; i++) {
                    const part = parts[i];
                    if (part.indexOf("page=") >= 0) {
                        continue;
                    } else {
                        valueToReturn += part + "&";
                    }
                }
                valueToReturn = valueToReturn.substring(0, valueToReturn.lastIndexOf("&"));
                return valueToReturn;
            } else {
                return previousfacetSearchKeyword;
            }
        } else {
            return previousfacetSearchKeyword;
        }
    }

    handleFacetChange = (value) => {
        if (value.length === 0) {
            this.getAllResults();
            return false;
        }
        value = value.split("=")[0] + "=" + encodeURIComponent(value.split("=")[1]);
        let previousfacetSearchKeyword = this.state.facetSearchKeyword;
        previousfacetSearchKeyword = this.clearPage(previousfacetSearchKeyword) || "";
        const previousFacetsSet = new Set();
        previousfacetSearchKeyword.split("&").forEach(item => previousFacetsSet.add(item));//& should be encoded for value terms
        if (!previousFacetsSet.has(value)) {
            value = `${previousfacetSearchKeyword}&${value}`;
            this.setState({ facetSearchKeyword: value });
        } else if (previousFacetsSet.has(value)) {
            value = `${previousfacetSearchKeyword}`;//value is already in url, do not append it and do not clear the previous facets
            this.setState({ facetSearchKeyword: value });
        } else if (previousfacetSearchKeyword === "") {
            this.setState({ facetSearchKeyword: value });
        }
        this.props.history.push(`?${value}`);
        return false;
    }

    unselectFacet = (value2Unselect) => {
        let previousfacetSearchKeyword = this.state.facetSearchKeyword;
        const parsed = queryString.parse(previousfacetSearchKeyword);
        const [key, value] = value2Unselect.split("=");
        if (parsed[key]) {
            if (Array.isArray(parsed[key])) {
                const index = parsed[key].indexOf(value);
                parsed[key].splice(index, 1);
                if (parsed[key].length === 0) {
                    delete parsed[key]
                }
            } else {
                delete parsed[key]

            }
        }
        let newSearchValue = '';
        if (Object.keys(parsed).length > 0) {
            for (const [key, value] of Object.entries(parsed)) {
                if (key === "page") {
                    continue;
                }
                if (Array.isArray(value)) {
                    for (let valueIndex = 0; valueIndex < value.length; valueIndex++) {
                        newSearchValue += key + "=" + encodeURIComponent(value[valueIndex]) + "&"
                    }
                } else {
                    newSearchValue += key + "=" + encodeURIComponent(value) + "&"
                }
            }
            if (newSearchValue.endsWith("&")) {
                newSearchValue = newSearchValue.substring(0, newSearchValue.lastIndexOf("&"));
            }
        }
        this.props.history.push(`?${newSearchValue}`);
        return false;
    }

    languageSelection = (event) => {
        const locale = event.target.value;
        this.props.setLocale(locale)
    }

    clearAllSearchFilters = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        this.setState({ searchKeyword: "", facetSearchKeyword: "", selectedFacets: [] });
        this.props.history.push(`/`);
        this.clearSortedBy();
    }

    markFacetAsSelected = (facetName) => {
        if (this.state.selectedFacets.indexOf(facetName) === -1) {//checks if i have allready selected the facet, if not add it to array
            this.setState(prevState => ({ selectedFacets: [...prevState.selectedFacets, facetName] }));
        }
    }

    handleUnselectedFacet = (index) => {
        const selectedArray = [...this.state.selectedFacets];
        selectedArray.splice(index, 1);
        this.setState({ selectedFacets: selectedArray });
    }

    handleClick = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: false });
    }

    getPages = (nextPageUrl, previousPageUrl) => {
        let previousPage = null;
        let nextPage = null;
        let currentPage = null;
        try {
            const parsedPrevious = queryString.parse(previousPageUrl);
            const parsedNext = queryString.parse(nextPageUrl);
            if (parsedPrevious) {
                previousPage = Number(parsedPrevious["page"]);
            }
            if (parsedNext) {
                nextPage = Number(parsedNext["page"]);
            }
            if (nextPage === 2 || !previousPage) {
                currentPage = 1;
            } else if (nextPage && nextPage > 0) {
                currentPage = nextPage - 1;
            } else if (previousPage && previousPage > 0) {
                currentPage = previousPage + 1;
            }
            if (this.state.selectedFacets.length > 0) {
                //currentPage = null;
            }
        } catch (err) {
            console.log(err);
            previousPage = null;
            nextPage = null;
            currentPage = null;
        }
        return { nextPage, previousPage, currentPage };
    }

    clearSortedBy = () => {
        this.setState({ sortBy: "" });
    }

    sortBy = (value) => {
        if (!value) {
            return;
        }
        let newOrderingQuery = value.value;
        const oldOrderingQuery = this.state.sortBy;
        this.setState({ sortBy: value.value });
        if (newOrderingQuery !== oldOrderingQuery) {
            var facetsAndOrdering = null;
            if (oldOrderingQuery) {
                facetsAndOrdering = this.state.facetSearchKeyword ? `${this.state.facetSearchKeyword.replace(oldOrderingQuery, newOrderingQuery)}` : `${newOrderingQuery}`;
            } else {
                facetsAndOrdering = this.state.facetSearchKeyword ? `${this.state.facetSearchKeyword}&${newOrderingQuery}` : `${newOrderingQuery}`;
            }
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            this.props.history.push(`?${facetsAndOrdering}`);
            if (!this.state.searchKeyword) {//if there is no search keyword just make a generic facet search
                axios.get(searchFacetUrl(facetsAndOrdering), { cancelToken: source.token })
                    .then(res => {
                        this.setState({ data: res.data, source: null, loading: false });
                    }).catch(err => { this.setState({ source: null, loading: false }); });
            } else {
                axios.get(searchKeywordCombineWithFacetUrl(this.state.searchKeyword || "", facetsAndOrdering), { cancelToken: source.token })
                    .then(res => {
                        this.setState({ data: res.data, source: null, loading: false });
                    }).catch(err => { this.setState({ source: null, loading: false }) });
            }
        }
    }

    render() {
        //const { listView, gridView } = this.state;
        const { width } = this.state;
        const isMobile = width <= 500;
        const open = Boolean(this.state.anchorEl);
        const id = open ? 'simple-popover' : undefined;
        const nextPageUrl = this.getNextPage();
        const previousPageUrl = this.getPreviousPage();
        const { nextPage, previousPage, currentPage } = this.getPages(nextPageUrl, previousPageUrl);
        return (<div>
            <Helmet>
                {/*<link rel="canonical" href={`${BASE_URL}catalogue/`}></link>*/}
                {nextPage && <link rel="next" href={`${BASE_URL}catalogue/?page=${nextPage}`}></link>}
                {previousPage && <link rel="prev" href={`${BASE_URL}catalogue/?page=${previousPage}`}></link>}
                {this.state.selectedFacets.length === 0 && <link rel="canonical" href={`${BASE_URL}catalogue/?page=${currentPage}`}></link>}
                {this.state.selectedFacets.length !== 0 && <link rel="canonical" href={`${BASE_URL}catalogue/`}></link>}
                {/*<meta property="og:url" content={`${BASE_URL}catalogue/`} />*/}
                {this.state.selectedFacets.length === 0 && <meta property="og:url" content={`${BASE_URL}catalogue/?page=${currentPage}`} />}
                {this.state.selectedFacets.length !== 0 && <meta property="og:url" content={`${BASE_URL}catalogue/`} />}
            </Helmet>
            <div className="search-top">
                <Container maxWidth="xl">
                    <Grid container spacing={2} className="searchClass" direction="row" justifyContent="flex-start" alignItems="center">
                        <form onSubmit={this.search}>
                            <Grid item xs={12} container spacing={3} className="searchClass" direction="row" justifyContent="flex-start" alignItems="center">
                                <Grid item sm={10} xs={10} >
                                    <FormattedMessage id="searchCatalogue.searchPlaceholder" defaultMessage="Search for services, tools, datasets, organizations ..." >

                                        {placeholder =>
                                            <Input id="input-search" type="search" className="search-box__input" value={decodeURIComponent(this.state.searchKeyword)} onChange={this.handleChange} placeholder={placeholder} aria-label="Search" />
                                        }
                                    </FormattedMessage>
                                </Grid>
                                <Grid item sm={2} xs={2}>
                                    <Button variant="contained" onClick={this.search} className="mat-flat-button search-box__button color-purple">
                                        <FormattedMessage id="searchCatalogue.searchButton" defaultMessage="Search" >
                                        </FormattedMessage>
                                    </Button>
                                </Grid>
                            </Grid>
                        </form>
                        <div>
                            <Tooltip title="Help for free text queries"><Button aria-describedby={id} onClick={this.handleClick} className="inner-link-simple--purple" aria-label="more-info-button"> <InfoIcon className="small-icon purple--font" aria-hidden="true" /> </Button></Tooltip>
                            <Popover className="search-lucene"
                                id={id}
                                open={open}
                                anchorEl={this.state.anchorEl}
                                onClose={this.handleClose}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}>
                                <div className="p-05"><Typography variant="caption"> <><span className="grey--font">Advanced queries are also supported (e.g. transl*, *ation, transl?tion, "Machine Translation"). For a more comprehensive reference, see  </span>
                                    <span><a href={"http://www.lucenetutorial.com/lucene-query-syntax.html"} target="_blank" rel="noopener noreferrer"> Lucene advanced queries</a></span>
                                    <span className="grey--font">)</span></>
                                </Typography></div></Popover>
                        </div>
                    </Grid>

                </Container>
            </div>

            <section id="page-content-section" className="page-content-section">
                <Container maxWidth="xl">
                    <Grid container direction="row" justifyContent="center" alignItems="center" className="search-results-main-container">
                        <Grid item spacing={3} container xs={12} className="search-results-container">
                            <Hidden only={['lg', 'md', 'sm', 'xl']}>
                                <Grid item xs={12} className="left-facets-area">
                                    <Accordion>
                                        <AccordionSummary expandIcon={<ReorderIcon />} aria-controls="panel1a-content" id="panel1a-header-accordion" >
                                            <Typography> Filters </Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <div>{this.state.data.facets && <FacetsComponentAccordion facets={this.state.data.facets} onSelectKeyword={this.handleFacetChange} onClearAllSearchFilters={this.clearAllSearchFilters} unselectFacet={this.unselectFacet} selectedFacets={this.state.selectedFacets} markFacetAsSelected={this.markFacetAsSelected} handleUnselectedFacet={this.handleUnselectedFacet} />}</div>
                                        </AccordionDetails>
                                    </Accordion>
                                </Grid>
                            </Hidden>
                            <Hidden only='xs'>
                                <Grid item xs={4} sm={3} className="left-facets-area">
                                    {this.state.data.facets && <FacetsComponentAccordion facets={this.state.data.facets} onSelectKeyword={this.handleFacetChange} onClearAllSearchFilters={this.clearAllSearchFilters} unselectFacet={this.unselectFacet} selectedFacets={this.state.selectedFacets} markFacetAsSelected={this.markFacetAsSelected} handleUnselectedFacet={this.handleUnselectedFacet} />}
                                </Grid>
                            </Hidden>
                            <Grid item container xs={12} sm={9} className="right-results-area">
                                {/*<Grid container spacing={2} direction="row" justifyContent="center" alignItems="center" className="search-results-top-container">
                                <Grid item container xs={12} className="botomMargin10 search-pager-top"  justifyContent="space-between" alignItems="center">
                                    <Grid item xs={2}> 
                                        {this.state.data.previous && 
                                            <Button onClick={this.getPreviousPage} classes={{ root: 'inner-link-outlined--teal' }}>&#8592;
                                            <FormattedMessage id="searchCatalogue.previousPage" defaultMessage="Previous"></FormattedMessage> 
                                            </Button>
                                        }
                                    </Grid>
                                    <Grid item xs={8}>
                                        {null && this.state.data.next}
                                        <div>{null && this.state.data.previous}</div>
                                    </Grid>
                                    <Grid item xs={2} style={{textAlign: "right"}}> 
                                        {this.state.data.next && 
                                        <Button onClick={this.getNextPage} classes={{ root: 'inner-link-outlined--teal' }}>
                                        <FormattedMessage id="searchCatalogue.nextPage" defaultMessage="Next" >
                                        </FormattedMessage> &#8594;</Button>
                                        }
                                        </Grid>
                                </Grid>
                                    </Grid>*/}

                                <Grid item container xs={12} alignItems="center" className="results-top-area">
                                    {(this.props?.match?.params?.term || this.state.selectedFacets.length > 0) &&
                                        <Grid item xs={9} className="SearchResultsCount">
                                            <span className="bold-p--id--small"> {this.state.data.count} &nbsp; search results {`${(this.props?.match?.params?.term && this.props?.match?.params?.term?.length >= 3) ? `for ${decodeURIComponent(this.props?.match?.params?.term)}` : ""}`}{this.state.loading ? <CircularProgress size={20} /> : void 0}</span>
                                        </Grid>
                                    }
                                    <Grid item xs={9} className="SearchResultsCount">
                                        <SelectedFacetsTags selectedFacets={this.state.selectedFacets} handleUnselectedFacet={this.handleUnselectedFacet} facetSearchKeyword={this.state.facetSearchKeyword} unselectFacet={this.unselectFacet} />
                                    </Grid>

                                    <Grid item xs={3} className="BtnGroup">
                                        <SortComponent sortBy={this.sortBy} selectedValue={this.state.sortBy} />
                                    </Grid>

                                    {<Grid item xs={12} className="results-bottom-border"><span></span></Grid>}

                                </Grid>

                                {isMobile && (
                                    <Grid container spacing={2} className="CardContainer" alignItems="stretch" direction="row" justifyContent="center">
                                        {this.state.data.results &&
                                            this.state.data.results.map(
                                                (resource, index) =>
                                                    <ResourceComponentCard key={index} resource={resource} keycloak={this.state.keycloak} />
                                            )}
                                    </Grid>
                                )}
                                {!isMobile && (
                                    <Grid container className="CardContainer" alignItems="stretch" direction="row" justifyContent="center">
                                        {this.state.data.results &&
                                            this.state.data.results.map(
                                                (resource, index) =>
                                                    <ResourceComponentList key={index} resource={resource} keycloak={this.state.keycloak} />
                                            )}
                                    </Grid>
                                )
                                }

                                <Grid item container xs={12} className="botomMargin pt-3" justifyContent="space-around" alignItems="center">
                                    <Pagination
                                        onClick={(e) => { document.body.scrollTop = 0; document.documentElement.scrollTop = 0; }}
                                        boundaryCount={3}
                                        page={currentPage}
                                        count={Math.min(500, Math.ceil(this.state.data.count / 20))}
                                        renderItem={(item) => {
                                            const paginationUrl = this.getUrlFromPagination(item.page);
                                            return <PaginationItem
                                                component={Link}
                                                to={paginationUrl}
                                                {...item}
                                            />
                                        }}
                                    />
                                </Grid>

                                {/*<Grid item container xs={12} className="botomMargin pt-3" justifyContent="space-around" alignItems="center">
                                    <Grid item xs={5} sm={5} > {this.state.data.previous && <Link to={previousPageUrl} className="inner-link-outlined--teal" > &#8592;
                                        <FormattedMessage id="searchCatalogue.previousPage" defaultMessage="Previous" ></FormattedMessage> (page {previousPage})</Link>}
                                    </Grid>
                                    <Grid item xs={1} sm={1}>{null && this.state.data.next}<div>{null && this.state.data.previous}</div></Grid>
                                    <Grid item xs={5} sm={5} style={{ textAlign: "right" }}> {this.state.data.next && <Link to={nextPageUrl} className="inner-link-outlined--teal">
                                        <FormattedMessage id="searchCatalogue.nextPage" defaultMessage="Next" ></FormattedMessage> (page {nextPage}) &#8594;</Link>}
                                    </Grid>
                            </Grid>*/}

                            </Grid>
                        </Grid>


                    </Grid>
                </Container>
            </section>
        </div >
        )
    }

}


export default SearchCatalogue;