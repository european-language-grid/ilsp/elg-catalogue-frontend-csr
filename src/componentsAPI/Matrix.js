import React from "react";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import LoadChartsTabs from "./MatrixCharts/LoadChartsTabs"; 
import GoToCatalogue from "./CommonComponents/GoToCatalogue";
import { Helmet } from "react-helmet";

export default class Matrix extends React.Component {
   
    render() {
        return <div>
            <Helmet>
                <title>European Language Equality / European Language Grid Dashboard</title>
                {/*<meta name="description" content={description} ></meta>
                <meta name="keywords" content={[...SEO_KEYWORDS].join(", ")} />
                <link rel="canonical" href={`${BASE_URL}catalogue/corpus/${this.state.pk}`}></link>*/}
            </Helmet>
            <div className="search-top">
                <GoToCatalogue />
            </div>
             <Container maxWidth="xl" className="matrix-overview">
                <Grid container spacing={1}>
                    <></>                 
                    </Grid>  
            </Container>
            <Container maxWidth="xl"  className="matrix-container">  
            
                <div className="tab-pane-container">
                <LoadChartsTabs/>               
                </div>
            </Container>

        </div >
    }

}