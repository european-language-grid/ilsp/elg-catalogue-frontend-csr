import React from "react";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {
  PieChart, Pie, Cell,
} from 'recharts';

const COLORS = ['#7c5cd6', '#636e7b'];

class ProjectChart extends React.Component {
  constructor() {
    super();
    this.createchartdata = this.createchartdata.bind(this);
  }

  typeofcurrency = (currency) => {
    switch (currency) {
      case "euro": return (
        <span> {'\u20AC'}</span>
      );
      default: return ""
    }
  }


  createchartdata() {
    const ammountdata = [];
    //console.log(this.props.described_entity);
    let ec_ammount = this.props.described_entity.ec_max_contribution; 
    if (ec_ammount) {
      ec_ammount = this.props.described_entity.ec_max_contribution.field_value ;      
    }
    else {ec_ammount = null;}
    let national_ammount = this.props.described_entity.national_max_contribution;
    if (national_ammount) {
    national_ammount = this.props.described_entity.national_max_contribution.field_value ;
    }
    else{national_ammount=null;}
    
    if (ec_ammount) {
      ammountdata.push({ name: 'EU contribution', value: this.props.described_entity.ec_max_contribution.field_value.amount.field_value });
    }
    if (national_ammount) {
      ammountdata.push({ name: 'National contribution', value: this.props.described_entity.national_max_contribution.field_value.amount.field_value });
    }

    return ammountdata;

  }

  render() {
    const { described_entity ,metadataLanguage} = this.props;
    const data = this.createchartdata();
    if (!data) {
      return <div></div>
  }

    return (
      <React.Fragment>
        <Grid container spacing={0}>
          <Grid item>

            <List>
            {described_entity.cost && described_entity.cost.field_value &&
              <ListItem className="transparent right-pills">
                  <div>
                    <Typography className="bold-p--id">{described_entity.cost.field_value.amount.field_label[metadataLanguage]||
                    described_entity.cost.field_value.amount.field_label["en"]}</Typography>
                    <Typography className="info_value">
                      {this.typeofcurrency(described_entity.cost.field_value.currency.label[metadataLanguage])}
                      {Intl.NumberFormat('en').format(described_entity.cost.field_value.amount.field_value)}
                    </Typography>
                  </div>
                  </ListItem>
             }
              
             

                {described_entity.ec_max_contribution &&  described_entity.ec_max_contribution.field_value && <ListItem className="purple right-pills">
                  <div>
                    <Typography variant ="h5" className=" white">{described_entity.ec_max_contribution.field_label[metadataLanguage]
                    ||described_entity.ec_max_contribution.field_label["en"]}</Typography>
                    <Typography variant ="h5" className=" white">
                      {this.typeofcurrency(described_entity.ec_max_contribution.field_value.currency.label[metadataLanguage])}
                      {Intl.NumberFormat('en').format(described_entity.ec_max_contribution.field_value.amount.field_value)}
                    </Typography>


                  </div>
                  </ListItem>
                }
              

              {described_entity.national_max_contribution && described_entity.national_max_contribution.field_value && <ListItem className="grey right-pills">                 
                  <div>
                    <Typography variant ="h5" className=" white">{described_entity.national_max_contribution.field_label[metadataLanguage]
                    ||described_entity.national_max_contribution.field_label["en"]}</Typography>
                    <Typography variant ="h5" className=" white">
                      {this.typeofcurrency(described_entity.national_max_contribution.field_value.currency.label[metadataLanguage])}
                      {Intl.NumberFormat('en').format(described_entity.national_max_contribution.field_value.amount.field_value)}

                    </Typography>
                  </div>
                  </ListItem>
                }
              
            </List>
          </Grid>

          <Grid item>
            {data &&
              <PieChart width={200} height={100}>
                <Pie
                  data={data}
                  cx={'50%'}
                  cy={'100%'}
                  startAngle={180}
                  endAngle={0}
                  innerRadius={30}
                  outerRadius={80}
                  fill="#9ca5b0"
                  paddingAngle={5}
                  dataKey="value"
                >
                  {
                    data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                  }
                </Pie>
              </PieChart>
            }


          </Grid>

        </Grid>

      </React.Fragment>
    );
  }

}

export default ProjectChart;