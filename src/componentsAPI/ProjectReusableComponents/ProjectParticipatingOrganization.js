import React from "react";
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";
//import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
//import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

class ProjectParticipatingOrganization extends React.Component {

  render() {
    const { described_entity,metadataLanguage } = this.props;
  
    return (
      <>
        {described_entity.participating_organization && described_entity.participating_organization.field_value.length>0 &&

          <div className="padding15">
            <Typography variant="h3" className="title-links">Participants</Typography>
            {


              <Table aria-label="simple table">
                
                <TableBody>
                  {
                    described_entity.participating_organization.field_value.map((keyword, index) =>                      
                      <TableRow key={index}>
                        <TableCell component="th" scope="row">
                        { keyword.full_metadata_record ? 
                            <div className="padding5 internal_url">
                               <Link to={commonParser.getFullMetadata(keyword.full_metadata_record).internalELGUrl}>
                                {keyword.organization_name.field_value[metadataLanguage] || keyword.organization_name.field_value[Object.keys(keyword.organization_name.field_value)[0]]}
                                </Link>
                                </div>
                                :
                                <div><Typography variant="h6" >{keyword.organization_name.field_value[metadataLanguage] || keyword.organization_name.field_value[Object.keys(keyword.organization_name.field_value)[0]]}</Typography></div>
                         }

                          
                        </TableCell>
                        <TableCell align="right"> { keyword.website && 
              <div>                                
                <a href={keyword.website.field_value} target="_blank" rel="noopener noreferrer" className="info_value" >
                        <div key={index}><span><LaunchIcon className="xsmall-icon"/> </span> <span> {keyword.website.field_label[metadataLanguage]||keyword.website.field_label["en"]}</span></div> 
                </a>
                </div>               
              }              
            </TableCell>

                        {/*<TableCell align="right"> {
                          <div>
                            <a href="/" target="_blank" rel="noopener noreferrer" className="info_value" >
                              <div key={index}><span><LaunchIcon /> </span> <span>Website </span></div>
                            </a>
                          </div>
                        }
                      </TableCell>*/}

                      </TableRow>

                    )}
                </TableBody>
              </Table>

            }

          </div>
        }
      </>
    );
  }

}

export default ProjectParticipatingOrganization;