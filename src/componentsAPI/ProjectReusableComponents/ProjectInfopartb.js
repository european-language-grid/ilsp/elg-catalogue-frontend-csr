import React from "react";

import Typography from '@material-ui/core/Typography';
import GeneralIdentifier from '../CommonComponents/GeneralIdentifier';

class ProjectInfopartb extends React.Component {

    render() {
        const { described_entity,metadataLanguage, identifier_name, identifier_scheme} = this.props;
        return (
            <React.Fragment>
                {described_entity.grant_number && 
                    <div className="padding15">
                        <Typography className="bold-p--id">
                            {described_entity.grant_number.field_label[metadataLanguage]||described_entity.grant_number.field_label["en"]}: {described_entity.grant_number.field_value}
                        </Typography>
                    </div>
                }
                {
                   <GeneralIdentifier data={described_entity} identifier_name={identifier_name} identifier_scheme={identifier_scheme} metadataLanguage={metadataLanguage}/> 
                }
                {described_entity.status &&
                    <div className="padding15">
                        <Typography className="bold-p--id"> {described_entity.status.field_label[metadataLanguage]||described_entity.status.field_label["en"]} </Typography>
                        <Typography className="info_value">{described_entity.status.field_value}</Typography>
                    </div>
                }
                {described_entity.related_call &&
                    <div className="padding15">
                        <Typography className="bold-p--id"> {described_entity.related_call.field_label[metadataLanguage]||described_entity.related_call.field_label["en"] }</Typography>
                        <Typography className="info_value ">{described_entity.related_call.field_value}</Typography>
                    </div>
                }
                {described_entity.related_programme &&
                    <div className="padding15">
                        <Typography className="bold-p--id"> {described_entity.related_programme.field_label[metadataLanguage]||described_entity.related_programme.field_label["en"] } </Typography>
                        <Typography className="info_value ">     {described_entity.related_programme.field_value} </Typography>
                    </div>
                }
                {described_entity.related_subprogramme &&
                    <div className="padding15">
                        <Typography className="bold-p--id">{described_entity.related_subprogramme.field_label[metadataLanguage]||described_entity.related_subprogramme.field_label["en"] } </Typography>
                        <Typography className="info_value"> {described_entity.related_subprogramme.field_value} </Typography>
                    </div>
                }
            </React.Fragment>
        );
    }

}

export default ProjectInfopartb;