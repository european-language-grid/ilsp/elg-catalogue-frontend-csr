import React from "react";
import Typography from '@material-ui/core/Typography';

class ProjectDocument extends React.Component {

  render() {
    const { described_entity , metadataLanguage} = this.props;
    return (
      <React.Fragment>

                  {described_entity.is_related_to_document && described_entity.is_related_to_document.field_value && described_entity.is_related_to_document.field_value.length>0 &&
                      <div className="padding15">
                       <Typography variant="h3" className="title-links">{described_entity.is_related_to_document.field_label[metadataLanguage] || described_entity.is_related_to_document.field_label["en"]}</Typography>
                         {described_entity.is_related_to_document.field_value.map((keyword, index) =>

                           <p key={index} className="info_value">{keyword.title.field_value[metadataLanguage] || keyword.title.field_value[Object.keys(keyword.title.field_value)[0]]} </p>

                    )}
                    </div>
                     }


             {described_entity.project_report && described_entity.project_report.field_value &&
               <div className="padding15">
               <Typography variant="h3" className="title-links">{described_entity.project_report.field_label[metadataLanguage] || described_entity.project_report.field_label["en"]}</Typography>
                 <p className="info_value">{described_entity.project_report.field_value[metadataLanguage] || described_entity.project_report.field_value[Object.keys(described_entity.project_report.field_value)[0]]}</p>
                </div>
            }
                 
      </React.Fragment>
    );
  }

}

export default ProjectDocument;