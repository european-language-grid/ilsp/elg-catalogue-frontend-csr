import React from 'react';
import { withStyles } from '@material-ui/core/styles';
//import { makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    backgroundColor: '#ccd2d8',
  },
  bar: {
    borderRadius: 20,
    backgroundColor: '#636e7b',
  },
})(LinearProgress);



export default class CustomProgressBar extends React.Component {
  render() {
    const { startDate, endDate } = this.props;
    var countDownDate = new Date(endDate).getTime();
    var startdate = new Date(startDate).getTime();
    var distanceWhole =  countDownDate - startdate; //Future date - current date
    // Get todays date and time
    var now = new Date().getTime();
    var distanceLeft = countDownDate - now;
    // Time calculations for minutes and percentage progressed
    var minutesLeft = Math.floor(distanceLeft / (1000 * 60));
    var minutesTotal = Math.floor(distanceWhole / (1000 * 60));
    var progress = Math.floor(((minutesTotal - minutesLeft) / minutesTotal) * 100);
    //console.log(progress);
    if (progress>100){
      progress=100;
    }
    //console.log(progress);
    return (
      <div>

        <BorderLinearProgress
          variant="determinate"
          color="secondary"
          value={progress}
        />

      </div>
    );

  }
}


