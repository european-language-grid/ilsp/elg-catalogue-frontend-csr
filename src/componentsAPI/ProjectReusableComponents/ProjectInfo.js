import React from "react";

import Typography from '@material-ui/core/Typography';
import DatesComponent from './ProjectDatesComponent';
import SocialMediaType from '../CommonComponents/SocialMediaType'
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";
import EmailsWithIcon from  '../CommonComponents/EmailsWithIcon';

class ProjectInfo extends React.Component {

    render() {
        const { described_entity ,metadataLanguage} = this.props;
       


        return (
            <React.Fragment>
                {(described_entity.project_alternative_name || described_entity.website||described_entity.email||described_entity.social_media_occupational_account||described_entity.project_start_date||described_entity.project_end_date)
                 && <Typography variant="h3" className="title-links">Project information</Typography>} 
                {/* described_entity.project_short_name &&
                <Typography className="acronym"> {described_entity.project_short_name[Object.keys(described_entity.project_short_name)[0]]}</Typography>
              */ }

                {described_entity.project_alternative_name &&
                    <div className="padding15">
                        {described_entity.project_alternative_name.field_value.map((altname, altnameIndex) =>
                            <span key={altnameIndex} className="info_value">{altname[metadataLanguage] || altname["en"]} </span>
                        )}
                    </div>
                }

                {described_entity.website &&
                    <div className="padding15">
                       {/* <Typography className="bold-p--id">{described_entity.website.field_label[metadataLanguage]||described_entity.website.field_label[Object.keys(described_entity.website.field_label)[0]]}</Typography>*/}

                        {described_entity.website.field_value.map((site, siteIndex) =>
                             <a key={siteIndex} href={site} target="_blank" rel="noopener noreferrer" className="info_value" >
                             {<div key={siteIndex}><span><LaunchIcon className="xsmall-icon"/> </span> <span>Website</span></div>}
                                </a>
                             )}  
                    </div>

                }
                {described_entity.email && described_entity.email.field_value.length>0 && <div className="padding15"><EmailsWithIcon emailArray={described_entity.email.field_value} /></div>}
                
                

                {described_entity.social_media_occupational_account &&
                    described_entity.social_media_occupational_account.field_value.length > 0 ? (
                        <div className="padding15">
                            <Typography className="bold-p--id">{described_entity.social_media_occupational_account.field_label[metadataLanguage]||described_entity.social_media_occupational_account.field_label["en"]}</Typography>
                            {described_entity.social_media_occupational_account.field_value.map((keyword, smindex) =>
                                <SocialMediaType  key={smindex} account_type={keyword.social_media_occupational_account_type.field_value} value={keyword.value.field_value} />
                            )}

                        </div>)
                    : void 0
                }

                {described_entity.project_start_date &&
                    described_entity.project_end_date &&
                    <DatesComponent startDate={described_entity.project_start_date.field_value} 
                    endDate={described_entity.project_end_date.field_value} 
                    startDateLabel={described_entity.project_start_date.field_label[metadataLanguage]||described_entity.project_start_date.field_label["en"]} 
                    endDateLabel={described_entity.project_end_date.field_label[metadataLanguage]||described_entity.project_end_date.field_label["en"]}/>
                }

                

            </React.Fragment>
        );
    }

}

export default ProjectInfo;