import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { TILDE_FOOTER_MENU_API, TILDE_FEEDBACK_MENU_API, FOOTER_BASE_URL } from "../../../config/constants";
import "../../../assets/icons-and-css-variables/colors.css";
import "../../../assets/icons-and-css-variables/variables.css";
import "../../../assets/icons-and-css-variables/icons.css";
import "../../Header/UniversalHeaderFolder/styles.css";
import "../../Header/UniversalHeaderFolder/nav.css";
import "./footer.css";
import footer_json from "../../../data/footer_json.json";
import feedback_json from "../../../data/feedback_json.json";

export default class UniversalFooter extends React.Component {

    constructor(props) {
        super(props);
        this.state = { data: null, feedbackData: null }
    }

    componentDidMount() {
        this.getFooterData();
        this.getFeedBackData();
    }

    getFooterData = () => {
        axios.get(TILDE_FOOTER_MENU_API).then(res => this.setState({ data: res.data })).catch(err => { this.setState({ data: footer_json }) });
    }

    getFeedBackData = () => {
        axios.get(TILDE_FEEDBACK_MENU_API).then(res => { this.setState({ feedbackData: res.data }); }).catch(err => { this.setState({ feedbackData: feedback_json }) })
    }

    getReactLinkAddress = (url) => {
        let reactLinkAddress = url.substring(url.indexOf("/catalogue/") + "/catalogue/".length);
        return reactLinkAddress ? ("/" + reactLinkAddress).replace("//", "/") : "/";
    }

    normilizeID = (title) => {
        return title.replace(/\s+/g, '-').toLowerCase().replace("&", "-").replace('--', '-');
    }

    fixMenuPathPrefix = (field_elg_menu_link_type, relative) => {
        switch (field_elg_menu_link_type) {
            case "CMS":
                //relative = `page/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "React":
                relative = `catalogue/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "External":
                break;
            case "Angular":
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                relative = relative === "news-page" ? "news" : relative;
                break;
            default:
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
        }
        return relative;
    }

    getExternalLink = (item, final_menu_url) => {
        return <a target="_blank" rel="noreferrer" href={final_menu_url}><svg className="xxs"><use href={`#${item.description}`}></use></svg><span>{item.title}</span></a>
    }

    getAngularOrCMSLink = (item, final_menu_url) => {
        return <a href={final_menu_url}><svg className="xxs"><use href={`#${item.description}`}></use></svg><span>{item.title}</span></a>
    }

    renderMenuData = (data = []) => {
        if (data && data.length > 0) {
            return data.map((item, index) => {
                const reactLink = item.field_elg_menu_link_type === "React";
                const isExternalLink = item.field_elg_menu_link_type === "External";
                const isAngularOrCMS = item.field_elg_menu_link_type === "CMS" || item.field_elg_menu_link_type === "Angular";
                const relative = this.fixMenuPathPrefix(item.field_elg_menu_link_type, item.relative);
                const final_menu_url = item.field_elg_menu_link_type === "External" ? item.relative : `${FOOTER_BASE_URL}${relative}`;
                return <li key={item.key} className="mat-button">
                    {reactLink && <Link to={this.getReactLinkAddress(item.uri)}>{item.title}</Link>}
                    {isExternalLink && this.getExternalLink(item, final_menu_url)}
                    {isAngularOrCMS && this.getAngularOrCMSLink(item, final_menu_url)}
                </li>
            })
        } else {
            return <></>
        }

    }

    render() {
        if (!this.state.data) {
            return <></>;
        }
        return <div className="footer-wrapper">
            <footer>
                <div className="items">
                    <div>
                        <ul>
                            {this.renderMenuData(this.state.feedbackData)}
                        </ul>
                    </div>
                    <div className="social">
                        <ul>
                            {this.renderMenuData(this.state.data)}
                        </ul>
                    </div>
                </div>

                <div className="legal">
                    <div className="funding">
                        <div><img src="https://s3.dbl.cloud.syseleven.net/dev-cms/2022-03/eu_flag.svg"
                            alt="European union flag" />
                        </div>
                        <div>
                            <p>
                                The European Language Grid has received funding from the
                                European Union’s Horizon 2020 research and innovation
                                programme under grant agreement № 825627. The European
                                Language Equality project has received funding from the
                                European Union under grant agreement № LC-01641480 –
                                101018166.
                            </p>
                        </div>
                    </div>
                    <p>© {new Date().getFullYear()} ELG Consortium</p>
                    <p><a href={`${FOOTER_BASE_URL}terms-of-use`}>Terms of Use</a></p>
                </div>
            </footer>
        </div>
    }
}
