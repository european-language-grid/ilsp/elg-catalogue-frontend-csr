import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import { TabContent, TabPane, } from "reactstrap";
import HelmetMetaData from "./CommonComponents/HelmetMetaData";
import Container from '@material-ui/core/Container';
//import { ReactComponent as BackIcon } from "./../assets/elg-icons/navigation-arrows-left-1.svg";
import GoToCatalogue from "./CommonComponents/GoToCatalogue";
import CorpusImage from "../assets/images/corpus.png";
import BlurLinearIcon from "./../assets/images/blur_linear_yellow.svg";
import BlurCircularIcon from "./../assets/images/blur_linear_yellow_model.svg";
import BlurOnIcon from "./../assets/images/blur_linear_yellow_grammar.svg";
import { SERVER_API_METADATARECORD, RETRIEVE_RECORD_INFO, getLogoutUrl, IS_ADMIN } from "../config/constants";
//common
import ExportMetadata from "./CommonComponents/ExportMetadata";
//import ShareMetadata from "./CommonComponents/ShareMetadata";
import ProgressBar from "./CommonComponents/ProgressBar";
//import Keywords from "./CommonComponents/Keywords";
import DescriptionRichText from "./CommonComponents/DescriptionRichText";
import ResourceActor from "./CommonComponents/ResourceActor";
import ResourceHeader from "./CommonComponents/ResourceHeader";
import AdditionalInfo from "./CommonComponents/AdditionalInfo";
import ResourceLifeCyrcleInfo from "./CommonComponents/ResourceLifeCyrcleInfo";
import Ethics from "./CommonComponents/Ethics";
import Attribution from "./CommonComponents/Attribution";
//import IsReplacedWith from "./CommonComponents/IsReplacedWith";
//lRC specific
//import ldParser from "../parsers/ldParser";
import ldParser from "../parsers/ldParser";
import commonParser from "../parsers/CommonParser";
//import ldLrSubclassParser from "../parsers/ldLrSubclassParser";
import ldLrSubclassParser from "../parsers/ldLrSubclassParser";
import NavigationTabs from "./ldReusableComponents/NavigationTabs";
//import LdGeneral from "./ldReusableComponents/LdGeneral";
import LdMediaPart from "./ldReusableComponents/LdMediaPart";
import LdMediaPartType from './ldReusableComponents/LdMediaPartType';
import LdUnspecifiedPartType from "./ldReusableComponents/LdUnspecifiedPartType";
//common with corpus 
import FundedBy from "./corpusReusableComponents/FundedBy";
//import Replaces from "./CommonComponents/Replaces";
import Relations from "./CommonComponents/Relations";
import Documentations from "./corpusReusableComponents/Documentations";
import Creation from "./corpusReusableComponents/Creation";
import ActualUse from "./corpusReusableComponents/ActualUse";
import Validation from "./corpusReusableComponents/Validation";
import CorpusGeneralCategories from "./corpusReusableComponents/CorpusGeneralCategories";
//import PhysicalResource from "./corpusReusableComponents/PhysicalResource";
import Versions from "./CommonComponents/Versions";
import Distributions from "./corpusReusableComponents/Distributions";
import Annotation from "./corpusReusableComponents/Annotation";
import RecordStats from "./CommonComponents/RecordStats";
import TryOutModelsTab from "./ldReusableComponents/TryOutModelsTab";
//import HasSubset from "./corpusReusableComponents/HasSubset";


function select_size(first_column, second_column, third_column) {
    let grid_size = 12;

    if (first_column && second_column && third_column) {
        grid_size = 4;
    }
    if (first_column && !second_column && !third_column) {
        grid_size = 12;
    }
    if ((first_column && second_column && !third_column) || (!first_column && second_column && third_column) || (first_column && !second_column && third_column)) {
        grid_size = 6;
    }
    return grid_size;

}

function typeofresource(type) {
    switch (type) {
        case "model": return (BlurCircularIcon);
        case "grammar": return (BlurOnIcon);
        default: return BlurLinearIcon;
    }
}

export default class LanguageDescription extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: '', metadataLanguage: '', tab: '1', showEthics: false, showDocs: false, howArea: false, showCreation: false, displayTabGeneral: false, pk: "" };
        this.state = {
            data: '', metadataLanguage: '', tab: '1', showEthics: false, showDocs: false, showCreation: false, showCategories: false,
            pk: "",
            showArea: false, displayTabGeneral: false,
            loading: true, number_of_failed_loads: 0,
            isOwner: false, isLegalValidator: false, isMetadataValidator: false, isTechnicalValidator: false, source: null,
            routeTab: null
        };
    }

    componentDidMount() {
        this.getMetadataResourceDetail();
        this.setState({ number_of_failed_loads: 0 });
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        clearTimeout(this.timeOutId);
    }


    displayTabGeneralFunction = (flag) => {
        this.setState({ displayTabGeneral: flag });
    }

    showEthicsFunction = (flag) => {
        this.setState({ showEthics: flag });
    }

    showDocsFunction = (flag) => {
        this.setState({ showDocs: flag });
    }

    showCreationFunction = (flag) => {
        this.setState({ showCreation: flag });
    }

    showAreaFunction = (flag) => {
        this.setState({ showArea: flag });
    }

    toggleTab = (tabTitles, tabIndex) => {
        //this.setState({ tab: tabIndex });
        try {
            const tab2Route = tabTitles[tabIndex - 1];
            const encodedTab2Route = encodeURIComponent(tab2Route.toLowerCase());
            this.props.history.push(`/ld/${this.state.pk}/${encodedTab2Route}/`);
        } catch (err) {
            console.log(err);
        }
    }

    updateRecord = () => {
        this.getMetadataResourceDetail();
    }

    retrieveRecordInfo = (id, source) => {
        if (this.props.keycloak && this.props.keycloak.authenticated) {
            axios.get(RETRIEVE_RECORD_INFO(id), { cancelToken: source.token })
                .then(res => {
                    const { curator = false, legal_validator = false, metadata_validator = false, technical_validator = false } = res.data;
                    this.setState({ isOwner: curator, isLegalValidator: legal_validator, isMetadataValidator: metadata_validator, isTechnicalValidator: technical_validator });
                })
                .catch(err => console.log(err));
        }
    }

    getMetadataResourceDetail = () => {
        const id = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const tab = this.props && this.props.match && this.props.match.params && this.props.match.params.tab;

        const url = SERVER_API_METADATARECORD(id);
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true, routeTab: tab });
        this.retrieveRecordInfo(id, source);
        axios.get(url)
            .then(res => {
                this.setState(res);
                const lang = Object.keys(res.data.described_entity.field_value.resource_name.field_value).includes("en") ? "en" : Object.keys(res.data.described_entity.field_value.resource_name.field_value)[0];
                this.setState({ metadataLanguage: lang, pk: id, source: null, loading: false, number_of_failed_loads: 0 });
            }).catch(err => {
                console.log(err);
                this.setState({ source: null, loading: false });
            });
    }

    componentDidUpdate(prevProps, prevState) {
        const thisId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const prevId = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.id;
        const thisTab = this.props && this.props.match && this.props.match.params && this.props.match.params.tab;
        const prevTab = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.tab;
        if (thisId !== prevId) {
            this.setState({ data: '', metadataLanguage: '', tab: '1', displayTabGeneral: false });
            this.getMetadataResourceDetail();
        } else if (thisTab !== prevTab) {
            this.setState({ routeTab: thisTab });
        }
    }


    reloadPage = () => {
        const number_of_failed_loads = this.state.number_of_failed_loads;
        if (number_of_failed_loads < 3) {
            this.setState({ number_of_failed_loads: number_of_failed_loads + 1 });
            this.getMetadataResourceDetail();
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        } else {
            this.timeOutId = setTimeout(() => { window.location = getLogoutUrl() }, 5000)
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        }
    }

    showTryOutTab = () => {
        let showTab = false;
        if (this.state.data && this.state.data.described_entity?.field_value?.is_ml_model_of_functional) {
            showTab = true;
        }
        return showTab;
    }

    render() {
        if (this.props.keycloak && !this.props.keycloak.authenticated) {
            if (commonParser.redirectUnregisteredUsersToKeycloak(this.props.location)) {
                this.props.keycloak.login();
            }
        }

        if (this.state.loading) {
            return <ProgressBar />
        } else {
            if (this.state.data) {
            } else {
                return this.reloadPage();
            }
        }

        const { data, metadataLanguage } = this.state;
        const languages = [];// ["english", "spanish"];
        const resourceName = ldParser.getResourceName(data, metadataLanguage) ? ldParser.getResourceName(data, metadataLanguage).value : "";
        const logo = ldParser.getLogo(data);
        const short_name = ldParser.getResourceShortName(data, metadataLanguage);
        const version = ldParser.getVersion(data, metadataLanguage) ? ldParser.getVersion(data, metadataLanguage).value : "";
        const version_label = ldParser.getVersion(data, metadataLanguage) ? ldParser.getVersion(data, metadataLanguage).label : "";
        const version_date = ldParser.getVersionDate(data, metadataLanguage) ? ldParser.getVersionDate(data, metadataLanguage).value : "";
        //const version_date_label = ldParser.getVersionDate(data, metadataLanguage) ? ldParser.getVersionDate(data, metadataLanguage).label : "";
        const resource_type = data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.ld_subclass.label["en"];
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        const description = ldParser.getDescription(data, metadataLanguage) ? ldParser.getDescription(data, metadataLanguage).value : "";
        const keywords = ldParser.getKeywords(data, metadataLanguage) ? ldParser.getKeywords(data, metadataLanguage).value : [];
        const keywords_labels = ldParser.getKeywords(data, metadataLanguage) ? ldParser.getKeywords(data, metadataLanguage).label : "";
        const domainKeywords = ldParser.getDomainKeywords(data, metadataLanguage) ? ldParser.getDomainKeywords(data, metadataLanguage).value : [];
        const domainKeywords_label = ldParser.getDomainKeywords(data, metadataLanguage) ? ldParser.getDomainKeywords(data, metadataLanguage).label : [];
        const subjectKeywords = ldParser.getSubjectKeywords(data, metadataLanguage) ? ldParser.getSubjectKeywords(data, metadataLanguage).value : [];
        const subjectKeywords_label = ldParser.getSubjectKeywords(data, metadataLanguage) ? ldParser.getSubjectKeywords(data, metadataLanguage).label : "";
        const intentedKeywords = ldParser.getIntendedKeywords(data, metadataLanguage) ? ldParser.getIntendedKeywords(data, metadataLanguage).value : [];
        const intentedKeywords_label = ldParser.getIntendedKeywords(data, metadataLanguage) ? ldParser.getIntendedKeywords(data, metadataLanguage).label : "";
        const citationText = ldParser.getCitationText(data, metadataLanguage) ? ldParser.getCitationText(data, metadataLanguage).value : "";
        const citationText_label = ldParser.getCitationText(data, metadataLanguage) ? ldParser.getCitationText(data, metadataLanguage).label : "";
        const citation_all_versions = ldParser.getCitationAllVersions(data, metadataLanguage) ? ldParser.getCitationAllVersions(data, metadataLanguage).value : "";
        const citation_all_versions_label = ldParser.getCitationAllVersions(data, metadataLanguage) ? ldParser.getCitationAllVersions(data, metadataLanguage).label : "Cite as";
        const mailingList = ldParser.getMailingList(data, metadataLanguage) ? ldParser.getMailingList(data, metadataLanguage).value : [];
        const discussionUrl = ldParser.getDiscussionUrl(data, metadataLanguage) ? ldParser.getDiscussionUrl(data, metadataLanguage).value : [];
        const additionalInfo = ldParser.getAdditionalInfo(data, metadataLanguage) ? ldParser.getAdditionalInfo(data, metadataLanguage).value : [];
        const additionalInfo_label = ldParser.getAdditionalInfo(data, metadataLanguage) ? ldParser.getAdditionalInfo(data, metadataLanguage).label : "";
        //const physical_resource = ldParser.getPhysicalResource(data, metadataLanguage) ? ldParser.getPhysicalResource(data, metadataLanguage).value : [];
        //const physical_resource_label = ldParser.getPhysicalResource(data, metadataLanguage) ? ldParser.getPhysicalResource(data, metadataLanguage).label : "";
        const personal_data_included = ldParser.getPersonalDataIncluded(data, metadataLanguage) ? ldParser.getPersonalDataIncluded(data, metadataLanguage).value : null;
        const personal_data_included_label = ldParser.getPersonalDataIncluded(data, metadataLanguage) ? ldParser.getPersonalDataIncluded(data, metadataLanguage).label : "";
        const personal_data_details = ldParser.getPersonalDataDetails(data, metadataLanguage) ? ldParser.getPersonalDataDetails(data, metadataLanguage).value : "";
        const personal_data_details_label = ldParser.getPersonalDataDetails(data, metadataLanguage) ? ldParser.getPersonalDataDetails(data, metadataLanguage).label : "";
        const sensitive_data_included = ldParser.getSensitiveDataIncluded(data, metadataLanguage) ? ldParser.getSensitiveDataIncluded(data, metadataLanguage).value : null;
        const sensitive_data_included_label = ldParser.getSensitiveDataIncluded(data, metadataLanguage) ? ldParser.getSensitiveDataIncluded(data, metadataLanguage).label : "";
        const sensitive_data_details = ldParser.getSensitiveDataDetails(data, metadataLanguage) ? ldParser.getSensitiveDataDetails(data, metadataLanguage).value : "";
        const sensitive_data_details_label = ldParser.getSensitiveDataDetails(data, metadataLanguage) ? ldParser.getSensitiveDataDetails(data, metadataLanguage).label : "";
        const anonymized = ldParser.getAnonymized(data, metadataLanguage) ? ldParser.getAnonymized(data, metadataLanguage).value : null;
        const anonymized_label = ldParser.getAnonymized(data, metadataLanguage) ? ldParser.getAnonymized(data, metadataLanguage).label : "";
        const anonymization_details = ldParser.getAnonymizedDetails(data, metadataLanguage) ? ldParser.getAnonymizedDetails(data, metadataLanguage).value : "";
        const anonymization_details_label = ldParser.getAnonymizedDetails(data, metadataLanguage) ? ldParser.getAnonymizedDetails(data, metadataLanguage).label : "";
        const ld_subclass = ldLrSubclassParser.getldsubclass(data.described_entity.field_value.lr_subclass) ? ldLrSubclassParser.getldsubclass(data.described_entity.field_value.lr_subclass).value : "";
        //const ld_subclass_label = ldLrSubclassParser.getldsubclass(data.described_entity.field_value.lr_subclass) ? ldLrSubclassParser.getldsubclass(data.described_entity.field_value.lr_subclass).label : "";
        const lr_subclass = data.described_entity.field_value.lr_subclass ? data.described_entity.field_value.lr_subclass.field_value : null;
        const model_typeArray = ldLrSubclassParser.getmodeltype(data.described_entity.field_value.lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmodeltype(data.described_entity.field_value.lr_subclass, metadataLanguage).value : "";
        const model_type_label = ldLrSubclassParser.getmodeltype(data.described_entity.field_value.lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmodeltype(data.described_entity.field_value.lr_subclass, metadataLanguage).label : "";
        const model_functionArray = ldLrSubclassParser.getmodelfunction(data.described_entity.field_value.lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmodelfunction(data.described_entity.field_value.lr_subclass, metadataLanguage).value : "";
        const model_function_label = ldLrSubclassParser.getmodelfunction(data.described_entity.field_value.lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmodelfunction(data.described_entity.field_value.lr_subclass, metadataLanguage).label : "";
        const { under_construction = false } = data.management_object || {};
        const { for_information_only = false } = data.management_object || {};
        const { is_latest_version = false } = data.management_object || {};
        const { tombstone = false } = data.management_object || {};
        const all_versions = data.described_entity.field_value.all_versions ? data.described_entity.field_value.all_versions : null;
        const all_versions_label = data.described_entity.field_value.all_versions ? (data.described_entity.field_value.all_versions.field_label[metadataLanguage] || data.described_entity.field_value.all_versions.field_label["en"]) : "";
        const { inferred_language = false } = data.management_object || {};
        const { versions_exceeding_limit = false } = data.management_object || {};
        const tabTitles = ["Overview"];
        if (!under_construction) {
            tabTitles.push("Download");
        } else if (IS_ADMIN(this.props.keycloak)) {
            tabTitles.push("Download");
        }
        if (this.showTryOutTab()) {
            tabTitles.push("Try out");
        }
        if (this.state.displayTabGeneral) {
            tabTitles.push("Related LRTs");
        }
        //control the number of columns to appear
        const first_column = this.state.showDocs || this.state.showCreation || data.described_entity.field_value.ipr_holder || false;
        const second_column = data.described_entity.field_value.actual_use || data.described_entity.field_value.validated || false;
        const third_column = this.state.showEthics || this.state.showArea || this.state.showLcrMedia || false;
        const grid_size = select_size(first_column, second_column, third_column);
        const default_image = typeofresource(resource_type);

        const tab2SetAsActive = commonParser.translateRouteTabToID(tabTitles, this.state.routeTab);
        if (this.state.tab !== tab2SetAsActive) {
            this.setState({ tab: tab2SetAsActive });
            return <></>;
        }

        return <div>
            <HelmetMetaData data={data}
                resourceName={resourceName}
                description={description}
                keywords={keywords}
                domainKeywords={domainKeywords}
                subjectKeywords={subjectKeywords}
                intentedKeywords={intentedKeywords}
                corpus_subclass={null}
                languages={languages}
                resourceShortName={short_name}
                for_information_only={for_information_only}
                ltAreaKeywordsArray={null}
                disciplines={null}
                servicesOffered={null}
                OrganizationRolesArray={null}
                ld_subclass={ld_subclass}
                lcr_subclass={null}
                type="ld"
                pk={this.state.pk}
            />

            <div className="search-top">
                <GoToCatalogue />
            </div>
            <Container maxWidth="xl">
                <div className="metadata-main-card-container">
                    <ResourceLifeCyrcleInfo key={data.management_object.status} status={data.management_object.status} keycloak={this.props.keycloak} />
                    {is_latest_version === false && data.management_object.status === "p" && under_construction === false && <section>
                        <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{"There is a "}<span className="bold">{"newer version "}</span>{"of this record available"}</span></div>
                    </section>}
                    <ResourceHeader
                        key={data.management_object ? 'header' + JSON.stringify(data.management_object) : 'header' + this.state.pk}
                        {...this.state}
                        logo={logo}
                        title={resourceName}
                        short_name={short_name}
                        version={version}
                        version_label={version_label}
                        version_Date={version_date}
                        resource_type={resource_type}
                        proxied={null}
                        entity_type={entity_type} default_image={default_image} under_construction={under_construction} for_information_only={for_information_only} keycloak={this.props.keycloak} data={data} pk={this.state.pk} updateRecord={this.updateRecord}
                        citationText={citationText} citationText_label={citationText_label}
                        citation_all_versions={citation_all_versions} citation_all_versions_label={citation_all_versions_label} tombstone={tombstone}
                        keywordsArray={keywords}
                        languagesArray={languages}
                        domainKeywordsArray={domainKeywords}
                        subjectKeywordsArray={subjectKeywords}
                        intentedKeywordsArray={intentedKeywords}
                        //ld_subclass={ld_subclass}
                        KeywordsLabel={keywords_labels}
                        DomainKeywordsLabel={domainKeywords_label}
                        subjectKeywordsLabel={subjectKeywords_label}
                        intentedKeywordsLabel={intentedKeywords_label}
                        //ld_subclassLabel={ld_subclass_label}
                        model_typeArray={model_typeArray}
                        model_type_label={model_type_label}
                        model_functionArray={model_functionArray}
                        model_function_label={model_function_label} inferred_language={inferred_language} description={description} url_type="ld" status={data.management_object.status} duplicate={data.management_object.duplicate} potential_duplicate={data.management_object.potential_duplicate} />
                </div>
                {(!tombstone || tombstone === "F") &&
                    <div className="tab-pane-container">
                        <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} tabTitles={tabTitles} />
                        <TabContent activeTab={this.state.tab}>
                            <TabPane tabId={`${tabTitles.indexOf('Overview') + 1}`}>
                                <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
                                    <Grid item xs={12} sm={8} md={8} className="search-results-main" >
                                        <DescriptionRichText description={description} />
                                        {/*<Keywords keywordsArray={keywords}
                                        languagesArray={languages}
                                        domainKeywordsArray={domainKeywords}
                                        subjectKeywordsArray={subjectKeywords}
                                        intentedKeywordsArray={intentedKeywords}
                                        //ld_subclass={ld_subclass}
                                        KeywordsLabel={keywords_labels}
                                        DomainKeywordsLabel={domainKeywords_label}
                                        subjectKeywordsLabel={subjectKeywords_label}
                                        intentedKeywordsLabel={intentedKeywords_label}
                                        //ld_subclassLabel={ld_subclass_label}
                                        model_typeArray={model_typeArray}
                                        model_type_label={model_type_label}
                                        model_functionArray={model_functionArray}
                                        model_function_label={model_function_label}
    />*/}
                                        <LdMediaPartType language_description_media_part={lr_subclass.language_description_media_part} metadataLanguage={metadataLanguage} inferred_language={inferred_language} />
                                        <LdUnspecifiedPartType unspecified_part={lr_subclass.unspecified_part} metadataLanguage={metadataLanguage} inferred_language={inferred_language} />



                                    </Grid>
                                    <Grid item xs={12} sm={4} md={4} className="MetadataSidebar" >

                                        {/*data.management_object.status === "p" && <ShareMetadata shareUrl={`${BASE_URL}catalogue/ld/${this.state.pk}`} title={resourceName} description={description} logo={logo} />*/}
                                        <RecordStats pk={this.state.pk} />
                                        <Versions all_versions={all_versions} all_versions_label={all_versions_label} metadataLanguage={metadataLanguage} versions_exceeding_limit={versions_exceeding_limit} additioanlInfo={additionalInfo} />
                                        <div className="ActionsButtonArea">
                                            {/*<Replaces data={data} metadataLanguage={metadataLanguage} />*/}
                                            {/*<IsReplacedWith data={data} metadataLanguage={metadataLanguage} />*/}
                                            <ResourceActor data={this.state.data.described_entity.field_value.resource_provider} metadataLanguage={metadataLanguage} />
                                            <FundedBy data={data} metadataLanguage={metadataLanguage} CorpusImage={CorpusImage} />
                                            <AdditionalInfo additioanlInfo={additionalInfo}
                                                mailingList={mailingList}
                                                discussionUrl={discussionUrl}
                                                data={data}
                                                metadataLanguage={metadataLanguage}
                                                AdditionalInfoLabel={additionalInfo_label}
                                                identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"}
                                                {...commonParser.getSourceOfMetadataRecord(data, metadataLanguage)}
                                            />

                                        </div>
                                        <ExportMetadata pk={this.state.pk} under_construction={under_construction} for_information_only={for_information_only} keycloak={this.props.keycloak} data={this.state.data} name={resourceName} {...this.state} />
                                    </Grid>
                                </Grid>
                                {grid_size &&
                                    <Grid container spacing={2} className="MetaDataDetailsBottomContainer">
                                        <Grid item xs={12} sm={grid_size} md={grid_size}>
                                            <Documentations data={data} metadataLanguage={metadataLanguage} showDocsFunction={this.showDocsFunction} showDocs={this.state.showDocs} />
                                            <Creation data={data} metadataLanguage={metadataLanguage} showCreationFunction={this.showCreationFunction} showCreation={this.state.showCreation} />
                                            <ResourceActor data={this.state.data.described_entity.field_value.ipr_holder} metadataLanguage={metadataLanguage} />
                                            <Attribution data={data} metadataLanguage={metadataLanguage} />

                                        </Grid>
                                        {second_column && <Grid item xs={12} sm={grid_size} md={grid_size}>
                                            <ActualUse data={data} metadataLanguage={metadataLanguage} />
                                            <Validation data={data} metadataLanguage={metadataLanguage} />
                                        </Grid>}

                                        <Grid item xs={12} sm={grid_size} md={grid_size}>
                                            <LdMediaPart data={data} metadataLanguage={metadataLanguage} />
                                            <Ethics personal_data_included={personal_data_included}
                                                personal_data_details={personal_data_details}
                                                sensitive_data_included={sensitive_data_included}
                                                sensitive_data_details={sensitive_data_details}
                                                anonymized={anonymized}
                                                anonymization_details={anonymization_details}
                                                personal_data_included_label={personal_data_included_label}
                                                personal_data_details_label={personal_data_details_label}
                                                sensitive_data_included_label={sensitive_data_included_label}
                                                sensitive_data_details_label={sensitive_data_details_label}
                                                anonymized_label={anonymized_label}
                                                anonymization_details_label={anonymization_details_label}
                                                showEthicsFunction={this.showEthicsFunction}
                                                showEthics={this.state.showEthics}
                                            />
                                            <CorpusGeneralCategories data={data} metadataLanguage={metadataLanguage} title="Language description categories" showAreaFunction={this.showAreaFunction} showArea={this.state.showArea} />

                                        </Grid>


                                    </Grid>
                                }
                            </TabPane>

                            {(!under_construction || (under_construction && IS_ADMIN(this.props.keycloak))) && <TabPane tabId={`${tabTitles.indexOf('Download') + 1}`}>
                                <Distributions data={data} metadataLanguage={metadataLanguage} {...this.state} />
                            </TabPane>}

                            <TabPane tabId={`${tabTitles.indexOf('Try out') + 1}`}>
                                <TryOutModelsTab data={this.state.data} metadataLanguage={metadataLanguage} />
                            </TabPane>

                            <TabPane tabId={`${tabTitles.indexOf('Related LRTs') + 1}`}>
                                {/*<HasSubset data={data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />*/}
                                <Annotation data={data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />
                                <Relations data={this.state.data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />
                                {/*<PhysicalResource physical_resource={physical_resource} physical_resource_label={physical_resource_label} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />*/}
                                {/*<LdGeneral
                                personal_data_included={personal_data_included}
                                personal_data_details={personal_data_details}
                                sensitive_data_included={sensitive_data_included}
                                sensitive_data_details={sensitive_data_details}
                                anonymized={anonymized}
                                anonymization_details={anonymization_details}
                                data={data}
                                metadataLanguage={metadataLanguage}
                                displayTabGeneralFunction={this.displayTabGeneralFunction}
                                displayTabGeneral={this.state.displayTabGeneral}
                                peresonal_data_included_label={personal_data_included_label}
                                peresonal_data_details_label={personal_data_details_label}
                                sensitive_data_included_label={sensitive_data_included_label}
                                sensitive_data_details_label={sensitive_data_details_label}
                                anonymized_label={anonymized_label}
                            anonymization_details_label={anonymization_details_label}
                            />*/}
                            </TabPane>

                        </TabContent>
                    </div>}
            </Container>

        </div >
    }

}