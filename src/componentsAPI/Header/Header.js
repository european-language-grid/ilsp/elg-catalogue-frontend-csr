import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import elgLogo from "../../assets/images/elg-logo.svg";
import UserIconComponent from "./UserIconComponent";
import AdminHeader from "./AdminHeader";
import CreateResourceHeader from "./CreateResourceHeader";
import MyResourcesHeader from "./MyResourcesHeader";
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import { ReactComponent as MenuIcon } from "../../assets/elg-icons/layout-dashboard.svg";
import CloseIcon from '@material-ui/icons/Close';
import { TILDE_MAIN_MENU_API, HEADER_BASE_URL } from "../../config/constants";
//import Typography from '@material-ui/core/Typography';
import UserDashboardComponent from "./UserDashboardComponent";
import Divider from '@material-ui/core/Divider';

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = { headerData: '', keycloak: props.keycloak, error: "", isOpen: false, width: window.innerWidth };
        this.isUnmount = false;
    }

    componentDidMount() {
        this.isUnmount = true;
        axios.get(TILDE_MAIN_MENU_API).then(res => {
            if (this.isUnmount) {
                this.setState({ headerData: res.data, error: "" });
            }
        }).catch(res => {
            if (this.isUnmount) {
                this.setState({ error: res });
            }
        });
    }

    fixMenuPathPrefix = (field_elg_menu_link_type, relative) => {
        switch (field_elg_menu_link_type) {
            case "CMS":
                //relative = `page/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "React":
                relative = `catalogue/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "External":
                break;
            case "Angular":
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            default:
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
        }
        return relative;
    }

    handleClose = () => {
        this.setState({ isOpen: false });
    };

    handleToggleOpen = () => {
        this.setState(prevState => ({
            isOpen: !prevState.isOpen
        }));
    };


    errorHeader = () => {
        return <Container maxWidth="xl" style={{ marginTop: "2%" }}>
            <Grid container direction="row" justifyContent="center" alignItems="center" >
                <Grid item container xs={12} sm={12} direction="row" justifyContent="center" alignItems="center" >
                    <Grid item xs={4} sm={3}>
                        <a href={HEADER_BASE_URL}>
                            <img className="header__logo" src={elgLogo} alt="elg logo" /></a>
                    </Grid>
                    <Grid item container xs={8} sm={9} className="header-menu-container" >
                        <Grid item xs={12} sm={12}>
                            <UserIconComponent keycloak={this.state.keycloak} baseURL={HEADER_BASE_URL} />
                            <UserDashboardComponent keycloak={this.state.keycloak} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    }

    render() {
        if (this.state.error) {
            return this.errorHeader();
        }
        if (!this.state.headerData) {
            return <div></div>
        }
        const menuArray = this.state.headerData;
        const { isOpen } = this.state;
        const { width } = this.state;
        const isMobile = width <= 500;

        const baseURL = `${HEADER_BASE_URL}`;
        const isloggedin = this.state.keycloak ? Boolean(this.state.keycloak.authenticated) : false;

        return <Container maxWidth="xl" style={{ marginTop: "1%" }}>
            <Grid container direction="row" justifyContent="center" alignItems="center" > {/*className="header-inner"*/}
                <Grid item container xs={4} sm={3} className="header-logo-container" justifyContent="flex-start">
                    <Grid item sm={8} xs={12}>
                        <a href={baseURL}>
                            <img className="header__logo" src={elgLogo} alt="elg logo" />
                        </a>
                        <span className="header__logo-badge">RELEASE 2</span>
                    </Grid>

                </Grid>

                <Grid item container xs={8} sm={9} md={9} className="header-menu-container" justifyContent="flex-end" alignItems="center">
                    {!isMobile && isloggedin && <Grid item xs={9}>
                        <UserDashboardComponent keycloak={this.state.keycloak} />
                    </Grid>
                    }

                    {isMobile && isloggedin && <Grid item xs={12}>
                        <UserIconComponent keycloak={this.state.keycloak} baseURL={baseURL} />
                    </Grid>}


                    {!isMobile && <Grid item xs={isloggedin ? 3 : 12}>
                        <UserIconComponent keycloak={this.state.keycloak} baseURL={baseURL} />
                    </Grid>}


                    {isMobile && (<Grid item xs={12} sm={12} md={12} className="menu-humburger"> <Button className="hamburger-button" onClick={this.handleToggleOpen}><MenuIcon className="small-icon" /></Button>
                        <Drawer anchor="right"
                            onClose={this.handleClose}
                            open={isOpen}
                        >
                            <div className="fr-1">
                                <Button className="hamburger-button mt-2" onClick={this.handleToggleOpen}><CloseIcon /></Button>
                            </div>
                            <List>
                                <ListItem><AdminHeader keycloak={this.state.keycloak} show_icon={false} /> </ListItem>
                                <ListItem> <CreateResourceHeader keycloak={this.state.keycloak} show_icon={false} /> </ListItem>
                                <ListItem> <MyResourcesHeader keycloak={this.state.keycloak} /> </ListItem>
                                <Divider />
                                {
                                    menuArray.map((menuItem, menuItemIndex) => {
                                        const { title } = menuItem || "";
                                        const { field_show_as_not_clickable, field_elg_menu_link_type } = menuItem || null;
                                        let { relative } = menuItem || "";
                                        relative = this.fixMenuPathPrefix(field_elg_menu_link_type, relative);
                                        const final_menu_url = field_elg_menu_link_type === "External" ? relative : `${baseURL}${relative}`;
                                        return field_show_as_not_clickable !== "1" ?
                                            <ListItem key={menuItemIndex}><a className="ml-3 nav-item ng-star-inserted active-list-item headerMenu" href={`${final_menu_url}`}>{title}</a> </ListItem>
                                            :
                                            <ListItem key={menuItemIndex}><button disabled style={{ cursor: 'auto' }} className="linkButton ml-3 nav-item ng-star-inserted active-list-item headerMenu">{title}</button> </ListItem>
                                    })
                                }
                            </List>

                        </Drawer> </Grid>)
                    }

                    {
                        !isMobile && (<> <Grid item container direction="row" justifyContent="center" alignItems="center">
                            <Grid item xs={12} sm={12} md={12}>
                                <ul className="menu">

                                    {menuArray.map((menuItem, menuItemIndex) => {
                                        const { title } = menuItem || "";
                                        const { field_show_as_not_clickable, field_elg_menu_link_type } = menuItem || null;
                                        let { relative } = menuItem || "";
                                        relative = this.fixMenuPathPrefix(field_elg_menu_link_type, relative);
                                        const final_menu_url = field_elg_menu_link_type === "External" ? relative : `${baseURL}${relative}`;
                                        return field_show_as_not_clickable !== "1" ?
                                            <li key={menuItemIndex}><a className="ml-3 nav-item ng-star-inserted active-list-item headerMenu" href={`${final_menu_url}`}>{title}</a> </li>
                                            :
                                            <li key={menuItemIndex}><button disabled style={{ cursor: 'auto' }} className="linkButton ml-3 nav-item ng-star-inserted active-list-item headerMenu">{title}</button> </li>
                                    })
                                    }
                                </ul>
                            </Grid>
                        </Grid>
                        </>)
                    }


                </Grid>
            </Grid>

        </Container>
    }
}