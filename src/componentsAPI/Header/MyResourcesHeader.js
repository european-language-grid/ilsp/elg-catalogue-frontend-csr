import React from "react";
import { Link } from "react-router-dom";
import { SHOW_MY_ITEMS_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";

export default class MyResourcesHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isOpen: false, UserRoles: [] };

    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        SHOW_MY_ITEMS_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        return isAuthorized;
    }

    render() {
        if (!this.isAuthorizedToView(this.state.UserRoles)) {
            return <></>
        }
        return <span>
            <Link className="mx-3 nav-item ng-star-inserted active-list-item headerMenu" to="/myItems">My items</Link>
        </span>
    }
}