import React from "react";
import { withRouter, Link } from "react-router-dom";
import axios from "axios";
import "../../../assets/icons-and-css-variables/colors.css";
import "../../../assets/icons-and-css-variables/variables.css";
import "../../../assets/icons-and-css-variables/icons.css";
import "./styles.css";
import "./nav.css";
//import "./footer.css";
import { PRODUCTION, BASE_URL, getKeycloakLink, TILDE_MAIN_MENU_API, HEADER_BASE_URL, getLogoutUrl } from "../../../config/constants";
import header_json from "../../../data/header_json.json";

class UniversalHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = { data: null }
    }

    componentDidMount() {
        //fetch("/assets/icons-and-css-variables/icons.html")
        this.getHeaderData();
    }

    uncheckInputs = () => {
        //console.log("uncheckInputs")
        if (document.activeElement instanceof HTMLElement) document.activeElement.blur();
        var clist = document.getElementsByTagName('input');
        for (var i = 0; i < clist.length; ++i) {
            clist[i].checked = false;
        }
    }

    fetchIcons = () => {
        fetch(`${window.location.origin}${(BASE_URL.indexOf('european-language-grid.eu/') >= 0 || window.location.origin.indexOf("192.168.188.194") >= 0 || PRODUCTION) ? '/catalogue/' : '/'}icons.html`)
            .then(response => {
                return response.text()
            })
            .then(data => {
                document.querySelector("#icons").innerHTML = data;
            });
    }

    getHeaderData = () => {
        axios.get(TILDE_MAIN_MENU_API).then(res => this.setState({ data: res.data }, this.fetchIcons)).catch(err => { this.setState({ data: header_json }, this.fetchIcons) });
    }

    getExternalIcon = () => {
        return <svg className="xxs">
            <use href="#external-link"></use>
        </svg>
    }

    getReactLinkAddress = (url) => {
        let reactLinkAddress = url.substring(url.indexOf("/catalogue/") + "/catalogue/".length);
        return reactLinkAddress ? ("/" + reactLinkAddress).replace("//", "/") : "/";
    }

    normilizeID = (title) => {
        return title.replace(/\s+/g, '-').toLowerCase().replace("&", "-").replace('--', '-');
    }

    fixMenuPathPrefix = (field_elg_menu_link_type, relative) => {
        switch (field_elg_menu_link_type) {
            case "CMS":
                //relative = `page/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "React":
                relative = `catalogue/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "External":
                break;
            case "Angular":
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                relative = relative === "news-page" ? "news" : relative;
                break;
            default:
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
        }
        return relative;
    }


    getChildItem = (item, index) => {
        const reactLink = item.field_elg_menu_link_type === "React";
        const isExternalLink = item.field_elg_menu_link_type === "External";
        const relative = this.fixMenuPathPrefix(item.field_elg_menu_link_type, item.relative);
        const final_menu_url = item.field_elg_menu_link_type === "External" ? item.relative : `${HEADER_BASE_URL}${relative}`;
        const hasBelow = item.below && item.below.length > 0;

        if (hasBelow) {
            return <li key={item.key} tabIndex={index} className={item.field_show_as_dropdown === "1" ? "dropdown" : void 0}>
                {reactLink ? <Link className="a-label__chevron" to={this.getReactLinkAddress(item.uri)} onClick={this.uncheckInputs}>{item.title}</Link> : <a className="a-label__chevron" rel={isExternalLink ? "noopener" : void 0} href={final_menu_url}>{item.title}{isExternalLink && this.getExternalIcon()}</a>}
                <button><label htmlFor={this.normilizeID(item.title)}>
                    <svg className="xs">
                        <use href="#arrow-right"></use>
                    </svg>
                </label></button>
                <input type="checkbox" id={this.normilizeID(item.title)} name={this.normilizeID(item.title)}
                    className="m-menu__checkbox" />
                <div className="m-menu">
                    <div className="m-menu__header">
                        <button><label className="m-menu__toggle" htmlFor={this.normilizeID(item.title)}>
                            <svg className="xs">
                                <use href="#arrow-left"></use>
                            </svg>
                        </label></button>
                        <span>{item.title}</span>
                    </div>
                    <ul>
                        {
                            item.below.map((below, belowIndex) => {
                                return this.getChildItem(below, belowIndex);
                            })
                        }
                    </ul>
                </div>
            </li>
        } else {
            return <li key={item.key} tabIndex={index} className={item.field_show_as_dropdown === "1" ? "dropdown" : void 0}>
                {reactLink ? <Link className="a-label__chevron" to={this.getReactLinkAddress(item.uri)} onClick={this.uncheckInputs}>{item.title}</Link> : <a className="a-label__chevron" rel={isExternalLink ? "noopener" : void 0} href={final_menu_url}>{item.title}{isExternalLink && this.getExternalIcon()}</a>}
            </li>
        }
    }

    render() {
        if (!this.state.data) {
            return <></>;
        }
        const { given_name = "", family_name = "", email = "" } = (this.props.keycloak && this.props.keycloak.authenticated && this.props.keycloak.idTokenParsed) || "";
        return <>
            <div id="icons"></div>
            <div className="nav-wrapper">
                <nav>
                    <div className="logo">
                        <a href={BASE_URL} className="elg-logo"> </a>
                    </div>

                    <div className="items">

                        <div className="profil">
                            {!email && <a href={getKeycloakLink(this.props.keycloak, "login", new URL("/catalogue" + this.props.history.location.pathname, getLogoutUrl()))}><svg className="xxs"><use href="#account-circle"></use></svg></a>}
                            {email && <Link to="/mygrid/"><span>My grid&nbsp;</span><svg className="xxs">
                                <use href="#layout-dashboard"></use>
                            </svg></Link>}
                            {email && <a href={`${HEADER_BASE_URL}profile`}><span>{given_name} {family_name}</span></a>}
                            {email && <a href={getKeycloakLink(this.props.keycloak, "logout")}><svg className="xxs">
                                <use href="#logout"></use>
                            </svg></a>}
                        </div>

                        <div className="m">
                            <input type="checkbox" id="menu" name="menu" className="m-menu__checkbox" />
                            <label className="m-menu__toggle" htmlFor="menu">
                                <svg className="xs">
                                    <use href="#navigation-menu"></use>
                                </svg>
                            </label>

                            <div className="m-menu">

                                <div className="m-menu__header">
                                    <button><label className="m-menu__toggle" htmlFor="menu">
                                        <svg className="xs">
                                            <use href="#remove-circle"></use>
                                        </svg>
                                    </label></button>
                                    <span></span>
                                </div>

                                <ul>
                                    {
                                        this.state.data.map((item, index) => {
                                            return this.getChildItem(item, index);
                                        })
                                    }
                                </ul>
                            </div>

                        </div>
                    </div>

                </nav>
            </div >
        </>
    }
}
export default withRouter(UniversalHeader);