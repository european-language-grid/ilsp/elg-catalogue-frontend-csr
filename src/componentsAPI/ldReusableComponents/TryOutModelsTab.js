import React from "react";
import { Link } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import GeneralIdentifier from "../CommonComponents/GeneralIdentifier";
import commonParser from "../../parsers/CommonParser";

export default class TryOutModelsTab extends React.Component {

    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <></>;
        }
        const { is_ml_model_of_functional, resource_name = { field_value: { en: "" } } } = data.described_entity?.field_value;
        if (!is_ml_model_of_functional) {
            return <></>;
        }
        return <>

            <Typography className="bold-p--id pb-3"> <span className="teal--font">{resource_name.field_value[metadataLanguage] || resource_name.field_value["en"]} </span>is being used by the following ELG integrated services</Typography>
            {false && <Typography className="bold-p--id">{is_ml_model_of_functional.field_label[metadataLanguage] || is_ml_model_of_functional.field_label["en"]}</Typography>}
            {is_ml_model_of_functional.field_value && is_ml_model_of_functional.field_value.map((item, index) => {
                let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                let version = item.version?.field_value || "";
                let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                return <div key={index}>
                    {full_metadata_record ?
                        <div className="internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={`${full_metadata_record.internalELGUrl}/try out`}>
                                <span className="info_value">{resource_name} ({version ? version : ''})</span>
                            </Link>
                            {false && <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />}
                        </div> :
                        <div>
                            <div className="info_value">{resource_name} ({version ? version : ''})</div>
                            <GeneralIdentifier data={item} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                        </div>
                    }
                </div>
            })}

        </>
    }
}