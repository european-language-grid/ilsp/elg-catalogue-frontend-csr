import React from "react";
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
import MediaPartIconType from "../CommonComponents/MediaPartIconType";
import Chip from '@material-ui/core/Chip';
import Languages from "../CommonComponents/Languages";

function a11yProps(index) {
    return {
        id: `wrapped-tab-${index}`,
        'aria-controls': `wrapped-tabpanel-${index}`,
    };
}

export default class LdUnspecifiedPartType extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0 };
        this.toggleTab = this.toggleTab.bind(this);
    }

    toggleTab(tabIndex) {
        this.setState({ tab: tabIndex });
    }

    render() {
        const { unspecified_part, metadataLanguage, inferred_language } = this.props;
        if (!unspecified_part) {
            return <div></div>
        }
        const mediaPart = unspecified_part.field_value;

        let linguality_type = mediaPart.linguality_type ? (mediaPart.linguality_type.label[metadataLanguage] || mediaPart.linguality_type.label[Object.keys(mediaPart.linguality_type.label)[0]]) : "";
        let linguality_type_label = mediaPart.linguality_type ? (mediaPart.linguality_type.field_label[metadataLanguage] || mediaPart.linguality_type.field_label["en"]) : "";
        let multilinguality_type = mediaPart.multilinguality_type ? (mediaPart.multilinguality_type.label[metadataLanguage] || mediaPart.multilinguality_type.label[Object.keys(mediaPart.multilinguality_type.label)[0]]) : "";
        let multilinguality_type_label = mediaPart.multilinguality_type ? (mediaPart.multilinguality_type.field_label[metadataLanguage] || mediaPart.multilinguality_type.field_label["en"]) : "";
        let multilinguality_type_details = (mediaPart.multilinguality_type_details && (mediaPart.multilinguality_type_details.field_value[metadataLanguage] || mediaPart.multilinguality_type_details.field_value[Object.keys(mediaPart.multilinguality_type_details.field_value)[0]])) || "";
        let multilinguality_type_details_label = mediaPart.multilinguality_type_details ? (mediaPart.multilinguality_type_details.field_label[metadataLanguage] || mediaPart.multilinguality_type_details.field_label["en"]) : "";
         
        let modality_type_label = mediaPart.modality_type ? (mediaPart.modality_type.field_label[metadataLanguage] || mediaPart.modality_type.field_label["en"]) : "";
        let modality_typeArray = (mediaPart.modality_type && mediaPart.modality_type.field_value.length > 0 && mediaPart.modality_type.field_value.map(modality => modality.label[metadataLanguage] || modality.label[Object.keys(modality.label)[0]])) || [];
       

        return <div>
            {/*<Typography variant="h3" className="section-links">{unspecified_part.field_label[metadataLanguage] || unspecified_part.field_label["en"]}</Typography>*/}
            <div className="tabs-main-container">
                <div className="vertical-tabs-container">
                    <Tabs value={this.state.tab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs">
                        {unspecified_part && 
                           
                            //let media_type = mediaPart.media_type ? (mediaPart.media_type.label[metadataLanguage] || mediaPart.media_type.label[Object.keys(mediaPart.media_type.label)[0]]) : "";
                            //let media_type_label = mediaPart.media_type ? (mediaPart.media_type.field_label[metadataLanguage] || mediaPart.media_type.field_label["en"]) : "";

                            <Tab label={<div><span className=""><MediaPartIconType media_type={"unspecified"} /></span></div>} {...a11yProps(0)}  />
                        }
                    </Tabs>

                   
                    <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" justifyContent="center" alignItems="flex-start" spacing={2}>
                            <Grid item xs={12}>
                                    {<Languages language={mediaPart.language} from_tool={false} inferred_language={inferred_language}/>}
                                    {<Languages language={mediaPart.metalanguage} from_tool={false} inferred_language={inferred_language}/>}
                                </Grid>

                                <Grid item xs={12}>
                                    {/*media_type && <div className="padding15"><Typography className="bold-p--id">{media_type_label}</Typography><span className="info_value">{media_type}</span></div>*/}
                                    {linguality_type && <div className="padding15"><Typography className="bold-p--id">{linguality_type_label}</Typography><span className="info_value">{linguality_type}</span></div>}
                                    {multilinguality_type && <div className="padding15"><Typography className="bold-p--id">{multilinguality_type_label}</Typography><span className="info_value">{multilinguality_type}</span></div>}
                                    {multilinguality_type_details && <div className="padding15"><Typography className="bold-p--id">{multilinguality_type_details_label}</Typography><span className="info_value">{multilinguality_type_details}</span></div>}
                                    {modality_typeArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{modality_type_label}</Typography><div className="flex wrap">{modality_typeArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                     
                                </Grid>
                                
                            </Grid>
                        </VerticalTabPanel>
                   
                </div>
            </div>
        </div>
    }
}

