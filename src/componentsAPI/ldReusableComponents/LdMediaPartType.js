import React from "react";
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../CustomVerticalTabs/VerticalTabPanel';
import LcrStaticElement from '../lcrReusableComponents/LcrStaticElement';
import LcrDynamicElement from '../lcrReusableComponents/LcrDynamicElement';
import Grid from '@material-ui/core/Grid';
import MediaPartIconType from "../CommonComponents/MediaPartIconType";
import Chip from '@material-ui/core/Chip';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import Languages from "../CommonComponents/Languages";

function a11yProps(index) {
    return {
        id: `wrapped-tab-${index}`,
        'aria-controls': `wrapped-tabpanel-${index}`,
    };
}

export default class LdMediaPartType extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0 };
        this.toggleTab = this.toggleTab.bind(this);
    }

    toggleTab(tabIndex) {
        this.setState({ tab: tabIndex });
    }

    render() {
        const { language_description_media_part, metadataLanguage, inferred_language } = this.props;
        if (!language_description_media_part) {
            return <div></div>
        }

        return <div>
            <Typography variant="h3" className="section-links">{language_description_media_part.field_label[metadataLanguage] || language_description_media_part.field_label["en"]}</Typography>
            <div className="tabs-main-container">
                <div className="vertical-tabs-container">
                    <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs">
                        {language_description_media_part && language_description_media_part.field_value.map((mediaPart, mediaPartIndex) => {
                            //let ld_media_type = mediaPart.ld_media_type ? mediaPart.ld_media_type.field_value : "";
                            let media_type = mediaPart.media_type ? (mediaPart.media_type.label[metadataLanguage] || mediaPart.media_type.label[Object.keys(mediaPart.media_type.label)[0]]) : "";
                            //let media_type_label = mediaPart.media_type ? (mediaPart.media_type.field_label[metadataLanguage] || mediaPart.media_type.field_label["en"]) : "";

                            return <Tab key={media_type + mediaPartIndex} label={<div><span className=""><MediaPartIconType media_type={media_type} /></span><span>{media_type}</span></div>} {...a11yProps(mediaPartIndex)} onClick={() => { this.toggleTab(mediaPartIndex); }} />
                        })}
                    </Tabs>

                    {language_description_media_part.field_value.map((mediaPart, mediaPartIndex) => {
                        //let ld_media_type = mediaPart.ld_media_type ? mediaPart.ld_media_type.field_value : "";
                        let linguality_type = mediaPart.linguality_type ? (mediaPart.linguality_type.label[metadataLanguage] || mediaPart.linguality_type.label[Object.keys(mediaPart.linguality_type.label)[0]]) : "";
                        let linguality_type_label = mediaPart.linguality_type ? (mediaPart.linguality_type.field_label[metadataLanguage] || mediaPart.linguality_type.field_label["en"]) : "";
                        let multilinguality_type = mediaPart.multilinguality_type ? (mediaPart.multilinguality_type.label[metadataLanguage] || mediaPart.multilinguality_type.label[Object.keys(mediaPart.multilinguality_type.label)[0]]) : "";
                        let multilinguality_type_label = mediaPart.multilinguality_type ? (mediaPart.multilinguality_type.field_label[metadataLanguage] || mediaPart.multilinguality_type.field_label["en"]) : "";
                        let multilinguality_type_details = (mediaPart.multilinguality_type_details && (mediaPart.multilinguality_type_details.field_value[metadataLanguage] || mediaPart.multilinguality_type_details.field_value[Object.keys(mediaPart.multilinguality_type_details.field_value)[0]])) || "";
                        let multilinguality_type_details_label = mediaPart.multilinguality_type_details ? (mediaPart.multilinguality_type_details.field_label[metadataLanguage] || mediaPart.multilinguality_type_details.field_label["en"]) : "";
                         
                         
                         
                        //let type_of_image_content = (mediaPart.type_of_image_content && (mediaPart.type_of_image_content.field_value[metadataLanguage] || mediaPart.type_of_image_content.field_value[Object.keys(mediaPart.type_of_image_content.field_value)[0]])) || "";
                        let type_of_image_content = []; 
                        (mediaPart.type_of_image_content && mediaPart.type_of_image_content.field_value.length > 0 && mediaPart.type_of_image_content.field_value.map((keyword, DIndex) => {
                            type_of_image_content.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                        }));
                        let type_of_image_content_label = mediaPart.type_of_image_content ? (mediaPart.type_of_image_content.field_label[metadataLanguage] || mediaPart.type_of_image_content.field_label["en"]) : "";
                        //let type_of_video_content = (mediaPart.type_of_video_content && (mediaPart.type_of_video_content.field_value[metadataLanguage] || mediaPart.type_of_video_content.field_value[Object.keys(mediaPart.type_of_video_content.field_value)[0]])) || "";
                        let type_of_video_content = []; 
                        (mediaPart.type_of_video_content && mediaPart.type_of_video_content.field_value.length > 0 && mediaPart.type_of_video_content.field_value.map((keyword, DIndex) => {
                            type_of_video_content.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                        }));
                        let type_of_video_content_label = mediaPart.type_of_video_content ? (mediaPart.type_of_video_content.field_label[metadataLanguage] || mediaPart.type_of_video_content.field_label["en"]) : "";
                        let text_included_in_imageArray = (mediaPart.text_included_in_image && mediaPart.text_included_in_image.field_value.length > 0 && mediaPart.text_included_in_image.field_value.map(textincluded => textincluded.label[metadataLanguage] || textincluded.label[Object.keys(textincluded.label)[0]])) || [];
                        let text_included_in_image_label = mediaPart.text_included_in_image ? (mediaPart.text_included_in_image.field_label[metadataLanguage] || mediaPart.text_included_in_image.field_label["en"]) : "";
                        let text_included_in_videoArray = (mediaPart.text_included_in_video && mediaPart.text_included_in_video.field_value.length > 0 && mediaPart.text_included_in_video.field_value.map(textincluded => textincluded.label[metadataLanguage] || textincluded.label[Object.keys(textincluded.label)[0]])) || [];
                        let text_included_in_video_label = mediaPart.text_included_in_video ? (mediaPart.text_included_in_video.field_label[metadataLanguage] || mediaPart.text_included_in_video.field_label["en"]) : "";
                        let modality_type_label = mediaPart.modality_type ? (mediaPart.modality_type.field_label[metadataLanguage] || mediaPart.modality_type.field_label["en"]) : "";
                        let modality_typeArray = (mediaPart.modality_type && mediaPart.modality_type.field_value.length > 0 && mediaPart.modality_type.field_value.map(modality => modality.label[metadataLanguage] || modality.label[Object.keys(modality.label)[0]])) || [];
                        let speech_item = (mediaPart.speech_item && mediaPart.speech_item.field_value.length > 0 && mediaPart.speech_item.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)][0])) || [];
                        let speech_item_label = mediaPart.speech_item ? (mediaPart.speech_item.field_label[metadataLanguage] || mediaPart.speech_item.field_label["en"]) : "";
                        let non_speech_item = (mediaPart.non_speech_item && mediaPart.non_speech_item.field_value.length > 0 && mediaPart.non_speech_item.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)][0])) || [];
                        let non_speech_item_label = mediaPart.non_speech_item ? (mediaPart.non_speech_item.field_label[metadataLanguage] || mediaPart.non_speech_item.field_label["en"]) : "";
                        let legend = mediaPart.legend ? (mediaPart.legend.field_value[metadataLanguage] || mediaPart.legend.field_value[Object.keys(mediaPart.legend.field_value)[0]]) : "";
                        let legend_label = mediaPart.legend ? (mediaPart.legend.field_label[metadataLanguage] || mediaPart.legend.field_label["en"]) : "";
                        let noise_level = mediaPart.noise_level ? (mediaPart.noise_level.label[metadataLanguage] || mediaPart.noise_level.label[Object.keys(mediaPart.noise_level.label)[0]]) : "";
                        let noise_level_label = mediaPart.noise_level ? (mediaPart.noise_level.field_label[metadataLanguage] || mediaPart.noise_level.field_label["en"]) : "";
                        let creation_details_label = mediaPart.creation_details ? (mediaPart.creation_details.field_label[metadataLanguage] || mediaPart.creation_details.field_label["en"]) : "";
                        let creation_details = mediaPart.creation_details ? (mediaPart.creation_details.field_value[metadataLanguage] || mediaPart.creation_details.field_value[Object.keys(mediaPart.creation_details.field_value)[0]]) : "";
                        let creation_mode_label = mediaPart.creation_mode ? (mediaPart.creation_mode.field_label[metadataLanguage] || mediaPart.creation_mode.field_label["en"]) : "";
                        let creation_mode = mediaPart.creation_mode ? (mediaPart.creation_mode.label[metadataLanguage] || mediaPart.creation_mode.label[Object.keys(mediaPart.creation_mode.label)[0]]) : "";
                        let is_created_by_label = mediaPart.is_created_by ? (mediaPart.is_created_by.field_label[metadataLanguage] || mediaPart.is_created_by.field_label["en"]) : "";
                        let is_created_byArray = (mediaPart.is_created_by && mediaPart.is_created_by.field_value.map((createdBy, createdByIndex) => {
                            let resource_name = createdBy.resource_name ? (createdBy.resource_name.field_value[metadataLanguage] || createdBy.resource_name.field_value[Object.keys(createdBy.resource_name.field_value)[0]]) : "";
                            let version = createdBy.version ? createdBy.version.field_value : "";
                            let full_metadata_record = commonParser.getFullMetadata(createdBy.full_metadata_record);
                            return <div key={createdByIndex}>
                                {full_metadata_record ?
                                    <div className="padding5 internal_url">
                                        <span><NavIcon className="xsmall-icon mr-05" /></span>
                                        <Link to={full_metadata_record.internalELGUrl}>
                                            <span className="info_value">{resource_name} ({version})</span>
                                        </Link>
                                    </div> :
                                    <span className="info_value">{resource_name} ({version})</span>
                                }
                            </div>
                        })) || [];
                        let has_original_source_label = mediaPart.has_original_source ? (mediaPart.has_original_source.field_label[metadataLanguage] || mediaPart.has_original_source.field_label["en"]) : "";
                        let has_original_sourceArray = (mediaPart.has_original_source && mediaPart.has_original_source.field_value.map((originalSource, originalSourceIndex) => {
                            let resource_name = originalSource.resource_name ? (originalSource.resource_name.field_value[metadataLanguage] || originalSource.resource_name.field_value[Object.keys(originalSource.resource_name.field_value)[0]]) : "";
                            let version = originalSource.version ? originalSource.version.field_value : "";
                            let full_metadata_record = commonParser.getFullMetadata(originalSource.full_metadata_record);
                            return <div key={originalSourceIndex}>
                                {full_metadata_record ?
                                    <div className="padding5 internal_url">
                                        <span><NavIcon className="xsmall-icon mr-05" /></span>
                                        <Link to={full_metadata_record.internalELGUrl}>
                                            <span className="info_value">{resource_name} ({version})</span>
                                        </Link>
                                    </div> :
                                    <span className="info_value">{resource_name} ({version})</span>
                                }
                            </div>
                        })) || [];
                        let original_source_description_label = mediaPart.original_source_description ? (mediaPart.original_source_description.field_label[metadataLanguage] || mediaPart.original_source_description.field_label["en"]) : "";
                        let original_source_description = mediaPart.original_source_description ? (mediaPart.original_source_description.field_value[metadataLanguage] || mediaPart.original_source_description.field_value[Object.keys(mediaPart.original_source_description.field_value)[0]]) : "";

                        let static_elementArray = mediaPart.static_element;
                        let dynamic_elementArray = mediaPart.dynamic_element;
                        let dynamic_element_label = mediaPart.dynamic_element ? (mediaPart.dynamic_element.field_label[metadataLanguage] || mediaPart.dynamic_element.field_label["en"]) : ""; 
                        let static_element_label = mediaPart.static_element ? (mediaPart.static_element.field_label[metadataLanguage] || mediaPart.static_element.field_label["en"]) : ""; 

                        return <VerticalTabPanel key={mediaPartIndex} value={this.state.tab} index={mediaPartIndex} className="vertical-tab-pannel">
                            <Grid container direction="row" justifyContent="center" alignItems="flex-start" spacing={2}>
                                <Grid item xs={12}>
                                    {<Languages language={mediaPart.language} from_tool={false} inferred_language={inferred_language} />}
                                    {<Languages language={mediaPart.metalanguage} from_tool={false} inferred_language={inferred_language}/>}
                                </Grid>
                                <Grid item xs={6}>
                                    {/*media_type && <div className="padding15"><Typography className="bold-p--id">{media_type_label}</Typography><span className="info_value">{media_type}</span></div>*/}
                                    {linguality_type && <div className="padding15"><Typography className="bold-p--id">{linguality_type_label}</Typography><span className="info_value">{linguality_type}</span></div>}
                                    {multilinguality_type && <div className="padding15"><Typography className="bold-p--id">{multilinguality_type_label}</Typography><span className="info_value">{multilinguality_type}</span></div>}
                                    {multilinguality_type_details && <div className="padding15"><Typography className="bold-p--id">{multilinguality_type_details_label}</Typography><span className="info_value">{multilinguality_type_details}</span></div>}
                                    {/*languageInfoArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{language_label}</Typography><span className="info_value">{languageInfoArray.map((item, index) => <span key={index} className="border-bottom-2">{item}</span>)}</span></div>*/}
                                    {/*metalanguageInfoArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{metalanguage_label}</Typography><span className="info_value">{metalanguageInfoArray.map((item, index) => <span key={index} className="border-bottom-2">{item}</span>)}</span></div>*/}
                                    

                                    {modality_typeArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{modality_type_label}</Typography><div className="flex wrap">{modality_typeArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {creation_mode && <div className="padding15"><Typography className="bold-p--id">{creation_mode_label}</Typography><span className="info_value">{creation_mode}</span></div>}
                                    {is_created_byArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{is_created_by_label}</Typography><span className="info_value">{is_created_byArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                    {creation_details && <div className="padding15"><Typography className="bold-p--id">{creation_details_label}</Typography><span className="info_value">{creation_details}</span></div>}
                                    {static_elementArray  && <Typography className="section-headings">{static_element_label}</Typography>}
                                    {<LcrStaticElement static_elementArray={static_elementArray} metadataLanguage={metadataLanguage} />}
                                </Grid>
                                <Grid item xs={6}>
                                    {has_original_sourceArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{has_original_source_label}</Typography><span className="info_value">{has_original_sourceArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                    {original_source_description && <div className="padding15"><Typography className="bold-p--id">{original_source_description_label}</Typography><span className="info_value">{original_source_description}</span></div>}
                                    {speech_item.length > 0 && <div className="padding15"><Typography className="bold-p--id">{speech_item_label}</Typography><div className="flex wrap">{speech_item.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {non_speech_item.length > 0 && <div className="padding15"><Typography className="bold-p--id">{non_speech_item_label}</Typography><div className="flex wrap">{non_speech_item.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {legend && <div className="padding15"><Typography className="bold-p--id">{legend_label}</Typography><span className="info_value">{legend}</span></div>}
                                    {noise_level && <div className="padding15"><Typography className="bold-p--id">{noise_level_label}</Typography><span className="info_value">{noise_level}</span></div>}
                                    {type_of_image_content.length > 0 && <div className="padding15"><Typography className="bold-p--id">{type_of_image_content_label}</Typography><div className="flex wrap">{type_of_image_content.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {type_of_video_content.length > 0  && <div className="padding15"><Typography className="bold-p--id">{type_of_video_content_label}</Typography><div className="flex wrap">{type_of_video_content.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {text_included_in_imageArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{text_included_in_image_label}</Typography><div className="flex wrap">{text_included_in_imageArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {text_included_in_videoArray.length > 0 && <div className="padding15"><Typography className="bold-p--id">{text_included_in_video_label}</Typography><div className="flex wrap">{text_included_in_videoArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagTeal" />)}</div></div>}
                                    {dynamic_elementArray && <Typography className="section-headings">{dynamic_element_label}</Typography>}
                                    {<LcrDynamicElement dynamic_elementArray={dynamic_elementArray} metadataLanguage={metadataLanguage} />}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>
                    })}
                </div>
            </div>
        </div>
    }
}

