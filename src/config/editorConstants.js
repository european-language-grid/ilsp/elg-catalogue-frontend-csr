import React from "react";
import { BASE_URL } from "./constants";
import { ReactComponent as TextIcon } from "./../assets/elg-icons/office-file-text-graph-alternate.svg";
import { ReactComponent as ContactIcon } from "./../assets/elg-icons/editor/send-email.svg";
import { ReactComponent as DocumentationIcon } from "./../assets/elg-icons/editor/book-library-1.svg";
import { ReactComponent as LRIcon } from "./../assets/elg-icons/editor/book-share.svg";
import { ReactComponent as CategoryIcon } from "./../assets/elg-icons/editor/layout-corners-dashboard-1.svg";
import { ReactComponent as TechnicalIcon } from "./../assets/elg-icons/editor/virtual-box.svg";
import { ReactComponent as EvaluationIcon } from "./../assets/elg-icons/editor/app-window-check.svg";
import { ReactComponent as DetailedInfoIcon } from "./../assets/elg-icons/editor/paper-write.svg";
import { ReactComponent as ParticipantsIcon } from "./../assets/elg-icons/editor/multiple-circle.svg";

export const EDITOR_UPLOAD_NEW_RECORD_ENDPOINT = `${BASE_URL}catalogue_backend/api/registry/metadatarecord/`;
export const EDITOR_UPDATE_EXISTING_RECORD = (pk) => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${pk}/`;
export const EDITOR_DRAFT_NEW_RECORD = `${BASE_URL}catalogue_backend/api/registry/metadatarecord/?draft=True`
export const EDITOR_UPDATE_DRAFT_RECORD = (pk) => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${pk}/?draft=True `

export const EDITOR_COMPATIBLE_SCHEMA_METADATARECORD = id => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${id}/`;

export const EDITOR_METADATA_VALIDATE = pk => `${BASE_URL}catalogue_backend/api/management/metadata-valid/${pk}/`; //expects internal and changes state to ingested. works with PATCH request (not POST)
export const EDITOR_LEGAL_VALIDATE = pk => `${BASE_URL}catalogue_backend/api/management/legally-valid/${pk}/`; //expects internal and changes state to ingested. works with PATCH request (not POST)
export const EDITOR_TECHNICAL_VALIDATE = pk => `${BASE_URL}catalogue_backend/api/management/technically-valid/${pk}/`; //expects internal and changes state to ingested. works with PATCH request (not POST)
export const EDITOR_REGISTER_SERVICE = pk => `${BASE_URL}catalogue_backend/api/processing/lt_service/${pk}/`; //expects internal and changes state to ingested. works with GET request (not POST)

export const MATCH_ORGANIZATION_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/match/organization/?query=${query}`;
export const MATCH_PROJECT_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/match/project/?query=${query}`;

export const MATCH_CORPUS_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?query_all=${query}`;//(query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?corpus=${query}`;
export const MATCH_SERVICE_TOOL_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?query_all=${query}`;//(query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?tool_service=${query}`;
export const MATCH_LD_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?query_all=${query}`;// (query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?ld=${query}`;
export const MATCH_LCR_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?query_all=${query}`;// (query) => `${BASE_URL}catalogue_backend/api/registry/match/lr/?lcr=${query}`;

export const LOOK_UP_ORGANIZATION_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/organization/?query=${query}`;
export const LOOK_UP_PROJECT_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/project/?query=${query}`;
export const LOOK_UP_CORPUS_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/lr/?corpus=${query}`;
//export const LOOK_UP_LANGUAGE = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/language/?language_label=${query}`;
export const LOOK_UP_LANGUAGE = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/language/?${query}`;
export const LOOK_UP_DOMAIN = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/domain/?query=${query}`;
export const LOOK_UP_SUBJECT = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/subject/?query=${query}`;
export const LOOK_UP_CV_RECOMMENDED = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/cv_recommended/?${query}`;//?lt_area, ?model_function, ?model_type
export const LOOK_UP_LR = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/lr/?query_all=${query}`;
export const LOOK_UP_DOCUMENT = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/document/?query=${query}`;
export const LOOK_UP_LICENCE = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/licence_terms/?query=${query}`;
export const LOOK_UP_PERSON = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/person/?${query}`;
export const LOOK_UP_GROUP_BY_NAME = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/group/?query=${query}`;
export const LOOK_UP_TEXT_TYPE = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/text_type/?query=${query}`;
export const LOOK_UP_ANY_TYPE = (entity, query) => `${BASE_URL}catalogue_backend/api/registry/lookup/${entity}/?query=${query}`;
export const LOOK_UP_REPOSITORY = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/repository/?query=${query}`;
export const LOOK_UP_ACCESS_RIGHTS_STATEMENT = (query) => `${BASE_URL}catalogue_backend/api/registry/lookup/access_rights_statement/?query=${query}`;
export const LOOK_UP_KEYWORD = (params) => `${BASE_URL}catalogue_backend/api/registry/lookup/keyword/?${params}`;
export const LOOK_UP_SERVICE_OFFERED = (params) => `${BASE_URL}catalogue_backend/api/registry/lookup/service_offered/?${params}`;

//remove schema2 to work with automatic json
export const EDITOR_MODELS_LR_SCHEMA = entity => `${BASE_URL}catalogue_backend/api/registry/schema2/described_entity/lr/?name=${entity}`; //works with corpus,lcr,ld
export const EDITOR_MODELS_SCHEMA = entity => `${BASE_URL}catalogue_backend/api/registry/schema2/described_entity/${entity}/`;
export const EDITOR_MODELS_SCHEMA_LR_SUBCLASS = entity => `${BASE_URL}catalogue_backend/api/registry/schema2/lr_subclass/${entity}/`;
export const EDITOR_MODELS_SCHEMA_GENERIC_ACTOR = actor => `${BASE_URL}catalogue_backend/api/registry/schema2/actor/${actor}/`;

export const EDITOR_MODELS_SCHEMA_MEDIA_PART = entity => `${BASE_URL}catalogue_backend/api/registry/schema2/media_part/${entity}/`;
export const EDITOR_MODELS_SCHEMA_LD_MODEL_GRAMMAR_PART = entity => `${BASE_URL}catalogue_backend/api/registry/schema2/ld_subclass/${entity}/`;//model,grammar
//export const EDITOR_MODELS_SCHEMA_LD_MODEL_PART = entity => `${BASE_URL}catalogue_backend/api/registry/schema2/ld_subclass/${entity}/`;//ngram_model,ml_model,grammar 

export const EDITOR_METADATARECORD_MODEL_SCHEMA = `${BASE_URL}catalogue_backend/api/registry/metadatarecord/`;//this is for the source_of_metadat_record field- notice the lack of schema in the url

export const EDITOR_INGEST = id => `${BASE_URL}catalogue_backend/api/management/ingest/?record_id__in=${id}`; //expects internal and changes state to ingested. works with PATCH request (not POST)
export const EDITOR_BULK_UNPUBLISH = ids => `${BASE_URL}catalogue_backend/api/management/unpublication-request/?record_id__in=${ids}`;//same for single
export const EDITOR_BULK_DELETE = ids => `${BASE_URL}catalogue_backend/api/management/delete/?record_id__in=${ids}`;//same for single
export const EDITOR_UNPUBLISH_UNDER_CONSTRUCTION = id => `${BASE_URL}catalogue_backend/api/management/unpublish/?record_id__in=${id}`;

export const SERVICE_REGISTRATION_BATCH = `${BASE_URL}catalogue_backend/api/processing/lt_service/batch_update/`;
export const BULK_ACTION_TECHNICALLY_VALIDATE = (ids) => `${BASE_URL}catalogue_backend/api/management/batch-technical-validation/?record_id__in=${ids}`;

export const EDITOR_CLAIM = pk => `${BASE_URL}catalogue_backend/api/management/claim/${pk}/`;


export const DASHBOARD_CREATE_NEW_VERSION = pk => `${BASE_URL}catalogue_backend/api/management/create_version/${pk}/`;
export const DASHBOARD_COPY_RECORD = pk => `${BASE_URL}catalogue_backend/api/management/copy_resource/${pk}/`;

//export const DEACTIVATE_VERSION = pk => `${BASE_URL}catalogue_backend/api/management/deactivate_version/${pk}/`;
export const DEACTIVATE_VERSION = pk => `${BASE_URL}catalogue_backend/api/management/deactivate_version/?record_id__in=${pk}`;

export const switchIcon = title => {
    let Output;
    switch (title) {
        case "Identity":
            Output = (<TextIcon className="small-icon  mr-05" />);
            break;
        case "Categories":
            Output = (<CategoryIcon className="small-icon  mr-05" />);
            break;
        case "Activities":
            Output = (<CategoryIcon className="small-icon  mr-05" />);
            break;
        case "Contact":
            Output = (<ContactIcon className="small-icon  mr-05" />);
            break;
        case "Documentation":
            Output = (<DocumentationIcon className="small-icon  mr-05" />);
            break;
        case "Related LRΤs":
            Output = (<LRIcon className="small-icon  mr-05" />);
            break;
        case "Technical":
            Output = (<TechnicalIcon className="small-icon  mr-05" />);
            break;
        case "Evaluation":
            Output = (<EvaluationIcon className="small-icon  mr-05" />);
            break;
        case "Participants":
            Output = (<ParticipantsIcon className="small-icon  mr-05" />);
            break;
        case "Detailed information":
            Output = (<DetailedInfoIcon className="small-icon  mr-05" />);
            break;
        // unknown type ... output null to not render
        default:
            Output = (null); // to return nothing, use null
            break;

    }
    return Output;
}

export const RESOURCE_ACCESS = "Resource access";
//service registration 
export const TOOL_TYPE_OPTIONS = [
    { value: 'http://w3id.org/meta-share/omtd-share/TextToSpeechSynthesis', abbr: 'TTS', humanReadable: 'Text-to-Speech Synthesis' },
    { value: 'http://w3id.org/meta-share/omtd-share/MachineTranslation', abbr: 'MT', humanReadable: 'Machine Translation' },
    { value: 'http://w3id.org/meta-share/omtd-share/InformationExtraction', abbr: 'IE', humanReadable: 'Information Extraction' },
    { value: 'http://w3id.org/meta-share/omtd-share/SpeechRecognition', abbr: 'ASR', humanReadable: 'Speech Recognition' },
    { value: 'http://w3id.org/meta-share/omtd-share/TextCategorization', abbr: 'TC', humanReadable: 'Text categorization' },
    { value: 'http://w3id.org/meta-share/omtd-share/ImageProcessing', abbr: 'IP', humanReadable: 'Image processing' },
    { value: 'http://w3id.org/meta-share/omtd-share/ResourceAccess', abbr: 'RA', humanReadable: RESOURCE_ACCESS }
]

export const STATUS_OPTIONS = [
    { value: 'NEW', humanReadable: 'New' },
    { value: 'PENDING', humanReadable: 'Pending' },
    { value: 'COMPLETED', humanReadable: 'Completed' },
]

//tabs
export const ORGANIZATION_TABS_HEADERS = ["Identity", "Activities", "Contact"];
export const ORGANIZATION_TABS_HEADERS_DIVISION = ['Identity'];
export const ORGANIZATION_TOP_TABS_HEADERS = ['Describe Organization', 'Describe Parent'];
export const PROJECT_TABS_HEADERS = ["Identity", "Categories", "Participants", "Detailed information"];

export const CORPUS_TOP_TABS_HEADERS = ["Language Resource/Technology", "Corpus", "Part", "Distribution", "DATA"];
export const CORPUS_TOP_TABS_HEADERS_TOOLTIPS = ["Language Resource/Technology", "Corpus", "Corpus Media Part", "Distribution", "DATA"];
export const CORPUS_FIRST_SECTION_TABS_HEADERS = ["Identity", "Categories", "Contact", "Documentation", "Related LRΤs"];
export const CORPUS_SECOND_SECTION_TABS_HEADERS = ["Technical"];
export const CORPUS_THIRD_SECTION_TABS_HEADERS = ["Media part"];
export const CORPUS_FOURTH_SECTION_TABS_HEADERS = ["Technical"];

export const SERVICETOOL_TOP_TABS_HEADERS = ['Language Resource/Technology', 'Tool/Service', "Distribution", "Data"];
export const SERVICETOOL_TOP_TABS_HEADERS_TOOLTIPS = ['Language Resource/Technology', 'Tool/Service', "Distribution", "Data"];
export const SERVICE_FIRST_SECTION_TABS_HEADERS = ["Identity", "Categories", "Contact", "Documentation", "Related LRΤs"];
export const SERVICE_SECOND_SECTION_TABS_HEADERS = ["Categories", "Technical", "Evaluation"];
export const SERVICE_THIRD_SECTION_TABS_HEADERS = ["Technical"];
export const SERVICE_FOURTCH_SECTION_TABS_HEADERS = ["Technical"];

export const LD_TOP_TABS_HEADERS = (type) => ["Language Resource/Technology", [OTHER, UNSPECIFIED].includes(type) ? "Language description" : "GRAMMAR / MODEL", "Part", "Distribution", "Data"];
export const LD_TOP_TABS_HEADERS_TOOLTIPS = (type) => ["Language Resource/Technology", [OTHER, UNSPECIFIED].includes(type) ? "Language description" : "GRAMMAR / MODEL", "Part", "Distribution", "Data"];
export const LD_FIRST_SECTION_TABS_HEADERS = ["Identity", "Categories", "Contact", "Documentation", "Related LRΤs"];
export const LD_SECOND_SECTION_TABS_HEADERS = ["Technical"];
export const LD_THIRD_SECTION_TABS_HEADERS = ["Media part"];
export const LD_FOURTH_SECTION_TABS_HEADERS = ["Technical"];

export const LCR_TOP_TABS_HEADERS = ["Language Resource/Technology", "Lcr", "Part", "Distribution", "Data"];
export const LCR_TOP_TABS_HEADERS_TOOLTIPS = ["Language Resource/Technology", "Lcr", "Part", "Distribution", "Data"];
export const LCR_FIRST_SECTION_TABS_HEADERS = ["Identity", "Categories", "Contact", "Documentation", "Related LRΤs"];
export const LCR_SECOND_SECTION_TABS_HEADERS = ["Technical"];
export const LCR_THIRD_SECTION_TABS_HEADERS = ["Media part"];
export const LCR_FOURTH_SECTION_TABS_HEADERS = ["Technical"];

export const HIDE_EDIT_ACTION = ["Person", "Group", "Document", "LicenceTerms", "Repository"];//["Corpus", "Tool/Service", "LexicalConceptualResource",Lexical/Conceptual resource","LanguageDescription", "Project", "Organization"];

export const DOCKER_IMAGE = "http://w3id.org/meta-share/meta-share/dockerImage";
export const EXECUTABLE_CODE = "http://w3id.org/meta-share/meta-share/executableCode";
export const GALAXY_WORKFLOW = "http://w3id.org/meta-share/meta-share/galaxyWorkflow";
export const LIBRARY = "http://w3id.org/meta-share/meta-share/library";
export const OTHER = "http://w3id.org/meta-share/meta-share/other";
export const PLUGIN = "http://w3id.org/meta-share/meta-share/plugin";
export const SOURCE_AND_EXECUTABLE = "http://w3id.org/meta-share/meta-share/sourceAndExecutableCode";
export const SOURCE_CODE = "http://w3id.org/meta-share/meta-share/sourceCode";
export const UNSPECIFIED = "http://w3id.org/meta-share/meta-share/unspecified";
export const WEB_SERVICE = "http://w3id.org/meta-share/meta-share/webService";
export const WORK_FLOW = "http://w3id.org/meta-share/meta-share/workflowFile";

export const SUBCLASS_TYPE_MODEL = "http://w3id.org/meta-share/meta-share/model";
export const SUBCLASS_TYPE_GRAMMAR = "http://w3id.org/meta-share/meta-share/grammar";
export const SUBCLASS_TYPE_OTHER = "http://w3id.org/meta-share/meta-share/other";
export const SUBCLASS_TYPE_UNSPECIFIED = "http://w3id.org/meta-share/meta-share/unspecified";