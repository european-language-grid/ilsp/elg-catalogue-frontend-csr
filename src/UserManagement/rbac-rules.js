/*Static permissions are helpful when, for example, you have to allow write access to owners and admins only. 
Dynamic permissions are helpful when, for example, you have only to allow the owner who is the owner of a resource to edit that resource.*/

const rules = {
    consumer: {
      static: ["home-page:visit"]
    },
    provider: {
      static: [
        "resources:list",
        "resources:create",
        "resources:create-draft",
        "users:getSelf",
        "home-page:visit",
        "dashboard-page:visit"
      ],
      dynamic: {
        "resources:edit": ({userId, postOwnerId}) => {
          if (!userId || !postOwnerId) return false;
          return userId === postOwnerId;
        }
      }
    },
    admin: {
      static: [
        "resources:list",
        "resources:create",
        "resources:create-draft",
        "resources:edit",
        "resources:delete",
        "users:get",
        "users:getSelf",
        "home-page:visit",
        "dashboard-page:visit"
      ]
    },
    contentManager: {
        static: [
          "resources:list",
          "resources:create",
          "resources:create-draft",
          "resources:edit",
          "resources:delete",
          "users:get",
          "users:getSelf",
          "home-page:visit",
          "dashboard-page:visit"
        ]
      }
  };
  
  export default rules;