import React from "react";
import rules from "./rbac-rules";

/*This component takes the rules that you defined and decides whether or not users can perform the desired action or see some part of the UI. 
If the answer is yes, then the Can component renders the yes prop. Otherwise, the component renders the no prop.*/

/*Using the Can component is very easy. You just have to pass the role and the 
perform prop to tell the component what is the user role and what action they are trying to execute. 
Then, you have to provide the yes and no props to the component.*/

//check() function that imperatively checks whether the user has the permission or not. 
const check = (rules, role, action, data) => {
  const permissions = rules[role];
  if (!permissions) {
    // role is not present in the rules
    return false;
  }

  const staticPermissions = permissions.static;

  if (staticPermissions && staticPermissions.includes(action)) { //find if the permission is present in the staticPermissions array of the user role
    // static rule not provided for action
    return true;
  }

  const dynamicPermissions = permissions.dynamic;

  if (dynamicPermissions) { //If static is not present, then the function checks if the dynamicPermissions object has a property with the name of the permission.
    const permissionCondition = dynamicPermissions[action]; //If there is, then, the function calls the method associated with that property and uses the returned value of the method to find if the user has the permission or not.
    if (!permissionCondition) {
      // dynamic rule not provided for action
      return false;
    }

    return permissionCondition(data);
  }
  return false;
};

/*const Can = props =>
  check(rules, props.role, props.perform, props.data)
    ? props.yes()
    : props.no();

Can.defaultProps = {
  yes: () => null,
  no: () => null
};
*/

class Can extends React.Component {
  render() {
    if (check(rules, this.props.role, this.props.perform, this.props.data)) {
      return this.props.yes();
    } else {
      return this.props.no();
    }
  }
}
export default Can;