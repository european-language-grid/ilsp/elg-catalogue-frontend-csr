import { ACCESSIBLE_QUERY } from "../Editor/Models/CorpusModel";

class LCRParser {
    constructor() {
        this.getResourceName = this.getResourceName.bind(this);
        this.getVersion = this.getVersion.bind(this);
        this.getVersionDate = this.getVersionDate.bind(this);
        this.getLogo = this.getLogo.bind(this);
        this.getResourceShortName = this.getResourceShortName.bind(this);
        this.getDescription = this.getDescription.bind(this);
        this.getUpdateFrequency = this.getUpdateFrequency.bind(this);
        this.getRevision = this.getRevision.bind(this);
        this.getMailingList = this.getMailingList.bind(this);
        this.getDiscussionUrl = this.getDiscussionUrl.bind(this);
        this.getCitationText = this.getCitationText.bind(this);
        this.getCitationAllVersions = this.getCitationAllVersions.bind(this);
        this.getKeywords = this.getKeywords.bind(this);
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getSubjectKeywords = this.getSubjectKeywords.bind(this);
        this.getIntendedKeywords = this.getIntendedKeywords.bind(this);
        this.getAdditionalInfo = this.getAdditionalInfo.bind(this);
        this.getFundingProjects = this.getFundingProjects.bind(this);
        this.getPhysicalResource = this.getPhysicalResource.bind(this);
        this.getLcrSubclass = this.getLcrSubclass.bind(this);
        this.getPersonalDataIncluded = this.getPersonalDataIncluded.bind(this);
        this.getPersonalDataDetails = this.getPersonalDataDetails.bind(this);
        this.getSensitiveDataIncluded = this.getSensitiveDataIncluded.bind(this);
        this.getSensitiveDataDetails = this.getSensitiveDataDetails.bind(this);
        this.getAnonymized = this.getAnonymized.bind(this);
        this.getAnonymizedDetails = this.getAnonymizedDetails.bind(this);
        this.getLanguageDependentValue = this.getLanguageDependentValue.bind(this);
        this.getLRType = this.getLRType.bind(this);
        this.getEntityType = this.getEntityType.bind(this);
    }

    getEntityType(data, lang) {
        let entity_type = data.described_entity.field_value.entity_type;
        if (entity_type) {
            entity_type = data.described_entity.field_value.entity_type.field_value;
            //const entity_type_label = data.described_entity.field_value.entity_type.field_label[lang] || 
            //data.described_entity.entity_type.field_label["en"];
            return { "label": "", "value": entity_type };
        } else { entity_type = null }
        return entity_type;
    }

    getLRType(data, lang) {
        let lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type;
        if (lr_type) {
            lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            const lr_type_label = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_label[lang] || data.described_entity.field_value.lr_subclass.field_value.lr_type.field_label["en"];
            return { "label": lr_type_label, "value": lr_type };
        } else { lr_type = null }
        return lr_type;
    }

    getResourceName(data, lang) {
        let resourceName = data.described_entity.field_value.resource_name;
        if (resourceName) {
            resourceName = data.described_entity.field_value.resource_name.field_value[lang] || data.described_entity.field_value.resource_name.field_value[Object.keys(data.described_entity.field_value.resource_name.field_value)[0]];
            const resourceName_label = data.described_entity.field_value.resource_name.field_label[lang] || data.described_entity.field_value.resource_name.field_label["en"];
            return { "label": resourceName_label, "value": resourceName };
        }
        return resourceName || "";
    }


    getVersion(data, lang) {
        let version = data.described_entity.field_value.version;
        if (version) {
            version = data.described_entity.field_value.version.field_value;
            const version_label = data.described_entity.field_value.version.field_label[lang] || data.described_entity.field_value.version.field_label["en"];
            return { "label": version_label, "value": version };
        } else { version = null; }
        return version;
    }

    getVersionDate(data, lang) {
        let versionDate = data.described_entity.field_value.version_date;
        if (versionDate) {
            versionDate = data.described_entity.field_value.version_date.field_value;
            const versionDate_label = data.described_entity.field_value.version_date.field_label[lang] || data.described_entity.field_value.version_date.field_label["en"];
            return { "label": versionDate_label, "value": versionDate };
        } else { versionDate = null }
        return versionDate;
    }

    getLogo(data) {
        let logo = data.described_entity.field_value.logo;
        if (logo) {
            logo = data.described_entity.field_value.logo.field_value;
        } else { logo = null };
        return logo;
    }


    getResourceShortName(data, lang) {
        let resourceShortName = data.described_entity.field_value.resource_short_name;
        if (resourceShortName) {
            resourceShortName = data.described_entity.field_value.resource_short_name.field_value[lang] || data.described_entity.field_value.resource_short_name.field_value[Object.keys(data.described_entity.field_value.resource_short_name.field_value)[0]];
            const resourceShortName_label = data.described_entity.field_value.resource_short_name.field_label[lang] || data.described_entity.field_value.resource_short_name.field_label["en"];
            return { "label": resourceShortName_label, "value": resourceShortName };
        }
        return resourceShortName || "";
    }

    getUpdateFrequency(data, lang) {
        let updateFrequency = data.described_entity.field_value.update_frequency;
        if (updateFrequency) {
            updateFrequency = data.described_entity.field_value.update_frequency.field_value[lang] || data.described_entity.field_value.update_frequency.field_value[Object.keys(data.described_entity.field_value.update_frequency.field_value)[0]];
            const updateFrequency_label = data.described_entity.field_value.update_frequency.field_label[lang] || data.described_entity.field_value.update_frequency.field_label["en"];
            return { "label": updateFrequency_label, "value": updateFrequency };
        }
        return updateFrequency || "";
    }

    getDescription(data, lang) {
        let description = data.described_entity.field_value.description;
        if (description) {
            description = data.described_entity.field_value.description.field_value[lang] || data.described_entity.field_value.description.field_value[Object.keys(data.described_entity.field_value.description.field_value)[0]];
            const description_label = data.described_entity.field_value.description.field_label[lang] || data.described_entity.field_value.description.field_label["en"];
            return { "label": description_label, "value": description };
        }
        return description || "";
    }

    getRevision(data, lang) {
        let revision = data.described_entity.field_value.revision;
        if (revision) {
            revision = data.described_entity.field_value.revision.field_value[lang] || data.described_entity.field_value.revision.field_value[Object.keys(data.described_entity.field_value.revision.field_value)[0]];
            const revision_label = data.described_entity.field_value.revision.field_label[lang] || data.described_entity.field_value.revision.field_label["en"];
            return { "label": revision_label, "value": revision };
        }
        return revision || "";
    }

    getMailingList(data, lang) {
        let mailingList = [];
        if (data.described_entity.field_value.mailing_list_name && data.described_entity.field_value.mailing_list_name.field_value.length > 0) {
            data.described_entity.field_value.mailing_list_name.field_value.forEach((item, index) => mailingList.push(item))
            const mailingList_label = data.described_entity.field_value.mailing_list_name.field_label[lang] || data.described_entity.field_value.mailing_list_name.field_label["en"];
            return { "label": mailingList_label, "value": mailingList };
        }
        return null;
    }

    getDiscussionUrl(data, lang) {
        let discussionUrl = [];
        if (data.described_entity.field_value.discussion_url && data.described_entity.field_value.discussion_url.field_value.length > 0) {
            data.described_entity.field_value.discussion_url.field_value.forEach((item, index) => discussionUrl.push(item));
            const discussionUrl_label = data.described_entity.field_value.discussion_url.field_label[lang] || data.described_entity.field_value.discussion_url.field_label["en"];
            return { "label": discussionUrl_label, "value": discussionUrl };
        }
        return null;
    }

    getCitationText(data, lang) {
        let citationText = data.described_entity.field_value.citation_text;
        if (citationText) {
            citationText = data.described_entity.field_value.citation_text.field_value[lang] || data.described_entity.field_value.citation_text.field_value[Object.keys(data.described_entity.field_value.citation_text.field_value)[0]];
            const citationText_label = data.described_entity.field_value.citation_text.field_label[lang] || data.described_entity.field_value.citation_text.field_label["en"];
            return { "label": citationText_label, "value": citationText };
        }
        return citationText || "";
    }
    getCitationAllVersions(data, lang) {
        let citation_all_versions = data.described_entity.field_value.citation_all_versions;
        if (citation_all_versions) {
            citation_all_versions = data.described_entity.field_value.citation_all_versions.field_value[lang] || data.described_entity.field_value.citation_all_versions.field_value[Object.keys(data.described_entity.field_value.citation_all_versions.field_value)[0]];
            const citation_all_versions_label = data.described_entity.field_value.citation_all_versions.field_label[lang] || data.described_entity.field_value.citation_all_versions.field_label["en"];
            return { "label": citation_all_versions_label, "value": citation_all_versions };
        }
        return citation_all_versions || "";
    }

    getKeywords(data, lang) {
        let keywords = [];
        if (data.described_entity.field_value.keyword && data.described_entity.field_value.keyword.field_value.length > 0) {
            data.described_entity.field_value.keyword.field_value.forEach((keyword, index) => {
                keywords.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
            const keyword_label = data.described_entity.field_value.keyword.field_label[lang] || data.described_entity.field_value.keyword.field_label["en"];
            return { "label": keyword_label, "value": keywords };
        }
        return null;
    }

    getDomainKeywords(data, lang) {
        let domainKeywords = [];
        if (data.described_entity.field_value.domain && data.described_entity.field_value.domain.field_value.length > 0) {
            data.described_entity.field_value.domain.field_value.forEach((keyword, index) => {
                domainKeywords.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
            const domain_label = data.described_entity.field_value.domain.field_label[lang] || data.described_entity.field_value.domain.field_label["en"];
            return { "label": domain_label, "value": domainKeywords };
        }
        return null;
    }

    getSubjectKeywords(data, lang) {
        let subject = [];
        if (data.described_entity.field_value.subject && data.described_entity.field_value.subject.field_value.length > 0) {
            data.described_entity.field_value.subject.field_value.forEach((keyword, index) => {
                subject.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
            const subject_label = data.described_entity.field_value.subject.field_label[lang] || data.described_entity.field_value.subject.field_label["en"];
            return { "label": subject_label, "value": subject };
        }
        return null;
    }

    getIntendedKeywords(data, lang) {
        let intendedKeywords = [];
        if (data.described_entity.field_value.intended_application && data.described_entity.field_value.intended_application.field_value.length > 0) {
            data.described_entity.field_value.intended_application.field_value.forEach((keyword, index) => {
                keyword.label ? (intendedKeywords.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]])) : intendedKeywords.push(keyword.value)
            });
            const intended_application_label = data.described_entity.field_value.intended_application.field_label[lang] || data.described_entity.field_value.intended_application.field_label["en"];
            return { "label": intended_application_label, "value": intendedKeywords };
        }
        return null;
    }

    getAdditionalInfo(data, lang) {
        let additionalInfo = [];
        let additionalInfo_label = "";
        if (data.described_entity.field_value.additional_info) {
            data.described_entity.field_value.additional_info.field_value.forEach(additional_infoItem => additionalInfo.push(additional_infoItem));
            additionalInfo_label = data.described_entity.field_value.additional_info.field_label[lang] ||
                data.described_entity.field_value.additional_info.field_label["en"];
        }
        return { "label": additionalInfo_label, "value": additionalInfo };
    }

    getFundingProjects(data, lang) {
        let fundingProjects = [];
        if (data.described_entity.field_value.funding_project && data.described_entity.field_value.funding_project.field_value && data.described_entity.field_value.funding_project.field_value.length > 0) {
            data.described_entity.field_value.funding_project.field_value.forEach((fundingProject, fundingProjectIndex) => {
                //let pk = fundingProject.pk;
                let project_name = fundingProject.project_name.field_value[lang] || fundingProject.project_name.field_value[Object.keys(fundingProject.project_name.field_value)[0]];
                let website = fundingProject.website ? (fundingProject.website.field_value) : "";
                let project_identifier = fundingProject.project_identifier ? [...fundingProject.project_identifier.field_value] : "";
                fundingProjects.push({ project_name, website, project_identifier });
            }
            )
        }
        return fundingProjects;
    }


    getPhysicalResource(data, lang) {
        let physical_resource = [];
        let physical_resource_label = "";
        if (data.described_entity.field_value.physical_resource) {
            data.described_entity.field_value.physical_resource.field_value.forEach(physical_resourceItem => physical_resource.push(physical_resourceItem.field_value));
            physical_resource_label = data.described_entity.field_value.physical_resource.field_label[lang] || data.described_entity.field_value.physical_resource.field_label["en"];
        }
        //return physical_resource;
        return { "label": physical_resource_label, "value": physical_resource };
    }

    getLcrSubclass(data, lang) {
        let lcr_subclass = "";
        let lcr_subclass_label = "";
        if (data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.lcr_subclass) {
            lcr_subclass = data.described_entity.field_value.lr_subclass.field_value.lcr_subclass.label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.lcr_subclass.label[Object.keys(data.described_entity.field_value.lr_subclass.field_value.lcr_subclass.label)[0]];
            lcr_subclass_label = data.described_entity.field_value.lr_subclass.field_value.lcr_subclass.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.lcr_subclass.field_label["en"];
        }
        return { "label": lcr_subclass_label, "value": lcr_subclass };
    }


    getPersonalDataIncluded(data, lang) {
        let personal_data_included = "";
        let personal_data_included_label = "";
        if (data.described_entity.field_value.lr_subclass && (data.described_entity.field_value.lr_subclass.field_value.personal_data_included !== null && data.described_entity.field_value.lr_subclass.field_value.personal_data_included !== undefined)) {
            //personal_data_included = Boolean(data.described_entity.field_value.lr_subclass.field_value.personal_data_included.field_value);
            personal_data_included = (data.described_entity.field_value.lr_subclass.field_value.personal_data_included.label[lang]||data.described_entity.field_value.lr_subclass.field_value.personal_data_included.label["en"]);
            personal_data_included_label = data.described_entity.field_value.lr_subclass.field_value.personal_data_included.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.personal_data_included.field_label["en"];
        }
        return { "label": personal_data_included_label, "value": personal_data_included };
    }

    getPersonalDataDetails(data, lang) {
        let personal_data_details = "";
        let personal_data_details_label = "";
        if (data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.personal_data_details) {
            personal_data_details = (data.described_entity.field_value.lr_subclass.field_value.personal_data_details.field_value[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.personal_data_details.field_value[Object.keys(data.described_entity.field_value.lr_subclass.field_value.personal_data_details.field_value)[0]]) || "";
            personal_data_details_label = data.described_entity.field_value.lr_subclass.field_value.personal_data_details.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.personal_data_details.field_label["en"];
        }
        return { "label": personal_data_details_label, "value": personal_data_details };
    }

    getSensitiveDataIncluded(data, lang) {
        let sensitive_data_included = "";
        let sensitive_data_included_label = "";
        if (data.described_entity.field_value.lr_subclass && (data.described_entity.field_value.lr_subclass.field_value.sensitive_data_included !== null && data.described_entity.field_value.lr_subclass.field_value.sensitive_data_included !== undefined)) {
            //sensitive_data_included = Boolean(data.described_entity.field_value.lr_subclass.field_value.sensitive_data_included.field_value);
            sensitive_data_included = (data.described_entity.field_value.lr_subclass.field_value.sensitive_data_included.label[lang]||data.described_entity.field_value.lr_subclass.field_value.sensitive_data_included.label["en"]);
            sensitive_data_included_label = data.described_entity.field_value.lr_subclass.field_value.sensitive_data_included.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.sensitive_data_included.field_label["en"];
        }
        return { "label": sensitive_data_included_label, "value": sensitive_data_included };
    }

    getSensitiveDataDetails(data, lang) {
        let sensitive_data_details = "";
        let sensitive_data_details_label = "";
        if (data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.sensitive_data_details) {
            sensitive_data_details = (data.described_entity.field_value.lr_subclass.field_value.sensitive_data_details.field_value[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.sensitive_data_details.field_value[Object.keys(data.described_entity.field_value.lr_subclass.field_value.sensitive_data_details.field_value)[0]]) || "";
            sensitive_data_details_label = data.described_entity.field_value.lr_subclass.field_value.sensitive_data_details.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.sensitive_data_details.field_label["en"];
        }
        return { "label": sensitive_data_details_label, "value": sensitive_data_details };
    }
    getAnonymized(data, lang) {
        let anonymized = "";
        let anonymized_label = "";
        if (data.described_entity.field_value.lr_subclass && (data.described_entity.field_value.lr_subclass.field_value.anonymized !== null && data.described_entity.field_value.lr_subclass.field_value.anonymized !== undefined)) {
            //anonymized = Boolean(data.described_entity.field_value.lr_subclass.field_value.anonymized.field_value);
            anonymized = (data.described_entity.field_value.lr_subclass.field_value.anonymized.label[lang]||data.described_entity.field_value.lr_subclass.field_value.anonymized.label["en"]);
            anonymized_label = data.described_entity.field_value.lr_subclass.field_value.anonymized.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.anonymized.field_label["en"];
        }
        return { "label": anonymized_label, "value": anonymized };
    }

    getAnonymizedDetails(data, lang) {
        let anonymization_details = "";
        let anonymization_details_label = "";
        if (data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.anonymization_details) {
            anonymization_details = (data.described_entity.field_value.lr_subclass.field_value.anonymization_details.field_value[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.anonymization_details.field_value[Object.keys(data.described_entity.field_value.lr_subclass.field_value.anonymization_details.field_value)[0]]) || "";
            anonymization_details_label = data.described_entity.field_value.lr_subclass.field_value.anonymization_details.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.anonymization_details.field_label["en"];
        }
        return { "label": anonymization_details_label, "value": anonymization_details };
    }



    getUserQuery(data, lang) {
        let user_query = "";
        let user_query_label = "";
        if (data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.user_query) {
            user_query = data.described_entity.field_value.lr_subclass.field_value.user_query.field_value;
            user_query_label = data.described_entity.field_value.lr_subclass.field_value.user_query.field_label[lang] ||
                data.described_entity.field_value.lr_subclass.field_value.user_query.field_label["en"];
        }
        return { "label": user_query_label, "value": user_query };
    }


    getLanguageDependentValue(entry, lang) {
        let returnValue = "";
        if (entry) {
            returnValue = entry[lang] || entry[Object.keys(entry)[0]];
        }
        return returnValue;
    }

    get_is_queried_by_service_url = (data) => {
        try {
            const distributions = data.described_entity.field_value.lr_subclass.field_value.dataset_distribution.field_value;
            for (let i = 0; distributions && i < distributions.length; i++) {
                const { dataset_distribution_form, is_queried_by } = distributions[i];
                if (dataset_distribution_form.field_value === ACCESSIBLE_QUERY) {
                    if (is_queried_by && is_queried_by.field_value.length === 1) {
                        return is_queried_by.field_value[0].full_metadata_record.field_value.link.field_value;
                    }
                }
            }
        } catch (error) {
            console.log(error);
            return null;
        }
    }

}


const lcrParser = new LCRParser();

export default lcrParser;