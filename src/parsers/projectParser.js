class ProjectParser {
    constructor() {
        this.getProjectName = this.getProjectName.bind(this);
        this.getSummary = this.getSummary.bind(this);
        this.getProjectShortName = this.getProjectShortName.bind(this);
        this.getLogo = this.getLogo.bind(this);
        this.getKeywords = this.getKeywords.bind(this);
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getLtAreaKeywords = this.getLtAreaKeywords.bind(this);
        this.getSubjectKeywords = this.getSubjectKeywords.bind(this);

        this.getKeywordsLabel = this.getKeywordsLabel.bind(this);
        this.getDomainKeywordsLabel = this.getDomainKeywordsLabel.bind(this);
        this.getLtAreaKeywordsLabel = this.getLtAreaKeywordsLabel.bind(this);
        this.getSubjectKeywordsLabel = this.getSubjectKeywordsLabel.bind(this);
    }

    getSummary(data, lang) {
        let summary = data.described_entity.field_value.project_summary;
        if (summary) {
            summary = data.described_entity.field_value.project_summary.field_value[lang] ||
                data.described_entity.field_value.project_summary.field_value[Object.keys(data.described_entity.field_value.project_summary.field_value)[0]]
        }
        return summary || "";
    }

    getProjectName(data, lang) {
        let ProjectName = data.described_entity.field_value.project_name;
        if (ProjectName) {
            ProjectName = data.described_entity.field_value.project_name.field_value[lang] ||
                data.described_entity.field_value.project_name.field_value[Object.keys(data.described_entity.field_value.project_name.field_value)[0]]
        }
        return ProjectName || "";
    }


    getProjectShortName(data, lang) {
        let ProjectShortName = null;
        if (data.described_entity.field_value.project_short_name && data.described_entity.field_value.project_short_name.field_value) {
            ProjectShortName = data.described_entity.field_value.project_short_name.field_value[lang] || data.described_entity.field_value.project_short_name.field_value[Object.keys(data.described_entity.field_value.project_short_name.field_value)[0]];
        }
        return ProjectShortName;
    }


    getLogo(data) {
        let logo = data.described_entity.field_value.logo;
        if (logo) {
            logo = data.described_entity.field_value.logo.field_value;

        } else logo = null;
        return logo;
    }

    getDescription(data, lang) {
        let description = data.described_entity.description;
        if (description) {
            description = data.described_entity.description[lang] || data.described_entity.description[Object.keys(data.described_entity.description)[0]]
        }
        return description || "";
    }


    getKeywords(data, lang) {
        let keywords = [];
        if (data.described_entity.field_value.keyword && data.described_entity.field_value.keyword.field_value && data.described_entity.field_value.keyword.field_value.length > 0) {
            data.described_entity.field_value.keyword.field_value.forEach((keyword, index) => {
                keywords.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return keywords;
    }

    getKeywordsLabel(data, lang) {
        let keywordslabel = "";
        if (data.described_entity.field_value.keyword && data.described_entity.field_value.keyword.field_label) {
            keywordslabel = data.described_entity.field_value.keyword.field_label[lang] ||
                data.described_entity.field_value.keyword.field_label["en"];
        }
        return keywordslabel;
    }

    getDomainKeywords(data, lang) {
        let domainKeywords = [];
        if (data.described_entity.field_value.domain && data.described_entity.field_value.domain.field_value && data.described_entity.field_value.domain.field_value.length > 0) {
            data.described_entity.field_value.domain.field_value.forEach((keyword, index) => {
                domainKeywords.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
        }
        return domainKeywords;
    }

    getDomainKeywordsLabel(data, lang) {
        let domainlabel = "";
        if (data.described_entity.field_value.domain && data.described_entity.field_value.domain.field_label) {
            domainlabel = data.described_entity.field_value.domain.field_label[lang] ||
                data.described_entity.field_value.domain.field_label["en"];
        }
        return domainlabel;
    }



    getLtAreaKeywords(data, lang) {
        let LtAreaKeywordsArray = data.described_entity.field_value.lt_area ?
            data.described_entity.field_value.lt_area.field_value.map(keyword =>
                (keyword.label ?
                    (keyword.label[lang] ||
                        keyword.label[Object.keys(keyword.label)[0]]) :
                    keyword.value)) : [];
        return LtAreaKeywordsArray;
    }

    getLtAreaKeywordsLabel(data, lang) {
        let lt_arealabel = "";
        if (data.described_entity.field_value.lt_area && data.described_entity.field_value.lt_area.field_label) {
            lt_arealabel = data.described_entity.field_value.lt_area.field_label[lang] ||
                data.described_entity.field_value.lt_area.field_label["en"];
        }
        return lt_arealabel;
    }

    getSubjectKeywords(data, lang) {
        let subject = [];
        if (data.described_entity.field_value.subject && data.described_entity.field_value.subject.field_value && data.described_entity.field_value.subject.field_value.length > 0) {
            data.described_entity.field_value.subject.field_value.forEach((keyword, index) => {
                subject.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
        }
        return subject;
    }

    getSubjectKeywordsLabel(data, lang) {
        let subjectlabel = "";
        if (data.described_entity.field_value.subject && data.described_entity.field_value.subject.field_label) {
            subjectlabel = data.described_entity.field_value.subject.field_label[lang] ||
                data.described_entity.field_value.subject.field_label["en"];
        }
        return subjectlabel;
    }






}


const projectParser = new ProjectParser();

export default projectParser;