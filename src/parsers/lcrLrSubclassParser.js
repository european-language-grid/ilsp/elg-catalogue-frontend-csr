class LcrLrSubclassParser {
    constructor() {
        this.getLrtype = this.getLrtype.bind(this);
        this.getEncodingLevel=this.getEncodingLevel.bind(this);
        this.getContentType=this.getContentType.bind(this);
        //this.getCompliesWith=this.getCompliesWith.bind(this);
        this.getTheoreticModel=this.getTheoreticModel.bind(this);
        this.getExtratextualInformationUnit = this.getExtratextualInformationUnit.bind(this);
        this.getExtratextualInformation= this.getExtratextualInformation.bind(this);
       
    }
    getLrtype(data ,lang) { 
        let lr_type =""; 
        let lr_type_label="";
        if (data.field_value.lr_type)  {
            lr_type= data.field_value.lr_type.field_value; 
            lr_type_label = data.field_value.lr_type.field_label[lang]||data.field_value.lr_type.field_label["en"];
        }
        return { "label": lr_type_label, "value": lr_type };
    }    
   
    getEncodingLevel(data,lang) {
        let encoding_level = [];
        let encoding_level_label="";
        if (data.field_value.encoding_level && data.field_value.encoding_level.field_value.length > 0) {
            data.field_value.encoding_level.field_value.forEach(item => encoding_level.push(item.label[lang]||item.label[Object.keys(item.label)[0]]));
            encoding_level_label = data.field_value.encoding_level.field_label[lang]||data.field_value.encoding_level.field_label["en"];
        }
        return { "label": encoding_level_label, "value": encoding_level };
    } 

    getContentType(data,lang) {
        let content_type = [];
        let content_type_label="";
        if (data.field_value.content_type && data.field_value.content_type.field_value.length > 0) {
            data.field_value.content_type.field_value.forEach(item => content_type.push(item.label[lang]||item.label[Object.keys(item.label)[0]]));
            content_type_label = data.field_value.content_type.field_label[lang]||data.field_value.content_type.field_label["en"];
        }
        return { "label": content_type_label, "value": content_type };
    } 

    /*getCompliesWith(data,lang) {
        let complies_with = [];
        let complies_with_label="";
        if (data.field_value.complies_with && data.field_value.complies_with.field_value.length > 0) {
            data.field_value.complies_with.field_value.forEach(item => complies_with.push(item.label[lang]||item.label[Object.keys(item.label)[0]]));
            complies_with_label = data.field_value.complies_with.field_label[lang]||data.field_value.complies_with.field_label["en"];
        }
        return { "label": complies_with_label, "value": complies_with };
    } */

    getExtratextualInformation(data,lang) {
        let extratextual_information = [];
        let extratextual_information_label="";
        if (data.field_value.extratextual_information && data.field_value.extratextual_information.field_value.length > 0) {
            data.field_value.extratextual_information.field_value.forEach(item => extratextual_information.push(item.label[lang]||item.label[Object.keys(item.label)[0]]));
            extratextual_information_label = data.field_value.extratextual_information.field_label[lang]||data.field_value.extratextual_information.field_label["en"];
        }
        return { "label": extratextual_information_label, "value": extratextual_information };
    } 

    getExtratextualInformationUnit(data,lang) {
        let extratextual_information_unit = [];
        let extratextual_information_unit_label="";
        if (data.field_value.extratextual_information_unit && data.field_value.extratextual_information_unit.field_value.length > 0) {
            data.field_value.extratextual_information_unit.field_value.forEach(item => extratextual_information_unit.push(item.label[lang]||item.label[Object.keys(item.label)[0]]));
            extratextual_information_unit_label = data.field_value.extratextual_information_unit.field_label[lang]||data.field_value.extratextual_information_unit.field_label["en"];
        }
        return { "label": extratextual_information_unit_label, "value": extratextual_information_unit };
    } 

   
    getTheoreticModel(data,lang){
        let theoretic_model = [];
        let theoretic_model_label="";
        if (data.field_value.theoretic_model && data.field_value.theoretic_model.field_value.length > 0) {
            theoretic_model_label = data.field_value.theoretic_model.field_label[lang]||data.field_value.theoretic_model.field_label["en"];
            data.field_value.theoretic_model.field_value.forEach((keyword, index) => {
                theoretic_model.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return { "label": theoretic_model_label, "value": theoretic_model };
    }     

}


const lcrLrSubclassParser = new LcrLrSubclassParser();

export default lcrLrSubclassParser;