class OrganizationParser {
    constructor() {
        this.getOrganizationName = this.getOrganizationName.bind(this);
        this.getOrganizationShortName = this.getOrganizationShortName.bind(this);
        this.getOrganizationAltName = this.getOrganizationAltName.bind(this);
        this.getLogo = this.getLogo.bind(this);
        this.getOrganizationRole = this.getOrganizationRole.bind(this);
        this.getOrganizationLegalStatus = this.getOrganizationLegalStatus.bind(this);
        this.getStartup = this.getStartup.bind(this);
        this.getCountryOfRegistration = this.getCountryOfRegistration.bind(this);
        this.getOrganizationBio = this.getOrganizationBio.bind(this);
        this.getKeywords = this.getKeywords.bind(this);
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getSocialMedia = this.getSocialMedia.bind(this);
        this.getLtAreaKeywords = this.getLtAreaKeywords.bind(this);
        this.getDisciplineKeywords = this.getDisciplineKeywords.bind(this);
        this.getservicesOfferedKeywords = this.getservicesOfferedKeywords.bind(this);
        this.getOrganizationWebsites = this.getOrganizationWebsites.bind(this);
        this.getOrganizationEmails = this.getOrganizationEmails.bind(this);
        this.getTelephoneNumbers = this.getTelephoneNumbers.bind(this);
        this.getFaxNumbers = this.getFaxNumbers.bind(this);
        this.getHasDivision = this.getHasDivision.bind(this);
        //labels
        this.getOrganizationRoleLabel = this.getOrganizationRoleLabel.bind(this);
        this.getOrganizationLegalStatusLabel = this.getOrganizationLegalStatusLabel.bind(this);
        this.getStartupLabel = this.getStartupLabel.bind(this);
        this.getCountryOfRegistrationLabel = this.getCountryOfRegistrationLabel.bind(this);
        this.getKeywordsLabel = this.getKeywordsLabel.bind(this);
        this.getDomainKeywordsLabel = this.getDomainKeywordsLabel.bind(this);
        this.getSocialMediaLabel = this.getSocialMediaLabel.bind(this);
        this.getLtAreaKeywordsLabel = this.getLtAreaKeywordsLabel.bind(this);
        this.getDisciplineKeywordsLabel = this.getDisciplineKeywordsLabel.bind(this);
        this.getservicesOfferedKeywordsLabel = this.getservicesOfferedKeywordsLabel.bind(this);
        this.getOrganizationWebsitesLabel = this.getOrganizationWebsitesLabel.bind(this);
        this.getOrganizationEmailsLabel = this.getOrganizationEmailsLabel.bind(this);
        this.getTelephoneNumbersLabel = this.getTelephoneNumbersLabel.bind(this);
        this.getFaxNumbersLabel = this.getFaxNumbersLabel.bind(this);
        this.getHasDivisionLabel = this.getHasDivisionLabel.bind(this);

    }

    getOrganizationName(data, lang) {
        let OrganizationName = data.described_entity.field_value.organization_name.field_value;
        if (OrganizationName) {
            OrganizationName = data.described_entity.field_value.organization_name.field_value[lang] ||
                data.described_entity.field_value.organization_name.field_value[Object.keys(data.described_entity.field_value.organization_name.field_value)[0]]
        }
        return OrganizationName || "";
    }


    getOrganizationShortName(data, lang) {
        let OrganizationShortName = [];
        if (data.described_entity.field_value.organization_short_name && data.described_entity.field_value.organization_short_name.field_value && data.described_entity.field_value.organization_short_name.field_value.length > 0) {
            data.described_entity.field_value.organization_short_name.field_value.forEach((keyword, index) => {
                OrganizationShortName.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return OrganizationShortName;
    }

    getOrganizationAltName(data, lang) {
        let OrganizationAltName = [];
        if (data.described_entity.field_value.organization_alternative_name && data.described_entity.field_value.organization_alternative_name.field_value && data.described_entity.field_value.organization_alternative_name.field_value.length > 0) {
            data.described_entity.field_value.organization_alternative_name.field_value.forEach((keyword, index) => {
                OrganizationAltName.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return OrganizationAltName;
    }


    getOrganizationRole(data, lang) {
        let OrganizationRoles = [];
        if (data.described_entity.field_value.organization_role && data.described_entity.field_value.organization_role.field_value && data.described_entity.field_value.organization_role.field_value.length > 0) {
            data.described_entity.field_value.organization_role.field_value.forEach((keyword, index) => {
                OrganizationRoles.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]])
            });
        }
        return OrganizationRoles;
    }

    getOrganizationRoleLabel(data, lang) {
        let OrganizationRolelabel = "";
        if (data.described_entity.field_value.organization_role && data.described_entity.field_value.organization_role.field_label) {
            OrganizationRolelabel = data.described_entity.field_value.organization_role.field_label[lang] ||
                data.described_entity.field_value.organization_role.field_label["en"];
        }
        return OrganizationRolelabel;
    }

    getOrganizationLegalStatus(data, lang) {
        let OrganizationLegalStatus = " ";
        if (data.described_entity.field_value.organization_legal_status) {
            OrganizationLegalStatus = data.described_entity.field_value.organization_legal_status.label[lang] ||
                data.described_entity.field_value.organization_legal_status.label[Object.keys(data.described_entity.field_value.organization_legal_status.label)[0]]
        }
        return OrganizationLegalStatus || null;;
    }

    getOrganizationLegalStatusLabel(data, lang) {
        let OrganizationLegalStatusLabel = " ";
        if (data.described_entity.field_value.organization_legal_status) {
            OrganizationLegalStatusLabel = data.described_entity.field_value.organization_legal_status.field_label[lang] ||
                data.described_entity.field_value.organization_legal_status.field_label["en"];
        }
        return OrganizationLegalStatusLabel || null;;
    }


    getLogo(data) {
        let logo = data.described_entity.field_value.logo;
        if (logo) {
            logo = data.described_entity.field_value.logo.field_value;

        } else logo = null;
        return logo;
    }


    getOrganizationBio(data, lang) {
        let OrganizationBio = " ";
        if (data.described_entity.field_value.organization_bio) {
            OrganizationBio = data.described_entity.field_value.organization_bio.field_value[lang] ||
                data.described_entity.field_value.organization_bio.field_value[Object.keys(data.described_entity.field_value.organization_bio.field_value)[0]]
        }
        return OrganizationBio || "";
    }


    getStartup(data) {
        let startup = "";
        if (data.described_entity.field_value.startup) {
            startup = Boolean(data.described_entity.field_value.startup.field_value);
        }
        return startup;
    }

    getStartupLabel(data, lang) {
        let StartupLabel = "";
        if (data.described_entity.field_value.startup) {
            StartupLabel = data.described_entity.field_value.startup.field_label[lang] ||
                data.described_entity.field_value.startup.field_label["en"];
        }
        return StartupLabel;
    }

    getCountryOfRegistration(data, lang) {
        let CountryOfRegistration = [];
        if (data.described_entity.field_value.country_of_registration && data.described_entity.field_value.country_of_registration.field_value && data.described_entity.field_value.country_of_registration.field_value.length > 0) {
            data.described_entity.field_value.country_of_registration.field_value.forEach((item, index) =>
                CountryOfRegistration.push(item.label[lang] || item.label[Object.keys(item.label)[0]]))
        }
        return CountryOfRegistration;
    }

    getCountryOfRegistrationLabel(data, lang) {
        let CountryOfRegistrationlabel = "";
        if (data.described_entity.field_value.country_of_registration && data.described_entity.field_value.country_of_registration.field_label) {
            CountryOfRegistrationlabel = data.described_entity.field_value.country_of_registration.field_label[lang] ||
                data.described_entity.field_value.country_of_registration.field_label["en"];
        }
        return CountryOfRegistrationlabel;
    }


    getKeywords(data, lang) {
        let keywords = [];
        if (data.described_entity.field_value.keyword && data.described_entity.field_value.keyword.field_value && data.described_entity.field_value.keyword.field_value.length > 0) {
            data.described_entity.field_value.keyword.field_value.forEach((keyword, index) => {
                keywords.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return keywords;
    }

    getKeywordsLabel(data, lang) {
        let keywordslabel = "";
        if (data.described_entity.field_value.keyword && data.described_entity.field_value.keyword.field_label) {
            keywordslabel = data.described_entity.field_value.keyword.field_label[lang] ||
                data.described_entity.field_value.keyword.field_label[Object.keys(data.described_entity.field_value.keyword.field_label)[0]];

        }
        return keywordslabel;
    }

    getDomainKeywords(data, lang) {
        let domainKeywords = [];
        if (data.described_entity.field_value.domain && data.described_entity.field_value.domain.field_value && data.described_entity.field_value.domain.field_value.length > 0) {
            data.described_entity.field_value.domain.field_value.forEach((keyword, index) => {
                domainKeywords.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
        }
        return domainKeywords;
    }

    getDomainKeywordsLabel(data, lang) {
        let domainKeywordslabel = "";
        if (data.described_entity.field_value.domain && data.described_entity.field_value.domain.field_label) {
            domainKeywordslabel = data.described_entity.field_value.domain.field_label[lang] ||
                data.described_entity.field_value.domain.field_label["en"];

        }
        return domainKeywordslabel;
    }

    getLtAreaKeywords(data, lang) {
        let LtAreaKeywordsArray = data.described_entity.field_value.lt_area ?
            data.described_entity.field_value.lt_area.field_value.map(keyword =>
                (keyword.label ?
                    (keyword.label[lang] ||
                        keyword.label[Object.keys(keyword.label)[0]]) :
                    keyword.value)) : [];
        return LtAreaKeywordsArray;
    }

    getLtAreaKeywordsLabel(data, lang) {
        let LtAreaKeywordslabel = "";
        if (data.described_entity.field_value.lt_area && data.described_entity.field_value.lt_area.field_label) {
            LtAreaKeywordslabel = data.described_entity.field_value.lt_area.field_label[lang] ||
                data.described_entity.field_value.lt_area.field_label["en"];
        }
        return LtAreaKeywordslabel;
    }

    getDisciplineKeywords(data, lang) {
        let disciplines = [];
        if (data.described_entity.field_value.discipline && data.described_entity.field_value.discipline.field_value && data.described_entity.field_value.discipline.field_value.length > 0) {
            data.described_entity.field_value.discipline.field_value.forEach((keyword, index) => {
                disciplines.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return disciplines;
    }

    getDisciplineKeywordsLabel(data, lang) {
        let disciplineslabel = "";
        if (data.described_entity.field_value.discipline && data.described_entity.field_value.discipline.field_label) {
            disciplineslabel = data.described_entity.field_value.discipline.field_label[lang] ||
                data.described_entity.field_value.discipline.field_label["en"];
        }
        return disciplineslabel;
    }

    getservicesOfferedKeywords(data, lang) {
        let servicesOffered = [];
        if (data.described_entity.field_value.service_offered && data.described_entity.field_value.service_offered.field_value && data.described_entity.field_value.service_offered.field_value.length > 0) {
            data.described_entity.field_value.service_offered.field_value.forEach((keyword, index) => {
                servicesOffered.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return servicesOffered;
    }

    getservicesOfferedKeywordsLabel(data, lang) {
        let servicesOfferedlabel = "";
        if (data.described_entity.field_value.service_offered && data.described_entity.field_value.service_offered.field_label) {
            servicesOfferedlabel = data.described_entity.field_value.service_offered.field_label[lang] ||
                data.described_entity.field_value.service_offered.field_label["en"];
        }
        return servicesOfferedlabel;
    }


    getOrganizationEmails(data) {
        let emails = [];
        if (data.described_entity.field_value.email && data.described_entity.field_value.email.field_value && data.described_entity.field_value.email.field_value.length > 0) {
            data.described_entity.field_value.email.field_value.forEach((keyword, index) => {
                emails.push(keyword)
            });
        }
        return emails;
    }

    getOrganizationEmailsLabel(data, lang) {
        let emailslabel = "";
        if (data.described_entity.field_value.email && data.described_entity.field_value.email.field_label) {
            emailslabel = data.described_entity.field_value.email.field_label[lang] ||
                data.described_entity.field_value.email.field_label["en"];
        }
        return emailslabel;
    }


    getOrganizationWebsites(data) {
        let websites = [];
        if (data.described_entity.field_value.website && data.described_entity.field_value.website.field_value && data.described_entity.field_value.website.field_value.length > 0) {
            data.described_entity.field_value.website.field_value.forEach((keyword, index) => {
                websites.push(keyword)
            });
        }
        return websites;
    }

    getOrganizationWebsitesLabel(data, lang) {
        let websiteslabel = "";
        if (data.described_entity.field_value.website && data.described_entity.field_value.website.field_label) {
            websiteslabel = data.described_entity.field_value.website.field_label[lang] ||
                data.described_entity.field_value.website.field_label["en"];
        }
        return websiteslabel;
    }

    getTelephoneNumbers(data) {
        let telephonenumbers = [];
        if (data.described_entity.field_value.telephone_number && data.described_entity.field_value.telephone_number.field_value && data.described_entity.field_value.telephone_number.field_value.length > 0) {
            data.described_entity.field_value.telephone_number.field_value.forEach((keyword, index) => {
                telephonenumbers.push(keyword)
            });
        }
        return telephonenumbers;
    }

    getTelephoneNumbersLabel(data, lang) {
        let telephonenumberslabel = " ";
        if (data.described_entity.field_value.telephone_number && data.described_entity.field_value.telephone_number.field_label) {
            telephonenumberslabel = data.described_entity.field_value.telephone_number.field_label[lang] ||
                data.described_entity.field_value.telephone_number.field_label["en"];
        }
        return telephonenumberslabel;
    }


    getFaxNumbers(data) {
        let faxnumbers = [];
        if (data.described_entity.field_value.fax_number && data.described_entity.field_value.fax_number.field_value && data.described_entity.field_value.fax_number.field_value.length > 0) {
            data.described_entity.field_value.fax_number.field_value.forEach((keyword, index) => {
                faxnumbers.push(keyword)
            });
        }
        return faxnumbers;
    }

    getFaxNumbersLabel(data, lang) {
        let faxnumberslabel = "";
        if (data.described_entity.field_value.fax_number && data.described_entity.field_value.fax_number.field_label) {
            faxnumberslabel = data.described_entity.field_value.fax_number.field_label[lang] ||
                data.described_entity.field_value.fax_number.field_label["en"];
        }
        return faxnumberslabel;
    }


    get_head_office_AddressSet = (data) => {
        let headOfficeAddress = [];
        if (data.described_entity.field_value.head_office_address && data.described_entity.field_value.head_office_address.field_value) {
            headOfficeAddress.push(data.described_entity.field_value.head_office_address.field_value);
        }
        return headOfficeAddress;
    }

    get_head_office_address_label = (data, lang) => {
        let head_office_address_label = "";
        if (data.described_entity.field_value.head_office_address && data.described_entity.field_value.head_office_address.field_label) {
            head_office_address_label = data.described_entity.field_value.head_office_address.field_label[lang] ||
                data.described_entity.field_value.head_office_address.field_label["en"];
        }
        return head_office_address_label;
    }

    get_other_office_address_label = (data, lang) => {
        let other_office_address_label = "";
        if (data.described_entity.field_value.other_office_address && data.described_entity.field_value.other_office_address.field_label) {
            other_office_address_label = data.described_entity.field_value.other_office_address.field_label[lang] ||
                data.described_entity.field_value.other_office_address.field_label["en"];
        }
        return other_office_address_label;
    }

    get_other_office_AddressSet = (data) => {
        let otherOfficeAddress = [];
        if (data.described_entity.field_value.other_office_address && data.described_entity.field_value.other_office_address.field_value && data.described_entity.field_value.other_office_address.field_value.length > 0) {
            otherOfficeAddress = data.described_entity.field_value.other_office_address.field_value;
        }
        return otherOfficeAddress;
    }


    getHasDivision(data) {
        let HasDivision = [];
        if (data.described_entity.field_value.has_division && data.described_entity.field_value.has_division.field_value && data.described_entity.field_value.has_division.field_value.length > 0) {
            HasDivision = data.described_entity.field_value.has_division.field_value;
        }
        return HasDivision;
    }

    getHasDivisionLabel(data, lang) {
        let HasDivisionlabel = "";
        if (data.described_entity.field_value.has_division && data.described_entity.field_value.has_division.field_label) {
            HasDivisionlabel = data.described_entity.field_value.has_division.field_label[lang] ||
                data.described_entity.field_value.has_division.field_label["en"];
        }
        return HasDivisionlabel;
    }

    getSocialMedia(data) {
        let socialMedia = {};
        if (data.described_entity.field_value.social_media_occupational_account && data.described_entity.field_value.social_media_occupational_account.field_value.length > 0) {
            socialMedia = data.described_entity.field_value.social_media_occupational_account.field_value;
        }
        return socialMedia;
    }

    getSocialMediaLabel(data, lang) {
        let socialMedialabel = "";
        if (data.described_entity.field_value.social_media_occupational_account && data.described_entity.field_value.social_media_occupational_account.field_label) {
            socialMedialabel = data.described_entity.field_value.social_media_occupational_account.field_label[lang] ||
                data.described_entity.field_value.social_media_occupational_account.field_label["en"];
        }
        return socialMedialabel;
    }




}


const organizationParser = new OrganizationParser();

export default organizationParser;