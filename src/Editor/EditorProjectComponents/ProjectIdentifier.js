import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { project_identifier_obj } from "../Models/ProjectModel";
import messages from "./../../config/messages"; 
export default class ProjectIdentifier extends React.Component {

    constructor(props) {
        super(props);
        this.state = { project_identifier: this.props.default_valueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            project_identifier: nextProps.default_valueArray || [],
        };
    }

    setIdentifier = (event, identifierIndex, buttonAction, identifierSelectionObjArray) => {
        //const project_identifier = this.props.default_valueArray;
        const { project_identifier } = this.state;
        const action = (event && event.target.name) || buttonAction;
        const project_identifier_obj_2_proccess = project_identifier[identifierIndex] || {};
        switch (action) {
            case "project_identifier_value":
                project_identifier_obj_2_proccess.value = event.target.value;
                project_identifier[identifierIndex] = { ...project_identifier_obj_2_proccess };
                break;
            case "project_identifier_selection":
                project_identifier_obj_2_proccess.project_identifier_scheme = identifierSelectionObjArray[0].value;
                project_identifier[identifierIndex] = { ...project_identifier_obj_2_proccess };
                break;
            case "addIdentifier":
                if (project_identifier.filter(item => item.value).length !== project_identifier.length ||
                    project_identifier.filter(item => item.project_identifier_scheme).length !== project_identifier.length) {
                    return;//don't add a new identifier, if there are blanks in others identifiers
                }
                project_identifier.push(JSON.parse(JSON.stringify(project_identifier_obj)));
                break;
            case "removeIdentifier":
                project_identifier.splice(identifierIndex, 1);
                this.props.updateModel_Identifier(this.props.field, project_identifier);
                break;
            default: break;
        }
        //this.props.updateModel_Identifier(this.props.field, project_identifier);
        this.setState({ project_identifier })
    }

    onBlur = () => {
        this.props.updateModel_Identifier(this.props.field, this.state.project_identifier);
    }


    render() {
        const { label, help_text, required, disable,
            //default_valueArray,type,project_identifier_value_max_length,pk_project_identifier_required, pk_project_identifier_read_only, pk_project_identifier_label, pk_project_identifier_type,
            project_identifier_scheme_choices, project_identifier_scheme_help_text, project_identifier_scheme_label, project_identifier_scheme_required, project_identifier_scheme_type,
            project_identifier_value_label, project_identifier_value_required, project_identifier_value_type, project_identifier_value_placeholder, project_identifier_value_help_text
            //project_identifier_scheme_read_only,project_identifier_value_read_only
        } = this.props;
        const default_valueArray = this.state.project_identifier;
        required && default_valueArray.length === 0 && default_valueArray.push(JSON.parse(JSON.stringify(project_identifier_obj)));
        return <div className="pb-3 inner--group nested--group" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {default_valueArray === 0 && <Button disabled={disable} className="inner-link-default--purple" onClick={(e) => this.setIdentifier(e, null, "addIdentifier")}>{messages.group_elements_create}</Button>}
                    {default_valueArray.filter(identifier => identifier.project_identifier_scheme !== "http://w3id.org/meta-share/meta-share/elg").length === 0 && <Button disabled={disable} className="inner-link-default--purple" onClick={(e) => this.setIdentifier(e, null, "addIdentifier")}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {
                default_valueArray.map((identifier, identifierIndex) => {
                    const default_value = identifier.value || "";
                    const default_project_scheme_choice = project_identifier_scheme_choices.filter(choice => choice.value === identifier.project_identifier_scheme).length > 0 ? project_identifier_scheme_choices.filter(choice => choice.value === identifier.project_identifier_scheme)[0].display_name : "";
                    if (default_project_scheme_choice === "ELG") {
                        return <div key={identifierIndex}></div>
                    }
                    return <div key={identifierIndex} className="pt-3 pb-3">
                        <Grid container spacing={1} direction="row" justifyContent="space-between" alignItems="baseline" >                        

                            <Grid item xs={4}>
                                <TextField className="wd-100" type={project_identifier_scheme_type} required={project_identifier_scheme_required} label={project_identifier_scheme_label} helperText={project_identifier_scheme_help_text} variant="outlined"
                                    disabled={disable}                                    
                                    select
                                    // error={default_project_scheme_choice === ""}
                                    value={default_project_scheme_choice}
                                    inputProps={{ name: 'project_identifier_selection' }}
                                    onChange={(e) => this.setIdentifier(e, identifierIndex, null, project_identifier_scheme_choices.filter(project_scheme_choice => project_scheme_choice.display_name === e.target.value))}
                                >
                                    {
                                        project_identifier_scheme_choices.filter(option => option.value !== "http://w3id.org/meta-share/meta-share/elg").map((project_scheme_choice, project_scheme_choice_index) => (
                                            <MenuItem key={project_scheme_choice_index} value={project_scheme_choice.display_name}>
                                                {project_scheme_choice.display_name}
                                            </MenuItem>
                                        ))
                                    }
                                </TextField></Grid>

                                <Grid item xs={6}>
                                <TextField className="wd-100" type={project_identifier_value_type} required={project_identifier_value_required} label={project_identifier_value_label} helperText={project_identifier_value_help_text} variant="outlined"
                                    //error={default_value === ""}
                                    disabled={disable}
                                    value={default_value}
                                    placeholder={project_identifier_value_placeholder ? project_identifier_value_placeholder : ""}
                                    inputProps={{ name: 'project_identifier_value' }}
                                    onChange={(e) => this.setIdentifier(e, identifierIndex)}
                                /></Grid>  

                            <Grid item xs={2} container direction="row" justifyContent="flex-start" alignItems="baseline" >
                            {((default_value !== "" || default_project_scheme_choice!=="") && ((default_valueArray.length - 1) === identifierIndex)) ?                               
                                <Grid item><Button disabled={disable} onClick={(e) => this.setIdentifier(e, identifierIndex, "addIdentifier")}><AddCircleOutlineIcon className="small-icon" /></Button></Grid>
                            :
                            <Grid item><span></span></Grid>
                            }
                            <Grid item><Button disabled={disable} onClick={(e) => this.setIdentifier(e, identifierIndex, "removeIdentifier")}><RemoveCircleOutlineIcon className="small-icon" /></Button></Grid>
                            </Grid>
                        </Grid>
                    </div>
                })
            }
        </div>
    }
}