import React from "react";
import projectSchemaParser from "./../../parsers/projectSchemaParser";
import ProgressBar from "./../../componentsAPI/CommonComponents/ProgressBar";
import { empty_project } from "./../Models/ProjectModel";
import VerticalTabPanel from './../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import GenericSchemaParser from "./../../parsers/GenericSchemaParser";
import validator from 'validator';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LanguageSpecificTextList from "../editorCommonComponents/LanguageSpecificTextList";
import DomainAutocomplete from "../editorCommonComponents/DomainAutocomplete";
import RecordSubjectAutocomplete from "../editorCommonComponents/RecordSubjectAutocomplete";
import SocialMedia from "../editorCommonComponents/SocialMedia";
import LtArea from "../editorCommonComponents/LtArea";
import Logo from "../editorCommonComponents/Logo";
import DateComponent from "../editorCommonComponents/DateComponent";
import FreeText from "../editorCommonComponents/FreeText";
import WebsiteList from "../editorCommonComponents/WebsiteList";
import EmailList from "../editorCommonComponents/EmailList";
import Cost from "../editorCommonComponents/Cost";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import ActorTypeAutocomplete from "../editorCommonComponents/ActorTypeAutocomplete"
import OrganizationAutocomplete from "./OrganizationAutocomplete";
import OrganizationAutocompleteSingle from "./OrganizationAutocompleteSingle";
import ProjectIdentifier from "./ProjectIdentifier"
import IsDescribedByAutocomplete from "../EditorServiceToolComponents/IsDescribedByAutocomplete";
import { generic_organization_obj } from "../Models/GenericModels";
import CountryAutocompleteMultiple from "./CountryAutocompleteMultiple";
import ProjectAutocomplete from "../editorCommonComponents/ProjectAutocomplete";
import { PROJECT_TABS_HEADERS } from "../../config/editorConstants";
import { switchIcon } from "../../config/editorConstants";
import LanguageSpecificTextListLookup from "../editorCommonComponents/LanguageSpecificTextListLookup";
import LanguageSpecificTextRichEditor from "../editorCommonComponents/LanguageSpecificTextRichEditor";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export default class CreateProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = { schema: props.schema, keycloak: props.keycloak, model: JSON.parse(JSON.stringify(empty_project)), tab: 0, grantNumberError: false };
        this.isUnmound = false;
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }


    updateModel = (model) => {
        this.setState({ model });
    }

    updateModel_generic_String = (obj2update, stringValue) => {
        const { model } = this.props;
        if (!model.described_entity[obj2update]) {
            model.described_entity[obj2update] = "";
        }
        model.described_entity[obj2update] = stringValue;
        this.props.updateModel(model);
    }

    setObjectGeneral = (field, valueObj) => {
        const { model } = this.props;
        model.described_entity[field] = valueObj;
        this.props.updateModel(model);
    }

    removeObjectGeneral = (field) => {
        const { model } = this.props;
        delete model.described_entity[field];
        this.props.updateModel(model);
    }

    updateModel_generic_array = (obj2update, valueArray) => {
        const { model } = this.props;
        if (!model.described_entity[obj2update]) {
            model.described_entity[obj2update] = [];
        }
        model.described_entity[obj2update] = valueArray;
        this.props.updateModel(model);
    }

    setStatus = (event, max_length) => {
        const { model } = this.props;
        if (!model.described_entity.status) {
            model.described_entity.status = "";
        }
        model.described_entity.status = event.target.value;
        this.props.updateModel(model);
    }

    setGrantnumber = (event, index) => {
        const { model } = this.props;
        if (!model.described_entity.grant_number) {
            model.described_entity.grant_number = "";
        }
        model.described_entity.grant_number = event.target.value;
        this.props.updateModel(model);
    }

    blur = (event, max_length) => {
        const valid = validator.isLength(event.target.value, { max: max_length });
        this.setState({ grantNumberError: !valid });
    }

    setSelectFundingCountry = (selectedObj) => {
        const { model } = this.props;
        if (!model.described_entity.funding_country) {
            model.described_entity.funding_country = [];
        }
        model.described_entity.funding_country = selectedObj.map(item => item.value);
        this.props.updateModel(model);
    }

    setCountrySelection = (selection) => {
        const { default_valueObj } = this.state;
        default_valueObj.country = selection.value;
        this.props.updateModel_address_set_obj(this.props.field, default_valueObj);
    }

    setSelectFundingType = (event, selectedObjArray) => {
        const { model } = this.props;
        model.described_entity.funding_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.props.updateModel(model);
    }

    onDescriptionChange = () => {
        this.props.onDescriptionChange();
    } 
    
    render() {
        if (!this.state.schema) {
            return <ProgressBar />
        }

        const { grantNumberError } = this.state;
        const { model } = this.props;
        const data = this.state.schema;

        const logo_default_value = model.described_entity.logo || "";
        const status_form_props = GenericSchemaParser.getFormElement("status", data.status);
        const { help_text: grant_number_help_text, label: grant_number_label, required: grant_number_required, type: grant_number_type, max_length: grant_number_max_length, placeholder: grant_number_placeholder } = projectSchemaParser.getGrantNumber(data);
        const grant_number_default_value = model.described_entity.grant_number || "";
        const grantNumberValidationError = "Value should be " + grant_number_max_length + " characters length";


        return <>

            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">

                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {PROJECT_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<><span>{switchIcon(tab)}</span><span> {tab} </span>  </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                        </Tabs>

                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("project_name", data.project_name)} defaultValueObj={model.described_entity.project_name || { "en": "" }} field="project_name" setLanguageSpecificText={this.setObjectGeneral} /></div>
                                    <div className="pb-3">
                                        <ProjectIdentifier
                                            {...GenericSchemaParser.getFormElement("project_identifier", data.project_identifier)}
                                            project_identifier_scheme_choices={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.choices}
                                            project_identifier_scheme_help_text={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.help_text}
                                            project_identifier_scheme_label={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.label}
                                            project_identifier_scheme_required={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.required}
                                            project_identifier_scheme_type={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.type}
                                            project_identifier_value_label={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.value.label}
                                            project_identifier_value_required={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.value.required}
                                            project_identifier_value_type={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.value.type}
                                            project_identifier_value_placeholder={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.value.placeholder ? GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.value.placeholder : ""}
                                            project_identifier_value_help_text={GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.value.help_text ? GenericSchemaParser.getFormElement("project_identifier", data.project_identifier).formElements.value.help_text : ""}
                                            default_valueArray={model.described_entity.project_identifier || []}
                                            field="project_identifier"
                                            updateModel_Identifier={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3"><TextField className="wd-100" type={grant_number_type} required={grant_number_required} label={grant_number_label}
                                        helperText={grantNumberError ? `${grantNumberValidationError}` : grant_number_help_text} variant="outlined" value={grant_number_default_value} placeholder={grant_number_placeholder}
                                        inputProps={{ name: 'grant_number', maxLength: grant_number_max_length }} onChange={(e) => this.setGrantnumber(e)}
                                        onBlur={(e) => this.blur(e, grant_number_max_length)}
                                        error={grantNumberError} />
                                    </div>
                                    <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("project_short_name", data.project_short_name)} defaultValueObj={model.described_entity.project_short_name || { "en": "" }} field="project_short_name" setLanguageSpecificText={this.setObjectGeneral} /></div>
                                    {false && <div className="pb-3"><LanguageSpecificTextList  {...GenericSchemaParser.getFormElement("project_alternative_name", data.project_alternative_name)} default_valueArray={model.described_entity.project_alternative_name || []} field="project_alternative_name" setLanguageSpecifictextList={this.setObjectGeneral} /></div>}
                                    {/*<div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("project_summary", data.project_summary)} defaultValueObj={model.described_entity.project_summary || { "en": "" }} field="project_summary" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>*/}
                                    <div className="pb-3"><LanguageSpecificTextRichEditor  onDescriptionChange={this.onDescriptionChange} {...GenericSchemaParser.getFormElement("project_summary", data.project_summary)} defaultValueObj={model.described_entity.project_summary || { "en": "" }} field="project_summary" multiline={true} rowsMax={6} setLanguageSpecificText={this.setObjectGeneral} /></div>

                                    <div className="pb-3"><EmailList className="wd-100" {...GenericSchemaParser.getFormElement("email", data.email)} default_value_Array={model.described_entity.email ? JSON.parse(JSON.stringify(model.described_entity.email)) : []} field="email" updateModel_email={this.updateModel_generic_array} /></div>
                                    <div className="pb-3"><WebsiteList className="wd-100" {...GenericSchemaParser.getFormElement("website", data.website)} default_value_Array={model.described_entity.website ? JSON.parse(JSON.stringify(model.described_entity.website)) : []} field="website" updateModel_website={this.updateModel_generic_array} /></div>
                                    <div className="pb-3"><Logo className="wd-100" {...GenericSchemaParser.getFormElement("logo", data.logo)} logo={logo_default_value} field="logo" keycloak={this.props.keycloak} model={model} updateModel_logo={this.setObjectGeneral} /></div>
                                    <div className="pb-3">
                                        <AutocompleteChoicesChips className="wd-100"
                                            {...GenericSchemaParser.getFormElement("funding_type", data.funding_type)}
                                            initialValuesArray={model.described_entity.funding_type || []}
                                            field="funding_type"
                                            lr_subclass={false}
                                            updateModel_array={this.updateModel_generic_array}
                                        />
                                    </div>
                                    <div className="pb-3"><ActorTypeAutocomplete  {...GenericSchemaParser.getFormElement("funder", data.funder)} initialValueArray={model.described_entity.funder || []} field="funder" updateModel_Array={this.setObjectGeneral} /></div>
                                    <div className="pb-3">
                                        <CountryAutocompleteMultiple
                                            className="wd-100"
                                            {...GenericSchemaParser.getFormElement("funding_country", data.funding_country)}
                                            initialValuesArray={model.described_entity.funding_country || []}
                                            field="funding_country"
                                            lr_subclass={false}
                                            updateModel_array={this.updateModel_generic_array}
                                        />
                                    </div>
                                    <div className="pb-3"><DateComponent className="wd-100" {...GenericSchemaParser.getFormElement("project_start_date", data.project_start_date)} initialValue={model.described_entity.project_start_date || null} maxDate={model.described_entity.project_end_date || null} field="project_start_date" updateModel={this.updateModel_generic_String} /></div>
                                    <div className="pb-3"><DateComponent className="wd-100" {...GenericSchemaParser.getFormElement("project_end_date", data.project_end_date)} initialValue={model.described_entity.project_end_date || null} minDate={model.described_entity.project_start_date || null} field="project_end_date" updateModel={this.updateModel_generic_String} /></div>
                                    <div className="pb-3"><TextField className="wd-100" placeholder={status_form_props.placeholder ? status_form_props.placeholder : ""} helperText={status_form_props.help_text} required={status_form_props.required} label={status_form_props.label} variant="outlined" value={model.described_entity.status || ""} inputProps={{ name: 'status' }} onChange={(e) => this.setStatus(e)} /></div>
                                    <div className="pb-3"><SocialMedia {...projectSchemaParser.getSocialMedia(data)} default_valueArray={model.described_entity.social_media_occupational_account ? JSON.parse(JSON.stringify(model.described_entity.social_media_occupational_account)) : []} field="social_media_occupational_account" updateModel_SocialMedia={this.updateModel_generic_array} /></div>
                                    <div><ProjectAutocomplete  {...GenericSchemaParser.getFormElement("replaces_project", data.replaces_project)} initialValueArray={model.described_entity.replaces_project || []} field="replaces_project" updateModel_Array={this.setObjectGeneral} /> </div>
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={1} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div><LtArea {...projectSchemaParser.getLtAreaKeywords(data)} default_valueArray={model.described_entity.lt_area || []} updateModel_ltArea={this.setObjectGeneral} /></div>
                                    <div><DomainAutocomplete   {...projectSchemaParser.getDomainKeywords(data)} default_valueArray={model.described_entity.domain || []} updateModel_Domain={this.updateModel_generic_array} /></div>
                                    <div><RecordSubjectAutocomplete   {...projectSchemaParser.getSubjectKeywords(data)} default_valueArray={model.described_entity.subject || []} updateModel_Subject={this.updateModel_generic_array} /></div>
                                    {/*<div><LanguageSpecificTextList  {...GenericSchemaParser.getFormElement("keyword", data.keyword)} default_valueArray={model.described_entity.keyword || []} field="keyword" setLanguageSpecifictextList={this.setObjectGeneral} /></div>*/}
                                    <div><LanguageSpecificTextListLookup  {...GenericSchemaParser.getFormElement("keyword", data.keyword)} default_valueArray={model.described_entity.keyword || []} field="keyword" field_type="keyword" entity_type="project" setLanguageSpecifictextList={this.updateModel_generic_array} /></div>
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={2} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3"><OrganizationAutocompleteSingle  {...GenericSchemaParser.getFormElement("coordinator", data.coordinator)} initialValue={model.described_entity.coordinator || JSON.parse(JSON.stringify(generic_organization_obj))} field="coordinator" updateModel={this.setObjectGeneral} /></div>
                                    <div className="pb-3"><OrganizationAutocomplete {...GenericSchemaParser.getFormElement("participating_organization", data.participating_organization)} initialValueArray={model.described_entity.participating_organization || []} field="participating_organization" updateModel_Array={this.setObjectGeneral} /></div>
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={3} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3"><Cost  {...GenericSchemaParser.getFormElement("cost", data.cost)} initialValue={model.described_entity.cost} field="cost" updateModel={this.setObjectGeneral} /></div>
                                    <div className="pb-3"><Cost  {...GenericSchemaParser.getFormElement("ec_max_contribution", data.ec_max_contribution)} initialValue={model.described_entity.ec_max_contribution} field="ec_max_contribution" updateModel={this.setObjectGeneral} /></div>
                                    <div className="pb-3"><Cost  {...GenericSchemaParser.getFormElement("national_max_contribution", data.national_max_contribution)} initialValue={model.described_entity.national_max_contribution} field="national_max_contribution" updateModel={this.setObjectGeneral} /></div>
                                    <div className="pb-3"><FreeText className="wd-100" {...GenericSchemaParser.getFormElement("funding_scheme_category", data.funding_scheme_category)} initialValue={model.described_entity.funding_scheme_category} field="funding_scheme_category" updateModel={this.updateModel_generic_String} /></div>
                                    <div className="pb-3"><FreeText className="wd-100" {...GenericSchemaParser.getFormElement("related_call", data.related_call)} initialValue={model.described_entity.related_call} field="related_call" updateModel={this.updateModel_generic_String} /></div>
                                    <div className="pb-3"><FreeText className="wd-100" {...GenericSchemaParser.getFormElement("related_programme", data.related_programme)} initialValue={model.described_entity.related_programme} field="related_programme" updateModel={this.updateModel_generic_String} /></div>
                                    <div className="pb-3"><FreeText className="wd-100" {...GenericSchemaParser.getFormElement("related_subprogramme", data.related_subprogramme)} initialValue={model.described_entity.related_subprogramme} field="related_subprogramme" updateModel={this.updateModel_generic_String} /></div>
                                    <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("project_report", data.project_report)} defaultValueObj={model.described_entity.project_report || { "en": "" }} field="project_report" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>
                                    <div className="pb-3"><IsDescribedByAutocomplete model={model} className="wd-100"   {...GenericSchemaParser.getFormElement("is_related_to_document", data.is_related_to_document)} initialValueArray={model.described_entity.is_related_to_document || []} field="is_related_to_document" updateModel_Array={this.setObjectGeneral} /></div>
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>
                    </div>
                </div>
            </form>

        </>
    }
}