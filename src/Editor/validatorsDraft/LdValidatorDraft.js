import * as yup from "yup";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { validateObject, validateObjectOptional, generalMessage } from "../validators/OrganizationValidator";
import {
    actorValidation, funding_project_validation, lr_validation, document_validation,
    licence_validation, costValidation, sizeValidation, isValidURL, source_of_metadata_record_validation,//, url_regex 
    language_validation
} from "./commonValidatorSchemaDraft";
import { language_description_subclass_validator, LdMediaTypeValidator } from "./LdAuxiliaryValidatorsDraft";
import {
    LD_TOP_TABS_HEADERS as se,//section
    LD_FIRST_SECTION_TABS_HEADERS as f,//first 
    LD_SECOND_SECTION_TABS_HEADERS as s,//second
    LD_THIRD_SECTION_TABS_HEADERS as t,//third
    LD_FOURTH_SECTION_TABS_HEADERS as fo,//fourth
    SUBCLASS_TYPE_MODEL
} from "../../config/editorConstants";
import {
    //CD_ROM, DVD_R, 
    ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY,
    //BLURAY, 
    DOWNLOADABLE,
    //HARD_DISK, OTHER, UNSPECIFIED
    //PAPER_COPY,
} from "../Models/CorpusModel";//same for ld

const p = GenericSchemaParser;

const fieldLocation = (step, tab) => `<strong style="color: black">  ${step} >  ${tab} >  </strong> `;

export const draft_validateLd = (model, data, subclass, schema_text_part, schema_audio_part, schema_video_part, schema_text_numerical, schema_image_part, schema_grammar_part, schema_ml_model_part, for_info) => {
    const schema = yup.object().shape({
        source_of_metadata_record: yup.object().notRequired().nullable().default(null).shape(
            source_of_metadata_record_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + "Source of metadata record")
        ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + "Source of metadata record"),
        described_entity: yup.object().shape({
            entity_type: yup.string().label("Entity type").notRequired().max(1000).default(null).nullable(),
            lr_identifier: yup.array().of(
                yup.object().shape({
                    lr_identifier_scheme: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.label).required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.label)).default(null).nullable(),
                    value: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + "Language Resource identifier value").required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + "Language Resource identifier value")).max(1000).default(null).nullable()
                }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).label).default(null).nullable()
            ).notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).label).nullable().default(null),
            resource_name: validateObject(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + `${p.getFormElement("resource_name", data.resource_name).label}`, 1000).default(null).nullable(),
            resource_short_name: validateObjectOptional(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("resource_short_name", data.resource_short_name).label, 300).notRequired().nullable().default(null),
            //draft discrepancy
            description: validateObjectOptional(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + `${p.getFormElement("description", data.description).label}`, 10000).default(null).nullable(),
            version: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + `${p.getFormElement("version", data.version).label}`).notRequired().max(50).nullable().default(null),
            version_date: yup.date().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("version_date", data.version_date).label).notRequired().nullable().default(null),
            resource_provider: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label)).shape(actorValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label)).default(null).nullable(),
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label).notRequired().nullable().default(null),
            resource_creator: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label)).shape(actorValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label)).default(null).nullable()
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label).notRequired().nullable().default(null),
            publication_date: yup.date().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("publication_date", data.publication_date).label).notRequired().nullable().default(null),
            funding_project: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label)).shape(funding_project_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label)).default(null).nullable()
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label).notRequired().nullable().default(null),
            logo: yup.string().label("").url(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[0]) + "Language resource logo must be a valid URL").nullable().default(null).notRequired(),
            //
            intended_application: yup.array().of(
                yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + p.getFormElement("intended_application", data.intended_application).label).notRequired().max(200).default(null).nullable(),
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + p.getFormElement("intended_application", data.intended_application).label).nullable().default(null).when('lr_subclass', function (lr_subclass, schema) {
                return lr_subclass.ld_subclass === SUBCLASS_TYPE_MODEL ? schema.notRequired() : schema.notRequired();
            }),
            complies_with: yup.array().of(
                yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + p.getFormElement("complies_with", data.complies_with).label).notRequired().default(null).nullable(),
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + p.getFormElement("complies_with", data.complies_with).label).nullable().default(null).notRequired(),
            domain: yup.array().of(
                yup.object().shape({
                    category_label: validateObject(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + "Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label, 150, generalMessage("Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label)).default(null).nullable(),
                    domain_identifier: yup.object().default(null).nullable().notRequired().shape({
                        domain_classification_scheme: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + "Domain classification scheme").required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + "Domain classification scheme")).default(null).nullable(),
                        value: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + "domain identifier value").max(1000).required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + "domain identifier value")).default(null).nullable(),
                    }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + "domain identifier").default(null).nullable(),
                })
            ).notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + "Domain").nullable().default(null),
            //draft discrepancy
            keyword: yup.array().of(
                validateObject(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + p.getFormElement('keyword', data.keyword).label, 100).default(null).nullable(),
            ).notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[1]) + `${p.getFormElement('keyword', data.keyword).label}`).nullable().default(null),
            //
            //draft discrepancy
            additional_info: yup.array().of(
                yup.object().shape({
                    landing_page: yup.string().max(1000).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " website").default(null).nullable().test({
                        name: 'landing_page url',
                        test: function (landing_page) {
                            if (landing_page) {
                                if (!isValidURL(landing_page)) {
                                    return this.createError({ message: fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " must be a valid URL", path: "" });
                                }
                            }
                            return true;
                        },
                    }),
                    email: yup.string().email().max(254).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " email").default(null).nullable()
                })
            ).notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + `${p.getFormElement("additional_info", data.additional_info).label}`).default(null).nullable().test({
                name: "validate existance of at least one entry",
                test: function (additional_info) {
                    for (let index = 0; additional_info && index < additional_info.length; index++) {
                        const { landing_page, email } = additional_info[index];
                        if (!landing_page && !email) {
                            return this.createError({ message: fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + `${p.getFormElement("additional_info", data.additional_info).label} landing page or email must be filled`, path: "" });
                        }
                    }
                    return true;
                }
            }),
            contact: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + p.getFormElement("contact", data.contact).label)).shape(actorValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + p.getFormElement("contact", data.contact).label)).default(null).nullable()
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[2]) + p.getFormElement("contact", data.contact).label).notRequired().nullable().default(null),
            //
            is_documented_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label)).shape(document_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label))
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label).notRequired().nullable().default(null),
            is_to_be_cited_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_documented_by).label)).shape(document_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by).label))
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by).label).notRequired().nullable().default(null),
            //
            //draft discrepancy
            relation: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("relation", data.relation).label)).shape({
                    related_lr: yup.object().nullable().default(null).required(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("relation", data.relation).formElements.related_lr.label + " required").shape(lr_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("relation", data.relation).formElements.related_lr.label)),
                    //draft discrepancy
                    relation_type: validateObjectOptional(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement('relation', data.relation).formElements.relation_type.label, 150).nullable().default(null)
                }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("relation", data.relation).label).notRequired().nullable().default(null)
            ).nullable().default(null),
            is_part_of: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label)).shape(lr_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label))
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label).notRequired().nullable().default(null),
            is_similar_to: yup.array().of(
                yup.object().required(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + generalMessage(p.getFormElement("is_similar_to", data.is_similar_to).label)).shape(lr_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_similar_to", data.is_similar_to).label))
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_similar_to", data.is_similar_to).label).notRequired().nullable().default(null),
            is_related_to_lr: yup.array().of(
                yup.object().required(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + generalMessage(p.getFormElement("is_related_to_lr", data.is_related_to_lr).label)).shape(lr_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_related_to_lr", data.is_related_to_lr).label))
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_related_to_lr", data.is_related_to_lr).label).notRequired().nullable().default(null),
            replaces: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("replaces", data.replaces).label)).shape(lr_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("replaces", data.replaces).label))
            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("replaces", data.replaces).label).notRequired().nullable().default(null),
            is_version_of: yup.object().notRequired().nullable().default(null).shape(lr_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[0], f[4]) + p.getFormElement("is_version_of", data.is_version_of).label)),
            ///////////////////
            lr_subclass: yup.object().shape({
                //draft discrepancy
                language_description_subclass: yup.object().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("language_description_subclass", subclass.language_description_subclass).label}`).test(
                    language_description_subclass_validator(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]), schema_grammar_part, schema_ml_model_part)
                ),
                ld_subclass: yup.string().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("language_description_subclass", subclass.language_description_subclass).label}`),
                //draft discrepancy
                personal_data_included: for_info ? yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("personal_data_included", subclass.personal_data_included).label}`) : yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("personal_data_included", subclass.personal_data_included).label}`),
                personal_data_details: validateObjectOptional(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + p.getFormElement("personal_data_details", subclass.personal_data_details).label, 500).notRequired().nullable().default(null),
                sensitive_data_included: for_info ? yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("sensitive_data_included", subclass.sensitive_data_included).label}`) : yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("sensitive_data_included", subclass.sensitive_data_included).label}`),
                sensitive_data_details: validateObjectOptional(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + p.getFormElement("sensitive_data_details", subclass.sensitive_data_details).label, 500).notRequired().nullable().default(null),
                anonymized: yup.string().when(['personal_data_included', 'sensitive_data_included'], {
                    is: (personal_data_included, sensitive_data_included) => personal_data_included === "http://w3id.org/meta-share/meta-share/yesP" || sensitive_data_included === "http://w3id.org/meta-share/meta-share/yesS",
                    then: yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("anonymized", subclass.anonymized).label}`),
                    otherwise: yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("anonymized", subclass.anonymized).label}`),
                }).notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("anonymized", subclass.anonymized).label}`),
                anonymization_details: validateObjectOptional(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + p.getFormElement("anonymization_details", subclass.anonymization_details).label, 1000).notRequired().nullable().default(null),
                /*required_hardware: yup.array().of(
                    yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + p.getFormElement("required_hardware", subclass.required_hardware).label).notRequired().default(null).nullable(),
                ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + p.getFormElement("required_hardware", subclass.required_hardware).label).notRequired().nullable().default(null),*/
                additional_hw_requirements: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[1], s[0]) + `${p.getFormElement("additional_hw_requirements", subclass.additional_hw_requirements).label}`).max(500).notRequired().nullable().default(null),
                ///////////////////
                //draft discrepancy
                language_description_media_part: yup.array().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0]) + p.getFormElement("language_description_media_part", subclass.language_description_media_part).label).test(
                    LdMediaTypeValidator(
                        fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0]),
                        schema_text_part, schema_video_part, schema_image_part)
                ).when('ld_subclass', function (ld_subclass, schema) {
                    return (ld_subclass === SUBCLASS_TYPE_MODEL || for_info) ? schema.notRequired() : schema.notRequired();
                }),
                unspecified_part: yup.object().shape({
                    language: yup.array().of(
                        yup.object().shape(language_validation(`${fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.language.label}`))
                    ).notRequired().nullable().default(null).label(`${fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.language.label}`),
                    metalanguage: yup.array().of(
                        yup.object().shape(language_validation(`${fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.metalanguage.label}`))
                    ).notRequired().nullable().default(null).label(`${fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.metalanguage.label}`),
                    multilinguality_type: yup.string().default(null).nullable().label(`${fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0])}` + subclass.unspecified_part.children.multilinguality_type.label).when('language', (language, schema) => {
                        return language && language.length >= 2 ? schema.notRequired() : schema.notRequired();
                    }),
                    multilinguality_type_details: validateObjectOptional(`${fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0])} ${subclass.unspecified_part.children.multilinguality_type_details.label}`, subclass.unspecified_part.children.multilinguality_type_details.max_length).notRequired().nullable().default(null),
                }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label).nullable().default(null).when('ld_subclass', function (ld_subclass, schema) {
                    if (ld_subclass === SUBCLASS_TYPE_MODEL || for_info) {
                        return schema.notRequired();
                    } else {
                        return schema.notRequired();
                    }
                }),
                ///////////////////
                //draft discrepancy
                dataset_distribution: yup.array().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("dataset_distribution", subclass.dataset_distribution).label).of(
                    yup.object().required().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("dataset_distribution", subclass.dataset_distribution).label).shape({
                        //draft discrepancy
                        dataset_distribution_form: yup.string().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("dataset_distribution_form", subclass.dataset_distribution.child.children.dataset_distribution_form).label),
                        /*distribution_location: yup.string().max(1000).default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_location", subclass.dataset_distribution.child.children.distribution_location).label)
                            //draft discrepancy
                            .when('dataset_distribution_form', (dataset_distribution_form, schema) => {
                                return [BLURAY, CD_ROM, HARD_DISK, DVD_R, OTHER, UNSPECIFIED].includes(dataset_distribution_form) ? schema.notRequired() : schema.notRequired();
                            }).test({
                                name: 'distribution_location valid url',
                                test: function (distribution_location) {
                                    if (distribution_location) {
                                        if (!isValidURL(distribution_location)) {
                                            return this.createError({ message: fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_location", subclass.dataset_distribution.child.children.distribution_location).label + " must be a valid URL", path: "" });
                                        }
                                    }
                                    return true;
                                },
                            }),*/
                        access_location: yup.string().max(1000).default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("access_location", subclass.dataset_distribution.child.children.access_location).label)
                            //draft discrepancy
                            .when(['dataset_distribution_form'], (dataset_distribution_form, schema) => {
                                return [ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY].includes(dataset_distribution_form) ? schema.notRequired() : schema.notRequired();
                            }).test({
                                name: 'access_location valid url',
                                test: function (access_location) {
                                    if (access_location) {
                                        if (!isValidURL(access_location)) {
                                            return this.createError({ message: fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("access_location", subclass.dataset_distribution.child.children.access_location).label + " must be a valid URL", path: "" });
                                        }
                                    }
                                    return true;
                                },
                            }),
                        download_location: yup.string().max(1000).default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("download_location", subclass.dataset_distribution.child.children.download_location).label).when(['dataset_distribution_form', 'access_location'], (dataset_distribution_form, access_location, schema) => {
                            if (dataset_distribution_form !== DOWNLOADABLE) {
                                return schema.notRequired();
                            }
                            if (access_location) {
                                return schema.notRequired();
                            }
                            if (model && model.management_object && model.management_object.size > 0) {
                                return schema.notRequired();
                            }
                            //draft discrepancy
                            return schema.notRequired();
                        }).test({
                            name: 'download_location valid url',
                            test: function (download_location) {
                                if (download_location) {
                                    if (!isValidURL(download_location)) {
                                        return this.createError({ message: fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("download_location", subclass.dataset_distribution.child.children.download_location).label + " must be a valid URL", path: "" });
                                    }
                                }
                                return true;
                            },
                        }),
                        samples_location: yup.array().notRequired().default(null).nullable().of(
                            yup.string().url().max(1000).default(null).nullable().notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("samples_location", subclass.dataset_distribution.child.children.samples_location).label)
                        ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("samples_location", subclass.dataset_distribution.child.children.samples_location).label),
                        membership_institution: yup.array().notRequired().default(null).nullable().of(
                            yup.string().default(null).nullable().notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("membership_institution", subclass.dataset_distribution.child.children.membership_institution).label)
                        ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("membership_institution", subclass.dataset_distribution.child.children.membership_institution).label),
                        //draft discrepancy
                        licence_terms: yup.array().of(
                            yup.object().required(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + generalMessage(p.getFormElement("licence_terms", subclass.dataset_distribution.child.children.licence_terms).label)).shape(licence_validation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("licence_terms", subclass.dataset_distribution.child.children.licence_terms).label))
                        ).notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("licence_terms", subclass.dataset_distribution.child.children.licence_terms).label).nullable().default(null),
                        cost: yup.object().shape(costValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Distribution Cost")).nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Distribution Cost"),
                        access_rights: yup.array().of(
                            yup.object().shape({
                                category_label: validateObject(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights category label", 150, generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights category label")).required().default(null).nullable(),
                                access_rights_statement_identifier: yup.object().default(null).nullable().notRequired().shape({
                                    access_rights_statement_scheme: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights statement scheme").required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights statement scheme")).default(null).nullable(),
                                    value: yup.string().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights statement value").max(1000).required(generalMessage(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights statement value")).default(null).nullable(),
                                }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights statement identifier").default(null).nullable(),
                            })
                        ).notRequired().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + "Access rights").nullable().default(null),
                        distribution_text_feature: yup.array().of(
                            //draft discrepancy
                            yup.object().notRequired(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_text_feature", subclass.dataset_distribution.child.children.distribution_text_feature).label).shape({
                                //draft discrepancy
                                size: yup.array().notRequired().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]), subclass.dataset_distribution.child.children.distribution_text_feature.child.children.size.child.children))
                                ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_text_feature.child.children.size).label),
                                //draft discrepancy
                                data_format: yup.array().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_text_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_text_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_text_feature", subclass.dataset_distribution.child.children.distribution_text_feature).label).nullable().default(null)
                            .when('language_description_media_part', function (language_description_media_part, schema) {
                                if (model.described_entity.lr_subclass.language_description_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.language_description_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.language_description_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/text") {
                                            //draft discrepancy
                                            return schema.notRequired();
                                        }
                                    }
                                }
                                schema.notRequired();
                            }),
                        distribution_video_feature: yup.array().of(
                            //draft discrepancy
                            yup.object().notRequired(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_video_feature", subclass.dataset_distribution.child.children.distribution_video_feature).label).shape({
                                size: yup.array().notRequired().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]), subclass.dataset_distribution.child.children.distribution_video_feature.child.children.size.child.children))
                                ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_video_feature.child.children.size).label),
                                //draft discrepancy
                                data_format: yup.array().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_video_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_video_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_video_feature", subclass.dataset_distribution.child.children.distribution_video_feature).label).nullable().default(null)
                            .when('language_description_media_part', function (language_description_media_part, schema) {
                                if (model.described_entity.lr_subclass.language_description_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.language_description_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.language_description_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/video") {
                                            ////draft discrepancy
                                            return schema.notRequired();
                                        }
                                    }
                                }
                                schema.notRequired();
                            }),
                        distribution_image_feature: yup.array().of(
                            ////draft discrepancy
                            yup.object().notRequired(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_image_feature", subclass.dataset_distribution.child.children.distribution_image_feature).label).shape({
                                size: yup.array().notRequired().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]), subclass.dataset_distribution.child.children.distribution_image_feature.child.children.size.child.children))
                                ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_image_feature.child.children.size).label),
                                //draft discrepancy
                                data_format: yup.array().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_image_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_image_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_image_feature", subclass.dataset_distribution.child.children.distribution_image_feature).label).nullable().default(null)
                            .when('language_description_media_part', function (language_description_media_part, schema) {
                                if (model.described_entity.lr_subclass.language_description_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.language_description_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.language_description_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/image") {
                                            ////draft discrepancy    
                                            return schema.notRequired();
                                        }
                                    }
                                }
                                schema.notRequired();
                            }),
                        distribution_audio_feature: yup.array().of(
                            //draft discrepancy
                            yup.object().notRequired(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_audio_feature", subclass.dataset_distribution.child.children.distribution_audio_feature).label).shape({
                                size: yup.array().notRequired().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]), subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.size.child.children))
                                ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.size).label),
                                audio_format: yup.array().nullable().default(null).notRequired().of(
                                    yup.object().notRequired().nullable().default(null).shape({
                                        //draft discrepancy
                                        data_format: yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format.child.children.data_format.label),
                                        byte_order: yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format.child.children.byte_order.label),
                                        //draft discrepancy
                                        compressed: yup.boolean().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format.child.children.compressed.label),
                                    }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("audio_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format).label),
                                ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("audio_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format).label),
                                duration_of_audio: yup.array().notRequired().nullable().default(null).notRequired().of(
                                    yup.object().notRequired().nullable().default(null).shape({
                                        //draft discrepancy
                                        amount: yup.number().notRequired().default(null).nullable().min(0).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio.child.children.amount.label),
                                        //draft discrepancy
                                        duration_unit: yup.string().notRequired().default(null).nullable().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio.child.children.duration_unit.label),
                                    }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("duration_of_audio", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio).label),
                                ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("duration_of_audio", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio).label),
                                //draft discrepancy
                                data_format: yup.array().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_audio_feature", subclass.dataset_distribution.child.children.distribution_audio_feature).label).nullable().default(null)
                            .when('language_description_media_part', function (language_description_media_part, schema) {
                                if (model.described_entity.lr_subclass.language_description_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.language_description_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.language_description_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/audio") {
                                            //draft discrepancy    
                                            return schema.notRequired();
                                        }
                                    }
                                }
                                schema.notRequired();
                            }),
                        distribution_unspecified_feature: yup.object().label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_unspecified_feature", subclass.dataset_distribution.child.children.distribution_unspecified_feature).label).shape({
                            size: yup.array().nullable().default(null).of(
                                yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]), subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.size.child.children))
                            ).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.size).label),
                            data_format: yup.array().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.data_format).label).of(
                                yup.string().notRequired().nullable().default(null).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.data_format).label)
                            )
                        }).label(fieldLocation(se(model.described_entity.lr_subclass.ld_subclass)[3], fo[0]) + p.getFormElement("distribution_unspecified_feature", subclass.dataset_distribution.child.children.distribution_unspecified_feature).label).nullable().default(null)
                            .when('ld_subclass', function (ld_subclass, schema) {
                                if (ld_subclass === SUBCLASS_TYPE_MODEL || for_info) {
                                    return schema.notRequired();
                                } else {
                                    return schema.notRequired();
                                }
                            }),
                    })
                )
            }).label("language description subclass").required().default(null).nullable()
        })
    });


    return schema.validate(model, { abortEarly: false });
}