import * as yup from "yup";
import organizationSchemaParser from "../../parsers/organizationSchemaParser";
import { Division_of_schema } from "../validators/OrganizationValidator";
export const generalMessage = (label) => `${label} is a required field. Please fill in the required information or remove it from the form`;

const testLanguageValue = (value) => {
    if (!value) {
        return false;
    }
    let result = true;
    Object.values(value).forEach(val => result && val ? result = true : result = false);
    return result;
}

const testLanguageKey = (value) => {
    if (!value) {
        return false;
    }
    let result = true;
    Object.keys(value).forEach(val => result && val ? result = true : result = false);
    return result;
}

export const validateObject = (label, max_length, generalMessage) => {
    return yup.object().label(label)
        //.test('value', `${label} is required`, (value) => {
        .test('value', generalMessage ? generalMessage : `${label} is required`, (value) => {
            return testLanguageValue(value);
        })
        .test('language key', `${label} language is required`, (value) => {
            return testLanguageKey(value);
        })
        .test('length of value', `${label} should be less than ${max_length} characters`, (value) => {
            if (!value) {
                return true;// return true for nulls?
            }
            if (max_length) {
                let result = true;
                Object.values(value).forEach(val => result && val.length <= max_length ? result = true : result = false);
                return result;
            } else {
                return true;
            }
        })
}

export const validateObjectOptional = (label, max_length) => {
    return yup.object().label(label)
        .test('language key', `${label} language is required`, (value) => {
            if (!value) {
                return true;
            }
            return testLanguageKey(value);
        })
        .test('length of value', `${label} should be less than ${max_length} characters`, (value) => {
            if (!value) {
                return true;
            }
            if (max_length) {
                let result = true;
                Object.values(value).forEach(val => result && val.length <= max_length ? result = true : result = false);
                return result;
            } else {
                return true;
            }
        })
}

export const draft_validateOrganization = (model, data) => {
    const schema = yup.object().shape({
        described_entity: yup.object().shape({
            entity_type: yup.string().label("Entity type").notRequired().max(1000),
            organization_name: validateObject(organizationSchemaParser.getOrganizationName(data).label, 300).required().nullable().default(null),
            organization_short_name: yup.array().of(
                validateObjectOptional(organizationSchemaParser.getOrganizationShortName(data).label, 100).nullable().default(null)//.required()
            ).notRequired().label(organizationSchemaParser.getOrganizationShortName(data).label).nullable().default(null),
            organization_alternative_name: yup.array().of(
                validateObjectOptional(organizationSchemaParser.getOrganizationAltName(data).label, 300).nullable().default(null)//.required()
            ).notRequired().label(organizationSchemaParser.getOrganizationAltName(data).label).nullable().default(null),
            organization_identifier: yup.array().of(
                yup.object().shape({
                    organization_identifier_scheme: yup.string().label(organizationSchemaParser.getOrganizationIdentifier(data).organization_identifier_scheme_label).required(generalMessage(organizationSchemaParser.getOrganizationIdentifier(data).organization_identifier_scheme_label)).nullable().default(null),
                    value: yup.string().label("organization identifier value").required(generalMessage("organization identifier value")).max(1000).nullable().default(null),
                }).label(organizationSchemaParser.getOrganizationIdentifier(data).label).nullable().default(null),
            ).notRequired().label(organizationSchemaParser.getOrganizationIdentifier(data).label).nullable().default(null),
            organization_bio: validateObjectOptional(organizationSchemaParser.getOrganizationBio(data).label, 10000).notRequired().nullable().default(null),
            logo: yup.string().label("").url("Organization logo must point to a valid URL").nullable().default(null).notRequired(),
            lt_area: yup.array().of(yup.string().label("lt area").required().max(200)).notRequired().label("lt area").nullable().default(null),
            service_offered: yup.array().of(
                validateObjectOptional(organizationSchemaParser.getservicesOfferedKeywords(data).label, 100).nullable().default(null)//.required()
            ).notRequired().label(organizationSchemaParser.getservicesOfferedKeywords(data).label).nullable().default(null),
            discipline: yup.array().of(
                validateObjectOptional(organizationSchemaParser.getDisciplineKeywords(data).label, 100).nullable().default(null)//.required()
            ).notRequired().label(organizationSchemaParser.getDisciplineKeywords(data).label).nullable().default(null),
            keyword: yup.array().of(
                validateObjectOptional(organizationSchemaParser.getKeywords(data).label, 100).nullable().default(null)//.required()
            ).notRequired().label(organizationSchemaParser.getKeywords(data).label).nullable().default(null),
            domain: yup.array().of(
                yup.object().shape({
                    category_label: validateObject("Domain " + organizationSchemaParser.getDomainKeywords(data).category_label, 150, generalMessage("Domain " + organizationSchemaParser.getDomainKeywords(data).category_label)).required().nullable().default(null),
                    domain_identifier: yup.object().default(null)
                        .nullable().notRequired().shape({
                            domain_classification_scheme: yup.string().label(organizationSchemaParser.getDomainKeywords(data).domain_label).required(generalMessage(organizationSchemaParser.getDomainKeywords(data).domain_label)).nullable().default(null),
                            value: yup.string().label("domain identifier value").max(1000).required(generalMessage("domain identifier value")).nullable().default(null)
                        }).label("domain identifier")
                })
            ).notRequired().label("Domain").nullable().default(null),
            email: yup.array().of(
                yup.string().email().max(254).label("Organization email").nullable().default(null),
            ).notRequired().label("Email").nullable().default(null),
            website: yup.array().of(
                yup.string().url("Organization website must point to a valid URL").max(1000).label("").nullable().default(null)
            ).notRequired().label("Website").nullable().default(null),
            head_office_address: yup.object().nullable().default(null).notRequired().label(organizationSchemaParser.GetHeadOfficeAddress(data).label).shape({
                address: validateObjectOptional(organizationSchemaParser.GetHeadOfficeAddress(data).address_label, 300).notRequired().nullable().default(null),
                region: validateObjectOptional(organizationSchemaParser.GetHeadOfficeAddress(data).region_label, 200).notRequired().nullable().default(null),
                zip_code: yup.string().label(organizationSchemaParser.GetHeadOfficeAddress(data).zip_code_label).max(20).notRequired().nullable().default(null),
                city: validateObjectOptional(organizationSchemaParser.GetHeadOfficeAddress(data).city_label, 100).notRequired().nullable().default(null),
                country: yup.string().label("Head office " + organizationSchemaParser.GetHeadOfficeAddress(data).country_label).required(generalMessage("Head office " + organizationSchemaParser.GetHeadOfficeAddress(data).country_label)).nullable().default(null),
            }),
            other_office_address: yup.array().notRequired().nullable().default(null).label(organizationSchemaParser.GetOtherOfficeAddress(data).label).of(
                yup.object().nullable().default(null).notRequired().label(organizationSchemaParser.GetOtherOfficeAddress(data).label).shape({
                    address: validateObjectOptional(organizationSchemaParser.GetOtherOfficeAddress(data).address_label, 300).notRequired().nullable().default(null),
                    region: validateObjectOptional(organizationSchemaParser.GetOtherOfficeAddress(data).region_label, 200).notRequired().nullable().default(null),
                    zip_code: yup.string().label(organizationSchemaParser.GetOtherOfficeAddress(data).zip_code_label).max(20).notRequired().nullable().default(null),
                    city: validateObjectOptional(organizationSchemaParser.GetOtherOfficeAddress(data).city_label, 100).notRequired().nullable().default(null),
                    country: yup.string().label("Other office " + organizationSchemaParser.GetOtherOfficeAddress(data).country_label).required(generalMessage("Other office " + organizationSchemaParser.GetOtherOfficeAddress(data).country_label)).nullable().default(null),
                }),
            ),
            telephone_number: yup.array().of(
                yup.string().max(31).label(organizationSchemaParser.getTelephoneNumbers(data).label).required().nullable().default(null)
            ).notRequired().label(organizationSchemaParser.getTelephoneNumbers(data).label).nullable().default(null),
            fax_number: yup.array().of(
                yup.string().max(31).label(organizationSchemaParser.getFaxNumbers(data).label).required()
            ).notRequired().label(organizationSchemaParser.getFaxNumbers(data).label).nullable().default(null),
            social_media_occupational_account: yup.array().of(
                yup.object().required().label(organizationSchemaParser.getSocialMedia(data).label).shape({
                    social_media_occupational_account_type: yup.string().label(organizationSchemaParser.getSocialMedia(data).social_media_label).required(generalMessage(organizationSchemaParser.getSocialMedia(data).social_media_label)).nullable().default(null),
                    value: yup.string().label("social media value").max(1000).required(generalMessage("social media value")).nullable().default(null),
                }),
            ).notRequired().label(organizationSchemaParser.getSocialMedia(data).label).nullable().default(null),
            member_of_association: yup.array().of(
                yup.object().shape({
                    organization_name: validateObject("Member of association organization name", 300, generalMessage("Member of association organization name")).required().nullable().default(null),
                    organization_short_name: yup.array().of(
                        validateObjectOptional("Member of association organization short name", 100).nullable().default(null)//.required()
                    ).notRequired().label("Member of association organization short name").nullable().default(null),
                    website: yup.array().of(
                        yup.string().url("Member of association websites must point to valid URLs").max(1000).label("").nullable().default(null),
                    ).notRequired().label("website").nullable().default(null),
                    organization_identifier: yup.array().of(
                        yup.object().shape({
                            organization_identifier_scheme: yup.string().label(organizationSchemaParser.get_member_of_association(data).organization_identifier_label).required(generalMessage(organizationSchemaParser.get_member_of_association(data).organization_identifier_label)).nullable().default(null),
                            value: yup.string().label("organization identifier value").required(generalMessage("organization identifier value")).max(1000).nullable().default(null),
                        }).label(organizationSchemaParser.get_member_of_association(data).label)
                    ).notRequired().label(organizationSchemaParser.get_member_of_association(data).label).nullable().default(null),
                    is_division_of: yup.array().of(
                        yup.object().notRequired().nullable().default(null).label("Member of association is division of")
                            .test({
                                name: 'test is division of recursion',
                                test: function (is_division_of) {
                                    const pending_array_with_is_division_of_obj = [];
                                    while (is_division_of) {
                                        if (Array.isArray(is_division_of)) {
                                            for (let i = 0; i < is_division_of.length; i++) {
                                                const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                                Division_of_schema(organizationSchemaParser.get_member_of_association(data).label + " ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                                const isDivisionOf = is_division_of[i]["is_division_of"];
                                                if (isDivisionOf && isDivisionOf.length > 0) {
                                                    pending_array_with_is_division_of_obj.push(isDivisionOf);
                                                }
                                            }
                                        } else {
                                            const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                            Division_of_schema(organizationSchemaParser.get_member_of_association(data).label + " ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                            const isDivisionOf = is_division_of["is_division_of"];
                                            if (isDivisionOf && isDivisionOf.length > 0) {
                                                pending_array_with_is_division_of_obj.push(isDivisionOf);
                                            }
                                        }
                                        is_division_of = pending_array_with_is_division_of_obj.shift()
                                    }
                                    return true;
                                },
                            })
                    )
                })
            ).notRequired().label(organizationSchemaParser.get_member_of_association(data).label).nullable().default(null),
            division_category: yup.string().notRequired().nullable().default(null).label("Division category"),
            //draft deviation
            is_division_of: yup.array().of(
                yup.object().shape({
                    organization_name: validateObject("Is division of", 300, generalMessage("Is division of")).notRequired().nullable().default(null),
                    organization_short_name: yup.array().of(
                        validateObjectOptional("Is division of organization short name", 100).nullable().default(null)//.required()
                    ).notRequired().label("Is division of organization short name").nullable().default(null),
                    website: yup.array().of(
                        yup.string().url("Is division of Organization website must point to valid URLs").max(1000).label("").nullable().default(null),
                    ).notRequired().label("website").nullable().default(null),
                    organization_identifier: yup.array().of(
                        yup.object().shape({
                            organization_identifier_scheme: yup.string().label("Is division of organization identifier scheme").required(generalMessage("Is division of organization identifier scheme")).nullable().default(null),
                            value: yup.string().label("Is division of organization identifier value").required(generalMessage("Is division of  organization identifier value")).max(1000).nullable().default(null),
                        }).label("Is division of organization identifier")
                    ).notRequired().label("Is division of").nullable().default(null),
                    is_division_of: yup.array().of(
                        yup.object().notRequired().nullable().default(null).label("is division of")
                            .test({
                                name: 'test is division of recursion',
                                test: function (is_division_of) {
                                    const pending_array_with_is_division_of_obj = [];
                                    while (is_division_of) {
                                        if (Array.isArray(is_division_of)) {
                                            for (let i = 0; i < is_division_of.length; i++) {
                                                const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                                Division_of_schema("").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                                const isDivisionOf = is_division_of[i]["is_division_of"];
                                                if (isDivisionOf && isDivisionOf.length > 0) {
                                                    pending_array_with_is_division_of_obj.push(isDivisionOf);
                                                }
                                            }
                                        } else {
                                            const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                            Division_of_schema("").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                            const isDivisionOf = is_division_of["is_division_of"];
                                            if (isDivisionOf && isDivisionOf.length > 0) {
                                                pending_array_with_is_division_of_obj.push(isDivisionOf);
                                            }
                                        }
                                        is_division_of = pending_array_with_is_division_of_obj.shift()
                                    }
                                    return true;
                                }
                            })
                    )
                })
            ).notRequired().nullable().default(null).label("Is division of"),
            replaces_organization: yup.array().of(
                yup.object().shape({
                    organization_name: validateObject("Replaces organization name", 300, generalMessage("Replaces organization name")).required().nullable().default(null),
                    organization_short_name: yup.array().of(
                        validateObjectOptional("Replaces organization short name", 100).nullable().default(null)//.required()
                    ).notRequired().label("Replaces organization short name").nullable().default(null),
                    website: yup.array().of(
                        yup.string().url("Replaces organization website must point to valid URLs").max(1000).label("").nullable().default(null),
                    ).notRequired().label("website").nullable().default(null),
                    organization_identifier: yup.array().of(
                        yup.object().shape({
                            organization_identifier_scheme: yup.string().label("Replaces organization identifier scheme").required(generalMessage("Replaces organization identifier scheme")).nullable().default(null),
                            value: yup.string().label("Replaces organization identifier value").required(generalMessage("Replaces organization identifier value")).max(1000).nullable().default(null),
                        }).label(organizationSchemaParser.get_member_of_association(data).label)
                    ).notRequired().label("Replaces organization").nullable().default(null),
                    is_division_of: yup.array().of(
                        yup.object().notRequired().nullable().default(null).label("is division of")
                            .test({
                                name: 'test is division of recursion',
                                test: function (is_division_of) {
                                    const pending_array_with_is_division_of_obj = [];
                                    while (is_division_of) {
                                        if (Array.isArray(is_division_of)) {
                                            for (let i = 0; i < is_division_of.length; i++) {
                                                const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                                Division_of_schema("Replaces ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                                const isDivisionOf = is_division_of[i]["is_division_of"];
                                                if (isDivisionOf && isDivisionOf.length > 0) {
                                                    pending_array_with_is_division_of_obj.push(isDivisionOf);
                                                }
                                            }
                                        } else {
                                            const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                            Division_of_schema("Replaces ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                            const isDivisionOf = is_division_of["is_division_of"];
                                            if (isDivisionOf && isDivisionOf.length > 0) {
                                                pending_array_with_is_division_of_obj.push(isDivisionOf);
                                            }
                                        }
                                        is_division_of = pending_array_with_is_division_of_obj.shift()
                                    }
                                    return true;
                                },
                            })
                    )
                })
            ).notRequired().label("Replaces organization").nullable().default(null),
        })
    });

    //var message = await schema.validate(model);
    //.then(valid => message = valid)
    //catch(error => message = error);
    return schema.validate(model);
    //return message;
    //return schema;
}