import React from "react";
import { Helmet } from "react-helmet";
import axios from "axios";
import DashboardAppBar from "../DashboardComponents/DashboardAppBar";
import ProgressBar from "../componentsAPI/CommonComponents/ProgressBar";
import { EDITOR_MODELS_SCHEMA } from "../config/editorConstants";
import Container from '@material-ui/core/Container';
import { empty_project } from "./Models/ProjectModel";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CreateProject from "./EditorProjectComponents/CreateProject";
/*import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import RefreshIcon from '@material-ui/icons/Refresh';*/
import DisplayModel from "./editorCommonComponents/DisplayModel";
import LookupProjectName from "./EditorProjectComponents/LookupProjectName";
import Grid from '@material-ui/core/Grid';
import ValidationYupErrors from "./editorCommonComponents/ValidationYupErrors";
import UserWarning from "./editorCommonComponents/UserWarning";
export default class ProjectEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schema: null, keycloak: props.keycloak, model: props.model ? JSON.parse(JSON.stringify(props.model)) : JSON.parse(JSON.stringify(empty_project)), backend_error_response: null, lookupProjectName: props.model ? false : true, yupError: null,
      showWarning: false
    };
    this.isUnmound = false;
  }

  componentDidMount() {
    this.isUnmound = true;
    axios.options(EDITOR_MODELS_SCHEMA("project")).then(res => {
      if (this.isUnmound) {
        if (res.data.actions && res.data.actions.POST) {
          this.setState({ schema: res.data.actions.POST });
        }
      }
    }).catch(err => { console.log(err) });
  }

  set_backend_error_response = (error) => {
    this.setState({ backend_error_response: error })
  }

  set_backend_successful_response = (response) => {
    this.setState({ model: response, backend_error_response: null })
  }

  set_yup_error = (yupError) => {
    this.setState({ yupError });
  }

  updateModel = (model) => {
    this.setState({ model });
  }

  hideLookUpByName = (projectName, restProperties) => {
    const { model } = this.state;
    if (restProperties && restProperties.pk) {
      //model.pk = restProperties.pk;
      model.described_entity.pk = restProperties.pk;//add pk of generic record inside described_entity
      delete restProperties.pk;
    }
    model.described_entity.project_name = { "en": projectName };
    model.described_entity = { ...model.described_entity, ...restProperties };
    this.setState({ lookupProjectName: false, model: model });
  }

  handleDescriptionChange = () => {
    //this.setState({showWarning: true});
    const { model } = this.state;
    const { project_summary = "" } = model.described_entity || {};
    (project_summary && project_summary["en"] && project_summary["en"].length < 50) ? this.setState({ showWarning: true }) : this.setState({ showWarning: false })
  }


  render() {
    if (!this.state.schema) {
      return <ProgressBar />
    }

    const data = this.state.schema;

    const { lookupProjectName } = this.state;
    if (lookupProjectName) {
      return <LookupProjectName hideLookUpByName={this.hideLookUpByName} keycloak={this.props.keycloak} />
    }

    return (
      <>
        <Helmet>
          <title>ELG - Create Project</title>
        </Helmet>
        <DashboardAppBar />
        <div className="editor-container-white pb-2">
          <Container maxWidth="xl">
            <div className="empty"></div>
            <Typography className="dashboard-title-box pb-05">Create a new project</Typography>
            <div>
              {this.state.backend_error_response && <div>
                <h3>Error</h3>
                <div className=" boxed">
                  <Paper elevation={13} >
                    <code>
                      <pre id="special">
                        {JSON.stringify(this.state.backend_error_response, null, 2)}
                      </pre>
                    </code>
                  </Paper>
                </div>
              </div>
              }
            </div>
            {!this.state.backend_error_response && <ValidationYupErrors key={this.state.yupError ? JSON.stringify(this.state.yupError) : "1"} yupError={this.state.yupError} requiredFields={[]} hide_under_construction="true" />}
            {<UserWarning showWarning={this.state.showWarning} />}
          </Container>


          <div className="editor-actions-header">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="space-between" alignItems="flex-start" className="grays--offwhite">
                <Grid item container xs={12} alignItems="center">
                  <DisplayModel data={data} model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <Container maxWidth="xl">
            <div className="editor-main-card-container">
              <CreateProject  onDescriptionChange={this.handleDescriptionChange} {...this.props} {...this.state} updateModel={this.updateModel} keycloak={this.props.keycloak} />
            </div>
          </Container>
          <div className="editor-actions-bottom">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="flex-end" alignItems="center" className="pb-3 pt-3 grays--offwhite">
                <Grid item container xs={12} alignItems="center">
                  <DisplayModel data={data} model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <div>
            {this.state.backend_error_response && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
              <h3>Error</h3>
              <div className=" boxed">
                <Paper elevation={13} >
                  <code>
                    <pre id="special">
                      {JSON.stringify(this.state.backend_error_response, null, 2)}
                    </pre>
                  </code>
                </Paper>
              </div>
            </div>
            }
          </div>
        </div>

      </>
    );
  }
}