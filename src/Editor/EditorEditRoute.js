import React from "react";
import axios from "axios";
import { withRouter, Redirect } from "react-router-dom";
import GoToCatalogue from "../componentsAPI/CommonComponents/GoToCatalogue";
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import ProgressBar from "../componentsAPI/CommonComponents/ProgressBar";
import { EDITOR_PERMITTED_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES, IS_ADMIN, IS_CONTENT_MANAGER, RETRIEVE_RECORD_INFO } from "../config/constants";
import { EDITOR_COMPATIBLE_SCHEMA_METADATARECORD } from "../config/editorConstants";
class EditorEditRoute extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null, keycloak: props.keycloak, UserRoles: [], loading: false,
            isOwner: false, isLegalValidator: false, isMetadataValidator: false, isTechnicalValidator: false, source: null
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
        this.getMetadataResourceDetail();
    }

    getMetadataResourceDetail = () => {
        const id = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const { keycloak } = this.state;
        var isAuthorized = false;
        EDITOR_PERMITTED_ROLES.forEach(item => {
            if (AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak).includes(item)) {
                isAuthorized = true;
            }
        });
        if (id && keycloak && keycloak.authenticated && isAuthorized) {
            const url = EDITOR_COMPATIBLE_SCHEMA_METADATARECORD(id);
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ loading: true, source: source });
            const metadataRequest = axios.get(url, { cancelToken: source.token });
            const retrieveRecordInfoRequest = axios.get(RETRIEVE_RECORD_INFO(id), { cancelToken: source.token });
            axios.all([metadataRequest, retrieveRecordInfoRequest])
                .then(axios.spread((...responses) => {
                    const metadataRequestResponse = responses[0]
                    const retrieveRecordInfoRequestResponse = responses[1]
                    const { curator = false, legal_validator = false, metadata_validator = false, technical_validator = false } = retrieveRecordInfoRequestResponse.data;
                    this.setState({
                        data: JSON.parse(JSON.stringify(metadataRequestResponse.data)),
                        isOwner: curator, isLegalValidator: legal_validator, isMetadataValidator: metadata_validator, isTechnicalValidator: technical_validator,
                        loading: false
                    })
                }))
                .catch(errors => {
                    console.log(errors);
                    this.setState({ data: null, loading: false });
                })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const thisId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const prevId = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.id;
        if (thisId !== prevId) {
            this.setState({ data: null });
            this.getMetadataResourceDetail();
        };
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        EDITOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        if (!isAuthorized) {
            return false;
        }
        const { data, keycloak } = this.state;
        const { status = null, deleted = false } = data.management_object || {};
        if (deleted) {
            isAuthorized = false;
        } else if (this.state.isOwner && ((status === "i") || (status === "d"))) {
            isAuthorized = true;
        } else if (IS_ADMIN(keycloak) && ((status === "i") || (status === "g") || (status === "d") || (status === "u"))) {
            isAuthorized = true;
        } else if (IS_CONTENT_MANAGER(keycloak) && ((status === "i") || (status === "g") || (status === "d") || (status === "u"))) {
            isAuthorized = true;
        } else {
            isAuthorized = false;
        }
        return isAuthorized;
    }

    render() {
        if (this.state.loading) {
            return <ProgressBar />
        }
        if (!this.props.keycloak || !this.props.keycloak.authenticated) {
            return <Redirect to="/" />
        }
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);
        if (!isAuth) {
            return <div>
                <div className="editor--header">
                    <Container maxWidth="xl"><Typography variant="h1" className="padding5 font-color-white">ELG EDITOR</Typography></Container>
                    <GoToCatalogue />
                </div>
                <Container style={{ padding: '0', maxWidth: "100%" }}>
                    <div>You do not have sufficient rights.</div>
                </Container>
            </div>
        }
        return (
            <div>
                {
                    React.cloneElement(this.props.children, {
                        model: this.state.data,
                        keycloak: this.state.keycloak,
                        isOwner: this.state.isOwner,
                        isTechnicalValidator: this.state.isTechnicalValidator,
                        isLegalValidator: this.state.isLegalValidator,
                        isMetadataValidator: this.state.isMetadataValidator
                    })
                }
            </div>
        )
    }
}
export default withRouter(EditorEditRoute);