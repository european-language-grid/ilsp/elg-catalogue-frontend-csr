import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import { sampleObj } from "../Models/ToolModel";
import SampleItem from "./SampleItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class SampleArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { sampleArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            sampleArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, sampleItemIndex, value) => {
        const { sampleArray } = this.state;
        switch (action) {
            case "addsampleItem":
                const filteredArray = sampleArray.filter(item => item && (item.sample_text || item.tag || item.samples_location));
                if (filteredArray.length !== sampleArray.length) {
                    return;//do not add element if required filds are blank
                }
                sampleArray.push(JSON.parse(JSON.stringify(sampleObj)));
                break;
            case "removesampleItem":
                sampleArray.splice(sampleItemIndex, 1);
                break;
            case "setsampleItem":
                sampleArray[sampleItemIndex] = value;
                this.props.updateModel_array(this.props.field, sampleArray);
                break;
            default: break;
        }
        this.setState({ sampleArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.sampleArray);
    }


    render() {
        const { sampleArray } = this.state;
        //console.log(this.props)

        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>

                {(sampleArray.length === 0) &&
                    <Grid item sm={1}>
                        <Tooltip title={`${messages.group_elements_create} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addsampleItem")}>{messages.group_elements_create}</Button></Tooltip>
                    </Grid>
                }
            </Grid>
            {
                sampleArray.map((sampleItem, sampleItemIndex) => {
                    return <div key={sampleItemIndex} onBlur={() => this.onBlur()}>
                        <SampleItem  {...this.props} sampleItem={sampleItem} sampleItemIndex={sampleItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            {sampleItemIndex >= 0 && <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removesampleItem", sampleItemIndex)}>{messages.array_elements_remove}</Button></Tooltip>}
                            {(sampleItem.sample_text || sampleItem.samples_location || sampleItem.tag) && <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addsampleItem")}>{messages.array_elements_add}</Button></Tooltip>}
                        </div>
                        <div style={{ marginTop: "5em" }} /></div>
                })
            }
        </div>
    }
}