import React from "react";
//import Grid from '@material-ui/core/Grid'
//import Typography from '@material-ui/core/Typography';
//import Button from '@material-ui/core/Button';
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import Website from "../editorCommonComponents/Website";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import LanguageModel from "./LanguageModel";
//import TypeSystem from "./Typesystem";
import LRAutocompleteSingle from "../EditorCorpusComponents/LRAutocompleteSingle";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
//import { resourceRelationsObj } from "../Models/LrModel";
import SampleArray from "./SampleArray";

export default class InputContentResourceItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { inputContentItem: props.inputContentItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            inputContentItem: nextProps.inputContentItem
        };
    }

    setSingleSelectListGeneric = (event, selectedObjArray, field, lr_subclass = false) => {
        const { inputContentItem } = this.state;
        if (selectedObjArray.length === 0) {
            selectedObjArray[0] = { value: null };
        }
        inputContentItem[field] = selectedObjArray[0].value;
        this.setState({ inputContentItem });
    }

    setString = (field, value) => {
        const { inputContentItem } = this.state;
        inputContentItem[field] = value;
        this.setState({ inputContentItem });
    }

    setArray = (field, array) => {
        const { inputContentItem } = this.state;
        if (!inputContentItem[field]) inputContentItem[field] = [];
        inputContentItem[field] = array;
        this.setState({ inputContentItem }, this.onBlur);
    }

    setObj = (field, obj) => {
        if (obj && obj.resource_name && !obj.resource_name["en"]) {
            return;
        }
        const { inputContentItem } = this.state;
        if (!inputContentItem[field]) inputContentItem[field] = {};
        inputContentItem[field] = obj;
        this.setState({ inputContentItem }, this.onBlur);
    }

    editorPlaceholderCheck = () => {
        const item = this.state.inputContentItem;
        if (this.props.field === "output_resource") {
            const websiteExist = item.samples_location ? true : false;
            const processing_resource_typeExist = item.processing_resource_type ? true : false;
            const media_type_Exists = item.media_type ? true : false;
            const data_formatExists = item.data_format && item.data_format.length ? true : false;
            const annotation_typeExists = item.annotation_type && item.annotation_type.length ? true : false;
            const segmentation_levelExists = item.segmentation_level && item.segmentation_level.length ? true : false;
            const character_encodingExists = item.character_encoding && item.character_encoding.length ? true : false;
            const modality_typeExists = item.modality_type && item.modality_type.length ? true : false;
            const modality_type_detailsExists = item.modality_type_details && item.modality_type_details.hasOwnProperty("en") && item.modality_type_details["en"] ? true : false;
            const typesystemExists = item.typesystem && item.typesystem !== null && Object.keys(item.typesystem).length && !item.typesystem.hasOwnProperty("editor-placeholder") ? true : false;
            const annotation_schemaExists = item.annotation_schema && item.annotation_schema !== null && Object.keys(item.annotation_schema).length && !item.annotation_schema.hasOwnProperty("editor-placeholder") ? true : false;
            const annotation_resourceExists = item.annotation_resource && item.annotation_resource !== null && Object.keys(item.annotation_resource).length && !item.annotation_resource.hasOwnProperty("editor-placeholder") ? true : false;
            const languageExists = item.language && item.language.filter(l => l.language_tag || l.language_id || l.script_id || l.region_id || (l.variant_id && l.variant_id.length) || (l.language_variety_name && l.language_variety_name.hasOwnProperty("pk") && l.language_variety_name["en"])).length ? true : false;
            //console.log(websiteExist, processing_resource_typeExist, media_type_Exists, data_formatExists, annotation_typeExists, segmentation_levelExists, character_encodingExists, modality_typeExists, modality_type_detailsExists, typesystemExists, annotation_schemaExists, annotation_resourceExists, languageExists);
            if (websiteExist === false && processing_resource_typeExist === false && media_type_Exists === false && data_formatExists === false && annotation_typeExists === false && segmentation_levelExists === false && character_encodingExists === false && modality_typeExists === false && modality_type_detailsExists === false && typesystemExists === false && annotation_schemaExists === false && annotation_resourceExists === false && languageExists === false) {
                item["editor-placeholder"] = true;
            } else {
                delete item["editor-placeholder"];
            }
        }
        return item;
    }

    onBlur = () => {
        const item = this.editorPlaceholderCheck();
        this.props.setValues("setInputContentItem", this.props.inputContentItemIndex, item);
    }

    render() {
        let { inputContentItem } = this.state;
        let languageIsRequiredRule = false;
        if (this.props.model && this.props.model.described_entity && this.props.model.described_entity.lr_subclass) {
            languageIsRequiredRule = this.props.model.described_entity.lr_subclass.language_dependent;
        }
        if (!languageIsRequiredRule) {
            inputContentItem.language = [];//delete language if language_dependent is false
        }
        return <div onBlur={this.onBlur}>
            <RecordSelectList className="wd-100"  {...this.props.formElements.processing_resource_type} default_value={inputContentItem.processing_resource_type || ""} field="processing_resource_type" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} />
            {languageIsRequiredRule && <div className="mb2"><LanguageModel className="wd-100" group_className={"white_nested_group"} {...this.props.formElements.language} required={languageIsRequiredRule} initialValuesArray={inputContentItem.language || []} field="language" updateModel_array={this.setArray} /></div>}
            <RecordSelectList className="wd-100"  {...this.props.formElements.media_type} default_value={inputContentItem.media_type || ""} field="media_type" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} />
            <AutocompleteRecommendedChoices className="wd-100"  {...this.props.formElements.data_format} recommended_choices={this.props.formElements.data_format.choices} initialValuesArray={inputContentItem.data_format || []} field="data_format" updateModel_array={this.setArray} />
            <AutocompleteRecommendedChoices className="wd-100"  {...this.props.formElements.annotation_type} recommended_choices={this.props.formElements.annotation_type.choices} initialValuesArray={inputContentItem.annotation_type || []} field="annotation_type" updateModel_array={this.setArray} />
            <SampleArray className="wd-100" model={this.props.model} {...this.props.formElements.sample} initialValueArray={inputContentItem.sample || []} field="sample" updateModel_array={this.setArray} />
            {false && <AutocompleteChoicesChips className="wd-100"  {...this.props.formElements.character_encoding} initialValuesArray={inputContentItem.character_encoding || []} field="character_encoding" updateModel_array={this.setArray} />}
            {false && <AutocompleteChoicesChips className="wd-100"  {...this.props.formElements.segmentation_level} initialValuesArray={inputContentItem.segmentation_level || []} field="segmentation_level" updateModel_array={this.setArray} />}
            {false && <Website key={inputContentItem.samples_location || ""} className="wd-100"  {...this.props.formElements.samples_location} website={inputContentItem.samples_location || ""} field="samples_location" updateModel_website={this.setString} />}
            {false && <LRAutocompleteSingle className="wd-100" formStuff={this.props.formElements.typesystem.formElements} label={this.props.formElements.typesystem.label} initialValue={inputContentItem.typesystem || JSON.parse(JSON.stringify({}))} help_text={this.props.formElements.typesystem.help_text} index="typesystem" field="typesystem" updateModel={this.setObj} />}  {/**notice the index prop instead of field!!! */}
            {false && <LRAutocompleteSingle className="wd-100" label={this.props.formElements.annotation_schema.label} help_text={this.props.formElements.annotation_schema.help_text} {...this.props.formElements.annotation_schema} formStuff={this.props.formElements.annotation_schema.formElements} initialValue={inputContentItem.annotation_schema || JSON.parse(JSON.stringify({}))} index="annotation_schema" field="annotation_schema" updateModel={this.setObj} />} {/**notice the index prop instead of field!!! */}
            {false && <LRAutocompleteSingle className="wd-100" label={this.props.formElements.annotation_resource.label} help_text={this.props.formElements.annotation_resource.help_text} {...this.props.formElements.annotation_resource} formStuff={this.props.formElements.annotation_resource.formElements} initialValue={inputContentItem.annotation_resource || JSON.parse(JSON.stringify({}))} index="annotation_resource" field="annotation_resource" updateModel={this.setObj} />} {/**notice the index prop instead of field!!! */}
            {false && <AutocompleteChoicesChips className="wd-100"  {...this.props.formElements.modality_type} initialValuesArray={inputContentItem.modality_type || []} field="modality_type" updateModel_array={this.setArray} />}
            {false && <LanguageSpecificText
                {...this.props.formElements.modality_type_details}
                defaultValueObj={inputContentItem.modality_type_details || { "en": "" }}
                field={"modality_type_details"}
                multiline={true} maxRows={6}
                setLanguageSpecificText={this.setObj}
            />}
        </div >
    }
}