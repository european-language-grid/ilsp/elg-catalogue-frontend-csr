import React from "react";
import FreeText from "../editorCommonComponents/FreeText";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";


export default class ParameterEnumerationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { enumerationItem: props.enumerationItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            enumerationItem: nextProps.enumerationItem
        };
    }

    setObjectGeneral = (field, valueObj) => {
        const { enumerationItem } = this.state;
        enumerationItem[field] = valueObj;
        this.setState({ enumerationItem }, this.onBlur);
    }

    setString = (field, value) => {
        const { enumerationItem } = this.state;
        enumerationItem[field] = value;
        this.setState({ enumerationItem }, this.onBlur);
    }


    onBlur = () => {
        const { enumerationItem } = this.state;
        this.props.updateModel("setenumerationItem", this.props.enumerationItemIndex, enumerationItem);
    }


    render() {
        const { enumerationItem } = this.state;
        //console.log(this.props)
        return <div onBlur={this.onBlur}>
            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.value_name} initialValue={enumerationItem.value_name} field="value_name" lr_subclass={true} updateModel={this.setString} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.value_label} defaultValueObj={enumerationItem.value_label || { "en": "" }} field="value_label" multiline={false} setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.value_description} defaultValueObj={enumerationItem.value_description || { "en": "" }} field="value_description" multiline={true} maxRows={3} setLanguageSpecificText={this.setObjectGeneral} /></div>
        </div>
    }

}