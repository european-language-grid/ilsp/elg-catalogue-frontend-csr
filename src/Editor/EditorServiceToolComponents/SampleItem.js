import React from "react";
import Grid from '@material-ui/core/Grid';
import FreeText from "../editorCommonComponents/FreeText";
import Website from "../editorCommonComponents/Website";
import FilePondDocument from "../editorCommonComponents/FilePondDocument"


export default class SampleItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { sampleItem: props.sampleItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            sampleItem: nextProps.sampleItem
        };
    }

    setString = (field, value) => {
        const { sampleItem } = this.state;
        sampleItem[field] = value;
        this.setState({ sampleItem }, this.onBlur);
    }

    setDocument = (index, url) => {
        const { sampleItem } = this.state;
        sampleItem[index] = encodeURI(url);//index="samples_location"
        this.setState({ sampleItem }, this.onBlur);
    }

    onBlur = () => {
        const { sampleItem } = this.state;
        this.props.updateModel("setsampleItem", this.props.sampleItemIndex, sampleItem);
    }

    render() {
        const { sampleItem } = this.state;
        //console.log(this.props)
        return <div onBlur={this.onBlur}>
            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.sample_text} initialValue={sampleItem.sample_text || ""} field="sample_text" lr_subclass={true} updateModel={this.setString} /></div>
            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.tag} initialValue={sampleItem.tag || ""} field="tag" lr_subclass={true} updateModel={this.setString} /></div>
            <Grid container alignItems="center">
                <Grid item xs={10}>
                    <Website key={sampleItem.samples_location || ""} className="wd-100"  {...this.props.formElements.samples_location} website={sampleItem.samples_location || ""} field="samples_location" disable={sampleItem.samples_location.indexOf("syseleven.net") >= 0} updateModel_website={this.setString} />
                </Grid>
                <Grid item xs={2}>
                    <FilePondDocument model={this.props.model} index={"samples_location"} setdocument={this.setDocument} />
                </Grid>
            </Grid>

        </div>
    }

}