import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
import messages from "./../../config/messages"; 

export default class LRIdentifier extends React.Component {

    constructor(props) {
        super(props);
        this.state = { LR_identifier: this.props.default_valueArray || [] }
    }

    setIdentifier = (event, identifierIndex, buttonAction, identifierSelectionObjArray) => {
        const { LR_identifier } = this.state;
        const action = (event && event.target.name) || buttonAction;
        const identifier_obj_2_proccess = LR_identifier[identifierIndex] || {};
        switch (action) {
            case "identifier_value":
                identifier_obj_2_proccess.value = event.target.value;
                LR_identifier[identifierIndex] = { ...identifier_obj_2_proccess };
                this.setState({ LR_identifier })
                return;
            case "identifier_selection":
                identifier_obj_2_proccess[this.props.identifier_scheme] = identifierSelectionObjArray[0].value;
                LR_identifier[identifierIndex] = { ...identifier_obj_2_proccess };
                break;
            case "addIdentifier":
                if (LR_identifier.filter(item => item.value).length !== LR_identifier.length ||
                    LR_identifier.filter(item => item[this.props.identifier_scheme]).length !== LR_identifier.length) {
                    return;//don't add a new identifier, if there are blanks in others identifiers
                }
                LR_identifier.push(JSON.parse(JSON.stringify(this.props.identifier_obj)));
                break;
            case "removeIdentifier":
                LR_identifier.splice(identifierIndex, 1);
                this.setState({ LR_identifier }, this.onBlur)
                return;
            default: break;
        }
        this.setState({ LR_identifier }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_Identifier(this.props.field, this.state.LR_identifier);
    }


    render() {
        const default_valueArray = this.state.LR_identifier;
        this.props.required && default_valueArray.length === 0 && default_valueArray.push(JSON.parse(JSON.stringify(this.props.identifier_obj)));
        return <div className="mb-3 inner--group nested--group" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {default_valueArray === 0 && <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setIdentifier(e, null, "addIdentifier")}>{messages.group_elements_create}</Button>}
                    {default_valueArray.filter(identifier => identifier[this.props.identifier_scheme] !== "http://w3id.org/meta-share/meta-share/elg").length === 0 && <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setIdentifier(e, null, "addIdentifier")}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {
                default_valueArray.map((identifier, identifierIndex) => {
                    const default_value = identifier.value || "";
                    const default_scheme_choice = this.props.scheme_choices.filter(choice => choice.value === identifier[this.props.identifier_scheme]).length > 0 ? this.props.scheme_choices.filter(choice => choice.value === identifier[this.props.identifier_scheme])[0].display_name : "";
                    if (default_scheme_choice === "ELG") {
                        return <div key={identifierIndex}></div>
                    }
                    return <div key={identifierIndex} className="pt-3 pb-3">
                        <Grid container spacing={1} direction="row" justifyContent="space-between" alignItems="baseline" >
                            <Grid item xs={4}>
                                <TextField className="wd-100" required={this.props.identifier_scheme_required} label={this.props.identifier_scheme_label} helperText={this.props.identifier_scheme_help_text} variant="outlined"
                                    disabled={this.props.disable || identifier.hasOwnProperty('pk')}
                                    select
                                    //error={default_scheme_choice === ""}
                                    value={default_scheme_choice}
                                    inputProps={{ name: 'identifier_selection' }}
                                    onChange={(e) => this.setIdentifier(e, identifierIndex, null, this.props.scheme_choices.filter(scheme_choice => scheme_choice.display_name === e.target.value))}
                                >
                                    {
                                        this.props.scheme_choices.filter(option => option.value !== "http://w3id.org/meta-share/meta-share/elg").map((scheme_choice, scheme_choice_index) => (
                                            <MenuItem onBlur={this.onBlur} key={scheme_choice_index} value={scheme_choice.display_name}>
                                                {scheme_choice.display_name}
                                            </MenuItem>
                                        ))
                                    }
                                </TextField>
                            </Grid>

                            <Grid item xs={6}>
                                <TextField className="wd-100" required={this.props.identifier_value_required} label={this.props.identifier_value_label} helperText={this.props.identifier_value_help_text} variant="outlined"
                                    //error={default_value === ""}
                                    placeholder={this.props.identifier_value_placeholder || ""}
                                    disabled={this.props.disable || identifier.hasOwnProperty('pk')}
                                    value={default_value}
                                    inputProps={{ name: 'identifier_value' }}
                                    onChange={(e) => this.setIdentifier(e, identifierIndex)}
                                    onBlur={this.onBlur}
                                /></Grid>

                            <Grid item xs={2} container direction="row" justifyContent="flex-start" alignItems="baseline" >
                                {((default_value !== "" || default_scheme_choice !== "") && ((default_valueArray.length - 1) === identifierIndex)) ?
                                    <Grid item><Button disabled={this.props.disable} onClick={(e) => this.setIdentifier(e, identifierIndex, "addIdentifier")}><AddCircleOutlineIcon className="small-icon" /></Button></Grid>
                                    :
                                    <Grid item><span></span></Grid>
                                }
                                <Grid item><Button disabled={this.props.disable} onClick={(e) => this.setIdentifier(e, identifierIndex, "removeIdentifier")}><RemoveCircleOutlineIcon className="small-icon" /></Button></Grid>
                            </Grid>
                            {this.props.children}
                        </Grid>
                    </div>
                })
            }
        </div>
    }
}