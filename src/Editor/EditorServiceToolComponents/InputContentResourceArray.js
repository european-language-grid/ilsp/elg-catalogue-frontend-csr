import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import messages from "./../../config/messages";
import { inputContentResourceObj } from "../Models/LrModel";
import InputContentResourceItem from "./InputContentResourceItem";
import Tooltip from '@material-ui/core/Tooltip';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

export default class InputContentResourceArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { inputContentResourceArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            inputContentResourceArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, inputContentItemIndex, value) => {
        const { inputContentResourceArray } = this.state;
        switch (action) {
            case "addInputContentResourceItem":
                const filteredArray = inputContentResourceArray.filter(item => item && item.processing_resource_type);
                if (filteredArray.length !== inputContentResourceArray.length) {
                    return;//do not add another element if required element is empty in previous ones
                }
                inputContentResourceArray.push(JSON.parse(JSON.stringify(inputContentResourceObj)));
                break;
            case "removeInputContentResourceItem":
                inputContentResourceArray.splice(inputContentItemIndex, 1);
                break;
            case "setInputContentItem":
                inputContentResourceArray[inputContentItemIndex] = value;
                break;
            default: break;
        }
        this.setState({ inputContentResourceArray }, this.onBlur)
        //this.props.updateModel_array(this.props.field, inputContentResourceArray, this.props.lr_subclass);
    }

    removeWarning = (e, index) => {
        e.stopPropagation();
        /* if (window.confirm(messages.editor_remove_division)) {
             this.setHasDivision(e, divisionIndex, "remove_division");
         }*/
        confirmAlert({
            //title: 'Confirm to remove',
            message: messages.editor_remove_input_content_resource(this.props.field === "output_resource" ? "output resource" : "input content resource", index),
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.setValues("removeInputContentResourceItem", index)
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.inputContentResourceArray, this.props.lr_subclass);
    }

    render() {
        const { inputContentResourceArray } = this.state;

        this.props.field !== "output_resource" && inputContentResourceArray.length === 0 && inputContentResourceArray.push(JSON.parse(JSON.stringify(inputContentResourceObj)));
        return <div className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {(this.props.field === "output_resource" && inputContentResourceArray.length === 0) &&
                    <Grid item sm={1}>
                        <Tooltip title={`${messages.group_elements_create} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addInputContentResourceItem")}>{messages.group_elements_create}</Button></Tooltip>
                    </Grid>
                }
            </Grid>
            {
                inputContentResourceArray.map((inputContentItem, inputContentItemIndex) => {
                    return <div key={inputContentItemIndex} onBlur={() => this.onBlur()}>
                        {(inputContentItem.processing_resource_type && inputContentItem.processing_resource_type !== "") &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, inputContentItemIndex)}>{messages.array_elements_remove}{` ${this.props.label}`} {inputContentItemIndex ? inputContentItemIndex + 1 : ""}</Button></Tooltip></Grid>
                            </Grid>
                        }
                        {(!inputContentItem.processing_resource_type && inputContentItem.processing_resource_type === "") &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, inputContentItemIndex)}>{messages.array_elements_remove}{` ${this.props.label}`} {inputContentItemIndex ? inputContentItemIndex + 1 : ""}</Button></Tooltip></Grid>
                            </Grid>
                        }

                        <InputContentResourceItem {...this.props} inputContentItem={inputContentItem} inputContentItemIndex={inputContentItemIndex} setValues={this.setValues} />

                        <div className="mb2 pt-3">
                            {((inputContentItem.processing_resource_type && inputContentItem.processing_resource_type !== "") && ((inputContentResourceArray.length - 1) === inputContentItemIndex)) ?
                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item>
                                        <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addInputContentResourceItem")}>{messages.array_elements_add}</Button></Tooltip>
                                    </Grid>
                                </Grid>
                                : <span></span>
                            }
                        </div>


                        {/*<div className="mb2 pt-3">
                            <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, inputContentItemIndex)}>{messages.array_elements_remove}{` ${this.props.label}`} {inputContentItemIndex ? inputContentItemIndex + 1 : ""}</Button></Tooltip>
                        </div>*/}
                    </div>
                })
            }
        </div >
    }

}