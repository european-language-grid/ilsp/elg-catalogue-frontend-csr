import React from "react";
import FreeText from "../editorCommonComponents/FreeText";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import Grid from '@material-ui/core/Grid';
//import CurrencyTextField from '@unicef/material-ui-currency-textfield';//removed material-ui-currency-textfield library, replace as it is in cost

export default class PerformanceIndicatorItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { performanceIndicatorItem: props.performanceIndicatorItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            performanceIndicatorItem: nextProps.performanceIndicatorItem
        };
    }

    setString = (field, value) => {
        const { performanceIndicatorItem } = this.state;
        performanceIndicatorItem[field] = value;
        this.setState({ performanceIndicatorItem });
    }

    setSelectListmetric = (event, selectedObjArray) => {
        const { performanceIndicatorItem } = this.state;
        performanceIndicatorItem.metric = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ performanceIndicatorItem });
    }

    onBlur = () => {
        const { performanceIndicatorItem } = this.state;
        this.props.updateModel("setperformanceIndicatorItem", this.props.performanceIndicatorItemIndex, performanceIndicatorItem);
    }

    render() {
        const { performanceIndicatorItem } = this.state;
        return <div onBlur={this.onBlur}>
            <Grid container spacing={1} direction="row" justifyContent="space-between" alignItems="baseline" >
                <Grid item xs={7}> {/*<CurrencyTextField className="wd-100"
                    label={this.props.formElements.measure.label}
                    required={this.props.formElements.measure.required}
                    helperText={this.props.formElements.measure.help_text}
                    variant="outlined"
                    minimumValue="0"
                    value={performanceIndicatorItem.measure}
                    currencySymbol=""
                    outputFormat="number"
                    onChange={(event, value) => { this.setString("measure", value) }}
    />*/}
                </Grid>
                <Grid item xs={5}>
                    <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.metric} choices={this.props.formElements.metric.choices} default_value={performanceIndicatorItem.metric} setSelectedvalue={this.setSelectListmetric} /></div>
                </Grid>
            </Grid>
            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.unit_of_measure_metric} initialValue={performanceIndicatorItem.unit_of_measure_metric || ""} field="unit_of_measure_metric" updateModel={this.setString} /></div>
        </div>
    }

}