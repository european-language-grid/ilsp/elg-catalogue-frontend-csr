import React from "react";
//import Container from '@material-ui/core/Container';
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
//import { ReactComponent as TextIcon } from "./../../assets/elg-icons/office-file-text-graph-alternate.svg";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import FreeTextList from "../editorCommonComponents/FreeTextList";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid'
import FreeText from "../editorCommonComponents/FreeText";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import InputContentResourceArray from "./InputContentResourceArray";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import LRAutocomplete from "../EditorCorpusComponents/LRAutocomplete";
import LRAutocompleteSingle from "../EditorCorpusComponents/LRAutocompleteSingle";
import ParameterArray from "./ParameterArray";
import OnOfSwitch from "../editorCommonComponents/OnOfSwitch";
import { SERVICE_SECOND_SECTION_TABS_HEADERS } from "../../config/editorConstants";
import { switchIcon } from "../../config/editorConstants";


function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}
export default class ServiceToolSecondStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: props.tabInSection || 0 };
    }

    collorElements = () => {
        const error = this.props.yupError;
        const elements2Color = []
        if (error && error.errors) {
            var ids = document.querySelectorAll("*[id]");
            error.errors.forEach((validationError, index) => {
                for (let index = 0; ids && index < ids.length; index++) {
                    const element = ids[index];
                    const element_id = ids[index].id;
                    element.classList.remove("yup-error-dynamic");
                    const parts = validationError.split(">");
                    if (parts && parts.length >= 5 && parts[4].includes(element_id)) {
                        elements2Color.push(element);
                        if (this.props.yupClickError && this.props.yupClickError.includes(element_id)) {
                            //element.scrollIntoView(false);
                            element.scrollIntoView({
                                behavior: 'auto',
                                block: 'center',
                                inline: 'center'
                            });
                        }
                    }
                }
            })
        }
        elements2Color.forEach(element => {
            element.classList.add("yup-error-dynamic");
        })
    }

    componentDidMount() {
        this.collorElements();
    }


    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
        this.props.settabInSection(tabIndex);
    }
    ///
    setObjectGeneralSubclass = (field, valueObj) => {
        console.log(field)
        const { model } = this.props;
        model.described_entity.lr_subclass[field] = valueObj;
        //this.setState({ model });
        this.props.updateModel(model);
    }


    setSingleSelectListGeneric = (event, selectedObjArray, field, lr_subclass) => {
        const { model } = this.props;
        if (selectedObjArray.length === 0) {
            selectedObjArray[0] = { value: null };
        }
        if (lr_subclass) {
            model.described_entity.lr_subclass[field] = selectedObjArray[0].value;
        } else {
            model.described_entity[field] = selectedObjArray[0].value;
        }
        this.props.updateModel(model);
    }

    updateModel_generic_array = (obj2update, valueArray, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = [];
            }
            model.described_entity.lr_subclass[obj2update] = valueArray;
            this.props.updateModel(model);
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = [];
            }
            model.described_entity[obj2update] = valueArray;
            this.props.updateModel(model);
        }
    }

    updateModel_generic_array_Subclass = (obj2update, valueArray) => {
        const { model } = this.props;
        if (!model.described_entity.lr_subclass[obj2update]) {
            model.described_entity.lr_subclass[obj2update] = [];
        }
        model.described_entity.lr_subclass[obj2update] = valueArray;
        this.props.updateModel(model);

    }

    updateModel_generic_String = (obj2update, stringValue, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = "";
            }
            model.described_entity.lr_subclass[obj2update] = stringValue;
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = "";
            }
            model.described_entity[obj2update] = stringValue;
        }
        this.props.updateModel(model);
    }

    updateBoolean = (field, boolean) => {
        const { model } = this.props;
        model.described_entity.lr_subclass[field] = Boolean(boolean);
        this.props.updateModel(model);
    }


    ///

    render() {
        //const data = this.props.schema;
        const subclass = this.props.schema_lr_subclass;
        const { model } = this.props;
        //const TABS_HEADERS = ["categories", "classification", "technical", "evaluation"];     

        return <div>
            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {
                                SERVICE_SECOND_SECTION_TABS_HEADERS.map((tab, index) => {
                                    return <Tab key={index} label={<><span>{switchIcon(tab)}</span><span> {tab} </span>  </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />
                                })
                            }
                        </Tabs>

                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("function", subclass.function).label}>
                                        <AutocompleteRecommendedChoices className="wd-100" {...GenericSchemaParser.getFormElement("function", subclass.function)} recommended_choices={GenericSchemaParser.getFormElement("function", subclass.function).choices} initialValuesArray={model.described_entity.lr_subclass.function || []} field="function" lr_subclass={true} updateModel_array={this.updateModel_generic_array} />
                                    </div>
                                    {false && <div className="pb-3" id={GenericSchemaParser.getFormElement("framework", subclass.framework).label}>
                                        <RecordSelectList className="wd-100" {...GenericSchemaParser.getFormElement("framework", subclass.framework)} choices={GenericSchemaParser.getFormElement("framework", subclass.framework).choices} default_value={model.described_entity.lr_subclass.framework} field="framework" lr_subclass={true} setSelectedvalue={this.setSingleSelectListGeneric} />
                                    </div>}
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("development_framework", subclass.development_framework).label}>
                                        <AutocompleteRecommendedChoices className="wd-100" {...GenericSchemaParser.getFormElement("development_framework", subclass.development_framework)} recommended_choices={GenericSchemaParser.getFormElement("development_framework", subclass.development_framework).choices} initialValuesArray={model.described_entity.lr_subclass.development_framework || []} field="development_framework" lr_subclass={true} updateModel_array={this.updateModel_generic_array} />
                                    </div>

                                    {/*<div className="pb-3"><RecordSelectList className="wd-100" {...GenericSchemaParser.getFormElement("ml_framework1", subclass.ml_framework1)} choices={GenericSchemaParser.getFormElement("ml_framework1", subclass.ml_framework1).choices} default_value={model.described_entity.lr_subclass.ml_framework1} field="ml_framework1" lr_subclass={true} setSelectedvalue={this.setSingleSelectListGeneric} /></div>*/}
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("implementation_language", subclass.implementation_language).label}>
                                        <FreeText className="wd-100" {...GenericSchemaParser.getFormElement("implementation_language", subclass.implementation_language)} initialValue={model.described_entity.lr_subclass.implementation_language} field="implementation_language" lr_subclass={true} updateModel={this.updateModel_generic_String} />
                                    </div>
                                    {false && <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("formalism", subclass.formalism)} defaultValueObj={model.described_entity.lr_subclass.formalism || { "en": "" }} field="formalism" lr_subclass={true} setLanguageSpecificText={this.setObjectGeneralSubclass} /></div>}
                                    {false && <div className="pb-3"><RecordSelectList className="wd-100" {...GenericSchemaParser.getFormElement("method", subclass.method)} choices={GenericSchemaParser.getFormElement("method", subclass.method).choices} default_value={model.described_entity.lr_subclass.method} field="method" lr_subclass={true} setSelectedvalue={this.setSingleSelectListGeneric} /></div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={1} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("language_dependent", subclass.language_dependent).label}>
                                        <OnOfSwitch {...GenericSchemaParser.getFormElement("language_dependent", subclass.language_dependent)} default_value={model.described_entity.lr_subclass.language_dependent} field="language_dependent" updateBoolean={this.updateBoolean} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("input_content_resource", subclass.input_content_resource).label}>
                                        <InputContentResourceArray model={model} {...GenericSchemaParser.getFormElement("input_content_resource", subclass.input_content_resource)} subclass={subclass} initialValueArray={model.described_entity.lr_subclass.input_content_resource || []} field="input_content_resource" lr_subclass={true} updateModel_array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("output_resource", subclass.output_resource).label}>
                                        <InputContentResourceArray model={model} {...GenericSchemaParser.getFormElement("output_resource", subclass.output_resource)} subclass={subclass} initialValueArray={model.described_entity.lr_subclass.output_resource || []} field="output_resource" lr_subclass={true} updateModel_array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("required_hardware", subclass.required_hardware).label}>
                                        <AutocompleteChoicesChips className="wd-100"
                                            {...GenericSchemaParser.getFormElement("required_hardware", subclass.required_hardware)}
                                            initialValuesArray={model.described_entity.lr_subclass.required_hardware || []}
                                            field="required_hardware"
                                            lr_subclass={true}
                                            updateModel_array={this.updateModel_generic_array}
                                        />
                                    </div>
                                    {false && <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("running_environment_details", subclass.running_environment_details)} defaultValueObj={model.described_entity.lr_subclass.running_environment_details || { "en": "" }} field="running_environment_details" lr_subclass={true} setLanguageSpecificText={this.setObjectGeneralSubclass} /></div>}
                                    {false && <div className="pb-3"><FreeTextList className="wd-100" {...GenericSchemaParser.getFormElement("requires_software", subclass.requires_software)} default_value_Array={model.described_entity.lr_subclass.requires_software ? JSON.parse(JSON.stringify(model.described_entity.lr_subclass.requires_software)) : []} field="requires_software" updateModel_Array={this.updateModel_generic_array_Subclass} /></div>}
                                    {false && <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("requires_lr", subclass.requires_lr)} initialValueArray={model.described_entity.lr_subclass.requires_lr || []} field="requires_lr" updateModel_Array={this.updateModel_generic_array_Subclass} /> </div>}
                                    {false && <div className="pb-3"><LRAutocompleteSingle className="wd-100" {...GenericSchemaParser.getFormElement("typesystem", subclass.typesystem)}
                                        label={GenericSchemaParser.getFormElement("typesystem", subclass.typesystem).label}
                                        help_text={GenericSchemaParser.getFormElement("typesystem", subclass.typesystem).help_text}
                                        formStuff={GenericSchemaParser.getFormElement("typesystem", subclass.typesystem).formElements}
                                        initialValue={model.described_entity.lr_subclass.typesystem || {}}
                                        index="typesystem" field="typesystem" updateModel={this.setObjectGeneralSubclass} />{/**notice the index prop instead of field!!! */}
                                    </div>}
                                    {false && <div className="pb-3">
                                        <LRAutocompleteSingle className="wd-100" {...GenericSchemaParser.getFormElement("annotation_schema", subclass.annotation_schema)}
                                            label={GenericSchemaParser.getFormElement("annotation_schema", subclass.annotation_schema).label}
                                            help_text={GenericSchemaParser.getFormElement("annotation_schema", subclass.annotation_schema).help_text}
                                            formStuff={GenericSchemaParser.getFormElement("annotation_schema", subclass.annotation_schema).formElements}
                                            initialValue={model.described_entity.lr_subclass.annotation_schema || {}}
                                            index="annotation_schema" field="annotation_schema" updateModel={this.setObjectGeneralSubclass} /> {/**notice the index prop instead of field!!! */}
                                    </div>}
                                    {false && <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("annotation_resource", subclass.annotation_resource)} initialValueArray={model.described_entity.lr_subclass.annotation_resource || []} field="annotation_resource" updateModel_Array={this.updateModel_generic_array_Subclass} /> </div>}
                                    {true && <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("ml_model", subclass.ml_model)} initialValueArray={model.described_entity.lr_subclass.ml_model || []} field="ml_model" updateModel_Array={this.updateModel_generic_array_Subclass} /> </div>}
                                    {false && <div className="pb-3"><RecordSelectList className="wd-100" {...GenericSchemaParser.getFormElement("previous_annotation_types_policy", subclass.previous_annotation_types_policy)} choices={GenericSchemaParser.getFormElement("previous_annotation_types_policy", subclass.previous_annotation_types_policy).choices} default_value={model.described_entity.lr_subclass.previous_annotation_types_policy} field="previous_annotation_types_policy" lr_subclass={true} setSelectedvalue={this.setSingleSelectListGeneric} /></div>}
                                    {true && <div className="pb-3"><ParameterArray   {...GenericSchemaParser.getFormElement("parameter", subclass.parameter)} initialValueArray={model.described_entity.lr_subclass.parameter || []} subclass={subclass} field="parameter" lr_subclass={true} updateModel_array={this.updateModel_generic_array_Subclass} /></div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={2} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3"><OnOfSwitch {...GenericSchemaParser.getFormElement("evaluated", subclass.evaluated)} default_value={model.described_entity.lr_subclass.evaluated} field="evaluated" updateBoolean={this.updateBoolean} /></div>
                                    <div className="pb-3"><RecordSelectList className="wd-100" {...GenericSchemaParser.getFormElement("trl", subclass.trl)} choices={GenericSchemaParser.getFormElement("trl", subclass.trl).choices} default_value={model.described_entity.lr_subclass.trl} field="trl" lr_subclass={true} setSelectedvalue={this.setSingleSelectListGeneric} /></div>
                                    {false && <div className="pb-3"><FreeText className="wd-100" {...GenericSchemaParser.getFormElement("running_time", subclass.running_time)} initialValue={model.described_entity.lr_subclass.running_time} field="running_time" lr_subclass={true} updateModel={this.updateModel_generic_String} /></div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                    </div>
                </div>
            </form>

        </div >
    }
}