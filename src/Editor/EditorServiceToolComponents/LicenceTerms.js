import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import CreateIcon from '@material-ui/icons/Create';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import TextField from '@material-ui/core/TextField';
//import Chip from '@material-ui/core/Chip';
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LRIdentifier from "./LRIdentifier";
import WebsiteList from "../editorCommonComponents/WebsiteList";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import { licence_terms_obj, licence_identifier_obj } from "../Models/LrModel";
import { LOOK_UP_LICENCE } from "../../config/editorConstants";
import CircularProgress from "@material-ui/core/CircularProgress";
import messages from "../../config/messages";

//const filter = createFilterOptions();
const checkElg = obj => obj.licence_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg";

export default class LicenceTerms extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [], licenceChoices: [], loading: false, source: null }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || []
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    getLicenceChoices = (inputLicence) => {
        if (inputLicence.trim().length <= 1) {
            //this.setState({ licenceChoices: [] });
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        axios.get(LOOK_UP_LICENCE(inputLicence), { cancelToken: source.token })
            .then((res) => {
                this.setState({ licenceChoices: res.data, loading: false, source: null })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false, source: null, licenceChoices: [] })
            });
    }

    setValues = (action, itemIndex, value) => {
        const { initialValueArray } = this.state;
        switch (action) {
            case "addArrayItem":
                const filtered = initialValueArray.filter(item => {
                    if (item.licence_terms_name && item.licence_terms_name["en"]) {
                    } else {
                        return false;
                    }
                    if (item.licence_terms_url) {
                        const filteredUrls = item.licence_terms_url.filter(url => url);
                        if (filteredUrls.length !== item.licence_terms_url.length) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    return true;
                })
                if (filtered.length !== initialValueArray.length) {
                    return;// do not add element if there are empty ones
                }
                initialValueArray.push(JSON.parse(JSON.stringify(licence_terms_obj)));
                break;
            case "removeArrayItem":
                initialValueArray.splice(itemIndex, 1);
                break;
            case "licence_terms_name":
                initialValueArray[itemIndex].licence_terms_name["en"] = value;
                if (initialValueArray[itemIndex].hasOwnProperty('editor-placeholder')) {
                    delete initialValueArray[itemIndex]['editor-placeholder'];
                }
                break;
            case "autoCompleteSelected":
                if (value.similarity) {
                    delete value["similarity"];
                }
                if (value.display_name) {
                    delete value["display_name"];
                }
                if (value.hasOwnProperty('editor-placeholder')) {
                    delete value['editor-placeholder'];
                }
                initialValueArray[itemIndex] = value;
                this.setState({ initialValueArray, licenceChoices: [] });
                this.onBlur();
                return;
            default: break;
        }
        this.setState({ initialValueArray, licenceChoices: [] });
    }

    set_licence_terms_name = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].licence_terms_name = value;
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray, licenceChoices: [] });
    }

    updateModel_generic_array = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].licence_terms_url = value;
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray, licenceChoices: [] });
    }

    update_identifier_Array = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].licence_identifier = value;
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray, licenceChoices: [] });
    }

    update_condition_of_use_array = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].condition_of_use = value;
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray, licenceChoices: [] });
    }

    onBlur = () => {
        const mappedArray = this.state.initialValueArray.map(item => {
            const exists_licence_terms_name = (item.licence_terms_name && item.licence_terms_name.hasOwnProperty("en") && item.licence_terms_name["en"]) ? true : false;
            const exists_licence_terms_url = (item.licence_terms_url && item.licence_terms_url.filter(url => url).length > 0) ? true : false;
            const exists_condition_of_use = (item.condition_of_use && item.condition_of_use.filter(condition => condition).length > 0) ? true : false;
            if (exists_licence_terms_name === false && exists_licence_terms_url === false && exists_condition_of_use === false) {
                item["editor-placeholder"] = true;
            } else {
                delete item['editor-placeholder'];
            }
            return item;
        })
        this.setState({ licenceChoices: [], loading: false });
        this.props.updateModel_Array(this.props.field, mappedArray);
    }

    autocompleteTextField = (licence, licenceIndex) => {
        const activeLanguage = this.props.activeLanguage || "en";
        const choices = this.props.formElements.licence_identifier.formElements.licence_identifier_scheme.choices;
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.loading}
                value={licence || null}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValues("autoCompleteSelected", licenceIndex, newValue);
                    } else {
                        this.setValues("licence_terms_name", licenceIndex, newValue);
                    }
                }}
                options={this.state.loading ? [] : this.state.licenceChoices}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                licence_terms_name: { 'en': params.inputValue },
                                display_name: `Missing ${this.props.label}? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                licence_terms_name: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.licence_terms_name ? Object.values(option.licence_terms_name)[0] : ""}
                getOptionLabel={(option) => option.licence_terms_name ? Object.keys(option.licence_terms_name).includes("en") ? option.licence_terms_name["en"] : Object.values(option.licence_terms_name)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} required={this.props.required} label={this.props.formElements.licence_terms_name.label} help_text={this.props.formElements.licence_terms_name.help_text} variant="outlined"
                        onChange={(e) => { this.getLicenceChoices(e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option.licence_terms_name)[0], inputValue);
                    //const parts = parse(Object.values(option.licence_terms_name)[0], matches);
                    const matches = match(option.licence_terms_name[activeLanguage] || Object.values(option.licence_terms_name)[0], inputValue);
                    const parts = parse(option.licence_terms_name[activeLanguage] || Object.values(option.licence_terms_name)[0], matches);
                    return (
                        <div>
                            {
                                option.licence_terms_name && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                            {
                                option.licence_terms_url && option.licence_terms_url.map((url, urlIndex) => {
                                    return <div key={urlIndex} className="info_url">
                                        {url}
                                    </div>
                                })
                            }
                            {/*
                                option.condition_of_use && option.condition_of_use.length > 0 && <div>
                                    <small>Condition of use:</small>
                                    {option.condition_of_use.map((condition, conditionIndex) => {
                                        let foundValue = "";
                                        return <span key={conditionIndex}>
                                            {
                                                this.props.formElements.condition_of_use.choices.filter(choice => { if (choice.value === condition) { foundValue = choice.display_name; return true; } else { return false; } }).length > 0 ?
                                                    <Chip label={foundValue} size="small" />
                                                    : ""
                                            }
                                        </span>
                                    })
                                    }
                                </div>
                                */}

                            {
                                option.licence_identifier && option.licence_identifier.map((identifier, identifierIndex) => {
                                    return <div key={identifierIndex}>
                                        {
                                            identifier.licence_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                                (`${identifier.value} - ${choices.filter(licenceChoiceUrl => licenceChoiceUrl.value === identifier.licence_identifier_scheme)[0].display_name}`)
                                        }
                                    </div>
                                })
                            }
                        </div>
                    );
                }
                }
            />
            < div className="mb2 mt1" >
                {/*< Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", licenceIndex)}> {messages.array_elements_add} </Button >*/}
                {licenceIndex >= 1 && <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                    <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", licenceIndex)}>{messages.array_elements_remove} </Button>}</Grid>
                </Grid>
                }
            </div >
        </span >
    }


    render() {
        const { initialValueArray } = this.state;
        initialValueArray.length === 0 && initialValueArray.push(JSON.parse(JSON.stringify(licence_terms_obj)));
        return <div className="mb2" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="flex-start">
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>

                <Grid item sm={1}>
                    {this.state.initialValueArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}>{messages.array_elements_add}</Button>}
                </Grid>

            </Grid>
            {
                initialValueArray.map((arrayItem, arrayItemIndex) => {
                    return <div className="mb2" key={arrayItemIndex} onBlur={this.onBlur}>
                        <div>
                            {!arrayItem.licence_terms_name || !arrayItem.licence_terms_name["en"] ?
                                this.autocompleteTextField(arrayItem, arrayItemIndex)
                                :
                                <div>
                                    <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                        <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", arrayItemIndex)}>{messages.array_elements_remove}</Button>}</Grid>
                                    </Grid>

                                    <LanguageSpecificText
                                        {...this.props.formElements.licence_terms_name}
                                        defaultValueObj={arrayItem.licence_terms_name || { "en": "" }}
                                        disable={arrayItem.pk >= 0 ? true : false}
                                        help_text={arrayItem.pk >= 0 ? '' : this.props.formElements.licence_terms_name.help_text}
                                        field={arrayItemIndex}//don't care about field, track index instead
                                        setLanguageSpecificText={this.set_licence_terms_name}
                                    />
                                    <WebsiteList
                                        {...this.props.formElements.licence_terms_url}
                                        help_text={arrayItem.pk >= 0 ? '' : this.props.formElements.licence_terms_url.help_text}
                                        className="wd-100"
                                        default_value_Array={arrayItem.licence_terms_url || []}
                                        field={arrayItemIndex}//don't care about field, track index instead
                                        updateModel_website={this.updateModel_generic_array}
                                        disable={arrayItem.pk >= 0 ? true : false}
                                    />

                                    <div className="mt-4">
                                        <AutocompleteChoicesChips
                                            className="wd-100"
                                            {...this.props.formElements.condition_of_use}
                                            initialValuesArray={arrayItem.condition_of_use || []}
                                            field={arrayItemIndex}
                                            lr_subclass={true}
                                            disabled={arrayItem.pk >= 0 ? true : false}
                                            updateModel_array={this.update_condition_of_use_array}
                                        />
                                    </div>

                                    {arrayItem.pk && ((arrayItem.licence_identifier.length === 1 && arrayItem.licence_identifier.some(checkElg)))
                                        ?
                                        (<></>)
                                        :
                                        (
                                            <LRIdentifier
                                                key={JSON.stringify(arrayItem.licence_identifier) + '_DistributionLicence_' + arrayItemIndex + '_' + this.props.index}
                                                {...this.props.formElements.licence_identifier}
                                                identifier_scheme="licence_identifier_scheme"
                                                scheme_choices={this.props.formElements.licence_identifier.formElements.licence_identifier_scheme.choices}
                                                identifier_scheme_required={this.props.formElements.licence_identifier.formElements.licence_identifier_scheme.required}
                                                identifier_scheme_label={this.props.formElements.licence_identifier.formElements.licence_identifier_scheme.label}
                                                identifier_scheme_help_text={arrayItem.pk >= 0 ? '' : this.props.formElements.licence_identifier.formElements.licence_identifier_scheme.help_text}
                                                identifier_value_required={this.props.formElements.licence_identifier.formElements.value.required}
                                                identifier_value_label={this.props.formElements.licence_identifier.formElements.value.label}
                                                identifier_value_help_text={arrayItem.pk >= 0 ? '' : this.props.formElements.licence_identifier.formElements.value.help_text}
                                                identifier_value_placeholder={arrayItem.pk >= 0 ? '' : this.props.formElements.licence_identifier.formElements.value.placeholder}
                                                identifier_obj={JSON.parse(JSON.stringify(licence_identifier_obj))}
                                                className="wd-100"
                                                default_valueArray={arrayItem.licence_identifier}
                                                disable={arrayItem.pk >= 0 ? true : false}
                                                field={arrayItemIndex}////don't care about field, track index instead
                                                updateModel_Identifier={this.update_identifier_Array}
                                            />
                                        )
                                    }

                                    <div className="pt-3 form-block-area">
                                        {((arrayItem.licence_terms_name !== "") && ((this.state.initialValueArray.length - 1) === arrayItemIndex)) ?
                                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", arrayItemIndex)}>{messages.array_elements_add} </Button></Grid>
                                            </Grid>
                                            : <div style={{ paddingBottom: "1em" }}></div>
                                        }
                                    </div>

                                </div>
                            }
                        </div>
                    </div>
                })
            }
        </div>
    }
}