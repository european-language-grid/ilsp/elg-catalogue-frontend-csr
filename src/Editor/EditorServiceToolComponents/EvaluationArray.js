import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { evaluationObj } from "../Models/LrModel";
import EvaluationItem from "./EvaluationItem";

export default class EvaluationArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { evaluationArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            evaluationArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, evaluationItemIndex, value) => {
        const { evaluationArray } = this.state;
        switch (action) {
            case "addEvaluationItem":
                evaluationArray.push(JSON.parse(JSON.stringify(evaluationObj)));
                break;
            case "removeEvaluationItem":
                evaluationArray.splice(evaluationItemIndex, 1);
                break;
            case "setEvaluationItem":
                evaluationArray[evaluationItemIndex] = value;
                this.props.updateModel_array(this.props.field, evaluationArray, this.props.lr_subclass);
                break;
            default: break;
        }
        this.setState({ evaluationArray })
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.evaluationArray, this.props.lr_subclass);
    }

    render() {
        const { evaluationArray } = this.state;
        //evaluationArray.length === 0 && evaluationArray.push(JSON.parse(JSON.stringify(evaluationObj)));
        return <div onBlur={this.onBlur} className="border-bottom">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addEvaluationItem")}><AddCircleOutlineIcon /></Button>
                </Grid>
            </Grid>
            {
                evaluationArray.map((evaluationItem, evaluationItemIndex) => {
                    return <div key={evaluationItemIndex} onBlur={() => this.onBlur()}>
                        <EvaluationItem
                            // key={JSON.stringify(evaluationItem) + "_distribution_item_" + evaluationItemIndex}
                            {...this.props} evaluationItem={evaluationItem} evaluationItemIndex={evaluationItemIndex} setValues={this.setValues} />
                        <div className="mb2 pt-3 fr">
                            <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("removeEvaluationItem", evaluationItemIndex)}><RemoveCircleOutlineIcon />{`${this.props.label} `}{evaluationItemIndex ? evaluationItemIndex + 1 : void 0}</Button>
                        </div>
                    </div>
                })
            }
        </div>
    }
}