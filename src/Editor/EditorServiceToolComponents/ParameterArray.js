import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import { parameterObj } from "../Models/ToolModel";
import ParameterItem from "./ParameterItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class ParameterArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { parameterArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            parameterArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, parameterItemIndex, value) => {
        const { parameterArray } = this.state;
        switch (action) {
            case "addparameterItem":
                const filtered = parameterArray.filter(item => {
                    return item && item.parameter_name && item.parameter_label && item.parameter_description && item.parameter_type;
                })
                if (filtered.length !== parameterArray.length) {
                    return; //do not add element if there are empty ones
                }
                parameterArray.push(JSON.parse(JSON.stringify(parameterObj)));
                break;
            case "removeparameterItem":
                parameterArray.splice(parameterItemIndex, 1);
                break;
            case "setparameterItem":
                parameterArray[parameterItemIndex] = value;
                this.props.updateModel_array(this.props.field, parameterArray);
                break;
            default: break;
        }
        this.setState({ parameterArray })
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.parameterArray);
    }

    render() {
        const { parameterArray } = this.state;
        //console.log(this.props)
        //parameterArray.length === 0 && parameterArray.push(JSON.parse(JSON.stringify(parameterObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addparameterItem")}><AddCircleOutlineIcon className="small-icon"/></Button>      
                </Grid>*/}
                {(parameterArray.length === 0) &&
                    <Grid item sm={1}>
                        <Tooltip title={`${messages.group_elements_create} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addparameterItem")}>{messages.group_elements_create}</Button></Tooltip>
                    </Grid>
                }
            </Grid>
            {
                parameterArray.map((parameterItem, parameterItemIndex) => {
                    return <div key={parameterItemIndex} onBlur={() => this.onBlur()}>
                        <ParameterItem  {...this.props} parameterItem={parameterItem} parameterItemIndex={parameterItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            {parameterItem.parameter_name && <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeparameterItem", parameterItemIndex)}>{messages.array_elements_remove}</Button></Tooltip>}
                            {parameterItem.parameter_name && <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addparameterItem")}>{messages.array_elements_add}</Button></Tooltip>}
                        </div>
                        <div style={{ marginTop: "5em" }} /></div>
                })
            }
        </div>
    }
}