import React from "react";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import IsDescribedByAutocomplete from "./IsDescribedByAutocomplete";
import LRAutocomplete from "../EditorCorpusComponents/LRAutocomplete";
import ActorTypeAutocomplete from "../editorCommonComponents/ActorTypeAutocomplete";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import Website from "../editorCommonComponents/Website";
import PerformanceIndicatorArray from "./PerformanceIndicatorArray";

export default class EvaluationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { evaluationItem: props.evaluationItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            evaluationItem: nextProps.evaluationItem
        };
    }

    setString = (field, value) => {
        const { evaluationItem } = this.state;
        evaluationItem[field] = value;
        this.setState({ evaluationItem }, this.onBlur);
    }

    setObjectGeneral = (field, valueObj) => {
        const { evaluationItem } = this.state;
        evaluationItem[field] = valueObj;
        this.setState({ evaluationItem }, this.onBlur);
    }

    updateModel__array = (obj2update, valueArray) => {
        const { evaluationItem } = this.state;
        if (!evaluationItem[obj2update]) {
            evaluationItem[obj2update] = [];
        }
        evaluationItem[obj2update] = valueArray;
        this.setState({ evaluationItem }, this.onBlur());
    }

    onBlur = () => {
        const { evaluationItem } = this.state;
        const exists_evaluation_level = evaluationItem.evaluation_level && evaluationItem.evaluation_level.length ? true : false;
        const exists_evaluation_type = evaluationItem.evaluation_type && evaluationItem.evaluation_type.length ? true : false;
        const exists_evaluation_criterion = evaluationItem.evaluation_criterion && evaluationItem.evaluation_criterion.length ? true : false;
        const exists_evaluation_measure = evaluationItem.evaluation_measure && evaluationItem.evaluation_measure.length ? true : false;
        const exist_evaluation_details = evaluationItem && evaluationItem.evaluation_details && evaluationItem.evaluation_details.hasOwnProperty("en") && evaluationItem.evaluation_details["en"] ? true : false;
        const exist_evaluation_report = evaluationItem && evaluationItem.evaluation_report.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exist_is_evaluated_by = evaluationItem && evaluationItem.is_evaluated_by.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exist_gold_standard_location = evaluationItem && evaluationItem.gold_standard_location ? true : false;
        const exist_evaluator = (evaluationItem.evaluator && evaluationItem.evaluator.filter(item => item.actor_type !== "actor_type" && !item.hasOwnProperty("editor-placeholder")).length) ? true : false;
        const exist_performance_indicator = evaluationItem && evaluationItem.performance_indicator.filter(item => ((item.measure && item.measure !== null && item.measure >= 0) || (item.metric && item.metric.length) || (item.unit_of_measure_metric && item.unit_of_measure_metric.length))).length ? true : false;
        //const exist_performance_indicator = evaluationItem && evaluationItem.performance_indicator.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        //console.log(exists_evaluation_level, exists_evaluation_type, exists_evaluation_criterion, exists_evaluation_measure, exist_evaluation_details, exist_evaluation_report, exist_is_evaluated_by, exist_gold_standard_location, exist_evaluator, exist_performance_indicator)
        if (exists_evaluation_level === false && exists_evaluation_type === false && exists_evaluation_criterion === false && exists_evaluation_measure === false && exist_evaluation_details === false && exist_evaluation_report === false && exist_is_evaluated_by === false && exist_gold_standard_location === false && exist_evaluator === false && exist_performance_indicator === false) {
            evaluationItem["editor-placeholder"] = true;
        } else {
            delete evaluationItem["editor-placeholder"];
        }
        this.props.setValues("setEvaluationItem", this.props.evaluationItemIndex, evaluationItem);
    }

    render() {
        const { evaluationItem } = this.state;
        return <div onBlur={this.onBlur}>
            {false && <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.evaluation_level} initialValuesArray={evaluationItem.evaluation_level || []} field="evaluation_level" updateModel_array={this.updateModel__array} /></div>}
            {false && <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.evaluation_type} initialValuesArray={evaluationItem.evaluation_type || []} field="evaluation_type" updateModel_array={this.updateModel__array} /></div>}
            {false && <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.evaluation_criterion} initialValuesArray={evaluationItem.evaluation_criterion || []} field="evaluation_criterion" updateModel_array={this.updateModel__array} /></div>}
            {false && <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.evaluation_measure} initialValuesArray={evaluationItem.evaluation_measure || []} field="evaluation_measure" updateModel_array={this.updateModel__array} /></div>}
            {false && <div className="pt-3 pb-3"><Website key={evaluationItem.gold_standard_location + "_gold_standard_location_" + this.props.evaluationItemIndex} className="wd-100"  {...this.props.formElements.gold_standard_location} website={evaluationItem.gold_standard_location || ""} field="gold_standard_location" updateModel_website={this.setString} /></div>}
            {false && <div className="pt-3 pb-3"><PerformanceIndicatorArray {...this.props.formElements.performance_indicator} initialValueArray={evaluationItem.performance_indicator || []} field="performance_indicator" updateModel_array={this.updateModel__array} /></div>}
            {false && <div className="pt-3 pb-3">
                <IsDescribedByAutocomplete
                    className="wd-70" {...this.props} index={this.props.softwareDistributionItemIndex}
                    {...this.props.formElements.evaluation_report} initialValueArray={evaluationItem.evaluation_report || []}
                    field="evaluation_report"
                    updateModel_Array={this.updateModel__array}
                />
            </div>}
            {false && <div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.is_evaluated_by} initialValueArray={evaluationItem.is_evaluated_by || []} field="is_evaluated_by" updateModel_Array={this.setObjectGeneral} /> </div>}
            {false && <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.evaluation_details} defaultValueObj={evaluationItem.evaluation_details || { "en": "" }} field="evaluation_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}
            {false && <div className="pt-3 pb-3"><ActorTypeAutocomplete  {...this.props.formElements.evaluator} initialValueArray={evaluationItem.evaluator || []} field="evaluator" updateModel_Array={this.setObjectGeneral} /></div>}
        </div>
    }
}