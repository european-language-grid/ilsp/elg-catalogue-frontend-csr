import React from "react";
//import Container from '@material-ui/core/Container';
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
//import { ReactComponent as TextIcon } from "./../../assets/elg-icons/office-file-text-graph-alternate.svg";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import FreeTextList from "../editorCommonComponents/FreeTextList";
import WebsiteList from "../editorCommonComponents/WebsiteList";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LanguageSpecificTextRichEditor from "../editorCommonComponents/LanguageSpecificTextRichEditor";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid'
import DateComponent from "../editorCommonComponents/DateComponent";
import FreeText from "../editorCommonComponents/FreeText";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import LRIdentifier from "./LRIdentifier";
import { lr_identifier } from "../Models/LrModel";
import Logo from "../editorCommonComponents/Logo";
import ActorTypeAutocomplete from "../editorCommonComponents/ActorTypeAutocomplete";
import ProjectAutocomplete from "../editorCommonComponents/ProjectAutocomplete";
import LRAutocomplete from "../EditorCorpusComponents/LRAutocomplete";
import DomainAutocomplete from "../editorCommonComponents/DomainAutocomplete";
import RecordSubjectAutocomplete from "../editorCommonComponents/RecordSubjectAutocomplete";
import projectSchemaParser from "./../../parsers/projectSchemaParser";//reuse from project in order to parse subject
import AdditionalInfoArray from "../editorCommonComponents/AdditionalInfoArray";
import IsDescribedByAutocomplete from "./IsDescribedByAutocomplete";
import RelationArray from "../EditorCorpusComponents/RelationArray";
import LRAutocompleteSingle from "../EditorCorpusComponents/LRAutocompleteSingle";
import ActualUseArray from "../EditorCorpusComponents/ActualUseArray";
import ValidationArray from "../EditorCorpusComponents/ValidationArray";
import OnOfSwitch from "../editorCommonComponents/OnOfSwitch";
import { SERVICE_FIRST_SECTION_TABS_HEADERS } from "../../config/editorConstants";
import SourceOfMetadataRecord from "../editorCommonComponents/SourceOfMetadataRecord";
import { switchIcon } from "../../config/editorConstants";
import LanguageSpecificTextListLookup from "../editorCommonComponents/LanguageSpecificTextListLookup";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}
export default class ServiceToolFirstStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: props.tabInSection || 0 };
    }

    collorElements = () => {
        const error = this.props.yupError;
        const elements2Color = []
        if (error && error.errors) {
            var ids = document.querySelectorAll("*[id]");
            error.errors.forEach((validationError, index) => {
                for (let index = 0; ids && index < ids.length; index++) {
                    const element = ids[index];
                    const element_id = ids[index].id;
                    element.classList.remove("yup-error-dynamic");
                    const parts = validationError.split(">");
                    if (parts && parts.length >= 5 && parts[4].includes(element_id)) {
                        elements2Color.push(element);
                        if (this.props.yupClickError && this.props.yupClickError.includes(element_id)) {
                            //element.scrollIntoView(false);
                            element.scrollIntoView({
                                behavior: 'auto',
                                block: 'center',
                                inline: 'center'
                            });
                        }
                    }
                }
            })
        }
        elements2Color.forEach(element => {
            element.classList.add("yup-error-dynamic");
        })
    }

    componentDidMount() {
        this.collorElements();
    }


    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
        this.props.settabInSection(tabIndex);
    }

    setObjectGeneral = (field, valueObj) => {
        const { model } = this.props;
        model.described_entity[field] = valueObj;
        //this.setState({ model });
        this.props.updateModel(model);
    }

    setSingleSelectListGeneric = (event, selectedObjArray, field, lr_subclass) => {
        const { model } = this.props;
        if (selectedObjArray.length === 0) {
            selectedObjArray[0] = { value: null };
        }
        if (lr_subclass) {
            model.described_entity.lr_subclass[field] = selectedObjArray[0].value;
        } else {
            model.described_entity[field] = selectedObjArray[0].value;
        }
        this.props.updateModel(model);
    }

    updateModel_generic_array = (obj2update, valueArray, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = [];
            }
            model.described_entity.lr_subclass[obj2update] = valueArray;
            this.props.updateModel(model);
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = [];
            }
            model.described_entity[obj2update] = valueArray;
            this.props.updateModel(model);
        }
    }

    updateModel_generic_String = (obj2update, stringValue, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = "";
            }
            model.described_entity.lr_subclass[obj2update] = stringValue;
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = "";
            }
            model.described_entity[obj2update] = stringValue;
        }
        this.props.updateModel(model);
    }

    updateBoolean = (field, boolean) => {
        const { model } = this.props;
        model.described_entity[field] = Boolean(boolean);
        this.props.updateModel(model);
    }

    setModelField = (key, value) => {
        const { model } = this.props;
        model[key] = value;
        this.props.updateModel(model);
    }

    onDescriptionChange = () => {
        this.props.onDescriptionChange();
    }


    render() {
        const data = this.props.schema;
        const { model } = this.props;
        //const TABS_HEADERS = ["Identification/ Administrative", "Classification/ Area of activities", "Contact", "Documentation", "Related LRs", "Usage"];


        return <div>

            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {
                                SERVICE_FIRST_SECTION_TABS_HEADERS.map((tab, index) => {
                                    return <Tab key={index} label={<><span>{switchIcon(tab)}</span><span> {tab} </span>  </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />
                                })
                            }
                        </Tabs>

                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("resource_name", data.resource_name).label}>
                                        <LanguageSpecificText {...GenericSchemaParser.getFormElement("resource_name", data.resource_name)} defaultValueObj={model.described_entity.resource_name || { "en": "" }} field="resource_name" setLanguageSpecificText={this.setObjectGeneral} />
                                    </div>
                                    <div id={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).label}>
                                        <LRIdentifier
                                            key={JSON.stringify(model.described_entity.lr_identifier) + 'model.described_entity_lr_identifier' || "model.described_entity_lr_identifier"}
                                            {...GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier)}
                                            identifier_scheme="lr_identifier_scheme"
                                            scheme_choices={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.choices}
                                            identifier_scheme_required={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.required}
                                            identifier_scheme_label={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.label}
                                            identifier_scheme_help_text={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.help_text}
                                            identifier_value_required={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.value.required}
                                            identifier_value_label={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.value.label}
                                            identifier_value_placeholder={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.value.placeholder}
                                            identifier_value_help_text={GenericSchemaParser.getFormElement("lr_identifier", data.lr_identifier).formElements.value.help_text}
                                            identifier_obj={JSON.parse(JSON.stringify(lr_identifier))}
                                            className="wd-100"
                                            default_valueArray={model.described_entity.lr_identifier}
                                            field="lr_identifier"
                                            updateModel_Identifier={this.updateModel_generic_array}
                                        />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("resource_short_name", data.resource_short_name).label}>
                                        <LanguageSpecificText {...GenericSchemaParser.getFormElement("resource_short_name", data.resource_short_name)} defaultValueObj={model.described_entity.resource_short_name || { "en": "" }} field="resource_short_name" setLanguageSpecificText={this.setObjectGeneral} />
                                    </div>
                                    {/*<div className="pb-3"><LanguageSpecificText  {...GenericSchemaParser.getFormElement("description", data.description)} defaultValueObj={model.described_entity.description || { "en": "" }} field="description" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>*/}
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("description", data.description).label}><LanguageSpecificTextRichEditor onDescriptionChange={this.onDescriptionChange} {...GenericSchemaParser.getFormElement("description", data.description)} defaultValueObj={model.described_entity.description || { "en": "" }} field="description" multiline={true} rowsMax={6} setLanguageSpecificText={this.setObjectGeneral} /></div>

                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("version", data.version).label}>
                                        <FreeText className="wd-100" {...GenericSchemaParser.getFormElement("version", data.version)} initialValue={model.described_entity.version || ""} field="version" updateModel={this.updateModel_generic_String} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("version_date", data.version_date).label}>
                                        <DateComponent className="wd-100" {...GenericSchemaParser.getFormElement("version_date", data.version_date)} initialValue={model.described_entity.version_date || null} field="version_date" updateModel={this.updateModel_generic_String} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("resource_provider", data.resource_provider).label}>
                                        <ActorTypeAutocomplete  {...GenericSchemaParser.getFormElement("resource_provider", data.resource_provider)} initialValueArray={model.described_entity.resource_provider || []} field="resource_provider" updateModel_Array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3"><SourceOfMetadataRecord initialValue={model.source_of_metadata_record} field="source_of_metadata_record" setModelField={this.setModelField} /></div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("resource_creator", data.resource_creator).label}>
                                        <ActorTypeAutocomplete  {...GenericSchemaParser.getFormElement("resource_creator", data.resource_creator)} initialValueArray={model.described_entity.resource_creator || []} field="resource_creator" updateModel_Array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("publication_date", data.publication_date).label}>
                                        <DateComponent className="wd-100" {...GenericSchemaParser.getFormElement("publication_date", data.publication_date)} initialValue={model.described_entity.publication_date || null} field="publication_date" updateModel={this.updateModel_generic_String} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("funding_project", data.funding_project).label}>
                                        <ProjectAutocomplete {...GenericSchemaParser.getFormElement("funding_project", data.funding_project)} initialValueArray={model.described_entity.funding_project || []} field="funding_project" updateModel_Array={this.updateModel_generic_array} />
                                    </div>
                                    <div id={GenericSchemaParser.getFormElement("logo", data.logo).label.toLowerCase()}>
                                        <Logo className="wd-100" key={"service_tool_described_entity_logo" + model.described_entity.logo} {...GenericSchemaParser.getFormElement("logo", data.logo)} logo={model.described_entity.logo || ""} field="logo" keycloak={this.props.keycloak} model={model} updateModel_logo={this.setObjectGeneral} />
                                    </div>
                                    {false && <div className="pb-3"><DateComponent className="wd-100" {...GenericSchemaParser.getFormElement("creation_start_date", data.creation_start_date)} initialValue={model.described_entity.creation_start_date || null} maxDate={model.described_entity.creation_end_date || null} field="creation_start_date" updateModel={this.updateModel_generic_String} /></div>}
                                    {false && <div className="pb-3"><DateComponent className="wd-100" {...GenericSchemaParser.getFormElement("creation_end_date", data.creation_end_date)} initialValue={model.described_entity.creation_end_date || null} minDate={model.described_entity.creation_start_date || null} field="creation_end_date" updateModel={this.updateModel_generic_String} /></div>}
                                    {false && <div className="pb-3"><RecordSelectList className="wd-100" {...GenericSchemaParser.getFormElement("creation_mode", data.creation_mode)} choices={GenericSchemaParser.getFormElement("creation_mode", data.creation_mode).choices} default_value={model.described_entity.creation_mode} field="creation_mode" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>}
                                    {false && <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("creation_details", data.creation_details)} defaultValueObj={model.described_entity.creation_details || { "en": "" }} field="creation_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}
                                    {false && <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("has_original_source", data.has_original_source)} initialValueArray={model.described_entity.has_original_source || []} field="has_original_source" updateModel_Array={this.updateModel_generic_array} /> </div>}
                                    {false && <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("original_source_description", data.original_source_description)} defaultValueObj={model.described_entity.original_source_description || { "en": "" }} multiline={true} maxRows={6} field="original_source_description" setLanguageSpecificText={this.setObjectGeneral} /></div>}
                                    {false && <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("is_created_by", data.is_created_by)} initialValueArray={model.described_entity.is_created_by || []} field="is_created_by" updateModel_Array={this.updateModel_generic_array} /> </div>}
                                    {false && <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("update_frequency", data.update_frequency)} defaultValueObj={model.described_entity.update_frequency || { "en": "" }} field="update_frequency" setLanguageSpecificText={this.setObjectGeneral} /></div>}
                                    {false && <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("revision", data.revision)} defaultValueObj={model.described_entity.revision || { "en": "" }} field="revision" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}
                                    {/*read only*/}
                                    {false && <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("citation_text", data.citation_text)} defaultValueObj={model.described_entity.citation_text || { "en": "" }} field="citation_text" setLanguageSpecificText={this.setObjectGeneral} /></div>}
                                    {false && <div><ActorTypeAutocomplete  {...GenericSchemaParser.getFormElement("ipr_holder", data.ipr_holder)} initialValueArray={model.described_entity.ipr_holder || []} field="ipr_holder" updateModel_Array={this.updateModel_generic_array} /></div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={1} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("intended_application", data.intended_application).label}>
                                        <AutocompleteRecommendedChoices className="wd-100" {...GenericSchemaParser.getFormElement("intended_application", data.intended_application)} recommended_choices={GenericSchemaParser.getFormElement("intended_application", data.intended_application).choices} initialValuesArray={model.described_entity.intended_application || []} field="intended_application" updateModel_array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pt-3 pb-3">
                                        <AutocompleteChoicesChips
                                            className="wd-100"
                                            {...GenericSchemaParser.getFormElement("complies_with", data.complies_with)}
                                            //recommended_choices={GenericSchemaParser.getFormElement("complies_with", data.complies_with).choices}
                                            initialValuesArray={model.described_entity.complies_with || []}
                                            field="complies_with"
                                            updateModel_array={this.updateModel_generic_array}
                                        />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("domain", data.domain).label}>
                                        <DomainAutocomplete {...GenericSchemaParser.getFormElement("domain", data.domain)} default_valueArray={model.described_entity.domain || []} updateModel_Domain={this.updateModel_generic_array}
                                            category_help_text={GenericSchemaParser.getFormElement("domain", data.domain).formElements.category_label.help_text}
                                            category_label={GenericSchemaParser.getFormElement("domain", data.domain).formElements.category_label.label}
                                            category_required={GenericSchemaParser.getFormElement("domain", data.domain).formElements.category_label.required}
                                            category_type={GenericSchemaParser.getFormElement("domain", data.domain).formElements.category_label.type}
                                            domain_choices={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.domain_classification_scheme.choices}
                                            domain_help_text={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.domain_classification_scheme.help_text}
                                            domain_label={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.domain_classification_scheme.label}
                                            domain_read_only={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.domain_classification_scheme.read_only}
                                            domain_required={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.domain_classification_scheme.required}
                                            value_label={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.value.label}
                                            value_required={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.required}
                                            domain_identifier_label={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.domain_classification_scheme.label}
                                            domain_identifier_help_text={GenericSchemaParser.getFormElement("domain", data.domain).formElements.domain_identifier.formElements.domain_classification_scheme.help_text}
                                        />
                                    </div>
                                    {/*<div><LanguageSpecificTextList  {...GenericSchemaParser.getFormElement("keyword", data.keyword)} default_valueArray={model.described_entity.keyword || []} field="keyword" setLanguageSpecifictextList={this.updateModel_generic_array} /></div>*/}
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("keyword", data.keyword).label}>
                                        <LanguageSpecificTextListLookup  {...GenericSchemaParser.getFormElement("keyword", data.keyword)} default_valueArray={model.described_entity.keyword || []} field="keyword" field_type="keyword" entity_type="lr" setLanguageSpecifictextList={this.updateModel_generic_array} />
                                    </div>
                                    {false && <div><RecordSubjectAutocomplete   {...projectSchemaParser.getSubjectKeywords(data)} default_valueArray={model.described_entity.subject || []} updateModel_Subject={this.updateModel_generic_array} /></div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={2} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("additional_info", data.additional_info).label}>
                                        <AdditionalInfoArray {...GenericSchemaParser.getFormElement("additional_info", data.additional_info)} field="additional_info" initialValuesArray={model.described_entity.additional_info || []} updateModel={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("contact", data.contact).label}>
                                        <ActorTypeAutocomplete {...GenericSchemaParser.getFormElement("contact", data.contact)} initialValueArray={model.described_entity.contact || []} field="contact" updateModel_Array={this.updateModel_generic_array} />
                                    </div>
                                    {false && <div className="pb-3"><FreeTextList className="wd-100" {...GenericSchemaParser.getFormElement("mailing_list_name", data.mailing_list_name)} default_value_Array={model.described_entity.mailing_list_name ? JSON.parse(JSON.stringify(model.described_entity.mailing_list_name)) : []} field="mailing_list_name" updateModel_Array={this.updateModel_generic_array} /></div>}
                                    {false && <div className="pb-3"><WebsiteList className="wd-100" {...GenericSchemaParser.getFormElement("discussion_url", data.discussion_url)} default_value_Array={model.described_entity.discussion_url ? JSON.parse(JSON.stringify(model.described_entity.discussion_url)) : []} field="discussion_url" updateModel_website={this.updateModel_generic_array} /></div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={3} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("is_documented_by", data.is_documented_by).label}>
                                        <IsDescribedByAutocomplete
                                            className="wd-100" {...this.props} index={0}
                                            {...GenericSchemaParser.getFormElement("is_documented_by", data.is_documented_by)} initialValueArray={model.described_entity.is_documented_by || []}
                                            field="is_documented_by"
                                            model={model}
                                            updateModel_Array={this.updateModel_generic_array}
                                        />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by).label}>
                                        <IsDescribedByAutocomplete className="wd-100" model={this.props.model} {...GenericSchemaParser.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by)} initialValueArray={model.described_entity.is_to_be_cited_by || []} field="is_to_be_cited_by" updateModel_Array={this.setObjectGeneral} />
                                    </div>
                                    {false && <div className="pb-3">
                                        <IsDescribedByAutocomplete
                                            className="wd-100" {...this.props} index={0}
                                            {...GenericSchemaParser.getFormElement("is_described_by", data.is_described_by)} initialValueArray={model.described_entity.is_described_by || []}
                                            field="is_described_by"
                                            model={model}
                                            updateModel_Array={this.updateModel_generic_array}
                                        />
                                    </div>}
                                    {false && <div className="pb-3"><RecordSelectList className="wd-100" {...GenericSchemaParser.getFormElement("support_type", data.support_type)} choices={GenericSchemaParser.getFormElement("support_type", data.support_type).choices} default_value={model.described_entity.support_type} field="support_type" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={4} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("replaces", data.replaces).label}>
                                        <LRAutocomplete {...GenericSchemaParser.getFormElement("replaces", data.replaces)} initialValueArray={model.described_entity.replaces || []} field="replaces" updateModel_Array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("is_version_of", data.is_version_of).label}>
                                        <LRAutocompleteSingle className="wd-100" {...GenericSchemaParser.getFormElement("is_version_of", data.is_version_of)}
                                            label={GenericSchemaParser.getFormElement("is_version_of", data.is_version_of).label}
                                            help_text={GenericSchemaParser.getFormElement("is_version_of", data.is_version_of).help_text}
                                            formStuff={GenericSchemaParser.getFormElement("is_version_of", data.is_version_of).formElements}
                                            initialValue={model.described_entity.is_version_of || {}}
                                            index="is_version_of" field="is_version_of" updateModel={this.setObjectGeneral} /> {/**notice the index prop instead of field!!! */}
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("is_part_of", data.is_part_of).label}>
                                        <LRAutocomplete {...GenericSchemaParser.getFormElement("is_part_of", data.is_part_of)} initialValueArray={model.described_entity.is_part_of || []} field="is_part_of" updateModel_Array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("is_similar_to", data.is_similar_to).label}>
                                        <LRAutocomplete {...GenericSchemaParser.getFormElement("is_similar_to", data.is_similar_to)} initialValueArray={model.described_entity.is_similar_to || []} field="is_similar_to" updateModel_Array={this.updateModel_generic_array} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("is_related_to_lr", data.is_related_to_lr).label}>
                                        <LRAutocomplete {...GenericSchemaParser.getFormElement("is_related_to_lr", data.is_related_to_lr)} initialValueArray={model.described_entity.is_related_to_lr || []} field="is_related_to_lr" updateModel_Array={this.setObjectGeneral} />
                                    </div>
                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("relation", data.relation).label}>
                                        <RelationArray {...GenericSchemaParser.getFormElement("relation", data.relation)} initialValueArray={model.described_entity.relation || []} field="relation" updateModel_array={this.updateModel_generic_array} />
                                    </div>
                                    {false && <div><LRAutocomplete {...GenericSchemaParser.getFormElement("is_part_with", data.is_part_with)} initialValueArray={model.described_entity.is_part_with || []} field="is_part_with" updateModel_Array={this.updateModel_generic_array} /> </div>}
                                    {false && <div><LRAutocomplete {...GenericSchemaParser.getFormElement("is_exact_match_with", data.is_exact_match_with)} initialValueArray={model.described_entity.is_exact_match_with || []} field="is_exact_match_with" updateModel_Array={this.updateModel_generic_array} /> </div>}
                                    {/*has metadata is missing*/}
                                    {false && <div><LRAutocomplete {...GenericSchemaParser.getFormElement("is_archived_by", data.is_archived_by)} initialValueArray={model.described_entity.is_archived_by || []} field="is_archived_by" updateModel_Array={this.updateModel_generic_array} /> </div>}
                                    {false && <div><LRAutocomplete {...GenericSchemaParser.getFormElement("is_continuation_of", data.is_continuation_of)} initialValueArray={model.described_entity.is_continuation_of || []} field="is_continuation_of" updateModel_Array={this.updateModel_generic_array} /> </div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                        <VerticalTabPanel value={this.state.tab} index={5} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    {false && <div className="pb-3"> <ActualUseArray {...GenericSchemaParser.getFormElement("actual_use", data.actual_use)} model={model} initialValueArray={model.described_entity.actual_use || []} field="actual_use" updateModel_array={this.updateModel_generic_array} /></div>}
                                    {false && <div className="pb-3"> <OnOfSwitch {...GenericSchemaParser.getFormElement("validated", data.validated)} default_value={model.described_entity.validated} field="validated" updateBoolean={this.updateBoolean} /></div>}
                                    {false && <div className="pb-3"> <ValidationArray {...GenericSchemaParser.getFormElement("validation", data.validation)} model={model} initialValueArray={model.described_entity.validation || []} field="validation" updateModel_array={this.updateModel_generic_array} /></div>}
                                    {false && <div className="pb-3">
                                        <IsDescribedByAutocomplete
                                            className="wd-100" {...this.props} index={0}
                                            {...GenericSchemaParser.getFormElement("is_cited_by", data.is_cited_by)} initialValueArray={model.described_entity.is_cited_by || []}
                                            field="is_cited_by"
                                            model={model}
                                            updateModel_Array={this.updateModel_generic_array}
                                        />
                                    </div>}
                                    {false && <div className="pb-3">
                                        <IsDescribedByAutocomplete
                                            className="wd-100" {...this.props} index={0}
                                            {...GenericSchemaParser.getFormElement("is_reviewed_by", data.is_reviewed_by)} initialValueArray={model.described_entity.is_reviewed_by || []}
                                            field="is_reviewed_by"
                                            model={model}
                                            updateModel_Array={this.updateModel_generic_array}
                                        />
                                    </div>}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                    </div>
                </div>
            </form>


        </div >
    }
}