import React from "react";
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
import Typography from '@material-ui/core/Typography';
import messages from "./../../config/messages";

export default class IsFunctionalService extends React.Component {
    handleBooleanChange = (e) => {
        this.props.handleFunctionalService(e.target.value === "true");
    }

    render() {
        return <div>
            <Dialog open={true} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="lg" disableBackdropClick={true} fullWidth={true}>
                <DialogTitle>{messages.dialog_functional_service_title_message}</DialogTitle>
                <DialogContent>
                    <DialogContentText component={"div"}>
                        <RecordRadioBoolean required={false} label="" help_text="" handleBooleanChange={this.handleBooleanChange} />
                        <Typography variant="caption">{messages.dialog_functional_service_info_message} <a href={messages.dialog_functional_service_infoLink_message} target="_blank" rel="noopener noreferrer">{messages.dialog_functional_service_infoLink_message}</a></Typography>
                    </DialogContentText>
                </DialogContent>
            </Dialog>
        </div>
    }
}