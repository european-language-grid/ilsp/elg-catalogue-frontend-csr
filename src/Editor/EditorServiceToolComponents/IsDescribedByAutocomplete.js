import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/task-checklist-add.svg";
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/navigation-arrows-down-1.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/task-checklist-remove.svg";
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import TextField from '@material-ui/core/TextField';
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import CircularProgress from "@material-ui/core/CircularProgress";
import { is_described_by_Obj, document_identifier_obj } from "../Models/LrModel";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LRIdentifier from "./LRIdentifier";
import { LOOK_UP_DOCUMENT } from "../../config/editorConstants";
import FilePondDocument from "../editorCommonComponents/FilePondDocument"
import FreeText from "../editorCommonComponents/FreeText";

//const filter = createFilterOptions();
const checkElg = obj => obj.document_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg";
export default class IsDescribedByAutocomplete extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [], documentChoices: [], loading: false, source: null }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || [],
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
        }
    }

    getDocumentChoices = (inputDocument) => {
        if (inputDocument.trim().length <= 2) {
            //this.setState({ documentChoices: [] });
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        axios.get(LOOK_UP_DOCUMENT(inputDocument), { cancelToken: source.token })
            .then((res) => {
                this.setState({ documentChoices: res.data, loading: false, source: null })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false, source: null, documentChoices: [] })
            });
    }

    setValues = (action, itemIndex, value) => {
        const { initialValueArray } = this.state;
        switch (action) {
            case "addArrayItem":
                const filteredArray = initialValueArray.filter(item => {
                    if (item && item.title) {
                        return item.title["en"];
                    }
                    return false;
                });
                if (filteredArray.length !== initialValueArray.length) {
                    return;//do not add element if there are empty values
                }
                initialValueArray.push(JSON.parse(JSON.stringify(is_described_by_Obj)));
                break;
            case "removeArrayItem":
                initialValueArray.splice(itemIndex, 1);
                break;
            case "autoCompleteSelected":
                if (value.similarity) {
                    delete value["similarity"];
                }
                if (value.display_name) {
                    delete value["display_name"];
                }
                if (value.hasOwnProperty('editor-placeholder')) {
                    delete value['editor-placeholder'];
                }
                initialValueArray[itemIndex] = value;
                this.setState({ initialValueArray, documentChoices: [] });
                this.onBlur();
                return;
            default: break;
        }
        this.setState({ initialValueArray, documentChoices: [] }, this.onBlur());
        //this.props.updateModel_Array(this.props.field, initialValueArray);
    }

    set_title = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].title = value;
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray, documentChoices: [] });
    }

    set_bibliographic_record = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].bibliographic_record = value;
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray, documentChoices: [] });
    }

    setDocument = (index, url) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].document_identifier = [];
        const obj = { value: url, document_identifier_scheme: "http://purl.org/spar/datacite/url" };
        initialValueArray[index].document_identifier.push(obj);
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray: initialValueArray, documentChoices: [] }, this.onBlur);
    }

    update_identifier_Array = (index, valueArray) => {
        const { initialValueArray } = this.state;
        initialValueArray[index].document_identifier = valueArray;
        if (initialValueArray[index].hasOwnProperty('editor-placeholder')) {
            delete initialValueArray[index]['editor-placeholder'];
        }
        this.setState({ initialValueArray, documentChoices: [] });
    }

    onBlur = () => {
        this.setState({ documentChoices: [], loading: false });
        this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    autocompleteTextField = (document, documentIndex, languageKey) => {
        const activeLanguage = this.props.activeLanguage || "en";
        const choices = this.props.formElements.document_identifier.formElements.document_identifier_scheme.choices;
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.loading}
                value={document || ""}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValues("autoCompleteSelected", documentIndex, newValue);
                    } else {
                        this.set_title(documentIndex, { "en": newValue })
                    }
                }}
                options={this.state.loading ? [] : this.state.documentChoices}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                title: { 'en': params.inputValue },
                                display_name: `Missing ${this.props.label}? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                title: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue.substring(0, 50) + "..."}? Add "${params.inputValue.substring(0, 50) + "..."}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //noOptionsText={<Button><AddCircleOutlineIcon /> Add a new one</Button>}
                //getOptionLabel={(option) => option.title ? Object.values(option.title)[0] : option}
                getOptionLabel={(option) => option.title ? Object.keys(option.title).includes("en") ? option.title["en"] : Object.values(option.title)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={this.props.formElements.title.label} helperText={this.props.formElements.title.help_text} placeholder={this.props.formElements.title.placeholder ? this.props.formElements.title.placeholder : ""} variant="outlined"
                        //onBlur={(e) => { this.set_title(documentIndex, { "en": e.target.value }) }}
                        onChange={(e) => { this.getDocumentChoices(e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option.title)[0], inputValue);
                    //const parts = parse(Object.values(option.title)[0], matches);
                    const matches = match(option.title[activeLanguage] || Object.values(option.title)[0], inputValue);
                    const parts = parse(option.title[activeLanguage] || Object.values(option.title)[0], matches);
                    return (
                        <div>
                            {
                                option.title && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                            {
                                option.document_identifier && option.document_identifier.map((identifier, identifierIndex) => {
                                    return <div key={identifierIndex}>
                                        {
                                            identifier.document_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                                (`${identifier.value} - ${choices.filter(documentChoiceUrl => documentChoiceUrl.value === identifier.document_identifier_scheme)[0].display_name}`)
                                        }
                                    </div>
                                })

                            }
                        </div>
                    );
                }
                }
            />
            <div className="pb-3">
                {/*{<Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", documentIndex)}>{"Add"}</Button>}*/}
                {documentIndex >= 1 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", documentIndex)}>{"Remove"}</Button>}
            </div>
        </span>
    }

    render() {
        const { initialValueArray } = this.state;
        if (this.state.initialValueArray.length === 0) {
            this.state.initialValueArray.push(JSON.parse(JSON.stringify(is_described_by_Obj)));
        }
        return <div className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {initialValueArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}>{"Add"}</Button>}
                    {/*initialValueArray.length !== 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}><AddCircleOutlineIcon /></Button>*/}
                </Grid>
            </Grid>
            {
                initialValueArray.map((arrayItem, arrayItemIndex) => {
                    const hide_help_text = arrayItem.hasOwnProperty("pk") ? true : false;
                    return <div className="pb-3" key={arrayItemIndex} onBlur={this.onBlur}>
                        <div>
                            {arrayItem.title && !arrayItem.title["en"] && !arrayItem.hasOwnProperty("pk") ?
                                this.autocompleteTextField(arrayItem, arrayItemIndex)
                                :
                                <div>
                                    <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                        <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", arrayItemIndex)}>{"Remove"}</Button>
                                        </Grid>
                                    </Grid>

                                    <LanguageSpecificText
                                        {...this.props.formElements.title}
                                        help_text={hide_help_text ? '' : this.props.formElements.title.help_text}
                                        defaultValueObj={arrayItem.title || { "en": "" }}
                                        disable={arrayItem.pk >= 0 ? true : false}
                                        field={arrayItemIndex}//don't care about field, track index instead
                                        setLanguageSpecificText={this.set_title}
                                    />
                                    <Grid container spacing={1}>
                                        <Grid item xs={arrayItem.hasOwnProperty("pk") ? 12 : 9}>
                                            {(arrayItem.pk && (!arrayItem.document_identifier || (arrayItem.document_identifier.length === 1 && arrayItem.document_identifier.some(checkElg))))
                                                ?
                                                (<></>)
                                                :
                                                (
                                                    <LRIdentifier
                                                        key={JSON.stringify(arrayItem.document_identifier) + '_' + arrayItemIndex + '_' + this.props.index}
                                                        {...this.props.formElements.document_identifier}
                                                        identifier_scheme="document_identifier_scheme"
                                                        scheme_choices={this.props.formElements.document_identifier.formElements.document_identifier_scheme.choices}
                                                        identifier_scheme_required={this.props.formElements.document_identifier.formElements.document_identifier_scheme.required}
                                                        identifier_scheme_label={this.props.formElements.document_identifier.formElements.document_identifier_scheme.label}
                                                        identifier_scheme_help_text={hide_help_text ? '' : this.props.formElements.document_identifier.formElements.document_identifier_scheme.help_text}
                                                        identifier_value_required={this.props.formElements.document_identifier.formElements.value.required}
                                                        identifier_value_label={this.props.formElements.document_identifier.formElements.value.label}
                                                        //identifier_help_text={hide_help_text ? '' : this.props.formElements.document_identifier.formElements.value.help_text}
                                                        identifier_value_placeholder={hide_help_text ? '' : this.props.formElements.document_identifier.formElements.value.placeholder}
                                                        identifier_value_help_text={hide_help_text ? '' : this.props.formElements.document_identifier.formElements.value.help_text}
                                                        identifier_obj={JSON.parse(JSON.stringify(document_identifier_obj))}
                                                        className="wd-100"
                                                        default_valueArray={arrayItem.document_identifier}
                                                        disable={arrayItem.pk >= 0 || (arrayItem.document_identifier && arrayItem.document_identifier.length && arrayItem.document_identifier[0].value && (arrayItem.document_identifier[0].value.indexOf("https://s3.dbl.cloud.syseleven.net/") >= 0 || arrayItem.document_identifier[0].value.indexOf("https://s3.cbk.cloud.syseleven.net/") >= 0)) ? true : false}
                                                        field={arrayItemIndex}////don't care about field, track index instead
                                                        updateModel_Identifier={this.update_identifier_Array}
                                                        required={true}
                                                    ></LRIdentifier>)}
                                        </Grid>
                                        {!arrayItem.hasOwnProperty("pk") && <Grid item xs={3}>
                                            <FilePondDocument model={this.props.model} index={arrayItemIndex} setdocument={this.setDocument} />
                                        </Grid>}
                                    </Grid>

                                    <FreeText
                                        {...this.props.formElements.bibliographic_record}
                                        className="wd-100"
                                        multiline={true}
                                        maxRows={10}
                                        help_text={hide_help_text ? '' : this.props.formElements.bibliographic_record.help_text}
                                        initialValue={arrayItem.bibliographic_record || ""}
                                        disabled={arrayItem.pk >= 0 ? true : false}
                                        field={arrayItemIndex}//don't care about field, track index instead
                                        updateModel={this.set_bibliographic_record}
                                    />

                                    <div className="mb2">

                                        {(this.state.initialValueArray.length - 1) === arrayItemIndex ?
                                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", arrayItemIndex)}>{"Add"} </Button></Grid>
                                            </Grid>
                                            : <span></span>}
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                })
            }
        </div >
    }
}