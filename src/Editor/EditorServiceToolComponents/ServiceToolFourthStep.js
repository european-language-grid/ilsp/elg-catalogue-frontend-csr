import React from "react";
//import Container from '@material-ui/core/Container';
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { ReactComponent as TechnicalIcon } from "./../../assets/elg-icons/editor/virtual-box.svg";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
import EvaluationArray from "./EvaluationArray";
//import OnOfSwitch from "../editorCommonComponents/OnOfSwitch";
import { SERVICE_FOURTCH_SECTION_TABS_HEADERS } from "../../config/editorConstants";
function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}
export default class ServiceToolFourthStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0 };
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    updateModel_generic_array_Subclass = (obj2update, valueArray) => {
        const { model } = this.props;
        if (!model.described_entity.lr_subclass[obj2update]) {
            model.described_entity.lr_subclass[obj2update] = [];
        }
        model.described_entity.lr_subclass[obj2update] = valueArray;
        this.props.updateModel(model);
    }

    updateBoolean = (field, boolean) => {
        const { model } = this.props;
        model.described_entity.lr_subclass[field] = Boolean(boolean);
        this.props.updateModel(model);
    }

    render() {
        //const data = this.props.schema;
        const subclass = this.props.schema_lr_subclass;
        const { model } = this.props;

        return <div>
            
                    <form >
                        <div className="tabs-main-container">
                            <div className="vertical-tabs-container-forms">
                                <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                                    {
                                        SERVICE_FOURTCH_SECTION_TABS_HEADERS.map((tab, index) => {
                                            return <Tab key={index} label={<><div><TechnicalIcon className="small-icon general-icon--tabs mr-05" /></div><div className="pt-2"> {tab} <div className="pt-2"></div></div> </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />
                                        })
                                    }
                                </Tabs>

                                <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                                    <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                        <Grid item xs>
                                            <div className="pb-3"><EvaluationArray model={model} {...GenericSchemaParser.getFormElement("evaluation", subclass.evaluation)} initialValueArray={model.described_entity.lr_subclass.evaluation || []} field="evaluation" lr_subclass={true} updateModel_array={this.updateModel_generic_array_Subclass} /></div>                                        </Grid>
                                    </Grid>
                                </VerticalTabPanel>

                            </div>
                        </div>
                    </form>
 
        </div >
    }
}