import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import { performanceIndicatorObj } from "../Models/ToolModel";
import PerformanceIndicatorItem from "./PerformanceIndicatorItem";

export default class PerformanceIndicatorArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { performanceIndicatorArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            performanceIndicatorArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, performanceIndicatorItemIndex, value) => {
        const { performanceIndicatorArray } = this.state;
        switch (action) {
            case "addperformanceIndicatorItem":
                const filteredArray = performanceIndicatorArray.filter(item => item && item.metric && item.measure >= 0 && item.unit_of_measure_metric);
                if (filteredArray.length !== performanceIndicatorArray.length) {
                    return;//do not add element if required filds are blank
                }
                performanceIndicatorArray.push(JSON.parse(JSON.stringify(performanceIndicatorObj)));
                break;
            case "removeperformanceIndicatorItem":
                performanceIndicatorArray.splice(performanceIndicatorItemIndex, 1);
                break;
            case "setperformanceIndicatorItem":
                performanceIndicatorArray[performanceIndicatorItemIndex] = value;
                this.props.updateModel_array(this.props.field, performanceIndicatorArray);
                break;
            default: break;
        }
        this.setState({ performanceIndicatorArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.performanceIndicatorArray);
    }

    render() {
        const { performanceIndicatorArray } = this.state;
        //performanceIndicatorArray.length === 0 && performanceIndicatorArray.push(JSON.parse(JSON.stringify(performanceIndicatorObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={11}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addperformanceIndicatorItem")}><AddCircleOutlineIcon className="xsmall-icon" /></Button>
                </Grid>
            </Grid>
            {
                performanceIndicatorArray.map((performanceIndicatorItem, performanceIndicatorItemIndex) => {
                    return <div key={performanceIndicatorItemIndex} onBlur={() => this.onBlur()}>
                        <PerformanceIndicatorItem  {...this.props} performanceIndicatorItem={performanceIndicatorItem} performanceIndicatorItemIndex={performanceIndicatorItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("removeperformanceIndicatorItem", performanceIndicatorItemIndex)}><RemoveCircleOutlineIcon className="xsmall-icon" /></Button>
                            <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addperformanceIndicatorItem")}><AddCircleOutlineIcon className="xsmall-icon" /></Button>
                        </div>
                        <div style={{ marginTop: "5em" }} />
                    </div>
                })
            }
        </div>
    }
}