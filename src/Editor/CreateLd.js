import React from "react";
import { withRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import ProgressBar from "../componentsAPI/CommonComponents/ProgressBar";
import Container from '@material-ui/core/Container';
import { empty_ld, ldMLModelObj, ldGrammarObj, ldMediaUnspecifiedPartObj } from "./Models/LdModel";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import HorizontalTabPanel from "../componentsAPI/CustomVerticalTabs/HorizontalTabPanel"
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import LdFirstStep from "./EditorLdComponents/LdFirstStep";
import LdSecondStep from "./EditorLdComponents/LdSecondStep";
import LdThirdStep from "./EditorLdComponents/LdThirdStep";
import LdFourthStep from "./EditorLdComponents/LdFourthStep";
import DisplayModel from "./editorCommonComponents/DisplayModel";
import LookupLdName from "./EditorLdComponents/LookupLdName";
import DashboardAppBar from "../DashboardComponents/DashboardAppBar";
import ValidationYupErrors from "./editorCommonComponents/ValidationYupErrors";
import Tooltip from '@material-ui/core/Tooltip';
import CorpusDataTab from "./EditorCorpusComponents/CorpusDataTab";
import DataComponentDialogueWraper from "./editorCommonComponents/DataComponentDialogueWraper";
import UserWarning from "./editorCommonComponents/UserWarning";
import {
  EDITOR_DRAFT_NEW_RECORD,
  EDITOR_MODELS_LR_SCHEMA,
  EDITOR_MODELS_SCHEMA_LR_SUBCLASS,
  EDITOR_MODELS_SCHEMA_MEDIA_PART,
  EDITOR_MODELS_SCHEMA_LD_MODEL_GRAMMAR_PART,
  LD_TOP_TABS_HEADERS_TOOLTIPS,
  LD_TOP_TABS_HEADERS,
  LD_FIRST_SECTION_TABS_HEADERS,
  LD_SECOND_SECTION_TABS_HEADERS,
  LD_THIRD_SECTION_TABS_HEADERS,
  LD_FOURTH_SECTION_TABS_HEADERS,
  SUBCLASS_TYPE_MODEL,
  SUBCLASS_TYPE_GRAMMAR,
  SUBCLASS_TYPE_OTHER
} from "../config/editorConstants";


function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

class CreateLd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schema: null, schema_lr_subclass: null, schema_text_part: null, schema_video_part: null, schema_image_part: null, schema_ml_model_part: null, schema_grammar_part: null,
      keycloak: props.keycloak, model: props.model ? JSON.parse(JSON.stringify(props.model)) : JSON.parse(JSON.stringify(empty_ld)), tab: 0, backend_error_response: null, LookupLDName: props.model ? false : true,
      under_construction: props.model ? (props.model.management_object && props.model.management_object.under_construction && props.model.management_object.under_construction ? true : false) : null,
      subclass: "",
      yupError: null,
      source: null, loading: false, showDataComponentDialogueWraper: false,
      tabInSection: 0, yupClickError: null, randomValue2ForceRerender: 0, showWarning: false
    };
  }

  componentWillUnmount = () => {
    if (this.state.source) {
      this.state.source.cancel("");
    }
  }

  componentDidMount() {
    if (this.state.source) {
      this.state.source.cancel("");
    }
    const record_pk = this.props.match.params.id;
    let params = (new URL(document.location)).searchParams;
    let subclass = params.get('subclass') || "";
    subclass = subclass.toLowerCase();
    if (subclass !== "model" && subclass !== "grammar" && subclass !== "other") {
      if (record_pk) {
      } else {
        //this.props.history.push("/createResource");
      }
    } else {
      this.setState({ subclass });
    }
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    this.setState({ source: source });
    const ldRequest = axios.options(EDITOR_MODELS_LR_SCHEMA("language_description"), { cancelToken: source.token });
    const ldSubclassRequest = axios.options(EDITOR_MODELS_SCHEMA_LR_SUBCLASS("language_description"), { cancelToken: source.token });
    const ldTextPartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("language_description_text_part"), { cancelToken: source.token });
    const ldVideoPartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("language_description_video_part"), { cancelToken: source.token });
    const ldImagePartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("language_description_image_part"), { cancelToken: source.token });
    const ldGrammarlPartRequest = axios.options(EDITOR_MODELS_SCHEMA_LD_MODEL_GRAMMAR_PART("grammar"), { cancelToken: source.token });
    const ldModellPartRequest = axios.options(EDITOR_MODELS_SCHEMA_LD_MODEL_GRAMMAR_PART("model"), { cancelToken: source.token });
    axios.all([ldRequest, ldSubclassRequest, ldTextPartRequest, ldVideoPartRequest, ldImagePartRequest, ldGrammarlPartRequest, ldModellPartRequest])
      .then(axios.spread((...responses) => {
        const corpusResponse = responses[0]
        const subclassResponse = responses[1]
        const textPartResponse = responses[2];
        const videoPartResponse = responses[3];
        const imagePartResponse = responses[4];
        const grammarPartResponse = responses[5];
        const modelPartResponse = responses[6];
        this.setState({
          schema: corpusResponse.data.actions.POST, schema_lr_subclass: subclassResponse.data.actions.POST, schema_text_part: textPartResponse.data.actions.POST,
          schema_video_part: videoPartResponse.data.actions.POST, schema_image_part: imagePartResponse.data.actions.POST,
          schema_grammar_part: grammarPartResponse.data.actions.POST, schema_ml_model_part: modelPartResponse.data.actions.POST,
          source: null
        })
      }))
      .catch(errors => { console.log(errors); this.setState({ source: null }); })
  }


  handleYupErrorClick = (item) => {
    let parts = item && item.split(">");
    if (parts && parts.length >= 3) {
      const sectionIndex = LD_TOP_TABS_HEADERS(this.state.model.described_entity.lr_subclass.ld_subclass).findIndex(item => item.toLowerCase() === parts[1].trim().toLowerCase()) || 0;
      const tabInSection = this.getTab(sectionIndex, parts[2]) || 0;
      this.setState({ tab: sectionIndex, tabInSection: tabInSection, yupClickError: item, randomValue2ForceRerender: Math.random() });
    }
  }

  getTab = (sectionIndex, tabToMatch) => {
    switch (sectionIndex) {
      case 0:
        return LD_FIRST_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 1:
        return LD_SECOND_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 2:
        return LD_THIRD_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 3:
        return LD_FOURTH_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      default: return 0;
    }
  }

  handleUnderConstruction = (value) => {
    this.setState({ under_construction: value });
  }

  set_backend_error_response = (error) => {
    this.setState({ backend_error_response: error })
  }

  set_backend_successful_response = (response) => {
    this.setState({ model: response, backend_error_response: null })
  }

  set_yup_error = (yupError) => {
    this.setState({ yupError });
  }

  updateModel = (model) => {
    this.setState({ model });
  }

  toggleTab = (index) => {
    this.setState({ tab: index, tabInSection: 0, yupClickError: null });
  };

  saveDraft = () => {
    if (this.state.source) {
      this.state.source.cancel("");
    }
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    this.setState({ source: source, loading: true });

    const model_2_save = this.state.model;
    if (model_2_save?.described_entity?.lr_subclass?.unspecified_part) {
      delete model_2_save.described_entity.lr_subclass.unspecified_part["editor-placeholder"];
    }

    axios.post(EDITOR_DRAFT_NEW_RECORD, model_2_save, {
      headers: {
        'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
        //'FUNCTIONAL-SERVICE': this.props.functional_service === true ? true : false
      },
      cancelToken: source.token
    }).then((res) => {
      let under_construction = false;
      if (res.data.management_object) {
        res.data.management_object.under_construction === true ? under_construction = true : under_construction = false;
      }
      this.setState({ under_construction: under_construction, source: null, laoding: false, model: res.data, showDataComponentDialogueWraper: true });
    }).catch((err) => {
      console.log(JSON.stringify(err));
      this.props.history.push("/createResource");
    });
  }


  hideLookUpByName = (ldName, restProperties) => {
    const { model } = this.state;
    if (restProperties && restProperties.pk) {
      //model.pk = restProperties.pk;
      model.described_entity.pk = restProperties.pk;//add pk of generic record inside described_entity
      delete restProperties.pk;
    }
    model.described_entity.resource_name = { "en": ldName };

    switch (this.state.subclass) {
      case "model":
        model.described_entity.lr_subclass.ld_subclass = SUBCLASS_TYPE_MODEL;
        model.described_entity.lr_subclass.language_description_subclass = JSON.parse(JSON.stringify(ldMLModelObj));
        model.described_entity.lr_subclass.unspecified_part = JSON.parse(JSON.stringify(ldMediaUnspecifiedPartObj));
        break;
      case "grammar":
        model.described_entity.lr_subclass.ld_subclass = SUBCLASS_TYPE_GRAMMAR;
        model.described_entity.lr_subclass.language_description_subclass = JSON.parse(JSON.stringify(ldGrammarObj));
        break;
      case "other":
        model.described_entity.lr_subclass.ld_subclass = SUBCLASS_TYPE_OTHER;
        model.described_entity.lr_subclass.language_description_subclass = null;
        break;
      default:
        //model.described_entity.lr_subclass.ld_subclass = SUBCLASS_TYPE_OTHER;
        //model.described_entity.lr_subclass.language_description_subclass = null;
        break;
    }

    model.described_entity = { ...model.described_entity, ...restProperties };
    this.setState({ LookupLDName: false, model: model }, this.saveDraft);
  }

  handleDescriptionChange = () => {
    //this.setState({showWarning: true});
    const { model } = this.state;
    const { description = "" } = model.described_entity || {};
    (description && description["en"] && description["en"].length < 50) ? this.setState({ showWarning: true }) : this.setState({ showWarning: false })
  }

  render() {
    const { LookupLDName } = this.state;
    if (LookupLDName) {
      return <LookupLdName hideLookUpByName={this.hideLookUpByName} keycloak={this.props.keycloak} subclass={this.state.subclass} />
    }

    if (!this.state.model.hasOwnProperty("pk")) {
      return <ProgressBar />
    }

    if (this.state.showDataComponentDialogueWraper) {
      return <DataComponentDialogueWraper {...this.props}  {...this.state} hideDataComponentDialogueWraper={() => { this.setState({ showDataComponentDialogueWraper: false }) }} />
    }

    if (!this.state.schema || !this.state.schema_lr_subclass) {
      return <ProgressBar />
    }

    const data = this.state.schema;


    //const requiredFields = ["resource name", "description", "version", "additional info", "keyword", "Language description type", "language description media part", "dataset distribution", "personal data included", "sensitive data included"];
    const requiredFields = [];

    return (
      <>
        <Helmet>
          <title>ELG - Create Language Description</title>
        </Helmet>
        <DashboardAppBar />
        <div className="editor-container-white pb-2">
          <Container maxWidth="xl">
            <div className="empty"></div>
            <Typography className="dashboard-title-box pb-05">Create a new language description</Typography>
            <div>
              {this.state.backend_error_response && <div>
                <h3>Error</h3>
                <div className=" boxed">
                  <Paper elevation={13} >
                    <code>
                      <pre id="special">
                        {JSON.stringify(this.state.backend_error_response, null, 2)}
                      </pre>
                    </code>
                  </Paper>
                </div>
              </div>
              }
            </div>
            {!this.state.backend_error_response && <ValidationYupErrors key={this.state.yupError ? JSON.stringify(this.state.yupError) : "1"} yupError={this.state.yupError} requiredFields={requiredFields} handleYupErrorClick={this.handleYupErrorClick} />}
            {<UserWarning showWarning={this.state.showWarning} />}
          </Container>

          <div className="editor-actions-header">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="flex-start" alignItems="center" className="grays--offwhite">
                <Grid item container xs={8} justifyContent="flex-start">
                  <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="horizontal" aria-label="simple tabs example" className="simple-tabs-forms">
                    {LD_TOP_TABS_HEADERS(this.state.model.described_entity.lr_subclass.ld_subclass).map((tab, index) => <Tab key={index} label={<Tooltip title={LD_TOP_TABS_HEADERS_TOOLTIPS(this.state.model.described_entity.lr_subclass.ld_subclass)[index]}><div className="pt-2"> {tab}</div></Tooltip>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                  </Tabs>
                </Grid>
                <Grid item xs={4} container alignItems="center" justifyContent="flex-end" className="grays--offwhite">
                  <DisplayModel
                    data={data}
                    schema_lr_subclass={this.state.schema_lr_subclass}
                    schema_text_part={this.state.schema_text_part}
                    schema_video_part={this.state.schema_video_part}
                    schema_image_part={this.state.schema_image_part}
                    schema_grammar_part={this.state.schema_grammar_part}
                    schema_ml_model_part={this.state.schema_ml_model_part}
                    handleUnderConstruction={this.handleUnderConstruction}
                    under_construction={this.state.under_construction}
                    model={this.state.model}
                    set_backend_error_response={this.set_backend_error_response}
                    set_backend_successful_response={this.set_backend_successful_response}
                    set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <Container maxWidth="xl">
            <div className="editor-main-card-container">
              <HorizontalTabPanel value={this.state.tab} index={0} className="horizontal-tab-pannel">
                <LdFirstStep onDescriptionChange={this.handleDescriptionChange} key={`section-0-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={1} className="horizontal-tab-pannel">
                <LdSecondStep key={`section-1-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={2} className="horizontal-tab-pannel">
                <LdThirdStep key={`section-2-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={3} className="horizontal-tab-pannel">
                <LdFourthStep key={`section-3-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={4} className="horizontal-tab-pannel">
                <CorpusDataTab key={`section-4-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
            </div>
          </Container>
          <div className="editor-actions-bottom">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="flex-end" alignItems="center" className="pb-3 pt-3 grays--offwhite">
                <Grid item container xs={12} alignItems="center">
                  <DisplayModel
                    data={data}
                    schema_lr_subclass={this.state.schema_lr_subclass}
                    schema_text_part={this.state.schema_text_part}
                    schema_video_part={this.state.schema_video_part}
                    schema_image_part={this.state.schema_image_part}
                    schema_grammar_part={this.state.schema_grammar_part}
                    schema_ml_model_part={this.state.schema_ml_model_part}
                    handleUnderConstruction={this.handleUnderConstruction}
                    under_construction={this.state.under_construction}
                    model={this.state.model}
                    set_backend_error_response={this.set_backend_error_response}
                    set_backend_successful_response={this.set_backend_successful_response}
                    hide_checkboxes={true}
                    set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <div>
            {this.state.backend_error_response && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
              <h3>Error</h3>
              <div className=" boxed">
                <Paper elevation={13} >
                  <code>
                    <pre id="special">
                      {JSON.stringify(this.state.backend_error_response, null, 2)}
                    </pre>
                  </code>
                </Paper>
              </div>
            </div>
            }
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(CreateLd)