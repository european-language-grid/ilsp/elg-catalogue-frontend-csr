import React from "react";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LanguageModel from "../EditorServiceToolComponents/LanguageModel";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";

export default class CorpusAudioPartItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }

    updateModel__array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        this.setState({ initialValue }, this.onBlur());
    }

    /*setSelectListLingualityType = (event, selectedObjArray) => {
        const { initialValue } = this.state;
        initialValue.linguality_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ initialValue });
    }*/

    setSelectListMultiLingualityType = (event, selectedObjArray) => {
        const { initialValue } = this.state;
        initialValue.multilinguality_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ initialValue });
    }

    onBlur = () => {
        const { initialValue } = this.state;
        const exists_language = initialValue.language && initialValue.language.filter(l => l.language_tag || l.language_id || l.script_id || l.region_id || (l.variant_id && l.variant_id.length) || (l.language_variety_name && l.language_variety_name.hasOwnProperty("pk") && l.language_variety_name["en"])).length ? true : false;
        const exists_metalanguage = initialValue.metalanguage && initialValue.metalanguage.filter(l => l.language_tag || l.language_id || l.script_id || l.region_id || (l.variant_id && l.variant_id.length) || (l.language_variety_name && l.language_variety_name.hasOwnProperty("pk") && l.language_variety_name["en"])).length ? true : false;

        const exists_multilinguality_type = initialValue.multilinguality_type ? true : false;
        const exist_multilinguality_type_details = initialValue && initialValue.multilinguality_type_details && initialValue.multilinguality_type_details.hasOwnProperty("en") && initialValue.multilinguality_type_details["en"] ? true : false;
        if (exists_language === false && exists_metalanguage === false && exists_multilinguality_type === false && exist_multilinguality_type_details === false) {
            initialValue["editor-placeholder"] = true;
        } else {
            delete initialValue["editor-placeholder"];
        }
        this.props.setValues("setPart", this.props.index, initialValue);
    }

    render() {
        const { initialValue } = this.state;
        const showMultilinguality = (initialValue.language && initialValue.language.length >= 2) ? true : false;

        if (!showMultilinguality) {
            initialValue.multilinguality_type = null;
            initialValue.multilinguality_type_details = null;
        }

        return <div onBlur={this.onBlur}>
            <div><LanguageModel className="wd-100" group_className={"nested--group"} {...GenericSchemaParser.getFormElement("language", this.props.language)} initialValuesArray={initialValue.language || []} field="language" updateModel_array={this.updateModel__array} /></div>
            {showMultilinguality && <>
                <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.multilinguality_type} choices={this.props.multilinguality_type.choices} default_value={initialValue.multilinguality_type} setSelectedvalue={this.setSelectListMultiLingualityType} /></div>
                <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.multilinguality_type_details} defaultValueObj={initialValue.multilinguality_type_details || { "en": "" }} multiline={true} maxRows={6} field="multilinguality_type_details" setLanguageSpecificText={this.updateModel__array} /></div>
            </>}
            <div><LanguageModel className="wd-100" group_className={"nested--group"} {...GenericSchemaParser.getFormElement("metalanguage", this.props.metalanguage)} initialValuesArray={initialValue.metalanguage || []} field="metalanguage" updateModel_array={this.updateModel__array} /></div>

            {/*to do ->modalityType*/}
            {/*to do ->speechItem*/}
            {/*to do ->nonSpeechItem*/}
            {/*to do ->legend*/}
            {/*to do ->noiseLevel*/}
        </div>

    }



}