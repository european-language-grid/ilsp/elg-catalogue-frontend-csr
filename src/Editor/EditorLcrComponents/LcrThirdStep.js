import React from "react";
//import Container from '@material-ui/core/Container';
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { ReactComponent as TextIcon } from "./../../assets/elg-icons/office-file-text-graph-alternate.svg";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
import LcrMediaPart from "./LcrMediaPart";
import { LCR_THIRD_SECTION_TABS_HEADERS } from "../../config/editorConstants";
import { ldMediaUnspecifiedPartObj } from "../Models/LdModel";
import UnspecifiedPartItem from "../EditorLdComponents/UnspecifiedPartItem";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export default class LcrThirdStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0 };
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    ///
    updateModel_generic_array = (obj2update, valueArray, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = [];
            }
            model.described_entity.lr_subclass[obj2update] = valueArray;
            this.props.updateModel(model);
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = [];
            }
            model.described_entity[obj2update] = valueArray;
            this.props.updateModel(model);
        }
    }

    updateModel_generic_Obj = (obj2update, valueObj, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = {};
            }
            model.described_entity.lr_subclass[obj2update] = valueObj;
            this.props.updateModel(model);
        }
    }

    render() {
        const subclass = this.props.schema_lr_subclass;
        const textPart = this.props.schema_text_part;
        const videoPart = this.props.schema_video_part;
        const imagePart = this.props.schema_image_part;
        const audioPart = this.props.schema_audio_part;
        const { model } = this.props;
        const lcr_subclass_default_value = model.described_entity.lr_subclass.lcr_subclass || "";

        const for_info = false
        let show_media_part_view = true;
        if (for_info) {
            if (!model.described_entity.lr_subclass["unspecified_part"]) {
                model.described_entity.lr_subclass["unspecified_part"] = JSON.parse(JSON.stringify(ldMediaUnspecifiedPartObj));
            }
            model.described_entity.lr_subclass.lexical_conceptual_resource_media_part = [];
            show_media_part_view = false;
        } else if (!for_info) {
            model.described_entity.lr_subclass["unspecified_part"] = null;
            show_media_part_view = true;
        }

        const initialValueArray = JSON.parse(JSON.stringify(model.described_entity.lr_subclass.lexical_conceptual_resource_media_part || []));

        return <div>
            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {LCR_THIRD_SECTION_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<><div><TextIcon className="small-icon general-icon--tabs mr-05" /></div><div className="pt-2"> {tab} <div className="pt-2"></div></div> </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                        </Tabs>

                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3">
                                        {show_media_part_view ?
                                            <LcrMediaPart
                                                //key={lcr_subclass_default_value}
                                                {...this.props}
                                                {...GenericSchemaParser.getFormElement("lexical_conceptual_resource_media_part", subclass.lexical_conceptual_resource_media_part)}
                                                subclass={subclass}
                                                textPart={textPart}
                                                videoPart={videoPart}
                                                imagePart={imagePart}
                                                audioPart={audioPart}
                                                initialValueArray={initialValueArray}
                                                field="lexical_conceptual_resource_media_part"
                                                lr_subclass={true}
                                                model={model}
                                                updateModel_Array={this.updateModel_generic_array}
                                                lcr_subclass_default_value={lcr_subclass_default_value}
                                            /> :
                                            <UnspecifiedPartItem
                                                {...subclass.unspecified_part}
                                                field="unspecified_part"
                                                lr_subclass={true}
                                                initialValue={JSON.parse(JSON.stringify(model.described_entity.lr_subclass["unspecified_part"] || ldMediaUnspecifiedPartObj))}
                                                setValues={this.updateModel_generic_Obj}
                                            />
                                        }
                                    </div>
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                    </div>
                </div>
            </form>


        </div>
    }
}