import React from "react";
import axios from "axios";
import { EDITOR_MODELS_SCHEMA_GENERIC_ACTOR, LOOK_UP_ORGANIZATION_BY_NAME } from "../../config/editorConstants";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LanguageSpecificTextList from "../editorCommonComponents/LanguageSpecificTextList";
import WebsiteList from "../editorCommonComponents/WebsiteList";
//import OrganizationIdentifier from "../EditorOrganizationComponents/OrganizationIdentifier";
import LRIdentifier from "../EditorServiceToolComponents/LRIdentifier";
import { generic_organization_obj } from "../Models/GenericModels";
import { organization_identifier_obj } from "../Models/OrganizationModel";
import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";
import { TextField } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import messages from "./../../config/messages";
import Typography from '@material-ui/core/Typography';
import OrganizationIsDivisionOfArray from "./OrganizationIsDivisionOfArray";
//import OrganizationIsDivisionOf from "./OrganizationIsDivisionOf";

//const filter = createFilterOptions();

const checkElg = obj => obj.organization_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg";

export default class GenericOrganization extends React.Component {
    constructor(props) {
        super(props);
        this.state = { schema: null, initialValue: props.initialValue || JSON.parse(JSON.stringify(generic_organization_obj)), options: [], loading: false, source: null };
        this.isUnmound = false;
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue || JSON.parse(JSON.stringify(generic_organization_obj))
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
        }
    }

    componentDidMount() {
        this.isUnmound = true;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        axios.options(EDITOR_MODELS_SCHEMA_GENERIC_ACTOR("generic_organization"), { cancelToken: source.token }).then(res => {
            if (this.isUnmound) {
                if (res.data.actions && res.data.actions.POST) {
                    this.setState({ schema: res.data.actions.POST, source: null });
                }
            }
        }).catch(err => { console.log(err); this.setState({ source: null }); });
    }

    getOrganizationChoices = (field, value) => {
        if (value.trim().length <= 2) {
            //this.setState({ options: [] });
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        axios.get(LOOK_UP_ORGANIZATION_BY_NAME(value), { cancelToken: source.token }).then(res => {
            this.setState({ options: res.data, loading: false, source: null })
        }).catch((err) => {
            console.log(err);
            this.setState({ loading: false, options: [], source: null })
        });
    }

    setValues = (action, value) => {
        let { initialValue } = this.state;
        switch (action) {
            case "autoCompleteSelected":
                if (value.similarity) {
                    delete value["similarity"];
                }
                if (value.display_name) {
                    delete value["display_name"];
                }
                if (value.hasOwnProperty('editor-placeholder')) {
                    delete value['editor-placeholder'];
                }
                initialValue = value;
                this.setState({ initialValue, options: [] });
                this.props.updateModel(this.props.index, initialValue);
                break;
            default: break;
        }
        this.setState({ initialValue, options: [] });
    }

    autocompleteTextField = (field) => {
        const activeLanguage = this.props.activeLanguage || "en";
        const scheme_choices = GenericSchemaParser.getFormElement("organization_identifier", this.state.schema.organization_identifier).formElements.organization_identifier_scheme.choices;
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.loading}
                value={this.state.initialValue}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValues("autoCompleteSelected", newValue);
                    } else {
                        this.setObjectGeneral(field, { "en": newValue });
                    }
                }}
                options={this.state.loading ? [] : this.state.options}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /* filtered.push({
                                 ...JSON.parse(JSON.stringify(generic_organization_obj)),
                                 [field]: { 'en': params.inputValue },
                                 display_name: `Missing ${GenericSchemaParser.getFormElement("organization_name", this.state.schema.organization_name).label}? Add "${params.inputValue}"`,
                             });*/
                            filtered.splice(0, 0, {
                                ...JSON.parse(JSON.stringify(generic_organization_obj)),
                                [field]: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue} ? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.organization_name ? Object.values(option[field])[0] : ""}
                getOptionLabel={(option) => option.organization_name ? Object.keys(option[field]).includes("en") ? option[field]["en"] : Object.values(option[field])[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={GenericSchemaParser.getFormElement(field, this.state.schema[field]).label} helperText={GenericSchemaParser.getFormElement(field, this.state.schema[field]).help_text} placeholder={GenericSchemaParser.getFormElement(field, this.state.schema[field]).placeholder} variant="outlined"
                        //onBlur={(e) => { this.setObjectGeneral(field, { "en": e.target.value }); }}
                        onChange={(e) => { this.getOrganizationChoices(field, e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    const parent = this.getParentGeneric(option);
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option[field])[0], inputValue);
                    //const parts = parse(Object.values(option[field])[0], matches);
                    const matches = match(option[field][activeLanguage] || Object.values(option[field])[0], inputValue);
                    const parts = parse(option[field][activeLanguage] || Object.values(option[field])[0], matches);
                    return (
                        <div>
                            {
                                option[field] && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                    {parent && <span>
                                        , {parent}
                                    </span>
                                    }
                                </div>
                            }
                            {
                                option.organization_identifier && option.organization_identifier.map((item, index) => {
                                    return <div key={index}>
                                        {item.organization_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                            (`${item.value} - ${scheme_choices.filter(schemeChoice => schemeChoice.value === item.organization_identifier_scheme)[0].display_name}`)
                                        }
                                    </div>
                                })
                            }
                            {
                                option.website && option.website.map((website, websiteIndex) => {
                                    return <div key={websiteIndex}>
                                        <span className="info_url">
                                            {<span>{website}</span>}
                                        </span>
                                    </div>
                                })

                            }

                        </div>
                    );
                }
                }
            />
        </span>
    }

    getParentGeneric = (item) => {
        const division_of = item?.is_division_of;
        let parent = this.getParents2(division_of);
        if (parent && parent.startsWith(",")) {
            parent = parent.substring(1).trim();
        }
        if (parent && parent.endsWith(",")) {
            parent = parent.substring(0, parent.length - 1).trim();
        }
        return parent;
    }

    getParents2 = (division_of) => {
        if (!division_of || division_of.length === 0) {
            return null;
        }
        let parents = "";
        for (let i = 0; i < division_of.length; i++) {
            const { organization_name } = division_of[i];
            if (division_of[i]["is_division_of"] && division_of[i]["is_division_of"].length > 0) {
                parents += this.getParents2(division_of[i]["is_division_of"]) + "--:--" + organization_name["en"];
            } else {
                parents += ", " + organization_name["en"];
            }
        }

        parents = parents.split("--:--");
        if (parents.length > 0) {
            let p = "";
            for (let i = parents.length - 1; i >= 0; i--) {
                let cleanName = parents[i].trim().startsWith(",") ? parents[i].trim().substring(1) : parents[i].trim();
                cleanName = cleanName.trim().endsWith(",") ? cleanName.substring(0, cleanName.trim().length - 1) : cleanName;
                p += cleanName + ", ";
            }

            parents = p.trim();
        }

        return parents;
    }

    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] });
    }

    updateModel_generic_array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }

    updateDevisionOf = (is_division_ofValue) => {
        const { initialValue } = this.state;
        if (!initialValue["is_division_of"]) {
            initialValue["is_division_of"] = null;
        }
        initialValue["is_division_of"] = is_division_ofValue;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }

    updateDevisionOfArray = (is_division_ofValue) => {
        const { initialValue } = this.state;
        if (!initialValue["is_division_of"]) {
            initialValue["is_division_of"] = [];
        }
        initialValue["is_division_of"] = is_division_ofValue;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }

    onBlur = () => {
        this.setState({ options: [], loading: false });
        if (this.state.initialValue && this.state.initialValue.organization_name && this.state.initialValue.organization_name["en"]) {
            this.props.updateModel(this.props.index, this.state.initialValue);
        }
    }

    render() {
        const { schema, initialValue } = this.state;
        if (!schema) {
            return <ProgressBar />
        }
        const hide_help_text = initialValue.hasOwnProperty("pk") ? true : false;
        return <div onBlur={this.onBlur}>
            {initialValue.organization_name && !initialValue.organization_name["en"] ?
                <div>
                    <div>{this.autocompleteTextField("organization_name")}</div>
                </div>
                :
                <div className="pb-3 inner--group nested--group">
                    <div onBlur={this.onBlur}>
                        <Typography variant="h3" className="section-links pb1" >{messages.editor_generic_organization}</Typography>
                        <LanguageSpecificText
                            {...GenericSchemaParser.getFormElement("organization_name", schema.organization_name)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("organization_name", schema.organization_name).help_text}
                            //placeholder={this.props.formElements && this.props.formElements.organization_name && this.props.formElements.organization_name.placeholder ? this.props.formElements.organization_name.placeholder : ""}
                            defaultValueObj={initialValue.organization_name || { "en": "" }}
                            field="organization_name"
                            disable={initialValue.pk >= 0 ? true : false}
                            setLanguageSpecificText={this.setObjectGeneral}
                        />
                        <LanguageSpecificTextList
                            {...GenericSchemaParser.getFormElement("organization_short_name", schema.organization_short_name)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("organization_short_name", schema.organization_short_name).help_text}
                            default_valueArray={initialValue.organization_short_name || []}
                            field="organization_short_name"
                            disable={initialValue.pk >= 0 ? true : false}
                            show_heading={false}
                            setLanguageSpecifictextList={this.setObjectGeneral}
                        />
                    </div>

                    {initialValue.pk && (!initialValue.organization_identifier || (initialValue.organization_identifier.length === 1 && initialValue.organization_identifier.some(checkElg)))
                        ?
                        (<></>)
                        :
                        (
                            <div className="inner--group nested--group">
                                <LRIdentifier
                                    key={JSON.stringify(initialValue) + 'generic_organization' + this.props.index}
                                    {...GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier)}
                                    identifier_scheme="organization_identifier_scheme"
                                    scheme_choices={GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.organization_identifier_scheme.choices}
                                    identifier_scheme_required={GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.organization_identifier_scheme.required}
                                    identifier_scheme_label={GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.organization_identifier_scheme.label}
                                    identifier_scheme_help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.organization_identifier_scheme.help_text}
                                    identifier_value_required={GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.value.required}
                                    identifier_value_label={GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.value.label}
                                    //identifier_value_placeholder={this.props.formElements && this.props.formElements.organization_identifier && this.props.formElements.organization_identifier.formElements && this.props.formElements.organization_identifier.formElements.value && this.props.formElements.organization_identifier.formElements.value.placeholder ? this.props.formElements.organization_identifier.formElements.value.placeholder : ""}
                                    identifier_value_placeholder={hide_help_text ? '' : GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.value.placeholder}
                                    identifier_value_help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.value.help_text}
                                    //identifier_help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("organization_identifier", schema.organization_identifier).formElements.value.help_text}
                                    identifier_obj={JSON.parse(JSON.stringify(organization_identifier_obj))}
                                    className="wd-100"
                                    default_valueArray={initialValue.organization_identifier}
                                    field="organization_identifier"
                                    disable={initialValue.pk >= 0 ? true : false}
                                    updateModel_Identifier={this.updateModel_generic_array}
                                /> </div>
                        )

                    }


                    <div className="pt-3 pb-3">
                        <WebsiteList
                            {...GenericSchemaParser.getFormElement("website", schema.website)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("website", schema.website).help_text}
                            //placeholder={this.props.formElements && this.props.formElements.website && this.props.formElements.website.placeholder ? this.props.formElements.website.placeholder : ""}
                            className="wd-100"
                            default_value_Array={initialValue.website || []}
                            field="website"
                            disable={initialValue.pk >= 0 ? true : false}
                            updateModel_website={this.updateModel_generic_array}
                        />
                    </div>

                    <div>
                        <OrganizationIsDivisionOfArray
                            key={(initialValue.is_division_of && initialValue.is_division_of.length > 0) ? JSON.stringify(initialValue.is_division_of) : `key-${Math.random()}`}
                            initialValueArray={JSON.parse(JSON.stringify(initialValue.is_division_of || []))}
                            child_to_add_parents_name={(initialValue.organization_name && initialValue.organization_name["en"]) || null}
                            disable={hide_help_text}
                            updateDevisionOf={this.updateDevisionOfArray}
                        />
                    </div>
                </div>
            }
        </div>
    }
}