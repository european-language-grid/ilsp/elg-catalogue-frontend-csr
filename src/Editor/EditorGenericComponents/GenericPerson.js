import React from "react";
import axios from "axios";
import { EDITOR_MODELS_SCHEMA_GENERIC_ACTOR, LOOK_UP_PERSON } from "../../config/editorConstants";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import EmailList from "../editorCommonComponents/EmailList";
import LRIdentifier from "../EditorServiceToolComponents/LRIdentifier";
import { generic_person_obj } from "../Models/GenericModels";
import { personal_identifier_obj } from "../Models/LrModel";
import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";
import { TextField } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
//import EmailsWithIcon from "../../componentsAPI/CommonComponents/EmailsWithIcon";
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import messages from "./../../config/messages";
import Typography from '@material-ui/core/Typography';

//const filter = createFilterOptions();

const checkElg = obj => obj.personal_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg";

export default class GenericPerson extends React.Component {
    constructor(props) {
        super(props);
        this.state = { schema: null, initialValue: props.initialValue || JSON.parse(JSON.stringify(generic_person_obj)), options: [], loading: false, source: null };
        this.isUnmound = false;
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue || JSON.parse(JSON.stringify(generic_person_obj))
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
        }
    }

    componentDidMount() {
        this.isUnmound = true;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        axios.options(EDITOR_MODELS_SCHEMA_GENERIC_ACTOR("generic_person"), { cancelToken: source.token }).then(res => {
            if (this.isUnmound) {
                if (res.data.actions && res.data.actions.POST) {
                    this.setState({ schema: res.data.actions.POST });
                }
            }
        }).catch(err => { console.log(err); this.setState({ source: null }); });;
    }

    getPersonChoices = (field, value) => {
        if (value.trim().length <= 2) {
            //this.setState({ options: []});
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        if (field === "surname") {
            axios.get(LOOK_UP_PERSON("surname=" + value), { cancelToken: source.token }).then(res => {
                this.setState({ options: res.data, loading: false, source: null })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false, options: [], source: null })
            });
        } else if (field === "given_name") {
            axios.get(LOOK_UP_PERSON("given_name=" + value), { cancelToken: source.token }).then(res => {
                this.setState({ options: res.data, loading: false, source: null })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false, options: [], source: null })
            });
        }
    }

    setValues = (action, value) => {
        let { initialValue } = this.state;
        switch (action) {
            case "autoCompleteSelected":
                if (value.similarity) {
                    delete value["similarity"];
                }
                if (value.display_name) {
                    delete value["display_name"];
                }
                if (value.hasOwnProperty('editor-placeholder')) {
                    delete value['editor-placeholder'];
                }
                initialValue = value;
                this.setState({ initialValue, options: [] });
                this.props.updateModel(this.props.index, initialValue);
                break;
            default: break;
        }
        this.setState({ initialValue, options: [] });
    }

    autocompleteTextField = (field) => {
        const activeLanguage = this.props.activeLanguage || "en";
        const scheme_choices = GenericSchemaParser.getFormElement("personal_identifier", this.state.schema.personal_identifier).formElements.personal_identifier_scheme.choices;
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.loading}
                value={this.state.initialValue}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValues("autoCompleteSelected", newValue);
                    } else {
                        this.setObjectGeneral(field, { "en": newValue });
                    }
                }}
                options={this.state.loading ? [] : this.state.options}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                ...JSON.parse(JSON.stringify(generic_person_obj)),
                                [field]: { 'en': params.inputValue },
                                display_name: `Missing ${GenericSchemaParser.getFormElement(field, this.state.schema[field]).label} ? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                ...JSON.parse(JSON.stringify(generic_person_obj)),
                                [field]: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue} ? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.surname ? Object.values(option[field])[0] : ""}
                getOptionLabel={(option) => option.surname ? Object.keys(option.surname).includes("en") ? option.surname["en"] : Object.values(option.surname)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={GenericSchemaParser.getFormElement(field, this.state.schema[field]).label} helperText={GenericSchemaParser.getFormElement(field, this.state.schema[field]).help_text} placeholder={GenericSchemaParser.getFormElement(field, this.state.schema[field]).placeholder ? GenericSchemaParser.getFormElement(field, this.state.schema[field]).placeholder : ""} variant="outlined"
                        //onBlur={(e) => { this.setObjectGeneral(field, { "en": e.target.value }); }}
                        onChange={(e) => { this.getPersonChoices(field, e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option[field])[0], inputValue);
                    //const parts = parse(Object.values(option[field])[0], matches);
                    const matches = match(option[field][activeLanguage] || Object.values(option[field])[0], inputValue);
                    const parts = parse(option[field][activeLanguage] || Object.values(option[field])[0], matches);
                    return (
                        <div>
                            {
                                option[field] && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                    {field === "surname" && option.given_name ? " " + Object.values(option.given_name)[0] : ""}
                                    {field === "given_name" && option.surname ? " " + Object.values(option.surname)[0] : ""}
                                </div>
                            }
                            {
                                option.email && option.email.map((email, emailIndex) => {
                                    return <div key={emailIndex}>
                                        <span className="info_url">
                                            {<span ><MailOutlineIcon /> </span>}
                                            {<span>{email}</span>}
                                        </span>
                                    </div>
                                })
                                //<EmailsWithIcon emailArray={option.email} />
                            }
                            {
                                option.personal_identifier && option.personal_identifier.map((item, index) => {
                                    return <div key={index}>
                                        {item.personal_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                            (`${item.value} - ${scheme_choices.filter(schemeChoice => schemeChoice.value === item.personal_identifier_scheme)[0].display_name}`)
                                        }
                                    </div>
                                })
                            }
                        </div>
                    );
                }
                }
            />
        </span>
    }

    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] });
    }

    updateModel_generic_array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }

    onBlur = () => {
        this.setState({ options: [], loading: false });
        this.props.updateModel(this.props.index, this.state.initialValue);
    }

    render() {
        const { schema, initialValue } = this.state;
        if (!schema) {
            return <ProgressBar />
        }
        const hide_help_text = initialValue.hasOwnProperty("pk") ? true : false;
        return <div onBlur={this.onBlur}>
            {/*<TextField
                helperText={GenericSchemaParser.getFormElement("actor_type", schema.actor_type).help_text}
                required={GenericSchemaParser.getFormElement("actor_type", schema.actor_type).required}
                label={GenericSchemaParser.getFormElement("actor_type", schema.actor_type).label}
                disabled={true}
                variant="outlined"
                //value={"Person"}
                value={initialValue.actor_type}
            />*/}
            {initialValue.surname && !initialValue.surname["en"] ?
                <div>
                    <div>{this.autocompleteTextField("surname")}</div>
                    <div> {/*this.autocompleteTextField("given_name")*/}</div>
                </div>
                :
                <div className="pb-3 inner--group nested--group">
                    <div onBlur={this.onBlur}>
                        <Typography variant="h3" className="section-links pb1">{messages.editor_generic_person}</Typography>
                        <LanguageSpecificText
                            {...GenericSchemaParser.getFormElement("surname", schema.surname)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("surname", schema.surname).help_text}
                            defaultValueObj={initialValue.surname || { "en": "" }}
                            field="surname"
                            disable={initialValue.pk >= 0 ? true : false}
                            setLanguageSpecificText={this.setObjectGeneral}
                        />
                    </div>
                    <div onBlur={this.onBlur}>
                        <LanguageSpecificText
                            {...GenericSchemaParser.getFormElement("given_name", schema.given_name)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("given_name", schema.given_name).help_text}
                            defaultValueObj={initialValue.given_name || { "en": "" }}
                            field="given_name"
                            disable={initialValue.pk >= 0 ? true : false}
                            setLanguageSpecificText={this.setObjectGeneral}
                        />
                    </div>
                    {initialValue.pk && (!initialValue.personal_identifier || (initialValue.personal_identifier.length === 1 && initialValue.personal_identifier.some(checkElg)))
                        ?
                        (<></>)
                        :
                        (
                            <div className="inner--group nested--group">
                                <LRIdentifier
                                    key={JSON.stringify(initialValue) + 'generic_person' + this.props.index}
                                    {...GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier)}
                                    identifier_scheme="personal_identifier_scheme"
                                    scheme_choices={GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.personal_identifier_scheme.choices}
                                    identifier_scheme_required={GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.personal_identifier_scheme.required}
                                    identifier_scheme_label={GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.personal_identifier_scheme.label}
                                    identifier_scheme_help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.personal_identifier_scheme.help_text}
                                    identifier_value_required={GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.value.required}
                                    identifier_value_label={GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.value.label}
                                    //identifier_help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.value.help_text}
                                    identifier_value_placeholder={hide_help_text ? '' : GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.value.placeholder}
                                    identifier_value_help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("personal_identifier", schema.personal_identifier).formElements.value.help_text}
                                    identifier_obj={JSON.parse(JSON.stringify(personal_identifier_obj))}
                                    className="wd-100"
                                    default_valueArray={initialValue.personal_identifier}
                                    field="personal_identifier"
                                    disable={initialValue.pk >= 0 ? true : false}
                                    updateModel_Identifier={this.updateModel_generic_array}
                                /></div>)}
                    <div className="pt-3 pb-3">
                        <EmailList
                            {...GenericSchemaParser.getFormElement("email", schema.email)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("email", schema.email).help_text}
                            className="wd-100"
                            default_value_Array={initialValue.email || []}
                            field="email"
                            disable={initialValue.pk >= 0 ? true : false}
                            updateModel_email={this.updateModel_generic_array}
                        /></div>
                </div>
            }
        </div>
    }
}