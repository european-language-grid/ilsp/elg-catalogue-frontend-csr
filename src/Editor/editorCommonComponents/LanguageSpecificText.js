import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import EulanguageSelection from "./EuLanguagesSelection";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class LanguageSpecificText extends React.Component {

    constructor(props) {
        super(props);
        this.state = { stateObj: this.props.defaultValueObj ? this.props.defaultValueObj : { "en": "" }, errorObj: { "en": "" } };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            stateObj: nextProps.defaultValueObj ? nextProps.defaultValueObj : { "en": "" },
            errorObj: { "en": "" }
        };
    }


    setValues = (event, langIndex, action2Perform, languageKey, oldLanguageValueObj) => {
        const obj = this.state.stateObj;
        const action = event.target.name || action2Perform;
        switch (action) {
            case "textValue":
                obj[languageKey] = event.target.value;
                break;
            case "changeLanguage":
                if (Object.keys(obj).includes(languageKey)) {
                    return;
                }
                const oldkey = oldLanguageValueObj.key;
                delete Object.assign(obj, { [languageKey]: obj[oldkey] })[oldkey];
                break;
            case "addLanguage":
                if (Object.values(obj).filter(val => val).length !== Object.values(obj).length) {
                    return; //do not add language field if there is an empty value field
                }
                if (Object.keys(obj).filter(val => val).length !== Object.keys(obj).length) {
                    return; //do not add language field if there is an empty language key
                }
                obj[""] = "";
                break;
            case "removeLanguage":
                delete obj[languageKey];
                break;
            default: return;
        }
        this.setState({ stateObj: obj });
    }

    onBlur = () => {
        const { errorObj } = this.state;
        this.props.required && Object.keys(this.state.stateObj).forEach(key => this.state.stateObj[key] ? errorObj[key] = "" : errorObj[key] = "Required field");
        if (!this.state.stateObj["en"]) {
            this.props.setLanguageSpecificText(this.props.field, null);
            return;
        }
        this.props.setLanguageSpecificText(this.props.field, this.state.stateObj);
    }

    render() {
        //const { field,defaultValueObj, allowMultipleLanguages = false } = this.props;
        const { type, required, label, help_text, className, multiline, maxRows, disable, placeholder } = this.props;
        // const defaultValuesArray = Object.values(this.state.stateObj) || [];
        const defaultLangArray = Object.keys(this.state.stateObj) || ["en"];
        const disabledLanguagesArray = [];

        return <div onBlur={this.onBlur}>
            {defaultLangArray.map((lang, langIndex) => {
                disabledLanguagesArray.push(lang);
                return <div className="inline--container" key={langIndex}>
                    <span className="wd-100">
                        <TextField
                            id={`${label}-${lang}-${langIndex}`}
                            fullWidth
                            //error={required && this.state.errorObj[lang] === "Required field"}
                            //error={required && this.state.stateObj[lang] === ""}
                            type={type}
                            disabled={disable}
                            className={className}
                            required={required}
                            label={label}
                            placeholder={placeholder ? placeholder : ""}
                            variant="outlined"
                            helperText={this.state.errorObj[lang] || help_text}
                            inputProps={{
                                // type: "password",
                                autoComplete: 'new-password'
                            }}
                            //inputProps={{ name: 'textValue' }}
                            //value={defaultValuesArray[langIndex] || ""}
                            value={this.state.stateObj[lang] || ""}
                            onChange={(e) => this.setValues(e, langIndex, "textValue", lang)}
                            multiline={multiline}
                            minRows={maxRows ? maxRows : void 0}
                            maxRows={maxRows ? maxRows : void 0}
                        />
                    </span>
                    <span className="pl10 wd-60">
                        <EulanguageSelection className={className} showHelpText={help_text ? true : false} lang={lang} languageIndex={langIndex} disable={disable} setValues={this.setValues} disabledLanguagesArray={disabledLanguagesArray} />
                    </span>
                    {langIndex === 0 && !disable && <Button disabled={disable} onClick={(e) => { this.setValues(e, langIndex, "addLanguage") }}><Tooltip title={messages.multilingual_elements_add_icon_hover}><AddCircleOutlineIcon className="small-icon" /></Tooltip></Button>}
                    {lang !== "en" && !disable && <Button disabled={disable} onClick={(e) => { this.setValues(e, langIndex, "removeLanguage", lang) }}><Tooltip title={messages.multilingual_elements_remove_icon_hover}><RemoveCircleOutlineIcon className="small-icon" /></Tooltip></Button>}
                </div>
            })}
        </div>
    }
}