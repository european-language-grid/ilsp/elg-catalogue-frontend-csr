import React from "react";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Switch from '@material-ui/core/Switch';

export default class RecordRadioBoolean extends React.Component {
    render() {
        const { required, label, help_text, default_value } = this.props;
        const default_value_boolean = (default_value === null || default_value === undefined) ? Boolean("") : Boolean(default_value);
        return <div className="mb1">
            <FormControl component="fieldset" required={required}>
                <FormLabel component="legend">{help_text}</FormLabel>
                <FormControlLabel
                    control={<Switch color={"primary"} checked={default_value_boolean} onChange={(e) => { this.props.updateBoolean(this.props.field, e.target.checked) }} name={label} />}
                    label={label}
                />
            </FormControl>
        </div>
    }
}