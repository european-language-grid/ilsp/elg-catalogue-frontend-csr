import React from "react";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import Chip from '@material-ui/core/Chip';

export default class AutocompleteChoicesChips extends React.Component {

    constructor(props) {
        super(props);
        this.state = { initialValuesArray: this.props.initialValuesArray || [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValuesArray: nextProps.initialValuesArray || [],
        };
    }

    setValues = (action, arrayIndex, newValue) => {
        const { initialValuesArray } = this.state;
        switch (action) {
            case "onchange":
                initialValuesArray.length = 0;
                const a = newValue.map(item => item.value || item);
                for (let index = 0; index < a.length; index++) {
                    let item2Add = a[index];
                    initialValuesArray.push(item2Add);
                }
                break;
            default: break;
        }
        this.setState({ initialValuesArray });
        this.onBlur();
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.initialValuesArray, this.props.lr_subclass);
    }

    renderAutocomplete = (default_valueArray) => {
        const initialValues = default_valueArray.map(inititalUrl => {
            let filteredArray = this.props.choices.filter(recommended => recommended.value === inititalUrl);
            if (filteredArray.length > 0) {
                return filteredArray[0].display_name;
            }
            return inititalUrl;
        })
        return <div onBlur={this.onBlur}><Grid container direction="row" alignItems="center" justifyContent="flex-start">
            <Grid item sm={12}>
                <Autocomplete
                    multiple
                    selectOnFocus
                    handleHomeEndKeys
                    autoHighlight
                    clearOnBlur={true}
                    blurOnSelect={true}
                    value={initialValues}
                    disabled={this.props.disabled}
                    onChange={(event, newValue) => {
                        newValue = newValue.map(item => this.props.choices.filter(recommended_choice => recommended_choice.display_name === item).length > 0 ? this.props.choices.filter(recommended_choice => recommended_choice.display_name === item)[0] : item);
                        this.setValues("onchange", null, newValue)
                    }}
                    options={this.props.choices}
                    getOptionLabel={(option) => {
                        const ar = this.props.choices.filter(recommended_choice => recommended_choice.value === option.value);
                        if (ar.length > 0) {
                            return ar[0].display_name;
                        }
                        return option;
                    }}
                    renderInput={(params) => (
                        <TextField className={this.props.className} {...params} disabled={this.props.disabled} label={this.props.label} helperText={this.props.help_text} required={this.props.required} variant="outlined" />)}
                    renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                            <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                        ))
                    }
                    renderOption={(option, { inputValue }) => {
                        const matches = match(option.display_name, inputValue);
                        const parts = parse(option.display_name, matches);
                        return (
                            <div>
                                {parts.map((part, index) => (
                                    <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                        {part.text}
                                    </span>
                                ))}
                            </div>
                        );
                    }
                    }
                />
            </Grid>
        </Grid>
        </div>
    }



    render() {
        const initialValuesArray = this.state.initialValuesArray;
        return <div className="mb1" onBlur={this.onBlur}>
            <div className={this.props.className}>
                {this.renderAutocomplete(initialValuesArray)}
            </div>
        </div>
    }


}