import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import validator from 'validator';
import Grid from '@material-ui/core/Grid';

//import Autocomplete from '@material-ui/lab/Autocomplete';

const emailValidationError = "Enter a valid email address e.g. example@elg.eu \n";

export default class EmailList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { emailArray: props.default_value_Array || [], errorArray: [], stopWriting: true };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            emailArray: nextProps.default_value_Array || [],
            stopWriting: true,
            errorArray: []
        };
    }

    componentDidMount() {
        const { errorArray, emailArray } = this.state;
        for (let index = 0; index < emailArray.length; index++) {
            const valid = validator.isEmail(emailArray[index] || "");
            errorArray[index] = !valid;
        }
        this.setState({ errorArray, stopWriting: true });
    }

    setEmail = (event, index, addRemoveEmail) => {
        const { errorArray, emailArray } = this.state;
        const action = event.target.name || addRemoveEmail;
        switch (action) {
            case "emailValue": emailArray[index] = event.target.value; break;
            case "addEmail":
                if (emailArray.filter(mail => mail).length < emailArray.length) {
                    return;//do not add another email if there is an empty field
                }
                emailArray.push("");
                errorArray.push("");
                break;
            case "removeEmail":
                emailArray.splice(index, 1);
                errorArray.splice(index, 1);
                this.setState({ emailArray, errorArray }, this.blur);
                return;
            default: break;
        }
        this.setState({ emailArray, errorArray, stopWriting: false });
        //this.props.updateModel_email("email", emailArray);
    }

    blur = (e, index) => {
        const { errorArray, emailArray } = this.state;
        const valid = validator.isEmail(emailArray[index] || "");
        errorArray[index] = !valid;
        this.setState({ errorArray, stopWriting: true });
        const filteredArray = emailArray.filter(item => item);
        if (emailArray.length === 1) {
            this.props.updateModel_email((this.props.field !== null && this.props.field !== undefined) ? this.props.field : "email", filteredArray);
            return;
        }
        this.props.updateModel_email((this.props.field !== null && this.props.field !== undefined) ? this.props.field : "email", filteredArray);
    }

    render() {
        const { type, required, label, help_text, className, disable } = this.props;
        const default_value_Array = this.state.emailArray;
        const default_value = default_value_Array[0] || "";
        const hasError = default_value ? (!validator.isEmail(default_value || "")) : false;
        return <div>
            <Grid container direction="row" justifyContent="space-between" alignItems="baseline" >
                <Grid item xs={11}>
                    <TextField className={className}
                        type={type}
                        error={hasError && this.state.stopWriting}
                        required={required}
                        label={label}
                        variant="outlined"
                        helperText={hasError && this.state.stopWriting ? `${emailValidationError}` : help_text}
                        inputProps={{ name: 'emailValue' }}
                        disabled={disable}
                        value={default_value}
                        onChange={(e) => this.setEmail(e, 0)}
                        onBlur={(e) => this.blur(e, 0)}
                    /></Grid>
                <Grid item xs={1}><Button disabled={disable} onClick={(e) => this.setEmail(e, 0, "addEmail")}><AddCircleOutlineIcon className="small-icon" /></Button></Grid>
            </Grid>
            {
                default_value_Array.map((item, index) => {
                    if (index === 0) {
                        return void 0;
                    }
                    const hasError = item ? (!validator.isEmail(item || "")) : false
                    return <Grid key={index} container direction="row" justifyContent="space-between" alignItems="baseline" >
                        <Grid item xs={11} className="pt1" key={index}>
                            <TextField className={className}
                                type={type}
                                error={hasError && this.state.stopWriting}
                                required={required}
                                label={label}
                                variant="outlined"
                                helperText={hasError && this.state.stopWriting ? `${emailValidationError}` : help_text}
                                inputProps={{ name: 'emailValue' }}
                                disabled={disable}
                                value={item}
                                onChange={(e) => this.setEmail(e, index)}
                                onBlur={(e) => this.blur(e, index)}
                            />
                        </Grid>
                        <Grid item xs={1}>
                            <Button disabled={disable} onClick={(e) => this.setEmail(e, index, "removeEmail")}><RemoveCircleOutlineIcon className="small-icon" /></Button>
                        </Grid>
                    </Grid>
                })
            }
        </div>
    }
}