import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';

export default class FreeTextList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { default_value_Array: props.default_value_Array || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            default_value_Array: nextProps.default_value_Array || [],
        };
    }

    setFreeText = (action, value, index) => {
        const { default_value_Array } = this.state;
        switch (action) {
            case "changeValue":
                default_value_Array[index] = value;
                this.setState({ default_value_Array });
                return;
            case "addItemToArray":
                if (default_value_Array.filter(item => item).length < default_value_Array.length) {
                    return;//do not add another textfield if there is an empty one
                }
                default_value_Array.push("");
                break;
            case "removeItemFromArray":
                default_value_Array.splice(index, 1);
                break;
            default:
                break;
        }
        this.setState({ default_value_Array }, this.onblur);
    }

    onblur = () => {
        const filteredArray = this.state.default_value_Array.filter(item => item);
        if (this.state.default_value_Array.length === 1) {
            this.props.updateModel_Array(this.props.field, filteredArray);
            return;
        }
        this.props.updateModel_Array(this.props.field, this.state.default_value_Array);
        //this.props.updateModel_Array(this.props.field, this.state.default_value_Array.filter(item => item));
    }


    render() {
        //const {type}=this.props;
        const { required, label, help_text, className, placeholder="" } = this.props;
        const { default_value_Array } = this.state;
        if (default_value_Array.length === 0) {
            default_value_Array.push("");
        }
        return <  >

            {
                default_value_Array.map((item, index) => {
                    return <Grid key={index} container direction="row" justifyContent="space-between" alignItems="baseline" onBlur={this.onblur}>
                        <Grid item xs={10} sm={10} className="mb2" key={index}>
                            <TextField
                                fullWidth
                                className={className}
                                placeholder={placeholder}
                                //type={type}
                                required={required}
                                id={`standard-required-free-text-name-${label}-${index}`}
                                inputProps={{
                                    // type: "password",
                                    autoComplete: 'new-password'
                                }}
                                label={label}
                                variant="outlined"
                                helperText={help_text}
                                value={item}
                                onChange={(e) => this.setFreeText("changeValue", e.target.value, index)}
                            /></Grid>
                        <Grid item xs={2} sm={2} container direction="row" justifyContent="flex-start" alignItems="baseline" >
                            {(default_value_Array.length - 1) === index ?
                                <Grid item><Tooltip title={"Add another " + label}><Button onClick={() => this.setFreeText("addItemToArray", null, index)}><AddCircleOutlineIcon className="small-icon" /></Button></Tooltip></Grid>
                                : <div style={{ paddingBottom: "1em" }}></div>
                            }
                            {(item !== "") && <Grid item><Tooltip title={"Remove " + label}><Button onClick={() => this.setFreeText("removeItemFromArray", null, index)}><RemoveCircleOutlineIcon className="small-icon" /></Button></Tooltip></Grid>}

                            {(item === "" && index !== 0) &&
                                <Grid item><Tooltip title={"Remove " + label}><Button onClick={() => this.setFreeText("removeItemFromArray", null, index)}><RemoveCircleOutlineIcon className="small-icon" /></Button></Tooltip></Grid>
                            }

                        </Grid>
                    </Grid>
                })
            }

        </ >
    }
}