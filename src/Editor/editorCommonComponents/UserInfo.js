import React from "react";
import { Alert, AlertTitle } from '@material-ui/lab';
import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
//import Button from '@material-ui/core/Button';
//import CloseIcon from '@material-ui/icons/Close';
import CancelIcon from '@material-ui/icons/Cancel';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';

export default class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: true }
    }

    getRequired = (messages, required) => {
        if (!required || required.length === 0) {
            return messages;
        }
        messages.push(<div>
            The following elements are required in order for you to be able to submit your record:
        {
                required.map(item => <span key={item}>
                    <Chip size="small" label={item} className="ChipTagLicence" />
                </span>)
            }
        </div>)
    }

    saveDraftInfo = (messages) => {
        messages.push(<div className="mt1">At any point you are able to save your progress as draft and continue editing at a later time</div>);
        messages.push(<div className="mt1">Once you submit your record you will not be able to save it as draft any more, but you will still be able to make changes and submit them.</div>);
        messages.push(<div className="mt1">Visit all tabs in order to fill in as much information as possible for better visibility of your record.</div>);
        !this.props.hide_under_construction && messages.push(<div className="mt1">If the metadata record is for a resource that you plan to deliver later, please check the "work in progress" box.</div>)
    }

    render() {
        const messages = [];
        const required = this.props.requiredFields;
        this.getRequired(messages, required);
        this.saveDraftInfo(messages);
        //const model = this.props.model.described_entity;
        return <div>
            <Collapse in={this.state.open}>
                <Alert
                    //variant="standard"
                    //variant="outlined"
                    //variant="filled"
                    severity="info"
                    action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                                this.setState({ open: false })
                            }}
                        >
                            <CancelIcon />
                        </IconButton>
                    }
                >
                    <AlertTitle>Info</AlertTitle>
                    <ol>
                        {messages.length > 0 && messages.map((item, index) => {
                            return <li key={index}>{item}</li>
                        })}
                    </ol>
                </Alert>
            </Collapse>
            {!this.state.open && <LiveHelpIcon onClick={(e) => { this.setState({ open: true }) }} />}
        </div>
    }
}