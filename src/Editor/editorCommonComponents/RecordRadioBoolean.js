import React from "react";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';

export default class RecordRadioBoolean extends React.Component {
    //shouldComponentUpdate(nextProps, nextState) {
    //    return this.props.default_value !== nextProps.default_value;
    //}
    render() {
        const { required, label, help_text, default_value, disabled } = this.props;
        const default_value_boolean = (default_value === null || default_value === undefined) ? "" : default_value;
        return <div className="mb1">
            <FormControl component="fieldset" required={required} disabled={disabled}>
                <FormLabel component="legend">{label}</FormLabel>
                <RadioGroup aria-label={label} name="gender1" value={default_value_boolean} onChange={this.props.handleBooleanChange}>
                    <FormControlLabel value={true} control={<Radio />} label="Yes" />
                    <FormControlLabel value={false} control={<Radio />} label="No" />
                    <FormHelperText>{help_text}</FormHelperText>
                </RadioGroup>
            </FormControl>
        </div>
    }
}