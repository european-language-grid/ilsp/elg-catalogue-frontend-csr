import React from "react";
import DataComponent from "./DataComponent";
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
//import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
//import RecordRadioBoolean from "./RecordRadioBoolean";
//import Typography from '@material-ui/core/Typography';
//import messages from "../../config/messages";
import Tooltip from '@material-ui/core/Tooltip';

export default class IsFunctionalService extends React.Component {

    constructor(props) {
        super(props);
        this.state = { buttonText: "skip" };
    }

    handleBooleanChange = (e) => {
        this.props.hideDataComponentDialogueWraper();
    }

    updateButtonText = (msg) => {
        this.setState({ buttonText: msg });
    }

    render() {
        return <div>
            <Dialog open={true} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="xl" disableBackdropClick={true} fullWidth={false}>
                <DialogTitle>Please upload your datasets for this record.</DialogTitle>
                <DialogContent>
                    {/* <DialogContentText component={"div"}>Please upload your datasets for this record.</DialogContentText>*/}
                </DialogContent>
                <DialogContent>
                    <DataComponent {...this.props} updateButtonText={this.updateButtonText} />
                </DialogContent>
                <DialogActions>
                    <Tooltip title="You can upload yor data at a later step">
                        <Button className="btn-remove" color="primary" onClick={() => this.handleBooleanChange()}>
                            {this.state.buttonText}
                        </Button>
                    </Tooltip>
                </DialogActions>
            </Dialog>
        </div>
    }
}