import React from "react";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import euLanguages from "../../data/OfficialEuLanguages.json";
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';

export default class EuLanguagesSelection extends React.Component {

    setValues = (event, newValue, defaultLangObj) => {
        const languageKey = newValue ? newValue.key || "" : "";
        if (!languageKey) {
            return;
        }
        this.props.setValues(event, this.props.languageIndex, "changeLanguage", languageKey, defaultLangObj, this.props.objectIndex);
    }

    render() {
        const { lang, languageIndex, disabledLanguagesArray, className, disable } = this.props;
        const disabledLanguagesArraySlice = languageIndex - 1 >= 0 ? disabledLanguagesArray.slice(0, languageIndex) : [];
        const languages = Object.keys(euLanguages).map((key) => { return { [key]: euLanguages[key], key: key, humanLabel: euLanguages[key] } });
        const defaultLangArray = languages.filter(item => item.key === lang);
        const defaultLangObj = lang && defaultLangArray.length > 0 ? defaultLangArray[0] : { key: "", humanLabel: "" };
        return <>
            <Autocomplete
                className={className}
                disabled={disable || defaultLangObj.key === "en"}
                autoHighlight
                value={defaultLangObj}
                onChange={(event, newValue) => {
                    this.setValues(event, newValue, defaultLangObj)
                }}
                options={languages}
                getOptionLabel={(option) => option.humanLabel || ""}
                getOptionSelected={(option, value) => option.key === value.key}
                getOptionDisabled={(option) => disabledLanguagesArraySlice.filter(item => item === option.key).length > 0}
                renderInput={(params) => (
                    <TextField {...params} error={defaultLangObj.key === ""} label="language" variant="outlined" helperText={this.props.showHelpText ? "select language" : ''} />
                )}
                renderOption={(option, { inputValue }) => {
                    const matches = match(option.humanLabel, inputValue);
                    const parts = parse(option.humanLabel, matches);
                    return (
                        <div>
                            {parts.map((part, index) => (
                                <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                    {part.text}
                                </span>
                            ))}
                        </div>
                    );
                }}
            />
        </>
    }
}