import React from "react";
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';

export default class RecordSelectList extends React.Component {

  render() {
    const initialValue = this.props.initialValue ? (this.props.initialValue.file.substring(this.props.initialValue.file.lastIndexOf("/") + 1)) : "";
    const choices = this.props.datasets;

    return <div className="mb2"><span>
      <FormControl variant="outlined" className={this.props.className}>
        {<InputLabel htmlFor="dataset">{this.props.service_tool ? "Associate a package with this distribution" : "Associate a dataset with this distribution"}</InputLabel>}
        <Select
          id={`dataset${this.props.index}`}
          variant="outlined"
          native
          value={initialValue || ""}
          onChange={(e) => { this.props.setSelectedvalue(choices.filter(item => item.file.substring(item.file.lastIndexOf("/") + 1) === e.target.value)) }}
          label={this.props.service_tool ? "Associate a package with this distribution" : "Associate a dataset with this distribution"}
          inputProps={{ name: 'dataset' }}
        >
          <option aria-label="None" value="" />
          {choices.map((item, index) => <option key={index} value={item.file.substring(item.file.lastIndexOf("/") + 1)}>{item.file.substring(item.file.lastIndexOf("/") + 1)}
          </option>)}
        </Select>
        {<FormHelperText>{this.props.service_tool ? "Associate a package with this distribution" : "Associate a dataset with this distribution"}</FormHelperText>}
      </FormControl>
    </span>
    </div>
  }
}