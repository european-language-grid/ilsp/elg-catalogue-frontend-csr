import React from "react";
//import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import EulanguageSelection from "./EuLanguagesSelection";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";
//import sanitizeHtml from 'sanitize-html';
import Typography from '@material-ui/core/Typography';

import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@blowstack/ckeditor5-full-free-build";
//here is a list of all the plugins, to use the full stack CKEditor we remove plugins that we don't want to use 
//['Alignment', 'Autoformat', 'AutoLink', 'BlockQuote', 'Bold', 'Paragraph','Underline',
//'CloudServices', 'Code', 'CodeBlock', 'Essentials', 'FontBackgroundColor', 
//'FontColor', 'FontFamily', 'FontSize', 'Heading', 'Highlight', 'HorizontalLine', 
//'HtmlEmbed', 'Image', 'ImageCaption', 'ImageInsert', 'ImageResize', 'ImageStyle', 'ImageToolbar', 'ImageUpload', 'MediaEmbed', 'LinkImage','AutoImage', 
//'Indent', 'IndentBlock', 'Italic', 'Link',  'List', 'ListStyle', 'MathType', 
//'MediaEmbedToolbar', 'PageBreak',  'PasteFromOffice', 'RemoveFormat', 'SimpleUploadAdapter', 'SpecialCharacters', 'SpecialCharactersArrows', 
//'SpecialCharactersCurrency', undefined, 'SpecialCharactersLatin', 'SpecialCharactersMathematical', 'SpecialCharactersText', 'Strikethrough', 'Subscript', 'Superscript', 
//'Table', 'TableCellProperties', 'TableProperties', 'TableToolbar', 'TextTransformation', 'Title', 'TodoList',  'WordCount']

const editorConfiguration = {
    removePlugins: [ 'Title', 'FontSize', 'FontFamily','FontBackgroundColor',  'Superscript',
    'MediaEmbedToolbar', 'ImageToolbar','CloudServices', 'MathType','PageBreak',  'PasteFromOffice','TodoList',], // Remove a few plugins from the default setup.
    
    //plugins: [ 'Table', 'TableToolbar', 'TableProperties', 'TableCellProperties', 'BlockQuote', 'Bold', 'Italic', 'Paragraph','Underline','Code', 'CodeBlock',
    //'Heading', 'Alignment','Indent', 'Autoformat', 'Link',  'List', 'ListStyle','Strikethrough', 'Subscript', 'Essentials'] ,

    toolbar: { items: [  'heading', 'bold', 'italic', 'underline' , 'strikethrough', 'subscript', 'blockQuote', 'link', '|', 'insertTable','|', 'undo', 'redo','|', 'numberedList', 'bulletedList', 'outdent', 'indent', '|','code', 'codeBlock', '|', ] } ,

    shouldNotGroupWhenFull: true,  //Set the shouldNotGroupWhenFull option to true, so items will not be grouped when the toolbar overflows

    link: {
        addTargetToExternalLinks: true
    },

    codeBlock: {
        languages: [
            { language: 'plaintext', label: 'Plain text' }, // The default language.
            //{ language: 'c', label: 'C' },
            //{ language: 'cs', label: 'C#' },
            //{ language: 'cpp', label: 'C++' },
            //{ language: 'css', label: 'CSS' },
            { language: 'diff', label: 'Diff' },
            //{ language: 'html', label: 'HTML' },
            //{ language: 'java', label: 'Java' },
            //{ language: 'javascript', label: 'JavaScript' },
            //{ language: 'php', label: 'PHP' },
            { language: 'python', label: 'Python' },
            //{ language: 'ruby', label: 'Ruby' },
            //{ language: 'typescript', label: 'TypeScript' },
            { language: 'xml', label: 'XML' }
        ]
    } ,
    heading: {
        options: [
            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
            { model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' }
        ]
    }


};


export default class LanguageSpecificTextRichEditor extends React.Component {

    constructor(props) {
        super(props);
        this.state = { stateObj: this.props.defaultValueObj ? this.props.defaultValueObj : { "en": "" }, errorObj: { "en": "" } };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            stateObj: nextProps.defaultValueObj ? nextProps.defaultValueObj : { "en": "" },
            errorObj: { "en": "" }
        };
    }

    onChangeInEditor = (event, editor, langIndex, languageKey, oldLanguageValueObj) => {
        const data = editor.getData();
        const obj = this.state.stateObj;
        obj[languageKey] = data;
        //const clean = sanitizeHtml(data, {
        //    allowedTags: ['a', 'li', 'ol', 'p', 'ul', 'b', 'i',  'u', 'strong', 'em', 'code', 'hr','blockquote','pre'],
        //    allowedAttributes: {
        //        'a': ['href', 'name', 'target']
        //    },
       //     allowedSchemes: ['http', 'https'],
        //});
        //console.log(clean);
        this.setState({ stateObj: obj })
    }


    setValues = (event, langIndex, action2Perform, languageKey, oldLanguageValueObj) => {

        const obj = this.state.stateObj;
        const action = event.target.name || action2Perform;

        switch (action) {
            //case "textValue":
            //    obj[languageKey] = event.target.value;
            //    break;
            case "changeLanguage":
                if (Object.keys(obj).includes(languageKey)) {
                    return;
                }
                const oldkey = oldLanguageValueObj.key;
                delete Object.assign(obj, { [languageKey]: obj[oldkey] })[oldkey];
                break;
            case "addLanguage":
                if (Object.values(obj).filter(val => val).length !== Object.values(obj).length) {
                    return; //do not add language field if there is an empty value field
                }
                if (Object.keys(obj).filter(val => val).length !== Object.keys(obj).length) {
                    return; //do not add language field if there is an empty language key
                }
                obj[""] = "";
                break;
            case "removeLanguage":
                delete obj[languageKey];
                break;
            default: return;
        }
        this.setState({ stateObj: obj });
    }

    onBlur = () => {
        const { errorObj } = this.state;
        this.props.required && Object.keys(this.state.stateObj).forEach(key => this.state.stateObj[key] ? errorObj[key] = "" : errorObj[key] = "Required field");

        if (!this.state.stateObj["en"]) {
            this.props.setLanguageSpecificText(this.props.field, null);
            return;
        }
        this.props.setLanguageSpecificText(this.props.field, this.state.stateObj);
        this.props.onDescriptionChange();
    }

    render() {
        //const { field,defaultValueObj, allowMultipleLanguages = false } = this.props;         
        const {
            //type, required, multiline, rowsMax,  placeholder, 
            label, help_text, className, disable,
        } = this.props;
        // const defaultValuesArray = Object.values(this.state.stateObj) || [];
        const defaultLangArray = Object.keys(this.state.stateObj) || ["en"];
        const disabledLanguagesArray = [];

        return <div onBlur={this.onBlur}>
            <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
            {defaultLangArray.map((lang, langIndex) => {
                disabledLanguagesArray.push(lang);
                return <div className="inline--container" key={langIndex}>
                    <span className="wd-100">
                        <CKEditor
                            editor={ClassicEditor}
                            id={`${label}-${lang}-${langIndex}`}
                            config={editorConfiguration}
                            data={this.state.stateObj[lang] || ""}
                            onReady={(editor) => {
                                // You can store the "editor" and use when it is needed.
                                //console.log(ClassicEditor.builtinPlugins.map( plugin => plugin.pluginName ));
                            }}

                            onChange={(event, editor) => this.onChangeInEditor(event, editor, langIndex, lang)}

                        />
                    </span>
                    <span className="pl10 wd-60">
                        <EulanguageSelection className={className} showHelpText={help_text ? true : false} lang={lang} languageIndex={langIndex} disable={disable} setValues={this.setValues} disabledLanguagesArray={disabledLanguagesArray} />
                    </span>
                    {langIndex === 0 && <Button disabled={disable} onClick={(e) => { this.setValues(e, langIndex, "addLanguage") }}><Tooltip title={messages.multilingual_elements_add_icon_hover}><AddCircleOutlineIcon className="small-icon" /></Tooltip></Button>}
                    {lang !== "en" && <Button disabled={disable} onClick={(e) => { this.setValues(e, langIndex, "removeLanguage", lang) }}><Tooltip title={messages.multilingual_elements_remove_icon_hover}><RemoveCircleOutlineIcon className="small-icon" /></Tooltip></Button>}

                     {/*<div>{(sanitizeHtml(this.state.stateObj[lang], {
                        allowedTags: ['a', 'li', 'ol', 'p', 'ul', 'b', 'i', 'u', 'strong', 'em', 'code',
                        'hr', 'blockquote', 'pre', 'h3', 'h2', 'br',
                        'sub', 'strike', 'caption', 'table', 'tbody','thead', 'th', 'td', 'tr'],
                        allowedAttributes: {
                            'a': ['href', 'name', 'target']
                        },
                        allowedSchemes: ['http', 'https'],
                    }) || "")}</div>*/}
                    {/*<div dangerouslySetInnerHTML={{
                        __html: sanitizeHtml(this.state.stateObj[lang], {
                            allowedTags: ['a', 'li', 'ol', 'p', 'ul', 'b', 'i', 'u', 'strong', 'em', 'code',
                            'hr', 'blockquote', 'pre', 'h3', 'h2', 'br',
                            'sub', 'strike', 'caption', 'table', 'tbody','thead', 'th', 'td', 'tr'],
                            allowedAttributes: {
                                'a': ['href', 'name', 'target']
                            },
                            allowedSchemes: ['http', 'https'],
                        })
                    }} />*/}
                </div>

            })}

        </div>
    }
}