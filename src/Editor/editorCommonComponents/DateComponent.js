import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers';
//import { KeyboardDatePicker } from '@material-ui/pickers';

export default function DateComponent(props) {
    const [selectedDate, setSelectedDate] = React.useState(props.initialValue ? new Date(props.initialValue) : null);

    const getMonthFromString = (mon) => {
        var d = Date.parse(mon + "1, 2012");
        if (!isNaN(d)) {
            return new Date(d).getMonth() + 1;
        }
        return -1;
    }

    const handleDateChange = (date) => {
        if (!date) {
            props.updateModel(props.field, null);
            return;
        }
        if (JSON.stringify(date).indexOf("Invalid Date") >= 0) {
            props.updateModel(props.field, null);
            return;
        }
        try {
            const [, month, day, year] = date.toString().split(" ");
            const monthIndex = getMonthFromString(month);
            if (year >= 0 && monthIndex >= 0 && day >= 0) {
                const assembledDate = `${year}-${(monthIndex + "").length === 2 ? monthIndex : "0" + monthIndex}-${day}`;
                props.updateModel(props.field, assembledDate);
            } else {
                props.updateModel(props.field, null);
            }
        } catch (err) {
        }
        setSelectedDate(date);
    };

    React.useEffect(() => {
        setSelectedDate(props.initialValue);
    }, [props.initialValue]);

    return <div>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
                className={props.className}
                clearable
                //autoOk
                inputVariant="outlined"
                views={["year", "month", "date"]}
                format="yyyy-MM-dd"
                placeholder="yyyy-mm-dd"
                minDate={props.minDate ? props.minDate : "1900-01-01"}
                maxDate={props.maxDate ? props.maxDate : "2100-01-01"}
                label={props.label}
                helperText={props.help_text}
                value={selectedDate}
                onChange={date => handleDateChange(date)}
            />
        </MuiPickersUtilsProvider>
    </div>
}