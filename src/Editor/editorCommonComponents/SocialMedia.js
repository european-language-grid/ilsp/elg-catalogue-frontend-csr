import React from "react";
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import Grid from '@material-ui/core/Grid';
import messages from "./../../config/messages";

const social_media_obj = {
    "social_media_occupational_account_type": "", "value": ""
};

export default class SocialMedia extends React.Component {

    constructor(props) {
        super(props);
        this.state = { socialMedia: this.props.default_valueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            socialMedia: nextProps.default_valueArray || [],
        };
    }


    setSelectSocialMediaAccount = (action, selectedValue, index) => {
        const { socialMedia } = this.state;
        var oldSocialObj = socialMedia[index];
        switch (action) {
            case "selectvalue": oldSocialObj = { ...oldSocialObj, social_media_occupational_account_type: (selectedValue && selectedValue.length > 0) ? selectedValue[0].value : null }; break;
            case "textvalue": oldSocialObj = { ...oldSocialObj, value: selectedValue }; break;
            case "addSocialMediaItem":
                if (socialMedia.filter(item => item.value && item.social_media_occupational_account_type).length < socialMedia.length) {
                    return; //do not add item if there is an empty social media 
                }
                socialMedia.push(JSON.parse(JSON.stringify(social_media_obj)));
                this.setState({ socialMedia });
                return;
            case "removeSocialMediaItem":
                socialMedia.splice(index, 1);
                this.setState({ socialMedia }, this.onBlur);
                return;
            default: break;
        }
        socialMedia[index] = oldSocialObj;
        this.setState({ socialMedia });
        //this.props.updateModel_SocialMedia('social_media_occupational_account', socialMedia);
    }

    onBlur = () => {
        const { socialMedia } = this.state;
        const filteredArray = socialMedia.filter(item => item.value || item.social_media_occupational_account_type);
        this.props.updateModel_SocialMedia((this.props.field !== null && this.props.field !== undefined) ? this.props.field : 'social_media_occupational_account', filteredArray);
        //this.props.updateModel_SocialMedia((this.props.field !== null && this.props.field !== undefined) ? this.props.field : 'social_media_occupational_account', this.state.socialMedia);
    }

    render() {
        const {
            help_text, label,
            social_media_required, social_media_label, social_media_choices, social_media_help_text,
            value_label, value_required, value_max_length, value_type, value_placeholder, value_help_text
        } = this.props;

        //if (this.state.socialMedia.length === 0) {
        //    this.state.socialMedia.push(JSON.parse(JSON.stringify(social_media_obj)));
        //}
        const default_valueArray = this.state.socialMedia;


        return <div className="pb-3 inner--group nested--group" >
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {default_valueArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setSelectSocialMediaAccount("addSocialMediaItem", null, 0)}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {
                default_valueArray.map((social, index) => {
                    const default_value_display_name = social_media_choices.filter(item => item.value === social.social_media_occupational_account_type).length > 0 ? social_media_choices.filter(item => item.value === social.social_media_occupational_account_type)[0].display_name : "";
                    return <div key={index}>
                        <Grid container spacing={1} direction="row" justifyContent="space-between" alignItems="baseline" >
                            <Grid item xs={4}>
                                <FormControl variant="outlined" required={social_media_required} className="wd-100">
                                    <InputLabel htmlFor="select-native-simple">{social_media_label}</InputLabel>
                                    <Select
                                        onBlur={this.onBlur}

                                        native
                                        value={default_value_display_name}
                                        onChange={(e) => { this.setSelectSocialMediaAccount("selectvalue", social_media_choices.filter(item => item.display_name === e.target.value), index) }}
                                        label={social_media_label}
                                    >
                                        <option aria-label="None" value="" />
                                        {social_media_choices.map((item, index) => <option key={index} value={item.display_name}>{item.display_name}</option>)}
                                    </Select>
                                    <FormHelperText>{social_media_help_text}</FormHelperText>
                                </FormControl></Grid>

                            <Grid item xs={6}>
                                <TextField className="wd-100" onBlur={this.onBlur} variant="outlined" placeholder={value_placeholder ? value_placeholder : ""} required={value_required} label={value_label} helperText={value_help_text} type={value_type} maxLength={value_max_length} value={social.value || ""} onChange={(e) => this.setSelectSocialMediaAccount("textvalue", e.target.value, index)} />
                            </Grid>

                            <Grid item xs={2} container direction="row" justifyContent="flex-start" alignItems="baseline" >
                                {((default_value_display_name !== "" || social.social_media_occupational_account_type !== "") && ((default_valueArray.length - 1) === index)) ?
                                    <Grid item><Button onClick={(e) => this.setSelectSocialMediaAccount("addSocialMediaItem")}><AddCircleOutlineIcon className="small-icon" /></Button></Grid>
                                    :
                                    <Grid item><span></span></Grid>
                                }
                                <Grid item>{<Button onClick={(e) => this.setSelectSocialMediaAccount("removeSocialMediaItem", e, index)}><RemoveCircleOutlineIcon className="small-icon" /></Button>}</Grid>
                            </Grid>

                        </Grid>
                    </div>
                })
            }
        </div>
    }
}