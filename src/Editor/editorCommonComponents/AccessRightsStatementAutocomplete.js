import React from "react";
import axios from "axios";
//import CircularProgress from '@material-ui/core/CircularProgress';
import { LOOK_UP_ACCESS_RIGHTS_STATEMENT } from "../../config/editorConstants";
import { access_rights_obj, access_rights_statement_identifier_obj } from "../Models/LrModel";
import LanguageSpecificText from "./LanguageSpecificText";
import IdentifierSingle from "./IdentifierSingle";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CircularProgress from "@material-ui/core/CircularProgress";
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import messages from "../../config/messages";
//const filter = createFilterOptions();

export default class AccessRightsStatementAutocomplete extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue || [], source: null, autocompleteLoading: false, options: [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue || [],
        };
    }

    setValue = (action, index, newValue) => {
        let initialValue = this.state.initialValue;
        switch (action) {
            case "fill_in_object":
                const filteredArray = initialValue.filter(item => {
                    if (item && item.category_label) {
                        return item.category_label["en"];
                    }
                    return false;
                });
                if (filteredArray.length !== initialValue.length) {
                    return;//do not add element if there are empty values
                }
                initialValue.push(JSON.parse(JSON.stringify(access_rights_obj)));
                break;
            case "remove_object":
                initialValue.splice(index, 1);
                break;
            case "setCategory":
                initialValue[index].category_label = newValue;
                if (initialValue[index].hasOwnProperty('editor-placeholder')) {
                    delete initialValue[index]['editor-placeholder'];
                }
                break;
            case "autoCompleteSelected":
                if (newValue.similarity) {
                    delete newValue["similarity"];
                }
                if (newValue.display_name) {
                    delete newValue["display_name"];
                }
                if (newValue.hasOwnProperty('editor-placeholder')) {
                    delete newValue['editor-placeholder'];
                }
                initialValue[index] = newValue;
                this.setState({ initialValue, options: [] });
                break;
            default:
                break;
        }
        this.setState({ initialValue: initialValue, options: [] }, this.onBlur);
        this.props.setModelField(this.props.field, initialValue);
    }

    set_category_label = (index, value) => {
        const { initialValue } = this.state;
        initialValue[index].category_label = value;
        if (initialValue[index].hasOwnProperty('editor-placeholder')) {
            delete initialValue[index]['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }

    update_identifier = (index, value) => {
        const { initialValue } = this.state;
        initialValue[index].access_rights_statement_identifier = value;
        if (initialValue[index].hasOwnProperty('editor-placeholder')) {
            delete initialValue[index]['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur());
    }

    onBlur = () => {
        this.setState({ options: [], autocompleteLoading: false });
        this.props.setModelField(this.props.field, this.state.initialValue);
    }


    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    getChoices = (value) => {
        if (value.trim().length <= 2) {
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, autocompleteLoading: true });
        axios.get(LOOK_UP_ACCESS_RIGHTS_STATEMENT(value), { cancelToken: source.token }).then(res => {
            this.setState({ options: res.data, source: null, autocompleteLoading: false });
        }).catch((err) => {
            console.log(err);
            this.setState({ source: null, autocompleteLoading: false, options: [] });
        });
    }

    autocompleteTextField = (itemIndex) => {
        const choices = this.props.formElements.access_rights_statement_identifier.formElements.access_rights_statement_scheme.choices;
        const activeLanguage = this.props.activeLanguage || "en";
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.autocompleteLoading}
                value={this.state.initialValue || ""}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValue("autoCompleteSelected", itemIndex, newValue);
                    } else {
                        this.setValue("setCategory", itemIndex, { "en": newValue })
                    }
                }}
                options={this.state.autocompleteLoading ? [] : this.state.options}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.autocompleteLoading) {
                        if (params.inputValue !== '') {
                            filtered.splice(0, 0, {
                                category_label: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.autocompleteLoading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.category_label ? Object.values(option.category_label)[0] : ""}
                getOptionLabel={(option) => option.category_label ? Object.keys(option.category_label).includes("en") ? option.category_label["en"] : Object.values(option.category_label)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={this.props.formElements.category_label.label} helperText={this.props.formElements.category_label.help_text} placeholder={this.props.formElements.category_label.placeholder ? this.props.formElements.category_label.placeholder : ""} variant="outlined"
                        onChange={(e) => { this.getChoices(e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option.category_label)[0], inputValue);
                    //const parts = parse(Object.values(option.category_label)[0], matches);
                    const matches = match(option.category_label[activeLanguage] || Object.values(option.category_label)[0], inputValue);
                    const parts = parse(option.category_label[activeLanguage] || Object.values(option.category_label)[0], matches);
                    return (
                        <div>
                            {
                                option.category_label && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                            {
                                option.access_rights_statement_identifier &&
                                <div >
                                    {
                                        (`${option.access_rights_statement_identifier.value} - ${choices.filter(accessRightsChoice => accessRightsChoice.value === option.access_rights_statement_identifier.access_rights_statement_scheme)[0].display_name}`)
                                    }
                                </div>

                            }
                        </div>
                    );
                }
                }
            />
        </span>
    }

    render() {
        return <div onBlur={this.onBlur}>
            <div className="pb-3 inner--group nested--group">
                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                    <Grid item sm={11}>
                        <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                        <Typography className="section-links" >{this.props.help_text} </Typography>
                    </Grid>
                    <Grid item sm={1}>
                        {this.state.initialValue && this.state.initialValue.length === 0 && <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setValue("fill_in_object")}>{messages.group_elements_create}</Button>}
                        {/*this.state.initialValue && <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setValue("remove_object")}>{messages.group_elements_remove}</Button>*/}
                    </Grid>
                </Grid>
                {this.state.initialValue.map((item, itemIndex) => {
                    const hide_help_text = (item && item.hasOwnProperty("pk")) ? true : false;
                    return <div key={itemIndex}>
                        <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                            <Grid item><Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setValue("remove_object", itemIndex)}>{messages.group_elements_remove}</Button>
                            </Grid>
                        </Grid>
                        {item && item.hasOwnProperty("editor-placeholder") && (item.category_label && !Object.values(item.category_label)[0]) ?
                            this.autocompleteTextField(itemIndex)
                            : <div>
                                <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                    <Grid item>
                                        {/*<Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", arrayItemIndex)}>{"Remove"}</Button>*/}
                                    </Grid>
                                </Grid>

                                <LanguageSpecificText
                                    {...this.props.formElements.category_label}
                                    help_text={hide_help_text ? '' : this.props.formElements.category_label.help_text}
                                    defaultValueObj={item.category_label || { "en": "" }}
                                    disable={item.pk >= 0 ? true : false}
                                    field={itemIndex}//don't care for field name track index instead
                                    setLanguageSpecificText={this.set_category_label}
                                />

                                <Grid container spacing={1}>
                                    <Grid item xs={item.hasOwnProperty("pk") ? 12 : 9}>
                                        {
                                            (item.hasOwnProperty("pk") && (!item.access_rights_statement_identifier || !item.access_rights_statement_identifier.access_rights_statement_scheme)) ? <></> :
                                                <div>
                                                    <IdentifierSingle
                                                        //initialIdentifierObj={item.access_rights_statement_identifier || JSON.parse(JSON.stringify(access_rights_statement_identifier_obj))}
                                                        initialIdentifierObj={item.access_rights_statement_identifier || {}}
                                                        field={itemIndex}//access_rights_statement_identifier //track index instead of field name
                                                        Identifier_Label={this.props.formElements.access_rights_statement_identifier.label}
                                                        Identifier_help_text={hide_help_text ? '' : this.props.formElements.access_rights_statement_identifier.help_text}
                                                        identifier_scheme_choices={this.props.formElements.access_rights_statement_identifier.formElements.access_rights_statement_scheme.choices}
                                                        identifier_scheme_help_text={hide_help_text ? '' : this.props.formElements.access_rights_statement_identifier.formElements.access_rights_statement_scheme.help_text}
                                                        identifier_scheme_label={this.props.formElements.access_rights_statement_identifier.formElements.access_rights_statement_scheme.label}
                                                        identifier_scheme_read_only={this.props.formElements.access_rights_statement_identifier.formElements.access_rights_statement_scheme.read_only}
                                                        identifier_scheme_required={this.props.formElements.access_rights_statement_identifier.formElements.access_rights_statement_scheme.required}
                                                        identifier_scheme_type={""}
                                                        identifier_value_label={this.props.formElements.access_rights_statement_identifier.formElements.value.label}
                                                        identifier_value_required={this.props.formElements.access_rights_statement_identifier.formElements.value.required}
                                                        identifier_value_type={""}
                                                        identifier_value_placeholder={hide_help_text ? '' : this.props.formElements.access_rights_statement_identifier.formElements.value.placeholder ? this.props.formElements.access_rights_statement_identifier.formElements.value.placeholder : ""}
                                                        identifier_value_read_only={this.props.formElements.access_rights_statement_identifier.formElements.value.read_only}
                                                        identifier_value_helptext={hide_help_text ? '' : this.props.formElements.access_rights_statement_identifier.formElements.value.help_text}
                                                        classification_scheme="access_rights_statement_scheme"
                                                        scheme_choice_to_hide="ELG"
                                                        scheme_choice_uri_to_hide="http://w3id.org/meta-share/meta-share/elg"
                                                        disable={item.hasOwnProperty("pk") ? true : false}
                                                        identifier_obj={access_rights_statement_identifier_obj}
                                                        updateModel_Identifier={this.update_identifier}
                                                    />
                                                </div>
                                        }
                                    </Grid>
                                </Grid>
                                <div className="pt-3 form-block-area">
                                    {
                                        ((item.category_label && item.category_label["en"]) && ((this.state.initialValue.length - 1) === itemIndex)) ?
                                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValue("fill_in_object")}>{messages.array_elements_add}</Button></Grid>
                                            </Grid>
                                            : <div style={{ paddingBottom: "1em" }}></div>
                                    }
                                </div>
                            </div>
                        }
                    </div>
                })}
            </div>
        </div>
    }

}