import React from "react";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import Chip from '@material-ui/core/Chip';
const filter = createFilterOptions();

export default class LtArea extends React.Component {

    constructor(props) {
        super(props);
        this.state = { ltAreaArray: this.props.default_valueArray };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            ltAreaArray: nextProps.default_valueArray || [],
        };
    }

    setValues = (action, arrayIndex, newValue) => {
        const { ltAreaArray } = this.state;
        switch (action) {
            case "onchange":
                ltAreaArray.length = 0;
                const a = newValue.map(item => item.value || item);
                for (let index = 0; index < a.length; index++) {
                    let item2Add = a[index];
                    ltAreaArray.push(item2Add);
                    //console.log(ltAreaArray);
                }
                break;
            default: break;
        }
        this.setState({ ltAreaArray });
        this.onBlur();
    }

    onBlur = () => {
        this.props.updateModel_ltArea("lt_area", this.state.ltAreaArray);
    }

    renderAutocomplete = (default_valueArray) => {
        const initialValues = default_valueArray.map(inititalUrl => {
            let filteredArray = this.props.recommended_choices.filter(recommended => recommended.value === inititalUrl);
            if (filteredArray.length > 0) {
                return filteredArray[0].display_name;
            }
            return inititalUrl;
        })
        return <div onBlur={this.onBlur}><Grid container direction="row" alignItems="center" justifyContent="flex-start">
            <Grid item sm={12}>
                <Autocomplete
                    multiple
                    selectOnFocus
                    handleHomeEndKeys
                    freeSolo
                    autoHighlight
                    clearOnBlur={true}
                    blurOnSelect={true}
                    value={initialValues}
                    onChange={(event, newValue) => {
                        newValue = newValue.map(item => this.props.recommended_choices.filter(recommended_choice => recommended_choice.display_name === item).length > 0 ? this.props.recommended_choices.filter(recommended_choice => recommended_choice.display_name === item)[0] : item);
                        this.setValues("onchange", null, newValue)
                    }}
                    options={this.props.recommended_choices}
                    filterOptions={(options, params) => {
                        const filtered = filter(options, params);
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                value: params.inputValue,
                                display_name: `Missing ${this.props.label}? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                value: params.inputValue,
                                display_name: `Missing ${params.inputValue} ? Add "${params.inputValue}"`,
                            });
                        }
                        return filtered;
                    }}
                    getOptionLabel={(option) => {
                        const ar = this.props.recommended_choices.filter(recommended_choice => recommended_choice.value === option.value);
                        if (ar.length > 0) {
                            return ar[0].display_name;
                        }
                        if (option.display_name) {
                            return option.display_name;
                        }
                        return option;
                    }}
                    renderInput={(params) => (
                        <TextField {...params} label={this.props.label} helperText={this.props.help_text} variant="outlined" placeholder={this.props.placeholder ? this.props.placeholder : ""} />)}
                    renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                            <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                        ))
                    }
                    renderOption={(option, { inputValue }) => {
                        const matches = match(option.display_name, inputValue);
                        const parts = parse(option.display_name, matches);
                        return (
                            <div>
                                {parts.map((part, index) => (
                                    <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                        {part.highlight ? '\u00A0' : ''}{part.text}
                                    </span>
                                ))}
                            </div>
                        );
                    }
                    }
                />
            </Grid>
        </Grid>
        </div>
    }


    render() {
        const default_valueArray = this.state.ltAreaArray;
        return <div className="mb1" onBlur={this.onBlur}>
            <div>
                {this.renderAutocomplete(default_valueArray)}
            </div>
        </div>
    }


}