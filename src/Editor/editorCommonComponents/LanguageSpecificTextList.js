import React from "react";
//import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import CreateIcon from '@material-ui/icons/Create';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/task-checklist-add.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/task-checklist-remove.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";

import LanguageSpecificText from "./LanguageSpecificText";
//import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';

export default class LanguageSpecificTextList extends React.Component {

    constructor(props) {
        super(props);
        this.state = { stateArray: this.props.default_valueArray || [], errorArray: [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            stateArray: nextProps.default_valueArray || [],
            errorArray: []
        };
    }


    setValues = (event, arrayItemIndex, action2Perform) => {
        const array = this.state.stateArray;
        const action = event.target.name || action2Perform;
        switch (action) {
            case "addArrayItem":
                if (array.filter(item => Object.values(item)[0]).length !== array.length) {
                    return;//do not add array item if there is an empty value
                }
                if (array.filter(item => Object.keys(item)[0]).length !== array.length) {
                    return;//do not add array item if there is an empty key value 
                }
                array.push({ "en": "" });
                break;
            case "removeArrayItem":
                array.splice(arrayItemIndex, 1);
                break;
            default: return;
        }
        this.setState({ stateArray: array }, this.onBlur);
    }

    setLanguageSpecificText = (arrayItemIndex, obj) => {
        if (obj === null) {
            return;
        }
        const array = this.state.stateArray;
        array[arrayItemIndex] = obj;
        this.setState({ stateArray: array });
        this.onBlur();
    }

    onBlur = () => {
        //let array = this.state.stateArray.filter(item => Object.values(item)[0] && Object.keys(item)[0]);
        //this.props.setLanguageSpecifictextList(this.props.field, array);
        if (this.state.stateArray && this.state.stateArray.length === 1) {
            const element = this.state.stateArray[0];
            if (element && element.hasOwnProperty("en") && !element["en"]) {
                this.props.setLanguageSpecifictextList(this.props.field, null);
                return;
            }
        }
        this.props.setLanguageSpecifictextList(this.props.field, this.state.stateArray);
    }

    render() {
        //const { type, className,help_text,required } = this.props;
        const { label, help_text, show_heading = true, disable = false } = this.props;
        const { stateArray } = this.state;
        //required && stateArray.length === 0 && stateArray.push({ "en": "" });
        stateArray.length === 0 && stateArray.push({ "en": "" });
        return <div className="mb1">
            {show_heading && <Typography variant="h3" className="section-links" >{label} </Typography>}
            {show_heading && <Typography className="section-links" >{help_text} </Typography>}
            {/*<Grid container direction="row" alignItems="center" justifyContent="flex-start" className="border-bottom">
                <Grid item sm={8}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>
                <Grid item sm={4}>
                    {stateArray.length === 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues(e, 0, "addArrayItem")}><CreateIcon /></Button>}
                    {stateArray.length !== 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues(e, 0, "addArrayItem")}><AddCircleOutlineIcon /><small>{label}</small></Button>}
                </Grid>
            </Grid>*/}
            {
                stateArray.map((arrayItem, arrayItemIndex) => {
                    //console.log(stateArray.length , arrayItemIndex )
                    return <div key={arrayItemIndex} onBlur={this.onBlur} className="multiple">
                        {/*(arrayItem.en !== "") &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={"Remove all" + label}>{<Button className="inner-link-outlined--purple" onClick={(e) => this.setValues(e, arrayItemIndex, "removeArrayItem")}><RemoveCircleOutlineIcon className="xsmall-icon" /></Button>}</Tooltip></Grid>
                            </Grid>
                        */}

                        {(arrayItem.en !== "") && !disable &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setValues(e, arrayItemIndex, "removeArrayItem")}>{"Remove"}</Button>}</Grid>
                            </Grid>
                        }
                        {(arrayItem.en === "" && arrayItemIndex !== 0) && !disable &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setValues(e, arrayItemIndex, "removeArrayItem")}>{"Remove"}</Button>}</Grid>
                            </Grid>
                        }
                        {/*(arrayItem.en === "" && arrayItemIndex !== 0) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={"Remove all" + label}>{<Button className="inner-link-outlined--purple" onClick={(e) => this.setValues(e, arrayItemIndex, "removeArrayItem")}><RemoveCircleOutlineIcon className="xsmall-icon" /></Button>}</Tooltip></Grid>
                            </Grid>
                        */}

                        <Grid container direction="row" justifyContent="space-between" alignItems="baseline" >
                            <Grid item xs={12}><LanguageSpecificText {...this.props} help_text={show_heading === false ? (help_text || "") : ""} defaultValueObj={arrayItem} field={arrayItemIndex} setLanguageSpecificText={this.setLanguageSpecificText} /></Grid>
                        </Grid>

                        {((arrayItem.en !== "") && ((stateArray.length - 1) === arrayItemIndex)) ?
                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues(e, arrayItemIndex, "addArrayItem")}>{"Add"}</Button></Grid>
                            </Grid>
                            : <div style={{ paddingBottom: "1em" }}></div>
                        }


                        {/*
                        <Grid  container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                            <Grid item>{(arrayItemIndex>=1) && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues(e, arrayItemIndex, "removeArrayItem")}><Tooltip title={"Remove " + label}><RemoveCircleOutlineIcon className="xsmall-icon" /></Tooltip></Button>}</Grid>
                            <Grid item>{<Button className="inner-link-outlined--purple" onClick={(e) => this.setValues(e, arrayItemIndex, "addArrayItem")}><Tooltip title={"Add another " + label}><AddCircleOutlineIcon className="xsmall-icon" /></Tooltip></Button>}</Grid>
                        </Grid>
                        */}


                    </div>
                })
            }
        </div>
    }
}