import React from "react";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import validator from 'validator';
import FilePondLogo from "./FilePondLogo";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const logoValidationError = "Enter a valid url, e.g. https://www.example.com \n";
const options = { protocols: ['http', 'https', 'ftp'], require_tld: true, require_protocol: true, require_host: true, require_valid_protocol: true, allow_underscores: false, host_whitelist: false, host_blacklist: false, allow_trailing_dot: false, allow_protocol_relative_urls: false, disallow_auth: false }

export default class Logo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { logo: props.logo, error: false };
    }

    componentDidMount() {
        const { logo } = this.state;
        if (logo) {
            const valid = validator.isURL(logo || "", options);
            const error = !valid;
            this.setState({ error });
        } else {
            this.setState({ error: false });
        }
    }

    setlogo = (value) => {
        this.setState({ logo: value, error: false });
    }

    setPastelogo = (value) => {
        this.setState({ logo: value, error: false });
        this.props.updateModel_logo(this.props.field, value);
    }

    setLogoS3 = (value) => {
        this.setState({ logo: value, error: false });
        this.props.updateModel_logo(this.props.field, value);
    }

    removeLogo = () => {
        this.setState({ logo: "", error: false });
        this.props.updateModel_logo(this.props.field, "");
    }

    blur = () => {
        let { logo } = this.state;
        if (logo) {
            const valid = validator.isURL(logo || "", options);
            const error = !valid;
            this.setState({ error });
        }
        if (logo === "") {
            this.props.updateModel_logo(this.props.field, "");
        } else {
            this.props.updateModel_logo(this.props.field, this.state.logo);
        }

    }

    render() {
        const { type, required, label, help_text, className, disable, placeholder } = this.props;
        const { logo, error } = this.state;
        return <div className="mb2" onBlur={() => { this.blur() }}>
            <span>
                {!(logo.includes("https://s3.dbl.cloud.syseleven.net") || logo.includes("https://s3.cbk.cloud.syseleven.net/")) && <Grid container spacing={1} alignItems="center">
                    <Grid item xs={8}>
                        <TextField className={className}
                            type={type}
                            error={error ? true : false}
                            required={required}
                            disabled={disable}
                            label={label}
                            placeholder={placeholder ? placeholder : ""}
                            variant="outlined"
                            helperText={error ? `${logoValidationError}` : help_text}
                            inputProps={{ name: 'logoValue' }}
                            value={logo}
                            onChange={(e) => this.setlogo(e.target.value)}
                            onBlur={(e) => this.blur()}
                            onPaste={(e) => this.setPastelogo(e.clipboardData.getData("Text"))}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <FilePondLogo keycloak={this.props.keycloak} model={this.props.model} setlogo={this.setLogoS3} />
                    </Grid>
                </Grid>}
            </span>
            {logo && <Grid container spacing={1} justifyContent="flex-start" alignItems="center">
                <Grid item xs={12}><span><img width="10%" src={logo} alt={""} /></span></Grid>
                {(logo.includes("https://s3.dbl.cloud.syseleven.net") || logo.includes("https://s3.cbk.cloud.syseleven.net/")) && <>
                    <Grid item><Typography className="section-links">{label}</Typography></Grid>
                    <Grid item xs={12}>
                        <Button className="inner-link-default--purple" onClick={(e) => this.removeLogo()}>{"Remove"}
                        </Button>
                    </Grid>
                </>}
            </Grid>
            }

        </div>
    }
}