import * as yup from "yup";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { validateObject, validateObjectOptional, generalMessage, Division_of_schema } from "./OrganizationValidator";
import { funding_project_validation, document_validation } from "./commonValidatorSchemas";

const p = GenericSchemaParser;

const costValidation = (label) => {
    return {
        amount: yup.number().positive().min(0).required(generalMessage(label)).label(label + " amount").nullable(),
        currency: yup.string().required(generalMessage(label + " currency")).label(label + " currency").nullable()
    }
};

export const validateProject = (model, data) => {
    const schema = yup.object().shape({
        described_entity: yup.object().shape({
            entity_type: yup.string().label("Entity type").notRequired().max(1000),
            project_identifier: yup.array().of(
                yup.object().shape({
                    //project_identifier_scheme: yup.string().oneOf(p.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.choices.map(choice => choice.value)).label(p.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.label).required(),
                    //value: yup.string().label("Project identifier value").required().max(1000)
                    //project_identifier_scheme: yup.string().oneOf(p.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.choices.map(choice => choice.value)).label(p.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.label).required(generalMessage(p.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.label)),
                    project_identifier_scheme: yup.string().label(p.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.label).required(generalMessage(p.getFormElement("project_identifier", data.project_identifier).formElements.project_identifier_scheme.label)).nullable().default(null),
                    value: yup.string().label("Project identifier value").required(generalMessage("Project identifier value")).max(1000).nullable().default(null)
                }).label(p.getFormElement("project_identifier", data.project_identifier).label).nullable().default(null)
            ).notRequired().label(p.getFormElement("project_identifier", data.project_identifier).label).nullable().default(null),
            project_name: validateObject(p.getFormElement("project_name", data.project_name).label, 300).required().nullable().default(null),
            project_short_name: validateObjectOptional(p.getFormElement("project_name", data.project_short_name).label, 100).notRequired().nullable().default(null),
            project_alternative_name: yup.array().of(
                validateObjectOptional(p.getFormElement('project_alternative_name', data.project_alternative_name).label, 300).required().nullable().default(null)
            ).notRequired().label(p.getFormElement('project_alternative_name', data.project_alternative_name).label).nullable().default(null),
            //funding_type,funding_country, lt_area: no need to validate
            funder: yup.array().of(
                yup.object().required(generalMessage("Funder")).shape({
                    actor_type: yup.string().required().max(20).label("Actor type").test('Funder Actor type is recuired. Fill in the required information or remove it from the form.', 'Funder Actor type is recuired. Fill in the required information or remove it from the form.', (value) => value === "Person" || value === "Organization" || value === "Group"),
                    surname: yup.object().when('actor_type', {
                        is: 'Person',
                        then: validateObject("Funder surname", 300, generalMessage("Funder surname")).required().nullable().default(null),
                        otherwise: yup.object().notRequired().nullable().default(null)
                    }),
                    given_name: yup.object().when('actor_type', {
                        is: 'Person',
                        then: validateObject("Funder name", 200, generalMessage("Funder name")).required().nullable().default(null),
                        otherwise: yup.object().notRequired().nullable().default(null)
                    }),
                    personal_identifier: yup.array().of(
                        yup.object().shape({
                            personal_identifier_scheme: yup.string().label("Funder Personal identifier scheme").required(generalMessage("Funder Personal identifier scheme")).nullable().default(null),
                            value: yup.string().label("Funder Personal identifier value").max(1000).required(generalMessage("Funder Personal identifier value")).nullable().default(null)
                        }).label("Funder Personal identifier").required().label("Funder Personal identifier").nullable().default(null)
                    ).notRequired().nullable().default(null).label("Funder Personal identifier").nullable().default(null),
                    email: yup.array().of(
                        yup.string().email().max(254).label("Funder email").nullable().default(null)
                    ).notRequired().label("Funder email").nullable().default(null),
                    organization_name: yup.object().when('actor_type', {
                        is: value => value === 'Organization' || value === "Group",
                        then: validateObject("Funder organization name", 300, generalMessage("Funder organization name")).required().nullable().default(null),
                        otherwise: yup.object().notRequired().nullable().default(null)
                    }),
                    organization_short_name: yup.array().when('actor_type', {
                        is: value => value === "Organization",
                        then: yup.array().of(
                            validateObjectOptional("Funder organization short name", 100, generalMessage("Funder organization short name")).notRequired().nullable().default(null)
                        ).notRequired().label("Funder organization short name").nullable().default(null),
                        otherwise: yup.array().of(
                            validateObjectOptional("Funder organization short name", 100, generalMessage("Funder organization short name")).notRequired().nullable().default(null)
                        ).notRequired().label("Funder organization short name").nullable().default(null),
                    }),
                    organization_identifier: yup.array().of(
                        yup.object().shape({
                            organization_identifier_scheme: yup.string().label("Funder organization identifier scheme").required(generalMessage("Funder organization identifier scheme")).nullable().default(null),
                            value: yup.string().label("Funder organization identifier value").max(1000).required(generalMessage("Funder organization identifier value")).nullable().default(null)
                        }).label("Funder organization identifier").required().label("Funder organization identifier")
                    ).notRequired().nullable().default(null).label("Funder organization identifier"),
                    group_identifier: yup.array().of(
                        yup.object().shape({
                            organization_identifier_scheme: yup.string().label("Funder organization identifier scheme").required(generalMessage("Funder organization identifier scheme")).nullable().default(null),
                            value: yup.string().label("Funder organization identifier value").max(1000).required(generalMessage("Funder organization identifier value")).nullable().default(null)
                        }).label("Funder organization identifier").required().label("Funder organization identifier").nullable().default(null)
                    ).notRequired().nullable().default(null).label("Funder organization identifier"),
                    website: yup.array().of(
                        yup.string().url("Funder organization website must point to a valid URL").max(1000).label("").nullable().default(null)
                    ).notRequired().label("Website").nullable().default(null).notRequired(),
                    is_division_of: yup.array().of(
                        yup.object().notRequired().nullable().default(null).label("is division of")
                            .test({
                                name: 'test is division of recursion',
                                test: function (is_division_of) {
                                    const pending_array_with_is_division_of_obj = [];
                                    while (is_division_of) {
                                        if (Array.isArray(is_division_of)) {
                                            for (let i = 0; i < is_division_of.length; i++) {
                                                const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                                Division_of_schema("Funder ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                                const isDivisionOf = is_division_of[i]["is_division_of"];
                                                if (isDivisionOf && isDivisionOf.length > 0) {
                                                    pending_array_with_is_division_of_obj.push(isDivisionOf);
                                                }
                                            }
                                        } else {
                                            const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                            Division_of_schema("Funder ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                            const isDivisionOf = is_division_of["is_division_of"];
                                            if (isDivisionOf && isDivisionOf.length > 0) {
                                                pending_array_with_is_division_of_obj.push(isDivisionOf);
                                            }
                                        }
                                        is_division_of = pending_array_with_is_division_of_obj.shift()
                                    }
                                    return true;
                                }
                            })
                    ).notRequired().nullable().default(null)
                })
            ).label(p.getFormElement("funder", data.funder).label).notRequired().nullable().default(null),
            project_start_date: yup.date().label(p.getFormElement("project_start_date", data.project_start_date).label).notRequired().nullable().default(null),
            project_end_date: yup.date().min(yup.ref('project_start_date'), "The project end date could not be before the project start date " + model.described_entity.project_start_date).label(p.getFormElement("project_end_date", data.project_end_date).label).notRequired().nullable().default(null),
            website: yup.array().of(
                yup.string().url("Project websites must point to valid URLs").max(1000).label("").nullable().default(null)
            ).notRequired().label("Website").nullable().default(null),
            email: yup.array().of(
                yup.string().email().max(254).label("Project email").nullable().default(null),
            ).notRequired().label("Project email").nullable().default(null),
            status: yup.string().label("Project status").max(20).notRequired().nullable().default(null),
            logo: yup.string().label("").url("Project logo must point to a valid URL").nullable().default(null).notRequired(),
            social_media_occupational_account: yup.array().of(
                yup.object().required().label(p.getFormElement("social_media_occupational_account", data.social_media_occupational_account).label).shape({
                    social_media_occupational_account_type: yup.string().label(p.getFormElement("social_media_occupational_account", data.social_media_occupational_account).formElements.social_media_occupational_account_type.label).required(generalMessage(p.getFormElement("social_media_occupational_account", data.social_media_occupational_account).formElements.social_media_occupational_account_type.label)).nullable().default(null),
                    value: yup.string().label("social media value").max(1000).required(generalMessage("social media value")).nullable().default(null)
                }),
            ).notRequired().label(p.getFormElement("social_media_occupational_account", data.social_media_occupational_account).label).nullable().default(null),
            grant_number: yup.string().label(p.getFormElement("grant_number", data.grant_number).label).max(50).notRequired().nullable().default(null),
            project_summary: validateObjectOptional(p.getFormElement("project_summary", data.project_summary).label, 10000).notRequired().nullable().default(null),
            project_report: validateObjectOptional(p.getFormElement("project_report", data.project_report).label).notRequired().nullable().default(null),
            lt_area: yup.array().of(yup.string().label("lt area").required().max(200)).notRequired().label("lt area").nullable().default(null),
            subject: yup.array().of(
                yup.object().shape({
                    category_label: validateObject("Subject " + p.getFormElement("subject", data.subject).formElements.category_label.label, 150, generalMessage("Subject " + p.getFormElement("subject", data.subject).formElements.category_label.label)).required().nullable().default(null),
                    subject_identifier: yup.object().default(null).nullable().notRequired().shape({
                        subject_classification_scheme: yup.string().label("Subject classification scheme").required(generalMessage("Subject classification scheme")).nullable().default(null),
                        value: yup.string().label("Subject identifier value").max(1000).required(generalMessage("Subject identifier value")).nullable().default(null)
                    }).label("Subject").nullable().default(null)
                })
            ).nullable().default(null).notRequired().label("Subject"),
            domain: yup.array().of(
                yup.object().shape({
                    category_label: validateObject("Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label, 150, generalMessage("Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label)).required().nullable().default(null),
                    domain_identifier: yup.object().default(null).nullable().notRequired().shape({
                        domain_classification_scheme: yup.string().label("Domain classification scheme").required(generalMessage("Domain classification scheme")).nullable().default(null),
                        value: yup.string().label("domain identifier value").max(1000).required(generalMessage("domain identifier value")).nullable().default(null)
                    }).label("domain identifier").nullable().default(null)
                })
            ).notRequired().label("Domain").nullable().default(null),
            keyword: yup.array().of(
                validateObjectOptional(p.getFormElement('keyword', data.keyword).label, 100).nullable().default(null)//.required()
            ).notRequired().label(p.getFormElement('keyword', data.keyword).label).nullable().default(null),
            cost: yup.object().shape(costValidation("Cost")).nullable().default(null).label("Cost"),
            ec_max_contribution: yup.object().shape(costValidation("EC max contribution")).nullable().default(null).label("EC max contribution"),
            national_max_contribution: yup.object().shape(costValidation("National max contribution")).nullable().default(null).label("National max contribution"),
            funding_scheme_category: yup.string().notRequired().nullable().default(null).max(150).label(p.getFormElement("funding_scheme_category", data.funding_scheme_category).label),
            related_call: yup.string().notRequired().nullable().default(null).max(150).label(p.getFormElement("related_call", data.related_call).label),
            related_programme: yup.string().notRequired().nullable().default(null).max(150).label(p.getFormElement("related_programme", data.related_programme).label),
            related_subprogramme: yup.string().notRequired().nullable().default(null).max(150).label(p.getFormElement("related_subprogramme", data.related_subprogramme).label),
            coordinator: yup.object().nullable().default(null).notRequired().shape({
                organization_name: validateObject("Coordinator organization name", 300, generalMessage("Coordinator organization name")).required().nullable().default(null),
                organization_short_name: yup.array().of(
                    validateObjectOptional("Coordinator organization short name", 100).nullable().default(null)//.required()
                ).notRequired().label("Coordinator organization short name").nullable().default(null),
                organization_identifier: yup.array().of(
                    yup.object().shape({
                        organization_identifier_scheme: yup.string().label("Coordinator organization identifier scheme").required(generalMessage("Coordinator organization identifier scheme")).nullable().default(null),
                        value: yup.string().label("Coordinator organization identifier value").max(1000).required(generalMessage("Coordinator organization identifier value")).nullable().default(null)
                    }).label("Coordinator organization identifier").required().label("Coordinator organization identifier")
                ).notRequired().nullable().default(null).label("Coordinator organization identifier").nullable().default(null),
                website: yup.array().of(
                    yup.string().url("Coordinator organization website must point to a valid URL").max(1000).label("").nullable().default(null)
                ).notRequired().label("Coordinator").nullable().default(null).notRequired(),
                is_division_of: yup.array().of(
                    yup.object().notRequired().nullable().default(null).label("is division of")
                        .test({
                            name: 'test is division of recursion',
                            test: function (is_division_of) {
                                const pending_array_with_is_division_of_obj = [];
                                while (is_division_of) {
                                    if (Array.isArray(is_division_of)) {
                                        for (let i = 0; i < is_division_of.length; i++) {
                                            const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                            Division_of_schema("Coordinator ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                            const isDivisionOf = is_division_of[i]["is_division_of"];
                                            if (isDivisionOf && isDivisionOf.length > 0) {
                                                pending_array_with_is_division_of_obj.push(isDivisionOf);
                                            }
                                        }
                                    } else {
                                        const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                        Division_of_schema("Coordinator ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                        const isDivisionOf = is_division_of["is_division_of"];
                                        if (isDivisionOf && isDivisionOf.length > 0) {
                                            pending_array_with_is_division_of_obj.push(isDivisionOf);
                                        }
                                    }
                                    is_division_of = pending_array_with_is_division_of_obj.shift()
                                }
                                return true;
                            }
                        })
                )
            }),
            participating_organization: yup.array().nullable().default(null).notRequired().of(
                yup.object().nullable().default(null).notRequired().shape({
                    organization_name: validateObject("Participating organization name", 300, generalMessage("Participating organization name")).required().nullable().default(null),
                    organization_short_name: yup.array().of(
                        validateObjectOptional("Participating organization short name", 100).nullable().default(null)//.required()
                    ).notRequired().label("Participating organization short name").nullable().default(null),
                    organization_identifier: yup.array().of(
                        yup.object().shape({
                            organization_identifier_scheme: yup.string().label("Participating organization identifier scheme").required(generalMessage("Participating organization identifier scheme")).nullable().default(null),
                            value: yup.string().label("Participating organization identifier value").max(1000).required(generalMessage("Participating organization identifier value")).nullable().default(null)
                        }).label("Participating organization identifier").required().label("Participating organization identifier").nullable().default(null)
                    ).notRequired().nullable().default(null).label("Participating organization identifier"),
                    website: yup.array().of(
                        yup.string().url("Participating organization website must point to a valid URL").max(1000).label("").nullable().default(null)
                    ).notRequired().label("Participating").nullable().default(null).notRequired(),
                    is_division_of: yup.array().of(
                        yup.object().notRequired().nullable().default(null).label("is division of")
                            .test({
                                name: 'test is division of recursion',
                                test: function (is_division_of) {
                                    const pending_array_with_is_division_of_obj = [];
                                    while (is_division_of) {
                                        if (Array.isArray(is_division_of)) {
                                            for (let i = 0; i < is_division_of.length; i++) {
                                                const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                                Division_of_schema("Participating organization ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                                const isDivisionOf = is_division_of[i]["is_division_of"];
                                                if (isDivisionOf && isDivisionOf.length > 0) {
                                                    pending_array_with_is_division_of_obj.push(isDivisionOf);
                                                }
                                            }
                                        } else {
                                            const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                            Division_of_schema("Participating organization ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                            const isDivisionOf = is_division_of["is_division_of"];
                                            if (isDivisionOf && isDivisionOf.length > 0) {
                                                pending_array_with_is_division_of_obj.push(isDivisionOf);
                                            }
                                        }
                                        is_division_of = pending_array_with_is_division_of_obj.shift()
                                    }
                                    return true;
                                }
                            })
                    )
                }),
            ),
            replaces_project: yup.array().of(
                yup.object().required(generalMessage("replaces project")).shape(funding_project_validation("replaces project")).default(null).nullable()
            ).label("replaces project").notRequired().nullable().default(null),
            is_related_to_document: yup.array().of(
                yup.object().required("Is related to document is required").shape(document_validation("Is related to document"))
            ).label("Is related to document").notRequired().nullable().default(null)
        })
    });


    return schema.validate(model);
}