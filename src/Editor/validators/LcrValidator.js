import * as yup from "yup";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { validateObject, validateObjectOptional, generalMessage } from "./OrganizationValidator";
import {
    actorValidation, funding_project_validation, lr_validation, document_validation,
    licence_validation, costValidation, sizeValidation, isValidURL, source_of_metadata_record_validation,//, url_regex 
    language_validation
} from "./commonValidatorSchemas";
import { LcrMediaTypeValidator } from "./LcrAuxiliaryValidators";
import {
    LCR_TOP_TABS_HEADERS as se,//section
    LCR_FIRST_SECTION_TABS_HEADERS as f,//first 
    LCR_SECOND_SECTION_TABS_HEADERS as s,//second
    LCR_THIRD_SECTION_TABS_HEADERS as t,//third
    LCR_FOURTH_SECTION_TABS_HEADERS as fo,//fourth
} from "../../config/editorConstants";
import {
    //CD_ROM, DVD_R,
    ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY,
    //BLURAY, 
    DOWNLOADABLE,
    //HARD_DISK, OTHER, UNSPECIFIED
    //PAPER_COPY, UNSPECIFIED
} from "../Models/CorpusModel";//same for lcr

const p = GenericSchemaParser;

const fieldLocation = (step, tab) => `<strong style="color: black">  ${step} >  ${tab} >  </strong> `;

export const validateLcr = (model, data, subclass, schema_text_part, schema_audio_part, schema_video_part, schema_text_numerical, schema_image_part, dataFiles, service_compliant_dataset, for_info) => {
    const schema = yup.object().shape({
        source_of_metadata_record: yup.object().notRequired().nullable().default(null).shape(
            source_of_metadata_record_validation(fieldLocation(se[0], f[0]) + "Source of metadata record")
        ).label(fieldLocation(se[0], f[0]) + "Source of metadata record"),
        described_entity: yup.object().shape({
            entity_type: yup.string().label("Entity type").notRequired().max(1000).default(null).nullable(),
            lr_identifier: yup.array().of(
                yup.object().shape({
                    lr_identifier_scheme: yup.string().label(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.label).required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.label)).default(null).nullable(),
                    value: yup.string().label(fieldLocation(se[0], f[0]) + "Language Resource identifier value").required(generalMessage(fieldLocation(se[0], f[0]) + "Language Resource identifier value")).max(1000).default(null).nullable()
                }).label(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).label).default(null).nullable()
            ).notRequired().label(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).label).nullable().default(null),
            resource_name: validateObject(fieldLocation(se[0], f[0]) + `${p.getFormElement("resource_name", data.resource_name).label}`, 1000).default(null).nullable(),
            resource_short_name: validateObjectOptional(fieldLocation(se[0], f[0]) + p.getFormElement("resource_short_name", data.resource_short_name).label, 300).notRequired().nullable().default(null),
            description: validateObject(fieldLocation(se[0], f[0]) + `${p.getFormElement("description", data.description).label}`, 10000).default(null).nullable(),
            version: yup.string().label(fieldLocation(se[0], f[0]) + `${p.getFormElement("version", data.version).label}`).notRequired().max(50).nullable().default(null),
            version_date: yup.date().label(fieldLocation(se[0], f[0]) + p.getFormElement("version_date", data.version_date).label).notRequired().nullable().default(null),
            resource_provider: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label)).shape(actorValidation(fieldLocation(se[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label)).default(null).nullable(),
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label).notRequired().nullable().default(null),
            resource_creator: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label)).shape(actorValidation(fieldLocation(se[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label)).default(null).nullable()
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label).notRequired().nullable().default(null),
            publication_date: yup.date().label(fieldLocation(se[0], f[0]) + p.getFormElement("publication_date", data.publication_date).label).notRequired().nullable().default(null),
            funding_project: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label)).shape(funding_project_validation(fieldLocation(se[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label)).default(null).nullable()
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label).notRequired().nullable().default(null),
            logo: yup.string().label("").url(fieldLocation(se[0], f[0]) + "Language resource logo must be a valid URL").nullable().default(null).notRequired(),
            //
            intended_application: yup.array().of(
                yup.string().label(fieldLocation(se[0], f[1]) + p.getFormElement("intended_application", data.intended_application).label).notRequired().max(200).default(null).nullable(),
            ).label(fieldLocation(se[0], f[1]) + p.getFormElement("intended_application", data.intended_application).label).notRequired().nullable().default(null),
            complies_with: yup.array().of(
                yup.string().label(fieldLocation(se[0], f[1]) + p.getFormElement("complies_with", data.complies_with).label).notRequired().default(null).nullable(),
            ).label(fieldLocation(se[0], f[1]) + p.getFormElement("complies_with", data.complies_with).label).nullable().default(null).notRequired(),
            domain: yup.array().of(
                yup.object().shape({
                    category_label: validateObject(fieldLocation(se[0], f[1]) + "Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label, 150, generalMessage("Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label)).default(null).nullable(),
                    domain_identifier: yup.object().default(null).nullable().notRequired().shape({
                        domain_classification_scheme: yup.string().label(fieldLocation(se[0], f[1]) + "Domain classification scheme").required(generalMessage(fieldLocation(se[0], f[1]) + "Domain classification scheme")).default(null).nullable(),
                        value: yup.string().label(fieldLocation(se[0], f[1]) + "domain identifier value").max(1000).required(generalMessage(fieldLocation(se[0], f[1]) + "domain identifier value")).default(null).nullable(),
                    }).label(fieldLocation(se[0], f[1]) + "domain identifier").default(null).nullable(),
                })
            ).notRequired().label(fieldLocation(se[0], f[1]) + "Domain").nullable().default(null),
            keyword: yup.array().of(
                validateObject(fieldLocation(se[0], f[1]) + p.getFormElement('keyword', data.keyword).label, 100).default(null).nullable(),
            ).required().label(fieldLocation(se[0], f[1]) + `${p.getFormElement('keyword', data.keyword).label}`).nullable().default(null),
            //
            additional_info: yup.array().of(
                yup.object().shape({
                    landing_page: yup.string().max(1000).label(fieldLocation(se[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " website").default(null).nullable().test({
                        name: 'landing_page url',
                        test: function (landing_page) {
                            if (landing_page) {
                                if (!isValidURL(landing_page)) {
                                    return this.createError({ message: fieldLocation(se[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " must be a valid URL", path: "" });
                                }
                            }
                            return true;
                        },
                    }),
                    email: yup.string().email().max(254).label(fieldLocation(se[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " email").default(null).nullable()
                })
            ).required().label(fieldLocation(se[0], f[2]) + `${p.getFormElement("additional_info", data.additional_info).label}`).default(null).nullable().test({
                name: "validate existance of at least one entry",
                test: function (additional_info) {
                    for (let index = 0; additional_info && index < additional_info.length; index++) {
                        const { landing_page, email } = additional_info[index];
                        if (!landing_page && !email) {
                            return this.createError({ message: fieldLocation(se[0], f[2]) + `${p.getFormElement("additional_info", data.additional_info).label} landing page or email must be filled`, path: "" });
                        }
                    }
                    return true;
                }
            }),
            contact: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[2]) + p.getFormElement("contact", data.contact).label)).shape(actorValidation(fieldLocation(se[0], f[2]) + p.getFormElement("contact", data.contact).label)).default(null).nullable()
            ).label(fieldLocation(se[0], f[2]) + p.getFormElement("contact", data.contact).label).notRequired().nullable().default(null),
            //
            is_documented_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label)).shape(document_validation(fieldLocation(se[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label))
            ).label(fieldLocation(se[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label).notRequired().nullable().default(null),
            is_to_be_cited_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_documented_by).label)).shape(document_validation(fieldLocation(se[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by).label))
            ).label(fieldLocation(se[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by).label).notRequired().nullable().default(null),
            //
            relation: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).label)).shape({
                    related_lr: yup.object().nullable().default(null).required(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).formElements.related_lr.label + " required").shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).formElements.related_lr.label)),
                    relation_type: validateObject(fieldLocation(se[0], f[4]) + p.getFormElement('relation', data.relation).formElements.relation_type.label, 150).nullable().default(null)
                }).label(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).label).notRequired().nullable().default(null)
            ).nullable().default(null),
            is_part_of: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label).notRequired().nullable().default(null),
            is_similar_to: yup.array().of(
                yup.object().required(fieldLocation(se[0], f[4]) + generalMessage(p.getFormElement("is_similar_to", data.is_similar_to).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_similar_to", data.is_similar_to).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_similar_to", data.is_similar_to).label).notRequired().nullable().default(null),
            is_related_to_lr: yup.array().of(
                yup.object().required(fieldLocation(se[0], f[4]) + generalMessage(p.getFormElement("is_related_to_lr", data.is_related_to_lr).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_related_to_lr", data.is_related_to_lr).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_related_to_lr", data.is_related_to_lr).label).notRequired().nullable().default(null),
            replaces: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("replaces", data.replaces).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("replaces", data.replaces).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("replaces", data.replaces).label).notRequired().nullable().default(null),
            is_version_of: yup.object().notRequired().nullable().default(null).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_version_of", data.is_version_of).label)),
            ///////////////////
            lr_subclass: yup.object().shape({
                encoding_level: yup.array().of(
                    yup.string().label(fieldLocation(se[1], s[0]) + p.getFormElement("encoding_level", subclass.encoding_level).label).required().default(null).nullable(),
                ).label(fieldLocation(se[1], s[0]) + p.getFormElement("encoding_level", subclass.encoding_level).label).required().nullable().default(null),
                lcr_subclass: yup.string().notRequired().nullable().default(null).label(se[1], s[0] + "LCR subclass"),
                content_type: yup.array().of(
                    yup.string().label(fieldLocation(se[1], s[0]) + p.getFormElement("content_type", subclass.content_type).label).notRequired().default(null).nullable(),
                ).label(fieldLocation(se[1], s[0]) + p.getFormElement("content_type", subclass.content_type).label).notRequired().nullable().default(null),
                /*complies_with: yup.array().of(
                    yup.string().label(fieldLocation(se[1], s[0]) + p.getFormElement("complies_with", subclass.complies_with).label).notRequired().default(null).nullable(),
                ).label(fieldLocation(se[1], s[0]) + p.getFormElement("complies_with", subclass.complies_with).label).notRequired().nullable().default(null),*/
                personal_data_included: for_info ? yup.string().notRequired().default(null).nullable().label(fieldLocation(se[1], s[0]) + `${p.getFormElement("personal_data_included", subclass.personal_data_included).label}`) : yup.string().required().default(null).nullable().label(fieldLocation(se[1], s[0]) + `${p.getFormElement("personal_data_included", subclass.personal_data_included).label}`),
                personal_data_details: validateObjectOptional(fieldLocation(se[1], s[0]) + p.getFormElement("personal_data_details", subclass.personal_data_details).label, 500).notRequired().nullable().default(null),
                sensitive_data_included: for_info ? yup.string().notRequired().default(null).nullable().label(fieldLocation(se[1], s[0]) + `${p.getFormElement("sensitive_data_included", subclass.sensitive_data_included).label}`) : yup.string().required().default(null).nullable().label(fieldLocation(se[1], s[0]) + `${p.getFormElement("sensitive_data_included", subclass.sensitive_data_included).label}`),
                sensitive_data_details: validateObjectOptional(fieldLocation(se[1], s[0]) + p.getFormElement("sensitive_data_details", subclass.sensitive_data_details).label, 500).notRequired().nullable().default(null),
                anonymized: yup.string().when(['personal_data_included', 'sensitive_data_included'], {
                    is: (personal_data_included, sensitive_data_included) => personal_data_included === "http://w3id.org/meta-share/meta-share/yesP" || sensitive_data_included === "http://w3id.org/meta-share/meta-share/yesS",
                    then: yup.string().required().default(null).nullable().label(fieldLocation(se[1], s[0]) + `${p.getFormElement("anonymized", subclass.anonymized).label}`),
                    otherwise: yup.string().notRequired().default(null).nullable().label(fieldLocation(se[1], s[0]) + `${p.getFormElement("anonymized", subclass.anonymized).label}`),
                }).notRequired().default(null).nullable().label(fieldLocation(se[1], s[0]) + `${p.getFormElement("anonymized", subclass.anonymized).label}`),
                anonymization_details: validateObjectOptional(fieldLocation(se[1], s[0]) + p.getFormElement("anonymization_details", subclass.anonymization_details).label, 500).notRequired().nullable().default(null),
                ///////////////////
                lexical_conceptual_resource_media_part: yup.array().default(null).nullable().label(fieldLocation(se[2], t[0]) + p.getFormElement("lexical_conceptual_resource_media_part", subclass.lexical_conceptual_resource_media_part).label).test(
                    LcrMediaTypeValidator(
                        fieldLocation(se[2], t[0]),
                        schema_text_part, schema_audio_part, schema_video_part, schema_image_part)
                ).when('lcr_subclass', function (lcr_subclass, schema) {
                    return (for_info) ? schema.notRequired() : schema.required();
                }),
                unspecified_part: yup.object().shape({
                    language: yup.array().of(
                        yup.object().shape(language_validation(`${fieldLocation(se[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.language.label}`))
                    ).required().nullable().default(null).label(`${fieldLocation(se[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.language.label}`),
                    metalanguage: yup.array().of(
                        yup.object().shape(language_validation(`${fieldLocation(se[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.metalanguage.label}`))
                    ).notRequired().nullable().default(null).label(`${fieldLocation(se[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label} ${subclass.unspecified_part.children.metalanguage.label}`),
                    multilinguality_type: yup.string().default(null).nullable().label(`${fieldLocation(se[2], t[0])}` + subclass.unspecified_part.children.multilinguality_type.label).when('language', (language, schema) => {
                        return language && language.length >= 2 ? schema.notRequired() : schema.notRequired();
                    }),
                    multilinguality_type_details: validateObjectOptional(`${fieldLocation(se[2], t[0])} ${subclass.unspecified_part.children.multilinguality_type_details.label}`, subclass.unspecified_part.children.multilinguality_type_details.max_length).notRequired().nullable().default(null),
                }).label(fieldLocation(se[2], t[0]) + p.getFormElement("unspecified_part", subclass.unspecified_part).label).nullable().default(null).when('lcr_subclass', function (lcr_subclass, schema) {
                    if (for_info) {
                        return schema.required();
                    } else {
                        return schema.notRequired();
                    }
                }),
                ///////////////////
                dataset_distribution: yup.array().required().default(null).nullable().label(fieldLocation(se[3], fo[0]) + p.getFormElement("dataset_distribution", subclass.dataset_distribution).label).of(
                    yup.object().required().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("dataset_distribution", subclass.dataset_distribution).label).shape({
                        dataset_distribution_form: yup.string().required().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("dataset_distribution_form", subclass.dataset_distribution.child.children.dataset_distribution_form).label),
                        dataset: yup.object().notRequired().nullable().default(null).shape({
                            id: yup.number().required(),
                            file: yup.string().required(),
                            "date_stored": yup.string().required()
                        }).label("dataset"),
                        private_resource: yup.boolean().notRequired().label(fieldLocation(se[3], fo[0]) + p.getFormElement("dataset_distribution", subclass.dataset_distribution).label).nullable().default(null),
                        /*distribution_location: yup.string().max(1000).default(null).nullable().label(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_location", subclass.dataset_distribution.child.children.distribution_location).label)
                            .when('dataset_distribution_form', (dataset_distribution_form, schema) => {
                                return [BLURAY, CD_ROM, HARD_DISK, DVD_R, OTHER, UNSPECIFIED].includes(dataset_distribution_form) ? schema.required() : schema.notRequired();
                            }).test({
                                name: 'distribution_location valid url',
                                test: function (distribution_location) {
                                    if (distribution_location) {
                                        if (!isValidURL(distribution_location)) {
                                            return this.createError({ message: fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_location", subclass.dataset_distribution.child.children.distribution_location).label + " must be a valid URL", path: "" });
                                        }
                                    }
                                    return true;
                                },
                            }),*/
                        access_location: yup.string().max(1000).default(null).nullable().label(fieldLocation(se[3], fo[0]) + p.getFormElement("access_location", subclass.dataset_distribution.child.children.access_location).label)
                            .when(['dataset_distribution_form'], (dataset_distribution_form, schema) => {
                                return [ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY].includes(dataset_distribution_form) ? schema.required() : schema.notRequired();
                            }).test({
                                name: 'access_location valid url',
                                test: function (access_location) {
                                    if (access_location) {
                                        if (!isValidURL(access_location)) {
                                            return this.createError({ message: fieldLocation(se[3], fo[0]) + p.getFormElement("access_location", subclass.dataset_distribution.child.children.access_location).label + " must be a valid URL", path: "" });
                                        }
                                    }
                                    return true;
                                },
                            }),
                        download_location: yup.string().max(1000).default(null).nullable().label(fieldLocation(se[3], fo[0]) + p.getFormElement("download_location", subclass.dataset_distribution.child.children.download_location).label).when(['dataset_distribution_form', 'access_location', 'dataset'], (dataset_distribution_form, access_location, dataset, schema) => {
                            if (dataset_distribution_form !== DOWNLOADABLE) {
                                return schema.notRequired();
                            }
                            if (access_location) {
                                return schema.notRequired();
                            }
                            if (dataset) {
                                return schema.notRequired();
                            }
                            return schema.required(fieldLocation(se[3], fo[0]) + "At least one of the following fields must be filled in: download location, access location or upload a dataset and accociate it with the distribution");
                        }).test({
                            name: 'download_location valid url',
                            test: function (download_location) {
                                if (download_location) {
                                    if (!isValidURL(download_location)) {
                                        return this.createError({ message: fieldLocation(se[3], fo[0]) + p.getFormElement("download_location", subclass.dataset_distribution.child.children.download_location).label + " must be a valid URL", path: "" });
                                    }
                                }
                                return true;
                            },
                        }),
                        samples_location: yup.array().notRequired().default(null).nullable().of(
                            yup.string().url().max(1000).default(null).nullable().notRequired().label(fieldLocation(se[3], fo[0]) + p.getFormElement("samples_location", subclass.dataset_distribution.child.children.samples_location).label)
                        ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("samples_location", subclass.dataset_distribution.child.children.samples_location).label),
                        is_queried_by: yup.array().notRequired().default(null).nullable().of(
                            yup.object().required(generalMessage(fieldLocation(se[3], fo[0]) + p.getFormElement("is_queried_by", p.getFormElement("is_queried_by", subclass.dataset_distribution.child.children.is_queried_by)).label)).shape(lr_validation(fieldLocation(se[3], fo[0]) + p.getFormElement("is_queried_by", subclass.dataset_distribution.child.children.is_queried_by).label))
                        ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("is_queried_by", subclass.dataset_distribution.child.children.is_queried_by).label),
                        membership_institution: yup.array().notRequired().default(null).nullable().of(
                            yup.string().default(null).nullable().notRequired().label(fieldLocation(se[3], fo[0]) + p.getFormElement("membership_institution", subclass.dataset_distribution.child.children.membership_institution).label)
                        ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("membership_institution", subclass.dataset_distribution.child.children.membership_institution).label),
                        licence_terms: yup.array().of(
                            yup.object().required(fieldLocation(se[3], fo[0]) + generalMessage(p.getFormElement("licence_terms", subclass.dataset_distribution.child.children.licence_terms).label)).shape(licence_validation(fieldLocation(se[3], fo[0]) + p.getFormElement("licence_terms", subclass.dataset_distribution.child.children.licence_terms).label))
                        ).required().label(fieldLocation(se[3], fo[0]) + p.getFormElement("licence_terms", subclass.dataset_distribution.child.children.licence_terms).label).nullable().default(null),
                        cost: yup.object().shape(costValidation(fieldLocation(se[3], fo[0]) + "Distribution Cost")).nullable().default(null).label(fieldLocation(se[3], fo[0]) + "Distribution Cost"),
                        access_rights: yup.array().of(
                            yup.object().shape({
                                category_label: validateObject(fieldLocation(se[3], fo[0]) + "Access rights category label", 150, generalMessage(fieldLocation(se[3], fo[0]) + "Access rights category label")).required().default(null).nullable(),
                                access_rights_statement_identifier: yup.object().default(null).nullable().notRequired().shape({
                                    access_rights_statement_scheme: yup.string().label(fieldLocation(se[3], fo[0]) + "Access rights statement scheme").required(generalMessage(fieldLocation(se[3], fo[0]) + "Access rights statement scheme")).default(null).nullable(),
                                    value: yup.string().label(fieldLocation(se[3], fo[0]) + "Access rights statement value").max(1000).required(generalMessage(fieldLocation(se[3], fo[0]) + "Access rights statement value")).default(null).nullable(),
                                }).label(fieldLocation(se[3], fo[0]) + "Access rights statement identifier").default(null).nullable(),
                            })
                        ).notRequired().label(fieldLocation(se[3], fo[0]) + "Access rights").nullable().default(null),
                        distribution_text_feature: yup.array().of(
                            yup.object().required(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_text_feature", subclass.dataset_distribution.child.children.distribution_text_feature).label).shape({
                                size: yup.array().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se[3], fo[0]), subclass.dataset_distribution.child.children.distribution_text_feature.child.children.size.child.children))
                                ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_text_feature.child.children.size).label),
                                data_format: yup.array().required().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_text_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_text_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_text_feature", subclass.dataset_distribution.child.children.distribution_text_feature).label).nullable().default(null)
                            .when('lexical_conceptual_resource_media_part', function (lexical_conceptual_resource_media_part, schema) {
                                let has_text_media_part = false;
                                let has_at_least_one_text_feature = false;
                                if (model.described_entity.lr_subclass.lexical_conceptual_resource_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.lexical_conceptual_resource_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.lexical_conceptual_resource_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/text") {
                                            has_text_media_part = true;
                                            break;
                                            //return schema.required();
                                        }
                                    }
                                }
                                if (has_text_media_part) {
                                    for (let i = 0; model.described_entity.lr_subclass.dataset_distribution && i < model.described_entity.lr_subclass.dataset_distribution.length; i++) {
                                        const distribution = model.described_entity.lr_subclass.dataset_distribution[i];
                                        if (distribution && distribution.distribution_text_feature && distribution.distribution_text_feature.length > 0) {
                                            has_at_least_one_text_feature = true;
                                            break;
                                        }
                                    }
                                }
                                if (has_text_media_part) {
                                    if (has_at_least_one_text_feature) {
                                        return schema.notRequired();
                                    } else {
                                        return schema.required();
                                    }
                                } else {
                                    return schema.notRequired();
                                }
                            }),
                        distribution_video_feature: yup.array().of(
                            yup.object().required(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_video_feature", subclass.dataset_distribution.child.children.distribution_video_feature).label).shape({
                                size: yup.array().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se[3], fo[0]), subclass.dataset_distribution.child.children.distribution_video_feature.child.children.size.child.children))
                                ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_video_feature.child.children.size).label),
                                data_format: yup.array().required().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_video_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_video_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_video_feature", subclass.dataset_distribution.child.children.distribution_video_feature).label).nullable().default(null)
                            .when('lexical_conceptual_resource_media_part', function (lexical_conceptual_resource_media_part, schema) {
                                let has_video_media_part = false;
                                let has_at_least_one_video_feature = false;
                                if (model.described_entity.lr_subclass.lexical_conceptual_resource_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.lexical_conceptual_resource_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.lexical_conceptual_resource_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/video") {
                                            has_video_media_part = true;
                                            break;
                                            //return schema.required();
                                        }
                                    }
                                }
                                if (has_video_media_part) {
                                    for (let i = 0; model.described_entity.lr_subclass.dataset_distribution && i < model.described_entity.lr_subclass.dataset_distribution.length; i++) {
                                        const distribution = model.described_entity.lr_subclass.dataset_distribution[i];
                                        if (distribution && distribution.distribution_video_feature && distribution.distribution_video_feature.length > 0) {
                                            has_at_least_one_video_feature = true;
                                            break;
                                        }
                                    }
                                }
                                if (has_video_media_part) {
                                    if (has_at_least_one_video_feature) {
                                        return schema.notRequired();
                                    } else {
                                        return schema.required();
                                    }
                                } else {
                                    return schema.notRequired();
                                }
                            }),
                        distribution_image_feature: yup.array().of(
                            yup.object().required(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_image_feature", subclass.dataset_distribution.child.children.distribution_image_feature).label).shape({
                                size: yup.array().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se[3], fo[0]), subclass.dataset_distribution.child.children.distribution_image_feature.child.children.size.child.children))
                                ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_image_feature.child.children.size).label),
                                data_format: yup.array().required().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_image_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_image_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_image_feature", subclass.dataset_distribution.child.children.distribution_image_feature).label).nullable().default(null)
                            .when('lexical_conceptual_resource_media_part', function (lexical_conceptual_resource_media_part, schema) {
                                let has_image_media_part = false;
                                let has_at_least_one_image_feature = false;
                                if (model.described_entity.lr_subclass.lexical_conceptual_resource_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.lexical_conceptual_resource_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.lexical_conceptual_resource_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/image") {
                                            has_image_media_part = true;
                                            break;
                                            //return schema.required();
                                        }
                                    }
                                }
                                if (has_image_media_part) {
                                    for (let i = 0; model.described_entity.lr_subclass.dataset_distribution && i < model.described_entity.lr_subclass.dataset_distribution.length; i++) {
                                        const distribution = model.described_entity.lr_subclass.dataset_distribution[i];
                                        if (distribution && distribution.distribution_image_feature && distribution.distribution_image_feature.length > 0) {
                                            has_at_least_one_image_feature = true;
                                            break;
                                        }
                                    }
                                }
                                if (has_image_media_part) {
                                    if (has_at_least_one_image_feature) {
                                        return schema.notRequired();
                                    } else {
                                        return schema.required();
                                    }
                                } else {
                                    return schema.notRequired();
                                }
                            }),
                        distribution_audio_feature: yup.array().of(
                            yup.object().required(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_audio_feature", subclass.dataset_distribution.child.children.distribution_audio_feature).label).shape({
                                size: yup.array().nullable().default(null).of(
                                    yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se[3], fo[0]), subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.size.child.children))
                                ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.size).label),
                                audio_format: yup.array().nullable().default(null).notRequired().of(
                                    yup.object().notRequired().nullable().default(null).shape({
                                        data_format: yup.string().required().default(null).nullable().label(fieldLocation(se[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format.child.children.data_format.label),
                                        byte_order: yup.string().notRequired().default(null).nullable().label(fieldLocation(se[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format.child.children.byte_order.label),
                                        compressed: yup.boolean().required().default(null).nullable().label(fieldLocation(se[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format.child.children.compressed.label),
                                    }).label(fieldLocation(se[3], fo[0]) + p.getFormElement("audio_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format).label),
                                ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("audio_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.audio_format).label),
                                duration_of_audio: yup.array().nullable().default(null).notRequired().of(
                                    yup.object().notRequired().nullable().default(null).shape({
                                        amount: yup.number().required().default(null).nullable().min(0).label(fieldLocation(se[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio.child.children.amount.label),
                                        duration_unit: yup.string().required().default(null).nullable().label(fieldLocation(se[3], fo[0]) + subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio.child.children.duration_unit.label),
                                    }).label(fieldLocation(se[3], fo[0]) + p.getFormElement("duration_of_audio", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio).label),
                                ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("duration_of_audio", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.duration_of_audio).label),
                                data_format: yup.array().required().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.data_format).label).of(
                                    yup.string().notRequired().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_audio_feature.child.children.data_format).label)
                                )
                            })
                        ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_audio_feature", subclass.dataset_distribution.child.children.distribution_audio_feature).label).nullable().default(null)
                            .when('lexical_conceptual_resource_media_part', function (lexical_conceptual_resource_media_part, schema) {
                                let has_audio_media_part = false;
                                let has_at_least_one_audio_feature = false;
                                if (model.described_entity.lr_subclass.lexical_conceptual_resource_media_part) {
                                    for (let i = 0; i < model.described_entity.lr_subclass.lexical_conceptual_resource_media_part.length; i++) {
                                        const media_part = model.described_entity.lr_subclass.lexical_conceptual_resource_media_part[i];
                                        if (media_part.media_type === "http://w3id.org/meta-share/meta-share/audio") {
                                            has_audio_media_part = true;
                                            break;
                                            //return schema.required();
                                        }
                                    }
                                }
                                if (has_audio_media_part) {
                                    for (let i = 0; model.described_entity.lr_subclass.dataset_distribution && i < model.described_entity.lr_subclass.dataset_distribution.length; i++) {
                                        const distribution = model.described_entity.lr_subclass.dataset_distribution[i];
                                        if (distribution && distribution.distribution_audio_feature && distribution.distribution_audio_feature.length > 0) {
                                            has_at_least_one_audio_feature = true;
                                            break;
                                        }
                                    }
                                }
                                if (has_audio_media_part) {
                                    if (has_at_least_one_audio_feature) {
                                        return schema.notRequired();
                                    } else {
                                        return schema.required();
                                    }
                                } else {
                                    return schema.notRequired();
                                }
                                //schema.notRequired();
                            }),
                        distribution_unspecified_feature: yup.object().label(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_unspecified_feature", subclass.dataset_distribution.child.children.distribution_unspecified_feature).label).shape({
                            size: yup.array().nullable().default(null).of(
                                yup.object().notRequired().default(null).nullable().shape(sizeValidation(fieldLocation(se[3], fo[0]), subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.size.child.children))
                            ).label(fieldLocation(se[3], fo[0]) + p.getFormElement("size", subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.size).label),
                            data_format: yup.array().required().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.data_format).label).of(
                                yup.string().notRequired().nullable().default(null).label(fieldLocation(se[3], fo[0]) + p.getFormElement("data_format", subclass.dataset_distribution.child.children.distribution_unspecified_feature.children.data_format).label)
                            )
                        }).label(fieldLocation(se[3], fo[0]) + p.getFormElement("distribution_unspecified_feature", subclass.dataset_distribution.child.children.distribution_unspecified_feature).label).nullable().default(null)
                            .when('lcr_subclass', function (lcr_subclass, schema) {
                                let has_at_least_one_unspecified_feature = false;
                                if (for_info) {
                                    for (let i = 0; model.described_entity.lr_subclass.dataset_distribution && i < model.described_entity.lr_subclass.dataset_distribution.length; i++) {
                                        const distribution = model.described_entity.lr_subclass.dataset_distribution[i];
                                        if (distribution && distribution.distribution_unspecified_feature) {
                                            has_at_least_one_unspecified_feature = true;
                                            break;
                                        }
                                    }
                                }
                                if (for_info) {
                                    if (has_at_least_one_unspecified_feature) {
                                        return schema.notRequired();
                                    } else {
                                        return schema.required();
                                    }
                                } else {
                                    return schema.notRequired();
                                }
                            }),
                    })
                ).test({
                    name: 'If has data=true there must be at least one downloadable distribution and the file associated with it',
                    test: function (distributionArray) {
                        let hasData = dataFiles && dataFiles.length > 0;
                        let found_distribution_that_requires_data = false;
                        for (let index = 0; hasData && distributionArray && index < distributionArray.length; index++) {
                            const distribution = distributionArray[index];
                            const { dataset_distribution_form } = distribution;
                            if ([DOWNLOADABLE].includes(dataset_distribution_form)) {
                                found_distribution_that_requires_data = true;
                            }
                        }
                        if (!found_distribution_that_requires_data && hasData) {
                            return this.createError({ message: fieldLocation(se[3], fo[0]) + `Since you have uploaded a content file you must create at least one distribution of downloadable form and associate the file with it. `, path: fieldLocation(se[3], fo[0]) });
                        }
                        return true;
                    },
                }).test({
                    name: 'Validations for service_compliant_dataset',
                    test: function (distributionArray) {
                        let found_compatible_distribution_form = false;
                        let is_queried_by_error_message = null;
                        for (let index = 0; service_compliant_dataset && distributionArray && index < distributionArray.length; index++) {
                            const distribution = distributionArray[index];
                            const { dataset_distribution_form, is_queried_by } = distribution;
                            if ([ACCESSIBLE_QUERY].includes(dataset_distribution_form)) {
                                found_compatible_distribution_form = true;
                                if (is_queried_by.length === 0) {
                                    is_queried_by_error_message = 'Query tool is missing. Since you are submitting an elg compatible resource that is accessible through query you should add an elg compatible service as a query tool';
                                } else if (is_queried_by.length !== 1) {
                                    is_queried_by_error_message = 'Only one query tool is allowed. Since you are submitting an elg compatible resource that is accessible through query you should add only one elg compatible service as a query tool';
                                }
                            }
                        }
                        if (service_compliant_dataset && !found_compatible_distribution_form) {
                            return this.createError({ message: fieldLocation(se[3], fo[0]) + `Since you are submitting an elg compatible resource you should create at least one "accessible through query" DatasetDistributionForm`, path: fieldLocation(se[3], fo[0]) });
                        } else if (service_compliant_dataset && found_compatible_distribution_form && is_queried_by_error_message) {
                            return this.createError({ message: fieldLocation(se[3], fo[0]) + is_queried_by_error_message, path: fieldLocation(se[3], fo[0]) });
                        }
                        return true;
                    },
                })
            }).label("lexical conceptual resource subclass").required().default(null).nullable()
        })
    });


    return schema.validate(model, { abortEarly: false });
}