import * as yup from "yup";
import validator from 'validator';
//import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { validateObject, generalMessage, validateObjectOptional, Division_of_schema } from "./OrganizationValidator";

//const p = GenericSchemaParser;
export const url_regex = "^(?:([a-z0-9+.-]+)://)(?:S+(?::S*)?@)?(?:(?:[1-9]d?|1dd|2[01]d|22[0-3])(?:.(?:1?d{1,2}|2[0-4]d|25[0-5])){2}(?:.(?:[1-9]d?|1dd|2[0-4]d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*.?)(?::d{2,5})?(?:[/?#]S*)?$";
const options = { protocols: ['http', 'https', 'ftp'], require_tld: false, require_protocol: true, require_host: true, require_valid_protocol: true, allow_underscores: false, host_whitelist: false, host_blacklist: false, allow_trailing_dot: false, allow_protocol_relative_urls: false, disallow_auth: false, allowLocal: true }

export const isValidURL = (url) => {
    const valid = validator.isURL(url || "", options);
    return valid;
}

export const actorValidation = (actorLabel) => {
    return {
        actor_type: yup.string().required().max(20).label(actorLabel).test(`${actorLabel} actor type is recuired. Fill in the required information or remove it from the form.`, `${actorLabel} actor type is recuired. Fill in the required information or remove it from the form.`, (value) => value === "Person" || value === "Organization" || value === "Group"),
        surname: yup.object().when('actor_type', {
            is: 'Person',
            then: validateObject(`${actorLabel} surname`, 300, generalMessage(`${actorLabel} surname`)).nullable().default(null),
            otherwise: yup.object().notRequired().nullable().default(null)
        }),
        given_name: yup.object().when('actor_type', {
            is: 'Person',
            then: validateObject(`${actorLabel} name`, 200, generalMessage(`${actorLabel} name`)).required().nullable().default(null),
            otherwise: yup.object().notRequired().nullable().default(null)
        }),
        personal_identifier: yup.array().of(
            yup.object().shape({
                personal_identifier_scheme: yup.string().label(`${actorLabel} Personal identifier scheme`).required(generalMessage(`${actorLabel} Personal identifier scheme`)),
                value: yup.string().label(`${actorLabel} Personal identifier value`).max(1000).required(generalMessage(`${actorLabel} Personal identifier value`))
            }).label(`${actorLabel} Personal identifier`).required().label(`${actorLabel} Personal identifier`)
        ).notRequired().nullable().default(null).label(`${actorLabel} Personal identifier`),
        email: yup.array().of(
            yup.string().email().max(254).label(`${actorLabel} email`).nullable().default(null)
        ).notRequired().label(`${actorLabel} email`).nullable().default(null),
        organization_name: yup.object().when('actor_type', {
            is: value => value === 'Organization' || value === "Group",
            then: validateObject(`${actorLabel} organization name`, 300, generalMessage(`${actorLabel} organization name`)).nullable().default(null),
            otherwise: yup.object().notRequired().nullable().default(null)
        }),
        organization_short_name: yup.array().when('actor_type', {
            is: value => value === 'Organization',
            then: yup.array().of(
                validateObjectOptional(`${actorLabel} organization short name`, 100, generalMessage(`${actorLabel} organization short name`)).notRequired().nullable().default(null)
            ).notRequired().label(`${actorLabel} organization short name`).nullable().default(null),
            otherwise: yup.array().of(
                validateObjectOptional(`${actorLabel} organization short name`, 100, generalMessage(`${actorLabel} organization short name`)).notRequired().nullable().default(null)
            ).notRequired().label(`${actorLabel} organization short name`).nullable().default(null),
        }),
        organization_identifier: yup.array().of(
            yup.object().shape({
                organization_identifier_scheme: yup.string().label(`${actorLabel} organization identifier scheme`).required(generalMessage(`${actorLabel} organization identifier scheme`)),
                value: yup.string().label(`${actorLabel} organization identifier value`).max(1000).required(generalMessage(`${actorLabel} organization identifier value`))
            }).label(`${actorLabel} organization identifier`).required().label(`${actorLabel} organization identifier`)
        ).notRequired().nullable().default(null).label(`${actorLabel} organization identifier`),
        group_identifier: yup.array().of(
            yup.object().shape({
                organization_identifier_scheme: yup.string().label(`${actorLabel} organization identifier scheme`).required(generalMessage(`${actorLabel} organization identifier scheme`)),
                value: yup.string().label(`${actorLabel} organization identifier value`).max(1000).required(generalMessage(`${actorLabel} organization identifier value`))
            }).label(`${actorLabel} organization identifier`).required().label(`${actorLabel} organization identifier`)
        ).notRequired().nullable().default(null).label(`${actorLabel} organization identifier`),
        website: yup.array().of(
            yup.string().url(`${actorLabel} organization website must point to a valid URL`).max(1000).label("").nullable().default(null)
        ).notRequired().label("Website").nullable().default(null).notRequired(),
        is_division_of: yup.array().of(
            yup.object().notRequired().nullable().default(null).label("is division of")
                .test({
                    name: 'test is division of recursion',
                    test: function (is_division_of) {
                        const pending_array_with_is_division_of_obj = [];
                        while (is_division_of) {
                            if (Array.isArray(is_division_of)) {
                                for (let i = 0; i < is_division_of.length; i++) {
                                    const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                    Division_of_schema(actorLabel + " ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                    const isDivisionOf = is_division_of[i]["is_division_of"];
                                    if (isDivisionOf && isDivisionOf.length > 0) {
                                        pending_array_with_is_division_of_obj.push(isDivisionOf);
                                    }
                                }
                            } else {
                                const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                Division_of_schema(actorLabel + " ").validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                const isDivisionOf = is_division_of["is_division_of"];
                                if (isDivisionOf && isDivisionOf.length > 0) {
                                    pending_array_with_is_division_of_obj.push(isDivisionOf);
                                }
                            }
                            is_division_of = pending_array_with_is_division_of_obj.shift()
                        }
                        return true;
                    }
                })
        ).notRequired().nullable().default(null)
    }
}

export const funding_project_validation = (fundingLabel) => {
    return {
        project_name: validateObject(`${fundingLabel} Project name`, 300, generalMessage(`${fundingLabel} Project name`)).nullable().default(null),
        project_short_name: validateObjectOptional(`${fundingLabel} Project short name`, 100, generalMessage(`${fundingLabel} Project short name`)).nullable().default(null),
        project_identifier: yup.array().of(
            yup.object().shape({
                project_identifier_scheme: yup.string().label(`${fundingLabel} project identifier scheme`).required(generalMessage(`${fundingLabel} project identifier scheme`)),
                value: yup.string().label(`${fundingLabel} project identifier value`).max(1000).required(generalMessage(`${fundingLabel} project identifier value`))
            }).label(`${fundingLabel} project identifier`).required().label(`${fundingLabel} project identifier`)
        ).notRequired().nullable().default(null).label(`${fundingLabel} project identifier`),
        website: yup.array().of(
            yup.string().url(`${fundingLabel} website must point to a valid URL`).max(1000).label("").nullable().default(null)
        ).notRequired().label("Website").nullable().default(null).notRequired(),
        grant_number: yup.string().label(`${fundingLabel} Grant number`).max(1000).notRequired().nullable().default(null),
        funding_type: yup.array().of(
            yup.string().label(`${fundingLabel} funding type`).notRequired().nullable().default(null)
        ).notRequired().label(`${fundingLabel} funding type`).nullable().default(null).notRequired(),
        funder: yup.array().of(
            yup.object().required(`${fundingLabel} Funder`).shape(actorValidation(`${fundingLabel} Funder`))
        ).label(`${fundingLabel} Funder`).notRequired().nullable().default(null),
    }
}

export const lr_validation = (parentLabel) => {
    return {
        resource_name: validateObject(`${parentLabel} Resource name`, 300, generalMessage(`${parentLabel} Resource name`)).nullable().default(null),
        lr_identifier: yup.array().of(
            yup.object().shape({
                lr_identifier_scheme: yup.string().label(`${parentLabel} Language Resource identifier scheme`).required(generalMessage(`${parentLabel} Language Resource identifier scheme`)),
                value: yup.string().label(`${parentLabel} Language Resource identifier value`).required(generalMessage(`${parentLabel} Language Resource identifier value`)).max(1000)
            }).label(`${parentLabel} Language Resource identifier scheme`)
        ).notRequired().label(`${parentLabel} Language Resource identifier scheme`).nullable().default(null),
        version: yup.string().label(`${parentLabel} Version`).max(50).notRequired().nullable().default(null),
    }
}

export const source_of_metadata_record_validation = (parentLabel) => {
    return {
        repository_name: validateObject(`${parentLabel} Repository name`, 200, generalMessage(`${parentLabel} Repository name`)).nullable().default(null),
        repository_identifier: yup.array().of(
            yup.object().shape({
                repository_identifier_scheme: yup.string().label(`${parentLabel} Repository identifier scheme`).required(generalMessage(`${parentLabel} Repository identifier scheme`)),
                value: yup.string().label(`${parentLabel} Repository identifier value`).required(generalMessage(`${parentLabel} Repository identifier value`)).max(1000)
            }).label(`${parentLabel} Repository identifier`)
        ).notRequired().label(`${parentLabel} Language Resource identifier`).nullable().default(null),
        repository_url: yup.string().url().label(`${parentLabel} Repository URL`).notRequired().default(null).nullable().max(1000),
        repository_institution: yup.object().shape({
            organization_name: validateObject(`${parentLabel} Institution organization name`, 300, generalMessage(`${parentLabel} Institution organization name`)).required().nullable().default(null),
            organization_short_name: yup.array().of(
                validateObjectOptional(`${parentLabel} Institution organization short name`, 100, generalMessage(`${parentLabel} Institution organization short name`)).notRequired().nullable().default(null)
            ).notRequired().label(`${parentLabel} Institution organization short name`).nullable().default(null),
            organization_identifier: yup.array().of(
                yup.object().shape({
                    organization_identifier_scheme: yup.string().label(`${parentLabel} Institution organization identifier scheme`).required(generalMessage(`${parentLabel} Institution organization identifier scheme`)).nullable().default(null),
                    value: yup.string().label(`${parentLabel} Institution organization identifier value`).max(1000).required(generalMessage(`${parentLabel} Institution organization identifier value`)).nullable().default(null)
                }).label(`${parentLabel} Institution organization identifier`).required().label(`${parentLabel} Institution organization identifier`)
            ).notRequired().nullable().default(null).label(`${parentLabel} Institution organization identifier`).nullable().default(null),
            website: yup.array().of(
                yup.string().url(`${parentLabel} Institution organization website must point to a valid URL`).max(1000).label("").nullable().default(null)
            ).notRequired().label("Institution website").nullable().default(null).notRequired(),
            is_division_of: yup.array().of(
                yup.object().notRequired().nullable().default(null).label(`${parentLabel} Institution is division of`)
                    .test({
                        name: 'test Institution is division of recursion',
                        test: function (is_division_of) {
                            const pending_array_with_is_division_of_obj = [];
                            while (is_division_of) {
                                if (Array.isArray(is_division_of)) {
                                    for (let i = 0; i < is_division_of.length; i++) {
                                        const { organization_name, organization_short_name, organization_identifier, website } = is_division_of[i];
                                        Division_of_schema(`${parentLabel} Institution `).validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                        const isDivisionOf = is_division_of[i]["is_division_of"];
                                        if (isDivisionOf && isDivisionOf.length > 0) {
                                            pending_array_with_is_division_of_obj.push(isDivisionOf);
                                        }
                                    }
                                } else {
                                    const { organization_name, organization_short_name, organization_identifier, website } = is_division_of;
                                    Division_of_schema(`${parentLabel} Institution `).validateSync({ organization_name, organization_short_name, organization_identifier, website });
                                    const isDivisionOf = is_division_of["is_division_of"];
                                    if (isDivisionOf && isDivisionOf.length > 0) {
                                        pending_array_with_is_division_of_obj.push(isDivisionOf);
                                    }
                                }
                                is_division_of = pending_array_with_is_division_of_obj.shift()
                            }
                            return true;
                        }
                    })
            ).notRequired().nullable().default(null)
        }).notRequired().nullable().default(null).label(`${parentLabel} Repository institution`)
    }
}

const textType_validation = (parentLabel) => {
    return {
        category_label: validateObject(`${parentLabel} Category label`, 150, generalMessage(`${parentLabel} Category label`)).nullable().default(null),
        text_type_identifier: yup.object().shape({
            text_type_classification_scheme: yup.string().label(`${parentLabel} Text type classification scheme`).required(generalMessage(`${parentLabel} Text type classification scheme`)),
            value: yup.string().label(`${parentLabel} Text type identifier value`).required(generalMessage(`${parentLabel} Text type identifier value`)).max(1000)
        }).notRequired().default(null).nullable().label(`${parentLabel} Text type identifier`)
    }
}

const audio_genre_validation = (parentLabel) => {
    return {
        category_label: validateObject(`${parentLabel} Category label`, 150, generalMessage(`${parentLabel} Category label`)).nullable().default(null),
        audio_genre_identifier: yup.object().shape({
            audio_genre_classification_scheme: yup.string().label(`${parentLabel} Audio genre classification scheme`).required(generalMessage(`${parentLabel} Audio genre classification scheme`)),
            value: yup.string().label(`${parentLabel} Audio genre identifier value`).required(generalMessage(`${parentLabel} Audio genre identifier value`)).max(1000)
        }).notRequired().default(null).nullable().label(`${parentLabel} Audio genre identifier`)
    }
}

const speech_genre_validation = (parentLabel) => {
    return {
        category_label: validateObject(`${parentLabel} Category label`, 150, generalMessage(`${parentLabel} Category label`)).nullable().default(null),
        speech_genre_identifier: yup.object().shape({
            speech_genre_classification_scheme: yup.string().label(`${parentLabel} Speech genre classification scheme`).required(generalMessage(`${parentLabel} Speech genre classification scheme`)),
            value: yup.string().label(`${parentLabel} Speech genre identifier value`).required(generalMessage(`${parentLabel} Speech genre identifier value`)).max(1000)
        }).notRequired().default(null).nullable().label(`${parentLabel} Speech genre identifier`)
    }
}

const video_genre_validation = (parentLabel) => {
    return {
        category_label: validateObject(`${parentLabel} Category label`, 150, generalMessage(`${parentLabel} Category label`)).nullable().default(null),
        video_genre_identifier: yup.object().shape({
            video_genre_classification_scheme: yup.string().label(`${parentLabel} Video genre classification scheme`).required(generalMessage(`${parentLabel} Video genre classification scheme`)),
            value: yup.string().label(`${parentLabel} Video genre identifier value`).required(generalMessage(`${parentLabel} Video genre identifier value`)).max(1000)
        }).notRequired().default(null).nullable().label(`${parentLabel} Video genre identifier`)
    }
}

const image_genre_validation = (parentLabel) => {
    return {
        category_label: validateObject(`${parentLabel} Category label`, 150, generalMessage(`${parentLabel} Category label`)).nullable().default(null),
        image_genre_identifier: yup.object().shape({
            image_genre_classification_scheme: yup.string().label(`${parentLabel} Image genre classification scheme`).required(generalMessage(`${parentLabel} Image genre classification scheme`)),
            value: yup.string().label(`${parentLabel} Image genre identifier value`).required(generalMessage(`${parentLabel} Image genre identifier value`)).max(1000)
        }).notRequired().default(null).nullable().label(`${parentLabel} Image genre identifier`)
    }
}

export const document_validation = (parentLabel) => {
    return {
        title: validateObject(`${parentLabel}'s field Title`, 500, generalMessage(`${parentLabel}'s  field Title`)).nullable().default(null),
        document_identifier: yup.array().of(
            yup.object().shape({
                document_identifier_scheme: yup.string().label(`${parentLabel}'s field document identifier scheme`).required(generalMessage(`${parentLabel} document identifier scheme`)),
                value: yup.string().label(`${parentLabel} 's document identifier value`).required(generalMessage(`${parentLabel}'s field document identifier value`)).max(1000)
            }).label(`${parentLabel}'s document identifier`)
        ).notRequired().label(`${parentLabel}'s document identifier`).nullable().default(null),
        bibliographic_record: yup.string().notRequired().nullable().default(null).max(5000).label(`${parentLabel} 's bibliographic record`)
    }
}

export const licence_validation = (parentLabel) => {
    return {
        licence_terms_name: validateObject(`${parentLabel}'s field Licence/terms of use/service name`, 200, generalMessage(`${parentLabel} s field Licence/terms of use/service name`)).nullable().default(null),
        licence_identifier: yup.array().of(
            yup.object().shape({
                licence_identifier_scheme: yup.string().label(`${parentLabel}'s field licence identifier scheme`).required(generalMessage(`${parentLabel}'s field licence identifier scheme`)).nullable().default(null),
                value: yup.string().label(`${parentLabel}'s field licence identifier value`).required(generalMessage(`${parentLabel}'s field licence identifier value`)).max(1000).nullable().default(null)
            }).label(`${parentLabel} licence identifier`)
        ).notRequired().label(`${parentLabel} 's field licence identifier`).nullable().default(null),
        condition_of_use: yup.array().of(
            yup.string().required().label(`${parentLabel}'s field condition of use`)
        ).required().nullable().default(null).label(`${parentLabel}'s field condition of use`),
        licence_terms_url: yup.array().of(
            yup.string().url().label(`${parentLabel}'s field Licence/terms of use/service URL`).required().default(null).nullable()
        ).required().nullable().default(null).label(`${parentLabel}'s field Licence/terms of use/service URL`)
    }
}

export const costValidation = (label) => {
    return {
        amount: yup.number().positive().min(0).required(generalMessage(label)).label(label + " amount").nullable().default(null),
        currency: yup.string().required(generalMessage(label + " currency")).label(label + " currency").nullable().default(null)
    }
};

export const language_validation = (parentLabel) => {
    return {
        glottolog_code: yup.string().max(10).label(`${parentLabel} Glottolog code`).nullable().default(null).when('language_id', (language_id, schema) => {
            return language_id === "mis" ? schema.required() : schema.notRequired();
        }),
        language_tag: yup.string().required().label(`${parentLabel} Language`).nullable().default(null),
        language_id: yup.string().required().label(`${parentLabel} Language identifier`).nullable().default(null),
        script_id: yup.string().notRequired().label(`${parentLabel} Language Script identifier`).nullable().default(null),
        region_id: yup.string().notRequired().label(`${parentLabel} Language Region identifier`).nullable().default(null),
        //variant_id: yup.string().notRequired().label(`${parentLabel} Language Variant identifier`).nullable().default(null),
        variant_id: yup.array().of(
            yup.string().notRequired().label(`${parentLabel} Language Variant identifier`).nullable().default(null),
        ).label(`${parentLabel} Language Variant identifier`).nullable().default(null).notRequired(),
        language_variety_name: validateObjectOptional(`${parentLabel} Language variety name`, 150).notRequired().nullable().default(null),
    }
}

export const intput_content_validation = (parentLabel, element, requiredLanguage) => {
    if (requiredLanguage) {
        return {
            processing_resource_type: yup.string().label(`${parentLabel} ${element.processing_resource_type.label}`).required(`${parentLabel} ${element.processing_resource_type.label} is required`).nullable().default(null),
            //samples_location: yup.string().url(`${parentLabel} ${element.samples_location.label} is not a valid url`).max(1000).label("").notRequired().nullable().default(null),
            sample: yup.array().of(
                yup.object().notRequired().nullable().default(null).shape({
                    sample_text: yup.string().label(`${parentLabel} ${element.sample.child.children.sample_text.label}`).notRequired().nullable().default(null).max(1000),
                    samples_location: yup.string().url(`${parentLabel} ${element.sample.child.children.samples_location.label} is not a valid url`).max(1000).label("").notRequired().nullable().default(null),
                    tag: yup.string().label(`${parentLabel} ${element.sample.child.children.tag.label}`).notRequired().nullable().default(null).max(100),
                }),
            ).label(`${parentLabel} ${element.sample.label}`).notRequired().nullable().default(null),
            language: yup.array().of(
                yup.object().required().shape(language_validation(`${parentLabel}`)),
            ).label(`${parentLabel} ${element.language.label}`).required().nullable().default(null),
            media_type: yup.string().label(`${parentLabel} ${element.media_type.label}`).notRequired().nullable().default(null),
            data_format: yup.array().of(
                yup.string().label(`${parentLabel} ${element.data_format.label}`).required().nullable().default(null)
            ).label(`${parentLabel} ${element.data_format.label}`).notRequired().nullable().default(null),
            character_encoding: yup.array().of(
                yup.string().label(`${parentLabel} ${element.character_encoding.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.character_encoding.label}`).notRequired().nullable().default(null),
            annotation_type: yup.array().of(
                yup.string().label(`${parentLabel} ${element.annotation_type.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.annotation_type.label}`).notRequired().nullable().default(null),
            segmentation_level: yup.array().of(
                yup.string().label(`${parentLabel} ${element.segmentation_level.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.segmentation_level.label}`).notRequired().nullable().default(null),
            typesystem: yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${element.typesystem.label}`)),
            annotation_schema: yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${element.annotation_schema.label}`)),
            annotation_resource: yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${element.annotation_resource.label}`)),
            modality_type: yup.array().of(
                yup.string().label(`${parentLabel} ${element.modality_type.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.modality_type.label}`).notRequired().nullable().default(null),
            modality_type_details: validateObjectOptional(`${parentLabel} ${element.modality_type_details.label}`, 500).notRequired().nullable().default(null),
        }
    } else {
        return {
            processing_resource_type: yup.string().label(`${parentLabel} ${element.processing_resource_type.label}`).required(`${parentLabel} ${element.processing_resource_type.label} is required`).nullable().default(null),
            //samples_location: yup.string().url(`${parentLabel} ${element.samples_location.label} is not a valid url`).max(1000).label("").notRequired().nullable().default(null),
            sample: yup.array().of(
                yup.object().notRequired().nullable().default(null).shape({
                    sample_text: yup.string().label(`${parentLabel} ${element.sample.child.children.sample_text.label}`).notRequired().nullable().default(null).max(1000),
                    samples_location: yup.string().url(`${parentLabel} ${element.sample.child.children.samples_location.label} is not a valid url`).max(1000).label("").notRequired().nullable().default(null),
                    tag: yup.string().label(`${parentLabel} ${element.sample.child.children.tag.label}`).notRequired().nullable().default(null).max(100),
                }),
            ).label(`${parentLabel} ${element.sample.label}`).notRequired().nullable().default(null),
            language: yup.array().of(
                yup.object().notRequired().nullable().default(null).shape(language_validation(`${parentLabel}`)).nullable().default(null),
            ).label(`${parentLabel} ${element.language.label}`).notRequired().nullable().default(null),
            media_type: yup.string().label(`${parentLabel} ${element.media_type.label}`).notRequired().nullable().default(null),
            data_format: yup.array().of(
                yup.string().label(`${parentLabel} ${element.data_format.label}`).required().nullable().default(null)
            ).label(`${parentLabel} ${element.data_format.label}`).notRequired().nullable().default(null),
            character_encoding: yup.array().of(
                yup.string().label(`${parentLabel} ${element.character_encoding.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.character_encoding.label}`).notRequired().nullable().default(null),
            annotation_type: yup.array().of(
                yup.string().label(`${parentLabel} ${element.annotation_type.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.annotation_type.label}`).notRequired().nullable().default(null),
            segmentation_level: yup.array().of(
                yup.string().label(`${parentLabel} ${element.segmentation_level.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.segmentation_level.label}`).notRequired().nullable().default(null),
            typesystem: yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${element.typesystem.label}`)),
            annotation_schema: yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${element.annotation_schema.label}`)),
            annotation_resource: yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${element.annotation_resource.label}`)),
            modality_type: yup.array().of(
                yup.string().label(`${parentLabel} ${element.modality_type.label}`).required().nullable().default(null),
            ).label(`${parentLabel} ${element.modality_type.label}`).notRequired().nullable().default(null),
            modality_type_details: validateObjectOptional(`${parentLabel} ${element.modality_type_details.label}`, 500).notRequired().nullable().default(null),
        }
    }
}

export const mediaTypeValidator = (parentLabel, schema_text_part, schema_audio_part, schema_video_part, schema_text_numerical, schema_image_part) => {
    return {
        name: 'media type validation',
        test: async function (corpus_media_part_array) {
            for (let index = 0; corpus_media_part_array && index < corpus_media_part_array.length; index++) {
                const mediaPart = corpus_media_part_array[index];
                const { media_type } = mediaPart;
                let result = null;
                switch (media_type) {
                    case "http://w3id.org/meta-share/meta-share/text":
                        result = await text_part(parentLabel, schema_text_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/audio":
                        result = await audio_part(parentLabel, schema_audio_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/textNumerical":
                        result = await text_numerical_part(parentLabel, schema_text_numerical, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/video":
                        result = await text_video_part(parentLabel, schema_video_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/image":
                        result = await text_image_part(parentLabel, schema_image_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    default:
                        break;
                }
            }
            return true;
        }
    }
}
const text_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Corpus text part > </strong>");
    const schema = yup.object().shape({
        corpus_media_type: yup.string().notRequired().default(null).nullable().label("corpus media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        //linguality_type: yup.string().required().default(null).nullable().label(parentLabel + part.linguality_type.label),
        /*multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('linguality_type', (linguality_type, schema) => {
            return linguality_type === "http://w3id.org/meta-share/meta-share/bilingual" || linguality_type === "http://w3id.org/meta-share/meta-share/multilingual" ? schema.required() : schema.notRequired();
        }),*/
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.required() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).required().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        text_type: yup.array().of(
            yup.object().nullable().default(null).shape(textType_validation(parentLabel))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.text_type.label}`),
        annotation: yup.array().of(
            yup.object().nullable().default(null).shape(annotationValidation(parentLabel, part.annotation.child.children))
        ).label(`${parentLabel} ${part.annotation.label}`),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const audio_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Corpus audio part > </strong>");
    const schema = yup.object().shape({
        corpus_media_type: yup.string().notRequired().default(null).nullable().label("corpus media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        /*linguality_type: yup.string().required().default(null).nullable().label(parentLabel + part.linguality_type.label),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('linguality_type', (linguality_type, schema) => {
            return linguality_type === "http://w3id.org/meta-share/meta-share/bilingual" || linguality_type === "http://w3id.org/meta-share/meta-share/multilingual" ? schema.required() : schema.notRequired();
        }),*/
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.required() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).required().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        audio_genre: yup.array().of(
            yup.object().nullable().default(null).shape(audio_genre_validation(parentLabel))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.audio_genre.label}`),
        speech_genre: yup.array().of(
            yup.object().nullable().default(null).shape(speech_genre_validation(parentLabel))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.speech_genre.label}`),
        number_of_participants: yup.number().notRequired().nullable().default(null).max(part.number_of_participants.max_value).min(part.number_of_participants.min_value).label(part.number_of_participants.label),
        dialect_accent_of_participants: yup.array().of(
            validateObjectOptional(`${parentLabel} ${part.dialect_accent_of_participants.label}`, part.dialect_accent_of_participants.max_length).notRequired().nullable().default(null),
        ).nullable().default(null).notRequired().label(`${parentLabel} ${part.dialect_accent_of_participants.label}`),
        geographic_distribution_of_participants: yup.array().of(
            validateObjectOptional(`${parentLabel} ${part.geographic_distribution_of_participants.label}`, part.geographic_distribution_of_participants.max_length).notRequired().nullable().default(null),
        ).nullable().default(null).notRequired().label(`${parentLabel} ${part.geographic_distribution_of_participants.label}`),
        annotation: yup.array().of(
            yup.object().nullable().default(null).shape(annotationValidation(parentLabel, part.annotation.child.children))
        ).label(`${parentLabel} ${part.annotation.label}`),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const text_numerical_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Corpus text numerical part > </strong>");
    const schema = yup.object().shape({
        corpus_media_type: yup.string().notRequired().default(null).nullable().label("corpus media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        type_of_text_numerical_content: yup.array().required().nullable().default(null).of(
            validateObject(parentLabel + part.type_of_text_numerical_content.label, part.type_of_text_numerical_content.max_length)
        ).label(parentLabel + part.type_of_text_numerical_content.label),
        number_of_participants: yup.number().notRequired().nullable().default(null).max(part.number_of_participants.max_value).min(part.number_of_participants.min_value).label(part.number_of_participants.label),
        dialect_accent_of_participants: yup.array().of(
            validateObjectOptional(`${parentLabel} ${part.dialect_accent_of_participants.label}`, part.dialect_accent_of_participants.max_length).notRequired().nullable().default(null),
        ).nullable().default(null).notRequired().label(`${parentLabel} ${part.dialect_accent_of_participants.label}`),
        geographic_distribution_of_participants: yup.array().of(
            validateObjectOptional(`${parentLabel} ${part.geographic_distribution_of_participants.label}`, part.geographic_distribution_of_participants.max_length).notRequired().nullable().default(null),
        ).nullable().default(null).notRequired().label(`${parentLabel} ${part.geographic_distribution_of_participants.label}`),
        annotation: yup.array().of(
            yup.object().nullable().default(null).shape(annotationValidation(parentLabel, part.annotation.child.children))
        ).label(`${parentLabel} ${part.annotation.label}`),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const text_video_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Corpus video part > </strong>");
    const schema = yup.object().shape({
        corpus_media_type: yup.string().notRequired().default(null).nullable().label("corpus media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        /*linguality_type: yup.string().required().default(null).nullable().label(parentLabel + part.linguality_type.label),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('linguality_type', (linguality_type, schema) => {
            return linguality_type === "http://w3id.org/meta-share/meta-share/bilingual" || linguality_type === "http://w3id.org/meta-share/meta-share/multilingual" ? schema.required() : schema.notRequired();
        }),*/
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.required() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        type_of_video_content: yup.array().required().nullable().default(null).of(
            validateObject(parentLabel + part.type_of_video_content.label, part.type_of_video_content.max_length)
        ).label(parentLabel + part.type_of_video_content.label),
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).required().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        video_genre: yup.array().of(
            yup.object().nullable().default(null).shape(video_genre_validation(parentLabel))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.video_genre.label}`),
        number_of_participants: yup.number().notRequired().nullable().default(null).max(part.number_of_participants.max_value).min(part.number_of_participants.min_value).label(part.number_of_participants.label),
        dialect_accent_of_participants: yup.array().of(
            validateObjectOptional(`${parentLabel} ${part.dialect_accent_of_participants.label}`, part.dialect_accent_of_participants.max_length).notRequired().nullable().default(null),
        ).nullable().default(null).notRequired().label(`${parentLabel} ${part.dialect_accent_of_participants.label}`),
        geographic_distribution_of_participants: yup.array().of(
            validateObjectOptional(`${parentLabel} ${part.geographic_distribution_of_participants.label}`, part.geographic_distribution_of_participants.max_length).notRequired().nullable().default(null),
        ).nullable().default(null).notRequired().label(`${parentLabel} ${part.geographic_distribution_of_participants.label}`),
        annotation: yup.array().of(
            yup.object().nullable().default(null).shape(annotationValidation(parentLabel, part.annotation.child.children))
        ).label(`${parentLabel} ${part.annotation.label}`),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const text_image_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Corpus image part > </strong>");
    const schema = yup.object().shape({
        corpus_media_type: yup.string().notRequired().default(null).nullable().label("corpus media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        /*linguality_type: yup.string().notRequired().default(null).nullable().label(parentLabel + part.linguality_type.label),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('linguality_type', (linguality_type, schema) => {
            return linguality_type === "http://w3id.org/meta-share/meta-share/bilingual" || linguality_type === "http://w3id.org/meta-share/meta-share/multilingual" ? schema.required() : schema.notRequired();
        }),*/
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.required() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        type_of_image_content: yup.array().required().nullable().default(null).of(
            validateObject(parentLabel + part.type_of_image_content.label, part.type_of_image_content.max_length)
        ).label(parentLabel + part.type_of_image_content.label),
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        image_genre: yup.array().of(
            yup.object().nullable().default(null).shape(image_genre_validation(parentLabel))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.image_genre.label}`),
        annotation: yup.array().of(
            yup.object().nullable().default(null).shape(annotationValidation(parentLabel, part.annotation.child.children))
        ).label(`${parentLabel} ${part.annotation.label}`),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const annotationValidation = (parentLabel, a) => {
    return {
        annotation_type: yup.array().required().nullable().default(null).of(yup.string().nullable().default(null).label(parentLabel + a.annotation_type.label)).label(parentLabel + a.annotation_type.label),
        annotated_element: yup.array().notRequired().nullable().default(null).of(yup.string().nullable().default(null).label(parentLabel + a.annotated_element.label)).label(parentLabel + a.annotated_element.label),
        segmentation_level: yup.array().notRequired().nullable().default(null).of(yup.string().nullable().default(null).label(parentLabel + a.segmentation_level.label)).label(parentLabel + a.segmentation_level.label),
        annotation_standoff: yup.boolean().notRequired().nullable().default(null).label(parentLabel + a.segmentation_level.label).label(parentLabel + a.annotation_standoff.label),
        guidelines: yup.array().of(
            yup.object().required(
                generalMessage(parentLabel + a.guidelines.label)
            ).shape(
                document_validation(parentLabel + a.guidelines.label)
            )
        ).nullable().default(null).notRequired().label(parentLabel + a.guidelines.label),
        /*tagset: yup.array().of(
            yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${a.tagset.label}`)),
        ).nullable().default(null).notRequired().label(parentLabel + a.tagset.label),*///removed at m2.3
        theoretic_model: validateObjectOptional(`${parentLabel} ${a.theoretic_model.label}`, 200).notRequired().nullable().default(null),
        annotation_mode: yup.string().nullable().default(null).notRequired().label(parentLabel + a.annotation_mode.label),
        annotation_mode_details: validateObjectOptional(`${parentLabel} ${a.annotation_mode_details.label}`, 1000).notRequired().nullable().default(null),
        is_annotated_by: yup.array().of(
            yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${a.is_annotated_by.label}`)),
        ).nullable().default(null).notRequired().label(parentLabel + a.is_annotated_by.label),
        typesystem: yup.array().of(
            yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${a.typesystem.label}`)),
        ).nullable().default(null).notRequired().label(parentLabel + a.typesystem.label),
        annotation_resource: yup.array().of(
            yup.object().notRequired().nullable().default(null).shape(lr_validation(`${parentLabel} ${a.annotation_resource.label}`)),
        ).nullable().default(null).notRequired().label(parentLabel + a.annotation_resource.label),
        annotator: yup.array().of(
            yup.object().required(generalMessage(parentLabel + a.annotator.label)).shape(actorValidation(parentLabel + a.annotator.label)).default(null).nullable()
        ).label(parentLabel + a.annotator.label).notRequired().nullable().default(null),
        interannotator_agreement: validateObjectOptional(`${parentLabel} ${a.interannotator_agreement.label}`, 300).notRequired().nullable().default(null),
        intraannotator_agreement: validateObjectOptional(`${parentLabel} ${a.intraannotator_agreement.label}`, 300).notRequired().nullable().default(null),
        annotation_report: yup.array().of(
            yup.object().required(
                generalMessage(parentLabel + a.annotation_report.label)
            ).shape(
                document_validation(parentLabel + a.annotation_report.label)
            )
        ).nullable().default(null).notRequired().label(parentLabel + a.annotation_report.label),
        annotation_start_date: yup.date().label(parentLabel + a.annotation_start_date.label).notRequired().nullable().default(null),
        annotation_end_date: yup.date().min(yup.ref('annotation_start_date'), "The annotation end date could not be before the corresponding start date ").label(parentLabel + a.annotation_end_date.labael).notRequired().nullable().default(null),
    }
}

export const sizeValidation = (parentLabel, s) => {
    return {
        amount: yup.number().required().default(null).nullable().min(0).label(parentLabel + s.amount.label),
        size_unit: yup.string().required().default(null).nullable().label(parentLabel + s.size_unit.label)
    }
}