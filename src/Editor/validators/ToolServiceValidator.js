import * as yup from "yup";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { validateObject, validateObjectOptional, generalMessage } from "./OrganizationValidator";
import { actorValidation, funding_project_validation, lr_validation, document_validation, intput_content_validation, licence_validation, costValidation, isValidURL, source_of_metadata_record_validation } from "./commonValidatorSchemas";
import {
    SERVICETOOL_TOP_TABS_HEADERS as se,//section
    SERVICE_FIRST_SECTION_TABS_HEADERS as f,//first 
    SERVICE_SECOND_SECTION_TABS_HEADERS as s,//second
    SERVICE_THIRD_SECTION_TABS_HEADERS as t,//third
    //SERVICE_FOURTH_SECTION_TABS_HEADERS as fo,//fourth
} from "../../config/editorConstants";
import {
    DOCKER_IMAGE, EXECUTABLE_CODE, LIBRARY, OTHER, PLUGIN, SOURCE_AND_EXECUTABLE, SOURCE_CODE,
    UNSPECIFIED, WEB_SERVICE, WORK_FLOW,
    //GALAXY_WORKFLOW
} from "../../config/editorConstants";
const p = GenericSchemaParser;

//const fieldLocation = (step, tab) => `<em style="color: black"> Step ${step} > Tab ${tab} >  </em> `;
const fieldLocation = (step, tab) => `<strong style="color: black">  ${step} >  ${tab} >  </strong> `;

export const validateToolService = (model, data, subclass, functional_service) => {
    const schema = yup.object().shape({
        source_of_metadata_record: yup.object().notRequired().nullable().default(null).shape(
            source_of_metadata_record_validation(fieldLocation(se[0], f[0]) + "Source of metadata record")
        ).label(fieldLocation(se[0], f[0]) + "Source of metadata record"),
        described_entity: yup.object().shape({
            entity_type: yup.string().label("Entity type").notRequired().max(1000).default(null).nullable(),
            lr_identifier: yup.array().of(
                yup.object().shape({
                    lr_identifier_scheme: yup.string().label(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.label).required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).formElements.lr_identifier_scheme.label)).default(null).nullable(),
                    value: yup.string().label(fieldLocation(se[0], f[0]) + "Language Resource identifier value").required(generalMessage(fieldLocation(se[0], f[0]) + "Language Resource identifier value")).max(1000).default(null).nullable()
                }).label(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).label).default(null).nullable()
            ).notRequired().label(fieldLocation(se[0], f[0]) + p.getFormElement("lr_identifier", data.lr_identifier).label).nullable().default(null),
            resource_name: validateObject(fieldLocation(se[0], f[0]) + `${p.getFormElement("resource_name", data.resource_name).label}`, 1000).default(null).nullable(),
            resource_short_name: validateObjectOptional(fieldLocation(se[0], f[0]) + p.getFormElement("resource_short_name", data.resource_short_name).label, 300).notRequired().nullable().default(null),
            description: validateObject(fieldLocation(se[0], f[0]) + `${p.getFormElement("description", data.description).label}`, 10000).default(null).nullable(),
            version: yup.string().label(fieldLocation(se[0], f[0]) + `${p.getFormElement("version", data.version).label}`).notRequired().max(50).nullable().default(null),
            version_date: yup.date().label(fieldLocation(se[0], f[0]) + p.getFormElement("version_date", data.version_date).label).notRequired().nullable().default(null),
            resource_provider: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label)).shape(actorValidation(fieldLocation(se[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label)).default(null).nullable(),
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("resource_provider", data.resource_provider).label).notRequired().nullable().default(null),
            resource_creator: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label)).shape(actorValidation(fieldLocation(se[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label)).default(null).nullable()
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("resource_creator", data.resource_creator).label).notRequired().nullable().default(null),
            publication_date: yup.date().label(fieldLocation(se[0], f[0]) + p.getFormElement("publication_date", data.publication_date).label).notRequired().nullable().default(null),
            funding_project: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label)).shape(funding_project_validation(fieldLocation(se[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label)).default(null).nullable()
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("funding_project", data.funding_project).label).notRequired().nullable().default(null),
            logo: yup.string().label("").url(fieldLocation(se[0], f[0]) + "Language resource logo must be a valid URL").nullable().default(null).notRequired(),
            creation_start_date: yup.date().label(fieldLocation(se[0], f[0]) + p.getFormElement("creation_start_date", data.creation_start_date).label).notRequired().nullable().default(null),
            creation_end_date: yup.date().min(yup.ref('creation_start_date'), "The Language resource creation start date could not be before the creation start date " + model.described_entity.creation_start_date).label(fieldLocation(se[0], f[0]) + p.getFormElement("creation_end_date", data.creation_end_date).label).notRequired().nullable().default(null),
            creation_mode: yup.string().label(fieldLocation(se[0], f[0]) + p.getFormElement("creation_mode", data.creation_mode).label).notRequired().nullable().default(null),
            creation_details: validateObjectOptional(fieldLocation(se[0], f[0]) + p.getFormElement("creation_details", data.creation_details).label, 3000).notRequired().nullable().default(null),
            original_source_description: validateObjectOptional(fieldLocation(se[0], f[0]) + p.getFormElement("original_source_description", data.original_source_description).label, 500).notRequired().nullable().default(null),
            update_frequency: validateObjectOptional(fieldLocation(se[0], f[0]) + p.getFormElement("update_frequency", data.update_frequency).label, 100).notRequired().nullable().default(null),
            revision: validateObjectOptional(fieldLocation(se[0], f[0]) + p.getFormElement("revision", data.revision).label, 500).notRequired().nullable().default(null),
            //citation_text: validateObjectOptional(fieldLocation(se[0], f[0]) + p.getFormElement("citation_text", data.citation_text).label).notRequired().nullable().default(null),
            is_created_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("is_created_by", data.is_created_by).label)).shape(lr_validation(fieldLocation(se[0], f[0]) + p.getFormElement("is_created_by", data.is_created_by).label))
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("is_created_by", data.is_created_by).label).notRequired().nullable().default(null),
            has_original_source: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("has_original_source", data.has_original_source).label)).shape(lr_validation(fieldLocation(se[0], f[0]) + p.getFormElement("has_original_source", data.has_original_source).label))
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("has_original_source", data.has_original_source).label).notRequired().nullable().default(null),
            ipr_holder: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[0]) + p.getFormElement("ipr_holder", data.ipr_holder).label)).shape(actorValidation(fieldLocation(se[0], f[0]) + p.getFormElement("ipr_holder", data.ipr_holder).label))
            ).label(fieldLocation(se[0], f[0]) + p.getFormElement("ipr_holder", data.ipr_holder).label).notRequired().nullable().default(null),
            //
            intended_application: yup.array().of(
                yup.string().label(fieldLocation(se[0], f[1]) + p.getFormElement("intended_application", data.intended_application).label).notRequired().max(200).default(null).nullable(),
            ).label(fieldLocation(se[0], f[1]) + p.getFormElement("intended_application", data.intended_application).label).notRequired().nullable().default(null),
            complies_with: yup.array().of(
                yup.string().label(fieldLocation(se[0], f[1]) + p.getFormElement("complies_with", data.complies_with).label).notRequired().default(null).nullable(),
            ).label(fieldLocation(se[0], f[1]) + p.getFormElement("complies_with", data.complies_with).label).nullable().default(null).notRequired(),
            domain: yup.array().of(
                yup.object().shape({
                    category_label: validateObject(fieldLocation(se[0], f[1]) + "Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label, 150, generalMessage("Domain " + p.getFormElement("domain", data.domain).formElements.category_label.label)).required().default(null).nullable(),
                    domain_identifier: yup.object().default(null).nullable().notRequired().shape({
                        domain_classification_scheme: yup.string().label(fieldLocation(se[0], f[1]) + "Domain classification scheme").required(generalMessage(fieldLocation(se[0], f[1]) + "Domain classification scheme")).default(null).nullable(),
                        value: yup.string().label(fieldLocation(se[0], f[1]) + "domain identifier value").max(1000).required(generalMessage(fieldLocation(se[0], f[1]) + "domain identifier value")).default(null).nullable(),
                    }).label(fieldLocation(se[0], f[1]) + "domain identifier").default(null).nullable(),
                })
            ).notRequired().label(fieldLocation(se[0], f[1]) + "Domain").nullable().default(null),
            keyword: yup.array().of(
                validateObject(fieldLocation(se[0], f[1]) + p.getFormElement('keyword', data.keyword).label, 100).required().default(null).nullable(),
            ).required().label(fieldLocation(se[0], f[1]) + `${p.getFormElement('keyword', data.keyword).label}`).nullable().default(null),
            subject: yup.array().of(
                yup.object().shape({
                    category_label: validateObject(fieldLocation(se[0], f[1]) + "Subject " + p.getFormElement("subject", data.subject).formElements.category_label.label, 150, generalMessage("Subject " + p.getFormElement("subject", data.subject).formElements.category_label.label)).required().default(null).nullable(),
                    subject_identifier: yup.object().default(null).nullable().notRequired().shape({
                        subject_classification_scheme: yup.string().label(fieldLocation(se[0], f[1]) + "Subject classification scheme").required(generalMessage(fieldLocation(se[0], f[1]) + "Subject classification scheme")).default(null).nullable(),
                        value: yup.string().label(fieldLocation(se[0], f[1]) + "Subject identifier value").max(1000).required(generalMessage(fieldLocation(se[0], f[1]) + "Subject identifier value")).default(null).nullable(),
                    }).label(fieldLocation(se[0], f[1]) + "Subject").default(null).nullable(),
                })
            ).nullable().default(null).notRequired().label(fieldLocation(se[0], f[1]) + "Subject"),
            //
            additional_info: yup.array().of(
                yup.object().shape({
                    //landing_page: yup.string().url(fieldLocation(se[0], f[2]) + `${p.getFormElement("additional_info", data.additional_info).label} website must be a valid URL`).max(1000).label(fieldLocation(se[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " website").default(null).nullable(),
                    landing_page: yup.string().max(1000).label(fieldLocation(se[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " website").default(null).nullable().test({
                        name: 'landing_page url',
                        test: function (landing_page) {
                            if (landing_page) {
                                if (!isValidURL(landing_page)) {
                                    return this.createError({ message: fieldLocation(se[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " must be a valid URL", path: "" });
                                }
                            }
                            return true;
                        },
                    }),
                    email: yup.string().email().max(254).label(fieldLocation(se[0], f[2]) + p.getFormElement("additional_info", data.additional_info).label + " email").default(null).nullable()
                })
            ).required().label(fieldLocation(se[0], f[2]) + `${p.getFormElement("additional_info", data.additional_info).label}`).default(null).nullable().test({
                name: "validate existance of at least one entry",
                test: function (additional_info) {
                    for (let index = 0; additional_info && index < additional_info.length; index++) {
                        const { landing_page, email } = additional_info[index];
                        if (!landing_page && !email) {
                            return this.createError({ message: fieldLocation(se[0], f[2]) + `${p.getFormElement("additional_info", data.additional_info).label} landing page or email must be filled`, path: "" });
                        }
                    }
                    return true;
                }
            }),
            contact: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[2]) + p.getFormElement("contact", data.contact).label)).shape(actorValidation(fieldLocation(se[0], f[2]) + p.getFormElement("contact", data.contact).label)).default(null).nullable()
            ).label(fieldLocation(se[0], f[2]) + p.getFormElement("contact", data.contact).label).notRequired().nullable().default(null),
            mailing_list_name: yup.array().of(
                yup.string().label(fieldLocation(se[0], f[2]) + p.getFormElement("mailing_list_name", data.mailing_list_name).label).max(100).default(null).nullable()
            ).label(fieldLocation(se[0], f[2]) + p.getFormElement("mailing_list_name", data.mailing_list_name).label).notRequired().nullable().default(null),
            discussion_url: yup.array().of(
                yup.string().url().label(fieldLocation(se[0], f[2]) + p.getFormElement("discussion_url", data.discussion_url).label).max(1000).default(null).nullable()
            ).label(fieldLocation(se[0], f[2]) + p.getFormElement("discussion_url", data.discussion_url).label).notRequired().nullable().default(null),
            //
            is_documented_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label)).shape(document_validation(fieldLocation(se[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label))
            ).label(fieldLocation(se[0], f[3]) + p.getFormElement("is_documented_by", data.is_documented_by).label).notRequired().nullable().default(null),
            is_to_be_cited_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_documented_by).label)).shape(document_validation(fieldLocation(se[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by).label))
            ).label(fieldLocation(se[0], f[3]) + p.getFormElement("is_to_be_cited_by", data.is_to_be_cited_by).label).notRequired().nullable().default(null),
            is_described_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[3]) + p.getFormElement("is_described_by", data.is_described_by).label)).shape(document_validation(fieldLocation(se[0], f[3]) + p.getFormElement("is_described_by", data.is_described_by).label))
            ).label(fieldLocation(se[0], f[3]) + p.getFormElement("is_described_by", data.is_described_by).label).notRequired().nullable().default(null),
            support_type: yup.string().notRequired().nullable().default(null).label(fieldLocation(se[0], f[3]) + "support type"),
            //
            relation: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).label)).shape({
                    related_lr: yup.object().nullable().default(null).required(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).formElements.related_lr.label + " required").shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).formElements.related_lr.label)),
                    relation_type: validateObject(fieldLocation(se[0], f[4]) + p.getFormElement('relation', data.relation).formElements.relation_type.label, 150).required().nullable().default(null)
                }).label(fieldLocation(se[0], f[4]) + p.getFormElement("relation", data.relation).label).notRequired().nullable().default(null)
            ).nullable().default(null),
            is_part_of: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_of", data.is_part_of).label).notRequired().nullable().default(null),
            is_part_with: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_with", data.is_part_with).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_with", data.is_part_with).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_part_with", data.is_part_with).label).notRequired().nullable().default(null),
            is_similar_to: yup.array().of(
                yup.object().required(fieldLocation(se[0], f[4]) + generalMessage(p.getFormElement("is_similar_to", data.is_similar_to).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_similar_to", data.is_similar_to).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_similar_to", data.is_similar_to).label).notRequired().nullable().default(null),
            is_related_to_lr: yup.array().of(
                yup.object().required(fieldLocation(se[0], f[4]) + generalMessage(p.getFormElement("is_related_to_lr", data.is_related_to_lr).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_related_to_lr", data.is_related_to_lr).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_related_to_lr", data.is_related_to_lr).label).notRequired().nullable().default(null),
            is_exact_match_with: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("is_exact_match_with", data.is_exact_match_with).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_exact_match_with", data.is_exact_match_with).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_exact_match_with", data.is_exact_match_with).label).notRequired().nullable().default(null),
            is_archived_by: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("is_archived_by", data.is_archived_by).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_archived_by", data.is_archived_by).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_archived_by", data.is_archived_by).label).notRequired().nullable().default(null),
            is_continuation_of: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("is_continuation_of", data.is_continuation_of).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_continuation_of", data.is_continuation_of).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("is_continuation_of", data.is_continuation_of).label).notRequired().nullable().default(null),
            replaces: yup.array().of(
                yup.object().required(generalMessage(fieldLocation(se[0], f[4]) + p.getFormElement("replaces", data.replaces).label)).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("replaces", data.replaces).label))
            ).label(fieldLocation(se[0], f[4]) + p.getFormElement("replaces", data.replaces).label).notRequired().nullable().default(null),
            is_version_of: yup.object().notRequired().nullable().default(null).shape(lr_validation(fieldLocation(se[0], f[4]) + p.getFormElement("is_version_of", data.is_version_of).label)),
            //SKIPPING ADDING LOCATION BECAUSE TAB IS MISSING
            validated: yup.boolean().notRequired().label(p.getFormElement("validated", data.validated).label).nullable(),
            actual_use: yup.array().of(
                yup.object().required().shape({
                    used_in_application: yup.array().of(yup.string().label("used in application").required().default(null).nullable()).notRequired().label("used in application").nullable().default(null),
                    has_outcome: yup.array().of(
                        yup.object().required(generalMessage(p.getFormElement("actual_use", data.actual_use).formElements.has_outcome.label)).shape(lr_validation(p.getFormElement("actual_use", data.actual_use).formElements.has_outcome.label))
                    ).label(p.getFormElement("actual_use", data.actual_use).formElements.has_outcome.label).notRequired().nullable().default(null),
                    usage_project: yup.array().of(
                        yup.object().required(generalMessage(p.getFormElement("actual_use", data.actual_use).formElements.usage_project.label)).shape(funding_project_validation(p.getFormElement("actual_use", data.actual_use).formElements.usage_project.label)).default(null).nullable()
                    ).label(p.getFormElement("actual_use", data.actual_use).formElements.usage_project.label).notRequired().nullable().default(null),
                    usage_report: yup.array().of(
                        yup.object().required(generalMessage(p.getFormElement("actual_use", data.actual_use).formElements.usage_report.label)).shape(document_validation(p.getFormElement("actual_use", data.actual_use).formElements.usage_report.label))
                    ).label(p.getFormElement("actual_use", data.actual_use).formElements.usage_report.label).notRequired().nullable().default(null),
                    actual_use_details: validateObjectOptional(p.getFormElement("actual_use", data.actual_use).formElements.actual_use_details.label, 500).notRequired().nullable().default(null),
                })
            ).notRequired().label(p.getFormElement("actual_use", data.actual_use).label).nullable().default(null),
            validation: yup.array().of(
                yup.object().required().shape({
                    validation_type: yup.string().notRequired().default(null).nullable(),
                    validation_mode: yup.string().notRequired().default(null).nullable(),
                    validation_details: validateObjectOptional(p.getFormElement("validation", data.validation).formElements.validation_details.label, 500).notRequired().nullable().default(null),
                    validation_extent: yup.string().notRequired().default(null).nullable(),
                    is_validated_by: yup.array().of(
                        yup.object().required(generalMessage(p.getFormElement("validation", data.validation).formElements.is_validated_by.label)).shape(lr_validation(p.getFormElement("validation", data.validation).formElements.is_validated_by.label)).default(null).nullable()
                    ).label(p.getFormElement("validation", data.validation).formElements.is_validated_by.label).notRequired().nullable().default(null),
                    validation_report: yup.array().of(
                        yup.object().required(generalMessage(p.getFormElement("validation", data.validation).formElements.validation_report.label)).shape(document_validation(p.getFormElement("validation", data.validation).formElements.validation_report.label)).default(null).nullable(),
                    ).label(p.getFormElement("validation", data.validation).formElements.validation_report.label).notRequired().nullable().default(null),
                    validator: yup.array().of(
                        yup.object().required(generalMessage(p.getFormElement("validation", data.validation).formElements.validator.label)).shape(actorValidation(p.getFormElement("validation", data.validation).formElements.validator.label)).default(null).nullable()
                    ).label(p.getFormElement("validation", data.validation).formElements.validator.label).notRequired().nullable().default(null),
                })
            ).notRequired().label(p.getFormElement("validation", data.validation).label).nullable().default(null),
            is_cited_by: yup.array().of(
                yup.object().required(generalMessage(p.getFormElement("is_cited_by", data.is_cited_by).label)).shape(document_validation(p.getFormElement("is_cited_by", data.is_cited_by).label))
            ).label(p.getFormElement("is_cited_by", data.is_cited_by).label).notRequired().nullable().default(null),
            is_reviewed_by: yup.array().of(
                yup.object().required(generalMessage(p.getFormElement("is_reviewed_by", data.is_reviewed_by).label)).shape(document_validation(p.getFormElement("is_reviewed_by", data.is_reviewed_by).label)).default(null).nullable()
            ).label(p.getFormElement("is_reviewed_by", data.is_reviewed_by).label).notRequired().nullable().default(null),
            ///////////////////
            lr_subclass: yup.object().required().shape({
                function: yup.array().of(
                    yup.string().label(fieldLocation(se[1], s[0]) + p.getFormElement("function", subclass.function).label).required().max(200).default(null).nullable()
                ).label(fieldLocation(se[1], s[0]) + `${p.getFormElement("function", subclass.function).label}`).required().default(null).nullable(),
                //framework: yup.string().notRequired().label(fieldLocation(se[1], s[0]) + p.getFormElement("framework", subclass.framework).label).nullable().default(null),
                //ml_framework1: yup.string().notRequired().label(fieldLocation(se[1], s[0]) + p.getFormElement("ml_framework1", subclass.ml_framework1).label).nullable().default(null),
                development_framework: yup.string().notRequired().label(fieldLocation(se[1], s[0]) + p.getFormElement("development_framework", subclass.development_framework).label).nullable().default(null),
                implementation_language: yup.string().label(fieldLocation(se[1], s[0]) + p.getFormElement("implementation_language", subclass.implementation_language).label).max(100).nullable().default(null),
                //SKIPPING ADDING LOATION BECAUSE TAB IS MISSING
                formalism: validateObjectOptional(p.getFormElement("formalism", subclass.formalism).label, 100).notRequired().nullable().default(null),
                method: yup.string().notRequired().label(p.getFormElement("method", subclass.method).label).nullable().default(null),
                //
                language_dependent: yup.boolean().required().label(fieldLocation(se[1], s[1]) + p.getFormElement("language_dependent", subclass.language_dependent).label).default(null).nullable(),
                required_hardware: yup.array().of(
                    yup.string().label(fieldLocation(se[1], s[1]) + p.getFormElement("required_hardware", subclass.required_hardware).label).required().default(null).nullable()
                ).label(fieldLocation(se[1], s[1]) + p.getFormElement("required_hardware", subclass.required_hardware).label).notRequired().nullable().default(null),
                running_environment_details: validateObjectOptional(fieldLocation(se[1], s[1]) + p.getFormElement("running_environment_details", subclass.running_environment_details).label, 200).notRequired().nullable().default(null),
                requires_software: yup.array().of(
                    yup.string().label(fieldLocation(se[1], s[1]) + p.getFormElement("running_environment_details", subclass.running_environment_details).label).required().default(null).nullable(),
                ).label(fieldLocation(se[1], s[1]) + p.getFormElement("running_environment_details", subclass.running_environment_details).label).notRequired().nullable().default(null),
                input_content_resource: yup.array().when('language_dependent', {
                    is: true,
                    then: yup.array().of(yup.object().required(generalMessage(fieldLocation(se[1], s[1]) + p.getFormElement("input_content_resource", subclass.input_content_resource).label)).shape(intput_content_validation(fieldLocation(se[1], s[1]) + p.getFormElement("input_content_resource", subclass.input_content_resource).label, subclass.input_content_resource.child.children, true))),
                    otherwise: yup.array().of(yup.object().required(generalMessage(fieldLocation(se[1], s[1]) + p.getFormElement("input_content_resource", subclass.input_content_resource).label)).shape(intput_content_validation(fieldLocation(se[1], s[1]) + p.getFormElement("input_content_resource", subclass.input_content_resource).label, subclass.input_content_resource.child.children, false)))
                }).required().label(fieldLocation(se[1], s[1]) + `${p.getFormElement("input_content_resource", subclass.input_content_resource).label}`),
                //output_resource: yup.array().of(
                //    yup.object().notRequired(generalMessage(p.getFormElement("output_resource", subclass.output_resource).label)).shape(intput_content_validation(p.getFormElement("output_resource", subclass.output_resource).label, subclass.output_resource.child.children, false))
                //).notRequired().label(p.getFormElement("output_resource", subclass.output_resource).label).nullable().default(null),
                output_resource: yup.array().when('language_dependent', {
                    is: true,
                    then: yup.array().of(yup.object().nullable().default(null).notRequired(generalMessage(fieldLocation(se[1], s[1]) + p.getFormElement("output resource", subclass.output_resource).label)).shape(intput_content_validation(fieldLocation(se[1], s[1]) + p.getFormElement("output_resource", subclass.output_resource).label, subclass.output_resource.child.children, true))),
                    otherwise: yup.array().of(yup.object().nullable().default(null).notRequired(generalMessage(fieldLocation(se[1], s[1]) + p.getFormElement("output resource", subclass.output_resource).label)).shape(intput_content_validation(fieldLocation(se[1], s[1]) + p.getFormElement("output_resource", subclass.output_resource).label, subclass.output_resource.child.children, false))),
                }).notRequired().label(fieldLocation(se[1], s[1]) + p.getFormElement("output_resource", subclass.output_resource).label).nullable().default(null),
                requires_lr: yup.array().of(
                    yup.object().required(generalMessage(fieldLocation(se[1], s[1]) + p.getFormElement("requires_lr", subclass.requires_lr).label)).shape(lr_validation(fieldLocation(se[1], s[1]) + p.getFormElement("requires_lr", subclass.requires_lr).label))
                ).label(fieldLocation(se[1], s[1]) + p.getFormElement("requires_lr", subclass.requires_lr).label).notRequired().nullable().default(null),
                typesystem: yup.object().notRequired().nullable().default(null).shape(lr_validation(fieldLocation(se[1], s[1]) + p.getFormElement("typesystem", subclass.typesystem).label)),
                annotation_schema: yup.object().notRequired().nullable().default(null).shape(lr_validation(fieldLocation(se[1], s[1]) + p.getFormElement("annotation_schema", subclass.annotation_schema).label)),
                annotation_resource: yup.array().of(
                    yup.object().required(generalMessage(fieldLocation(se[1], s[1]) + p.getFormElement("annotation_resource", subclass.annotation_resource).label)).shape(lr_validation(fieldLocation(se[1], s[1]) + p.getFormElement("annotation_resource", subclass.annotation_resource).label))
                ).label(fieldLocation(se[1], s[1]) + p.getFormElement("annotation_resource", subclass.annotation_resource).label).notRequired().nullable().default(null),
                //previous_annotation_types_policy: yup.string().notRequired().label(fieldLocation(se[1], s[1]) + p.getFormElement("previous_annotation_types_policy", subclass.previous_annotation_types_policy).label).nullable().default(null),
                ml_model: yup.array().of(
                    yup.object().required(generalMessage(fieldLocation(se[1], s[1]) + p.getFormElement("ml_model", subclass.ml_model).label)).shape(lr_validation(fieldLocation(se[1], s[1]) + p.getFormElement("ml_model", subclass.ml_model).label))
                ).label(fieldLocation(se[1], s[1]) + p.getFormElement("ml_model", subclass.ml_model).label).notRequired().nullable().default(null),
                parameter: yup.array().of(
                    yup.object().shape({
                        parameter_name: yup.string().label(fieldLocation(se[1], s[1]) + p.getFormElement("parameter_name", subclass.parameter.child.children.parameter_name).label).required().max(100).default(null).nullable(),
                        parameter_label: validateObject(fieldLocation(se[1], s[1]) + p.getFormElement("parameter_label", subclass.parameter.child.children.parameter_label).label, 100).required().default(null).nullable(),
                        parameter_description: validateObject(fieldLocation(se[1], s[1]) + p.getFormElement("parameter_description", subclass.parameter.child.children.parameter_description).label, 100).required().default(null).nullable(),
                        parameter_type: yup.string().label(fieldLocation(se[1], s[1]) + p.getFormElement("parameter_type", subclass.parameter.child.children.parameter_type).label).required().default(null).nullable(),
                        optional: yup.boolean().required().label(fieldLocation(se[1], s[1]) + p.getFormElement("optional", subclass.parameter.child.children.optional).label).default(null).nullable(),
                        multi_value: yup.boolean().required().label(fieldLocation(se[1], s[1]) + p.getFormElement("multi_value", subclass.parameter.child.children.multi_value).label).default(null).nullable(),
                        default_value: yup.string().label(fieldLocation(se[1], s[1]) + p.getFormElement("default_value", subclass.parameter.child.children.default_value).label).notRequired().max(100).default(null).nullable(),
                        data_format: yup.array().of(
                            yup.string().label(fieldLocation(se[1], s[1]) + "data format").notRequired().default(null).nullable(),
                        ).label(p.getFormElement(fieldLocation(se[1], s[1]) + "data_format", subclass.parameter.child.children.data_format).label).notRequired().nullable().default(null),
                        enumeration_value: yup.array().of(
                            yup.object().shape({
                                value_name: yup.string().label(fieldLocation(se[1], s[1]) + p.getFormElement("enumeration_value", subclass.parameter.child.children.enumeration_value.child).formElements.value_name.label).required().default(null).nullable(),
                                value_label: validateObject(fieldLocation(se[1], s[1]) + `${p.getFormElement("enumeration_value", subclass.parameter.child.children.enumeration_value.child).formElements.value_label.label}`, 150).default(null).nullable(),
                                value_description: validateObject(fieldLocation(se[1], s[1]) + `${p.getFormElement("enumeration_value", subclass.parameter.child.children.enumeration_value.child).formElements.value_description.label}`, 500).default(null).nullable(),
                            }).notRequired().nullable().default(null)
                        ).label(p.getFormElement(fieldLocation(se[1], s[1]) + "enumeration_value", subclass.parameter.child.children.enumeration_value).label).notRequired().nullable().default(null),
                    }).notRequired().nullable().default(null)
                ).label(fieldLocation(se[1], s[1]) + p.getFormElement("parameter", subclass.parameter).label).notRequired().nullable().default(null),
                //
                trl: yup.string().notRequired().nullable().default(null).label(fieldLocation(se[1], s[2]) + "trl"),
                running_time: yup.string().notRequired().max(100).nullable().default(null).label(fieldLocation(se[1], s[1]) + "running time"),
                ///////////////////
                software_distribution: yup.array().of(
                    yup.object().label(fieldLocation(se[2], t[0]) + p.getFormElement("software_distribution", subclass.software_distribution).label).required().shape({
                        software_distribution_form: yup.string().required().label(fieldLocation(se[2], t[0]) + p.getFormElement("software_distribution_form", subclass.software_distribution.child.children.software_distribution_form).label).default(null).nullable(),
                        /*web_service_type: yup.string().label(fieldLocation(se[2], t[0]) + p.getFormElement("web_service_type", subclass.software_distribution.child.children.web_service_type).label).url(fieldLocation(se[2], t[0]) + `${p.getFormElement("web_service_type", subclass.software_distribution.child.children.web_service_type).label} must be a valid URL`).max(1000).default(null).nullable().when('software_distribution_form', (software_distribution_form, schema) => {
                            return software_distribution_form === "http://w3id.org/meta-share/meta-share/webService" ? schema.required() : schema.notRequired();
                        }),*/
                        operating_system: yup.array().nullable().default(null).notRequired().label(fieldLocation(se[2], t[0]) + p.getFormElement("operating_system", subclass.software_distribution.child.children.operating_system).label).of(yup.string().required().label(fieldLocation(se[2], t[0]) + p.getFormElement("operating_system", subclass.software_distribution.child.children.operating_system).label).nullable().default(null)),
                        membership_institution: yup.array().nullable().default(null).notRequired().label(fieldLocation(se[2], t[0]) + p.getFormElement("membership_institution", subclass.software_distribution.child.children.membership_institution).label).of(yup.string().required().label(fieldLocation(se[2], t[0]) + p.getFormElement("membership_institution", subclass.software_distribution.child.children.membership_institution).label).nullable().default(null)),
                        /*execution_location: yup.string().label(fieldLocation(se[2], t[0]) + p.getFormElement("execution_location", subclass.software_distribution.child.children.execution_location).label).max(1000).default(null).nullable().when('software_distribution_form', (software_distribution_form, schema) => {
                            return functional_service ? schema.matches(url_regex, fieldLocation(se[2], t[0]) + `${p.getFormElement("execution_location", subclass.software_distribution.child.children.execution_location).label} must be a valid URL`).required() : schema.notRequired();
                        }),*/
                        //download_location: yup.string().url(fieldLocation(se[2], t[0]) + `${p.getFormElement("download_location", subclass.software_distribution.child.children.download_location).label} must be a valid URL`).max(1000).notRequired().default(null).nullable(),
                        /*docker_download_location: yup.string().label(fieldLocation(se[2], t[0]) + `${p.getFormElement("docker_download_location", subclass.software_distribution.child.children.docker_download_location).label}`).max(1000).default(null).nullable().when('software_distribution_form', (software_distribution_form, schema) => {
                            return functional_service ? schema.required() : schema.notRequired();
                        }),*/
                        //service_adapter_download_location: yup.string().url(fieldLocation(se[2], t[0]) + `${p.getFormElement("service_adapter_download_location", subclass.software_distribution.child.children.service_adapter_download_location).label} must be a valid URL`).max(1000).notRequired().default(null).nullable(),
                        /*access_location: yup.string().label(fieldLocation(se[2], t[0]) + p.getFormElement("access_location", subclass.software_distribution.child.children.access_location).label).url(`${p.getFormElement("access_location", subclass.software_distribution.child.children.access_location).label} must be a valid URL`).max(1000).default(null).nullable().when('software_distribution_form', (software_distribution_form, schema) => {
                            return ['http://w3id.org/meta-share/meta-share/galaxyWorkflow', 'http://w3id.org/meta-share/meta-share/other', 'http://w3id.org/meta-share/meta-share/sourceAndExecutableCode', 'http://w3id.org/meta-share/meta-share/workflowFile'].includes(software_distribution_form) ? schema.required() : schema.notRequired();
                        }),*/
                        //demo_location: yup.string().url(fieldLocation(se[2], t[0]) + `${p.getFormElement("demo_location", subclass.software_distribution.child.children.demo_location).label} must be a valid URL`).max(1000).notRequired().default(null).nullable(),
                        additional_hw_requirements: yup.string().label(fieldLocation(se[2], t[0]) + `${p.getFormElement("additional_hw_requirements", subclass.software_distribution.child.children.additional_hw_requirements).label}`).max(500).notRequired().nullable().default(null),
                        command: yup.string().label(`${p.getFormElement("command", subclass.software_distribution.child.children.command).label}`).max(500).notRequired().nullable().default(null),
                        attribution_text: validateObjectOptional(fieldLocation(se[2], t[0]) + p.getFormElement("attribution_text", subclass.software_distribution.child.children.attribution_text).label).notRequired().nullable().default(null),
                        copyright_statement: validateObjectOptional(fieldLocation(se[2], t[0]) + p.getFormElement("copyright_statement", subclass.software_distribution.child.children.copyright_statement).label, 200).notRequired().nullable().default(null),
                        availability_start_date: yup.date().label(fieldLocation(se[2], t[0]) + p.getFormElement("availability_start_date", subclass.software_distribution.child.children.availability_start_date).label).notRequired().nullable().default(null),
                        availability_end_date: yup.date().min(yup.ref('availability_start_date'), "The available end date of the software disstribution could not be before the corresponding start date ").label(fieldLocation(se[2], t[0]) + p.getFormElement("availability_end_date", subclass.software_distribution.child.children.availability_end_date).label).notRequired().nullable().default(null),
                        is_described_by: yup.array().of(
                            yup.object().required(generalMessage(fieldLocation(se[2], t[0]) + p.getFormElement("is_described_by", subclass.software_distribution.child.children.is_described_by).label)).shape(document_validation(fieldLocation(se[2], t[0]) + p.getFormElement("is_described_by", subclass.software_distribution.child.children.is_described_by).label))
                        ).label(fieldLocation(se[2], t[0]) + p.getFormElement("is_described_by", subclass.software_distribution.child.children.is_described_by).label).notRequired().nullable().default(null),
                        licence_terms: yup.array().of(
                            yup.object().required(fieldLocation(se[2], t[0]) + generalMessage(p.getFormElement("licence_terms", subclass.software_distribution.child.children.licence_terms).label)).shape(licence_validation(fieldLocation(se[2], t[0]) + p.getFormElement("licence_terms", subclass.software_distribution.child.children.licence_terms).label))
                        ).required().label(fieldLocation(se[2], t[0]) + p.getFormElement("licence_terms", subclass.software_distribution.child.children.licence_terms).label).nullable().default(null),
                        cost: yup.object().shape(costValidation(fieldLocation(se[2], t[0]) + "Software distribution Cost")).nullable().default(null).label(fieldLocation(se[2], t[0]) + "Software distribution Cost"),
                        access_rights: yup.array().of(
                            yup.object().shape({
                                category_label: validateObject(fieldLocation(se[2], t[0]) + "Access rights category label", 150, generalMessage(fieldLocation(se[2], t[0]) + "Access rights category label")).required().default(null).nullable(),
                                access_rights_statement_identifier: yup.object().default(null).nullable().notRequired().shape({
                                    access_rights_statement_scheme: yup.string().label(fieldLocation(se[2], t[0]) + "Access rights statement scheme").required(generalMessage(fieldLocation(se[2], t[0]) + "Access rights statement scheme")).default(null).nullable(),
                                    value: yup.string().label(fieldLocation(se[2], t[0]) + "Access rights statement value").max(1000).required(generalMessage(fieldLocation(se[2], t[0]) + "Access rights statement value")).default(null).nullable(),
                                }).label(fieldLocation(se[2], t[0]) + "Access rights statement identifier").default(null).nullable(),
                            })
                        ).notRequired().label(fieldLocation(se[2], t[0]) + "Access rights").nullable().default(null),
                        distribution_rights_holder: yup.array().of(
                            yup.object().required(generalMessage(fieldLocation(se[2], t[0]) + p.getFormElement("distribution_rights_holder", subclass.software_distribution.child.children.distribution_rights_holder).label)).shape(actorValidation(fieldLocation(se[2], t[0]) + p.getFormElement("distribution_rights_holder", subclass.software_distribution.child.children.distribution_rights_holder).label))
                        ).label(fieldLocation(se[2], t[0]) + p.getFormElement("distribution_rights_holder", subclass.software_distribution.child.children.distribution_rights_holder).label).notRequired().nullable().default(null),
                        private_resource: yup.boolean().label(fieldLocation(se[2], t[0]) + p.getFormElement("private_resource", subclass.software_distribution.child.children.private_resource).label).default(null).nullable().when('software_distribution_form', (software_distribution_form, schema) => {
                            return software_distribution_form === DOCKER_IMAGE ? schema.required() : schema.notRequired();
                        }),
                    })
                ).label(fieldLocation(se[2], t[0]) + `${p.getFormElement("software_distribution", subclass.software_distribution).label}`).required().default(null).nullable(),
                ///////////////////SKIPPING ADDING LOCATION BECAUSE SECTION IS MISSING
                evaluated: yup.boolean().notRequired().label(p.getFormElement("evaluated", subclass.evaluated).label).nullable().default(null),
                evaluation: yup.array().of(
                    yup.object().label(p.getFormElement("evaluation", subclass.evaluation).label).required().shape({
                        evaluation_level: yup.array().of(yup.string().required()).notRequired().nullable().default(null).label(p.getFormElement("evaluation_level", subclass.evaluation.child.children.evaluation_level).label),
                        evaluation_type: yup.array().of(yup.string().required()).notRequired().nullable().default(null).label(p.getFormElement("evaluation_type", subclass.evaluation.child.children.evaluation_type).label),
                        evaluation_criterion: yup.array().of(yup.string().required()).notRequired().nullable().default(null).label(p.getFormElement("evaluation_criterion", subclass.evaluation.child.children.evaluation_criterion).label),
                        evaluation_measure: yup.array().of(yup.string().required()).notRequired().nullable().default(null).label(p.getFormElement("evaluation_measure", subclass.evaluation.child.children.evaluation_measure).label),
                        is_evaluated_by: yup.array().of(
                            yup.object().required(generalMessage(p.getFormElement("is_evaluated_by", subclass.evaluation.child.children.is_evaluated_by).label)).shape(lr_validation(p.getFormElement("is_evaluated_by", subclass.evaluation.child.children.is_evaluated_by).label))
                        ).label(p.getFormElement("is_evaluated_by", subclass.evaluation.child.children.is_evaluated_by).label).notRequired().nullable().default(null),
                        evaluation_report: yup.array().of(
                            yup.object().required(generalMessage(p.getFormElement("evaluation_report", subclass.evaluation.child.children.evaluation_report).label)).shape(document_validation(p.getFormElement("evaluation_report", subclass.evaluation.child.children.evaluation_report).label))
                        ).label(p.getFormElement("evaluation_report", subclass.evaluation.child.children.evaluation_report).label).notRequired().nullable().default(null),
                        evaluation_details: validateObjectOptional(p.getFormElement("evaluation_details", subclass.evaluation.child.children.evaluation_details).label, 500).notRequired().nullable().default(null),
                        evaluator: yup.array().of(
                            yup.object().required(generalMessage(p.getFormElement("evaluator", subclass.evaluation.child.children.evaluator).label)).shape(actorValidation(p.getFormElement("evaluator", subclass.evaluation.child.children.evaluator).label))
                        ).label(p.getFormElement("evaluator", subclass.evaluation.child.children.evaluator).label).notRequired().nullable().default(null),
                        gold_standard_location: yup.string().url(`${p.getFormElement("gold_standard_location", subclass.evaluation.child.children.gold_standard_location).label} must be a valid URL`).max(1000).label(p.getFormElement("gold_standard_location", subclass.evaluation.child.children.gold_standard_location).label).notRequired().nullable().default(null),
                        performance_indicator: yup.array().of(
                            yup.object().shape({
                                metric: yup.string().label(`${p.getFormElement("performance_indicator", subclass.evaluation.child.children.performance_indicator.child.children.metric).label}`).required().default(null).nullable(),
                                measure: yup.number().positive().min(0).required(generalMessage(p.getFormElement("measure", subclass.evaluation.child.children.performance_indicator.child.children.measure).label)).label(p.getFormElement("measure", subclass.evaluation.child.children.performance_indicator.child.children.measure).label).nullable(),
                                unit_of_measure_metric: yup.string().label(p.getFormElement("unit_of_measure_metric", subclass.evaluation.child.children.performance_indicator.child.children.unit_of_measure_metric).label).required().max(100).nullable().default(null)
                            })
                        ).label(p.getFormElement("performance_indicator", subclass.evaluation.child.children.performance_indicator).label).notRequired().nullable().default(null),
                    })
                ).label(p.getFormElement("evaluation", subclass.evaluation).label).notRequired(),
            })
        })
    });


    return schema.validate(model, { abortEarly: false });
}

export const validateDistributionLinks = (model, data, subclass, functional_service, dataFiles) => {
    const distribution_path = (index) => fieldLocation(se[2], t[0] + `> Software Distribution ${index + 1}`);
    const distribution_pathGeneric = () => fieldLocation(se[2], t[0] + `> Software Distribution `);
    const distribution_general = fieldLocation(se[2], t[0]);
    const schema = yup.object().shape({
        described_entity: yup.object().shape({
            lr_subclass: yup.object().required().shape({
                software_distribution: yup.array().label(fieldLocation(se[2], t[0]) + p.getFormElement("software_distribution", subclass.software_distribution).label)
                    .test({
                        name: 'docker_download_location',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form, docker_download_location, service_adapter_download_location } = distribution;
                                if (software_distribution_form === DOCKER_IMAGE) {
                                    if (!docker_download_location) {
                                        return this.createError({ message: distribution_path(index) + `> Docker download location is required`, path: distribution_path });
                                    }
                                }
                                //validate the format of docker_download_location, service_adapter_download_location
                                if (docker_download_location) {
                                    if (docker_download_location.indexOf(":/") >= 0 || docker_download_location.indexOf("//") >= 0) {
                                        return this.createError({ message: distribution_path(index) + `> Docker download location ${docker_download_location} is not a valid image reference. The valid format is [host.name/]image/name:version , e.g. docker.io/username/image`, path: distribution_path });
                                    }
                                }
                                if (service_adapter_download_location) {
                                    if (service_adapter_download_location.indexOf(":/") >= 0 || service_adapter_download_location.indexOf("//") >= 0) {
                                        return this.createError({ message: distribution_path(index) + `> Service adapter location ${service_adapter_download_location} is not a valid image reference. The valid format is [host.name/]image/name:version , e.g. docker.io/username/image`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    }).test({
                        name: 'one of download_location-access_location-execution_location must be filled',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form, download_location, access_location, execution_location } = distribution;
                                const package_content_file = distribution.package;
                                if (software_distribution_form === EXECUTABLE_CODE) {
                                    if (!download_location && !access_location && !execution_location && !package_content_file) {
                                        return this.createError({ message: distribution_path(index) + `> At least one of the following must be filled in: Download location, Access location, Execution location. Or upload a content file and associate it with the distribution.`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    }).test({
                        name: 'one of access_location-execution_location must be filled',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form, access_location, execution_location } = distribution;
                                if (software_distribution_form === WEB_SERVICE) {
                                    if (!access_location && !execution_location) {
                                        return this.createError({ message: distribution_path(index) + `> At least one of the following must be filled in: Access location, Execution location`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    }).test({
                        name: 'one of access_location - download_location must be filled',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form, access_location, download_location } = distribution;
                                const package_content_file = distribution.package;
                                if ([LIBRARY, OTHER, PLUGIN, SOURCE_AND_EXECUTABLE, SOURCE_CODE, UNSPECIFIED, WORK_FLOW].includes(software_distribution_form)) {
                                    if (software_distribution_form === UNSPECIFIED || software_distribution_form === OTHER) {
                                        if (!access_location && !download_location) {
                                            return this.createError({ message: distribution_path(index) + `> At least one of the following must be filled in: Access location, Download location`, path: distribution_path });
                                        }
                                    } else if (!access_location && !download_location && !package_content_file) {
                                        return this.createError({ message: distribution_path(index) + `> At least one of the following must be filled in: access location, download location or upload a content file and accociate it with the distribution`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    }).test({
                        name: 'execution location is required',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form, execution_location, elg_compatible_distribution } = distribution;
                                if ([DOCKER_IMAGE].includes(software_distribution_form) && functional_service === true && elg_compatible_distribution) {
                                    if (!execution_location) {
                                        return this.createError({ message: distribution_path(index) + `> Execution location is required`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    }).test({
                        name: 'functional service should have docker image distribution',
                        test: function (distributionArray) {
                            let found_docker_image_for_functional_Service = false;
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form } = distribution;
                                if (functional_service && [DOCKER_IMAGE].includes(software_distribution_form)) {
                                    found_docker_image_for_functional_Service = true;
                                }
                            }
                            if (functional_service && !found_docker_image_for_functional_Service) {
                                return this.createError({ message: distribution_pathGeneric() + `A docker image distribution should be created for ELG integrated services`, path: distribution_path });
                            }
                            return true;
                        },
                    }).test({
                        name: 'elg_compatible_distribution should be assigned for functional service',
                        test: function (distributionArray) {
                            let numberOfDockerImages = 0;
                            let found_elg_compatible_distribution = false;
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { elg_compatible_distribution, software_distribution_form } = distribution;
                                if (software_distribution_form === DOCKER_IMAGE) {
                                    numberOfDockerImages++;
                                }
                                if (elg_compatible_distribution && software_distribution_form === DOCKER_IMAGE) {
                                    found_elg_compatible_distribution = true;
                                }
                            }
                            if (functional_service && !found_elg_compatible_distribution && numberOfDockerImages >= 2) {
                                return this.createError({ message: distribution_pathGeneric() + `There are multiple docker image distributions, you should select the one to be used for the ELG integration.`, path: distribution_path });
                            }
                            return true;
                        },
                    })/*.test({
                        name: 'access location is required',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form, access_location } = distribution;
                                if ([GALAXY_WORKFLOW, OTHER, SOURCE_AND_EXECUTABLE, WORK_FLOW].includes(software_distribution_form)) {
                                    if (!access_location) {
                                        return this.createError({ message: distribution_path(index) + `> Access location is required`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    })*/.test({
                        name: 'is url',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { execution_location, download_location, access_location, demo_location } = distribution;
                                if (execution_location) {
                                    if (!isValidURL(execution_location)) {
                                        return this.createError({ message: distribution_path(index) + `> Execution location is not a valid url`, path: distribution_path });
                                    }
                                }
                                if (download_location) {
                                    if (!isValidURL(download_location)) {
                                        return this.createError({ message: distribution_path(index) + `> Download location is not a valid url`, path: distribution_path });
                                    }
                                }
                                if (access_location) {
                                    if (!isValidURL(access_location)) {
                                        return this.createError({ message: distribution_path(index) + `> Access location is not a valid url`, path: distribution_path });
                                    }
                                }
                                if (demo_location) {
                                    if (!isValidURL(demo_location)) {
                                        return this.createError({ message: distribution_path(index) + `> Demo location is not a valid url`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    }).test({
                        name: 'web_service_type required',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form, web_service_type } = distribution;
                                if (software_distribution_form === WEB_SERVICE) {
                                    if (!web_service_type) {
                                        return this.createError({ message: distribution_path(index) + `> Web service type is required`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    }).test({
                        name: 'max length url',
                        test: function (distributionArray) {
                            for (let index = 0; distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { execution_location, download_location, docker_download_location, service_adapter_download_location, access_location, demo_location } = distribution;
                                const MAX_LENGTH = 1000;
                                if (execution_location) {
                                    if (execution_location.length > MAX_LENGTH) {
                                        return this.createError({ message: distribution_path(index) + `> Execution location must be at most ${MAX_LENGTH} characters`, path: distribution_path });
                                    }
                                }
                                if (download_location) {
                                    if (download_location.length > MAX_LENGTH) {
                                        return this.createError({ message: distribution_path(index) + `> Download location must be at most ${MAX_LENGTH} characters`, path: distribution_path });
                                    }
                                }
                                if (docker_download_location) {
                                    if (docker_download_location.length > MAX_LENGTH) {
                                        return this.createError({ message: distribution_path(index) + `> Docker download location must be at most ${MAX_LENGTH} characters`, path: distribution_path });
                                    }
                                }
                                if (service_adapter_download_location) {
                                    if (service_adapter_download_location.length > MAX_LENGTH) {
                                        return this.createError({ message: distribution_path(index) + `> Service adapter download location must be at most ${MAX_LENGTH} characters`, path: distribution_path });
                                    }
                                }
                                if (access_location) {
                                    if (access_location.length > MAX_LENGTH) {
                                        return this.createError({ message: distribution_path(index) + `> Access location must be at most ${MAX_LENGTH} characters`, path: distribution_path });
                                    }
                                }
                                if (demo_location) {
                                    if (demo_location.length > MAX_LENGTH) {
                                        return this.createError({ message: distribution_path(index) + `> Demo location must be at most ${MAX_LENGTH} characters`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    })/*.test({
                        name: 'All datafiles associated with a distribution',
                        test: function (distributionArray) {
                            if (dataFiles) {
                                for (let index = 0; index < dataFiles.length; index++) {
                                    const file_id = dataFiles[index].id;
                                    let found_data_file_with_no_distribution_assignment = true;
                                    for (let distributionIndex = 0; distributionArray && distributionIndex < distributionArray.length; distributionIndex++) {
                                        const distribution = distributionArray[distributionIndex];
                                        const package_content_file = distribution.package;
                                        if (package_content_file && package_content_file.id === file_id) {
                                            found_data_file_with_no_distribution_assignment = false;
                                        }
                                    }
                                    if (found_data_file_with_no_distribution_assignment) {
                                        return this.createError({ message: distribution_general + `> All uploaded files must be associated with a distribution`, path: distribution_path });
                                    }
                                }
                            }
                            return true;
                        },
                    })*/.test({
                        name: 'If has data=true there must be one distribution with the form libray, plugin, sourceAndExecutable, sourceCode, workFile and file associated with it',
                        test: function (distributionArray) {
                            let hasData = dataFiles && dataFiles.length > 0;

                            let found_distribution_that_requires_data = false;
                            for (let index = 0; hasData && distributionArray && index < distributionArray.length; index++) {
                                const distribution = distributionArray[index];
                                const { software_distribution_form } = distribution;
                                if ([LIBRARY, PLUGIN, SOURCE_AND_EXECUTABLE, SOURCE_CODE, WORK_FLOW].includes(software_distribution_form)) {
                                    found_distribution_that_requires_data = true;
                                }
                            }
                            if (!found_distribution_that_requires_data && hasData) {
                                return this.createError({ message: distribution_general + `> Since you have uploaded a content file you must create a distribution with one of the following forms: library, plugin, source and executable, source code, workflow file and associate the file with it. `, path: distribution_path });
                            }
                            return true;
                        },
                    })
            })
        })
    });
    return schema.validate(model, { abortEarly: false });
}