import React from "react";
import { Helmet } from "react-helmet";
import { withRouter } from "react-router-dom";
import axios from "axios";
import DashboardAppBar from "../DashboardComponents/DashboardAppBar";
import ProgressBar from "../componentsAPI/CommonComponents/ProgressBar";
import Container from '@material-ui/core/Container';
import { empty_lr } from "./Models/LrModel";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import HorizontalTabPanel from "../componentsAPI/CustomVerticalTabs/HorizontalTabPanel"
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ServiceToolFirstStep from "./EditorServiceToolComponents/ServiceToolFirstStep";
import ServiceToolSecondStep from "./EditorServiceToolComponents/ServiceToolSecondStep";
import ServiceToolThirdStep from "./EditorServiceToolComponents/ServiceToolThirdStep";
import ServiceToolFourthStep from "./EditorServiceToolComponents/ServiceToolFourthStep";
import DisplayModel from "./editorCommonComponents/DisplayModel";
import LookupServiceToolByName from "./EditorServiceToolComponents/LookupServiceToolByName";
import IsFunctionalService from "./EditorServiceToolComponents/IsFunctionalService";
import ValidationYupErrors from "./editorCommonComponents/ValidationYupErrors";
import Tooltip from '@material-ui/core/Tooltip';
import CorpusDataTab from "./EditorCorpusComponents/CorpusDataTab";
import UserWarning from "./editorCommonComponents/UserWarning";
import {
  SERVICETOOL_TOP_TABS_HEADERS,
  SERVICETOOL_TOP_TABS_HEADERS_TOOLTIPS,
  SERVICE_FIRST_SECTION_TABS_HEADERS,
  SERVICE_SECOND_SECTION_TABS_HEADERS,
  SERVICE_THIRD_SECTION_TABS_HEADERS,
  SERVICE_FOURTCH_SECTION_TABS_HEADERS,
  EDITOR_MODELS_SCHEMA,
  EDITOR_MODELS_SCHEMA_LR_SUBCLASS,
  EDITOR_DRAFT_NEW_RECORD
} from "../config/editorConstants";

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

class CreateService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schema: null, schema_lr_subclass: null, keycloak: props.keycloak, model: props.model ? JSON.parse(JSON.stringify(props.model)) : JSON.parse(JSON.stringify(empty_lr)), tab: 0, backend_error_response: null, lookupServiceToolByName: props.model ? false : true,
      functional_service: props.model ? (props.model.management_object && props.model.management_object.functional_service && props.model.management_object.functional_service ? true : false) : null,
      under_construction: props.model ? (props.model.management_object && props.model.management_object.under_construction && props.model.management_object.under_construction ? true : false) : null,
      yupError: null,
      source: null, loading: false, showDataComponentDialogueWraper: false,
      tabInSection: 0, yupClickError: null, randomValue2ForceRerender: 0, showWarning: false
    };
    this.isUnmound = false;
  }

  componentWillUnmount = () => {
    if (this.state.source) {
      this.state.source.cancel("");
    }
  }

  componentDidMount() {
    if (this.state.source) {
      this.state.source.cancel("");
    }
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    this.setState({ source: source });
    const serviceRequest = axios.options(EDITOR_MODELS_SCHEMA("lr"), { cancelToken: source.token });
    const serviceSubclassRequest = axios.options(EDITOR_MODELS_SCHEMA_LR_SUBCLASS("tool_service"), { cancelToken: source.token });
    axios.all([serviceRequest, serviceSubclassRequest])
      .then(axios.spread((...responses) => {
        const serviceResponse = responses[0]
        const subclassResponse = responses[1]
        this.setState({
          schema: serviceResponse.data.actions.POST, schema_lr_subclass: subclassResponse.data.actions.POST, source: null
        })
      }))
      .catch(errors => { console.log(errors); this.setState({ source: null }); })
  }

  handleYupErrorClick = (item) => {
    let parts = item && item.split(">");
    if (parts && parts.length >= 3) {
      const sectionIndex = SERVICETOOL_TOP_TABS_HEADERS.findIndex(item => item.toLowerCase() === parts[1].trim().toLowerCase()) || 0;
      const tabInSection = this.getTab(sectionIndex, parts[2]) || 0;
      this.setState({ tab: sectionIndex, tabInSection: tabInSection, yupClickError: item, randomValue2ForceRerender: Math.random() });
    }
  }

  getTab = (sectionIndex, tabToMatch) => {
    switch (sectionIndex) {
      case 0:
        return SERVICE_FIRST_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 1:
        return SERVICE_SECOND_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 2:
        return SERVICE_THIRD_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 3:
        return SERVICE_FOURTCH_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      default: return 0;
    }
  }

  set_backend_error_response = (error) => {
    this.setState({ backend_error_response: error })
  }

  set_backend_successful_response = (response) => {
    this.setState({ model: response, backend_error_response: null })
  }

  set_yup_error = (yupError) => {
    this.setState({ yupError });
  }

  updateModel = (model) => {
    this.setState({ model });
  }

  toggleTab = (index) => {
    this.setState({ tab: index, tabInSection: 0, yupClickError: null });
  };

  handleFunctionalService = (value) => {
    this.setState({ functional_service: value });
  }

  handleUnderConstruction = (value) => {
    this.setState({ under_construction: value });
  }

  saveDraft = () => {
    if (this.state.source) {
      this.state.source.cancel("");
    }
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    this.setState({ source: source, loading: true });
    axios.post(EDITOR_DRAFT_NEW_RECORD, this.state.model, {
      headers: {
        'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
        'FUNCTIONAL-SERVICE': this.state.functional_service === true ? true : false
      },
      cancelToken: source.token
    }).then((res) => {
      //console.log(JSON.stringify(res));
      let under_construction = false;
      if (res.data.management_object) {
        res.data.management_object.under_construction === true ? under_construction = true : under_construction = false;
      }
      this.setState({ under_construction: under_construction, source: null, laoding: false, model: res.data, showDataComponentDialogueWraper: true });
    }).catch((err) => {
      console.log(JSON.stringify(err));
      this.props.history.push("/createResource");
    });
  }

  hideLookUpByName = (serviceName, restProperties) => {
    const { model } = this.state;
    if (restProperties && restProperties.pk) {
      //model.pk = restProperties.pk;
      model.described_entity.pk = restProperties.pk;//add pk of generic record inside described_entity
      delete restProperties.pk;
    }
    model.described_entity.resource_name = { "en": serviceName };
    model.described_entity = { ...model.described_entity, ...restProperties };
    this.setState({ lookupServiceToolByName: false, model: model }, this.saveDraft);
  }

  handleDescriptionChange = () => {
    //this.setState({showWarning: true});
    const { model } = this.state;
    const { description = "" } = model.described_entity || {};
    (description && description["en"] && description["en"].length < 50) ? this.setState({ showWarning: true }) : this.setState({ showWarning: false })
  }

  render() {
    const { lookupServiceToolByName } = this.state;
    if (lookupServiceToolByName) {
      return <LookupServiceToolByName hideLookUpByName={this.hideLookUpByName} keycloak={this.props.keycloak} />
    }

    if (!this.state.model.hasOwnProperty("pk")) {
      return <ProgressBar />
    }

    /*if (this.state.showDataComponentDialogueWraper) {
      return <DataComponentDialogueWraper {...this.props}  {...this.state} hideDataComponentDialogueWraper={() => { this.setState({ showDataComponentDialogueWraper: false }) }} />
    }*/

    if (!this.state.schema || !this.state.schema_lr_subclass) {
      return <ProgressBar />
    }

    if (this.state.functional_service === null) {
      return <IsFunctionalService handleFunctionalService={this.handleFunctionalService} />
    }

    return (
      <>
        <Helmet>
          <title>ELG - Create Service or Tool</title>
        </Helmet>
        <DashboardAppBar />
        <div className="editor-container-white pb-2">
          <Container maxWidth="xl">
            <div className="empty"></div>
            <Typography className="dashboard-title-box pb-05">Create a new Service or Tool</Typography>
            <div>
              {this.state.backend_error_response && <div>
                <h3>Error</h3>
                <div className=" boxed">
                  <Paper elevation={13} >
                    <code>
                      <pre id="special">
                        {JSON.stringify(this.state.backend_error_response, null, 2)}
                      </pre>
                    </code>
                  </Paper>
                </div>
              </div>
              }
            </div>
            {!this.state.backend_error_response && <ValidationYupErrors key={this.state.yupError ? JSON.stringify(this.state.yupError) : "1"} yupError={this.state.yupError} requiredFields={[]} handleYupErrorClick={this.handleYupErrorClick} />}
            {<UserWarning showWarning={this.state.showWarning} />}
          </Container>

          <div className="editor-actions-header">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="space-between" alignItems="center" className="grays--offwhite">
                <Grid item sm={7}>
                  <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="horizontal" aria-label="simple tabs example" className="simple-tabs-forms">
                    {SERVICETOOL_TOP_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<Tooltip title={SERVICETOOL_TOP_TABS_HEADERS_TOOLTIPS[index]}><div className="pt-2"> {tab}</div></Tooltip>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                  </Tabs>
                </Grid>
                <Grid item container sm={5} alignItems="center" spacing={1} justifyContent="flex-end">
                  <DisplayModel data={this.state.schema} schema_lr_subclass={this.state.schema_lr_subclass} model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} handleFunctionalService={this.handleFunctionalService} functional_service={this.state.functional_service} under_construction={this.state.under_construction} handleUnderConstruction={this.handleUnderConstruction} tool_service={true} set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <Container maxWidth="xl">
            <div className="editor-main-card-container">
              <HorizontalTabPanel value={this.state.tab} index={0} className="horizontal-tab-pannel">
                <ServiceToolFirstStep onDescriptionChange={this.handleDescriptionChange} key={`section-0-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={1} className="horizontal-tab-pannel">
                <ServiceToolSecondStep key={`section-1-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={2} className="horizontal-tab-pannel">
                <ServiceToolThirdStep key={`section-2-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              {false && <HorizontalTabPanel value={this.state.tab} index={3} className="horizontal-tab-pannel">
                <ServiceToolFourthStep  {...this.props}  {...this.state} updateModel={this.updateModel} />
              </HorizontalTabPanel>}
              <HorizontalTabPanel value={this.state.tab} index={3} className="horizontal-tab-pannel">
                <CorpusDataTab key={`section-3-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
            </div>
          </Container>
          <div className="editor-actions-bottom">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="flex-end" alignItems="center" className="pb-3 pt-3 grays--offwhite">
                <Grid item container xs={12} alignItems="center">
                  <DisplayModel
                    data={this.state.schema}
                    schema_lr_subclass={this.state.schema_lr_subclass}
                    model={this.state.model}
                    set_backend_error_response={this.set_backend_error_response}
                    set_backend_successful_response={this.set_backend_successful_response}
                    handleFunctionalService={this.handleFunctionalService}
                    functional_service={this.state.functional_service}
                    under_construction={this.state.under_construction}
                    handleUnderConstruction={this.handleUnderConstruction}
                    hide_checkboxes={true}
                    tool_service={true}
                    set_yup_error={this.set_yup_error}
                  />
                </Grid>
              </Grid>
            </Container>
          </div>

          <div>
            {this.state.backend_error_response && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
              <h3>Error</h3>
              <div className=" boxed">
                <Paper elevation={13} >
                  <code>
                    <pre id="special">
                      {JSON.stringify(this.state.backend_error_response, null, 2)}
                    </pre>
                  </code>
                </Paper>
              </div>
            </div>
            }
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(CreateService)