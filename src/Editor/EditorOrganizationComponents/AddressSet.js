import React from "react";
import Typography from '@material-ui/core/Typography';
import { head_office_address_set_obj } from "../Models/OrganizationModel";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import CountryAutocomplete from "./CountryAutocomplete";
import { TextField } from "@material-ui/core";
import messages from "./../../config/messages"; 

export default class RecordAddressSet extends React.Component {
    constructor(props) {
        super(props);
        this.state = { default_valueObj: this.props.default_valueObj || {} }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            default_valueObj: nextProps.default_valueObj || {},
        };
    }

    setAddressSet = (action, value) => {
        let { default_valueObj } = this.state;
        switch (action) {
            case "add_address_set_empty_obj":
                default_valueObj = { ...JSON.parse(JSON.stringify(head_office_address_set_obj)) };
                this.props.updateModel_address_set_obj(this.props.field, default_valueObj);
                break;
            case "remove_address_set":
                if (this.props.other_address) {
                    if (this.props.other_address.length !== 0) {
                        return;
                    }
                }
                default_valueObj = null;
                this.props.updateModel_address_set_obj(this.props.field, default_valueObj);
                break;
            case "set_zip_code":
                default_valueObj.zip_code = value;
                break;
            default:
                break;
        }
        this.setState({ default_valueObj });
    }

    setLanguageSpecificText = (field, obj) => {
        const { default_valueObj } = this.state;
        default_valueObj[field] = obj;
        //this.setState({ default_valueObj });
        this.props.updateModel_address_set_obj(this.props.field, default_valueObj);
    }

    setSelection = (selection) => {
        const { default_valueObj } = this.state;
        default_valueObj.country = selection.value;
        //this.setState({ default_valueObj });
        this.props.updateModel_address_set_obj(this.props.field, default_valueObj);
    }

    onBlur = () => {
        this.props.updateModel_address_set_obj(this.props.field, this.state.default_valueObj);
    }


    render() {
        const {
            label, help_text,
            address_help_text, address_label, address_required, address_type,
            region_help_text, region_label, region_required, region_type,
            zip_code_help_text, zip_code_label, zip_code_required, zip_code_type, zip_code_read_only, zip_code_max_length,
            city_help_text, city_label, city_required, city_type,
            country_help_text, country_label, country_required, country_type, country_choices,
        } = this.props;

        const address = this.state.default_valueObj;
        var isEmpty = !Object.keys(address).length;

        return <div className="pb-3 inner--group nested--group" onBlur={this.onBlur}>
            {!this.props.hideRedudantInformation && <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>

                {isEmpty && <Grid item sm={1}>
                    <Button className="inner-link-default--purple" onClick={(e) => this.setAddressSet("add_address_set_empty_obj")}>{messages.group_elements_create}</Button>
                </Grid>} 

                {!this.props.hideRedudantInformation && !isEmpty && <Grid container direction="row" justifyContent="flex-end" alignItems="baseline">
                    <Grid item sm={1}>
                        {<Button className="inner-link-default--purple" onClick={(e) => this.setAddressSet("remove_address_set")}>{messages.group_elements_remove}</Button>}
                    </Grid>
                </Grid>
                }

            </Grid>}

            {!isEmpty && <div className="mtl20">           

                <div>
                    <CountryAutocomplete
                        className="wd-70"
                        type={country_type}
                        required={country_required}
                        label={country_label}
                        help_text={country_help_text}
                        choices={country_choices}
                        default_value={address.country || ""}
                        setSelection={this.setSelection}
                    />

                    <LanguageSpecificText
                        defaultValueObj={address.address || { "en": "" }}
                        field={"address"}
                        type={address_type}
                        required={address_required}
                        label={address_label}
                        help_text={address_help_text}
                        setLanguageSpecificText={this.setLanguageSpecificText}
                    />
                    <LanguageSpecificText
                        defaultValueObj={address.region || { "en": "" }}
                        field={"region"}
                        type={region_type}
                        required={region_required}
                        label={region_label}
                        help_text={region_help_text}
                        setLanguageSpecificText={this.setLanguageSpecificText}
                    />
                    <LanguageSpecificText
                        defaultValueObj={address.city || { "en": "" }}
                        field={"city"}
                        type={city_type}
                        required={city_required}
                        label={city_label}
                        help_text={city_help_text}
                        setLanguageSpecificText={this.setLanguageSpecificText}
                    />
                    <TextField
                        className="wd-70"
                        variant="outlined"
                        maxLength={zip_code_max_length}
                        type={zip_code_type}
                        required={zip_code_required}
                        disabled={zip_code_read_only}
                        label={zip_code_label}
                        helperText={zip_code_help_text}
                        value={address.zip_code || ""}
                        onChange={(e) => this.setAddressSet("set_zip_code", e.target.value)}
                        inputProps={{
                            // type: "password",
                            autoComplete: 'new-password'
                        }}
                    />
                    
                </div>
            </div>
            }
        </div >
    }
}