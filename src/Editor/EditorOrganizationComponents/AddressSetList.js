import React from "react";
import Typography from '@material-ui/core/Typography';
import { address_set_obj } from "../Models/OrganizationModel";
import Button from '@material-ui/core/Button';
import messages from "./../../config/messages"; 
import Grid from '@material-ui/core/Grid';
import AddressSet from "./AddressSet";

export default class RecordAddressSet extends React.Component {

    constructor(props) {
        super(props);
        this.state = { default_valueArray: props.default_valueArray || [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            default_valueArray: nextProps.default_valueArray || [],
        };
    }

    setAddressSet = (action, index) => {
        const { default_valueArray } = this.state;
        switch (action) {
            case "add_address_set_empty_obj":
                if (default_valueArray.filter(address => address.country).length < default_valueArray.length) {
                    return; //do not add another address obj if there is an empty one
                }
                default_valueArray.push(JSON.parse(JSON.stringify(address_set_obj)));
                break;
            case "remove_address_set_empty_obj":
                default_valueArray.splice(index, 1);
                break;
            default:
                break;
        }
        this.setState({ default_valueArray }, this.onBlur);
    }

    setAddressObj = (index, obj) => {
        const { default_valueArray } = this.state;
        default_valueArray[index] = obj;
        this.setState({ default_valueArray });
    }

    onBlur = () => {
        this.props.updateModel_address_set(this.props.field, this.state.default_valueArray);
    }


    render() {
        const { default_valueArray } = this.state;

        return <div className="inner--group nested--group" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {default_valueArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setAddressSet("add_address_set_empty_obj")}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {
                default_valueArray.map((address, addressIndex) => {
                    return <div  key={addressIndex}>
                        <Grid container direction="row" alignItems="baseline" justifyContent="flex-end">
                            <Grid item>
                                {<Button className="inner-link-default--purple" onClick={(e) => this.setAddressSet("remove_address_set_empty_obj", addressIndex)}>{messages.group_elements_remove}</Button>}
                            </Grid>
                        </Grid>
                        <AddressSet
                            {...this.props}
                            //default_valueObj={address}
                            default_valueObj={JSON.parse(JSON.stringify(address))}
                            field={addressIndex}//don't care about address, instead track addressIndex
                            hideRedudantInformation={true}
                            updateModel_address_set_obj={this.setAddressObj}
                        />
                        <Grid container direction="row" alignItems="baseline" justifyContent="flex-start">
                        { (default_valueArray.length - 1) === addressIndex ?
                            <Grid item>                               
                                {<Button className="inner-link-default--purple" onClick={(e) => this.setAddressSet("add_address_set_empty_obj")}>{messages.array_elements_add}</Button>}
                            </Grid>
                            :<span></span>
                        }
                        
                        </Grid>
                        <div style={{ paddingTop: "1em",marginTop:"1em", background:"#fff"}} />
                    </div>
                })
            }
        </div>
    }
}