export const empty_lr = sessionStorage.org ? JSON.parse(sessionStorage.org) : {
    "described_entity": {
        "entity_type": "LanguageResource",
        "resource_name": { "en": "" },
        "lr_subclass": {
            "lr_type": "ToolService",
            "language_dependent": false,
            "evaluated": false
        }
    }
};

export const source_of_metadata_record_obj = {
    "repository_name": { "en": "" },
    "repository_identifier": null,
    "repository_url": "",
    "repository_institution": null,
    'editor-placeholder': true
};

export const repository_identifier_obj = { "repository_identifier_scheme": "", "value": "" };

export const resourceRelationsObj = {
    "resource_name": { "en": "" },
    "lr_identifier": null,
    "version": "",
    'editor-placeholder': true
};

export const access_rights_statement_identifier_obj = { "access_rights_statement_scheme": "", "value": "" };

export const access_rights_obj = {
    "category_label": { "en": "" },
    "access_rights_statement_identifier": null,
    'editor-placeholder': true
};

export const lr_identifier = {
    "lr_identifier_scheme": "", "value": ""
};

export const document_identifier_obj = {
    "document_identifier_scheme": "", "value": ""
};

export const licence_identifier_obj = {
    "licence_identifier_scheme": "", "value": ""
};

export const personal_identifier_obj = {
    "personal_identifier_scheme": "", "value": ""
};
export const actualUseObj = {
    "used_in_application": [],
    "has_outcome": [],
    "usage_project": [],
    "usage_report": [],
    "actual_use_details": null,//{ "en": "" },
    'editor-placeholder': true
}

export const relationObj = {
    "relation_type": { "en": "" },
    "related_lr": null,
    'editor-placeholder': true
}

export const annotationObj = {
    'editor-placeholder': true,
    "annotation_type": [],
    "annotated_element": [],
    "segmentation_level": [],
    "annotation_standoff": null,
    "guidelines": [],
    //"tagset": [],//removed at m2.3
    "theoretic_model": null,
    "annotation_mode": null,
    "annotation_mode_details": null,
    "is_annotated_by": [],
    "annotator": [],
    "annotation_start_date": null,
    "annotation_end_date": null,
    "interannotator_agreement": null,
    "intraannotator_agreement": null,
    "annotation_report": [],
    "typesystem": [],
    "annotation_resource": []
}


export const validationObj = {
    "validation_type": null,
    "validation_mode": null,
    "validation_details": null,//{ "en": "" },
    "validation_extent": null,
    "is_validated_by": [],
    "validation_report": [],
    "validator": [],
    "editor-placeholder": true
}


export const inputContentResourceObj = {
    "processing_resource_type": null,
    //"samples_location": "",
    "language": [],
    "media_type": null,
    "data_format": [],
    "character_encoding": [],
    "annotation_type": [],
    "segmentation_level": [],
    "typesystem": null,
    "annotation_schema": null,
    "annotation_resource": null,
    "modality_type": [],
    "modality_type_details": null//{ "en": "" }
};

export const languageModelObj = {
    "language_tag": null,
    "language_id": null,
    "script_id": null,
    "region_id": null,
    "variant_id": null,
    "language_variety_name": null
};

export const typesystemObj = {
    "resource_name": { "en": "" },
    "lr_identifier": null,
    "version": null
};

export const softwareDistributionObj = {
    "software_distribution_form": null,
    "execution_location": "",
    "download_location": "",
    "docker_download_location": "",
    "service_adapter_download_location": "",
    "access_location": "",
    "demo_location": "",
    "is_described_by": [],
    "additional_hw_requirements": "",
    "command": "",
    "web_service_type": null,
    "operating_system": [],
    "licence_terms": [],
    "cost": null,
    "membership_institution": [],
    "attribution_text": null,
    "copyright_statement": null,
    "availability_start_date": null,
    "availability_end_date": null,
    "distribution_rights_holder": [],
    "access_rights": [],
    "package": null,
    "private_resource": null,
};

export const evaluationObj = {
    "evaluation_level": [],
    "evaluation_type": [],
    "evaluation_criterion": [],
    "evaluation_measure": [],
    "gold_standard_location": "",
    "performance_indicator": [],
    "evaluation_report": [],
    "is_evaluated_by": [],
    "evaluation_details": null,
    "evaluator": [],
    "editor-placeholder": true,
};

export const is_described_by_Obj = {
    "editor-placeholder": true,
    "title": { "en": "" },
    "document_identifier": null,
    "bibliographic_record": ""
};

export const licence_terms_obj = {
    "licence_terms_name": { "en": "" },
    "licence_terms_url": [],
    "condition_of_use": [],
    "licence_identifier": null,
    "editor-placeholder": true
};

export const cost_obj = { "amount": null, "currency": null };

export const additional_info_obj = {
    "landing_page": "",
    "email": ""
};