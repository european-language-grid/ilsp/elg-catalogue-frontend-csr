export const empty_organization = sessionStorage.org ? JSON.parse(sessionStorage.org) : {
    "described_entity": {
        "entity_type": "Organization",
        "organization_name": { "en": "" }
    }
}

export const domainObj = {
    "editor-placeholder": true, "category_label": { "en": "" }, "domain_identifier": null
}

export const domain_identifier = {
    "domain_classification_scheme": "", "value": ""
}
export const organization_identifier_obj = { "organization_identifier_scheme": "", "value": "" };

//this is used for other_office_address
export const address_set_obj = {
    "country": ""
};

export const head_office_address_set_obj = {
    "country": ""
};

export const has_division_obj = {
    'editor-placeholder': true,
};