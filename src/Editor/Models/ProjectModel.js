export const empty_project = sessionStorage.org ? JSON.parse(sessionStorage.org) : {
    "described_entity": {
        "entity_type": "Project",
        "project_name": { "en": "" }
    }
}

export const subjectObj = {
    "category_label": { "": "" }, "subject_identifier": { "subject_classification_scheme": "", "value": "" }
}

export const funding_set_obj = {
    "currency": "", "amount": ""
}

export const genericProjectObj = {
    'editor-placeholder': true,
    "project_name": { "en": "" },
    "project_identifier": [],
    "grant_number": "",
}

export const project_identifier_obj = { "project_identifier_scheme": "", "value": "" };