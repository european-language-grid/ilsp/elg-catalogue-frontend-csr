import React from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
//import { ReactComponent as EditIcon } from "./../../assets/elg-icons/editor/pencil.svg";
import { ReactComponent as LockIcon } from "./../../assets/elg-icons/editor/lock-1.svg";
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';
import { MATCH_LD_BY_NAME } from "../../config/editorConstants";
import { AUTHENTICATED_KEYCLOAK_USER_USERNAME, get_landing_page_url_Editor, LD, getLogoutUrl } from "../../config/constants";
import messages from "./../../config/messages";
import Grid from '@material-ui/core/Grid';
import UnpubilshUnderConstructionButton from "../editorCommonComponents/UnpubilshUnderConstructionButton";

class LookupLdName extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [], loading: false, display: true, lookupName: "", finishedLookup: false, source: null
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    lookUpByNameSearchButton = (userInput) => {
        this.setState({ lookupName: userInput, finishedLookup: false });
        if (userInput.trim().length <= 2) {
            this.setState({ data: [] });
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        if (this.state.lookupName && this.state.lookupName.length >= 3) {
            axios.get(MATCH_LD_BY_NAME(userInput), { cancelToken: source.token })
                .then((res) => {
                    this.setState({ data: res.data, loading: false, finishedLookup: true, source: null })
                }).catch((err) => {
                    this.setState({ data: [], loading: false, finishedLookup: true, source: null })
                    console.log(err);
                });
        } else {
            this.setState({ loading: false, finishedLookup: true, source: null });
        }

    }

    getList = () => {
        if (this.state.finishedLookup && this.state.data.length === 0 && this.state.lookupName && this.state.lookupName.length >= 3 && !this.state.loading) {
            return <h6>{messages.lookup_nomatch}</h6>
        }
        return <Grid container direction="row" justifyContent="space-between" alignItems="center" spacing={2} className="mt1 mb2">
            {this.state.data.map((item, index) => {
                return <Grid item container justifyContent="flex-start" alignItems="center" className="padding5 bottom-border" key={index}>
                    {item.full_metadata_record ? this.showFullRecord(item) : this.showGenericRecord(item)} </Grid>
            })}
        </Grid>
    }


    checkIfUserIsOwner = (item) => {
        const { keycloak } = this.props;
        if ((item.curator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(keycloak))) { //&& ((item.status === "g")
            return true;
        } else {
            return false;

        }
    }

    showFullRecord = (item) => {
        const { under_construction = null } = item;
        const url = item.metadata_record;
        var n = url.indexOf("/metadatarecord/");
        const pk = url.substring(n + 16, url.length - 2);
        const path_to_landing_page = get_landing_page_url_Editor(pk, item.resource_type);//if this exists then the resource type is compatible
        //const elg_record_url = `${window.location.origin}${path_to_landing_page}`;
        let origin_part = getLogoutUrl();
        origin_part = origin_part.substring(0, origin_part.length - 1);
        const elg_record_url = origin_part + path_to_landing_page + "/";
        return <>
            {(item.status === "p" && path_to_landing_page) &&
                <>
                    <Grid item sm={under_construction ? 10 : 12}><a target="_blank" rel="noopener noreferrer" href={elg_record_url}>{item.resource_name["en"]}</a></Grid>
                    {under_construction && <Grid item sm={2}><UnpubilshUnderConstructionButton under_construction={under_construction} pk={pk} entity_type={LD} /></Grid>}
                </>
            }
            {(item.status === "p" && !path_to_landing_page) && <Grid item>{item.resource_name["en"]}</Grid>}
            {item.status === "g" && <>
                <Grid item sm={10}> {item.resource_name["en"]} </Grid>
                <Grid item sm={2}> <Tooltip title={messages.lookup_lock_tooltip}><Button className="inner-link-default-editor--teal"><LockIcon className="xsmall-icon" /></Button></Tooltip>
                </Grid>
            </>}
            {(item.status === "i" || item.status === "d") &&
                (this.checkIfUserIsOwner(item) ?
                    <>
                        <Grid item sm={10}> {item.resource_name["en"]} </Grid>
                        <Grid item sm={2}>
                            {(item.resource_type === LD || item.resource_type === "LanguageDescription") ?
                                <Tooltip title={"item in edit status"}>
                                    <Button className="inner-link-default-editor--teal"
                                        onClick={() => { this.props.history.push({ pathname: `/create/language_description/${pk}` }); }}>
                                        {messages.lookup_edit_button}
                                    </Button>
                                </Tooltip> :
                                <Tooltip title={item.resource_type}><Button className="inner-link-default-editor--teal"><LockIcon className="xsmall-icon" /></Button></Tooltip>
                            }
                        </Grid>
                    </>
                    : <><Grid item sm={10}> {item.resource_name["en"]} </Grid>
                        <Grid item sm={2}>
                            <Tooltip title={messages.lookup_lock_tooltip}><Button className="inner-link-default-editor--teal"><LockIcon className="xsmall-icon" /></Button></Tooltip>
                        </Grid>
                    </>

                )
            }
            {item.status === "u" &&
                <><Grid item sm={10}> {item.resource_name["en"]} </Grid>
                    <Grid item sm={2}>
                        <Tooltip title={messages.lookup_lock_unpublish_tooltip}><Button className="inner-link-default-editor--teal"><LockIcon className="xsmall-icon" /></Button></Tooltip>
                    </Grid>
                </>
            }
        </>
    }


    showGenericRecord = (item) => {
        return <><Grid item sm={10}>
            <span className="summary-title">{item.resource_name[Object.keys(item.resource_name)[0]]}</span> </Grid>
            <Grid item sm={2}> <Tooltip title={messages.lookup_edit_tooltip}><Button className="inner-link-default-editor--teal" disabled={item.locked} onClick={() => { this.editGeneric(item) }}>{messages.lookup_edit_button}</Button></Tooltip></Grid>
        </>
    }

    editGeneric = (item) => {
        this.setState({ display: false });
        if (item && item.metadata_record && item.metadata_record.actor_type) {
            delete item.metadata_record.actor_type;
        }
        this.props.hideLookUpByName(null, item.metadata_record);
    }

    cancel = () => {
        this.setState({ display: false })
        this.props.history.push("/createResource");
    }

    create = () => {
        this.setState({ display: false })
        this.props.hideLookUpByName(this.state.lookupName);
    }

    displayCreateButton = () => {
        if (this.state.loading || !this.state.finishedLookup) {
            return false;
        } else if (this.state.lookupName.length <= 2) {
            return false;
        }
        return true;
    }

    disableCreateButton = () => {
        const dataArray = this.state.data || [];
        for (let index = 0; index < dataArray.length; index++) {
            const item = dataArray[index];
            if (item && item.exact) {
                return true;
            }
        }
        return false;
    }

    render() {
        let ld_label = "Language description name";
        let helpText_to_use = messages.lookup_ld;
        switch (this.props.subclass) {
            case "model":
                ld_label = "Model name";
                helpText_to_use = messages.lookup_model;
                break;
            case "grammar":
                ld_label = "Grammar name";
                helpText_to_use = messages.lookup_grammar;
                break;
            case "other":
                break;
            default:
                break;
        }
        return <div>
            {this.state.display && <Dialog open={this.state.display} onClose={this.disableRetrieveRecord} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="lg" fullWidth={true} style={{insetBlockStart: "130px"}}>
                <DialogTitle id="alert-dialog-title">
                    <form onSubmit={(e) => { e.preventDefault(); this.lookUpByNameSearchButton(this.state.lookupName) }}>
                        <div style={{ display: "flex", justifyItems: "space-between" }}>
                            <div className="wd-100" >
                                <TextField autoFocus className="wd-100" label={ld_label} onChange={(e) => this.setState({ lookupName: e.target.value, finishedLookup: false })} /*onChange={(e) => this.lookUpByName(e)}*/ variant="outlined"
                                    helperText={helpText_to_use}
                                    InputProps={{
                                        endAdornment: (
                                            <>
                                                {this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
                                            </>
                                        ),
                                    }}
                                />
                            </div>
                        </div>
                    </form>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText component={"span"} id="alert-dialog-description">
                        <>{this.getList()}</>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Tooltip title={this.disableCreateButton() ? `${this.state.lookupName} already exists` : ""} placement="top">
                        <span>
                            <Button hidden={!this.displayCreateButton()} disabled={this.disableCreateButton()} onClick={this.create} color="primary" >
                                Create {this.state.lookupName}
                            </Button>
                        </span>
                    </Tooltip>
                    {!this.state.finishedLookup && <Button disabled={this.state.loading || (!this.state.lookupName || (this.state.lookupName && this.state.lookupName.length <= 2))} color="primary" onClick={(e) => { this.lookUpByNameSearchButton(this.state.lookupName) }}>search</Button>}
                    <Button disabled={this.state.loading} onClick={this.cancel} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>
            }
        </div>
    }

}

export default withRouter(LookupLdName);