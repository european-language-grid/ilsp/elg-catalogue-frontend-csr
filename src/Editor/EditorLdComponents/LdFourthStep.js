import React from "react";
import axios from "axios";
//import Container from '@material-ui/core/Container';
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { ReactComponent as TextIcon } from "./../../assets/elg-icons/office-file-text-graph-alternate.svg";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
import DatasetDistributionArray from "../EditorCorpusComponents/DatasetDistributionArray";
import { LD_FOURTH_SECTION_TABS_HEADERS } from "../../config/editorConstants";
import { LIST_DATA_OF_RECORD } from "../../config/constants";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export default class CorpusFourthStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0, source: null, datasets: [], loading: false };
    }

    componentWillUnmount = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    componentDidMount() {
        this.retrieveRecordData();
    }

    retrieveRecordData = () => {
        if (this.props.model.hasOwnProperty("pk")) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            axios.get(LIST_DATA_OF_RECORD(this.props.model.pk), { cancelToken: source.token })
                .then(res => { this.setState({ source: null, loading: false, datasets: res.data.content_files }); })
                .catch(err => { this.setState({ source: null, loading: false, datasets: [] }); })
        } else {
            this.setState({ datasets: [], loading: false })
        }
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    updateModel_generic_array = (obj2update, valueArray, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = [];
            }
            model.described_entity.lr_subclass[obj2update] = valueArray;
            this.props.updateModel(model);
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = [];
            }
            model.described_entity[obj2update] = valueArray;
            this.props.updateModel(model);
        }
    }

    render() {
        //const data = this.props.schema;
        if (this.state.loading) {
            // return <ProgressBar />
        }
        const { model } = this.props;
        const subclass = this.props.schema_lr_subclass;
        let LanguageDescriptionTextPartlingualityType = ""; //have to parse/save the value in linguality type of each media part, this is used in a rule inside SizeItem, not sure if this is the way to go...
        let LanguageDescriptionVideoPartlingualityType = "";
        let LanguageDescriptionImagePartlingualityType = "";
        let media_typeText = "";
        let media_typeVideo = "";
        let media_typeImage = "";

        model.described_entity.lr_subclass.language_description_media_part && model.described_entity.lr_subclass.language_description_media_part.map((mediaPart, mediaPartIndex) => {
            let media_type = mediaPart.ld_media_type ? mediaPart.ld_media_type : "";
            if (media_type === "LanguageDescriptionTextPart") {
                LanguageDescriptionTextPartlingualityType = mediaPart.linguality_type ? mediaPart.linguality_type : "";
                media_typeText = mediaPart.media_type ? mediaPart.media_type : "";
                return <></>;
            }

            if (media_type === "LanguageDescriptionVideoPart") {
                LanguageDescriptionVideoPartlingualityType = mediaPart.linguality_type ? mediaPart.linguality_type : "";
                media_typeVideo = mediaPart.media_type ? mediaPart.media_type : "";
                return <></>;
            }
            if (media_type === "LanguageDescriptionImagePart") {
                LanguageDescriptionImagePartlingualityType = mediaPart.linguality_type ? mediaPart.linguality_type : "";
                media_typeImage = mediaPart.media_type ? mediaPart.media_type : "";
                return <></>;
            }

            else return <></>;
        })

        return <div>
            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {LD_FOURTH_SECTION_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<><div><TextIcon className="small-icon general-icon--tabs mr-05" /></div><div className="pt-2"> {tab} <div className="pt-2"></div></div> </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                        </Tabs>
                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3">
                                        <DatasetDistributionArray
                                            {...this.props}
                                            CorpusTextPartlingualityType={LanguageDescriptionTextPartlingualityType}
                                            CorpusAudioPartlingualityType={[]}
                                            CorpusVideoPartlingualityType={LanguageDescriptionVideoPartlingualityType}
                                            CorpusImagePartlingualityType={LanguageDescriptionImagePartlingualityType}
                                            media_typeText={media_typeText}
                                            media_typeTextNumerical={""}
                                            media_typeImage={media_typeImage}
                                            media_typeVideo={media_typeVideo}
                                            media_typeAudio={""}
                                            {...GenericSchemaParser.getFormElement("dataset_distribution", subclass.dataset_distribution)}
                                            subclass={subclass}
                                            model={model}
                                            initialValueArray={model.described_entity.lr_subclass.dataset_distribution || []}
                                            field="dataset_distribution"
                                            lr_subclass={true}
                                            updateModel_array={this.updateModel_generic_array}
                                            datasets={this.state.datasets}
                                        />
                                    </div>

                                </Grid>
                            </Grid>
                        </VerticalTabPanel>
                    </div>
                </div>
            </form>

        </div>
    }
}