import React from "react";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
//import LanguageSpecificTextList from "../editorCommonComponents/LanguageSpecificTextList";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
//import LRAutocomplete from "../EditorCorpusComponents/LRAutocomplete";
//import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
//import Website from "../editorCommonComponents/Website";

export default class LdSubclassGrammarItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }

    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        this.setState({ initialValue });
    }

    updateModel__array = (field, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[field]) {
            initialValue[field] = [];
        }
        initialValue[field] = valueArray;
        this.setState({ initialValue });

    }

    setObjectBoolean = (e) => {
        const { initialValue } = this.state;
        if (initialValue.weighted_grammar === null || initialValue.weighted_grammar === undefined) {
            initialValue.weighted_grammar = null;
        }
        initialValue.weighted_grammar = e.target.value === "true";
        this.setState({ initialValue });
    }

    onBlur = () => {
        this.props.setValues(this.state.initialValue);
    }

    render() {
        const { initialValue } = this.state;
        return <div>
            <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100"  {...GenericSchemaParser.getFormElement("encoding_level", this.props.encoding_level)} initialValuesArray={initialValue.encoding_level || []} field="encoding_level" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"><LanguageSpecificText {...this.props.formalism} defaultValueObj={initialValue.formalism || { "en": "" }} field="formalism" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100"  {...GenericSchemaParser.getFormElement("ld_task", this.props.ld_task)} lr_subclass={true} initialValuesArray={initialValue.ld_task || []} field="ld_task" updateModel_array={this.updateModel__array} /></div>

            {/*<div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100"  {...GenericSchemaParser.getFormElement("grammatical_phenomena_coverage", this.props.grammatical_phenomena_coverage)} lr_subclass={true} initialValuesArray={initialValue.grammatical_phenomena_coverage || []} field="grammatical_phenomena_coverage" updateModel_array={this.updateModel__array} /></div>
            
            <div className="pb-3"><LanguageSpecificTextList {...this.props.theoretic_model} default_valueArray={initialValue.theoretic_model || []} field="theoretic_model" setLanguageSpecifictextList={this.setObjectGeneral} /></div>
             <div className="pb-3"><LanguageSpecificText {...this.props.attached_lexicon_position} defaultValueObj={initialValue.attached_lexicon_position || { "en": "" }} field="attached_lexicon_position" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pb-3"><LanguageSpecificText {...this.props.robustness} defaultValueObj={initialValue.robustness || { "en": "" }} field="robustness" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pb-3"><LanguageSpecificText {...this.props.shallowness} defaultValueObj={initialValue.shallowness || { "en": "" }} field="shallowness" setLanguageSpecificText={this.setObjectGeneral} /></div>

            <div className="pb-3"><LanguageSpecificText {...this.props.output} defaultValueObj={initialValue.output || { "en": "" }} field="output" setLanguageSpecificText={this.setObjectGeneral} /></div>

            <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("requires_lr", this.props.requires_lr)} initialValueArray={initialValue.requires_lr || []} field="requires_lr" updateModel_Array={this.setObjectGeneral} /> </div> 
            <div className="pb-3"><RecordRadioBoolean className="wd-100" type={this.props.weighted_grammar.type} required={this.props.weighted_grammar.required} label={this.props.weighted_grammar.label} help_text={this.props.weighted_grammar.help_text} default_value={initialValue.weighted_grammar} handleBooleanChange={this.setObjectBoolean} /></div>
            <div className="pb-3"><Website className="wd-100" {...GenericSchemaParser.getFormElement("related_lexicon_type", this.props.related_lexicon_type)} website={initialValue.related_lexicon_type} field="related_lexicon_type" updateModel_website={this.setObjectGeneral}/></div>
            <div className="pb-3"><Website className="wd-100" {...GenericSchemaParser.getFormElement("compatible_lexicon_type", this.props.compatible_lexicon_type)} website={initialValue.compatible_lexicon_type} field="compatible_lexicon_type" updateModel_website={this.setObjectGeneral}/></div>
            */}
        </div>
    }

}