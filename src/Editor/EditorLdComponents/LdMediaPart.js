import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/navigation-arrows-down-1.svg";
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import LdVideoPartItem from "./LdVideoPartItem";
import LdTextPartItem from "./LdTextPartItem";
import LdImagePartItem from "./LdImagePartItem";
import { ldMediaPartTextObj, ldMediaPartVideoObj, ldMediaPartImageObj } from "../Models/LdModel";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css


function PartChoice(props) {
    return <div>
        <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...props} required={true} default_value=""
            choices={["text part", "video part", "image part"].map((item) => { return { "display_name": item, "value": item, index: props.index } })}
            setSelectedvalue={props.setPart} /></div>
    </div>
}

export default class LdMediaPart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [], expanded: { index: 0, isExpanded: true } };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || []
        };
    }

    setValues = (action, index, value) => {
        const { initialValueArray } = this.state;
        const { model } = this.props;
        switch (action) {
            case "addArrayItem":
                if (initialValueArray.filter(item => item.ld_media_type !== "ld_media_type").length !== initialValueArray.length) {
                    return;
                }
                initialValueArray.push({ ld_media_type: "ld_media_type" });
                //this.props.updateModel_Array(this.props.field, initialValueArray, this.props.lr_subclass);
                this.setState({ initialValueArray });
                this.setState({ expanded: { index: initialValueArray.length - 1, isExpanded: true } });
                break;
            case "removeArrayItem":
                initialValueArray.splice(index, 1);
                //not sure if this is the place for this, but if we remove for example a text part that has dataset_distribution.distribution_text_feature filled out, then DistributionTextArray will remain in the model 
                //if  we remove text_part --- but it should't --- have to ask if there is a mapping/relation betwenn media parts and dataset_distribution.
                if (initialValueArray.filter(item => item.media_type === "http://w3id.org/meta-share/meta-share/text").length === 0) {
                    model.described_entity.lr_subclass.dataset_distribution && model.described_entity.lr_subclass.dataset_distribution.map((distributionItem, distributionIndex) => {
                        distributionItem.distribution_text_feature ? distributionItem.distribution_text_feature = [] : void 0;
                        return { distributionItem }
                    })
                }
                if (initialValueArray.filter(item => item.media_type === "http://w3id.org/meta-share/meta-share/video").length === 0) {
                    model.described_entity.lr_subclass.dataset_distribution && model.described_entity.lr_subclass.dataset_distribution.map((distributionItem, distributionIndex) => {
                        distributionItem.distribution_video_feature ? distributionItem.distribution_video_feature = [] : void 0;
                        return { distributionItem }
                    })
                }
                if (initialValueArray.filter(item => item.media_type === "http://w3id.org/meta-share/meta-share/image").length === 0) {
                    model.described_entity.lr_subclass.dataset_distribution && model.described_entity.lr_subclass.dataset_distribution.map((distributionItem, distributionIndex) => {
                        distributionItem.distribution_image_feature ? distributionItem.distribution_image_feature = [] : void 0;
                        return { distributionItem }
                    })
                }
                if (initialValueArray.length === 0) {
                    this.props.updateModel_Array(this.props.field, [], this.props.lr_subclass);
                    return;
                }
                this.props.updateModel_Array(this.props.field, initialValueArray, this.props.lr_subclass);
                break;
            case "setPart":
                initialValueArray[index] = value;
                this.props.updateModel_Array(this.props.field, initialValueArray, this.props.lr_subclass);
                break;
            default:
                break;
        }
        this.setState({ initialValueArray }, this.onBlur);
        //this.props.updateModel_Array(this.props.field, initialValueArray);
    }

    setPart = (e, val) => {
        const { initialValueArray } = this.state;
        if (val[0].value === "text part") {
            initialValueArray[val[0].index] = JSON.parse(JSON.stringify(ldMediaPartTextObj));
        } else if (val[0].value === "video part") {
            initialValueArray[val[0].index] = JSON.parse(JSON.stringify(ldMediaPartVideoObj));
        } else if (val[0].value === "image part") {
            initialValueArray[val[0].index] = JSON.parse(JSON.stringify(ldMediaPartImageObj));
        }
        this.setState({ initialValueArray }, this.onBlur);
    }

    updateModel = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index] = value;
        this.setState(initialValueArray);
        this.props.updateModel_Array(this.props.field, this.state.initialValueArray, this.props.lr_subclass);
    }

    onBlur = () => {
        const array = this.state.initialValueArray;
        this.props.updateModel_Array(this.props.field, array.length ? array : [], this.props.lr_subclass);
    }

    removeWarning = (e, index, ld_media_type) => {
        e.stopPropagation();
        confirmAlert({
            //title: 'Confirm to remove Corpus Part',
            message: messages.editor_remove_corpus_part(index, ld_media_type),
            buttons: [
                {
                    label: 'Yes',
                    onClick: (e) => this.setValues("removeArrayItem", index)
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }


    render() {
        const { initialValueArray } = this.state;

        initialValueArray.length === 0 && initialValueArray.push({ ld_media_type: "ld_media_type" });
        return <div className="mb2" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={10}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
            </Grid>
            {initialValueArray.map((item, index) => {
                if (item.ld_media_type === "ld_media_type") {
                    return <div key={index}>
                        {!this.props.disabled && <PartChoice {...this.props.formElements.ld_media_type} index={index} setPart={this.setPart} />}
                        {!this.props.disabled && index !== 0 && <Tooltip title={`${messages.array_elements_remove}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{`${messages.array_elements_remove}`}</Button></Tooltip>}
                    </div>
                } else if (item.ld_media_type === "LanguageDescriptionTextPart") {
                    const front_end_part_display_value = "text part";
                    return <div key={index}>
                        <Accordion className="FunctionBox--accordions--editor" expanded={this.state.expanded.index === index && this.state.expanded.isExpanded} onChange={() => {
                            if (this.state.expanded.index === index) {
                                this.setState({ expanded: { index: index, isExpanded: !this.state.expanded.isExpanded } }, this.onBlur)
                            } else {
                                this.setState({ expanded: { index: index, isExpanded: true } }, this.onBlur)
                            }
                        }
                        }>
                            <AccordionSummary expandIcon={<ExpandMoreIcon className="purple--font" />} aria-controls="panel1a-content" id="panel1a-header" className="FunctionBox--accordions__editortitle">
                                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                                    <Grid item sm={10}>
                                        <Typography variant="h4" className="section-links" >{front_end_part_display_value}</Typography>
                                    </Grid>
                                    <Grid item sm={2}>
                                        {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_part_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, index, item.ld_media_type)}>{`${messages.array_elements_remove} ${front_end_part_display_value}`}</Button></Tooltip>}
                                    </Grid>
                                </Grid>
                            </AccordionSummary>
                            <AccordionDetails>
                                <LdTextPartItem index={index} {...this.props.textPart} initialValue={item} setValues={this.setValues} />
                            </AccordionDetails>
                        </Accordion>
                        <div className="mb2 pt1">
                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" spacing={1}>
                                <Grid item>
                                    {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_part_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, index, item.ld_media_type)}>{`${messages.array_elements_remove} ${front_end_part_display_value}`}</Button></Tooltip>}
                                </Grid>
                                <Grid item sm={2}>
                                    {!this.props.disabled && (this.state.initialValueArray.length - 1) === index ?
                                        <Tooltip title={`${messages.corpus_part_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{`${messages.corpus_part_add} ${this.props.label}`}</Button></Tooltip>
                                        : <span></span>
                                    }
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                } else if (item.ld_media_type === "LanguageDescriptionVideoPart") {
                    const front_end_part_display_value = "video part";
                    return <div key={index}>
                        <Accordion className="FunctionBox--accordions--editor" expanded={this.state.expanded.index === index && this.state.expanded.isExpanded} onChange={(e) => {
                            if (this.state.expanded.index === index) {
                                this.setState({ expanded: { index: index, isExpanded: !this.state.expanded.isExpanded } }, this.onBlur)
                            } else {
                                this.setState({ expanded: { index: index, isExpanded: true } }, this.onBlur)
                            }
                        }
                        }>
                            <AccordionSummary expandIcon={<ExpandMoreIcon className="purple--font" />} aria-controls="panel1a-content" id="panel1a-header" className="FunctionBox--accordions__editortitle">
                                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                                    <Grid item sm={10}>
                                        <Typography variant="h4" className="section-links" >{front_end_part_display_value}</Typography>
                                    </Grid>
                                    <Grid item sm={2}>
                                        {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_part_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, index, item.ld_media_type)}>{`${messages.array_elements_remove} ${front_end_part_display_value}`}</Button></Tooltip>}
                                    </Grid>
                                </Grid>
                            </AccordionSummary>
                            <AccordionDetails>
                                <LdVideoPartItem {...this.props.videoPart} index={index} initialValue={item} setValues={this.setValues} />
                            </AccordionDetails>
                        </Accordion>
                        <div className="mb2 pt1">
                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" spacing={1}>
                                <Grid item>
                                    {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_part_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, index, item.ld_media_type)}>{`${messages.array_elements_remove} ${front_end_part_display_value}`}</Button></Tooltip>}
                                </Grid>
                                <Grid item sm={2}>
                                    {!this.props.disabled && (this.state.initialValueArray.length - 1) === index ?
                                        <Tooltip title={`${messages.corpus_part_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{`${messages.corpus_part_add} ${this.props.label}`}</Button></Tooltip>
                                        : <span></span>
                                    }
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                } else if (item.ld_media_type === "LanguageDescriptionImagePart") {
                    const front_end_part_display_value = "image part";
                    return <div key={index}>
                        <Accordion className="FunctionBox--accordions--editor" expanded={this.state.expanded.index === index && this.state.expanded.isExpanded} onChange={() => {
                            if (this.state.expanded.index === index) {
                                this.setState({ expanded: { index: index, isExpanded: !this.state.expanded.isExpanded } }, this.onBlur)
                            } else {
                                this.setState({ expanded: { index: index, isExpanded: true } }, this.onBlur)
                            }
                        }
                        }>
                            <AccordionSummary expandIcon={<ExpandMoreIcon className="purple--font" />} aria-controls="panel1a-content" id="panel1a-header" className="FunctionBox--accordions__editortitle">

                                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                                    <Grid item sm={10}>
                                        <Typography variant="h4" className="section-links" >{front_end_part_display_value}</Typography>
                                    </Grid>
                                    <Grid item sm={2}>
                                        {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_part_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, index, item.ld_media_type)}>{`${messages.array_elements_remove} ${front_end_part_display_value}`}</Button></Tooltip>}
                                    </Grid>
                                </Grid>
                            </AccordionSummary>
                            <AccordionDetails>
                                <LdImagePartItem {...this.props.imagePart} index={index} initialValue={item} setValues={this.setValues} />
                            </AccordionDetails>
                        </Accordion>
                        <div className="mb2 pt1">
                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" spacing={1}>
                                <Grid item>
                                    {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_part_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, index, item.ld_media_type)}>{`${messages.array_elements_remove} ${front_end_part_display_value}`}</Button></Tooltip>}
                                </Grid>
                                <Grid item sm={2}>
                                    {!this.props.disabled && (this.state.initialValueArray.length - 1) === index ?
                                        <Tooltip title={`${messages.corpus_part_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{`${messages.corpus_part_add} ${this.props.label}`}</Button></Tooltip>
                                        : <span></span>
                                    }
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                }
                return <div key={index}></div>
            })
            }
        </div>
    }
}