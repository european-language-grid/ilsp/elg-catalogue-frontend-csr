import React from "react";
import LRAutocompleteSingle from "./LRAutocompleteSingle";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";

export default class RelationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { relationItem: props.relationItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            relationItem: nextProps.relationItem
        };
    }

    setObjectGeneral = (field, valueObj) => {
        //if (!valueObj) {
        //    return;
        //}
        const { relationItem } = this.state;
        relationItem[field] = valueObj;
        if (relationItem.hasOwnProperty('editor-placeholder')) {
            delete relationItem['editor-placeholder'];
        }
        this.setState({ relationItem }, this.onBlur);
    }

    setRelationObj = (field, valueObj) => {
        if (!valueObj) {
            return;
        }
        if (!Object.keys(valueObj).length) {
            return;
        }
        const { relationItem } = this.state;
        relationItem[field] = valueObj;
        if (relationItem.hasOwnProperty('editor-placeholder')) {
            delete relationItem['editor-placeholder'];
        }
        this.setState({ relationItem }, this.onBlur);
    }



    onBlur = () => {
        const { relationItem } = this.state;
        const exists_relation_type = (relationItem.relation_type && relationItem.relation_type.hasOwnProperty("en") && relationItem.relation_type["en"]) ? true : false;
        const exists_related_lr = (relationItem.related_lr && !relationItem.related_lr.hasOwnProperty("editor-placeholder")) ? true : false;
        if (exists_relation_type === false && exists_related_lr === false) {
            relationItem["editor-placeholder"] = true;
        } else {
            delete relationItem['editor-placeholder'];
        }
        this.props.updateModel("setrelationItem", this.props.relationItemIndex, relationItem);
    }


    render() {
        const { relationItem } = this.state;
        return <div onBlur={this.onBlur}>
            {/*relationItem && relationItem.related_lr && relationItem.related_lr.hasOwnProperty("resource_name") && <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.relation_type} defaultValueObj={relationItem.relation_type} field="relation_type" setLanguageSpecificText={this.setObjectGeneral} /></div>*/}
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.relation_type} defaultValueObj={relationItem.relation_type || { "en": "" }} field="relation_type" setLanguageSpecificText={this.setObjectGeneral} /></div>

            <div className="pt-3 pb-3"><LRAutocompleteSingle hideRemoveButton={true} index={"related_lr"} formStuff={this.props.formElements.related_lr.formElements} initialValue={relationItem.related_lr || {}} field="related_lr" updateModel={this.setRelationObj} /> </div>
        </div>
    }

}