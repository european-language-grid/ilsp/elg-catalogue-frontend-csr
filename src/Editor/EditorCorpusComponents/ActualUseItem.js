import React from "react";
import ProjectAutocomplete from "../editorCommonComponents/ProjectAutocomplete"
import LRAutocomplete from "./LRAutocomplete";
import IsDescribedByAutocomplete from "../EditorServiceToolComponents/IsDescribedByAutocomplete";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";

export default class ActualUseItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { actualUseItem: props.actualUseItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            actualUseItem: nextProps.actualUseItem
        };
    }

    setObjectGeneral = (field, valueObj) => {
        const { actualUseItem } = this.state;
        actualUseItem[field] = valueObj;
        this.setState({ actualUseItem }, this.onBlur);
    }

    updateModel__array = (obj2update, valueArray) => {
        const { actualUseItem } = this.state;
        if (!actualUseItem[obj2update]) {
            actualUseItem[obj2update] = [];
        }
        actualUseItem[obj2update] = valueArray;
        this.setState({ actualUseItem }, this.onBlur);
    }

    onBlur = () => {
        const { actualUseItem } = this.state;
        const exist_used_in_application = (actualUseItem.used_in_application && actualUseItem.used_in_application.length) ? true : false;
        const exist_has_outcomeFilled = (actualUseItem.has_outcome && actualUseItem.has_outcome.filter(item => !item.hasOwnProperty('editor-placeholder')).length) ? true : false;;
        const exist_usage_project = (actualUseItem.usage_project && actualUseItem.usage_project.filter(item => !item.hasOwnProperty("editor-placeholder")).length) ? true : false;;
        const exist_usage_report = (actualUseItem.usage_report && actualUseItem.usage_report.filter(item => !item.hasOwnProperty("editor-placeholder")).length) ? true : false;;
        const exist_actual_use_details = actualUseItem && actualUseItem.actual_use_details && actualUseItem.actual_use_details.hasOwnProperty("en") && actualUseItem.actual_use_details["en"] ? true : false;
        //console.log(exist_actual_use_details, exist_has_outcomeFilled, exist_usage_project, exist_usage_report, exist_used_in_application);
        if (exist_used_in_application === false && exist_has_outcomeFilled === false && exist_usage_project === false && exist_usage_report === false && exist_actual_use_details === false) {
            actualUseItem["editor-placeholder"] = true;
        } else {
            delete actualUseItem["editor-placeholder"];
        }
        this.props.updateModel("setactualUseItem", this.props.actualUseItemIndex, actualUseItem);
    }


    render() {
        const { actualUseItem } = this.state;
        //for tool service
        if (this.props.model && this.props.model.described_entity.lr_subclass && this.props.model.described_entity.lr_subclass.lr_type === "ToolService") {
            return <>
                {false && <div className="pt-3 pb-3"><AutocompleteRecommendedChoices className="wd-70" {...this.props.formElements.used_in_application}
                    recommended_choices={this.props.formElements.used_in_application.choices} initialValuesArray={actualUseItem.used_in_application || []} field="used_in_application" updateModel_array={this.updateModel__array} /></div>}
                {false && <div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.has_outcome} initialValueArray={actualUseItem.has_outcome || []} field="has_outcome" updateModel_Array={this.setObjectGeneral} /> </div>}
                {false && <div className="pt-3 pb-3"><ProjectAutocomplete {...this.props.formElements.usage_project} initialValueArray={actualUseItem.usage_project || []} field="usage_project" updateModel_Array={this.setObjectGeneral} />  </div>}
                {false && <div className="pt-3 pb-3"> <IsDescribedByAutocomplete className="wd-70" {...this.props.formElements.usage_report} initialValueArray={actualUseItem.usage_report || []} field="usage_report" updateModel_Array={this.setObjectGeneral} /></div>}
                {false && <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.actual_use_details} defaultValueObj={actualUseItem.actual_use_details || { "en": "" }} field="actual_use_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}
            </>
        }
        //for corpus
        return <>
            <div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.has_outcome} initialValueArray={actualUseItem.has_outcome || []} field="has_outcome" updateModel_Array={this.setObjectGeneral} /> </div>
            <div className="pt-3 pb-3"><AutocompleteRecommendedChoices className="wd-70" {...this.props.formElements.used_in_application}
                recommended_choices={this.props.formElements.used_in_application.choices} initialValuesArray={actualUseItem.used_in_application || []} field="used_in_application" updateModel_array={this.updateModel__array} /></div>
            <div className="pt-3 pb-3"><ProjectAutocomplete {...this.props.formElements.usage_project} initialValueArray={actualUseItem.usage_project || []} field="usage_project" updateModel_Array={this.setObjectGeneral} />  </div>
            <div className="pt-3 pb-3"> <IsDescribedByAutocomplete className="wd-70" {...this.props.formElements.usage_report} initialValueArray={actualUseItem.usage_report || []} field="usage_report" updateModel_Array={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.actual_use_details} defaultValueObj={actualUseItem.actual_use_details || { "en": "" }} field="actual_use_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>
        </>

    }



}