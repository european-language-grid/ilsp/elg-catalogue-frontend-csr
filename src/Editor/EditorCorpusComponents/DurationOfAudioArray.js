import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import { duration_of_audioObj } from "../Models/CorpusModel";
import DurationOfAudioItem from "./DurationOfAudioItem";
import messages from "./../../config/messages";
import Tooltip from '@material-ui/core/Tooltip';

export default class DurationOfAudioArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { duration_of_audioArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            duration_of_audioArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, duration_of_audioItemIndex, value) => {
        const { duration_of_audioArray } = this.state;
        switch (action) {
            case "addduration_of_audioItem":
                if (duration_of_audioArray.filter(item => item.amount !== null && item.duration_unit).length !== duration_of_audioArray.length) {
                    return;//do not add obj if elements are blank
                }
                duration_of_audioArray.push(JSON.parse(JSON.stringify(duration_of_audioObj)));
                break;
            case "removeduration_of_audioItem":
                duration_of_audioArray.splice(duration_of_audioItemIndex, 1);
                break;
            case "setduration_of_audioItem":
                duration_of_audioArray[duration_of_audioItemIndex] = value;
                this.props.updateModel_array(this.props.field, duration_of_audioArray);
                break;
            default: break;
        }
        this.setState({ duration_of_audioArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.duration_of_audioArray);
    }

    render() {
        const { duration_of_audioArray } = this.state;
        //duration_of_audioArray.length === 0 && duration_of_audioArray.push(JSON.parse(JSON.stringify(duration_of_audioObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {<Grid item sm={1}>
                    {duration_of_audioArray.length === 0 && <Tooltip title={`${messages.group_elements_create} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addduration_of_audioItem")}>{messages.group_elements_create}</Button></Tooltip>}
                </Grid>}
            </Grid>
            {
                duration_of_audioArray.map((duration_of_audioItem, duration_of_audioItemIndex) => {
                    return <div key={duration_of_audioItemIndex} onBlur={() => this.onBlur()}>
                        <Grid container direction="row" alignItems="baseline" justifyContent="flex-start" spacing={1} >
                            <Grid item xs={10}>
                                <DurationOfAudioItem  {...this.props} duration_of_audioItem={duration_of_audioItem} duration_of_audioItemIndex={duration_of_audioItemIndex} updateModel={this.setValues} />
                            </Grid>
                            <Grid item xs={2}>
                                <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button onClick={(e) => this.setValues("removeduration_of_audioItem", duration_of_audioItemIndex)}><RemoveCircleOutlineIcon className="small-icon" /></Button></Tooltip>
                                <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button onClick={(e) => this.setValues("addduration_of_audioItem")}><AddCircleOutlineIcon className="small-icon" /></Button></Tooltip>
                            </Grid>
                        </Grid>
                    </div>
                })
            }
        </div>
    }
}