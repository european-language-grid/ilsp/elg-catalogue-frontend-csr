import React from "react";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import SizeArray from "./SizeArray";


export default class DistributionImageItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { distribution_image_featureItem: props.distribution_image_featureItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            distribution_image_featureItem: nextProps.distribution_image_featureItem
        };
    }

    updateModel__array = (obj2update, valueArray) => {
        const { distribution_image_featureItem } = this.state;
        if (!distribution_image_featureItem[obj2update]) {
            distribution_image_featureItem[obj2update] = [];
        }
        distribution_image_featureItem[obj2update] = valueArray;
        this.setState({ distribution_image_featureItem }, this.onBlur());
    }


    onBlur = () => {
        const { distribution_image_featureItem } = this.state
        const exists_size = (distribution_image_featureItem.size && distribution_image_featureItem.size.filter(item => {
            if ((item.amount !== null && item.amount >= 0) ||
                item.size_unit ||
                (item.language && item.language.length) ||// needs change for full schema
                (item.domain && item.domain.length) ||// needs change for full schema
                (item.text_genre && item.text_genre.length) ||// needs change for full schema
                (item.audio_genre && item.audio_genre.length) ||// needs change for full schema
                (item.speech_genre && item.speech_genre.length) ||// needs change for full schema
                (item.image_genre && item.image_genre.length)// needs change for full schema
            ) {
                return true;
            } else { return false; }
        }
        ).length) ? true : false;
        const exists_data_format = (distribution_image_featureItem.data_format && distribution_image_featureItem.data_format.length) ? true : false;
        const exists_image_format = (distribution_image_featureItem.image_format && distribution_image_featureItem.image_format.length) ? true : false;//meeds change for full schema
        if (exists_size === false && exists_data_format === false && exists_image_format === false) {
            distribution_image_featureItem["editor-placeholder"] = true;
        } else {
            delete distribution_image_featureItem["editor-placeholder"];
        }
        this.props.updateModel("setdistribution_image_featureItem", this.props.distribution_image_featureItemIndex, distribution_image_featureItem);
    }


    render() {
        const { distribution_image_featureItem } = this.state;
        return <>
            <div className="pb-3"> <SizeArray {...this.props} {...this.props.formElements.size} initialValueArray={distribution_image_featureItem.size || []} field="size" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"><AutocompleteRecommendedChoices className="wd-100" {...this.props.formElements.data_format} recommended_choices={this.props.formElements.data_format.choices} initialValuesArray={distribution_image_featureItem.data_format || []} field="data_format" updateModel_array={this.updateModel__array} /></div>
            {/*to do image_format*/}
        </>

    }



}