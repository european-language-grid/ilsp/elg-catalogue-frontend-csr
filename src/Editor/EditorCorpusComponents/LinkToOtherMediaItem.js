import React from "react";
import RecordMultipleSelectList from "../editorCommonComponents/RecordMultipleSelectList";
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";

export default class LinkToOtherMediaItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { linkToOtherMediaItem: props.linkToOtherMediaItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            linkToOtherMediaItem: nextProps.linkToOtherMediaItem
        };
    }

    setString = (field, value) => {
        const { linkToOtherMediaItem } = this.state;
        linkToOtherMediaItem[field] = value;
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }

    setObjectGeneral = (field, valueObj) => {
        const { linkToOtherMediaItem } = this.state;
        linkToOtherMediaItem[field] = valueObj;
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }

    updateModel__array = (obj2update, valueArray) => {
        const { linkToOtherMediaItem } = this.state;
        if (!linkToOtherMediaItem[obj2update]) {
            linkToOtherMediaItem[obj2update] = [];
        }
        linkToOtherMediaItem[obj2update] = valueArray;
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }

    setsynchronized_with_textBoolean = (e) => {
        const { linkToOtherMediaItem } = this.state;
        if (linkToOtherMediaItem.synchronized_with_text === null || linkToOtherMediaItem.synchronized_with_text === undefined) {
            linkToOtherMediaItem.synchronized_with_text = null;
        }
        linkToOtherMediaItem.synchronized_with_text = e.target.value === "true";
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }
    setsynchronized_with_videoBoolean = (e) => {
        const { linkToOtherMediaItem } = this.state;
        if (linkToOtherMediaItem.synchronized_with_video === null || linkToOtherMediaItem.synchronized_with_video === undefined) {
            linkToOtherMediaItem.synchronized_with_video = null;
        }
        linkToOtherMediaItem.synchronized_with_video = e.target.value === "true";
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }
    synchronized_with_audioBoolean = (e) => {
        const { linkToOtherMediaItem } = this.state;
        if (linkToOtherMediaItem.synchronized_with_audio === null || linkToOtherMediaItem.synchronized_with_audio === undefined) {
            linkToOtherMediaItem.synchronized_with_audio = null;
        }
        linkToOtherMediaItem.synchronized_with_audio = e.target.value === "true";
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }
    setsynchronized_with_imageBoolean = (e) => {
        const { linkToOtherMediaItem } = this.state;
        if (linkToOtherMediaItem.synchronized_with_image === null || linkToOtherMediaItem.synchronized_with_image === undefined) {
            linkToOtherMediaItem.synchronized_with_image = null;
        }
        linkToOtherMediaItem.synchronized_with_image = e.target.value === "true";
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }
    setsynchronized_with_text_numericalBoolean = (e) => {
        const { linkToOtherMediaItem } = this.state;
        if (linkToOtherMediaItem.synchronized_with_text_numerical === null || linkToOtherMediaItem.synchronized_with_text_numerical === undefined) {
            linkToOtherMediaItem.synchronized_with_text_numerical = null;
        }
        linkToOtherMediaItem.synchronized_with_text_numerical = e.target.value === "true";
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }

    //setSelectListAnnotationMode = (event, selectedObjArray) => {
    //    const { linkToOtherMediaItem } = this.state;
    //    linkToOtherMediaItem.linkToOtherMedia_mode = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
    //    this.setState({ linkToOtherMediaItem });
    //}

    setMultipleSelectListOtherMedia = (selectedObj) => {
        const { linkToOtherMediaItem } = this.state;
        if (!linkToOtherMediaItem.other_media) {
            linkToOtherMediaItem.other_media = [];
        }
        linkToOtherMediaItem.other_media = selectedObj.map(item => item.value);
        this.setState({ linkToOtherMediaItem }, this.onBlur);
    }

    onBlur = () => {

        const { linkToOtherMediaItem } = this.state;
        const exist_synchronized_with_text = linkToOtherMediaItem.synchronized_with_text ? true : false;
        const exist_synchronized_with_audio = linkToOtherMediaItem.synchronized_with_audio ? true : false;
        const exist_synchronized_with_video = linkToOtherMediaItem.synchronized_with_video ? true : false;
        const exist_synchronized_with_text_numerical = linkToOtherMediaItem.synchronized_with_text_numerical ? true : false;
        const exists_annotation_type = linkToOtherMediaItem.other_media && linkToOtherMediaItem.other_media.length ? true : false;
        const exist_media_type_details = linkToOtherMediaItem && linkToOtherMediaItem.media_type_details && linkToOtherMediaItem.media_type_details.hasOwnProperty("en") && linkToOtherMediaItem.media_type_details["en"] ? true : false;
        if (exist_synchronized_with_text === false && exist_synchronized_with_audio === false && exist_synchronized_with_video === false && exist_synchronized_with_text_numerical === false && 
            exists_annotation_type === false && exist_media_type_details === false) {
            linkToOtherMediaItem["editor-placeholder"] = true;
        } else {
            delete linkToOtherMediaItem["editor-placeholder"];
        }
        this.props.updateModel("setlinkToOtherMediaItem", this.props.linkToOtherMediaItemIndex, linkToOtherMediaItem);
    }



    render() {
        const { linkToOtherMediaItem } = this.state;
        const synchronized_with_text_required = linkToOtherMediaItem.other_media.includes("http://w3id.org/meta-share/meta-share/text");
        const synchronized_with_audio_required = linkToOtherMediaItem.other_media.includes("http://w3id.org/meta-share/meta-share/audio");
        const synchronized_with_video_required = linkToOtherMediaItem.other_media.includes("http://w3id.org/meta-share/meta-share/video");
        const synchronized_with_text_numerical_required = linkToOtherMediaItem.other_media.includes("http://w3id.org/meta-share/meta-share/textNumerical");
        var linkToOtherMediaItem_default_valueArray = linkToOtherMediaItem.other_media || [];
        linkToOtherMediaItem_default_valueArray = this.props.formElements.other_media.choices.filter(item => linkToOtherMediaItem_default_valueArray.includes(item.value)).map(item => item.display_name) || [];
        return <>
            <div className="pt-3 pb-3"><RecordMultipleSelectList className="wd-100" 
            label={this.props.formElements.other_media.label}
            type={this.props.formElements.other_media.type}
            help_text={this.props.formElements.other_media.help_text}
            required={this.props.formElements.other_media.required}
            choices={this.props.formElements.other_media.choices} default_valueArray={linkToOtherMediaItem_default_valueArray} field="other_media" setMultiplevalueSelections={this.setMultipleSelectListOtherMedia} /></div>

            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.media_type_details} defaultValueObj={linkToOtherMediaItem.media_type_details} field="media_type_details" setLanguageSpecificText={this.setObjectGeneral} /></div>

            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                label={this.props.formElements.synchronized_with_text.label}
                help_text={this.props.formElements.synchronized_with_text.help_text}
                required={synchronized_with_text_required}
                default_value={linkToOtherMediaItem.synchronized_with_text} handleBooleanChange={this.setsynchronized_with_textBoolean} /></div>

            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                label={this.props.formElements.synchronized_with_audio.label}
                help_text={this.props.formElements.synchronized_with_audio.help_text}
                required={synchronized_with_audio_required}
                default_value={linkToOtherMediaItem.synchronized_with_audio} handleBooleanChange={this.synchronized_with_audioBoolean} /></div>

            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                label={this.props.formElements.synchronized_with_video.label}
                help_text={this.props.formElements.synchronized_with_video.help_text}
                required={synchronized_with_video_required}
                default_value={linkToOtherMediaItem.synchronized_with_video} handleBooleanChange={this.setsynchronized_with_videoBoolean} /></div>

            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                label={this.props.formElements.synchronized_with_text_numerical.label}
                help_text={this.props.formElements.synchronized_with_text_numerical.help_text}
                required={synchronized_with_text_numerical_required}
                default_value={linkToOtherMediaItem.synchronized_with_text_numerical} handleBooleanChange={this.setsynchronized_with_text_numericalBoolean} /></div>

        </>

    }



}