import React from "react";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import LanguageModel from "../EditorServiceToolComponents/LanguageModel";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import MediaTypeAutocomplete from "../editorCommonComponents/MediaTypeAutocomplete";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import { videoGenreObj, video_genre_identifierObj } from "../Models/GenericModels";
import { TextField } from "@material-ui/core";
import LanguageSpecificTextList from "../editorCommonComponents/LanguageSpecificTextList";
import AnnotationArray from "./AnnotationArray";
//import LinkToOtherMediaArray from "./LinkToOtherMediaArray";

export default class CorpusVideoPartItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }

    setNumber = (field, value) => {
        if (value.length === 0) {
            value = null;
        } else {
            value = Number(value);
        }
        const { initialValue } = this.state;
        initialValue[field] = value;
        this.setState({ initialValue });
    }


    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        this.setState({ initialValue });
    }

    updateModel__array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        this.setState({ initialValue }, this.onBlur());

    }

    /*setSelectListLingualityType = (event, selectedObjArray) => {
        const { initialValue } = this.state;
        initialValue.linguality_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ initialValue });
    }*/

    setSelectListMultiLingualityType = (event, selectedObjArray) => {
        const { initialValue } = this.state;
        initialValue.multilinguality_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ initialValue });
    }

    onBlur = () => {
        const { initialValue } = this.state;
        const exist_type_of_video_content = initialValue.type_of_video_content && initialValue.type_of_video_content.length ? true : false;
        const exists_multilinguality_type = initialValue.multilinguality_type ? true : false;
        const exists_multilinguality_type_details = initialValue && initialValue.multilinguality_type_details && initialValue.multilinguality_type_details.hasOwnProperty("en") && initialValue.multilinguality_type_details["en"] ? true : false;
        const exists_language = initialValue.language && initialValue.language.filter(l => l.language_tag || l.language_id || l.script_id || l.region_id || (l.variant_id && l.variant_id.length) || (l.language_variety_name && l.language_variety_name.hasOwnProperty("pk") && l.language_variety_name["en"])).length ? true : false;
        const exists_video_genre = initialValue.video_genre && initialValue.video_genre.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_number_of_participants = initialValue.number_of_participants !== null && initialValue.number_of_participants >= 0 ? true : false;
        const exists_dialect_accent_of_participants = initialValue.dialect_accent_of_participants && initialValue.dialect_accent_of_participants.length ? true : false;
        const exists_geographic_distribution_of_participants = initialValue.geographic_distribution_of_participants && initialValue.geographic_distribution_of_participants.length ? true : false;
        const exists_annotation = initialValue.annotation && initialValue.annotation.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        //const exists_link_to_other_media = initialValue && initialValue.link_to_other_media.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        if (exist_type_of_video_content === false && exists_language === false && exists_video_genre === false && exists_number_of_participants === false && exists_dialect_accent_of_participants === false && exists_geographic_distribution_of_participants === false &&
            exists_annotation === false && exists_multilinguality_type === false && exists_multilinguality_type_details === false) {
            initialValue["editor-placeholder"] = true;
        } else {
            delete initialValue["editor-placeholder"];
        }
        this.props.setValues("setPart", this.props.index, initialValue);
    }


    render() {
        const { initialValue } = this.state;
        const { corpus_subclass_default_value } = this.props;
        const showMultilinguality = (initialValue.language && initialValue.language.length >= 2) ? true : false;
        const showAnnotation = ["http://w3id.org/meta-share/meta-share/annotatedCorpus", "http://w3id.org/meta-share/meta-share/annotationsCorpus"].includes(corpus_subclass_default_value);//needs more work in order to remove annotationArray if corpus_subclass changes value

        if (!showMultilinguality) {
            initialValue.multilinguality_type = null;
            initialValue.multilinguality_type_details = null;
        }

        return <div onBlur={this.onBlur}>
            <div><LanguageModel className="wd-100" group_className={"nested--group"} {...GenericSchemaParser.getFormElement("language", this.props.language)} initialValuesArray={initialValue.language || []} field="language" updateModel_array={this.updateModel__array} /></div>
            {showMultilinguality && <>
                <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.multilinguality_type} choices={this.props.multilinguality_type.choices} default_value={initialValue.multilinguality_type} required={showMultilinguality} setSelectedvalue={this.setSelectListMultiLingualityType} /></div>
                <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.multilinguality_type_details} defaultValueObj={initialValue.multilinguality_type_details || { "en": "" }} field="multilinguality_type_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>
            </>}

            <div className="pt-3 pb-3"><LanguageSpecificTextList {...this.props.type_of_video_content} default_valueArray={initialValue.type_of_video_content || []} field="type_of_video_content" setLanguageSpecifictextList={this.updateModel__array} /></div>

            <div><MediaTypeAutocomplete
                label={this.props.video_genre.label}
                help_text={this.props.video_genre.help_text}
                category_help_text={GenericSchemaParser.getFormElement("video_genre", this.props.video_genre).formElements.category_label.help_text}
                category_label={GenericSchemaParser.getFormElement("video_genre", this.props.video_genre).formElements.category_label.label}
                category_required={GenericSchemaParser.getFormElement("video_genre", this.props.video_genre).formElements.category_label.required}
                category_type={GenericSchemaParser.getFormElement("video_genre", this.props.video_genre).formElements.category_label.type}
                mediaType_choices={GenericSchemaParser.getFormElement("video_genre", this.props.video_genre).formElements.video_genre_identifier.formElements.video_genre_classification_scheme.choices}
                mediaType_help_text={this.props.video_genre.child.children.video_genre_identifier.children.video_genre_classification_scheme.help_text}
                mediaType_label={this.props.video_genre.child.children.video_genre_identifier.children.video_genre_classification_scheme.label}
                mediaType_read_only={this.props.video_genre.child.children.video_genre_identifier.children.video_genre_classification_scheme.read_only}
                mediaType_required={this.props.video_genre.child.children.video_genre_identifier.children.video_genre_classification_scheme.required}
                value_label={this.props.video_genre.child.children.video_genre_identifier.children.value.label}
                value_required={this.props.video_genre.child.children.video_genre_identifier.children.value.required}
                value_placeholder={this.props.video_genre.child.children.video_genre_identifier.children.value.placeholder ? this.props.text_type.child.children.video_genre_identifier.children.value.placeholder : ""}
                value_help_text={this.props.video_genre.child.children.video_genre_identifier.children.value.help_text}
                media_type_identifier_label={this.props.video_genre.child.children.video_genre_identifier.label}
                media_type_identifier_help_text={this.props.video_genre.child.children.video_genre_identifier.help_text}
                mediaTypeObj={videoGenreObj}
                media_type_identifierObj={video_genre_identifierObj}
                media_type_identifier="video_genre_identifier"
                entityType={"video_genre"}
                media_type_classification_scheme="video_genre_classification_scheme"
                default_valueArray={initialValue.video_genre || []} updateModel_MediaType={this.updateModel__array} /></div>

            <div className="pt-3 pb-3"><TextField
                className="wd-100"
                variant="outlined"
                type="number"
                required={this.props.number_of_participants.required}
                disabled={this.props.number_of_participants.read_only}
                label={this.props.number_of_participants.label}
                helperText={this.props.number_of_participants.help_text}
                value={initialValue.number_of_participants === null ? "" : initialValue.number_of_participants}
                onChange={(e) => { this.setNumber("number_of_participants", e.target.value) }}
                inputProps={{ min: `${this.props.number_of_participants.min_value}`, max: `${this.props.number_of_participants.max_value}`, step: "1" }}
            /> </div>

            <div><LanguageSpecificTextList {...this.props.dialect_accent_of_participants} default_valueArray={initialValue.dialect_accent_of_participants || []} field="dialect_accent_of_participants" setLanguageSpecifictextList={this.updateModel__array} /></div>
            <div><LanguageSpecificTextList  {...this.props.geographic_distribution_of_participants} default_valueArray={initialValue.geographic_distribution_of_participants || []} field="geographic_distribution_of_participants" setLanguageSpecifictextList={this.updateModel__array} /></div>
            {showAnnotation && <div><AnnotationArray {...GenericSchemaParser.getFormElement("annotation", this.props.annotation)} initialValueArray={initialValue.annotation || []} field="annotation" updateModel_array={this.updateModel__array} /></div>}
            {/*<div className="pt-3 pb-3"><LinkToOtherMediaArray {...GenericSchemaParser.getFormElement("link_to_other_media", this.props.link_to_other_media)} initialValueArray={initialValue.link_to_other_media || []} field="link_to_other_media" updateModel_array={this.updateModel__array} /></div>*/}

        </div>

    }
}