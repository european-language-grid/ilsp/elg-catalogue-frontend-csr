//it is not used. distribution unspecified part is not a multiple field. 
//keeping it just in case


/*import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { distribution_unspecified_featureObj } from "../Models/CorpusModel";
import DistributionUnspecifiedItem from "./DistributionUnspecifiedItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class DistributionUnspecifiedArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { distribution_unspecified_featureArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            distribution_unspecified_featureArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, distribution_unspecified_featureItemIndex, value) => {
        const { distribution_unspecified_featureArray } = this.state;
        switch (action) {
            case "adddistribution_unspecified_featureItem":
                const filteredArray = distribution_unspecified_featureArray.filter(item => {
                    if (item.data_format && item.data_format.length) {
                    } else {
                        return false;
                    }
                    if (item.size) {
                        if (item.size.filter(size => {
                            if (!(size.amount !== null && Number(size.amount) >= 0)) {
                                return false;
                            } if (!size.size_unit) {
                                return false;
                            }
                            return true;
                        }).length !== item.size.length) {
                            return false;
                        }
                    }
                    return true;
                })
                if (filteredArray.length !== distribution_unspecified_featureArray.length) {
                    return;//do not add element if required fields are blank
                }
                distribution_unspecified_featureArray.push(JSON.parse(JSON.stringify(distribution_unspecified_featureObj)));
                break;
            case "removedistribution_unspecified_featureItem":
                distribution_unspecified_featureArray.splice(distribution_unspecified_featureItemIndex, 1);
                break;
            case "setdistribution_unspecified_featureItem":
                distribution_unspecified_featureArray[distribution_unspecified_featureItemIndex] = value;
                this.props.updateModel_array(this.props.field, distribution_unspecified_featureArray);
                break;
            default: break;
        }
        this.setState({ distribution_unspecified_featureArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.distribution_unspecified_featureArray);
    }

    render() {
        const { distribution_unspecified_featureArray } = this.state;
        distribution_unspecified_featureArray.length === 0 && distribution_unspecified_featureArray.push(JSON.parse(JSON.stringify(distribution_unspecified_featureObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    {distribution_unspecified_featureArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("adddistribution_unspecified_featureItem")}>{messages.group_elements_create}</Button>}
    </Grid>*/}
{/*          </Grid>
            {
                distribution_unspecified_featureArray.map((distribution_unspecified_featureItem, distribution_unspecified_featureItemIndex) => {
                    return <div key={distribution_unspecified_featureItemIndex} onBlur={() => this.onBlur()}>
                        {(distribution_unspecified_featureItem.size && distribution_unspecified_featureItem.size.length > 0) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removedistribution_unspecified_featureItem", distribution_unspecified_featureItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }
                        {((!distribution_unspecified_featureItem.size && distribution_unspecified_featureItem.size.length === 0) && distribution_unspecified_featureItemIndex !== 0) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removedistribution_unspecified_featureItem", distribution_unspecified_featureItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }
                        <DistributionUnspecifiedItem  {...this.props} distribution_unspecified_featureItem={distribution_unspecified_featureItem} distribution_unspecified_featureItemIndex={distribution_unspecified_featureItemIndex} updateModel={this.setValues} />
                        <div className="mb2">{((distribution_unspecified_featureItem.size && distribution_unspecified_featureItem.size.length > 0) && ((distribution_unspecified_featureArray.length - 1) === distribution_unspecified_featureItemIndex)) ?
                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("adddistribution_unspecified_featureItem")}>{messages.array_elements_add}</Button></Tooltip>
                                </Grid>
                            </Grid>
                            : <span></span>
                        }
                        </div>
                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }
        </div>
    }
}*/}