import React from "react";
//import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import SizeArray from "./SizeArray";


export default class DistributionUnspecifiedItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { distribution_unspecified_featureItem: props.distribution_unspecified_featureItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            distribution_unspecified_featureItem: nextProps.distribution_unspecified_featureItem
        };
    }

    updateModel__array = (obj2update, valueArray) => {
        const { distribution_unspecified_featureItem } = this.state;
        if (!distribution_unspecified_featureItem[obj2update]) {
            distribution_unspecified_featureItem[obj2update] = [];
        }
        distribution_unspecified_featureItem[obj2update] = valueArray;
        this.setState({ distribution_unspecified_featureItem }, this.onBlur);
    }


    onBlur = () => {
        const { distribution_unspecified_featureItem } = this.state
        const exists_size = (distribution_unspecified_featureItem.size && distribution_unspecified_featureItem.size.filter(item => {
            if ((item.amount !== null && item.amount >= 0) ||
                item.size_unit ||
                (item.language && item.language.length) ||// needs change for full schema
                (item.domain && item.domain.length) ||// needs change for full schema
                (item.text_genre && item.text_genre.length) ||// needs change for full schema
                (item.audio_genre && item.audio_genre.length) ||// needs change for full schema
                (item.speech_genre && item.speech_genre.length) ||// needs change for full schema
                (item.image_genre && item.image_genre.length)// needs change for full schema
            ) {
                return true;
            } else { return false; }
        }
        ).length) ? true : false;
        const exists_data_format = (distribution_unspecified_featureItem.data_format && distribution_unspecified_featureItem.data_format.length) ? true : false;
        const exists_character_encoding = (distribution_unspecified_featureItem.character_encoding && distribution_unspecified_featureItem.character_encoding.length) ? true : false;
        if (exists_size === false && exists_data_format === false && exists_character_encoding === false) {
            distribution_unspecified_featureItem["editor-placeholder"] = true;
        } else {
            delete distribution_unspecified_featureItem["editor-placeholder"];
        }
        //this.props.updateModel("setdistribution_unspecified_featureItem", this.props.distribution_unspecified_featureItemIndex, distribution_unspecified_featureItem);
        this.props.updateModel(this.props.field, distribution_unspecified_featureItem);
    }


    render() {
        const { distribution_unspecified_featureItem } = this.state;
        return <>
            <div className="pb-3"> <SizeArray {...this.props} {...this.props.formElements.size} initialValueArray={distribution_unspecified_featureItem.size || []} field="size" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"><AutocompleteRecommendedChoices className="wd-100" {...this.props.formElements.data_format}
                recommended_choices={this.props.formElements.data_format.choices} initialValuesArray={distribution_unspecified_featureItem.data_format || []} field="data_format" updateModel_array={this.updateModel__array} /></div>
        </>

    }



}