import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import { actualUseObj } from "../Models/LrModel";
import ActualUseItem from "./ActualUseItem";
import messages from "./../../config/messages";
import Tooltip from '@material-ui/core/Tooltip';

export default class ActualUseArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { actualUseArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            actualUseArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, actualUseItemIndex, value) => {
        const { actualUseArray } = this.state;
        switch (action) {
            case "addactualUseItem":
                actualUseArray.push(JSON.parse(JSON.stringify(actualUseObj)));
                break;
            case "removeactualUseItem":
                if (actualUseArray.length === 1) {
                    this.props.updateModel_array(this.props.field, []);
                    return;
                }
                actualUseArray.splice(actualUseItemIndex, 1);
                break;
            case "setactualUseItem":
                actualUseArray[actualUseItemIndex] = value;
                this.props.updateModel_array(this.props.field, actualUseArray);
                break;
            default: break;
        }
        this.setState({ actualUseArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.actualUseArray);
    }

    render() {
        const { actualUseArray } = this.state;
        //console.log(this.props)
        actualUseArray.length === 0 && actualUseArray.push(JSON.parse(JSON.stringify(actualUseObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addactualUseItem")}><AddCircleOutlineIcon className="xsmall-icon"/></Button>      
                </Grid>*/}
            </Grid>
            {
                actualUseArray.map((actualUseItem, actualUseItemIndex) => {
                    return <div key={actualUseItemIndex} onBlur={() => this.onBlur()}>
                        {(actualUseItem.used_in_application && actualUseItem.used_in_application.length>0) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeactualUseItem", actualUseItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }
                        {(!actualUseItem.used_in_application && actualUseItem.used_in_application.length===0) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeactualUseItem", actualUseItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }

                        <ActualUseItem  {...this.props} actualUseItem={actualUseItem} actualUseItemIndex={actualUseItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            {((actualUseItem.used_in_application && actualUseItem.used_in_application.length > 0) && ((actualUseArray.length - 1) === actualUseItemIndex)) ?
                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item>
                                    <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addactualUseItem")}>{messages.array_elements_add}</Button></Tooltip>
                                    </Grid>
                                </Grid>
                                :<span></span>
                            }
                        </div>
                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }
        </div>
    }
}