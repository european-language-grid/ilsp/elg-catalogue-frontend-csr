import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import { linkToOtherMediaObj } from "../Models/CorpusModel";
import LinkToOtherMediaItem from "./LinkToOtherMediaItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class LinkToOtherMediaArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { linkToOtherMediaArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            linkToOtherMediaArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, linkToOtherMediaItemIndex, value) => {
        const { linkToOtherMediaArray } = this.state;
        switch (action) {
            case "addlinkToOtherMediaItem":
                linkToOtherMediaArray.push(JSON.parse(JSON.stringify(linkToOtherMediaObj)));
                break;
            case "removelinkToOtherMediaItem":
                linkToOtherMediaArray.splice(linkToOtherMediaItemIndex, 1);
                break;
            case "setlinkToOtherMediaItem":
                linkToOtherMediaArray[linkToOtherMediaItemIndex] = value;
                this.props.updateModel_array(this.props.field, linkToOtherMediaArray);
                break;
            default: break;
        }
        this.setState({ linkToOtherMediaArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.linkToOtherMediaArray);
    }

    render() {
        const { linkToOtherMediaArray } = this.state;
        //console.log(this.props)
        linkToOtherMediaArray.length === 0 && linkToOtherMediaArray.push(JSON.parse(JSON.stringify(linkToOtherMediaObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center"  justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addlinkToOtherMediaItem")}><AddCircleOutlineIcon className="xsmall-icon"/></Button>      
                </Grid>*/}
            </Grid>
            {  
                linkToOtherMediaArray.map((linkToOtherMediaItem, linkToOtherMediaItemIndex) => {
                    //console.log(linkToOtherMediaItem)
                    return <div key={linkToOtherMediaItemIndex} onBlur={() => this.onBlur()}>
                        <LinkToOtherMediaItem  {...this.props} linkToOtherMediaItem={linkToOtherMediaItem} linkToOtherMediaItemIndex={linkToOtherMediaItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">    
                        {linkToOtherMediaItem.other_media && linkToOtherMediaItem.other_media.length>0 && <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button onClick={(e) => this.setValues("removelinkToOtherMediaItem", linkToOtherMediaItemIndex)}><RemoveCircleOutlineIcon className="small-icon"/></Button></Tooltip>}
                        {linkToOtherMediaItem.other_media && linkToOtherMediaItem.other_media.length>0 && <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button onClick={(e) => this.setValues("addlinkToOtherMediaItem")}><AddCircleOutlineIcon className="small-icon"/></Button></Tooltip>}                       
                        </div>
                        <div style={{ marginTop: "5em" }} />
                    </div>
                })
             }
        </div>
    }
}