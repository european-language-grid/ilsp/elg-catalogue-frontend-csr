import React from "react";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import SizeArray from "./SizeArray";


export default class DistributionVideoItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { distribution_video_featureItem: props.distribution_video_featureItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            distribution_video_featureItem: nextProps.distribution_video_featureItem
        };
    }

    updateModel__array = (obj2update, valueArray) => {
        const { distribution_video_featureItem } = this.state;
        if (!distribution_video_featureItem[obj2update]) {
            distribution_video_featureItem[obj2update] = [];
        }
        distribution_video_featureItem[obj2update] = valueArray;
        this.setState({ distribution_video_featureItem }, this.onBlur);
    }


    onBlur = () => {
        const { distribution_video_featureItem } = this.state
        const exists_size = (distribution_video_featureItem.size && distribution_video_featureItem.size.filter(item => {
            if ((item.amount !== null && item.amount >= 0) ||
                item.size_unit ||
                (item.language && item.language.length) ||// needs change for full schema
                (item.domain && item.domain.length) ||// needs change for full schema
                (item.text_genre && item.text_genre.length) ||// needs change for full schema
                (item.audio_genre && item.audio_genre.length) ||// needs change for full schema
                (item.speech_genre && item.speech_genre.length) ||// needs change for full schema
                (item.image_genre && item.image_genre.length)// needs change for full schema
            ) {
                return true;
            } else { return false; }
        }
        ).length) ? true : false;
        const exists_data_format = (distribution_video_featureItem.data_format && distribution_video_featureItem.data_format.length) ? true : false;
        const exists_video_format = (distribution_video_featureItem.video_format && distribution_video_featureItem.video_format.length) ? true : false;//needs change for full schema

        if (exists_size === false && exists_data_format === false && exists_video_format === false) {
            distribution_video_featureItem["editor-placeholder"] = true;
        } else {
            delete distribution_video_featureItem["editor-placeholder"];
        }
        this.props.updateModel("setdistribution_video_featureItem", this.props.distribution_video_featureItemIndex, distribution_video_featureItem);
    }


    render() {
        const { distribution_video_featureItem } = this.state;
        return <>
            <div className="pb-3"> <SizeArray {...this.props} {...this.props} {...this.props.formElements.size} initialValueArray={distribution_video_featureItem.size || []} field="size" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"><AutocompleteRecommendedChoices className="wd-100" {...this.props.formElements.data_format} recommended_choices={this.props.formElements.data_format.choices} initialValuesArray={distribution_video_featureItem.data_format || []} field="data_format" updateModel_array={this.updateModel__array} /></div>
            {/*to do videο_format*/}
        </>

    }



}