import React from "react";
//import RecordSelectList from "../editorCommonComponents/RecordSelectList";
//import DomainAutocomplete from "../editorCommonComponents/DomainAutocomplete";
//import LanguageModel from "../EditorServiceToolComponents/LanguageModel";
//import MediaTypeAutocomplete from "../editorCommonComponents/MediaTypeAutocomplete";
import TextField from '@material-ui/core/TextField';
//import { audioGenreObj, audio_genre_identifierObj, speechGenreObj, speech_genre_identifierObj } from "../Models/GenericModels";
//import { textGenreObj, text_genre_identifierObj } from "../Models/GenericModels";
//import { videoGenreObj, video_genre_identifierObj } from "../Models/GenericModels";
//import { imageGenreObj, image_genre_identifierObj } from "../Models/GenericModels";
import AutocompleteRecommendedChoicesSelectOneOption from "../editorCommonComponents/AutocompleteRecommendedChoicesSelectOneOption";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

export default class SizeItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { sizeItem: props.sizeItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            sizeItem: nextProps.sizeItem
        };
    }

    setNumber = (field, value) => {
        if (value.length === 0) {
            value = null;
        } else {
            value = Number(value);
        }
        const { sizeItem } = this.state;
        sizeItem[field] = value;
        this.setState({ sizeItem },this.onBlur());
    }

    updateModel__array = (obj2update, valueArray) => {
        const { sizeItem } = this.state;
        if (!sizeItem[obj2update]) {
            sizeItem[obj2update] = [];
        }
        sizeItem[obj2update] = valueArray;
        this.setState({ sizeItem }, this.onBlur());
    }

    /* setArray = (field, array ) => {
        const { sizeItem } = this.state;
        if (!sizeItem[field]) sizeItem[field] = [];
        sizeItem[field] = array;
        this.setState({ sizeItem });
    }*/


    setArray = (field, array, showItem) => {
        const { sizeItem } = this.state;
        if (!sizeItem[field] || showItem === false) sizeItem[field] = [];
        sizeItem[field] = array;
        this.setState({ sizeItem }, this.onBlur());
    }

    /*setSelectListValueSizeUnit = (event, selectedObjArray) => {
        const { sizeItem } = this.state;
        sizeItem.size_unit = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ sizeItem }, this.onBlur());
    }*/

    setString = (field, value, lr_subclass) => {
        const { sizeItem } = this.state;
        sizeItem.size_unit = value;
        this.setState({ sizeItem }, this.onBlur());
    }

    onBlur = () => {
        const { sizeItem } = this.state;
        this.props.updateModel("setsizeItem", this.props.sizeItemIndex, sizeItem);
    }


    render() {
        const { sizeItem } = this.state;
        //the following are working ok, but not used in minimal version
        //const { CorpusTextPartlingualityType, CorpusAudioPartlingualityType, CorpusVideoPartlingualityType, CorpusImagePartlingualityType } = this.props;
        //const showItem = ['http://w3id.org/meta-share/meta-share/bilingual', 'http://w3id.org/meta-share/meta-share/multilingual'].includes(CorpusTextPartlingualityType) ||
        //    ['http://w3id.org/meta-share/meta-share/bilingual', 'http://w3id.org/meta-share/meta-share/multilingual'].includes(CorpusAudioPartlingualityType) ||
        //    ['http://w3id.org/meta-share/meta-share/bilingual', 'http://w3id.org/meta-share/meta-share/multilingual'].includes(CorpusVideoPartlingualityType) ||
        //    ['http://w3id.org/meta-share/meta-share/bilingual', 'http://w3id.org/meta-share/meta-share/multilingual'].includes(CorpusImagePartlingualityType);
        
        return <>

            <Grid container direction="row" alignItems="baseline" justifyContent="flex-start" spacing={1} >
                <Grid item xs={6}>
                <Typography variant="h3" className="section-links" >{this.props.formElements.amount.label} </Typography>
                    <TextField className="wd-100"
                        type={"number"}
                        required={this.props.formElements.amount.required}
                        label={this.props.formElements.amount.label}
                        helperText={this.props.formElements.amount.help_text} variant="outlined"
                        value={sizeItem.amount === null ? "" : sizeItem.amount}
                        inputProps={{ name: 'amount' }}
                        onChange={(e) => { this.setNumber("amount", e.target.value) }} />
                </Grid>
                <Grid item xs={6}>
                    <div className="wd-100">
                        <AutocompleteRecommendedChoicesSelectOneOption
                            className="wd-100"
                            {...this.props.formElements.size_unit}
                            initialValue={sizeItem.size_unit || ""}
                            field="size_unit"
                            lr_subclass={true}
                            updateModel_String={this.setString}
                        />
                    </div>
                </Grid>
            </Grid>
            {/*<div>
                <DomainAutocomplete
                    {...this.props.formElements.domain}
                    category_help_text={this.props.formElements.domain.formElements.category_label.help_text}
                    category_label={this.props.formElements.domain.formElements.category_label.label}
                    category_required={this.props.formElements.domain.formElements.category_label.required}
                    category_type={this.props.formElements.domain.formElements.category_label.type}
                    domain_choices={this.props.formElements.domain.formElements.domain_identifier.formElements.domain_classification_scheme.choices}
                    domain_help_text={this.props.formElements.domain.formElements.domain_identifier.formElements.domain_classification_scheme.help_text}
                    domain_label={this.props.formElements.domain.formElements.domain_identifier.formElements.domain_classification_scheme.label}
                    domain_read_only={this.props.formElements.domain.formElements.domain_identifier.formElements.domain_classification_scheme.read_only}
                    domain_required={this.props.formElements.domain.formElements.domain_identifier.formElements.domain_classification_scheme.required}
                    value_label={this.props.formElements.domain.formElements.domain_identifier.formElements.value.label}
                    value_required={this.props.formElements.domain.formElements.domain_identifier.formElements.value.required}
                    domain_identifier_label={this.props.formElements.domain.formElements.domain_identifier.formElements.domain_classification_scheme.label}
                    domain_identifier_help_text={this.props.formElements.domain.formElements.domain_identifier.formElements.domain_classification_scheme.help_text}
                    default_valueArray={sizeItem.domain || []} updateModel_Domain={this.updateModel__array} />
            </div>

            <div>{showItem && <LanguageModel className="wd-100" {...this.props.formElements.language} sizeItemsArray={sizeItem.language || []} showItem={showItem} field="language" updateModel_array={this.setArray} />}</div>

            

            <div>
                <MediaTypeAutocomplete
                    label={this.props.formElements.audio_genre.label}
                    help_text={this.props.formElements.audio_genre.help_text}
                    category_help_text={this.props.formElements.audio_genre.formElements.category_label.help_text}
                    category_label={this.props.formElements.audio_genre.formElements.category_label.label}
                    category_required={this.props.formElements.audio_genre.formElements.category_label.required}
                    category_type={this.props.formElements.audio_genre.formElements.category_label.type}
                    mediaType_choices={this.props.formElements.audio_genre.formElements.audio_genre_identifier.formElements.audio_genre_classification_scheme.choices}
                    mediaType_help_text={this.props.formElements.audio_genre.formElements.audio_genre_identifier.formElements.audio_genre_classification_scheme.help_text}
                    mediaType_label={this.props.formElements.audio_genre.formElements.audio_genre_identifier.formElements.audio_genre_classification_scheme.label}
                    mediaType_read_only={this.props.formElements.audio_genre.formElements.audio_genre_identifier.formElements.audio_genre_classification_scheme.read_only}
                    mediaType_required={this.props.formElements.audio_genre.formElements.audio_genre_identifier.formElements.audio_genre_classification_scheme.required}
                    value_label={this.props.formElements.audio_genre.formElements.audio_genre_identifier.formElements.value.label}
                    value_required={this.props.formElements.audio_genre.formElements.audio_genre_identifier.formElements.value.required}
                    media_type_identifier_label={this.props.formElements.audio_genre.formElements.audio_genre_identifier.label}
                    media_type_identifier_help_text={this.props.formElements.audio_genre.formElements.audio_genre_identifier.help_text}
                    mediaTypeObj={audioGenreObj}
                    media_type_identifierObj={audio_genre_identifierObj}
                    media_type_identifier="audio_genre_identifier"
                    entityType={"audio_genre"}
                    media_type_classification_scheme="audio_genre_classification_scheme"
                    default_valueArray={sizeItem.audio_genre || []} updateModel_MediaType={this.updateModel__array} /></div>

            <div>
                <MediaTypeAutocomplete
                    label={this.props.formElements.speech_genre.label}
                    help_text={this.props.formElements.speech_genre.help_text}
                    category_help_text={this.props.formElements.speech_genre.formElements.category_label.help_text}
                    category_label={this.props.formElements.speech_genre.formElements.category_label.label}
                    category_required={this.props.formElements.speech_genre.formElements.category_label.required}
                    category_type={this.props.formElements.speech_genre.formElements.category_label.type}
                    mediaType_choices={this.props.formElements.speech_genre.formElements.speech_genre_identifier.formElements.speech_genre_classification_scheme.choices}
                    mediaType_help_text={this.props.formElements.speech_genre.formElements.speech_genre_identifier.formElements.speech_genre_classification_scheme.help_text}
                    mediaType_label={this.props.formElements.speech_genre.formElements.speech_genre_identifier.formElements.speech_genre_classification_scheme.label}
                    mediaType_read_only={this.props.formElements.speech_genre.formElements.speech_genre_identifier.formElements.speech_genre_classification_scheme.read_only}
                    mediaType_required={this.props.formElements.speech_genre.formElements.speech_genre_identifier.formElements.speech_genre_classification_scheme.required}
                    value_label={this.props.formElements.speech_genre.formElements.speech_genre_identifier.formElements.value.label}
                    value_required={this.props.formElements.speech_genre.formElements.speech_genre_identifier.formElements.value.required}
                    media_type_identifier_label={this.props.formElements.speech_genre.formElements.speech_genre_identifier.label}
                    media_type_identifier_help_text={this.props.formElements.speech_genre.formElements.speech_genre_identifier.help_text}
                    mediaTypeObj={speechGenreObj}
                    media_type_identifierObj={speech_genre_identifierObj}
                    media_type_identifier="speech_genre_identifier"
                    entityType={"speech_genre"}
                    media_type_classification_scheme="speech_genre_classification_scheme"
                    default_valueArray={sizeItem.speech_genre || []} updateModel_MediaType={this.updateModel__array} /></div>

            <div>
                <MediaTypeAutocomplete
                    label={this.props.formElements.text_genre.label}
                    help_text={this.props.formElements.text_genre.help_text}
                    category_help_text={this.props.formElements.text_genre.formElements.category_label.help_text}
                    category_label={this.props.formElements.text_genre.formElements.category_label.label}
                    category_required={this.props.formElements.text_genre.formElements.category_label.required}
                    category_type={this.props.formElements.text_genre.formElements.category_label.type}
                    mediaType_choices={this.props.formElements.text_genre.formElements.text_genre_identifier.formElements.text_genre_classification_scheme.choices}
                    mediaType_help_text={this.props.formElements.text_genre.formElements.text_genre_identifier.formElements.text_genre_classification_scheme.help_text}
                    mediaType_label={this.props.formElements.text_genre.formElements.text_genre_identifier.formElements.text_genre_classification_scheme.label}
                    mediaType_read_only={this.props.formElements.text_genre.formElements.text_genre_identifier.formElements.text_genre_classification_scheme.read_only}
                    mediaType_required={this.props.formElements.text_genre.formElements.text_genre_identifier.formElements.text_genre_classification_scheme.required}
                    value_label={this.props.formElements.text_genre.formElements.text_genre_identifier.formElements.value.label}
                    value_required={this.props.formElements.text_genre.formElements.text_genre_identifier.formElements.value.required}
                    media_type_identifier_label={this.props.formElements.text_genre.formElements.text_genre_identifier.label}
                    media_type_identifier_help_text={this.props.formElements.text_genre.formElements.text_genre_identifier.help_text}
                    mediaTypeObj={textGenreObj}
                    media_type_identifierObj={text_genre_identifierObj}
                    media_type_identifier="text_genre_identifier"
                    entityType={"text_genre"}
                    media_type_classification_scheme="text_genre_classification_scheme"
                    default_valueArray={sizeItem.text_genre || []} updateModel_MediaType={this.updateModel__array} /></div>


            <div>
                <MediaTypeAutocomplete
                    label={this.props.formElements.video_genre.label}
                    help_text={this.props.formElements.video_genre.help_text}
                    category_help_text={this.props.formElements.video_genre.formElements.category_label.help_text}
                    category_label={this.props.formElements.video_genre.formElements.category_label.label}
                    category_required={this.props.formElements.video_genre.formElements.category_label.required}
                    category_type={this.props.formElements.video_genre.formElements.category_label.type}
                    mediaType_choices={this.props.formElements.video_genre.formElements.video_genre_identifier.formElements.video_genre_classification_scheme.choices}
                    mediaType_help_text={this.props.formElements.video_genre.formElements.video_genre_identifier.formElements.video_genre_classification_scheme.help_text}
                    mediaType_label={this.props.formElements.video_genre.formElements.video_genre_identifier.formElements.video_genre_classification_scheme.label}
                    mediaType_read_only={this.props.formElements.video_genre.formElements.video_genre_identifier.formElements.video_genre_classification_scheme.read_only}
                    mediaType_required={this.props.formElements.video_genre.formElements.video_genre_identifier.formElements.video_genre_classification_scheme.required}
                    value_label={this.props.formElements.video_genre.formElements.video_genre_identifier.formElements.value.label}
                    value_required={this.props.formElements.video_genre.formElements.video_genre_identifier.formElements.value.required}
                    media_type_identifier_label={this.props.formElements.video_genre.formElements.video_genre_identifier.label}
                    media_type_identifier_help_text={this.props.formElements.video_genre.formElements.video_genre_identifier.help_text}
                    mediaTypeObj={videoGenreObj}
                    media_type_identifierObj={video_genre_identifierObj}
                    media_type_identifier="video_genre_identifier"
                    entityType={"video_genre"}
                    media_type_classification_scheme="video_genre_classification_scheme"
                    default_valueArray={sizeItem.video_genre || []} updateModel_MediaType={this.updateModel__array} /></div>

            <div>
                <MediaTypeAutocomplete
                    label={this.props.formElements.image_genre.label}
                    help_text={this.props.formElements.image_genre.help_text}
                    category_help_text={this.props.formElements.image_genre.formElements.category_label.help_text}
                    category_label={this.props.formElements.image_genre.formElements.category_label.label}
                    category_required={this.props.formElements.image_genre.formElements.category_label.required}
                    category_type={this.props.formElements.image_genre.formElements.category_label.type}
                    mediaType_choices={this.props.formElements.image_genre.formElements.image_genre_identifier.formElements.image_genre_classification_scheme.choices}
                    mediaType_help_text={this.props.formElements.image_genre.formElements.image_genre_identifier.formElements.image_genre_classification_scheme.help_text}
                    mediaType_label={this.props.formElements.image_genre.formElements.image_genre_identifier.formElements.image_genre_classification_scheme.label}
                    mediaType_read_only={this.props.formElements.image_genre.formElements.image_genre_identifier.formElements.image_genre_classification_scheme.read_only}
                    mediaType_required={this.props.formElements.image_genre.formElements.image_genre_identifier.formElements.image_genre_classification_scheme.required}
                    value_label={this.props.formElements.image_genre.formElements.image_genre_identifier.formElements.value.label}
                    value_required={this.props.formElements.image_genre.formElements.image_genre_identifier.formElements.value.required}
                    media_type_identifier_label={this.props.formElements.image_genre.formElements.image_genre_identifier.label}
                    media_type_identifier_help_text={this.props.formElements.image_genre.formElements.image_genre_identifier.help_text}
                    mediaTypeObj={imageGenreObj}
                    media_type_identifierObj={image_genre_identifierObj}
                    media_type_identifier="image_genre_identifier"
                    entityType={"image_genre"}
                    media_type_classification_scheme="image_genre_classification_scheme"
            default_valueArray={sizeItem.image_genre || []} updateModel_MediaType={this.updateModel__array} /></div>*/}



        </>

    }



}