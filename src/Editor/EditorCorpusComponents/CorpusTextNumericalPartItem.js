import React from "react";
//import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import LanguageSpecificTextList from "../editorCommonComponents/LanguageSpecificTextList";
import { TextField } from "@material-ui/core";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import AnnotationArray from "./AnnotationArray";
//import LinkToOtherMediaArray from "./LinkToOtherMediaArray";

export default class CorpusTextNumericalPartItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }

    setNumber = (field, value) => {
        if (value.length === 0) {
            value = null;
        } else {
            value = Number(value);
        }
        const { initialValue } = this.state;
        initialValue[field] = value;
        this.setState({ initialValue });
    }

    updateModel__array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        this.setState({ initialValue }, this.onBlur());

    }

    onBlur = () => {
        const { initialValue } = this.state;
        const exists_annotation = initialValue && initialValue.annotation && initialValue.annotation.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        //const exists_link_to_other_media = initialValue && initialValue.link_to_other_media.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_number_of_participants = initialValue.number_of_participants !== null && initialValue.number_of_participants >= 0 ? true : false;
        const exist_type_of_text_numerical_content = initialValue && initialValue.type_of_text_numerical_content && initialValue.type_of_text_numerical_content.length ? true : false;
        const exist_dialect_accent_of_participants = initialValue && initialValue.dialect_accent_of_participants && initialValue.dialect_accent_of_participants.length ? true : false;
        const exist_geographic_distribution_of_participants = initialValue && initialValue.geographic_distribution_of_participants && initialValue.geographic_distribution_of_participants.length ? true : false;

        if (exists_annotation === false && exist_type_of_text_numerical_content === false && exists_number_of_participants === false && exist_dialect_accent_of_participants === false && exist_geographic_distribution_of_participants === false) {
            initialValue["editor-placeholder"] = true;
        } else {
            delete initialValue["editor-placeholder"];
        }
        this.props.setValues("setPart", this.props.index, initialValue);
    }


    render() {
        const { initialValue } = this.state;
        const { corpus_subclass_default_value } = this.props;
        const showAnnotation = ["http://w3id.org/meta-share/meta-share/annotatedCorpus", "http://w3id.org/meta-share/meta-share/annotationsCorpus"].includes(corpus_subclass_default_value);//needs more work in order to remove annotationArray if corpus_subclass changes value

        return <div onBlur={this.onBlur}>
            <div className="pt-3 pb-3"><LanguageSpecificTextList {...this.props.type_of_text_numerical_content} default_valueArray={initialValue.type_of_text_numerical_content || []} field="type_of_text_numerical_content" setLanguageSpecifictextList={this.updateModel__array} /></div>
            {false && <div className="pt-3 pb-3">
                <AutocompleteChoicesChips
                    className="wd-100"
                    {...this.props.modality_type}
                    choices={this.props.modality_type.child.choices}
                    initialValuesArray={initialValue.modality_type || []}
                    field="modality_type"
                    lr_subclass={true}
                    updateModel_array={this.updateModel__array}
                />
            </div>}
            <div className="pt-3 pb-3">
                <TextField
                    className="wd-100"
                    variant="outlined"
                    type="number"
                    required={this.props.number_of_participants.required}
                    disabled={this.props.number_of_participants.read_only}
                    label={this.props.number_of_participants.label}
                    helperText={this.props.number_of_participants.help_text}
                    value={initialValue.number_of_participants === null ? "" : initialValue.number_of_participants}
                    onChange={(e) => { this.setNumber("number_of_participants", e.target.value) }}
                    inputProps={{ min: `${this.props.number_of_participants.min_value}`, max: `${this.props.number_of_participants.max_value}`, step: "1" }}
                /></div>

            <div className="pt-3 pb-3"><LanguageSpecificTextList {...this.props.dialect_accent_of_participants} default_valueArray={initialValue.dialect_accent_of_participants || []} field="dialect_accent_of_participants" setLanguageSpecifictextList={this.updateModel__array} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificTextList  {...this.props.geographic_distribution_of_participants} default_valueArray={initialValue.geographic_distribution_of_participants || []} field="geographic_distribution_of_participants" setLanguageSpecifictextList={this.updateModel__array} /></div>
            {showAnnotation && <div className="pt-3 pb-3"><AnnotationArray {...GenericSchemaParser.getFormElement("annotation", this.props.annotation)} initialValueArray={initialValue.annotation || []} field="annotation" updateModel_array={this.updateModel__array} /></div>}
            {/*<div className="pt-3 pb-3"><LinkToOtherMediaArray {...GenericSchemaParser.getFormElement("link_to_other_media", this.props.link_to_other_media)} initialValueArray={initialValue.link_to_other_media || []} field="link_to_other_media" updateModel_array={this.updateModel__array} /></div>*/}

        </div>

    }



}