import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import { audio_formatObj } from "../Models/CorpusModel";
import AudioFormatItem from "./AudioFormatItem";
import messages from "./../../config/messages";
//import Tooltip from '@material-ui/core/Tooltip';

export default class AudioFormatArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { audio_formatArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            audio_formatArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, audio_formatItemIndex, value) => {
        const { audio_formatArray } = this.state;
        switch (action) {
            case "addaudio_formatItem":
                if (audio_formatArray.filter(item => item.data_format && item.compressed !== null).length !== audio_formatArray.length) {
                    return;//do not add element if byte order is blank
                }
                audio_formatArray.push(JSON.parse(JSON.stringify(audio_formatObj)));
                break;
            case "removeaudio_formatItem":
                audio_formatArray.splice(audio_formatItemIndex, 1);
                break;
            case "setaudio_formatItem":
                audio_formatArray[audio_formatItemIndex] = value;
                this.props.updateModel_array(this.props.field, audio_formatArray);
                break;
            default: break;
        }
        this.setState({ audio_formatArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.audio_formatArray);
    }

    render() {
        const { audio_formatArray } = this.state;
        //console.log(this.props)
        //audio_formatArray.length === 0 && audio_formatArray.push(JSON.parse(JSON.stringify(audio_formatObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {audio_formatArray.length === 0 && <Tooltip title={`${messages.group_elements_create} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addaudio_formatItem")}>{messages.group_elements_create}</Button></Tooltip>}
                </Grid>
            </Grid>
            {
                audio_formatArray.map((audio_formatItem, audio_formatItemIndex) => {
                    return <div key={audio_formatItemIndex} onBlur={() => this.onBlur()}>
                        {
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeaudio_formatItem", audio_formatItemIndex)}>{messages.array_elements_remove}</Button></Grid>
                            </Grid>
                        }
                        <AudioFormatItem  {...this.props} audio_formatItem={audio_formatItem} audio_formatItemIndex={audio_formatItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            {audio_formatItemIndex === audio_formatArray.length - 1 &&
                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addaudio_formatItem")}>{messages.array_elements_add}</Button></Grid>
                                </Grid>
                            }

                        </div>
                        <div style={{ marginTop: "5em" }} />
                    </div>
                })
            }
        </div>
    }
}