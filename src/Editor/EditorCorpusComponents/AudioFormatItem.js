import React from "react";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import OnOfSwitch from "../editorCommonComponents/OnOfSwitch";
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
import { TextField } from "@material-ui/core";
import AutocompleteRecommendedChoicesSelectOneOption from "../editorCommonComponents/AutocompleteRecommendedChoicesSelectOneOption";

export default class AudioFormatItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { audio_formatItem: props.audio_formatItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            audio_formatItem: nextProps.audio_formatItem
        };
    }

    setString = (field, value, lr_subclass) => {
        const { audio_formatItem } = this.state;
        audio_formatItem[field] = value;
        this.setState({ audio_formatItem }, this.onBlur());
    }

    setObjectGeneral = (field, valueObj) => {
        const { audio_formatItem } = this.state;
        audio_formatItem[field] = valueObj;
        this.setState({ audio_formatItem }, this.onBlur());
    }

    updateModel__array = (obj2update, valueArray) => {
        const { audio_formatItem } = this.state;
        if (!audio_formatItem[obj2update]) {
            audio_formatItem[obj2update] = [];
        }
        audio_formatItem[obj2update] = valueArray;
        this.setState({ audio_formatItem }, this.onBlur());

    }

    updateBoolean = (field, boolean) => {
        const { audio_formatItem } = this.state;
        audio_formatItem[field] = Boolean(boolean);
        this.setState({ audio_formatItem }, this.onBlur());
    }

    setCompressedBoolean = (e) => {
        const { audio_formatItem } = this.state;
        if (audio_formatItem.compressed === null || audio_formatItem.compressed === undefined) {
            audio_formatItem.compressed = null;
        }
        audio_formatItem.compressed = e.target.value === "true";
        this.setState({ audio_formatItem }, this.onBlur());
    }

    setArray = (field, array) => {
        const { audio_formatItem } = this.state;
        if (!audio_formatItem[field]) audio_formatItem[field] = [];
        audio_formatItem[field] = array;
        this.setState({ audio_formatItem }, this.onBlur());
    }

    setSelectListValuebyte_order = (event, selectedObjArray) => {
        const { audio_formatItem } = this.state;
        audio_formatItem.byte_order = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ audio_formatItem }, this.onBlur());
    }

    setSelectListValuesign_convention = (event, selectedObjArray) => {
        const { audio_formatItem } = this.state;
        audio_formatItem.sign_convention = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ audio_formatItem }, this.onBlur());
    }

    setSelectListValueaudio_quality_measure_included = (event, selectedObjArray) => {
        const { audio_formatItem } = this.state;
        audio_formatItem.audio_quality_measure_included = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ audio_formatItem }, this.onBlur());
    }

    setSelectListValuecompression_name = (event, selectedObjArray) => {
        const { audio_formatItem } = this.state;
        audio_formatItem.compression_name = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ audio_formatItem }, this.onBlur());
    }

    onBlur = () => {
        const { audio_formatItem } = this.state;
        const exists_data_format = audio_formatItem.data_format ? true : false;
        const exists_byte_order = audio_formatItem.byte_order ? true : false;
        const exists_sign_convention = audio_formatItem.sign_convention ? true : false;
        const exists_audio_quality_measure_included = audio_formatItem.audio_quality_measure_included ? true : false;
        const exists_compression_name = audio_formatItem.compression_name ? true : false;
        const exists_signal_encoding = audio_formatItem.signal_encoding && audio_formatItem.signal_encoding.length ? true : false;
        const exists_recording_quality = audio_formatItem.recording_quality && audio_formatItem.recording_quality.length ? true : false;
        const exists_compressed = audio_formatItem && audio_formatItem.compressed !== null ? true : false;
        const exists_compression_loss = audio_formatItem && audio_formatItem.compression_loss !== null ? true : false;
        const exists_sampling_rate = audio_formatItem.sampling_rate ? true : false;
        const exists_quantization = audio_formatItem.quantization ? true : false;
        const exists_number_of_tracks = audio_formatItem.number_of_tracks ? true : false;
        if (exists_data_format === false && exists_byte_order === false && exists_sign_convention === false && exists_audio_quality_measure_included === false && exists_compression_name === false && exists_signal_encoding === false &&
            exists_recording_quality === false && exists_compressed === false && exists_compression_loss === false && exists_sampling_rate === false && exists_quantization === false && exists_number_of_tracks === false) {
            audio_formatItem["editor-placeholder"] = true;
        } else {
            delete audio_formatItem["editor-placeholder"];
        }
        this.props.updateModel("setaudio_formatItem", this.props.audio_formatItemIndex, audio_formatItem);
    }


    render() {
        const { audio_formatItem } = this.state;
        return <>
            <div className="pt-3 pb-3">
                <AutocompleteRecommendedChoicesSelectOneOption
                    className="wd-100"
                    {...this.props.formElements.data_format}
                    initialValue={audio_formatItem.data_format || ""}
                    field="data_format"
                    lr_subclass={true}
                    updateModel_String={this.setString}
                />
            </div>
            <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.byte_order} choices={this.props.formElements.byte_order.choices} default_value={audio_formatItem.byte_order} setSelectedvalue={this.setSelectListValuebyte_order} /></div>
            {false && <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...this.props.formElements.sign_convention} choices={this.props.formElements.sign_convention.choices} default_value={audio_formatItem.sign_convention} setSelectedvalue={this.setSelectListValuesign_convention} /></div>}
            {false && <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...this.props.formElements.audio_quality_measure_included} choices={this.props.formElements.audio_quality_measure_included.choices} default_value={audio_formatItem.audio_quality_measure_included} setSelectedvalue={this.setSelectListValueaudio_quality_measure_included} /></div>}
            {false && <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...this.props.formElements.compression_name} choices={this.props.formElements.compression_name.choices} default_value={audio_formatItem.compression_name} setSelectedvalue={this.setSelectListValuecompression_name} /></div>}

            {false && <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.signal_encoding} initialValuesArray={audio_formatItem.signal_encoding || []} field="signal_encoding" updateModel_array={this.updateModel__array} /></div>}
            {false && <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.recording_quality} initialValuesArray={audio_formatItem.recording_quality || []} field="recording_quality" updateModel_array={this.updateModel__array} /></div>}

            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100" {...this.props.formElements.compressed} default_value={audio_formatItem.compressed} handleBooleanChange={this.setCompressedBoolean} /></div>
            {false && <div className="pt-3 pb-3"> <OnOfSwitch {...this.props.formElements.compressed} default_value={audio_formatItem.compressed} field="compressed" updateBoolean={this.updateBoolean} /></div>}
            {false && <div className="pt-3 pb-3"> <OnOfSwitch {...this.props.formElements.compression_loss} default_value={audio_formatItem.compression_loss} field="compression_loss" updateBoolean={this.updateBoolean} /></div>}

            {false && <div className="pt-3 pb-3"> <TextField
                className="wd-100"
                variant="outlined"
                type="number"
                required={this.props.formElements.sampling_rate.required}
                disabled={this.props.formElements.sampling_rate.read_only}
                label={this.props.formElements.sampling_rate.label}
                helperText={this.props.formElements.sampling_rate.help_text}
                value={audio_formatItem.sampling_rate || ""}
                onChange={(e) => this.setString("sampling_rate", e.target.value)}
                inputProps={{ min: `${this.props.formElements.sampling_rate.min_value}`, max: `${this.props.formElements.sampling_rate.max_value}`, step: "1" }}
            /> </div>}

            {false && <div className="pt-3 pb-3"> <TextField
                className="wd-100"
                variant="outlined"
                type="number"
                required={this.props.formElements.quantization.required}
                disabled={this.props.formElements.quantization.read_only}
                label={this.props.formElements.quantization.label}
                helperText={this.props.formElements.quantization.help_text}
                value={audio_formatItem.quantization || ""}
                onChange={(e) => this.setString("quantization", e.target.value)}
                inputProps={{ min: `${this.props.formElements.quantization.min_value}`, max: `${this.props.formElements.quantization.max_value}`, step: "1" }}
            /> </div>}

            {false && <div className="pt-3 pb-3"> <TextField
                className="wd-100"
                variant="outlined"
                type="number"
                required={this.props.formElements.number_of_tracks.required}
                disabled={this.props.formElements.number_of_tracks.read_only}
                label={this.props.formElements.number_of_tracks.label}
                helperText={this.props.formElements.number_of_tracks.help_text}
                value={audio_formatItem.number_of_tracks || ""}
                onChange={(e) => this.setString("number_of_tracks", e.target.value)}
                inputProps={{ min: `${this.props.formElements.number_of_tracks.min_value}`, max: `${this.props.formElements.number_of_tracks.max_value}`, step: "1" }}
            /> </div>}


        </>

    }



}