import React from "react";
import LRAutocomplete from "./LRAutocomplete";
import IsDescribedByAutocomplete from "../EditorServiceToolComponents/IsDescribedByAutocomplete";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import DateComponent from "../editorCommonComponents/DateComponent";
import ActorTypeAutocomplete from "../editorCommonComponents/ActorTypeAutocomplete";

export default class AnnotationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { annotationItem: props.annotationItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            annotationItem: nextProps.annotationItem
        };
    }

    setString = (field, value) => {
        const { annotationItem } = this.state;
        annotationItem[field] = value;
        this.setState({ annotationItem });
    }


    setObjectGeneral = (field, valueObj) => {
        const { annotationItem } = this.state;
        annotationItem[field] = valueObj;
        this.setState({ annotationItem }, this.onBlur());
    }

    updateModel__array = (obj2update, valueArray) => {
        const { annotationItem } = this.state;
        if (!annotationItem[obj2update]) {
            annotationItem[obj2update] = [];
        }
        annotationItem[obj2update] = valueArray;
        this.setState({ annotationItem }, this.onBlur());
    }

    onBlur = () => {
        const { annotationItem } = this.state;
        const exists_annotation_type = annotationItem.annotation_type && annotationItem.annotation_type.length ? true : false;
        const exists_annotated_element = annotationItem.annotated_element && annotationItem.annotated_element.length ? true : false;
        const exists_segmentation_level = annotationItem.segmentation_level && annotationItem.segmentation_level.length ? true : false;
        const exists_guidelines = annotationItem && annotationItem.guidelines.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        //removed at m2.3// const exists_tagset = annotationItem && annotationItem.tagset.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_annotation_standoff = annotationItem && annotationItem.annotation_standoff !== null ? true : false;
        const exists_theoretic_model = annotationItem && annotationItem.theoretic_model && annotationItem.theoretic_model.hasOwnProperty("en") && annotationItem.theoretic_model["en"] ? true : false;
        const exists_annotation_mode = annotationItem.annotation_mode ? true : false;
        const exists_annotation_mode_details = annotationItem && annotationItem.annotation_mode_details && annotationItem.annotation_mode_details.hasOwnProperty("en") && annotationItem.annotation_mode_details["en"] ? true : false;
        const exists_is_annotated_by = annotationItem && annotationItem.is_annotated_by.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_typesystem = annotationItem && annotationItem.typesystem.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_annotation_resource = annotationItem && annotationItem.annotation_resource.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_annotator = (annotationItem.annotator && annotationItem.annotator.filter(item => item.actor_type !== "actor_type" && !item.hasOwnProperty("editor-placeholder")).length) ? true : false;
        const exists_interannotator_agreement = annotationItem && annotationItem.interannotator_agreement && annotationItem.interannotator_agreement.hasOwnProperty("en") && annotationItem.interannotator_agreement["en"] ? true : false;
        const exists_intraannotator_agreement = annotationItem && annotationItem.intraannotator_agreement && annotationItem.intraannotator_agreement.hasOwnProperty("en") && annotationItem.intraannotator_agreement["en"] ? true : false;
        const exists_annotation_report = annotationItem && annotationItem.annotation_report.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_annotation_start_date = annotationItem.annotation_start_date ? true : false;
        const exists_annotation_end_date = annotationItem.annotation_end_date ? true : false;

        if (exists_annotation_type === false && exists_annotated_element === false && exists_segmentation_level === false && exists_guidelines === false && exists_annotation_standoff === false &&
            exists_theoretic_model === false && exists_annotation_mode === false && exists_annotation_mode_details === false && exists_is_annotated_by === false && exists_typesystem === false && exists_annotation_resource === false && exists_annotator === false &&
            exists_interannotator_agreement === false && exists_intraannotator_agreement === false && exists_annotation_report === false && exists_annotation_start_date === false && exists_annotation_end_date === false) {
            annotationItem["editor-placeholder"] = true;
        } else {
            delete annotationItem["editor-placeholder"];
        }
        this.props.updateModel("setannotationItem", this.props.annotationItemIndex, annotationItem);
    }

    setEvautatedBoolean = (e) => {
        const { annotationItem } = this.state;
        if (annotationItem.annotation_standoff === null || annotationItem.annotation_standoff === undefined) {
            annotationItem.annotation_standoff = null;
        }
        annotationItem.annotation_standoff = e.target.value === "true";
        this.setState({ annotationItem });
    }
    setSelectListAnnotationMode = (event, selectedObjArray) => {
        const { annotationItem } = this.state;
        annotationItem.annotation_mode = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ annotationItem });
    }


    render() {
        const { annotationItem } = this.state;
        //console.log(this.props.formElements)
        return <div onBlur={this.onBlur}>

            <div className="pt-3 pb-3"><AutocompleteRecommendedChoices className="wd-100" {...this.props.formElements.annotation_type} recommended_choices={this.props.formElements.annotation_type.choices} initialValuesArray={annotationItem.annotation_type || []} field="annotation_type" updateModel_array={this.updateModel__array} /></div>
            <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.annotated_element} initialValuesArray={annotationItem.annotated_element || []} field="annotated_element" updateModel_array={this.updateModel__array} /></div>
            <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100" {...this.props.formElements.segmentation_level} initialValuesArray={annotationItem.segmentation_level || []} field="segmentation_level" updateModel_array={this.updateModel__array} /></div>
            <div className="pt-3 pb-3"><IsDescribedByAutocomplete className="wd-100" model={this.props.model} {...this.props.formElements.guidelines} initialValueArray={annotationItem.guidelines || []} field="guidelines" updateModel_Array={this.setObjectGeneral} /></div>
            {/*<div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.tagset} initialValueArray={annotationItem.tagset || []} field="tagset" updateModel_Array={this.setObjectGeneral} /> </div>*/}{/*removed at m2.3*/}
            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100" {...this.props.formElements.annotation_standoff} default_value={annotationItem.annotation_standoff} handleBooleanChange={this.setEvautatedBoolean} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.theoretic_model} defaultValueObj={annotationItem.theoretic_model || { "en": "" }} field="theoretic_model" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...this.props.formElements.annotation_mode} choices={this.props.formElements.annotation_mode.choices} default_value={annotationItem.annotation_mode} setSelectedvalue={this.setSelectListAnnotationMode} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.annotation_mode_details} defaultValueObj={annotationItem.annotation_mode_details || { "en": "" }} field="annotation_mode_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.is_annotated_by} initialValueArray={annotationItem.is_annotated_by || []} field="is_annotated_by" updateModel_Array={this.setObjectGeneral} /> </div>
            {<div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.typesystem} initialValueArray={annotationItem.typesystem || []} field="typesystem" updateModel_Array={this.setObjectGeneral} /> </div>}
            {<div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.annotation_resource} initialValueArray={annotationItem.annotation_resource || []} field="annotation_resource" updateModel_Array={this.setObjectGeneral} /> </div>}
            <div className="pt-3 pb-3"><ActorTypeAutocomplete  {...this.props.formElements.annotator} initialValueArray={annotationItem.annotator || []} field="annotator" updateModel_Array={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.interannotator_agreement} defaultValueObj={annotationItem.interannotator_agreement || { "en": "" }} field="interannotator_agreement" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.intraannotator_agreement} defaultValueObj={annotationItem.intraannotator_agreement || { "en": "" }} field="intraannotator_agreement" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><IsDescribedByAutocomplete model={this.props.model} className="wd-100" {...this.props.formElements.annotation_report} initialValueArray={annotationItem.annotation_report || []} field="annotation_report" updateModel_Array={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><DateComponent className="date-label wd-100" label={this.props.formElements.annotation_start_date.label} help_text={this.props.formElements.annotation_start_date.help_text} initialValue={annotationItem.annotation_start_date || null} maxDate={annotationItem.annotation_end_date || null} field="annotation_start_date" updateModel={this.setString} /></div>
            <div className="pt-3 pb-3"><DateComponent className="date-label wd-100" label={this.props.formElements.annotation_end_date.label} help_text={this.props.formElements.annotation_end_date.help_text} initialValue={annotationItem.annotation_end_date || null} minDate={annotationItem.annotation_start_date || null} field="annotation_end_date" updateModel={this.setString} /></div>

        </div>

    }



}