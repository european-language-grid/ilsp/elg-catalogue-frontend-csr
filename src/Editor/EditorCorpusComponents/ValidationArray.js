import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import { validationObj } from "../Models/LrModel";
import ValidationItem from "./ValidationItem";

export default class ValidationArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { validationArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            validationArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, validationItemIndex, value) => {
        const { validationArray } = this.state;
        switch (action) {
            case "addvalidationItem":
                validationArray.push(JSON.parse(JSON.stringify(validationObj)));
                break;
            case "removevalidationItem":
                validationArray.splice(validationItemIndex, 1);
                break;
            case "setvalidationItem":
                validationArray[validationItemIndex] = value;
                this.props.updateModel_array(this.props.field, validationArray);
                break;
            default: break;
        }
        this.setState({ validationArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.validationArray);
    }

    render() {
        const { validationArray } = this.state;
        //console.log(this.props)
        validationArray.length === 0 && validationArray.push(JSON.parse(JSON.stringify(validationObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addvalidationItem")}><AddCircleOutlineIcon className="xsmall-icon"/></Button>      
                </Grid>*/}
            </Grid>
            {
                validationArray.map((validationItem, validationItemIndex) => {
                    return <div key={validationItemIndex} onBlur={() => this.onBlur()}>
                        <ValidationItem  {...this.props} validationItem={validationItem} validationItemIndex={validationItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("removevalidationItem", validationItemIndex)}><RemoveCircleOutlineIcon className="xsmall-icon" /></Button>
                            <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addvalidationItem")}><AddCircleOutlineIcon className="xsmall-icon" /></Button>
                        </div>
                        <div style={{ marginTop: "5em" }} />
                    </div>
                })
            }
        </div>
    }
}