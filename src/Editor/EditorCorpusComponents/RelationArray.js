import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { relationObj } from "../Models/LrModel";
import RelationItem from "./RelationItem";
import messages from "./../../config/messages";

export default class RelationArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { relationArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            relationArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, relationItemIndex, value) => {
        const { relationArray } = this.state;
        switch (action) {
            case "addrelationItem":
                const filteredArray = relationArray.filter(item => {
                    if (item.hasOwnProperty("relation_type")) {
                        if (item && item.relation_type && item.relation_type.hasOwnProperty("en") && !item.relation_type["en"]) {
                            return false;
                        }
                    }
                    if (item.related_lr) {
                        if (item.related_lr.resource_name && item.related_lr.resource_name.hasOwnProperty("en") && item.related_lr.resource_name["en"]) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    return false;
                })
                if (filteredArray.length !== relationArray.length) {
                    return;//do not add element if there are empty ones
                }
                relationArray.push(JSON.parse(JSON.stringify(relationObj)));
                break;
            case "removerelationItem":
                relationArray.splice(relationItemIndex, 1);
                break;
            case "setrelationItem":
                relationArray[relationItemIndex] = value;
                //this.props.updateModel_array(this.props.field, relationArray);
                break;
            default: break;
        }
        this.setState({ relationArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.relationArray);
        //this.props.updateModel_array(this.props.field, this.state.relationArray);
    }

    render() {
        const { relationArray } = this.state;
        //console.log(this.props)
        relationArray.length === 0 && relationArray.push(JSON.parse(JSON.stringify(relationObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addrelationItem")}><AddCircleOutlineIcon className="xsmall-icon"/></Button>      
                </Grid>*/}
            </Grid>
            {
                relationArray.map((relationItem, relationItemIndex) => {
                    return <div key={relationItemIndex} onBlur={() => this.onBlur()}>
                        {((relationItem.related_lr && relationItem.related_lr.hasOwnProperty("resource_name") && relationItem.related_lr.resource_name.hasOwnProperty("en") && relationItem.related_lr.resource_name.en !== "") || (relationItem && relationItem.hasOwnProperty("pk"))) &&
                            <Grid container direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Button className="inner-link-default--purple" onClick={(e) => this.setValues("removerelationItem", relationItemIndex)}>{messages.array_elements_remove}</Button>
                            </Grid>
                        }

                        {(!relationItem.related_lr && relationItemIndex !== 0) &&
                            <Grid container direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Button className="inner-link-default--purple" onClick={(e) => this.setValues("removerelationItem", relationItemIndex)}>{messages.array_elements_remove}</Button></Grid>
                        }

                        <RelationItem  {...this.props} relationItem={relationItem} relationItemIndex={relationItemIndex} updateModel={this.setValues} />

                        {((relationItem.related_lr && relationItem.related_lr.hasOwnProperty("resource_name") && relationItem.related_lr.resource_name.hasOwnProperty("en") && relationItem.related_lr.resource_name.en !== "") && ((relationArray.length - 1) === relationItemIndex)) ?
                            <Grid container direction="row" justifyContent="flex-start" alignItems="baseline" >
                                {/*<Button className="inner-link-default--purple" onClick={(e) => this.setValues("removerelationItem", relationItemIndex)}>{messages.array_elements_remove}</Button>*/}
                                <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addrelationItem")}>{messages.array_elements_add}</Button>
                            </Grid>
                            : <span></span>
                        }



                        {/*<div className="mb2 pt-3">
                            <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("removerelationItem", relationItemIndex)}><RemoveCircleOutlineIcon className="xsmall-icon" /></Button>
                            <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addrelationItem")}><AddCircleOutlineIcon className="xsmall-icon" /></Button>
                        </div>*/}
                        <div style={{ marginTop: "2em" }} />
                    </div>
                })
            }
        </div>
    }
}