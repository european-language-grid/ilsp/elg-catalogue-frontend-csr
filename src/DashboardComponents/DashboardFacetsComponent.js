import React from "react";
import { FormattedMessage } from 'react-intl';
//import {FormattedHTMLMessage} from 'react-intl';
import Link from '@material-ui/core/Link';
import messages from "./../config/messages";
import { Button } from "@material-ui/core";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import { HIDE_ENTITIES_FROM_MY_ITEMS } from "../config/constants";
//import Grid from '@material-ui/core/Grid';
//import TextField from '@material-ui/core/TextField';

class DashboardFacetsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expandedResourceType: false, expandedStatus: false,
            itemsToShowResourceType: 10, itemsToShowStatus: 5,
            itemsToShowCurators: 5, expandedCurators: false,////
            searchKeyword: ''
        };

        this.toggleloadMoreResourceType = this.toggleloadMoreResourceType.bind(this);
        this.toggleloadMoreStatus = this.toggleloadMoreStatus.bind(this);
        this.handleResourceTypeSearch = this.handleResourceTypeSearch.bind(this);
        this.handleStatusSearch = this.handleStatusSearch.bind(this);
        this.clearAllFilters = this.clearAllFilters.bind(this);
        this.handleUnselected = this.handleUnselected.bind(this);
        this.handleUserSubmit = this.handleUserSubmit.bind(this); ///remove after filter curator
    }


    toggleloadMoreResourceType() {
        this.state.itemsToShowResourceType === 3 ? (
            this.setState({ itemsToShowResourceType: this.props.facets._filter_resource_type.resource_type.buckets.length, expandedResourceType: true })
        ) : (
            this.setState({ itemsToShowResourceType: 3, expandedResourceType: false })
        )
    }
    //** */
    toggleloadMoreCurators = () => {
        this.state.itemsToShowCurators === 5 ? (

            this.setState({ itemsToShowCurators: this.props.facets._filter_curator.curator.buckets.length, expandedCurators: true })
        ) : (
            this.setState({ itemsToShowCurators: 5, expandedCurators: false })
        )
    }

    toggleloadMoreStatus() {
        this.state.itemsToShowStatus === 3 ? (
            this.setState({ itemsToShowStatus: this.props.facets._filter_status.status.buckets.length, expandedStatus: true })
        ) : (
            this.setState({ itemsToShowStatus: 3, expandedStatus: false })
        )
    }

    handleUnselected(index, keyword) {
        /*const selectedArray = [...this.state.selectedFacetsNames];
        selectedArray.splice(index, 1);
        this.setState({ selectedFacetsNames: selectedArray });*/
        this.props.handleUnselectedFacet(index);
        this.props.unselectFacet(keyword);
    }

    ////888888888888888
    handleCurator = (event, value, selectedFacets) => {
        const index = selectedFacets.indexOf(value);
        const keyword = "curator__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleResourceTypeSearch(event, value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        const keyword = "resource_type__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleUnpublicationRequested(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("unpublication_requested__term" + value);
        const keyword = "unpublication_requested__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("unpublication_requested__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleStatusSearch(value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        let keyword = "status__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handlelegally_valid(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("legally_valid__term" + value);
        const keyword = "legally_valid__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("legally_valid__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handletechnically_valid(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("technically_valid__term" + value);
        const keyword = "technically_valid__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("technically_valid__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handlemetadata_valid(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("metadata_valid__term" + value);
        const keyword = "metadata_valid__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("metadata_valid__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleValidatorAssignmentRequired(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("validator_assignment_required__term" + value);
        const keyword = "validator_assignment_required__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("validator_assignment_required__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handlefunctional_service(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("functional_service__term" + value);
        const keyword = "functional_service__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("functional_service__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleHasData(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("has_data__term" + value);
        const keyword = "has_data__term=" + value;
        if (index === -1) {
            this.props.markFacetAsSelected("has_data__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }
    ///remove after filter curator
    handleUserSubmit(event, selectedFacets) {
        // selectedFacets will contain the constant string "username" instead of the actual
        // search username, in order to indicate that this facet is selected
        event.preventDefault();
        const index = selectedFacets.indexOf("username");
        //const urlUsername = this.state.username.replace("+", "%2B")//we handle encoding during construction 
        const urlUsername = this.state.username;
        const keyword = "username=" + urlUsername;
        if (!urlUsername.trim()) {
            this.handleUnselected(index, keyword);
            return;
        }
        if (index === -1) {
            this.props.markFacetAsSelected("username");
            this.props.onSelectKeyword(keyword);
        }
        else {
            //this.props.handleUsernameChange(keyword);
            this.handleUnselected(index, keyword);
        }
    }
    handleservice_status(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("service_status__term" + value);
        const keyword = "service_status__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("service_status__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleLicence(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("licence__term" + value);
        const keyword = "licence__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("licence__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleresubmitted(event, value, selectedFacets) {
        const index = selectedFacets.indexOf("resubmitted__term" + value);
        const keyword = "resubmitted__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected("resubmitted__term" + value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    ///remove after filter curator
    /*handleUserChange(event, selectedFacets) {
        if (!event.target.value.trim()) {
            this.setState({ username: event.target.value.trim() }, () => { this.handleUserSubmit(event, selectedFacets) });
        } else {
            this.setState({ username: event.target.value });
        }
    }*/


    /*markFacetAsSelected(facetName) {
        if (this.state.selectedFacetsNames.indexOf(facetName) === -1) {//checks if i have allready selected the facet, if not add it to array
            this.setState(prevState => ({ selectedFacetsNames: [...prevState.selectedFacetsNames, facetName] }));
            //this.state.selectedFacetsNames.forEach(facetName => document.getElementById(facetName).classList.add('FacetActive'));
        }
        //document.getElementById(facetName) && document.getElementById(facetName).classList.add('FacetActive');//comment out for release. need to find solution 
    }*/

    clearAllFilters() {
        this.setState({
            expandedResourceType: false, expandedStatus: false,
            itemsToShowResourceType: 10, itemsToShowStatus: 5,
            itemsToShowCurators: 5, expandedCurators: false,////
            searchKeyword: ''
        });
        this.props.onClearAllSearchFilters();
    }


    render() {
        const { facets } = this.props;
        const { itemsToShowResourceType, itemsToShowStatus } = this.state;
        //console.log(facets)
        const selectedFacetsUriEncoded = this.props.selectedFacets || [];
        const selectedFacets = [];
        //let userMessage;
        selectedFacetsUriEncoded.forEach(element => {
            selectedFacets.push(decodeURIComponent(element));
        });
        //if (facets && facets._filter_resource_type && facets._filter_resource_type.resource_type && facets._filter_resource_type.resource_type.buckets) {
        //facets._filter_resource_type.resource_type.buckets = JSON.parse(JSON.stringify(facets._filter_resource_type.resource_type.buckets.filter(item => !HIDE_ENTITIES_FROM_MY_ITEMS.includes(item.key))));
        //}
        return (
            <div style={{ marginTop: "1em" }}>
                {selectedFacets.length > 0 && <div style={{ marginBottom: "1em" }}> <Button classes={{ root: 'inner-link-outlined--teal' }} endIcon={<HighlightOffIcon />} onClick={() => this.clearAllFilters()}>
                    <FormattedMessage id="facetsComponent.clearAllFilters" defaultMessage={messages.dashoard_items_clear_filters}>
                    </FormattedMessage>
                </Button> </div>}

                {/* 
                    <div>
                        <h5 className="bold padding5">{userMessage} </h5>
                        <form onSubmit={(event) => this.handleUserSubmit(event, selectedFacets)}>
                            <Grid container spacing={1} style={{ paddingBottom: "20px" }}>
                                <Grid item xs={8}>
                                    <TextField
                                        type="search"
                                        label="curator username"
                                        variant="outlined"
                                        onChange={(event) => this.handleUserChange(event, selectedFacets)}
                                        size="small"
                                        value={this.state.username}
                                     
                                    />
                                </Grid>
                                <Grid item xs={2}>
                                    <Button onClick={(event) => this.handleUserSubmit(event, selectedFacets)} className="inner-link-default--purple">search</Button>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
               */ }


                {/*alternative*/}
                {facets._filter_curator && facets._filter_curator.curator.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.curator" defaultMessage={messages.dashboard_curator_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_curator.curator.buckets.slice(0, this.state.itemsToShowCurators).map((curator, index) =>
                                <li className="FacetButton" button="true" key={curator.key} onClick={(e) => this.handleCurator(e, curator.key, selectedFacets)} >
                                    {selectedFacets.indexOf(curator.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {curator.key} </span>
                                    <span className="count" > ({curator.doc_count}) </span>

                                </li>)
                            }
                            {facets._filter_curator.curator.buckets.length > 5 &&
                                <div className="ShowMore">
                                    <Link component="button" onClick={this.toggleloadMoreCurators} >
                                        {this.state.expandedCurators ? (<span>
                                            <FormattedMessage id="facetsComponent.showLess" defaultMessage={messages.show_less} >
                                            </FormattedMessage>
                                        </span>) : (<span>
                                            <FormattedMessage id="facetsComponent.showMore" defaultMessage={messages.show_more} >
                                            </FormattedMessage>
                                        </span>)}
                                    </Link>
                                </div>
                            }
                        </ul>
                    </div>
                }
                {/*alternative*/}

                {facets._filter_resource_type && facets._filter_resource_type.resource_type.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.items" defaultMessage={messages.dashboard_items_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_resource_type.resource_type.buckets.slice(0, itemsToShowResourceType).map((resource_type, index) =>
                                <li className={HIDE_ENTITIES_FROM_MY_ITEMS.includes(resource_type.key) ? "" : "FacetButton"} button="true" key={resource_type.key} onClick={(e) => HIDE_ENTITIES_FROM_MY_ITEMS.includes(resource_type.key) ? void 0 : this.handleResourceTypeSearch(e, resource_type.key, selectedFacets)} >
                                    {selectedFacets.indexOf(resource_type.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {resource_type.key} </span>
                                    <span className="count" > ({resource_type.doc_count}) </span>

                                </li>)
                            }
                            {facets._filter_resource_type.resource_type.buckets.length > 10 &&
                                <div className="ShowMore">
                                    <Link component="button" onClick={this.toggleloadMoreResourceType} >
                                        {this.state.expandedResourceType ? (<span>
                                            <FormattedMessage id="facetsComponent.showLess" defaultMessage={messages.show_less} >
                                            </FormattedMessage>
                                        </span>) : (<span>
                                            <FormattedMessage id="facetsComponent.showMore" defaultMessage={messages.show_more} >
                                            </FormattedMessage>
                                        </span>)}
                                    </Link>
                                </div>
                            }
                        </ul>
                    </div>
                }


                {facets._filter_status && facets._filter_status.status.buckets.length > 0 && !(facets._filter_status.status.buckets.length === 1 && facets._filter_status.status.buckets[0].key === "LanguageResource") &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.status" defaultMessage={messages.dashboard_status_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_status.status.buckets.slice(0, itemsToShowStatus).map((status, index) =>
                                status.key !== "LanguageResource" && <li className="FacetButton" button="true" key={status.key} onClick={() => this.handleStatusSearch(status.key, selectedFacets)} >
                                    {selectedFacets.indexOf(status.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    {/*<span className="FacetText" > {status.key}</span>*/}
                                    {status.key === "published" && <span className="FacetText" > {messages.published_tag}</span>}
                                    {status.key === "internal" && <span className="FacetText" > {messages.internal_tag}</span>}
                                    {status.key === "ingested" && <span className="FacetText" > {messages.ingested_tag}</span>}
                                    {status.key !== "published" && status.key !== "internal" && status.key !== "ingested" && <span className="FacetText" > {status.key}</span>}
                                    <span className="count" > ({status.doc_count}) </span>
                                </li>)
                            }
                            {facets._filter_status.status.buckets.length > 5 &&
                                <div className="ShowMore">
                                    <Link component="button" onClick={this.toggleloadMoreStatus} >
                                        {this.state.expandedStatus ? (<span>
                                            <FormattedMessage id="facetsComponent.showLess" defaultMessage={messages.show_less} >
                                            </FormattedMessage>
                                        </span>) : (<span>
                                            <FormattedMessage id="facetsComponent.showMore" defaultMessage={messages.show_more} >
                                            </FormattedMessage>
                                        </span>)}
                                    </Link>
                                </div>
                            }
                        </ul>
                    </div>
                }

                {facets._filter_functional_service && facets._filter_functional_service.functional_service.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.functional_service" defaultMessage={messages.dashboard_functional_service_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_functional_service.functional_service.buckets.map((functional_service, index) =>
                                <li className="FacetButton" button="true" key={functional_service.key_as_string} onClick={(e) => this.handlefunctional_service(e, functional_service.key_as_string, selectedFacets)} >
                                    {selectedFacets.indexOf(`functional_service__term${functional_service.key_as_string}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {functional_service.key_as_string === "false" ? "no" : "yes"} </span>
                                    <span className="count" > ({functional_service.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }

                {facets._filter_service_status && facets._filter_service_status.service_status.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.service_status" defaultMessage={messages.dashboard_service_status_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_service_status.service_status.buckets.map((service_status, index) =>
                                <li className="FacetButton" button="true" key={service_status.key} onClick={(e) => this.handleservice_status(e, service_status.key, selectedFacets)} >
                                    {selectedFacets.indexOf(`service_status__term${service_status.key}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {service_status.key} </span>
                                    <span className="count" > ({service_status.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }

                {facets._filter_has_data && facets._filter_has_data.has_data.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.hasdata" defaultMessage={messages.dashboard_hasdata_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_has_data.has_data.buckets.map((has_data, index) =>
                                <li className="FacetButton" button="true" key={has_data.key_as_string} onClick={(e) => this.handleHasData(e, has_data.key_as_string, selectedFacets)} >
                                    {selectedFacets.indexOf(`has_data__term${has_data.key_as_string}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {has_data.key_as_string === "false" ? "no" : "yes"} </span>
                                    <span className="count" > ({has_data.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }

                {facets._filter_validator_assignment_required && facets._filter_validator_assignment_required.validator_assignment_required.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.validation" defaultMessage={messages.dashboard_validation_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_validator_assignment_required.validator_assignment_required.buckets.map((validator_assignment_required, index) =>
                                <li className="FacetButton" button="true" key={validator_assignment_required.key_as_string} onClick={(e) => this.handleValidatorAssignmentRequired(e, validator_assignment_required.key_as_string, selectedFacets)} >
                                    {selectedFacets.indexOf(`validator_assignment_required__term${validator_assignment_required.key_as_string}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {validator_assignment_required.key_as_string === "false" ? "no" : "yes"} </span>
                                    <span className="count" > ({validator_assignment_required.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }
                {facets._filter_resubmitted && facets._filter_resubmitted.resubmitted.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.resubmitted" defaultMessage={messages.dashboard_resubmitted_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_resubmitted.resubmitted.buckets.map((resubmitted, index) =>
                                <li className="FacetButton" button="true" key={resubmitted.key_as_string} onClick={(e) => this.handleresubmitted(e, resubmitted.key_as_string, selectedFacets)} >
                                    {selectedFacets.indexOf(`resubmitted__term${resubmitted.key_as_string}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {resubmitted.key_as_string} </span>
                                    <span className="count" > ({resubmitted.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }

                {facets._filter_technically_valid && facets._filter_technically_valid.technically_valid.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.technically_valid" defaultMessage={messages.dashboard_technically_valid_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_technically_valid.technically_valid.buckets.map((technically_valid, index) =>
                                <li className="FacetButton" button="true" key={technically_valid.key} onClick={(e) => this.handletechnically_valid(e, technically_valid.key, selectedFacets)} >
                                    {selectedFacets.indexOf(`technically_valid__term${technically_valid.key}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {technically_valid.key} </span>
                                    <span className="count" > ({technically_valid.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }

                {facets._filter_metadata_valid && facets._filter_metadata_valid.metadata_valid.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.metadata_valid" defaultMessage={messages.dashboard_metadata_valid_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_metadata_valid.metadata_valid.buckets.map((metadata_valid, index) =>
                                <li className="FacetButton" button="true" key={metadata_valid.key} onClick={(e) => this.handlemetadata_valid(e, metadata_valid.key, selectedFacets)} >
                                    {selectedFacets.indexOf(`metadata_valid__term${metadata_valid.key}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {metadata_valid.key} </span>
                                    <span className="count" > ({metadata_valid.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }
                {facets._filter_legally_valid && facets._filter_legally_valid.legally_valid.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myitemsfacetsComponent.legally_valid" defaultMessage={messages.dashboard_legally_valid_facet} >
                        </FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_legally_valid.legally_valid.buckets.map((legally_valid, index) =>
                                <li className="FacetButton" button="true" key={legally_valid.key} onClick={(e) => this.handlelegally_valid(e, legally_valid.key, selectedFacets)} >
                                    {selectedFacets.indexOf(`legally_valid__term${legally_valid.key}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {legally_valid.key} </span>
                                    <span className="count" > ({legally_valid.doc_count}) </span>

                                </li>)
                            }

                        </ul>
                    </div>
                }


                {facets._filter_licence && facets._filter_licence.licence.buckets.length > 0 &&
                    <div>
                        <h5 className="bold ml-05"><FormattedMessage id="myDownloads.licence" defaultMessage="Licence" ></FormattedMessage></h5>
                        <ul className="facets-list">
                            {facets._filter_licence.licence.buckets.map((licence, index) =>
                                <li className="FacetButton" button="true" key={licence.key} onClick={(e) => this.handleLicence(e, licence.key, selectedFacets)} >
                                    {selectedFacets.indexOf(`licence__term${licence.key}`) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                    <span className="FacetText" > {licence.key} </span>
                                    <span className="count" > ({licence.doc_count}) </span>
                                </li>)
                            }

                        </ul>
                    </div>
                }

            </div>
        )
    }
}


export default DashboardFacetsComponent;