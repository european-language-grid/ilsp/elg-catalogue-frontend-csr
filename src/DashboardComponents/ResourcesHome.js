import React from "react";
import { withRouter } from "react-router-dom";
//import {useLocation} from "react-router-dom"
//import { Redirect, withRouter } from "react-router-dom";
//import { TabContent, TabPane, } from "reactstrap";
//import NavigationTabs from "./NavigationTabs";
import messages from "./../config/messages";
import DashboardAppBar from "./DashboardAppBar"
import SearchMyResources from "./myResources/SearchMyResources";
import SearchMyValidations from "./myValidations/SearchMyValidations";
import SearchMyUsage from "./myUsage/SearchMyUsage";
import SearchMyDownloads from "./myDownloads/SearchMyDownloads";
//import SearchMySupervisorTasks from "./mySupervisorTasks/SearchMySupervisorTasks";
import SimpleTabPanel from "../componentsAPI/CustomVerticalTabs/SimpleTabPanel";
import Container from '@material-ui/core/Container';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { IS_LEGAL_VALIDATOR, IS_METADATA_VALIDATOR, IS_TECHNICAL_VALIDATOR, IS_ADMIN, IS_CONTENT_MANAGER, AUTHENTICATED_KEYCLOAK_USER_ROLES, SHOW_MY_ITEMS_ROLES, SHOW_MY_VALIDATION_ROLES/*, SHOW_CONSUMER_GRID_ROLES */ } from "../config/constants"; //IS_CONTENT_MANAGER,

function a11yProps(index) {
	return {
		id: `full-width-tab-${index}`,
		'aria-controls': `full-width-tabpanel-${index}`,
	};
}


class ResourcesHome extends React.Component {

	constructor(props) {
		super(props);
		const tabIndexes = this.getTabIndexes();
		const initialTab = this.props.location.state ? tabIndexes[this.props.location.state.tab] : 0;
		this.state = {
			tabIndexes: tabIndexes,
			tab: initialTab,
		}
	}

	componentDidMount() {
		const pathName = this.props.location.pathname;
		const tab_from_state = this.props.location && this.props.location.state && this.props.location.state.tab;
		if (tab_from_state) {
			this.toggleTab(tab_from_state);
		} else if (pathName) {
			switch (pathName) {
				case "/myValidations":
					this.toggleTab('myval');
					break;
				case "/myItems":
					this.toggleTab('myres');
					break;
				case "/myUsage":
					this.toggleTab('myuse');
					break;
				case "/myDownloads":
					this.toggleTab('mydown');
					break;
				default:
					break;
			}
		}
	}

	toggleTab = (tabLabel) => {
		if (tabLabel === "myres") {
			this.props.history.push("/myItems");
		} else if (tabLabel === "myval") {
			this.props.history.push("/myValidations");
		} else if (tabLabel === "myuse") {
			this.props.history.push("/myUsage");
		} else if (tabLabel === "mydown") {
			this.props.history.push("/myDownloads");
		}

		this.setState({ tab: this.state.tabIndexes[tabLabel] });
	}

	getTabIndexes = () => {
		const showMyRes = SHOW_MY_ITEMS_ROLES.filter(item => AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak).includes(item)).length > 0;
		const showMyVal = SHOW_MY_VALIDATION_ROLES.filter(item => AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak).includes(item)).length > 0;
		const showMyUse = this.props.keycloak && this.props.keycloak.authenticated;//SHOW_CONSUMER_GRID_ROLES.filter(item => AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak).includes(item)).length > 0;
		const showMyDown = this.props.keycloak && this.props.keycloak.authenticated;//SHOW_CONSUMER_GRID_ROLES.filter(item => AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak).includes(item)).length > 0;
		const tabsToCreate = [];
		showMyRes && tabsToCreate.push("myres");
		showMyVal && tabsToCreate.push("myval");
		showMyUse && tabsToCreate.push("myuse");
		showMyDown && tabsToCreate.push("mydown");
		var tabIndexes = {};
		tabsToCreate.forEach((item, index) => tabIndexes[item] = index);
		/*var tabIndexes = {
			'myres': 0,
			'myval': 1,
		};*/
		return tabIndexes;
	}

	render() {
		let tabTitles = { 'myres': messages.dashboard_my_items, 'myval': messages.dashboard_my_validations, 'myuse': messages.dashboard_my_usage, 'mydown': messages.dashboard_my_downloads, }
		//the following are use to control the indexing of tabs, because MaterialUI returns error in the case that a user is not validator, but he is supervisor , the second tab is not visible and the indexing of the tabs is 0,1 (not 0,2) 

		return (
			<>
				<DashboardAppBar {...this.props} />
				<Container maxWidth="xl" className="my-resouces-table dashboard-app-bar-mg" >
					<div>
						<Tabs value={this.state.tab} onChange={this.toggleTab} aria-label="simple tabs example" className="simple-tabs-forms">
							{(AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak).includes("provider") || IS_ADMIN(this.props.keycloak) || IS_CONTENT_MANAGER(this.props.keycloak)) && <Tab label={tabTitles['myres']} {...a11yProps(0)} onClick={() => { this.toggleTab('myres'); }} />}
							{(IS_LEGAL_VALIDATOR(this.props.keycloak) || IS_METADATA_VALIDATOR(this.props.keycloak) || IS_TECHNICAL_VALIDATOR(this.props.keycloak) || IS_ADMIN(this.props.keycloak)) && <Tab label={tabTitles['myval']} {...a11yProps(1)} onClick={() => { this.toggleTab('myval'); }} />}
							{this.props.keycloak && this.props.keycloak.authenticated && <Tab label={tabTitles['myuse']} {...a11yProps(0)} onClick={() => { this.toggleTab('myuse'); }} />}
							{this.props.keycloak && this.props.keycloak.authenticated && <Tab label={tabTitles['mydown']} {...a11yProps(0)} onClick={() => { this.toggleTab('mydown'); }} />}
						</Tabs>

						{(AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak).includes("provider") || IS_ADMIN(this.props.keycloak) || IS_CONTENT_MANAGER(this.props.keycloak)) &&
							<SimpleTabPanel value={this.state.tab} index={this.state.tabIndexes['myres']} className="myitems--outer">
								<SearchMyResources {...this.props} keycloak={this.props.keycloak} />
							</SimpleTabPanel>}

						{this.props.keycloak && this.props.keycloak.authenticated &&
							<SimpleTabPanel value={this.state.tab} index={this.state.tabIndexes['myuse']} className="myitems--outer">
								<SearchMyUsage {...this.props} keycloak={this.props.keycloak} />
							</SimpleTabPanel>}

						{this.props.keycloak && this.props.keycloak.authenticated &&
							<SimpleTabPanel value={this.state.tab} index={this.state.tabIndexes['mydown']} className="myitems--outer">
								<SearchMyDownloads {...this.props} keycloak={this.props.keycloak} />
							</SimpleTabPanel>}

						{(IS_LEGAL_VALIDATOR(this.props.keycloak) || IS_METADATA_VALIDATOR(this.props.keycloak) || IS_TECHNICAL_VALIDATOR(this.props.keycloak) || IS_ADMIN(this.props.keycloak) || IS_CONTENT_MANAGER(this.props.keycloak)) &&
							<SimpleTabPanel value={this.state.tab} index={this.state.tabIndexes['myval']} className="myitems--outer">
								<SearchMyValidations {...this.props} keycloak={this.props.keycloak} />
							</SimpleTabPanel>}

					</div>
				</Container>
			</>
		)
	}

}

export default withRouter(ResourcesHome);