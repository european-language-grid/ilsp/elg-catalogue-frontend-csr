import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Grid from '@material-ui/core/Grid';
//import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
//import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import GetAppIcon from '@material-ui/icons/GetApp';
import Typography from "@material-ui/core/Typography";
//
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
//import CircularProgress from '@material-ui/core/CircularProgress';
//import Tooltip from "@material-ui/core/Tooltip";
import DashboardAppBar from "../DashboardAppBar";
import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";
import Papa from "papaparse";
import { getAuthorizationHeader, getUrlToLandingPageFromCatalogue, TOOL_SERVICE } from "../../config/constants";
import { SERVICE_REGISTRATION_BATCH } from "../../config/editorConstants";
import { keycloak } from "../../App";

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);
//const filterOut = ['OpenAPI Spec URL', 'YAML File', '']
export default class BatchServiceRegistrationTable extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = { data: null, loading: true, upload_tsv_error: null }
    }

    componentDidMount() {
        this.fetchRequiredData();
    }

    fetchRequiredData() {
        this.setState({ loading: true });
        Papa.parse(SERVICE_REGISTRATION_BATCH, {
            download: true,
            downloadRequestHeaders: {
                'Authorization': getAuthorizationHeader(keycloak)
            },
            header: true,
            skipEmptyLines: true,
            error: this.errorData,
            complete: this.updateData,
        });
    }

    errorData = (err, file) => {
        toast.error("An error occured, while fetching the required data");
        this.setState({ loading: false });
        console.log(err);
    }

    updateData = (result) => {
        const data = result.data;
        this.setState({ data: data, loading: false });
    }

    uploadTSV = (response) => {
        try {
            response = JSON.parse(response);
            //console.log(response);
            this.setState({ upload_tsv_error: null });
            toast.success("Registration file submitted successfully");
            this.fetchRequiredData();
            if (response && response.failed_ids && response.failed_ids.length > 0) {
                let services = (response && response.services && response.services.length > 0 && response.services) || "";
                if (services) {
                    services = JSON.stringify(services, null, 2);
                    this.setState({ upload_tsv_error: services });
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    uploadTSVError = (response) => {
        try {
            response = JSON.parse(response);
            console.log(response);
            this.setState({ upload_tsv_error: JSON.stringify(response, null, 2) });
            toast.error("Upload failed");
            this.fetchRequiredData();
        } catch (error) {
            console.log(error);
        }
    }

    uploadTSVComponent = () => {
        return <FilePond
            credits={false}
            allowMultiple={false}
            name="file"
            //onprocessfileabort={() => {}}
            labelIdle={'<span class="filepond--label-action"> Upload a tsv file for service reristration </span>'}
            server={
                {
                    url: SERVICE_REGISTRATION_BATCH,
                    process: {
                        method: 'PUT',
                        headers: {
                            'Authorization': getAuthorizationHeader(keycloak),
                            'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
                            'FUNCTIONAL-SERVICE': this.state.functional_service === true ? true : false,
                            "XML-UPLOAD-WITH-DATA": this.state.xml_upload_with_data === true ? true : false
                        },
                        onload: (response) => this.uploadTSV(response),
                        onerror: (response) => this.uploadTSVError(response),
                    },
                    remove: null,
                    revert: null,
                    fetch: null,
                    restore: null,
                    load: null
                }
            }
            //onremovefile={(file) => { this.setState({ xmlUploadError: null, successResponse: null, loading: false }); }}
            onprocessfilestart={(file) => { this.setState({ upload_tsv_error: null }) }}
        />
    }

    downloadCSV = () => {
        axios.get(SERVICE_REGISTRATION_BATCH, { responseType: 'blob', timeout: 30000, })
            .then(res => {
                if (res && res.data) {
                    const url = window.URL.createObjectURL(new Blob([res.data]));
                    var [, filename] = res.headers['content-disposition'].split('filename=');
                    if (!filename) {
                        try {
                            const content_disp = res.headers['content-disposition'];
                            if (content_disp.indexOf("b?") > 0 && content_disp.indexOf("?=") >= 0) {
                                var s = content_disp.indexOf("b?");
                                var e = content_disp.lastIndexOf("?=");
                                const payload = content_disp.substring(s + 2, e);
                                const decoded = Buffer.from(payload, "base64").toString("utf-8");
                                filename = decoded.substring(decoded.indexOf("filename=") + "filename=".length);
                            } else {
                                filename = "undefined";
                                filename = filename + ".tsv";
                            }
                        } catch (err) {
                            filename = "undefined";
                            filename = filename + ".tsv";
                        }
                    }
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', filename);
                    document.body.appendChild(link);
                    link.click();
                    link.remove();
                }
            })
            .catch(err => { });
    }

    ShowErrorMessage = () => {
        return <div>
            {this.state.upload_tsv_error && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
                <h3>Error</h3>
                <div className="boxed">
                    <Paper elevation={13} >
                        <code>
                            <pre id="special">
                                {this.state.upload_tsv_error}
                            </pre>
                        </code>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    construct_example_row = (keys = []) => {
        const example_row = {
        };
        for (let index = 0; index < keys.length; index++) {
            const key = keys[index];
            if (key === "Service ID") {
                example_row[key] = 1234;
            } else if (key === "Resource ID") {
                example_row[key] = 4321;
            } else if (key === "Tool Type") {
                example_row[key] = "http://w3id.org/meta-share/omtd-share/MachineTranslation";
            } else if (key === "ELG Execution Location") {
                example_row[key] = "http://service-srv-mt-example.elg-srv-dev.svc.cluster.local/translate";
            } else if (key === "ELG GUI URL") {
                example_row[key] = "/dev/gui-ie/index-mt.html?srcdir=ltr&targetdir=ltr";
            } else if (key === "Status") {
                example_row[key] = "COMPLETED";
            } else if (key === "ELG Hosted") {
                example_row[key] = "True";
            } else if (key === "Accessor ID") {
                example_row[key] = "mt-example";
            } else if (key === "Docker Download Location (Read-only)") {
                example_row[key] = "registry.gitlab.com/provider/mt-example:v1.0.0";
            } else if (key === "Service Adapter Download Location (Read-only)") {
                example_row[key] = "";
            } else if (key === "Execution Location (Read-only)") {
                example_row[key] = "http://localhost:8080/translate";
            } else if (key === "Private Resource (Read-only)") {
                example_row[key] = "False";
            } else if (key === "Record Version (Read-only)") {
                example_row[key] = "1.0.0";
            } else {
                example_row[key] = "";
            }
        }
        return example_row;
    }

    render() {
        if (this.state.loading) {
            return <ProgressBar />
        }
        const data = this.state.data;
        if (!data || data.length === 0) {
            return <>
                <Grid container direction="row" alignItems="flex-start" spacing={2}>
                    <Grid item xs>
                        <DashboardAppBar {...this.props} />
                    </Grid>
                    <Grid item xs={12}>
                        <div style={{ marginTop: "5%" }}>
                            <Typography variant="h3" align="center">There are no pending services to be validated</Typography>
                        </div>
                    </Grid>
                </Grid>
            </>
        }
        const keys = Object.keys(data[0]);
        const resource_id_index = keys.indexOf("Resource ID");

        data.unshift(this.construct_example_row(keys));

        return <>
            <Grid container direction="row" alignItems="flex-start" spacing={2}>
                <Grid item xs>
                    <DashboardAppBar {...this.props} />
                </Grid>
                <Grid item xs={12}>
                    <div style={{ marginTop: "5%" }}>
                        {this.uploadTSVComponent()}
                    </div>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" align="center">Your pending service registrations <span>
                        <IconButton onClick={this.downloadCSV}>
                            <GetAppIcon />
                        </IconButton>
                    </span></Typography>
                </Grid>
                <Grid item xs={12}>
                    <TableContainer component={Paper} style={{ marginBottom: "5%" }}>
                        <Table aria-label="Batch service registration table" size="small">
                            <TableHead>
                                <TableRow>
                                    {keys.map((item, index) => <TableCell key={index}>{item}</TableCell>)}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.map((row, row_index) => (
                                    row_index === 0 ? <TableRow key={row_index}>
                                        {keys.map((item, index) => <TableCell key={index} style={{ backgroundColor: "lightgrey", fontStyle: "italic" }}> {row[item]}</TableCell>)}
                                    </TableRow> :
                                        <TableRow key={row_index}>
                                            {keys.map((item, index) => <TableCell key={index}>{(resource_id_index >= 0 && resource_id_index === index) ? <Link to={getUrlToLandingPageFromCatalogue({ id: row[item], resource_type: TOOL_SERVICE })}>{row[item]}</Link> : row[item]}</TableCell>)}
                                        </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                        <Typography variant="caption">*The first grey row of the table is an example row</Typography>
                    </TableContainer>
                </Grid>
                {this.state.upload_tsv_error && <Grid item xs={12}>
                    <div style={{ marginLeft: "2%" }}>
                        {this.ShowErrorMessage()}
                    </div>
                </Grid>}
            </Grid>
        </>
    }
}