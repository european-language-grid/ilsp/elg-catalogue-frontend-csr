import React from "react";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { ReactComponent as IngestIcon } from "./../../assets/elg-icons/editor/cursor-move-target-up.svg";
import messages from "../../config/messages";
import { DEACTIVATE_VERSION } from "../../config/editorConstants";
import { toast } from "react-toastify";
import { getImage } from "./commonFunctions";
import ResourceStatus from "../ResourceStatus";
import { keycloak } from "../../App";


export default class DeactivateVersionBatch extends React.Component {
    constructor(props) {
        super(props);
        this.state = { displayDialog: true, loading: false, deactivate_version_items: this.props.deactivate_version_items || [] };
    }

    handleMultipleDeactivateVersion = () => {
        this.setState({ loading: true });
        const Ids = this.state.deactivate_version_items.map(item => item.id).join(",");
        axios.patch(DEACTIVATE_VERSION(Ids)).then((res) => {
            this.success("Records submited successfully.", res);
        }).catch((err) => {
            this.failure("Records submission failed.", err);
        });
        //setTimeout(() => { this.success("Records submited successfully.", []); }, 6000);
    }

    success = (msg, res) => {
        this.setState({ loading: false });
        toast.success(msg, { autoClose: 3500 });
        this.disableDisplay();
        try {
            if (res && res.data && res.data.length > 0) {
                this.props.updateRecords(res.data);
                //window.location.reload();
            }
        } catch (err) {
        }

    }

    failure = (msg, err = "") => {
        toast.error(msg, { autoClose: 3500 });
        this.setState({ loading: false });
        this.disableDisplay();
        try {
            if (err && err.response && err.response.data) {
                console.log(err);
            } else {
                console.log(err);
            }
        } catch (catcherr) {
            console.log(err);
        }
    }

    disableDisplay = () => {
        this.setState({ displayDialog: false });
        this.props.clearSelect("");
    }


    render() {
        if (this.state.deactivate_version_items.length === 0) {
            return <></>;
        }

        const otherStatusItems = this.props.checkedList?.filter(item => !this.state.deactivate_version_items.includes(item)) || []

        return <div>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg'>
                {!this.state.loading && <DialogTitle> You are about to deactivate the version of the following records.</DialogTitle>}
                {this.state.loading && <DialogTitle> Please wait while we are submitting your request. <CircularProgress size={20} /></DialogTitle>}
                <DialogContent>
                    <DialogContentText component={"div"}>
                        {this.state.deactivate_version_items.map((item, index) => {
                            const image = getImage(item);
                            return <div style={{ marginBottom: "5px" }} key={JSON.stringify(item)}>
                                <Grid item xs={12} container alignItems="center">
                                    <Grid item xs={6} >
                                        <Typography variant="h5">
                                            <span style={{ marginRight: "10px" }}>{image}</span>
                                            <span>{item.resource_name} <small> <b>version {item.version}</b></small></span>
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <ResourceStatus status={item.status} xSize={12} keycloak={keycloak} />
                                    </Grid>
                                </Grid>
                            </div>
                        })}
                    </DialogContentText>
                    {otherStatusItems.length > 0 && <DialogContentText component={"div"}>
                        <div>
                            <hr />
                            <Typography variant="h5" style={{ marginBottom: "10px" }}>
                                {messages.editor_bulk_general_message}
                            </Typography>
                            {otherStatusItems.map((excludeItem, excludeItemIndex) => {
                                const image = getImage(excludeItem);
                                return <div style={{ marginBottom: "5px" }} key={JSON.stringify(excludeItem)}>
                                    <Grid item xs={12} container alignItems="center">
                                        <Grid item xs={6} >
                                            <span>
                                                <Typography variant="h5">
                                                    <span style={{ marginRight: "10px" }}>{image}</span>
                                                    <span>{excludeItem.resource_name} <small> <b>version {excludeItem.version}</b></small> </span>
                                                </Typography>
                                            </span>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <ResourceStatus status={excludeItem.status} xSize={12} keycloak={keycloak} />
                                        </Grid>
                                    </Grid>
                                </div>
                            })}
                        </div>
                    </DialogContentText>
                    }
                </DialogContent>
                <hr />
                <DialogActions>
                    <Button
                        color="primary"
                        disabled={this.state.loading}
                        onClick={this.disableDisplay}
                        autoFocus
                    >
                        {messages.dialog_validationResponse_close_button}
                    </Button>
                    <Button
                        color="primary"
                        disabled={this.state.loading}
                        onClick={() => this.handleMultipleDeactivateVersion()}
                    >
                        <IngestIcon className="xsmall-icon" />
                        {messages.editor_action_ingest_secondary}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    }
}