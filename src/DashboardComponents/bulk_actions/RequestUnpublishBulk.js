import React from "react";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { ReactComponent as IngestIcon } from "./../../assets/elg-icons/editor/cursor-move-target-up.svg";
import messages from "../../config/messages";
import { EDITOR_BULK_UNPUBLISH } from "../../config/editorConstants";
import { toast } from "react-toastify";
import { getImage } from "./commonFunctions";
import ResourceStatus from "../ResourceStatus";
import { keycloak } from "../../App";


export default class RequestUnpublishBulk extends React.Component {
    constructor(props) {
        super(props);
        this.state = { displayDialog: true, loading: false, published_items: this.props.published_items, reason: "" };
    }

    handleMultipleUnpublish = () => {
        this.setState({ loading: true });
        const Ids = this.state.published_items.map(item => item.id).join(",");
        axios.patch(EDITOR_BULK_UNPUBLISH(Ids), { reason: this.state.reason }).then((res) => {
            this.success("Your request to unpublish the selected records was submitted successfully.", res);
        }).catch((err) => {
            this.failure("Your request to unpublish the selected records was failed.", err);
        });
        //setTimeout(() => { this.success("Records submited successfully.", []); }, 6000);
    }

    success = (msg, res) => {
        this.setState({ loading: false });
        toast.success(msg, { autoClose: 3500 });
        this.disableDisplay();
        try {
            if (res && res.data && res.data.length > 0) {
                this.props.updateRecords(res.data);
                //window.location.reload();
            }
        } catch (err) {
        }

    }

    failure = (msg, err = "") => {
        toast.error(msg, { autoClose: 3500 });
        this.setState({ loading: false });
        this.disableDisplay();
        try {
            if (err && err.response && err.response.data) {
                console.log(err);
            } else {
                console.log(err);
            }
        } catch (catcherr) {
            console.log(err);
        }
    }

    disableDisplay = () => {
        this.setState({ displayDialog: false });
        this.props.clearSelect("");
    }

    render() {
        if (this.state.published_items.length === 0) {
            return <></>;
        }
        const otherStatusItems = this.props.checkedList?.filter(item => !this.state.published_items.includes(item)) || []
        return <div>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg'>
                {!this.state.loading && <DialogTitle> You are about to send a {messages.editor_action_unpublish_primary} the following records.</DialogTitle>}
                {this.state.loading && <DialogTitle> Please wait while your request submission is in progress. <CircularProgress size={20} /></DialogTitle>}
                <DialogContent>
                    <DialogContentText component={"div"}>
                        {this.state.published_items.map((item, index) => {
                            const image = getImage(item);
                            return <div style={{ marginBottom: "5px" }} key={JSON.stringify(item)}>
                                <Grid item xs={12} container alignItems="center">
                                    <Grid item xs={10} >
                                        <Typography variant="h5">
                                            <span style={{ marginRight: "10px" }}>{image}</span>
                                            <span>{item.resource_name}</span>
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <ResourceStatus status={item.status} xSize={12} keycloak={keycloak} />
                                    </Grid>
                                </Grid>
                            </div>
                        })}
                        <Grid item xs={12} container alignItems="center">
                            <Grid item xs={12}>
                                <TextField className="pb-3 wd-100"
                                    //type="text"
                                    minRows={10}
                                    multiline={true}
                                    required={true}
                                    label={"Unpublication request reason"}
                                    placeholder={"Unpublication request reason"}
                                    variant="outlined"
                                    helperText={
                                        <span>Please specify the reason for your unpublication request.</span>
                                    }
                                    value={this.state.reason}
                                    onChange={(e) => { this.setState({ reason: e.target.value }) }}
                                />
                            </Grid>
                        </Grid>
                    </DialogContentText>
                    {otherStatusItems.length > 0 && <DialogContentText component={"div"}>
                        <div>
                            <hr />
                            <Typography variant="h5" style={{ marginBottom: "10px" }}>
                                {messages.editor_bulk_ingest}
                            </Typography>
                            {otherStatusItems.map((excludeItem, excludeItemIndex) => {
                                const image = getImage(excludeItem);
                                return <div style={{ marginBottom: "5px" }} key={JSON.stringify(excludeItem)}>
                                    <Grid item xs={12} container alignItems="center">
                                        <Grid item xs={10} >
                                            <span>
                                                <Typography variant="h5">
                                                    <span style={{ marginRight: "10px" }}>{image}</span>
                                                    <span>{excludeItem.resource_name}</span>
                                                </Typography>
                                            </span>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <ResourceStatus status={excludeItem.status} unpublication_requested={excludeItem.unpublication_requested} xSize={12} keycloak={keycloak} />
                                        </Grid>
                                    </Grid>
                                </div>
                            })}
                        </div>
                    </DialogContentText>
                    }
                </DialogContent>
                <hr />
                <DialogActions>
                    <Button
                        color="primary"
                        disabled={this.state.loading}
                        onClick={this.disableDisplay}
                        autoFocus
                    >
                        {messages.dialog_validationResponse_close_button}
                    </Button>
                    <Button
                        color="primary"
                        disabled={this.state.loading || !this.state.reason}
                        onClick={() => this.handleMultipleUnpublish()}
                    >
                        <IngestIcon className="xsmall-icon" />
                        {messages.editor_action_unpublish_secondary}
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    }
}