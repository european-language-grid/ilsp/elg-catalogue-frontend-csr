import React from "react";
import ToolServiceImage from "../../assets/images/toolservice.svg";
import CorpusImage from "../../assets/images/resource.svg";
import ResourceImage from "../../assets/elg-icons/archive-folder.svg";
import BlurLinearIcon from "../../assets/images/blur_linear_yellow.svg";
import OrganizationImage from "../../assets/elg-icons/buildings-modern.svg";
import ProjectImage from "../../assets/elg-icons/human-resources-team-settings.svg";
import { TOOL_SERVICE, CORPUS, LCR, LD, PROJECT, ORGANIZATION, ML_MODEL, GRAMMAR, Uncategorized_Language_Description, OTHER } from "../../config/constants";

const getImage = (item, imageWidth = "16px", imageHeight = "22px") => {
    switch (item.resource_type) {
        case TOOL_SERVICE:
            return <img src={ToolServiceImage} alt={item.resource_type} />;
        case "Corpus" || CORPUS:
            return <img src={CorpusImage} alt={item.resource_type} />;
        case "Organization" || ORGANIZATION:
            return <img width={imageWidth} height={imageHeight} src={OrganizationImage} alt={item.resource_type} />;
        case "Project" || PROJECT:
            return <img width={imageWidth} height={imageHeight} src={ProjectImage} alt={item.resource_type} />;
        case LCR:
            return <img width={imageWidth} height={imageHeight} src={ResourceImage} alt={item.resource_type} />;
        case LD:
            return <img width={imageWidth} height={imageHeight} src={BlurLinearIcon} alt={item.resource_type} />;
        case ML_MODEL:
            return <img width={imageWidth} height={imageHeight} src={BlurLinearIcon} alt={item.resource_type} />;
        case GRAMMAR:
            return <img width={imageWidth} height={imageHeight} src={BlurLinearIcon} alt={item.resource_type} />;
        case Uncategorized_Language_Description:
            return <img width={imageWidth} height={imageHeight} src={BlurLinearIcon} alt={item.resource_type} />;
        case OTHER:
            return <img width={imageWidth} height={imageHeight} src={BlurLinearIcon} alt={item.resource_type} />;
        default:
            return false;
    }
}

export { getImage }