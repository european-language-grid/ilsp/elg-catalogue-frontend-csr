
import React from "react";
import { withRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
//import messages from "../../../config/messages";
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import { ReactComponent as HelpIcon } from "./../assets/elg-icons/editor/question-help-square.svg";
import GoToCatalogue from "./../componentsAPI/CommonComponents/GoToCatalogue";
import { AUTHENTICATED_KEYCLOAK_USER_ROLES/*, SHOW_DASHBOARD_ROLES*/ } from "./../config/constants";
import AdminHeader from "../componentsAPI/Header/AdminHeader";
//import CreateResourceHeader from "../componentsAPI/Header/CreateResourceHeader";
import MyItemsHeader from "../componentsAPI/Header/MyItemsHeader";
import MyGridHeader from "../componentsAPI/Header/MyGridHeader";
//import MyConsumerHeader from "../componentsAPI/Header/MyConsumerHeader";
import MyValidationsHeader from "../componentsAPI/Header/MyValidationsHeader";
import { keycloak } from "../App";
import Hidden from '@material-ui/core/Hidden';
import ReorderIcon from '@material-ui/icons/Reorder';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';

class DashboardAppBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: 0, headerData: '', keycloak: keycloak, UserRoles: [] };
  }

  componentDidMount() {
    const { keycloak } = this.state;
    this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
  }

  isAuthorizedToView = (roles) => {
    var isAuthorized = false;
    /*SHOW_DASHBOARD_ROLES.forEach(item => {
      if (roles.includes(item)) {
        isAuthorized = true;
      }
    })*/
    isAuthorized = this.state.keycloak && this.state.keycloak.authenticated
    return isAuthorized;
  }

  render() {
    if (!this.state.UserRoles.length === 0) {
      return <div></div>
    }

    const isAuth = this.isAuthorizedToView(this.state.UserRoles);
    if (!isAuth) {
      return <div></div>
    }

    /*const consumerRoutes = [
      { url: '/myUsage', human_name: 'My usage' },
      { url: '/myDownloads', human_name: 'My downloads' }
    ];*/

    return (<>
      <Helmet>
        <title>European Language Grid Dashboard</title>
      </Helmet>

      <Hidden only={['lg', 'md', 'sm', 'xl']}>
        <Accordion>
          <AccordionSummary expandIcon={<ReorderIcon />} aria-controls="panel1a-content" id="panel1a-header" >
            <Typography> Options </Typography>
          </AccordionSummary>
          <AccordionDetails>

            <AppBar position="static" className="dashboard_app_bar">
              <Toolbar>
                <Container maxWidth="xl" className="editor--app--bar">
                  <Grid container>
                    {/*<Grid item xs={12}><CreateResourceHeader {...this.state} {...this.props} show_icon={true} /> </Grid>*/}
                    <Grid item xs={12}><MyGridHeader {...this.state} {...this.props} /></Grid>
                    <Grid item xs={12}><MyItemsHeader {...this.state} {...this.props} /></Grid>
                    <Grid item xs={12}><MyValidationsHeader {...this.state} {...this.props} /></Grid>
                    <Grid item xs={12}><Button className="app--bar--default" value={this.state.value} onClick={(event, newValue) => { this.props.history.push("/feedback") }} startIcon={<HelpIcon className="xsmall-icon" />}><Typography variant="h6" className="pl-1">Feedback</Typography> </Button></Grid>
                    <Grid item xs={12}><AdminHeader {...this.state} {...this.props} show_icon={true} /></Grid>
                    <Grid item xs={12}><GoToCatalogue /> </Grid>
                  </Grid>
                </Container>
              </Toolbar>
            </AppBar>

          </AccordionDetails>
        </Accordion>
      </Hidden>

      <Hidden only='xs'>
        <AppBar position="static" className="dashboard_app_bar">
          <Toolbar>
            <Container maxWidth="xl" className="editor--app--bar">
              <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                <Grid item container xs={11} md={10}>
                  {/*<CreateResourceHeader {...this.state} {...this.props} show_icon={true} />*/}
                  <Grid item xs={12} md={2}>
                    <MyGridHeader {...this.state} {...this.props} />
                  </Grid>
                  <Grid item xs={12} md={2}>
                    <MyItemsHeader {...this.state} {...this.props} />
                  </Grid>
                  <Grid item xs={12} md={2}>
                    <MyValidationsHeader {...this.state} {...this.props} />
                  </Grid>
                  <Grid item xs={12} md={2}>
                    <Button className="app--bar--default" value={this.state.value} onClick={(event, newValue) => { this.props.history.push("/feedback") }} startIcon={<HelpIcon className="xsmall-icon" />}><Typography variant="h6" className="pl-1">Feedback</Typography> </Button>
                  </Grid>
                  <Grid item xs={12} md={2}>
                    <AdminHeader {...this.state} {...this.props} show_icon={true} />
                  </Grid>
                </Grid>
                <Grid item container xs={12} justifyContent="flex-end" md={2}>
                  <Grid item xs={12}><GoToCatalogue /></Grid>
                </Grid>
              </Grid>
            </Container>
          </Toolbar>
        </AppBar>
      </Hidden>




    </>
    );
  }
}

export default withRouter(DashboardAppBar);
