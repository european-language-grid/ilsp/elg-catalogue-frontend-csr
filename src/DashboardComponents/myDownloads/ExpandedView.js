import React from 'react';
//import { withRouter, Link } from "react-router-dom";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import GetAppIcon from '@material-ui/icons/GetApp';
//import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
//import ExpandLessIcon from '@material-ui/icons/ExpandLess';
//import CountUp from 'react-countup';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
//import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { DOWNLOAD_RESOURCE_ENDPOINT, SERVER_API_CONSUMER_DOWNLOADS_EXPAND } from "../../config/constants";
//import CircularProgress from '@material-ui/core/CircularProgress';
import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";

//const columns = [{ key: "downloaded_at", display_value: "Downloaded at" }, { key: "source_of_download", display_value: "Source" }, { key: "user_ip", display_value: "Ip" }, { key: "is_latest_download", display_value: "latest" }, { key: "licenses", display_value: "Licences" }, { key: "download", display_value: "Download" }];
const columns = [{ key: "downloaded_at", display_value: "Downloaded" }, { key: "mapped_source_of_download", display_value: "Source" }, { key: "licenses", display_value: "Licences" }, { key: "download", display_value: "Download" }];

class ExpandedView extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = { data: null, source: null, loading: null, showDialogue: true, s3URL: null };
    }

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.get(SERVER_API_CONSUMER_DOWNLOADS_EXPAND(this.props.resource.item_id), null, { cancelToken: source.token }).then((response) => {
            this.setState({ data: response.data, source: null, loading: false });
        }).catch((errorResponse) => {
            console.log("error while fetching expanded data");
            this.setState({ source: null, loading: false, data: null });
        });
    }

    handleAccept(record_id, distribution_id, fileName) {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.post(DOWNLOAD_RESOURCE_ENDPOINT(record_id), null, {
            headers: {
                'Cache-Control': 'no-store, max-age=0',
                'Pragma': 'no-cache',
                'Expires': 'Wed, 21 Oct 2015 07:28:00 GMT',
                'filename': fileName,
                "elg-resource-distribution-id": distribution_id,
                'ACCEPT-LICENCE': true
            },
            cancelToken: source.token
        }).then((response) => {
            this.setState({ s3URL: response.data["s3-url"], source: null, loading: false });
        }).catch((errorResponse) => {
            console.log("error while downloading");
            this.setState({ source: null, loading: false, "s3-url": null });
        });
    }

    disableDisplay = () => {
        this.setState({ showDialogue: false });
        this.props.handleExpandeView(false);
    }

    formatDate = (string2Format) => {
        return new Intl.DateTimeFormat("en-GB", {
            year: "numeric",
            month: "long",
            day: "2-digit"
        }).format(new Date(string2Format))
    }

    tableRender = () => {
        return <React.Fragment>
            <Grid container direction="row" alignItems="flex-start" spacing={2}>
                <Grid item xs={12}>
                    <TableContainer component={Paper} style={{ marginBottom: "5%" }}>
                        <Table aria-label="Batch service registration table" size="small">
                            <TableHead>
                                <TableRow>
                                    {columns.map((item, index) => <TableCell key={index}>{item.display_value}</TableCell>)}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    this.state.data.sort((item1, item2) => {
                                        if (item1 && item2) {
                                            /*if (item1.is_latest_download) {
                                                return -1;
                                            } else if (item2.is_latest_download) {
                                                return 1;
                                            }
                                            return 0;
                                            */
                                            return new Date(item2.downloaded_at) - new Date(item1.downloaded_at);
                                        }
                                        return 0;
                                    }).map((resource, resourceIndex) => {
                                        const has_distribution_id = resource.downloaded_distribution_id;
                                        const metadata_record = resource.metadata_record;
                                        //const file_name = this.props.resource.file_name;
                                        const file_name = resource.file_name && resource.file_name.substring(resource.file_name.lastIndexOf("/") + 1);
                                        const enableDownload = has_distribution_id && file_name && !this.state.loading;
                                        return <TableRow key={resourceIndex + JSON.stringify(resource)}>
                                            {columns.map((item, index) => {
                                                if (item.key === "downloaded_at") {
                                                    return <TableCell key={resourceIndex + index + 1}>{this.formatDate(resource[item.key])}</TableCell>
                                                } else if (item.key === "is_latest_download") {
                                                    return <TableCell key={resourceIndex + index + 1}>{resource[item.key] ? "Yes" : "No"}</TableCell>
                                                } else if (item.key === "licenses") {
                                                    const mapedArray = resource[item.key].map((licence, licenceIndex) => {
                                                        return <div key={licence + licenceIndex + resourceIndex + resource.id}>{licence}</div>
                                                    })
                                                    return <TableCell key={resourceIndex + index + 1}>{mapedArray}</TableCell>
                                                } else if (item.key === "download") {
                                                    return <TableCell key={resourceIndex + index + 1} >
                                                        <span>
                                                            <IconButton disabled={!enableDownload} onClick={() => { this.handleAccept(metadata_record, has_distribution_id, resource.file_name) }}>
                                                                <GetAppIcon fontSize='small' />
                                                                <Typography variant="caption" className="grey--font">{file_name}</Typography>
                                                            </IconButton>
                                                        </span>
                                                    </TableCell>
                                                }
                                                return <TableCell key={resourceIndex + index + 1}>{resource[item.key]}</TableCell>
                                            })}
                                        </TableRow>
                                    }
                                    )
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </React.Fragment >
    }

    render() {
        const hasData = this.state.data;
        return <>
            <Dialog open={this.state.showDialogue} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth='xl' fullWidth
                style={{insetBlockStart: "130px"}}
            >
                <DialogTitle align="center">{this.props.resource.resource_name}</DialogTitle>
                {hasData ? <DialogContent>
                    <DialogContentText component={"div"}>
                        {this.tableRender()}
                    </DialogContentText>
                </DialogContent> : <>
                    {this.state.loading ? <ProgressBar /> : "No available information"}
                </>
                }
                <DialogActions>
                    <Button
                        color="primary"
                        onClick={this.disableDisplay}
                        autoFocus
                    >
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
            {(this.state.s3URL) && <div style={{ display: 'none' }}>
                <iframe title="Download Reource" src={this.state.s3URL} />
            </div>}
        </>
    }
}

export default ExpandedView;