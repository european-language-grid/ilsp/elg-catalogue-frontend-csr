import React from "react";
import { withRouter } from "react-router-dom";
//import Grid from '@material-ui/core/Grid';
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
//import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
//import RadioGroup from '@material-ui/core/RadioGroup';
//import Radio from '@material-ui/core/Radio';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
//import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import TextField from '@material-ui/core/TextField';
//import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import { EDITOR_REGISTER_SERVICE } from "../../config/editorConstants";
import { TOOL_SERVICE } from "../../config/constants";
import { TOOL_TYPE_OPTIONS, STATUS_OPTIONS, RESOURCE_ACCESS } from "../../config/editorConstants";
//import messages from "../../config/messages";
import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";

import Website from "../../Editor/editorCommonComponents/Website";
import RecordRadioBoolean from "../../Editor/editorCommonComponents/RecordRadioBoolean";
//import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from "@material-ui/core/TextField";
//import * as yup from "yup";
import { isValidURL } from "../../Editor/validators/commonValidatorSchemas";

class ServiceRegistration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayDialog: true, loading: false, source: null, error: null,
            tool_type: null, status: null, elg_execution_location: null, elg_gui_url: null, accessor_id: null,
            metadata_record: null, openAPI_spec_url: null, yaml_file: null, elg_hosted: null, private_resource: null,
            requires_login: true,
            validationFormErrors: null
        };
    }

    componentDidMount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        if (this.props.type === 'service_registration') {
            axios.get(EDITOR_REGISTER_SERVICE(this.props.resource.id))
                .then(res => { this.setState(res.data); this.setState({ loading: false, source: null, initialStatus: res.data.status }); })
                .catch(err => { this.setState({ loading: false, source: null }); console.log(err) })
        }
    }

    componentWillUnmount = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    updateDate = (field, newDate) => {
        this.setState({ date: newDate });
    }

    disableDisplay = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        this.setState({
            displayDialog: false, loading: false, source: null, error: null,
            tool_type: null, status: null, elg_execution_location: null, elg_gui_url: null, accessor_id: null,
            metadata_record: null, openAPI_spec_url: null, yaml_file: null, elg_hosted: null, private_resource: null,
            requires_login: true,
            validationFormErrors: null
        });
        this.props.hideServiceRegistration();
    }

    validateFields = (fields) => {
        const errors = [];
        if (!isValidURL(fields["elg_execution_location"])) {
            errors.push("Elg execution location is not a valid url");
        }
        /*if (!isValidURL(fields["elg_gui_url"])) {
            errors.push("Elg gui url is not a valid url");
        }*/
        /*if (!fields["elg_execution_location"]) {
            errors.push("Elg execution location cannot be blank");
        }*/
        if ((fields["elg_hosted"] === null || fields["elg_hosted"] === undefined) && fields["status"] === "COMPLETED") {
            errors.push("Elg hosted cannot be empty");
        }
        if (!fields["elg_gui_url"]) {
            errors.push("Elg gui url cannot be blank");
        }
        if (!fields["tool_type"]) {
            errors.push("Tool type cannot be empty");
        }
        if (!fields["accessor_id"] && fields["tool_type"] !== RESOURCE_ACCESS) {
            errors.push("Accessor id cannot be empty");
        }
        if (!fields["status"]) {
            errors.push("Status cannot be empty");
        }
        return errors;
    }

    construct_payload_data = () => {
        return {
            accessor_id: this.state.accessor_id,
            elg_execution_location: this.state.elg_execution_location,
            elg_gui_url: this.state.elg_gui_url,
            tool_type: TOOL_TYPE_OPTIONS.filter(item => item.humanReadable === this.state.tool_type)[0].value,
            status: STATUS_OPTIONS.filter(item => item.value === this.state.status)[0].value,
            elg_hosted: this.state.elg_hosted,
            requires_login: this.state.requires_login
        }
    }

    handleSubmit = () => {
        const errors = this.validateFields(this.state);
        if (errors.length > 0) {
            this.setState({ validationFormErrors: errors }, this.focusError);
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true, validationFormErrors: null });
        if (this.props.type === 'service_registration') {
            axios.patch(EDITOR_REGISTER_SERVICE(this.props.resource.id), this.construct_payload_data())
                .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
        }
    }

    successResponse = (response) => {
        toast.success("Success", { autoClose: 3500 });
        this.disableDisplay();
        //this.setState({ displayDialog: false })
        //this.props.hideServiceRegistration(this.props.action);
        //this.props.history.push("/myValidations");
        // Reload landing page (needs testing)
        //const current = this.props.location.pathname;
        //this.props.history.replace(current);
    }

    errorResponse = (responseError) => {
        toast.error("Your request failed.", { autoClose: 3500 });
        let errData = '';
        try {
            errData = responseError.response.data;
        } catch (err) {
            errData = 'The request has failed. Please contact the ELG team.'
        }
        this.setState({ error: errData }, this.focusError);//render json as error report
    }

    setValue = (field, value) => {
        this.setState({ [field]: value });
    }

    showVlidationFormErrors = () => {
        return <div>
            {this.state.validationFormErrors && <div style={{ marginTop: "100px", paddingBottom: "100px" }}>
                <h3>Errors</h3>
                <div className="boxed">
                    <Paper elevation={13} >
                        {this.state.validationFormErrors.map((item, index) => <li key={index}>
                            <span style={{ color: "red" }}>
                                {item}
                            </span>
                        </li>)}
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    get_tool_type_form = () => {
        return <FormControl className="wd-100 mb2">
            <InputLabel htmlFor="tool-type-selection">{"Tool Type"}</InputLabel>
            <Select
                id="tool-type-selection"
                name="tool-type-selection"
                native
                value={this.state.tool_type ? this.state.tool_type : ""}
                onChange={(e) => { this.setValue("tool_type", e.target.value) }}
                inputProps={{ name: 'tool-type-selection' }}
            >
                <option aria-label="None" value="" />
                {TOOL_TYPE_OPTIONS.map((item, index) => <option key={index} value={item.humanReadable}>{item.humanReadable}</option>)}
            </Select>
            {/*<FormHelperText>Tool type</FormHelperText>*/}
        </FormControl>
    }

    get_status_form = () => {
        return <FormControl
            className="wd-100 mb2">
            <InputLabel htmlFor="select-native-simple">{"Status"}</InputLabel>
            <Select
                variant="outlined"
                native
                disabled={this.state.initialStatus === "COMPLETED"}
                value={this.state.status ? this.state.status : ""}
                onChange={(e) => { this.setValue("status", e.target.value) }}
                inputProps={{ name: 'value' }}
            >
                <option aria-label="None" value="" />
                {STATUS_OPTIONS.map((item, index) => <option key={index} value={item.value}>{item.humanReadable}</option>)}
            </Select>
            {/*<FormHelperText>Some important helper text</FormHelperText>*/}
        </FormControl>
    }

    get_elg_execution_location_form = () => {
        return <Website
            className="wd-100"
            variant="outlined"
            label="ELG execution location"
            //helperText={"ELG location of execution of this LT Service"}
            help_text={"ELG location of execution of this LT Service"}
            placeholder={"http://localhost:8080"}
            required={true}
            //value={this.state.elg_execution_location || ""}
            website={this.state.elg_execution_location || ""}
            field="elg_execution_location"
            updateModel_website={this.setValue}
        //onChange={(e) => { this.setState({ elg_execution_location: e.target.value }) }}
        />
    }

    get_elg_gui_url_form = () => {
        return <TextField
            id="elg-gui-url-id"
            name="elg-gui-url-id"
            className="wd-100 mb2"
            type="text"
            variant="outlined"
            label="ELG gui url"
            helperText={"URL GUI of the service"}
            //placeholder={"/dev/gui-ie/"}
            required={true}
            value={this.state.elg_gui_url || ""}
            onChange={(e) => { this.setValue("elg_gui_url", e.target.value) }}
        />
    }

    get_accessor_id_form = () => {
        return <TextField
            className="wd-100 mb2"
            id="Accessor-id"
            name="Accessor-id"
            type="text"
            variant="outlined"
            //type="number"
            label="Accessor id"
            help_text={"A unique accessor id for REST service calls"}
            required={this.state.tool_type !== RESOURCE_ACCESS}
            value={this.state.accessor_id}
            onChange={(e) => { this.setValue("accessor_id", e.target.value) }
            }
        />
    }

    get_non_editable_urls = () => {
        const { execution_location, docker_download_location, service_adapter_download_location } = this.state;
        return <>
            {execution_location && <TextField
                className="wd-100 mb2"
                label="execution location"
                variant="outlined"
                disabled={true}
                value={execution_location || ""}
            />}
            {docker_download_location && <TextField
                className="wd-100 mb2"
                label="docker download location"
                variant="outlined"
                disabled={true}
                value={docker_download_location || ""}
            />}
            {service_adapter_download_location && <TextField
                className="wd-100 mb2"
                label="service adapter location"
                variant="outlined"
                disabled={true}
                value={service_adapter_download_location || ""}
            />}
        </>
    }

    get_elg_hosted_form = () => {
        return <>
            <RecordRadioBoolean
                className="wd-100 mb2"
                label={"elg hosted"}
                //help_text={"elg hosted service"}
                required={true}
                default_value={this.state.elg_hosted}
                handleBooleanChange={(e) => { this.setState({ elg_hosted: e.target.value === "true" }) }}
            />
        </>
    }

    requires_login = () => {
        return <>
            <RecordRadioBoolean
                className="wd-100 mb2"
                label={"Requires login"}
                help_text={"In order to test this service users need to be logged in"}
                required={true}
                default_value={this.state.requires_login}
                handleBooleanChange={(e) => { this.setState({ requires_login: e.target.value === "true" }) }}
            />
        </>
    }

    get_elg_private_resource_boolean_form = () => {
        return <>
            <RecordRadioBoolean
                className="wd-100 mb2"
                label={"private resource"}
                disabled={true}
                //help_text={"elg hosted service"}
                required={false}
                default_value={this.state.private_resource}
                handleBooleanChange={(e) => { }}
            />
        </>
    }

    ShowErrorMessage(error) {
        return <div>
            {error && <div >
                <h3>Error</h3>
                <div className="boxed">
                    <Paper elevation={13} >
                        <code>
                            <pre id="special">
                                {JSON.stringify(error)}
                            </pre>
                        </code>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    getLRDialogue = () => {
        return <>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg' style={{ insetBlockStart: "130px" }}>

                <DialogContent>
                    <form onSubmit={(e) => { e.preventDefault(); this.handleSubmit() }}>
                        <Typography className="pb-3" align="center" color="primary" variant="body1"> Please fill in the following fields in order to validate  <small className="teal--font">{this.props.resource.resource_name}</small></Typography>

                        <div>
                            {this.get_tool_type_form()}
                        </div>
                        <div>
                            {this.get_elg_execution_location_form()}
                        </div>
                        <div>
                            {this.get_elg_gui_url_form()}
                        </div>
                        <div>
                            {this.get_elg_hosted_form()}
                        </div>
                        <div className="pb-3">
                            {this.requires_login()}
                        </div>
                        <div>
                            {this.get_accessor_id_form()}
                        </div>
                        <div>
                            {this.get_non_editable_urls()}
                        </div>
                        <div>
                            {this.get_elg_private_resource_boolean_form()}
                        </div>
                        <div>
                            {this.get_status_form()}
                        </div>


                        <div id='erf'>
                            {this.state.validationFormErrors && this.state.validationFormErrors.length > 0 && this.showVlidationFormErrors()}
                        </div>

                        <DialogContentText component={"div"}>
                            {this.state.error && this.ShowErrorMessage(this.state.error)}
                        </DialogContentText>
                    </form>
                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        disabled={this.state.loading}
                        onClick={() => this.handleSubmit()}
                    >
                        Submit
                    </Button>
                    <span>
                        <Button
                            color="primary"
                            disabled={this.state.loading}
                            onClick={() => this.disableDisplay()}
                        >
                            Cancel
                        </Button>
                    </span>


                </DialogActions>
            </Dialog>
        </>
    }

    focusError = () => {
        try {
            const errorArea = document.getElementById("erf");
            if (errorArea && (this.state.validationFormErrors || this.state.error)) {
                errorArea.scrollIntoView({
                    behavior: 'auto',
                    block: 'center',
                    inline: 'center'
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        if (this.state.loading) {
            return <ProgressBar />
        }
        const { resource_type } = this.props.resource;
        if ((resource_type === TOOL_SERVICE || resource_type === "ToolService")) {
            return this.getLRDialogue();
        } else {
            return <></>
        }
    }
}

export default withRouter(ServiceRegistration)