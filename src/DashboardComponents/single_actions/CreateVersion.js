import React from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
//import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
//import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import DateComponent from "../../Editor/editorCommonComponents/DateComponent";
//import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import { TOOL_SERVICE, CORPUS, LD, LCR, PROJECT, ORGANIZATION, ML_MODEL, GRAMMAR, OTHER, Uncategorized_Language_Description } from "../../config/constants";
import { DASHBOARD_CREATE_NEW_VERSION } from "../../config/editorConstants";
//import { keycloak } from "./../../App";
//import messages from "../../config/messages";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import messages from "../../config/messages";

class CreateVersion extends React.Component {
    constructor(props) {
        super(props);
        this.state = { displayDialog: true, loading: false, source: null, error: null, version: "", date: "", resource_name: "", functional_service: false };
    }

    componentWillUnmount = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    updateDate = (field, newDate) => {
        this.setState({ date: newDate });
    }

    disableDisplay = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        this.setState({ displayDialog: true, loading: false, source: null, error: null, version: "", date: "", resource_name: "", functional_service: null });
        this.props.hideCreateVersion(this.props.action);
    }

    handleSubmit = (payload) => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.patch(DASHBOARD_CREATE_NEW_VERSION(this.props.resource.id), payload, { cancelToken: source.token })
            .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
            .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
    }

    successResponse = (response) => {
        toast.success("Success", { autoClose: 3500 });
        this.redirectToLandingPage(response);
        //this.disableDisplay();
    }

    errorResponse = (responseError) => {
        //"Please specify a new version for this resource by following this template: v1.0.0"
        toast.error("Your request failed.", { autoClose: 3500 });
        let errData = '';
        try {
            const isVersionError = this.versionError(responseError.response.data);
            if (isVersionError && isVersionError.length > 0) {
                errData = isVersionError[0]
            } else {
                errData = responseError.response.data;
            }
        } catch (err) {
            errData = 'The request has failed. Please contact the ELG team.'
        }
        this.setState({ error: errData });//render json as error report
    }

    versionError = (errData) => {
        if (errData.hasOwnProperty("described_entity") && errData.described_entity.hasOwnProperty("version")) {
            const veresionErrorArray = errData.described_entity.version.map(item => item);
            return veresionErrorArray;
        } else {
            return null;
        }
    }

    ShowErrorMessage(error) {
        return <div>
            {error && <div >
                <h3>Error</h3>
                <div className="boxed">
                    <Paper elevation={13} >
                        <code>
                            <pre id="special">
                                {JSON.stringify(error)}
                            </pre>
                        </code>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    redirectToLandingPage = (response) => {
        let url = '/myItems';
        try {
            const pk = response.data.new_version_pk;
            switch (this.props.resource.resource_type) {
                case PROJECT: url = `/project/${pk}`; break;
                case ORGANIZATION: url = `/organization/${pk}`; break;
                case TOOL_SERVICE: url = `/tool-service/${pk}`;; break;
                case CORPUS: url = `/corpus/${pk}`; break;
                case LCR: url = `/lcr/${pk}`; break;
                case LD: url = `/ld/${pk}`; break;
                case "ToolService": url = `/tool-service/${pk}`; break;
                case "LexicalConceptualResource": url = `/lcr/${pk}`; break;
                case "LanguageDescription": url = `/ld/${pk}`; break;
                default: url = `/myItems`; break;
            }
        } catch (err) {
            console.log("cannot redirect");
            url = `/myItems`;
        }
        this.props.history.push({ pathname: url });
    }

    getLanguageResource = () => {
        return <>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg'>
                <DialogContent>
                    <Typography className="pb-3" align="center" color="primary" variant="body1">Please fill in the following fields in order to create a new version for the record <small className="teal--font">{this.props.resource.resource_name}</small></Typography>
                    <DialogContentText component={"div"}>
                        <TextField className="pb-3 wd-100"
                            //type="text"
                            required={true}
                            label={"Version"}
                            placeholder={"1.0.0"}
                            variant="outlined"
                            helperText={
                                <span>
                                    The new version of the record that will be created.
                                    Recommended format: major_version.minor_version.patch (see semantic versioning guidelines at <a target="_blank" rel="noopener noreferrer" href="http://semver.org"> http://semver.org</a> <OpenInNewIcon style={{ width: 10, height: 10 }} />)
                                </span>
                            }
                            value={this.state.version}
                            onChange={(e) => { this.setState({ version: e.target.value }) }}
                        />
                        <div className="pb-3">
                            <DateComponent
                                className="wd-100"
                                label="version date"
                                help_text="The date of the LRT version (latest update of the particular version if possible)"
                                initialValue={this.state.date || null}
                                field="version_date"
                                updateModel={this.updateDate}
                            />
                        </div>
                        {[TOOL_SERVICE, "ToolService"].includes(this.props.resource.resource_type) && <div className="pb-3">
                            <FormControlLabel className="upload-checkboxes p-05 wd-100"
                                control={<Checkbox
                                    checked={this.state.functional_service === true ? true : false}
                                    color="primary"
                                    inputProps={{ 'aria-label': 'functional service checkbox' }}
                                    onChange={e => { this.setState({ functional_service: e.target.checked }); }}
                                />}
                                label={
                                    <span>
                                        {messages.label_functional_service} |&nbsp;
                                        <a href={messages.dialog_functional_service_infoLink_message} target="_blank" rel="noopener noreferrer">If the metadata record is for a service to be integrated in ELG</a>
                                        <OpenInNewIcon style={{ width: 10, height: 10 }} />
                                    </span>
                                }
                            />
                        </div>}
                    </DialogContentText>
                    <DialogContentText component={"div"}>
                        {this.state.error && this.ShowErrorMessage(this.state.error)}
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        onClick={() => this.disableDisplay()}
                        autoFocus
                    >
                        cancel
                    </Button>
                    <Tooltip title={this.state.version ? `create a new record with ${this.state.version} version` : 'Version is a required field'}>
                        <span>
                            <Button
                                color="primary"
                                disabled={this.state.loading || !this.state.version}
                                onClick={
                                    () =>
                                        [TOOL_SERVICE, "ToolService"].includes(this.props.resource.resource_type) ?
                                            this.handleSubmit({ "version": this.state.version, "version_date": this.state.version_date, "elg_compatible_service": this.state.functional_service }) :
                                            this.handleSubmit({ "version": this.state.version, "version_date": this.state.version_date })
                                }
                            >
                                Create new version
                            </Button>
                        </span>
                    </Tooltip>
                </DialogActions>
            </Dialog>
        </>
    }

    get_Project_Organization = () => {
        return <>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg'>
                <DialogContent>
                    <Typography className="pb-3" align="center" color="primary" variant="body1">Please fill in the following fields in order to create a new version for the record <small className="teal--font">{this.props.resource.resource_name}</small></Typography>
                    <DialogContentText component={"div"}>
                        <TextField className="pb-3 wd-100"
                            //type="text"
                            required={true}
                            label={this.props.resource.resource_type === PROJECT ? "Project name" : "Organization name"}
                            placeholder={this.props.resource.resource_type === PROJECT ? "Project Y" : "Organization Y"}
                            variant="outlined"
                            helperText={this.props.resource.resource_type === PROJECT ? "The official title of the project" : "The official title of the organization"}
                            value={this.state.resource_name}
                            onChange={(e) => { this.setState({ resource_name: e.target.value }) }}
                        />
                    </DialogContentText>
                    <DialogContentText component={"div"}>
                        {this.state.error && this.ShowErrorMessage(this.state.error)}
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        onClick={() => this.disableDisplay()}
                        autoFocus
                    >
                        cancel
                    </Button>
                    <Tooltip title={this.state.resource_name ? `create a new record` : `${this.props.resource.resource_type === PROJECT ? 'Project' : 'Organization'} name is a required field`}>
                        <span>
                            <Button
                                color="primary"
                                disabled={this.state.loading || !this.state.resource_name}
                                onClick={() => this.handleSubmit({ "resource_name": this.state.resource_name })}
                            >
                                Create new version
                            </Button>
                        </span>
                    </Tooltip>
                </DialogActions>
            </Dialog>
        </>
    }

    render() {
        const { resource_type } = this.props.resource;
        if ((resource_type === CORPUS || resource_type === LCR || resource_type === LD || resource_type === ML_MODEL || resource_type === GRAMMAR || resource_type === OTHER || resource_type === Uncategorized_Language_Description || resource_type === TOOL_SERVICE || resource_type === "ToolService" || resource_type === "LexicalConceptualResource" || resource_type === "LanguageDescription")) {
            return this.getLanguageResource();
        } else if (resource_type === PROJECT || resource_type === ORGANIZATION) {
            return this.get_Project_Organization();
        } else {
            return <></>
        }
    }
}

export default withRouter(CreateVersion)