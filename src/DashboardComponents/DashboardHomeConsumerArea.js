
import React from "react";
import { Redirect, withRouter } from "react-router-dom";
import axios from "axios";
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import ProfileImage from "./../assets/images/undraw_site_content_re_4ctl.svg";
import { ReactComponent as DashboardImage } from "./../assets/images/undraw_statistic_chart_38b6.svg";
import CustomizedProgressBars from "./../componentsAPI/CommonComponents/LinearProgress";
import Button from '@material-ui/core/Button';
import { HEADER_BASE_URL, SHOW_CONSUMER_GRID_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES, SERVER_API_CONSUMER_STATS, SHOW_MY_VALIDATION_ROLES } from "./../config/constants";

class DashboardHomeConsumerArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = { keycloak: props.keycloak, data: "", user_stats: "" };
  }

  componentDidMount() {
    this.getUserQuotas();
  }

  getUserQuotas() {
    if (!this.state.keycloak || !this.state.keycloak.authenticated) {
      return;
    }
    let authorized = false;
    SHOW_CONSUMER_GRID_ROLES.forEach(item => {
      if (AUTHENTICATED_KEYCLOAK_USER_ROLES(this.state.keycloak).includes(item)) {
        authorized = true;
      }
    });
    if (!authorized) {
      return;
    }
    axios.get(SERVER_API_CONSUMER_STATS)
      .then(res => {
        this.setState({ user_stats: res.data.results });
      }).catch(err => console.log(err));
  }


  render() {
    if (!this.state.keycloak || !this.state.keycloak.authenticated) {
      return <Redirect to="/" />
    }
    //console.log(this.state.user_stats)
    const { given_name = "", family_name = "", email = "" } = this.state.keycloak.idTokenParsed || "";
    const { bytes = "", bytes_asr = "", calls = "", bytes_asr_percent = "", bytes_percent = "", calls_percent = "" } = this.state.user_stats[0] || "";

    let authorize_validator_section = false;
    SHOW_MY_VALIDATION_ROLES.forEach(item => {
      if (AUTHENTICATED_KEYCLOAK_USER_ROLES(this.state.keycloak).includes(item)) {
        authorize_validator_section = true;
      }
    });


    return (<>
      <Grid item container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={2} sm={2} xs={12}>
        <Grid item sm={12} xs={12} style={{ display: 'flex' }}>
          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
            <CardMedia>
              <Grid container direction="row" justifyContent="flex-start" alignItems="center" className="bg-soft-primary">
                <Grid item xs={7} className="profile-card--text p-3">
                  <Typography variant="caption" className="profile-card--username pt-05">{given_name} {family_name}</Typography>
                  <Typography variant="subtitle2" className="profile-card--username" component="p">{email}</Typography>
                </Grid>
                <Grid item xs={5} className="profile-card--img align-self-end">
                  <img src={ProfileImage} alt="profile" className="img-fluid" />
                </Grid>
              </Grid>
            </CardMedia>
            <CardContent>
              <Typography className="bold grey--dark--font bigger-text"> Welcome to your grid ! </Typography>
              <Typography variant="body2" color="textSecondary"> Here you can: </Typography>
              <div className="dashboard-text-lists">
                <ul>
                  <li><Typography variant="body2" color="textSecondary">view and update your profile</Typography></li>
                  <li><Typography variant="body2" color="textSecondary">view your items and your tasks</Typography></li>
                  <li><Typography variant="body2" color="textSecondary">access creation forms</Typography></li>
                  <li><Typography variant="body2" color="textSecondary">upload items</Typography></li>
                </ul>
              </div>
            </CardContent>
            <CardActions>
              <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                <Grid item sm={12}>
                  <a className="inner-link-outlined--teal" href={`${HEADER_BASE_URL}profile`} style={{ float: "left", fontWeight: "bolder", fontSize: "0.875rem" }}>View profile</a>
                </Grid>
              </Grid>
            </CardActions>
          </Card>
        </Grid>
      </Grid>

      <Grid item container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={2} sm={authorize_validator_section ? 8 : 10} xs={12}>
        <Grid item sm={12} xs={12} style={{ display: 'flex' }}>
          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
            <CardContent>
              <Grid item container sm={12}>
                <Grid item container justifyContent="flex-start" alignItems="center" sm={10} className="padding5">
                  <Typography variant="h3" className="teal--font pb-3">Daily usage</Typography>
                </Grid>
                <Grid item container sm={2} justifyContent="flex-end">
                  <DashboardImage className="big-icon" />
                </Grid>


                <Grid item container sm={12} className="padding5">
                  <Grid item sm={4} className="padding5">
                    <Typography color="textSecondary" variant="caption">calls</Typography>
                    <CustomizedProgressBars variant="determinate" value={1 - Number(calls_percent)} />
                    <Typography color="textSecondary" variant="caption">remaining: {calls}</Typography>
                  </Grid>
                  <Grid item sm={4} className="padding5">
                    <Typography color="textSecondary" variant="caption">bytes (for processing text)</Typography>
                    <CustomizedProgressBars variant="determinate" value={1 - Number(bytes_percent)} />
                    <Typography color="textSecondary" variant="caption">remaining: {bytes}</Typography>
                  </Grid>

                  <Grid item sm={4} className="padding5">
                    <Typography color="textSecondary" variant="caption">bytes (for processing binary data)</Typography>
                    <CustomizedProgressBars variant="determinate" value={1 - Number(bytes_asr_percent)} />
                    <Typography color="textSecondary" variant="caption">remaining: {bytes_asr}</Typography>
                  </Grid>



                </Grid>


              </Grid>
            </CardContent>
            <CardActions className="pt-2">
              <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                <Grid item sm={2}><Button classes={{ root: 'inner-link-outlined--teal' }} onClick={(event, newValue) => { this.props.history.push("/myUsage") }}> My usage </Button></Grid>
                <Grid item sm={2}><Button classes={{ root: 'inner-link-outlined--teal' }} onClick={(event, newValue) => { this.props.history.push("/myDownloads") }}> My downloads </Button>  </Grid>
              </Grid>
            </CardActions>

          </Card>
        </Grid>
      </Grid>


      {authorize_validator_section && <Grid item container direction="row" justifyContent="flex-start" alignItems="stretch" sm={2} xs={12}>
        <Grid item sm={12} xs={12} style={{ display: 'flex' }}>
          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
            <CardMedia className="dashboard-card-media">
              <Grid container direction="row" justifyContent="flex-start" alignItems="center" className="bg-dark-primary">
                <Grid item xs={12} >
                  <Typography className="grey--font-offwhite pt-3 pb-3  bigger-text bold" style={{ marginTop: "1.7em", marginBottom: "1.7em", paddingLeft: "1em" }}> Validating items </Typography>
                </Grid>
              </Grid>
            </CardMedia>
            <CardContent>
              <Typography variant="body1" color="textSecondary">Access the area for validators, and view your tasks. </Typography>
            </CardContent>
            <CardActions style={{ justifyContent: "flex-start" }} className="pt-2">
              <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={(event, newValue) => { this.props.history.push("/myValidations") }}> My Validations </Button>
            </CardActions>
            {
              ["admin", "content_manager", "technical_validator"].filter(item => AUTHENTICATED_KEYCLOAK_USER_ROLES(this.state.keycloak).includes(item)).length > 0 &&
              <CardActions style={{ justifyContent: "flex-start" }}>
                <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={(event, newValue) => { this.props.history.push("/batch-validation/batch-validate/") }}> Batch service registration </Button>
              </CardActions>
            }
          </Card>
        </Grid>
      </Grid>}

    </>
    );
  }
}

export default withRouter(DashboardHomeConsumerArea);
