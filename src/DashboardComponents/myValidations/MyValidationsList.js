import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withRouter, Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import ResourceStatus from "../ResourceStatus";
import { TOOL_SERVICE, CORPUS, LD, PROJECT, ORGANIZATION, LCR, AUTHENTICATED_KEYCLOAK_USER_USERNAME, getUrlToLandingPageFromCatalogue, IS_ADMIN, ML_MODEL, GRAMMAR, OTHER, Uncategorized_Language_Description } from "../../config/constants";
//import { ReactComponent as CreatedIcon } from "./../assets/elg-icons/editor/calendar-clock.svg";
//import { ReactComponent as ModifiedIcon } from "./../assets/elg-icons/editor/calendar-refresh.svg";
//import CircularProgress from '@material-ui/core/CircularProgress';
import Checkbox from '@material-ui/core/Checkbox';
//import ResourceActions from "../../componentsAPI/CommonComponents/actions/ResourceActions";
import Chip from '@material-ui/core/Chip';
import Tooltip from '@material-ui/core/Tooltip';

class MyValidationsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resourceData: props.resource, expanded: false, expandedLang: false,
            expandedLicence: false, keycloak: props.keycloak, anchorEl: false, loading: false,
        };
    }

    handleClick = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: false });
    }

    navigateToResourceDetail(resource, isOwner, isSupervisor) {
        if (resource.status === "draft") {
            return "";
        }
        if (isOwner || isSupervisor || IS_ADMIN(this.props.keycloak)) {
            const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
            return url2LandingPage;
        }
        const permitted_statuses = ["ingested", /*"approved",*/ "rejected", "published"];
        if (!permitted_statuses.includes(resource.status)) {
            return "";
        } else {
            const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
            return url2LandingPage;
        }
    }

    handleCheck = (e) => {
        if (e.target.checked) {
            this.props.addCheckedResource2List(this.state.resourceData);
        } else {
            this.props.removeResourceFromCheckedList(this.state.resourceData);
        }
    }

    convertData = (resource) => {
        let data = {
            pk: resource.id,
            management_object: {
                unpublication_requested: resource.unpublication_requested,
                size: resource.size,
                status: "",
                legally_valid: resource.legally_valid,
                metadata_valid: resource.metadata_valid
            },
            described_entity: {
                field_value: {
                    entity_type: {
                        field_value: { "en": "" }
                    },
                    project_name: {
                        field_value: { "en": "" }
                    },
                    organization_name: {
                        field_value: { "en": "" }
                    },
                    resource_name: {
                        field_value: { "en": "" }
                    },
                    lr_subclass: {
                        field_value: {
                            lr_type: {
                                field_value: ""
                            }
                        }
                    }
                }
            }
        };
        if (resource.status === "internal") {
            data.management_object.status = "i";
        } else if (resource.status === "draft") {
            data.management_object.status = "d";
        } else if (resource.status === "published") {
            data.management_object.status = "p";
        } else if (resource.status === "ingested") {
            data.management_object.status = "g";
            //} else if (resource.status === "approved") {
            //    data.management_object.status = "a";
        } else if (resource.status === "unpublished") {
            data.management_object.status = "u";
        } else if (resource.status === "rejected") {//???
            data.management_object.status = "r";
        } else {
            console.log(resource.status);
        }
        if (resource.entity_type === PROJECT) {
            data.described_entity.field_value.entity_type.field_value = PROJECT;
            data.described_entity.field_value.project_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === TOOL_SERVICE || resource.resource_type === "ToolService") {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "ToolService";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === CORPUS) {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = CORPUS;
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === LCR || resource.resource_type === "LexicalConceptualResource") {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "LexicalConceptualResource";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === LD || resource.resource_type === ML_MODEL || resource.resource_type === GRAMMAR || resource.resource_type === OTHER || resource.resource_type === Uncategorized_Language_Description) {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "LanguageDescription";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.entity_type === ORGANIZATION) {
            data.described_entity.field_value.entity_type.field_value = ORGANIZATION;
            data.described_entity.field_value.organization_name.field_value = { "en": resource.resource_name }
        }
        return data;
    }

    render() {
        //console.log(this.state.resourceData)
        const resource = this.state.resourceData;
        //const data = this.convertData(resource);
        const isOwner = resource.curator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        const isContentManager = resource.content_manager === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        //const isSupervisor = resource.supervisor === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        //const isLegalValidator = resource.legal_validator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        //const isMetadataValidator = resource.metadata_validator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);

        const resourceLandingPage = this.navigateToResourceDetail(resource, isOwner, isContentManager);
        return (
            <React.Fragment>
                <Paper className="DashboardListItem" style={{ padding: "0.5em" }}>
                    <Grid item xs={12} container spacing={2} justifyContent="space-between" alignItems="center" className="ResourceListItem--inner">
                        <Grid item sm={1}>
                            <Checkbox
                                checked={(this.props.checkedList.find(item => item.id === resource.id) && this.props.checkedList.find(item => item.id === resource.id).id >= 0) ? true : false}
                                size="small"
                                onChange={e => { this.handleCheck(e) }}
                            />
                        </Grid>
                        <Grid item container xs={8}>
                            <Grid item sm={12}>
                                {resourceLandingPage && <Typography variant="h5" className="ResourceListTitle pt-05" ><Link to={resourceLandingPage}>{resource.resource_name}</Link></Typography>}
                                {!resourceLandingPage && <Tooltip title="inaccessible to you at this status"><Typography variant="h5" className="ResourceListTitle pt-05" >{resource.resource_name}</Typography></Tooltip>}

                            </Grid>

                            <Grid item container sm={12}>
                                <Grid item sm={12}>
                                    {resource.version && resource.version !== "undefined" && <Grid item><Typography variant="caption" className="grey--font"> Version: {resource.version} </Typography></Grid>}
                                    <Grid item><Typography variant="caption" className="grey--font">{resource.entity_type !== "LanguageResource" ? resource.entity_type : resource.resource_type}</Typography></Grid>
                                    <Grid item><Typography variant="caption" className="grey--font">{resource.repository}</Typography></Grid>
                                    {resource.ingestion_date ? <Grid item container direction="column" justifyContent="flex-start" alignItems="flex-start" >
                                        {resource.ingestion_date && <Grid item><Typography variant="caption">submitted:&nbsp;
                                            {new Intl.DateTimeFormat("en-GB", {
                                                year: "numeric",
                                                month: "long",
                                                day: "2-digit"
                                            }).format(new Date(resource.ingestion_date))} </Typography></Grid>
                                        }
                                    </Grid> : void 0}
                                    {resource.legal_validation_date ? <Grid item container direction="column" justifyContent="flex-start" alignItems="flex-start" >
                                        {resource.legal_validation_date && <Grid item><Typography variant="caption">legal validation date:&nbsp;
                                            {new Intl.DateTimeFormat("en-GB", {
                                                year: "numeric",
                                                month: "long",
                                                day: "2-digit"
                                            }).format(new Date(resource.legal_validation_date))} </Typography></Grid>
                                        }
                                    </Grid> : void 0}
                                    {resource.metadata_validation_date ? <Grid item container direction="column" justifyContent="flex-start" alignItems="flex-start" >
                                        {resource.metadata_validation_date && <Grid item><Typography variant="caption">metadata validation date:&nbsp;
                                            {new Intl.DateTimeFormat("en-GB", {
                                                year: "numeric",
                                                month: "long",
                                                day: "2-digit"
                                            }).format(new Date(resource.metadata_validation_date))} </Typography></Grid>
                                        }
                                    </Grid> : void 0}

                                    <Grid item sm={12} >
                                        <ResourceStatus status={resource.status} unpublication_requested={resource.unpublication_requested} marked_as_deleted={resource.marked_as_deleted} xSize={6} keycloak={this.state.keycloak} />
                                    </Grid>

                                    <Grid item sm={12} container direction="row" justifyContent="flex-start" alignItems="flex-start" className="pt-05 pb-05">
                                        {/*resource.processable === true && <Chip size="small" label={"processable"} className="ChipTagGreyDark" />*/}
                                        {resource.has_data === true && <Chip size="small" label={"has data"} className="ChipTagGreen" />}
                                        {resource.resubmitted === true && <Chip size="small" label={"resubmitted"} className="ChipTagYellow" />}
                                    </Grid>
                                    <Grid item sm={12}>
                                    {resource.legal_validator && <Grid item className="padding5">
                                        <span><Typography variant="body2" className="Text-fontSize--12 grey--font">legal validator: {resource.legal_validator} </Typography></span>                                        
                                    </Grid>}
                                    {resource.metadata_validator && <Grid item className="padding5">
                                        <span><Typography variant="body2" className="Text-fontSize--12 grey--font">metadata validator: {resource.metadata_validator}</Typography> </span>                                        
                                    </Grid>}
                                    {resource.technical_validator && <Grid item className="padding5">
                                        <span><Typography variant="body2" className="Text-fontSize--12 grey--font">technical validator: {resource.technical_validator}</Typography> </span>                                        
                                    </Grid>}
                                    {resource.curator && <Grid item className="padding5">
                                        <span><Typography variant="body2" className="Text-fontSize--12 grey--font">curator: {resource.curator}</Typography></span>
                                    </Grid>}
                                </Grid>
                                </Grid>
                                
                                <Grid item sm={12}>
                                    {resource.review_comments && resource.review_comments.length > 0 && <div className="review-card"><Typography variant="body2" className="Text-fontSize--12 bold">Review comments</Typography>
                                        {resource.review_comments.map((comment, index) => (<div key={index}><Typography variant="caption" className="pt-05" >{comment}</Typography></div>))} </div>}
                                    {resource.validator_notes && resource.validator_notes.length > 0 && <div className="review-card mt-1"><Typography variant="body2" className="Text-fontSize--12 bold">Validator notes</Typography>
                                        {resource.validator_notes.map((note, index) => (<div key={index}><Typography variant="caption" className="pt-05" >{note}</Typography></div>))}</div>}

                                </Grid>

                            </Grid>


                        </Grid>


                        {/*<Grid item xs={2} className="container-align-center">
                            {
                                this.state.loading ?
                                    <CircularProgress /> :
                                    <ResourceActions
                                        keycloak={this.state.keycloak}
                                        isOwner={isOwner}
                                        isSupervisor={isSupervisor}
                                        isLegalValidator={isLegalValidator}
                                        isMetadataValidator={isMetadataValidator}
                                        pk={data.pk}
                                        data={data}
                                        className="inner-actions-link-no--border--teal"
                                        updateRecord={this.props.updateRecords}
                                        tab='myval'
                                    />
                            }
                        </Grid>*/}

                        <Grid item container sm={3} justifyContent="flex-start">
                            
                            {resource.legally_valid && <Grid item sm={12} className="padding5 pt1"> 
                            <Typography className="Text-fontSize--12 grey--font">legally valid <span> </span>
                                {resource.legally_valid === "yes" &&  <span className="badge bg-green mr-05">yes</span> }
                                {resource.legally_valid === "no" &&  <span className="badge bg-red mr-05">rejected</span> }
                                {resource.legally_valid === "not validated" &&  <span className="badge bg-light-pink mr-05">not validated</span> }                            
                            </Typography>
                                
                                
                               
                            </Grid>}

                            {resource.metadata_valid && <Grid item sm={12} className="padding5">
                                 <Typography className="Text-fontSize--12 grey--font">metadata valid <span> </span>
                                 {resource.metadata_valid === "yes" && <span className="badge bg-green mr-05">yes</span>}
                                {resource.metadata_valid === "no" && <span className="badge bg-red mr-05">rejected</span>}
                                {resource.metadata_valid === "not validated" && <span className="badge bg-light-pink mr-05">not validated</span>}
                                 </Typography>  
                                
                            </Grid>}

                            {resource.technically_valid && <Grid item sm={12} className="padding5">
                               <Typography className="Text-fontSize--12 grey--font">technically valid <span> </span> 
                                {resource.technically_valid === "yes" && <span className="badge bg-green mr-05">yes</span>}
                                {resource.technically_valid === "no" && <span className="badge bg-red mr-05">rejected</span>}
                                {resource.technically_valid === "not validated" && <span className="badge bg-light-pink mr-05">not validated</span>}
                                </Typography> 
                                
                            </Grid>}
                            {resource.service_status && <Grid item sm={12} className="padding5">
                                 <Typography className="Text-fontSize--12 grey--font">service status <span> </span> 
                                    {resource.service_status === "Completed" && <span className="badge bg-green mr-05">{resource.service_status}</span>}
                                    {resource.service_status === "Pending" && <span className="badge bg-light-pink mr-05">{resource.service_status}</span>}
                                    {resource.service_status === "New" && <span className="badge bg-green-dark mr-05">{resource.service_status}</span>}
                                 </Typography> 
                                
                            </Grid>}

                        </Grid>
                    </Grid>
                </Paper >
            </React.Fragment >
        );

    }
}
export default withRouter(MyValidationsList);
