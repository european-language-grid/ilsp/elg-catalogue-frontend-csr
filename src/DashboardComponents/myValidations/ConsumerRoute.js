import React from "react";
import { withRouter, Redirect } from "react-router-dom";
//import GoToCatalogue from "../../componentsAPI/CommonComponents/GoToCatalogue";
//import Container from '@material-ui/core/Container';
//import Typography from '@material-ui/core/Typography';
import { /*SHOW_CONSUMER_GRID_ROLES, SHOW_DASHBOARD_ROLES,*/ AUTHENTICATED_KEYCLOAK_USER_ROLES, loginFunction } from "../../config/constants";

class ConsumerRoute extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: 0, keycloak: props.keycloak, UserRoles: [], sendUserToKeycloak: false };
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
        //this.getSearchParams();
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        /*SHOW_DASHBOARD_ROLES.concat(SHOW_CONSUMER_GRID_ROLES).forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        })*/
        isAuthorized = this.props.keycloak && this.props.keycloak.authenticated;
        return isAuthorized;
    }

    componentDidUpdate(prevProps, prevState) {
        let search1 = this.props && this.props.location && this.props.location.search;
        let search2 = prevProps && prevProps.location && prevProps.location.search;
        if (search1 !== search2) {
            //this.getSearchParams();
        }
    }

    getSearchParams = () => {
        let searchParamsFromURL = this.props.location ? this.props.location.search : "";
        if (searchParamsFromURL && searchParamsFromURL.indexOf("?") >= 0) {
            searchParamsFromURL = searchParamsFromURL.substring(searchParamsFromURL.indexOf("?") + 1);
            if (searchParamsFromURL.indexOf("&") >= 0) {
                const paramsArray = searchParamsFromURL.split("&");
                for (let index = 0; index < paramsArray.length; index++) {
                    const param = paramsArray[index];
                    if (param.indexOf("=") >= 0 && param.indexOf("auth") >= 0) {
                        const auth = param.split("=")[1];
                        if (auth === "true") {
                            //this.setState({ sendUserToKeycloak: true });
                            return true;
                        } else {
                            //this.setState({ sendUserToKeycloak: false });
                            return false;
                        }
                    }
                }
            } else {
                if (searchParamsFromURL.indexOf("=") >= 0 && searchParamsFromURL.indexOf("auth") >= 0) {
                    const auth = searchParamsFromURL.split("=")[1];
                    if (auth === "true") {
                        //this.setState({ sendUserToKeycloak: true });
                        return true;
                    } else {
                        //this.setState({ sendUserToKeycloak: false });
                        return false;
                    }
                }
            }
        }
        return false;
    }


    render() {
        if (this.props.keycloak && !this.props.keycloak.authenticated) {
            if (this.getSearchParams() === true) {
                loginFunction(this.props.keycloak);
            }
        }

        if (!this.props.keycloak || !this.props.keycloak.authenticated) {
            return <Redirect to="/" />
        }
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);
        if (!isAuth) {
            return <div>
                {/*<div className="editor--header">
                    <Container maxWidth="lg"><Typography variant="h1" className="padding5 font-color-white">ELG EDITOR</Typography></Container>
                    <GoToCatalogue />
                </div>
                <Container style={{ padding: '0', maxWidth: "100%" }}>
                    <div>You do not have sufficient rights.</div>
        </Container>*/}
            </div>
        }
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
}
export default withRouter(ConsumerRoute);