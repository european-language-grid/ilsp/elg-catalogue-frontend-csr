import React from "react";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import messages from "../../../config/messages";
import { BULK_ACTION_TECHNICALLY_VALIDATE } from "../../../config/editorConstants";
import { toast } from "react-toastify";
import { getImage } from "../../bulk_actions/commonFunctions";
import ResourceStatus from "../../ResourceStatus";
import { keycloak } from "../../../App";


export default class PerformMetadataTechnicalValidation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayDialog: true, loading: false, ingested_items: this.props.ingested_items,
            dialogueIndex: 0, selectedOption: "", technicalSelectedOption: "", reasons: "", validator_notes: ""
        };
    }


    handleSubmit = () => {
        const ids = this.state.ingested_items.map(item => item.id).join(",");
        this.setState({ loading: true });
        if (this.state.selectedOption === "Reject" || this.state.technicalSelectedOption === "Reject") {
            axios.patch(BULK_ACTION_TECHNICALLY_VALIDATE(ids), {
                "metadata_approve": this.state.selectedOption === "Approve",
                "technically_approve": this.state.technicalSelectedOption === "Approve",
                "review_comments": this.state.reasons,
                "validator_notes": this.state.validator_notes
            })
                .then(res => { this.success("Records submited successfully.", res); })
                .catch(err => { this.failure("Records submission failed.", err); })
        }
        else if (this.state.selectedOption === "Approve" && this.state.technicalSelectedOption === "Approve") {
            axios.patch(BULK_ACTION_TECHNICALLY_VALIDATE(ids), {
                "metadata_approve": true,
                "technically_approve": true,
                "validator_notes": this.state.validator_notes
            })
                .then(res => { this.success("Records submited successfully.", res); })
                .catch(err => { this.failure("Records submission failed.", err); })
        }
        else {
            // Validator notes only
            axios.patch(BULK_ACTION_TECHNICALLY_VALIDATE(ids), { "validator_notes": this.state.validator_notes })
                .then(res => { this.success("Records submited successfully.", res); })
                .catch(err => { this.failure("Records submission failed.", err); })
        }

    }

    success = (msg, res) => {
        this.setState({ loading: false });
        toast.success(msg, { autoClose: 3500 });
        this.disableDisplay();
        try {
            if (res && res.data && res.data.length > 0) {
                this.props.updateRecords(res.data);
                //window.location.reload();
            }
        } catch (err) {
        }
    }

    failure = (msg, err = "") => {
        toast.error(msg, { autoClose: 3500 });
        this.setState({ loading: false });
        this.disableDisplay();
        try {
            if (err && err.response && err.response.data) {
                console.log(err);
            } else {
                console.log(err);
            }
        } catch (catcherr) {
            console.log(err);
        }
    }

    disableDisplay = (event, reason) => {
        if (reason === 'backdropClick') {// reason !== 'escapeKeyDown'
            return;
        }
        this.setState({ displayDialog: false });
        this.props.clearSelect("");
    }


    render() {
        if (this.state.ingested_items.length === 0) {
            return <></>;
        }

        const options = ["Approve", "Reject"];
        const isTechnical = true;
        const otherStatusItems = this.props.checkedList?.filter(item => !this.state.ingested_items.includes(item)) || []

        if (this.state.dialogueIndex === 0) {
            return <div>
                <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth='lg'>
                    {!this.state.loading && <DialogTitle> You are about to {messages.validator_action_technical} to the following records.</DialogTitle>}
                    {this.state.loading && <DialogTitle> Please wait while we are submitting your request. <CircularProgress size={20} /></DialogTitle>}
                    <DialogContent>
                        <DialogContentText component={"div"}>
                            {this.state.ingested_items.map((item, index) => {
                                const image = getImage(item);
                                return <div style={{ marginBottom: "5px" }} key={JSON.stringify(item)}>
                                    <Grid item xs={12} container alignItems="center">
                                        <Grid item xs={6} >
                                            <Typography variant="h5">
                                                <span style={{ marginRight: "10px" }}>{image}</span>
                                                <span>{item.resource_name}</span>
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <ResourceStatus status={item.status} xSize={12} keycloak={keycloak} />
                                        </Grid>
                                    </Grid>
                                </div>
                            })}
                        </DialogContentText>
                        {otherStatusItems.length > 0 && <DialogContentText component={"div"}>
                            <div>
                                <hr />
                                <Typography variant="h5" style={{ marginBottom: "10px" }}>
                                    {messages.editor_bulk_general_message}
                                </Typography>
                                {otherStatusItems.map((excludeItem, excludeItemIndex) => {
                                    const image = getImage(excludeItem);
                                    return <div style={{ marginBottom: "5px" }} key={JSON.stringify(excludeItem)}>
                                        <Grid item xs={12} container alignItems="center">
                                            <Grid item xs={6} >
                                                <span>
                                                    <Typography variant="h5">
                                                        <span style={{ marginRight: "10px" }}>{image}</span>
                                                        <span>{excludeItem.resource_name}</span>
                                                    </Typography>
                                                </span>
                                            </Grid>
                                            <Grid item xs={2}>
                                                <ResourceStatus status={excludeItem.status} xSize={12} keycloak={keycloak} />
                                            </Grid>
                                        </Grid>
                                    </div>
                                })}
                            </div>
                        </DialogContentText>
                        }
                    </DialogContent>
                    <hr />
                    <DialogActions>
                        <Button
                            color="primary"
                            disabled={this.state.loading}
                            onClick={this.disableDisplay}
                            autoFocus
                        >
                            {messages.dialog_validationResponse_close_button}
                        </Button>
                        <Button
                            color="primary"
                            disabled={this.state.loading}
                            onClick={() => { this.setState({ dialogueIndex: 1 }) }}
                        >
                            Continue
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        } else if (this.state.dialogueIndex === 1) {
            return <Dialog
                open={this.state.displayDialog}
                onClose={this.disableDisplay}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="-dialog-title">
                    <Typography className="pb-3" align="center" color="primary" variant="body1"> Please fill in the following fields in order to validate the records</Typography>
                </DialogTitle>

                <DialogContent>

                    <DialogContentText component={"div"}>
                        <Typography variant='h4'>Metadata validation: </Typography>
                        <RadioGroup
                            aria-label="validate"
                            name="validate"
                            value={this.state.selectedOption}
                            onChange={(e) => { this.setState({ reasons: "", selectedOption: e.target.value }) }}
                        >
                            {options.map((option) => (
                                <FormControlLabel value={option} key={option} control={<Radio />} label={option} />
                            ))}
                        </RadioGroup>

                        {
                            isTechnical && <>
                                <Typography variant='h4'>Technical validation: </Typography>
                                <RadioGroup
                                    aria-label="validate"
                                    name="validate"
                                    value={this.state.technicalSelectedOption}
                                    onChange={(e) => { this.setState({ reasons: "", technicalSelectedOption: e.target.value }) }}
                                >
                                    {options.map((option) => (
                                        <FormControlLabel value={option} key={option} control={<Radio />} label={option} />
                                    ))}
                                </RadioGroup>
                            </>
                        }
                    </DialogContentText>

                    <DialogContentText component={"div"}>

                        {
                            (this.state.selectedOption === "Reject" || this.state.technicalSelectedOption === "Reject") &&
                            <Grid>
                                <Grid item>
                                    <TextField className="pb-3 wd-100"
                                        multiline
                                        minRows={8}
                                        required={this.state.selectedOption === "Reject" || this.state.technicalSelectedOption === "Reject"}
                                        label={"Reasons"}
                                        placeholder={"Reason for rejection"}
                                        variant="outlined"
                                        helperText={
                                            <span>
                                                Please state the reasons for rejecting the records (to be sent out to the curator)
                                            </span>
                                        }
                                        value={this.state.reasons}
                                        onChange={(e) => { this.setState({ reasons: e.target.value }) }}
                                    />
                                </Grid>
                            </Grid>
                        }

                        <div>
                            <Grid>
                                <Grid item>
                                    <TextField className="pb-3 wd-100"
                                        multiline
                                        minRows={8}
                                        required={false}
                                        label={"Validator notes"}
                                        placeholder={"Validator notes"}
                                        variant="outlined"
                                        helperText={
                                            <span>
                                                Internal notes (visible only to other validators)
                                            </span>
                                        }
                                        value={this.state.validator_notes}
                                        onChange={(e) => { this.setState({ validator_notes: e.target.value }) }}
                                    />
                                </Grid>
                            </Grid>
                        </div>
                    </DialogContentText>

                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        disabled={this.state.loading ||
                            (this.state.selectedOption === "Reject" && !this.state.reasons) ||
                            (this.state.technicalSelectedOption === "Reject" && !this.state.reasons) ||
                            ((!this.state.selectedOption && !this.state.validator_notes) ||
                                (isTechnical && !this.state.technicalSelectedOption && !this.state.validator_notes))}
                        onClick={() => this.handleSubmit()}
                    >
                        Submit
                    </Button>
                    <span>
                        <Button
                            color="primary"
                            disabled={this.state.loading}
                            onClick={() => this.disableDisplay()}
                        >
                            Cancel
                        </Button>
                    </span>


                </DialogActions>
            </Dialog>
        } else {
            return <></>
        }

    }
}