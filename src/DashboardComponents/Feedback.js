
import React from "react";
import { Redirect, withRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import DashboardAppBar from "./DashboardAppBar";
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Feedback extends React.Component {
  constructor(props) {
    super(props);
    this.state = { keycloak: props.keycloak, data: "" };
  }

  render() {
    if (!this.state.keycloak || !this.state.keycloak.authenticated) {
      return <Redirect to="/" />
    }

    return (<>
      <Helmet>
        <title>European Language Grid Feedback</title>
      </Helmet>

      <DashboardAppBar {...this.props} />
      <div className="editor-container pt-50 pb-150">
        <Container maxWidth="xl">
          <Typography className="dashboard-title-box pb-3">Feedback</Typography>

          <Grid container direction="row" justifyContent="flex-start" alignItems="center" className="mb2">
            <Grid item sm={12} xs={12}>
              <Card>
                <CardContent>
                  <div className="profile-card--text mb-2">
                    <Typography variant="h6"> We welcome your feedback (questions, suggestions, problems) regarding the ELG platform. </Typography>
                  </div>
                  <Typography variant="body2" color="textSecondary" component="p"> You can submit a question at <a href="https://www.european-language-grid.eu/contact/" target="_blank" rel="noopener noreferrer">https://www.european-language-grid.eu/contact/</a>  or participate in the discussion and check previous feedback at our dedicated GitLab repository: <a href="https://gitlab.com/european-language-grid/platform/elg-platform" target="_blank" rel="noopener noreferrer">https://gitlab.com/european-language-grid/platform/elg-platform</a>. </Typography>

                  <Typography variant="body2" color="textSecondary" component="p">To access the GitLab repository, you will need to have a GitLab account; if you don't have one already, it's free and easy to register at <a href="https://gitlab.com/users/sign_up" target="_blank" rel="noopener noreferrer">https://gitlab.com/users/sign_up</a>. </Typography>

                  <Typography variant="body2" color="textSecondary" component="p">Once logged in, you can join the ongoing conversation at our list of issues: <a href="https://gitlab.com/european-language-grid/platform/elg-platform/-/issues" target="_blank" rel="noopener noreferrer">https://gitlab.com/european-language-grid/platform/elg-platform/-/issues</a>, or create a new issue at: <a href="https://gitlab.com/european-language-grid/platform/elg-platform/-/issues/new" target="_blank" rel="noopener noreferrer">https://gitlab.com/european-language-grid/platform/elg-platform/-/issues/new</a>.</Typography>

                  <Typography variant="body2" color="textSecondary">We recommend the use of labels for easier tracking:
                    <ul>
                      <li> clarification: for asking questions on the use of the platform or metadata descriptions</li>
                      <li> bug: for bugs or technical issues you've run into while using the platform</li>
                      <li> feature: suggestions for improvements of existing features or adding new features.</li>
                    </ul>
                  </Typography>

                  <Typography variant="body2" color="textSecondary" component="p">You can also find more information on the use of the platform in the ELG User Manual at: <a href="https://european-language-grid.readthedocs.io" target="_blank" rel="noopener noreferrer">https://european-language-grid.readthedocs.io</a>.</Typography>
                </CardContent>
              </Card>


            </Grid>
          </Grid>


        </Container>

      </div>

    </>
    );
  }
}

export default withRouter(Feedback);
